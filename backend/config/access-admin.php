<?php
return [
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'roles' => ['admin', 'accountant','corp_accountant'],
            ]
        ],
        'denyCallback' => function () {
            throw new \yii\web\NotFoundHttpException();
        }
    ]
];