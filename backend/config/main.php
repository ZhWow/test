<?php

use yii\helpers\ArrayHelper;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'admin' => ArrayHelper::merge([
            'class' => 'mdm\admin\Module'
        ], require('access-admin.php')
        ),
        'admin-panel' => ArrayHelper::merge([
            'class' => 'backend\modules\admin\Module'
        ], require('access-admin.php')
        ),
        'agent' => ArrayHelper::merge([
            'class' => 'backend\modules\agent\Module'
        ], require('access-login.php')
        ),
        'translate' => ArrayHelper::merge([
            'class' => 'backend\modules\translate\Module'
        ], require('access-login.php')
        ),
        'integra' => ArrayHelper::merge([
            'class' => 'backend\modules\integra\Module'
        ], require('access-login.php')
        ),
        'hotels' => ArrayHelper::merge([
            'class' => 'backend\modules\hotels\Module'
        ], require('access-login.php')
        ),
        'accountant' => ArrayHelper::merge([
            'class' => 'backend\modules\accountant\Module'
        ], require('access-login.php')
        ),
        'service' => ArrayHelper::merge([
            'class' => 'backend\modules\service\Module'
        ], require('access-login.php')
        ),
        'hotel_payment' => [
            'class' => 'backend\modules\h_payment\Module',
        ],
        'api' => [
            'class' => 'backend\modules\api\Module',
        ],
        'hotel-penalty' => [
            'class' => 'backend\modules\h_penalty\Module',
        ],
        'robot' => [
            'class' => 'modules\robot\frontend\Module',
        ],
        'notifications' => [
            'class' => 'backend\modules\notifications\Module',
        ],

    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@backend/views/theme'
                ],
            ],
        ],
        'request' => [
            'class' => 'common\components\LangRequest'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                /*[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],*/
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['admin'],
                    'logFile' => '@runtime/logs/admin.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['integra_error'],
                    'logFile' => '@runtime/logs/integra_error.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/http-request.log',
                    'categories' => ['yii\httpclient\*'],
                ],
				[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['user_corp_error'],
                    'logFile' => '@runtime/logs/user_corp_error.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['user_agency_error'],
                    'logFile' => '@runtime/logs/user_agency_error.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['user_hotel_error'],
                    'logFile' => '@runtime/logs/user_hotel_error.log',
                    'logVars' => [],
                ],
				[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['api_error'],
                    'logFile' => '@runtime/logs/api_error.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['create_ticket_error'],
                    'logFile' => '@runtime/logs/create_ticket_error.log',
                    'logVars' => [],
                ],
				[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['hotel_penalty_error'],
                    'logFile' => '@runtime/logs/hotel_penalty_error.log',
                    'logVars' => [],
                ],
				[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['register_book'],
                    'logFile' => '@runtime/logs/register_book.log',
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class'=>'common\components\LangUrlManager',
            'rules' => [
            ],
        ],
        'httpclient' => [
            'class' => 'yii\httpclient\Client',
        ],
        'slack' => [
            'class' => 'understeam\slack\Client',
        ],

    ],
    'params' => $params,
];
