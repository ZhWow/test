<?php

namespace backend\models;

use Yii;

class TFee extends \backend\models\base\TFee
{
    public static function getClassCabin()
    {
        return [
            'any' => Yii::t('app', 'Any'),
            'economy' => Yii::t('flt', 'Economy'),
            'business' => Yii::t('flt', 'Business'),
            'first' => Yii::t('flt', 'First'),
        ];
    }

    public static function getClassList()
    {
        $query = Yii::$app->db_manager->createCommand('SELECT DISTINCT class FROM t_fee')->queryColumn();
        $result = [];
        foreach ($query as $q) {
            $option = Yii::t('app', $q);
            $result[$q] = $option;
        }
        return $result;
    }

    public static function getType()
    {
        return [
            'all' => Yii::t('app', 'All'),
            'domestic' => Yii::t('flt', 'Domestic'),
            'international' => Yii::t('flt', 'International'),
        ];
    }

    public static function getPassengerType()
    {
        return [
            'all' => Yii::t('app', 'All'),
            'adt' => Yii::t('app', 'Adults'),
            'chd' => Yii::t('app', 'Children'),
            'inf' => Yii::t('app', 'Infant'),
        ];
    }

    public static function getSubClassEconomyList()
    {
        return [
            'Y' => 'Y',
            'S' => 'S',
            'W' => 'W',
            'B' => 'B',
            'K' => 'K',
            'L' => 'L',
            'M' => 'M',
            'V' => 'V',
            'U' => 'U',
            'H' => 'H',
            'T' => 'T',
            'E' => 'E',
            'Q' => 'Q',
            'O' => 'O',
            'N' => 'N',
            'G' => 'G',
            'R' => 'R'
        ];
    }
    public static function getSubClassBusinessList()
    {
        return [
            'J' => 'J',
            'C' => 'C',
            'Z' => 'Z',
            'I' => 'I',
            'D' => 'D'
        ];
    }
    public static function getSubClassFirstList()
    {
        return [
            'F' => 'F',
            'A' => 'A',
            'P' => 'P'
        ];
    }


}
