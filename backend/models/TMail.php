<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "t_mail".
 *
 * @property integer $id
 * @property string $reciepent
 * @property string $htmlText
 * @property string $title
 * @property string $attachment
 * @property integer $isAttachment
 * @property integer $isSent
 * @property string $placeDate
 * @property string $sentDate
 */
class TMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_mail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reciepent', 'htmlText', 'title'], 'required'],
            [['htmlText'], 'string'],
            [['isAttachment', 'isSent'], 'integer'],
            [['placeDate', 'sentDate'], 'safe'],
            [['reciepent'], 'string'],
            [['attachment'], 'string'],
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reciepent' => 'Reciepent',
            'htmlText' => 'Html Text',
            'title' => 'Title',
            'attachment' => 'Attachment',
            'isAttachment' => 'Is Attachment',
            'isSent' => 'Is Sent',
            'placeDate' => 'Place Date',
            'sentDate' => 'Sent Date',
        ];
    }
}
