<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "h_hotels".
 *
 * @property integer $id
 * @property string $countryIso
 * @property integer $cityID
 * @property integer $hotelID
 * @property string $hotelName
 * @property integer $stars
 * @property string $address
 * @property string $telephone
 * @property string $fax
 * @property string $email
 * @property string $latitude
 * @property string $longitude
 * @property string $lastUpdateDate
 * @property string $accommodationType
 * @property string $rating
 * @property string $description
 * @property string $link
 */
class Hotel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_hotels';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityID', 'hotelID', 'stars'], 'integer'],
            [['description'], 'string'],
            [['countryIso', 'hotelName', 'telephone', 'email', 'latitude', 'longitude', 'lastUpdateDate', 'accommodationType', 'rating'], 'string', 'max' => 200],
            [['address', 'fax'], 'string', 'max' => 300],
            [['link'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'countryIso' => 'Country Iso',
            'cityID' => 'City ID',
            'hotelID' => 'Hotel ID',
            'hotelName' => 'Hotel Name',
            'stars' => 'Stars',
            'address' => 'Address',
            'telephone' => 'Telephone',
            'fax' => 'Fax',
            'email' => 'Email',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'lastUpdateDate' => 'Last Update Date',
            'accommodationType' => 'Accommodation Type',
            'rating' => 'Rating',
            'description' => 'Description',
            'link' => 'Link',
        ];
    }
}
