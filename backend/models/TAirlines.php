<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "t_airlines".
 *
 * @property integer $id
 * @property string $code
 * @property string $airline
 * @property string $countrycode
 * @property string $provyder
 * @property string $active
 */
class TAirlines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_airlines';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'airline', 'countrycode', 'provyder', 'active'], 'required'],
            [['code', 'countrycode', 'active'], 'string', 'max' => 100],
            [['airline', 'provyder'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'airline' => 'Airline',
            'countrycode' => 'Countrycode',
            'provyder' => 'Provyder',
            'active' => 'Active',
        ];
    }
}
