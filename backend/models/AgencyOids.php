<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_agency_oids".
 *
 * @property integer $id
 * @property string $OID
 * @property string $AgencyName
 * @property string $AgencyType
 * @property integer $agencyID
 */
class AgencyOids extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_agency_oids';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agencyID'], 'integer'],
            [['OID', 'AgencyName', 'AgencyType'], 'string', 'max' => 9],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'OID' => 'Oid',
            'AgencyName' => 'Agency Name',
            'AgencyType' => 'Agency Type',
            'agencyID' => 'Agency ID',
        ];
    }
}
