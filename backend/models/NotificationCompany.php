<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "t_notification_company".
 *
 * @property integer $id
 * @property integer $corp_id
 * @property string $email
 * @property integer $onBook
 * @property integer $onApprove
 * @property integer $onApproveErr
 * @property integer $onAgentRefund
 * @property integer $onIssueTicket
 * @property integer $onRefund
 * @property integer $onCancel
 */
class NotificationCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_notification_company';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['corp_id', 'onBook', 'onApprove', 'onApproveErr', 'onAgentRefund', 'onIssueTicket', 'onRefund', 'onCancel'], 'integer'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'corp_id' => 'Corp ID',
            'email' => 'Email',
            'onBook' => 'On Book',
            'onApprove' => 'On Approve',
            'onApproveErr' => 'On Approve Err',
            'onAgentRefund' => 'On Agent Refund',
            'onIssueTicket' => 'On Issue Ticket',
            'onRefund' => 'On Refund',
            'onCancel' => 'On Cancel',
        ];
    }
}
