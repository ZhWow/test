<?php

namespace backend\models;

use Yii;
use backend\modules\integra\models\TUsers;

/**
 * This is the model class for table "t_corp_t_users".
 *
 * @property integer $t_corp_id
 * @property string $t_users_id
 *
 * @property TCorp $tCorp
 * @property TUsers $tUsers
 */
class TCorpTUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_corp_t_users';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_corp_id', 't_users_id'], 'required'],
            [['t_corp_id', 't_users_id'], 'integer'],
            [['t_corp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Corp::className(), 'targetAttribute' => ['t_corp_id' => 'id']],
            [['t_users_id'], 'exist', 'skipOnError' => true, 'targetClass' => TUsers::className(), 'targetAttribute' => ['t_users_id' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            't_corp_id' => 'T Corp ID',
            't_users_id' => 'T Users ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTCorp()
    {
        return $this->hasOne(Corp::className(), ['id' => 't_corp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTUsers()
    {
        return $this->hasOne(TUsers::className(), ['ID' => 't_users_id']);
    }

    public static function allowedCorporations()
    {
        $manager = Yii::$app->getAuthManager();

        if ( Yii::$app->user->can('agency') ) {
            $agency_id = [];
            $corp_allowed = [];

            foreach ($manager->getPermissions() as $permission) {
                if (Yii::$app->user->can($permission->name)) {
                    $t_user_result = TUsers::findOne(['user_login' => $permission->name]);
                    $agency_id[] = $t_user_result->ID;
                }
            }

            $corp_ids = self::find()->select(['t_corp_id'])->where(['IN','t_users_id',$agency_id])->all();

            foreach ($corp_ids as $corp_id) {
                $corp_allowed[] = $corp_id->t_corp_id;
            }

            $corps = Corp::find()->where(['IN', 'id', $corp_allowed])->asArray()->all();

        } else {
            $corps = Corp::find()->asArray()->all();
        }

        return $corps;
    }

    public static function allowedCorporationsObjects()
    {
        $manager = Yii::$app->getAuthManager();

        if ( Yii::$app->user->can('agency') ) {
            $agency_id = [];
            $corp_allowed = [];

            foreach ($manager->getPermissions() as $permission) {
                if (Yii::$app->user->can($permission->name)) {
                    $t_user_result = TUsers::findOne(['user_login' => $permission->name]);
                    $agency_id[] = $t_user_result->ID;
                }
            }

            $corp_ids = self::find()->select(['t_corp_id'])->where(['IN','t_users_id',$agency_id])->all();

            foreach ($corp_ids as $corp_id) {
                $corp_allowed[] = $corp_id->t_corp_id;
            }

            $corps = Corp::find()->where(['IN', 'id', $corp_allowed])->all();

        } else {
            $corps = Corp::find()->all();
        }

        return $corps;
    }
}
