<?php

namespace backend\models\base;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "t_fee".
 *
 * @property integer $id
 * @property integer $id_company
 * @property string $class
 * @property string $subclass
 * @property double $cash
 * @property double $proc
 * @property string $airline
 * @property string $flight
 * @property string $type
 * @property string $passenger
 * @property string $corp
 * @property string $airline_code
 */
class TFee extends \yii\db\ActiveRecord
{
    public $airline_name;
    public $subclass_economy;
    public $subclass_business;
    public $subclass_first;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_fee';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company'], 'integer'],
            [['cash', 'proc'], 'number'],
            //[['airline'], 'string'],
            [['class', 'flight', 'type', 'passenger', 'corp'], 'string', 'max' => 100],
            [['airline'], 'each', 'rule' => ['string']],
            [['airline_name'], 'each', 'rule' => ['string']],
            [['subclass'], 'each', 'rule' => ['string']],
            //[['subclass'], 'string', 'max' => 200],
            ['type', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_company' => 'Id Company',
            'class' => 'Class',
            'subclass' => 'Subclass',
            'cash' => 'Cash',
            'proc' => 'Proc',
            'airline' => 'Airline',
            'flight' => 'Flight',
            'type' => 'Type',
            'passenger' => 'Passenger',
            'corp' => 'Corp',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $airlines = Yii::$app->request->post('TFee')['airline'];
            $airlineValue = '';
            $i = 0;
            foreach ($airlines as $airline) {
                if ($airline) {
                    $separator = $i === 0 ? '' : '/';
                    $airlineValue .= $separator . $airline;
                } else {
                    continue;
                }
                $i++;
            }

            $subclass_economy = Yii::$app->request->post('TFee')['subclass_economy'] != '' ? Yii::$app->request->post('TFee')['subclass_economy'] : [];
            $subclass_business = Yii::$app->request->post('TFee')['subclass_business'] != '' ? Yii::$app->request->post('TFee')['subclass_business'] : [];
            $subclass_first = Yii::$app->request->post('TFee')['subclass_first'] != '' ? Yii::$app->request->post('TFee')['subclass_first'] : [];

            $subclasses = ArrayHelper::merge($subclass_economy, $subclass_business);
            $subclasses = ArrayHelper::merge($subclasses, $subclass_first);

            $subclassValue = '';
            $q = 0;
            foreach ($subclasses as $subclass) {
                $separator = $q === 0 ? '' : '/';
                $subclassValue .= $separator . $subclass;
                $q++;
            }

            $this->airline = $airlineValue;
            $this->subclass = $subclassValue;

            //$this->token = $this->token . '; path=/; HttpOnly';
            return true;
        } else {
            return false;
        }
    }
}
