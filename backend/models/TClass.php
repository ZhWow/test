<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "t_class_new".
 *
 * @property integer $id
 * @property string $airlineCode
 * @property string $ResBookDesigCode
 * @property string $classType
 * @property string $classTypeRu
 * @property string $classSubType
 */
class TClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_class_new';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['airlineCode'], 'string', 'max' => 2],
            [['ResBookDesigCode'], 'string', 'max' => 1],
            [['classType', 'classTypeRu', 'classSubType'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'airlineCode' => 'Airline Code',
            'ResBookDesigCode' => 'Res Book Desig Code',
            'classType' => 'Class Type',
            'classTypeRu' => 'Class Type Ru',
            'classSubType' => 'Class Sub Type',
        ];
    }
}
