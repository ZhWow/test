<?php

namespace backend\models;

use Yii;

class TBook
{
    public static function find($condition = false, $or = '')
    {
        $all = [];

        $corps = TCorpTUsers::allowedCorporations();

        $where = '';
        if (is_array($condition)) {
            $i = 0;
            $size = count($condition);
            foreach ($condition as $key => $val) {
                $where .= $i === 0 ? ' WHERE ' : ' AND ';

                if (is_array($val)) {
                    $ins = '';
                    $q = 0;
                    foreach ($val as $in) {
                        if ($q === 0) {
                            $ins .= $in;
                        }
                        $ins .= ', ' . $in;
                        $q++;

                    }
                    $where .= $key . ' IN (' . $ins . ')';
                } else {
                    $where .= $key . '=' . $val;

                }

                if ($size === $i) {
                    $where .= '';
                }


                $i++;
            }
        }

        foreach ($corps as $corp) {
            $query = 'SELECT * FROM t_book ' . $where . $or; // TODO where isContentIn
            $results = Yii::$app->{'db_' . $corp['base']}->createCommand($query)->queryAll();
            foreach ($results as $result) {
                $all[] = $result;
            }
        }

        return $all;
    }
}