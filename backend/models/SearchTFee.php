<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TFee;

/**
 * SearchTFee represents the model behind the search form about `backend\models\TFee`.
 */
class SearchTFee extends TFee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_company'], 'integer'],
            [['class', 'subclass', 'airline', 'flight', 'type', 'passenger', 'corp'], 'safe'],
            [['cash', 'proc'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TFee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company' => $this->id_company,
            'cash' => $this->cash,
            'proc' => $this->proc,
        ]);

        $query->andFilterWhere(['like', 'class', $this->class])
            ->andFilterWhere(['like', 'subclass', $this->subclass])
            ->andFilterWhere(['like', 'airline', $this->airline])
            ->andFilterWhere(['like', 'flight', $this->flight])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'passenger', $this->passenger])
            ->andFilterWhere(['like', 'corp', $this->corp]);

        return $dataProvider;
    }
}
