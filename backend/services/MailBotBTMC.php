<?php
/**
 * Created by PhpStorm.
 * User: karim
 * Date: 07.11.2017
 * Time: 10:59
 */

namespace backend\services;


use yii\web\View;

class MailBotBTMC extends View
{

    public static function getView($view, $data)
    {
        $self = new self();
        return $self->renderFile($view, ['data' => $data]);
    }
}