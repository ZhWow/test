<?php

namespace backend\services;

class Registry
{
    /**
     * @var mixed[]
     */
    protected $data = [];

    private static $instance;

    /**
     * Registry constructor.
     */
    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            $registry = new self();
            self::$instance = $registry;
        }
        return self::$instance;
    }

    /**
     * Добавляет значение в реестр
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Возвращает значение из реестра по ключу
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * Удаляет значение из реестра по ключу
     *
     * @param string $key
     * @return void
     */
    public function removeData($key)
    {
        if (array_key_exists($key, $this->data)) {
            unset($this->data[$key]);
        }
    }
}