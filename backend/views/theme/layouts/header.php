<?php

use common\widgets\WLang;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<style>
    .select-lang {
        width: 130px !important;
    }
</style>
<div id="overlay-loader">
    <div class="loader-main">
        <div></div>
    </div>
</div>
<header class="main-header">

    <?= Html::a('<span class="logo-mini">B</span><span class="logo-lg">BTM</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">



            <ul class="nav navbar-nav">

                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->can('agent')) : ?>

                    <?= $this->render('header/_qway-callme-notification', [
                        'directoryAsset' => $directoryAsset,
                    ]) ?>

                    <!-- Integra -->
                    <?= $this->render('header/_integra-notification', [
                        'directoryAsset' => $directoryAsset,
                    ]) ?>

                    <?= $this->render('header/_refund-notification', [
                        'directoryAsset' => $directoryAsset,
                    ]) ?>

                    <?= $this->render('header/_exchange-notification', [
                        'directoryAsset' => $directoryAsset,
                    ]) ?>
                <?php endif; ?>


                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--                        <i class="fa fa-language"></i>-->
                        <img src="/images/flags/gif/<?= \common\models\SysLang::getCurrent()->url ?>.gif" alt="">
                        <span class="label label-success"></span>
                    </a>
                    <?= WLang::widget();?>
                </li>

                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?=  Yii::$app->user->identity->email ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?=  Yii::$app->user->identity->email ?>
                                <small></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--<li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>

                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>



                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
