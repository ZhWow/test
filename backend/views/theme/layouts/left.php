<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=  Yii::$app->user->identity->email ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php

        $items = [];

        if (!Yii::$app->user->isGuest && Yii::$app->user->can('agent')) {

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('admin')) {
                $report = ['label' => 'Отчеты', 'url' => ['/agent/default/report']];
            } else {
                $report = [];
            }

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('admin')) {
                $adminCorpReport = ['label' => 'Отчеты Авиа корп.', 'url' => ['/agent/report-ticket/air']];
                $adminHotelReport = ['label' => 'Отчеты Отели корп.', 'url' => ['/agent/report-ticket/hotel']];
                $adminOfflineTicketsReport = ['label' => 'Отчеты Авиа Офф.', 'url' => ['/agent/report-ticket/offline-tickets']];
                $adminCorpConversionReport = ['label' => 'Отчеты Коверсия.', 'url' => ['/agent/report-ticket/corp-conversion']];
            } else {
                $adminCorpReport = [];
                $adminHotelReport = [];
                $adminOfflineTicketsReport = [];
                $adminCorpConversionReport = [];
            }

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('hotels')) {
                $settingHotels = ['label' => 'Управление отелями.', 'url' => ['/hotel_payment']];
            } else {
                $settingHotels = [];
            }

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('accountant')) {
                $accountant = ['label' => 'Accountant', 'icon' => 'bar-chart', 'url' => ['/accountant']];
            } else {
                $accountant = [];
            }

            $items = [
                //['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                ['label' => 'Переводы', 'icon' => 'arrows-h', 'url' => ['/translate']],
                /*['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],*/
                ['label' => 'Integra', 'icon' => 'download', 'url' => ['/integra']],

                [
                    'label' => 'Управление продажами',
                    'icon' => 'pie-chart',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Активные билеты', 'url' => ['/agent/default/active-tickets']],
                        ['label' => 'Активные брони', 'url' => ['/agent/default/active-book']],
                        ['label' => 'Заявки на обмен', 'url' => ['/agent/default/exchange']],
                        ['label' => 'Заявки на регистрацию', 'url' => ['/agent/default/registration']],
                        ['label' => 'Заявки на возврат', 'url' => ['/agent/default/refunds']],
                        ['label' => 'Внести бронь', 'url' => ['/agent/default/register-book']],
                        ['label' => 'Билеты Qway', 'url' => ['/agent/default/qway-active-tickets']],
                        $report,
                        $adminCorpReport,
                        $adminHotelReport,
                        $adminOfflineTicketsReport,
                        $adminCorpConversionReport,
                    ],
                ],

                [
                    'label' => 'Hotels',
                    'icon' => 'bed',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Подтвержденные', 'url' => ['/hotels/hotel/confirm']],
                        ['label' => 'Не подтвержденные', 'url' => ['/hotels']],
                        ['label' => 'Отмененые', 'url' => ['/hotels/hotel/to-canceled']],
                        ['label' => 'Подтверждение отеля', 'url' => ['/hotels/hotel/hotels-list']],
                        $settingHotels,
                    ],
                ],

                [
                    'label' => 'Сервисные сборы',
                    'icon' => 'money',
                    'url' => '#',
                    'items' => [
                        ['label' => 'ЖД и Гостиницы', 'url' => ['/service']],
                        ['label' => 'Авиа', 'url' => ['/service/service/fees']],
                    ],
                ],

                $accountant,
            ];

            if (!Yii::$app->user->isGuest && (Yii::$app->user->can('qway_agent')|| Yii::$app->user->can('admin'))) {
                $items[] = [
                    'label' => 'QWAY',
                    'icon' => 'quora',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Билеты', 'icon' => 'plane', 'url' => ['/agent/default/qway-active-tickets']],
                        ['label' => 'Брони', 'icon' => 'ticket', 'url' => ['/agent/default/qway-active-bookings']],
                        ['label' => 'Пассажиры', 'icon' => 'address-book', 'url' => ['/agent/default/qway-passengers']],
                        ['label' => 'Mobile Перезвонить', 'icon' => 'phone', 'url' => ['/agent/qway/call-me-requests']],
                        ['label' => 'Mobile Брони', 'icon' => 'phone', 'url' => ['/agent/qway/app-bookings']],
                    ],
                ];
            }



            if (!Yii::$app->user->isGuest && Yii::$app->user->can('admin')) {
                $items[] = [
                    'label' => 'API',
                    'icon' => 'code',
                    'url' => '#',
                    'items' => [
                        ['label' => 'User - Agency', 'icon' => 'building-o', 'url' => ['/api/sys-user-agency/index']],
                        ['label' => 'User - Corpo', 'icon' => 'building', 'url' => ['/api/sys-user-corp/index']],
                        ['label' => 'User - Hotel', 'icon' => 'bed', 'url' => ['/api/sys-user-hotel/index']],
                    ],
                ];
            }

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('corp_accountant_reports')) {
                $items[] = [
                    'label' => 'Отчеты',
                    'icon' => 'list',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Авиа', 'url' => ['/agent/corp']],
                        ['label' => 'ЖД', 'url' => ['/accountant/corp/rail-report']],
                        ['label' => 'Гостиницы', 'url' => ['/accountant/corp/hotel-report']],
                    ],
                ];
            }

            if (!Yii::$app->user->isGuest && Yii::$app->user->can('admin')) {
                $items[] = [
                    'label' => 'RBAC',
                    'icon' => 'cogs',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Users', 'icon' => 'group', 'url' => ['/admin-panel/user/index'],],
                        ['label' => 'Role', 'icon' => 'eye', 'url' => ['/admin-panel/auth/index'],],
                        ['label' => 'Create role', 'icon' => 'pencil', 'url' => ['/admin/role/create'],],
                        ['label' => 'Create permission', 'icon' => 'pencil', 'url' => ['/admin/permission/create'],],
                    ],
                ];
            }

        } else if (!Yii::$app->user->isGuest && Yii::$app->user->can('hotel')) {

            $tUser = \backend\modules\integra\models\TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
            $rooms = \backend\modules\hotels\models\HRooms::find()->where(['hotelID' => $tUser->hotelID])->all();

            $availability = [];
            if ($rooms) {
                $availability = ['label' => 'Тарифы', 'icon' => 'filter', 'url' => ['/hotels/availability']];
            }

            $items = [
                [
                    'label' => 'Заявки',
                    'icon' => 'list',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Подтвержденные', 'url' => ['/hotels/manager/confirm']],
                        ['label' => 'Не подтвержденные', 'url' => ['/hotels/manager']],
                        ['label' => 'Отмененые', 'url' => ['/hotels/manager/to-canceled']],
                    ],
                ],

                ['label' => 'Комнаты', 'icon' => 'bed', 'url' => ['/hotels/room']],
                $availability,
                ['label' => 'Отчет', 'icon' => 'bar-chart', 'url' => ['/hotels/manager/report']],
                ['label' => 'Редактировать', 'icon' => 'pencil-square-o', 'url' => ['/hotels/manager/edit']],
            ];
        } else if (!Yii::$app->user->isGuest && Yii::$app->user->can('corp_accountant_reports')) {
            $items[] = [
                        'label' => 'Отчеты',
                        'icon' => 'list',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Авиа', 'url' => ['/agent/corp']],
                            ['label' => 'ЖД', 'url' => ['/accountant/corp/rail-report']],
                            ['label' => 'Гостиницы', 'url' => ['/accountant/corp/hotel-report']],
                        ],
                    ];
        } else if (!Yii::$app->user->isGuest && Yii::$app->user->can('hotels')) {
            $items[] = [
                'label' => 'Orders',
                'icon' => 'vcard-o',
                'url' => '#',
                'items' => [
                    ['label' => 'Подтвержденные', 'url' => ['/hotels/hotel/confirm']],
                    ['label' => 'Не подтвержденные', 'url' => ['/hotels']],
                    ['label' => 'Отмененые', 'url' => ['/hotels/hotel/to-canceled']],
                ],
            ];

            $items[] = [
                'label' => 'Admin',
                'icon' => 'handshake-o',
                'url' => '#',
                'items' => [
                    ['label' => 'Подтверждение отеля', 'url' => ['/hotels/hotel/hotels-list']],
                    ['label' => 'Управление отелями.', 'url' => ['/hotel_payment']],
                    ['label' => 'Управление штрафами', 'url' => ['/hotel-penalty/penalty']],
                ],
            ];

            $items[] = [
                'label' => 'Fares',
                'icon' => 'bed',
                'url' => '/hotels/admin/rooms',
                'items' => [
                    ['label' => 'Номера', 'url' => ['/hotels/admin/rooms']],
                ],
            ];
        } else if (!Yii::$app->user->isGuest && Yii::$app->user->can('agency')) {
            $items = [
                ['label' => 'Integra', 'icon' => 'download', 'url' => ['/integra']],
                [
                    'label' => 'Управление продажами',
                    'icon' => 'pie-chart',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Активные билеты', 'url' => ['/agent/default/active-tickets']],
                        ['label' => 'Активные брони', 'url' => ['/agent/default/active-book']],
                        ['label' => 'Заявки на обмен', 'url' => ['/agent/default/exchange']],
                        ['label' => 'Заявки на возврат', 'url' => ['/agent/default/refunds']],
                    ],
                ],
                [
                    'label' => 'Сервисные сборы',
                    'icon' => 'money',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Авиа', 'url' => ['/service/service/fees']],
                    ],
                ],
            ];
        } else if (!Yii::$app->user->isGuest && Yii::$app->user->can('accountant')) {
            $items = [$accountant = ['label' => 'PSP Payments', 'icon' => 'bar-chart', 'url' => ['/accountant']]];
        }






        ?>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $items,
            ]
        ) ?>

    </section>

</aside>
