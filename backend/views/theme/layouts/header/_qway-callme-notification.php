<?php

$CallMe = \backend\modules\agent\models\AppCallme::find()->where(['status' => 0])->all();

?>

<style>
    .qway-callme-notification-item h4 {
        margin: 0 !important;
    }

    .qway-callme-notification-item p {
        margin: 0 !important;
    }
</style>

<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-phone"></i>
        <span class="label label-warning"><?= count($CallMe) ?></span>
    </a>
    <ul class="dropdown-menu">

        <li class="header"><?= Yii::t('flt', 'Call Me') . ' ' . count($CallMe) . ' requests' ?></li>


        <li>
            <ul class="menu">
                <?php foreach ($CallMe as $item) : ?>
                    <li class="qway-callme-notification-item">
                        <a href="#" >
                            <h4>
                                <?= $item->phone ?>
                                <small><?= $item->email ?></small>
                            </h4>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>



        <li class="footer"><a href="/agent/qway/call-me-requests">See All Call Me Requests</a></li>
    </ul>
</li>

