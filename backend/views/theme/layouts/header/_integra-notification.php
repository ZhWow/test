<?php

use backend\modules\integra\models\AmadeusIntegra;

$OID = \backend\modules\integra\models\SearchAmadeusIntegra::accessForFind();
$integras = AmadeusIntegra::find()->where(['CONFLICT' => 1, 'FIRST_OWNER_OFFICEID' => $OID])->all();

?>

<style>
    /*.integra-notification-item {
        text-decoration: none;
    }*/
    .integra-notification-item h4 {
        margin: 0 !important;
    }

    .integra-notification-item p {
        margin: 0 !important;;
    }
</style>

<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-download"></i>
        <span class="label label-danger"><?= count($integras) ?></span>
    </a>
    <ul class="dropdown-menu">

        <li class="header"><?= 'Integra ' . count($integras) . ' conflict'?></li>


        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php foreach ($integras as $integra) : ?>
                    <li class="integra-notification-item">
                        <a href="#" >
                            <h4>
                                <?= $integra->DOCUMENT_NUMBER ?>
                                <small><?= $integra->FO_NUMBER ?  'EXCHANGE' : $integra->DOCUMENT_ACTION ?></small>
                            </h4>
                            <p><?= $integra->ERR_MESSAGE ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>

            </ul>
        </li>



        <li class="footer"><a href="/integra">See All Conflict</a></li>
    </ul>
</li>