<?php

$items = \backend\modules\integra\models\TElectronicTickets::find(['to_cancel' => [2, 3]]);

?>
<style>
    /*.integra-notification-item {
        text-decoration: none;
    }*/
    .refund-notification-item h4 {
        margin: 0 !important;
    }

    .refund-notification-item p {
        margin: 0 !important;
    }
</style>
<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-reply"></i>
        <span class="label label-warning"><?= count($items) ?></span>
    </a>
    <ul class="dropdown-menu">

        <li class="header"><?= Yii::t('flt', 'Refund') . ' ' . count($items) ?></li>


        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php foreach ($items as $item) : ?>
                    <li class="refund-notification-item">
                        <a href="#" >
                            <h4>
                                <?= $item['eTicketNumber'] ?>
                                <small><?= $item['to_cancel'] == 2 ? Yii::t('flt', 'Refund') : Yii::t('flt', 'Must refund') ?></small>
                            </h4>
                            <p><?= $item['GivenName'] . ' ' . $item['Surname'] ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>

            </ul>
        </li>



        <li class="footer"><a href="/agent/default/refunds">See All Exchange</a></li>
    </ul>
</li>
