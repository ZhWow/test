<?php

$items = \backend\modules\integra\models\TElectronicTickets::find(['to_cancel' => [10, 12]]);

?>
<style>
    /*.integra-notification-item {
        text-decoration: none;
    }*/
    .exchange-notification-item h4 {
        margin: 0 !important;
    }

    .exchange-notification-item p {
        margin: 0 !important;
    }
</style>
<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-undo"></i>
        <span class="label label-warning"><?= count($items) ?></span>
    </a>
    <ul class="dropdown-menu">

        <li class="header"><?= Yii::t('flt', 'Exchange') . ' ' . count($items) ?></li>


        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php foreach ($items as $item) : ?>
                    <li class="exchange-notification-item">
                        <a href="#" >
                            <h4>
                                <?= $item['eTicketNumber'] ?>
                                <small><?= $item['to_cancel'] == 10 ? Yii::t('flt', 'Exchange') : Yii::t('flt', 'Must exchange') ?></small>
                            </h4>
                            <p><?= $item['GivenName'] . ' ' . $item['Surname'] ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>

            </ul>
        </li>



        <li class="footer"><a href="/agent/default/exchange">See All Refunds</a></li>
    </ul>
</li>
