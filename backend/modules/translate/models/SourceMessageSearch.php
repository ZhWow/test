<?php

namespace backend\modules\translate\models;

/*use common\modules\translate\models\Message;
use common\modules\translate\models\SysLang;*/
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use common\modules\translate\models\SourceMessage;
use common\modules\translate\models\Message;
use common\modules\translate\models\SourceMessage;
use common\models\SysLang;


/**
 * Class SourceMessageSearch
 * @package backend\modules\translate\models
 *
 * @property string $language Come from the search form, one of the system languages
 * @property int $translation_status Come from the search form, It has two states, 0 or 1
 * @property string $translation Already translated words
 *
 * @property array $cyrillicId
 */
class SourceMessageSearch extends SourceMessage
{
    public $language;
    public $translation_status;
    public $id_from;
    public $id_to;
    public $translation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'translation_status', 'id_from', 'id_to'], 'integer'],
            [['category', 'message', 'language', 'translation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $cyrillic When true SourceMessages displays a list from which the field in the database has a Cyrillic message
     *
     * @return ActiveDataProvider
     */
    public function search($params, $cyrillic = false)
    {
        $query = SourceMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        if ($cyrillic) {
            $query->andWhere(['in', 'id', $this->getCyrillicId()]);
        } else {
            $query->andWhere(['not in', 'id', $this->getCyrillicId()]);
        }

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['>=', 'id', $this->id_from])
            ->andFilterWhere(['<=', 'id', $this->id_to])
            ->andFilterWhere(['in', 'id', $this->langMessage($this->language)])
            ->andWhere(['in', 'id', $this->similarMessage()])
            ->orderBy('id DESC');

        return $dataProvider;
    }

    /**
     * @deprecated new version this method - translationStatus()
     */
    public function translationType()
    {
        return $this->translationStatus();
    }

    /**
     * Returns all languages ​​in the system, format [ en-US => English ]. Method for drop down search
     * @return array
     */
    public function languages()
    {
        $allSysLang = self::getAllSysLang();
        $languages = [];
        foreach ($allSysLang as $sysLang) {
            /* @var $sysLang SysLang */
            $languages[$sysLang->local] = $sysLang->name;
        }
        return $languages;
    }

    /**
     * If translation_status not zero, returns an array of ID not translated to language that came.
     * If translation_status A higher unit is translated. Translation_status value attributable to a filter in the form of
     * @param string $lang This property language, came from the mold
     * @return array
     */
    public function langMessage($lang)
    {
        $ids = [];
        /* @var $messages Message[] */
        $messages = Message::find()->where(['language' => $lang])->all();
        if ($this->translation_status == 0) {
            foreach ($messages as $item) {
                if (empty(trim($item->translation))) {
                    $ids[] = $item->id;
                }
            }
        } else {
            foreach ($messages as $item) {
                if (!empty(trim($item->translation))) {
                    $ids[] = $item->id;
                }
            }
        }
        return $ids; // my_todo нужно проверить на уникальности значении
    }

    /**
     * Returns an array of ID SourceMessages in which the field in the database has a Cyrillic message
     * @return array
     */
    public function getCyrillicId()
    {
        $ids = [];
        $source_message = SourceMessage::find()->all();

        foreach ($source_message as $item) {
            /* @var $item SourceMessage */
            if (SourceMessage::checkToCyrillic($item)) {
                $ids[] = $item->id;
            }
        }
        return $ids;
    }

    /**
     * Returns an array of ID Source Messages where there was a translation
     * @return array
     */
    public function similarMessage(){
        $id = [];
        $query = Message::find();
        if (!empty($this->translation)) {
            $query->where(['like', 'translation', $this->translation]);
        }
        /* @var $messages Message[] */
        $messages = $query->all();
        foreach ($messages as $m) {
            $id[] = $m->id;
        }
        return $id;
    }

    public static function translationStatus()
    {
        return [
            0 => Yii::t('trn', 'not translated'),
            1 => Yii::t('trn', 'translated'),
        ];
    }
}
