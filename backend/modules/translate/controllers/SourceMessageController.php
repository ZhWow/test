<?php

namespace backend\modules\translate\controllers;

use backend\modules\translate\models\archive\ArchTranslateData;
use Yii;
use common\modules\translate\models\SourceMessage;
use backend\modules\translate\models\SourceMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\translate\services\WriteToJsService;
use yii\web\UploadedFile;
use yii\base\Exception;
use backend\modules\translate\services\ArchiveService as Archive;
use backend\modules\translate\services\LogInfoMessageService as LogInfo;

/**
 * SourceMessageController implements the CRUD actions for SourceMessage model.
 */
class SourceMessageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SourceMessage models.
     * @param $cyrillic
     * @return mixed
     */
    public function actionIndex($cyrillic = false)
    {
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $cyrillic);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SourceMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->existingTranslate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->saveTranslates()) {
                $jsService = new WriteToJsService();
                $jsService->execute();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('trn', 'language translates not saved'));
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays translation statistics
     * @return string
     */
    public function actionStat()
    {
        return $this->render('stat');
    }

    /**
     * Fills translations from excel document. If replace true will be replaced
     * @param bool $replace
     * @return \yii\web\Response
     */
    public function actionLoadExcel($replace = false)
    {
        $model = new SourceMessage();
        $model->excelFile = UploadedFile::getInstance($model, 'excelFile');
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $statusOk = false;
            if (Archive::createTranslate()) {
                LogInfo::startLoadExcelDoc($replace);
                if ($model->saveFromExcel($replace, $model->excelFile->tempName)) {
                    LogInfo::successLoadExcelDoc($model->new_msg, $model->empty_msg, $model->rpl_msg);
                    $statusOk = true;
                } else {
                    LogInfo::errorLoadExcelDoc();
                    Yii::error('language translates not saved', 'translate');
                    Yii::$app->getSession()->setFlash('error', Yii::t('trn', 'language translates not saved'));
                }
            } else {
                Yii::error('Can not create archives', 'translate');
                Yii::$app->getSession()->setFlash('error', Yii::t('trn', 'Can not create archives'));
            }

            if ($statusOk) {
                $transaction->commit();
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'translate');
            $transaction->rollBack();
        }
        $jsService = new WriteToJsService();
        $jsService->execute();
        return $this->redirect(['index']);
    }

    /**
     * Multiple removal SourceMessage
     * @return \yii\web\Response
     */
    public function actionDeleteMultiple()
    {
        $keys = Yii::$app->request->post('keys');
        foreach ($keys as $key) {
            $this->findModel($key)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * He returned to the state last saved in the archive
     * @return \yii\web\Response
     */
//    public function actionRevert()
//    {
//        if (ArchTranslateData::getLastVersion() === null) {
//            Yii::$app->session->setFlash('error', Yii::t('trn', 'Not have backup copies'));
//            return $this->redirect('index');
//        }
//        $transaction = Yii::$app->db->beginTransaction();
//        try {
//            $success = false;
//            SourceMessage::deleteAll();
//            LogInfo::revertStart();
//            if (SourceMessage::revertModels()) {
//                $success = true;
//            } else {
//                Yii::$app->session->setFlash('error', Yii::t('trn', 'Can not be reverted'));
//                LogInfo::notBeReverted();
//            }
//            if ($success) {
//                $transaction->commit();
//            }
//        } catch (Exception $e) {
//            Yii::error($e->getMessage(), 'translate');
//            Yii::$app->session->setFlash('error', 'has not reverted');
//            $transaction->rollBack();
//        }
//        return $this->redirect('index');
//    }

    // TODO
    public function actionFixMessages()
    {
        if (SourceMessage::fixMessages()) {
            Yii::$app->session->setFlash('success', 'yes');
        } else {
            Yii::$app->session->setFlash('error', 'no');
        }
        return $this->redirect('index');
    }
}
