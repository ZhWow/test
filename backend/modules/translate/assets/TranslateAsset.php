<?php

namespace backend\modules\translate\assets;

use yii\web\AssetBundle;

class TranslateAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/translate/assets';
    public $css = [
        'css/translate.css'
    ];
    public $js = [
        'js/translate.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}