(function() {

    $('#sys-source-message-index_filter_toggle').on('click', function () {
        var filterPanel = $('#filter-panel');
        filterPanel.is(':visible') ? filterPanel.hide() : filterPanel.show();
    });

    $('#load-type').on('change', function () {
        $('#upload-excel').attr('action', $(this).val());
    });

    $('#checkbox-button-deleted').on('click', function () {
        var keys = $('.delete-item-checked').toArray().map(function (item) {
            if ($(item).is(':checked')) {
                return $(item).val();
            }
        });
        keys = keys.filter(function(item) { return item; });

        if (keys.length > 0) {
            $.ajax({
                cache: false,
                type: 'POST',
                url: '/translate/source-message/delete-multiple',
                data: {'keys': keys}
            });
        }
    });

}());