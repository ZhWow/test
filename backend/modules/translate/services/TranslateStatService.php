<?php

namespace backend\modules\translate\services;

use common\modules\translate\models\Message;
use common\modules\translate\models\SourceMessage;

class TranslateStatService
{
    /**
     * Returns the number of Source Messages which do not have
     * translations in a particular language
     * @param string $lang
     * @return int
     */
    public static function getNotTranslatedMessagesNum($lang)
    {
        $i = 0;
        $messages = Message::find()->where(['language' => $lang])->all();
        /* @var Message $message */
        foreach ($messages as $message) {
            if (!SourceMessage::checkToCyrillic($message->sourceMessage)) {
                if (empty(trim($message->translation))) {
                    $i++;
                }
            }
        }
        return $i;
    }

    /**
     * Number Source Messages whose message to the field contains Cyrillic
     * @return int
     */
    public static function getCyrillicCountSize()
    {
        $i = 0;
        $sourceMessages = SourceMessage::find()->all();
        foreach ($sourceMessages as $item) {
            /* @var $item SourceMessage */
            if (SourceMessage::checkToCyrillic($item)) {
                $i++;
            }
        }
        return $i;
    }
}

?>