<?php

namespace backend\modules\translate\services;

use Yii;

class LogInfoMessageService
{
    private static $translate = 'translate_info';

    public static function startLoadExcelDoc($replace)
    {
        Yii::info("\n" . '>>>>>>>>>>>>>>>loading from document ' . ($replace ? 'with REPLACE' : '') . ' is started', self::$translate);
    }

    public static function successLoadExcelDoc($newMsg, $emptyMsg, $rplMsg)
    {
        Yii::info("\n" . '<<<<<<<<<<<<<<<<<loading has come to the end successfully' . "\n"
            . 'new - ' . $newMsg . ', fill - ' . $emptyMsg . ', replace - ' . $rplMsg, self::$translate);
    }

    public static function errorLoadExcelDoc()
    {
        Yii::info(['<<<<<<<<<<<<<<<loading ended with error'], self::$translate);
    }

    public static function revertStart()
    {
        Yii::info("\n" . '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< REVERT START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', self::$translate);
    }

    public static function notBeReverted()
    {
        Yii::info("\n" . '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< REVERT ENDED WITH ERROR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', self::$translate);
    }

    public static function revertedVersion($lastVersion, $createdAt)
    {
        Yii::info("\n" . '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Reverted to version ' . $lastVersion . ' user - '
            . (Yii::$app->user->identity)->username . ' date - '
            . Yii::$app->formatter->asDatetime($createdAt, 'yyyy-MM-dd HH:mm') . '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',
            self::$translate);
    }
}