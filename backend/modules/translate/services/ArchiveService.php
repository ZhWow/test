<?php

namespace backend\modules\translate\services;

use Yii;
use backend\modules\translate\models\archive\ArchTranslateData;
use backend\modules\translate\models\archive\ArchSourceMessage;
use backend\modules\translate\models\archive\ArchMessage;
use common\modules\translate\models\SourceMessage;
use common\modules\translate\models\Message;

class ArchiveService
{
    public static function createTranslate()
    {
        $version = ArchTranslateData::find()->max('version') + 1;

        $archSourceMessage = self::createArchSourceMessage($version);
        $archMessage = self::createArchMessage($version);
        $translateData = self::createTranslateData($version);

        if ($archSourceMessage && $archMessage && $translateData) {
            return true;
        }
        return false;
    }

    private static function createTranslateData($version)
    {
        /* @var ArchTranslateData $archTranslateData */
        $archTranslateData = new ArchTranslateData();
        $archTranslateData->version = $version;
        $archTranslateData->created_by = Yii::$app->user->id;
        return $archTranslateData->save();
    }

    private static function createArchSourceMessage($version)
    {
        $success = true;
        /* @var SourceMessage $sourceMessage */
        foreach (SourceMessage::find()->each(10) as $sourceMessage) {
            /* @var ArchSourceMessage $archSourceMessage */
            $archSourceMessage = new ArchSourceMessage();
            if ($archSourceMessage->load(['ArchSourceMessage' => $sourceMessage->attributes])) {
                $archSourceMessage->version = $version;
                if (!$archSourceMessage->save()) {
                    Yii::error('Source Message archive not save' . $archSourceMessage->getErrors(), 'translate');
                    $success = false;
                }
            } else {
                Yii::error('Source Message archive not load' . $archSourceMessage->getErrors(), 'translate');
                $success = false;
            }
        }

        return $success;
    }

    private static function createArchMessage($version)
    {
        $success = true;
        /* @var Message $message */
        foreach (Message::find()->each(10) as $message) {
            /* @var ArchMessage $archMessage */
            $archMessage = new ArchMessage();
            if ($archMessage->load(['ArchMessage' => $message->attributes])) {
                $archMessage->version = $version;
                if (!$archMessage->save()) {
                    Yii::error($archMessage->getErrors(), 'translate');
                    Yii::error($archMessage->id, 'translate');
                    $success = false;
                }
            } else {
                Yii::error('Message archive not load' . $archMessage->getErrors(), 'translate');
                $success = false;
            }
        }

        return $success;
    }
}