<?php

namespace backend\modules\translate\services;

use yii\helpers\Html;
use common\modules\translate\models\SysLang;
use common\modules\translate\models\SourceMessage;

/**
 * Class ViewHelpService
 * @package backend\modules\translate\services
 */
class ViewHelpService
{
    // background color constants
    const ALL_COLOR = ['red', 'orange', 'yellow', 'blue', 'dark']; // need add color

    public static function getColumnsIndexView()
    {

        $columnsArray = [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model) {
                    return [
                        'class' => 'delete-item-checked',
                        'value' => $model->id
                    ];
                },
            ],
            [
                'attribute' => 'id',
                'value' => 'id',
                'contentOptions' => function () {
                    return self::setCol(1);
                }
            ],
            [
                'attribute' => 'category',
                'value' => 'category',
                'contentOptions' => function () {
                    return self::setCol(1);
                }
            ],
            [
                'attribute' => 'message',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model SourceMessage */
                    return Html::a($model->message, ['update', 'id' => $model->id]);
                },
                'contentOptions' => function ($model) {
                    /* @var $model SourceMessage */
                    return SourceMessage::checkToCyrillic($model) ? self::getBackgroundColor(0) : [];
                }
            ],
        ]; // end $columnsArray

        $sysLangAll = SourceMessage::getAllSysLang(true);

        $i = 0;
        foreach ($sysLangAll as $sysLang) {
            /* @var $sysLang SysLang */
            $localTranslate = $sysLang->local;
            $column = [
                'attribute' => $sysLang->url,
                'value' => function ($model) use ($localTranslate) {
                    /* @var $model SourceMessage */
                    return $model->getTranslations($localTranslate) ? $model->getTranslations($localTranslate) : '';
                },
                'contentOptions' => function ($model) use ($localTranslate, $i) {
                    /* @var $model SourceMessage */
                    $styleAttr = empty($model->getTranslations($localTranslate)) ? self::getBackgroundColor($i) : [];
                    $classAttr = self::setCol(2);
                    return array_merge($styleAttr, $classAttr);
                }
            ];
            array_push($columnsArray, $column);
            $i++;
        }
        return $columnsArray;
    }

    public static function getStatNotLangTranslate()
    {
        $sysLangAll = SysLang::findBySql('
            SELECT `local`, `name`
            FROM `sys_lang`')
            ->all();

        $resultText = '';

        foreach ($sysLangAll as $sysLang) {
            /* @var $sysLang SysLang */
            $resultText .=
                '<tr>
                    <td>Не переведенные на ' . $sysLang->name . ':</td>
                    <td>' . TranslateStatService::getNotTranslatedMessagesNum($sysLang->local) . '</td>
                </tr>';
        }

        return $resultText;
    }

    public static function getColumnsExportMenu()
    {
        $columnsArray = [
            'id',
            'category:html',
            'message:html',
        ];

        $sysLangAll = SourceMessage::getAllSysLang();
        foreach ($sysLangAll as $sysLang) {
            $localTranslate = $sysLang->local;
            $column = [
                'format' => 'html',
                'label' => $sysLang->url,
                'value' => function ($model) use ($localTranslate) {
                    /* @var $model SourceMessage */
                    return $model->getTranslations($localTranslate);
                }
            ];
            array_push($columnsArray, $column);
        }

        return $columnsArray;
    }

    public static function showFilter($param, $translate)
    {
        if (!empty(trim($param))) {
            return $translate . ':' . $param . '&nbsp';
        }
        return '';
    }

    public static function showArrayFilter($param, $translate, $translateStatus)
    {
        if (array_key_exists($param, $translateStatus)) {
            return $translate . ':' . $translateStatus[$param] . '&nbsp';
        }
        return '';
    }

    private static function getBackgroundColor($i)
    {
        $maxSizeColor = count(self::ALL_COLOR);
        $pos = $maxSizeColor <= $i ? rand(0, $maxSizeColor - 1) : $i;
        return ['style' => 'background-color:' . self::ALL_COLOR[$pos] . ';'];
    }

    private static function setCol($size)
    {
        return ['class' => ' col-md-' . $size . ' '];
    }
}