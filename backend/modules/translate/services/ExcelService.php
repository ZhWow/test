<?php

namespace backend\modules\translate\services;

use Yii;
use common\modules\translate\models\SourceMessage;
use common\modules\translate\models\Message;
use yii\base\Exception;

trait ExcelService
{
    public $rpl_msg = 0;
    public $new_msg = 0;
    public $empty_msg = 0;

    /**
     * @param $replace boolean
     * @param $file resource
     * @return boolean
     */
    public function saveFromExcel($replace = false, $file)
    {
        $fill_msg = 0;
        $new_msg = 0;
        $rpl_msg = 0;
        $_params = Yii::$app->controller->module->params['excel_columns'];
        $objPHPExcel = \PHPExcel_IOFactory::load($file);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $_col_msg_category = $_params['message_category'];
        foreach ($sheetData as $item) {
            if (trim($item[$_params['message_id']]) == 'ID' || trim($item[$_params['message_id']]) == 'Номер') {
                continue;
            }
            $category = trim($item[$_col_msg_category]) == '' ? self::DEFAULT_CATEGORY
                : $item[$_col_msg_category];
            $message = $item[$_params['message']];
            if (trim(empty($message)) || !preg_match('/[a-zA-Z]/i', $message) || !preg_match('/[a-zA-Z]/i', $category)) {
                continue;
            }
            if (empty(trim($item[$_params['message_kz']]))
                && empty(trim($item[$_params['message_ru']]))
                && empty(trim($item[$_params['message_en']]))
            ) {
                continue;
            }
            $lang_messages = [
                $_params['lang_kz'] => $item[$_params['message_kz']],
                $_params['lang_ru'] => $item[$_params['message_ru']],
                $_params['lang_en'] => $item[$_params['message_en']],
            ];
            /* @var SourceMessage $src_msg */
            $src_msg = SourceMessage::find()->where(['like binary', 'message', $message])
                ->andWhere(['like binary', 'category', $category])->one();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $success = true;
                if ($src_msg !== null) {
                    if ($replace) {
                        $result_fill = $src_msg->saveTranslateFromExcel($lang_messages);
                        $new_msg += $result_fill[0];
                        $fill_msg += $result_fill[1];
                        $rpl_msg += $result_fill[2];
                        if (($result_fill[0] + $result_fill[1] + $result_fill[2] + $result_fill[3]) != self::langCount()) {
                            self::writeErrors($message, $category, $src_msg->id);
                            $success = false;
                        }
                    } else {
                        $result_empty = $src_msg->fillEmptyTranslations($lang_messages);
                        $new_msg += $result_empty[0];
                        $fill_msg += $result_empty[1];
                        if (($result_empty[0] + $result_empty[1] + $result_empty[2]) != self::langCount()) {
                            self::writeErrors($message, $category, $src_msg->id);
                            $success = false;
                        }
                    }
                } else {
                    $src_msg = new SourceMessage();
                    $src_msg->category = $category;
                    $src_msg->message = $message;
                    if ((int)$item[$_params['message_id']] != 0) {
                        if (($any_src_msg = SourceMessage::findOne((int)$item[$_params['message_id']])) === null) {
                            $src_msg->id = (int)$item[$_params['message_id']];
                        }
                    }
                    if ($src_msg->save()) {
                        $result_new = $src_msg->newMsgTranslates($src_msg->id, $lang_messages);
                        $new_msg += $result_new;
                        if ($result_new != self::langCount()) {
                            self::writeErrors($message, $category, $src_msg->id);
                            $success = false;
                        }
                    } else {
                        self::writeErrors($message, $category);
                        $success = false;
                    }
                }
                if ($success) {
                    $transaction->commit();
                } else {
                    return false;
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
        Yii::$app->getSession()->setFlash('success',
            'Создано: ' . $new_msg . '<br>Заменено: ' . $rpl_msg . '<br>Найдено пустых: ' . $fill_msg);
        return true;
    }

    /**
     * Replace translations
     * @param array $lang_messages
     * @return array
     */
    public function saveTranslateFromExcel(array $lang_messages)
    {
        $_replaced = 0;
        $_new = 0;
        $_filled = 0;
        $_missed = 0;
        foreach ($lang_messages as $lang => $translation) {
            if ($this->isEmpty($translation)) {
                $translation = null;
            } else {
                $translation = html_entity_decode($translation, ENT_QUOTES);
            }
            /* @var Message $msg */
            if (($msg = $this->findMessageOne($lang)) !== null) {
                $translation = trim($translation);
                $msg->translation = trim($msg->translation);
                if (!$this->isEmpty($translation)) {
                    if ($this->isEmpty($msg->translation)) {
                        $count_type = self::FILL_EMPTY;
                    } else {
                        if ($msg->translation == $translation) {
                            $count_type = self::SKIP;
                        } else {
                            $count_type = self::REPLACE;
                        }
                    }
                    $msg->translation = trim($translation);
                    if ($msg->save()) {
                        if ($count_type == self::SKIP) {
                            $_missed++;
                        } else {
                            $count_type == self::FILL_EMPTY ? $_filled++ : $_replaced++;
                        }
                    }
                } else {
                    $_missed++;
                }
            } else {
                if ($this->createOneMessage($lang, $translation)) {
                    $_new++;
                }
            }
        }
        return [$_new, $_filled, $_replaced, $_missed];
    }

    protected static function writeErrors($message, $category, $id = null)
    {
        Yii::error([
            'id' => $id,
            'message' => $message,
            'category' => $category
        ], 'translate');
    }

    /**
     * Fills Message model if not have translation.
     * @param array $lang_messages
     * @return array
     */
    public function fillEmptyTranslations(array $lang_messages)
    {
        $_new = 0;
        $_fill = 0;
        $_missed = 0;
        foreach ($lang_messages as $lang => $translation) {
            /* @var Message $msg */
            $msg = $this->findMessageOne($lang);
            if ($msg !== null) {
                if (!$this->isEmpty($translation)
                    && $this->isEmpty($msg->translation)
                ) {
                    $msg->translation = html_entity_decode($translation, ENT_QUOTES);
                    if ($msg->save()) {
                        $_fill++;
                    }
                } else {
                    $_missed++;
                }
            } else {
                if ($this->createOneMessage($lang, $translation)) {
                    $_new++;
                }
            }
        }
        return [$_new, $_fill, $_missed];
    }

    /**
     * Creates a new Message model.
     * @param integer $id
     * @param array $lang_messages
     * @return integer
     */
    public function newMsgTranslates($id, array $lang_messages)
    {
        $_new = 0;
        foreach ($lang_messages as $lang => $translation) {
            $msg = new Message();
            $msg->id = $id;
            $msg->language = $lang;
            if ($this->isEmpty($translation)) {
                $msg->translation = null;
            } else {
                $msg->translation = html_entity_decode($translation, ENT_QUOTES);
            }
            if ($msg->save()) {
                $_new++;
            }
        }
        return $_new;
    }

    /**
     * @param $translation string
     * @return boolean
     */
    public function isEmpty($translation)
    {
        $translation = trim($translation);
        $empty_values = ['', '(not set)', '(не задано)'];
        if (in_array($translation, $empty_values)) {
            return true;
        }
        return false;
    }

    /**
     * @param $language string
     * @param $translation string
     * @return boolean
     */
    public function createOneMessage($language, $translation)
    {
        $message = new Message();
        $message->id = $this->id;
        $message->language = $language;
        $message->translation = $translation;
        return $message->save();
    }

    private function findMessageOne($lang)
    {
        return Message::find()->where(['id' => $this->id, 'language' => $lang])->one();
    }
}