<?php

namespace backend\modules\translate\services;

use Yii;
use backend\modules\translate\models\archive\ArchTranslateData;
use common\modules\translate\models\Message;
use common\modules\translate\models\SourceMessage;
use backend\modules\translate\models\archive\ArchSourceMessage;
use backend\modules\translate\models\archive\ArchMessage;
use common\models\User;
use backend\modules\translate\services\LogInfoMessageService as LogInfo;

trait RevertTranslateService
{
    public static function revertModels()
    {
        $lastVersion = ArchTranslateData::getLastVersion();

        $revertSourceMessage = self::revertSourceMessage($lastVersion);
        $revertMessage = self::revertMessage($lastVersion);
        $revertTranslateData = self::revertTranslationData($lastVersion);

        if ($revertSourceMessage && $revertMessage && $revertTranslateData) {
            return true;
        }
        return false;
    }

    private static function revertTranslationData($lastVersion)
    {
        /* @var ArchTranslateData $archTranslateData */
        $archTranslateData = ArchTranslateData::find()->where(['version' => $lastVersion])->andWhere(['reverted' => 0])->one();
        $archTranslateData->reverted = 1;
        if ($archTranslateData->save()) {
            LogInfo::revertedVersion($lastVersion, $archTranslateData->created_at);
            $crUser = User::findOne($archTranslateData->created_by);
            $crUsername = ($crUser !== null ? $crUser->username : 'User not found');
            $setFlashMsg = 'Reverted to version ' . $lastVersion . ' user - ' . $crUsername . ' date - ' . $archTranslateData->created_at;
            Yii::$app->session->setFlash('success', $setFlashMsg);
        }
        return $archTranslateData->save();
    }

    private static function revertSourceMessage($lastVersion)
    {
        $success = true;
        foreach (ArchSourceMessage::find()->where(['version' => $lastVersion])->each(10) as $archSourceMessage) {
            $sourceMessage = new SourceMessage(['scenario' => self::SCENARIO_REVERT]);
            if ($sourceMessage->load(['SourceMessage' => $archSourceMessage->attributes])) {
                if (!$sourceMessage->save(false)) {
                    Yii::error($sourceMessage->getErrors(), 'translate');
                    $success = false;
                }
            } else {
                Yii::error('ArchSourceMessage not load revert', 'translate');
                $success = false;
            }
        }
        return $success;
    }

    private static function revertMessage($lastVersion)
    {
        $success = true;
        foreach (ArchMessage::find()->where(['version' => $lastVersion])->each(10) as $archMessage) {
            $message = new Message();
            if ($message->load(['Message' => $archMessage->attributes])) {
                if (!$message->save(false)) {
                    Yii::error($message->getErrors(), 'translate');
                    $success = false;
                }
            } else {
                Yii::error('ArchMessage not load revert', 'translate');
                $success = false;
            }
        }
        return $success;
    }
}