<?php

return [
    'params' => [
        'excel_columns' => [
            'lang_en' => 'en-US',
            'lang_kz' => 'kz-KZ',
            'lang_ru' => 'ru-RU',
            'message_id' => 'A',
            'message_category' => 'B',
            'message' => 'C',
            'message_kz' => 'D',
            'message_ru' => 'E',
            'message_en' => 'F',
        ],
    ],
];