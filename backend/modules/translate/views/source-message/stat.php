<?php

use common\modules\translate\models\SourceMessage;
use backend\modules\translate\services\TranslateStatService;
use backend\modules\translate\services\ViewHelpService;

/* @var $this yii\web\View */

$this->title = 'Statistics';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Source Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="translate-source_message-stat">
    <div class="panel">
        <div class="panel-body">

            <table class="table  table-striped table-bordered" style="width: auto;">
                <?php
                echo ViewHelpService::getStatNotLangTranslate();
                ?>

                <tr>
                    <td>С кириллицей:</td>
                    <td> <?= TranslateStatService::getCyrillicCountSize() ?></td>
                </tr>
                <tr>
                    <td>Общее количество:</td>
                    <td> <?= SourceMessage::find()->count() ?></td>
                </tr>
            </table>

        </div>
    </div>
</div>
