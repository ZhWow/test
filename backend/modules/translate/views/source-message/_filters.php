<?php

use yii\helpers\Html;
use backend\modules\translate\models\archive\ArchTranslateData;

?>

<div class="col-md-9">
    <div id="filter-source-message" class="row">

        <div class="col-md-2">
            <?= Html::a(Yii::t('trn', 'With cyrillic'), ['index', 'cyrillic' => true],
                ['class' => 'btn btn-raised btn-default']) ?>
        </div>

        <div class="col-md-3">
            <?= Html::a(Yii::t('trn', 'Load from document'), ['create'],
                ['data-target' => '#load-excel-modal', 'data-toggle' => 'modal', 'class' => 'btn btn-raised btn-primary']) ?>
        </div>

        <div class="col-md-3">
            <?= Html::a(Yii::t('trn', 'Create Source Message'), ['create'], ['class' => 'btn btn-raised btn-success']) ?>
        </div>

        <div class="col-md-3">
            <?= Html::a(Yii::t('trn', 'Statistic'), ['stat'], ['class' => 'btn btn-raised btn-info']) ?>
        </div>

        <div class="col-md-2">
            <?= Html::a(Yii::t('trn', 'Fix'), ['fix-messages'], ['class' => 'btn btn-raised btn-default']) ?>
        </div>

    </div>

</div>