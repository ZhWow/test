<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\translate\models\SourceMessage */

$this->title = Yii::t('trn', 'Create Sys Source Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('trn', 'Sys Source Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sys-source-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
