<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\translate\models\SourceMessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-3">
    <div class="sys-source-message-search">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <?= $form->field($model, 'language')->dropDownList($model->languages(), [
            'prompt' => Yii::t('app', 'Select language for filter')
        ]) ?>

        <?= $form->field($model, 'translation_status')->dropDownList($model->translationStatus(), [
            'prompt' => Yii::t('app', 'Select type of translation')
        ]) ?>

        <div class="form-group row between-id-search">
            <label class="control-label">Between IDs</label>
            <div>
                <?= $form->field($model, 'id_from', ['options' => ['class' => 'col-md-6']])->textInput() ?>

                <?= $form->field($model, 'id_to', ['options' => ['class' => 'col-md-6']])->textInput() ?>
            </div>

        </div>

        <?= $form->field($model, 'translation')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('trn', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('trn', 'Reset'), 'index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>