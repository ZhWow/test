<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\translate\services\ViewHelpService;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\translate\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $cyrillic */

$this->title = Yii::t('trn', 'Source Messages');
$this->params['breadcrumbs'][] = $this->title;

\backend\modules\translate\assets\TranslateAsset::register($this);
?>

<div class="sys-source-message-index row">

    <h1><?= Html::encode($this->title) ?></h1>

    <button id="sys-source-message-index_filter_toggle" type="button" class="btn btn-primary">
        <span class="glyphicon glyphicon-cog"></span> Фильтры
    </button>

    <div id="filter-panel" class="collapse filter-panel">

        <div class="row">
            <?= $this->render('_filters') ?>
            <?= $this->render('_search', ['model' => $searchModel]) ?>
        </div>

        <h3><?= Yii::t('trn', 'Export to document') ?></h3>
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'filename' => 'translates_' . Yii::$app->formatter->asDatetime(time(), 'yyyyMMdd_HHmm'),
            'columns' => ViewHelpService::getColumnsExportMenu(),
            'target' => '_self',
            'showConfirmAlert' => false,
            'fontAwesome' => true,
        ]) ?>
        <hr>

    </div>

    <h2><?= Yii::t('app', 'Filter') ?></h2>
    <p>
        <?php
        echo ViewHelpService::showFilter($searchModel->language, Yii::t('trn', 'filter-language'));
        echo ViewHelpService::showArrayFilter($searchModel->translation_status, Yii::t('trn', 'filter-translation status'), $searchModel->translationStatus());
        echo ViewHelpService::showFilter($searchModel->id_from, Yii::t('trn', 'filter-id - from'));
        echo ViewHelpService::showFilter($searchModel->id_to, Yii::t('trn', ' to'));
        echo ViewHelpService::showFilter($searchModel->translation, Yii::t('trn', 'filter-translation'));
        ?>
    </p>

    <?= Html::button(Yii::t('trn', 'Delete selected items'), [
        'id' => 'checkbox-button-deleted',
        'class' => 'btn btn-default'
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ViewHelpService::getColumnsIndexView(),
    ]); ?>
    
</div>

<?= $this->render('_modal_load_excel') ?>
