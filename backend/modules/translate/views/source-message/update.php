<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\translate\models\SourceMessage */

$this->title = Yii::t('trn', 'Update {modelClass}: ', [
    'modelClass' => 'Sys Source Message',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('trn', 'Sys Source Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('trn', 'Update');
?>
<div class="sys-source-message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
