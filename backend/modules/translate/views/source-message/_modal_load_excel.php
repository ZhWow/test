<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use common\modules\translate\models\SourceMessage;

$modalAttr = [
    'id' => 'load-excel-modal',
    'header' => Yii::t('trn', 'Load excel file')
];

$dropDownOptions = [
    '/translate/source-message/load-excel' => Yii::t('trn', 'NOT Replace'),
    '/translate/source-message/load-excel?replace=true' => Yii::t('trn', 'Replace')
];

$dropDownPromptOption = [
    'id' => 'load-type',
    'class' => 'form-control',
    'prompt' => Yii::t('trn', 'Please select type of upload translate from excel')
];

$activeFormOptions = [
    'options' => ['enctype' => 'multipart/form-data', 'id' => 'upload-excel'],
    'action' => '/translate/source-message/load-excel'
];

$fileInputOptions = [
    'options' => [
        'accept' => 'other',
    ],
    'pluginOptions' => [
        'showPreview' => false,
        'showUpload' => false,
    ]
];

$model = new SourceMessage();
?>

<?php Modal::begin($modalAttr);?>

    <?= Html::dropDownList('Excel upload type', null, $dropDownOptions, $dropDownPromptOption) ?>

    <?php $form = ActiveForm::begin($activeFormOptions);?>

        <?= $form->field($model, 'excelFile')->widget(FileInput::className(), $fileInputOptions) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('trn', 'Load'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

<?php Modal::end(); ?>