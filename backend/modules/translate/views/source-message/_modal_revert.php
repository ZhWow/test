<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use backend\modules\translate\models\archive\ArchTranslateData;
use common\models\User;

$modalAttr = [
    'id' => 'revert-modal',
    'header' => Yii::t('trn', 'Revert translation')
];

?>

<?php Modal::begin($modalAttr); ?>

    <div class="alert alert-info">
        <?php $lastVersion = ArchTranslateData::getLastVersion();
        if ($lastVersion !== null) {
            /* @var ArchTranslateData $archTranslateData */
            $archTranslateData = ArchTranslateData::find()->where(['version' => $lastVersion])->andWhere(['reverted' => 0])->one();
            $text = Yii::t('trn', 'user - {User}, version - {Version}, date - {Date}',
                ['User' => User::findOne($archTranslateData->created_by)->username, 'Version' => $archTranslateData->version,
                    'Date' => $archTranslateData->created_at]);
        } else {
            $text = Yii::t('trn', 'Not have backup copies');
        }
        echo $text;
        ?>
    </div>

    <div class="modal-footer">
        <?= Html::button(Yii::t('trn', 'Modal window cancel'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>

        <?= Html::a(Yii::t('trn', 'Revert'), ['revert'], ['class' => 'btn btn-primary']) ?>
    </div>

<?php Modal::end(); ?>