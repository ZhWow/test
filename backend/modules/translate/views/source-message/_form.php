<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\translate\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sys-source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true]) ?>

    <?php
    $allSysLang = \common\modules\translate\models\SourceMessage::getAllSysLang();
    foreach ($allSysLang as $sysLang) {
        /* @var $sysLang \common\modules\translate\models\SysLang */
        echo $form->field($model, $sysLang->url)->textInput(['maxlength' => true]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('trn', 'Create') : Yii::t('trn', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
