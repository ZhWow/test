<?php

namespace backend\modules\notifications\models\base;

use Yii;

/**
 * This is the model class for table "t_corp".
 *
 * @property integer $id
 * @property integer $blocked
 * @property string $domain
 * @property integer $applyFee
 * @property integer $switcher
 * @property string $base
 * @property string $company
 * @property string $company_ru
 * @property string $contract
 * @property string $contract_date
 * @property string $bin
 * @property string $phone
 * @property string $address
 * @property string $fullname
 * @property string $corPh
 * @property string $url
 * @property string $coordinator
 * @property string $coor_phone
 * @property string $email
 * @property string $log
 * @property string $pass
 * @property string $ip
 * @property string $base_log
 * @property string $port
 * @property string $base_pass
 * @property string $code_1c
 * @property string $manager
 * @property string $support
 * @property string $contact
 * @property integer $deposit
 * @property integer $credit
 * @property integer $bonus
 *
 * @property TGoogleanalytics[] $tGoogleanalytics
 * @property TRemarks[] $tRemarks
 */
class TCorp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_corp';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked', 'applyFee', 'switcher', 'deposit', 'credit', 'bonus'], 'integer'],
            [['base', 'company', 'company_ru', 'phone', 'address', 'email'], 'required'],
            [['domain', 'base', 'phone', 'coor_phone', 'ip', 'base_log', 'port', 'base_pass', 'code_1c', 'manager', 'support', 'contact'], 'string', 'max' => 100],
            [['company', 'company_ru', 'contract', 'contract_date', 'bin', 'address', 'fullname', 'corPh', 'url', 'coordinator', 'email', 'log', 'pass'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blocked' => 'Blocked',
            'domain' => 'Domain',
            'applyFee' => 'Apply Fee',
            'switcher' => 'Switcher',
            'base' => 'Base',
            'company' => 'Company',
            'company_ru' => 'Company Ru',
            'contract' => 'Contract',
            'contract_date' => 'Contract Date',
            'bin' => 'Bin',
            'phone' => 'Phone',
            'address' => 'Address',
            'fullname' => 'Fullname',
            'corPh' => 'Cor Ph',
            'url' => 'Url',
            'coordinator' => 'Coordinator',
            'coor_phone' => 'Coor Phone',
            'email' => 'Email',
            'log' => 'Log',
            'pass' => 'Pass',
            'ip' => 'Ip',
            'base_log' => 'Base Log',
            'port' => 'Port',
            'base_pass' => 'Base Pass',
            'code_1c' => 'Code 1c',
            'manager' => 'Manager',
            'support' => 'Support',
            'contact' => 'Contact',
            'deposit' => 'Deposit',
            'credit' => 'Credit',
            'bonus' => 'Bonus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTGoogleanalytics()
    {
        return $this->hasMany(TGoogleanalytics::className(), ['corp' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTRemarks()
    {
        return $this->hasMany(TRemarks::className(), ['corpId' => 'id']);
    }
}
