<?php

namespace backend\modules\notifications\models\base;

use Yii;
use backend\modules\integra\models\base\TUsers;

/**
 * This is the model class for table "t_agencydetails".
 *
 * @property integer $id
 * @property string $agencyId
 * @property string $address
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $website
 * @property string $bin
 * @property string $logofile
 *
 * @property TUsers $agency
 */
class TAgencydetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_agencydetails';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agencyId'], 'integer'],
            [['address'], 'string'],
            [['phone', 'mobile', 'email', 'website', 'bin'], 'string', 'max' => 50],
            [['logofile'], 'string', 'max' => 200],
            [['agencyId'], 'exist', 'skipOnError' => true, 'targetClass' => TUsers::className(), 'targetAttribute' => ['agencyId' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agencyId' => 'Agency ID',
            'address' => 'Address',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'website' => 'Website',
            'bin' => 'Bin',
            'logofile' => 'Logofile',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(TUsers::className(), ['ID' => 'agencyId']);
    }
}
