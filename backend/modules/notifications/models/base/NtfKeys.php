<?php

namespace backend\modules\notifications\models\base;

use Yii;

/**
 * This is the model class for table "ntf_keys".
 *
 * @property integer $id
 * @property string $triger
 * @property string $type
 */
class NtfKeys extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ntf_keys';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['triger', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'triger' => 'Triger',
            'type' => 'Type',
        ];
    }
}
