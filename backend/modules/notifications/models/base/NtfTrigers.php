<?php

namespace backend\modules\notifications\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ntf_trigers".
 *
 * @property integer $id
 * @property string $triger
 * @property integer $corp_id
 * @property integer $key_id
 */
class NtfTrigers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ntf_trigers';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['corp_id', 'key_id'], 'integer'],
            [['triger'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'triger' => 'Triger',
            'corp_id' => 'Corp ID',
            'key_id' => 'Key ID',
        ];
    }

    public function getKeys()
    {
        return $this->hasOne(NtfKeys::className(), ['id' => 'key_id']);
    }
}
