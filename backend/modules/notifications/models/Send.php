<?php
/**
 * User: Leonic
 * Date: 12.12.2017
 * Time: 18:46
 */

namespace backend\modules\notifications\models;

use backend\modules\hotels\models\HReservationConfirmed;
use backend\modules\notifications\models\base\TCorp;
use backend\modules\integra\models\base\AmadeusIntegra;
use backend\modules\notifications\models\base\NtfTrigers;
use yii\db\Expression;

class Send
{
    public $trigger;
    public $corp;
    public $condition = [];
    public $keys = [];

    public function __construct($post)
    {
        $this->trigger = $post['trigger'];
        $this->condition = $post['condition'];
        $this->corp = TCorp::findOne($post['corp_id']);
        $this->keys = isset($post['keys']) ? $post['keys'] : false;

        $triggers = NtfTrigers::findAll(['corp_id' => $this->corp->id, 'triger' => $this->trigger]);

        if ($triggers) {
            $this->send($triggers);
        }
    }

    public function send($triggers)
    {
        foreach ($triggers as $trigger) {
            if ($trigger->keys) {
                $type = array_keys($this->condition)[0];
                $model = false;
                $sendClassName = 'backend\modules\notifications\services\\'.$trigger->keys->type;

                if ($this->condition['provider'] == 'flight') {
                    if ($type == 'ticket') {
                        $model = AmadeusIntegra::findOne($this->condition['ticket']);
                        $sendClassName::send($this->keys, $this, $model);
                    }
                } elseif ($this->condition['provider'] == 'rail') {
                    //TODO Rail
                } elseif ($this->condition['provider'] == 'hotel') {
                    //TODO Hotel
                    if ($this->trigger == 'cron_checkout') {
                        HReservationConfirmed::setDb($this->corp->base);
                        $hReservation = HReservationConfirmed::find()->where(['=', 'str_to_date(checkOutDateTime, "%d.%m.%Y")', new Expression('current_date()')])->all();
                        if ($hReservation) {
                            foreach ($hReservation as $item) {
                                if ($item->hotel_email) {
                                    $this->trigger = $item->hotel_email;
                                    $sendClassName::send($trigger->keys, $this, $item);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}