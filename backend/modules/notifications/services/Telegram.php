<?php
/**
 * User: Leonic
 * Date: 12.12.2017
 * Time: 19:54
 */

namespace backend\modules\notifications\services;

use yii\web\View;
use yii\httpclient\Client;
use backend\modules\notifications\models\Send;
use backend\modules\notifications\models\base\NtfKeys;

class Telegram
{
    public static function send(NtfKeys $key, Send $trigger, $model)
    {
        $viewsPath = '@backend/modules/notifications/views';
        $viewClass = new View();
        $content = $viewClass->renderAjax($viewsPath . '/telegram/' . $trigger->trigger . '_'.
            array_keys($trigger->condition)[0], ['model' => $model, 'trigger' => $trigger]);

        $client = new Client();
        $client->createRequest()
            ->setMethod('post')
            ->setUrl('http://api.btmc.kz/telegrambot.php')
            ->setData(['chatID' => $key->triger, 'message' => $content])
            ->send();
    }
}