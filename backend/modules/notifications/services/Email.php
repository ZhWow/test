<?php
/**
 * User: Leonic
 * Date: 12.12.2017
 * Time: 19:07
 */

namespace backend\modules\notifications\services;

use yii\web\View;
use backend\models\TMail;
use common\modules\helpers\CreatePdf;
use backend\modules\notifications\models\Send;
use backend\modules\notifications\models\base\NtfKeys;

class Email
{
    public static function send(NtfKeys $key, Send $trigger, $model)
    {
        $viewsPath = '@backend/modules/notifications/views';
        $viewClass = new View();
        $content = $viewClass->renderAjax($viewsPath. '/mail/' . $trigger->trigger . '_'. array_keys($trigger->condition)[0], ['model' => $model, 'trigger' => $trigger]);
        $html  = $viewClass->renderAjax($viewsPath . '/notification/html', ['content' => $content]);
        $tMail = new TMail();
        $tMail->reciepent = $key->triger;
        $tMail->htmlText = $html;
        $tMail->title = 'Qway no-reply';

        if (array_keys($trigger->condition)[0] == 'ticket') {
            $pdfTemplate = $viewClass->renderAjax($viewsPath . '/notification/_pdf-ticket', ['model' => $model, 'trigger' => $trigger]);

            $pdfClass = new CreatePdf();
            $filePdf = $pdfClass->createPdf($model->DOCUMENT_NUMBER. '_' . $model->LAST_NAME. '_' . $model->FIRST_NAME, $pdfTemplate);

            $tMail->attachment = $filePdf;
            $tMail->isAttachment = 1;
        }

        $tMail->save(false);
    }
}