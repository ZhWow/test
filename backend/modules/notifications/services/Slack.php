<?php
/**
 * User: Leonic
 * Date: 13.12.2017
 * Time: 13:15
 */

namespace backend\modules\notifications\services;

use backend\modules\integra\models\base\AmadeusIntegra;
use Yii;
use yii\web\View;
use backend\modules\notifications\models\Send;
use backend\modules\notifications\models\base\NtfKeys;

class Slack
{
    public static function send(NtfKeys $key, Send $trigger, $model)
    {
        $viewsPath = '@backend/modules/notifications/views';
        $viewClass = new View();
        $content = $viewClass->renderAjax($viewsPath . '/slack/' . $trigger->trigger . '_'. array_keys($trigger->condition)[0], ['model' => $model, 'trigger' => $trigger]);

        $client = Yii::$app->slack;
        $client->url = $key->triger;
        $client->username = 'My awesome application';
        $client->send('Qway Web App Bot', ':thumbs_up:', [json_decode($content, true)]);
    }
}