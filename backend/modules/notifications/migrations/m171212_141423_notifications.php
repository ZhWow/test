<?php
namespace backend\modules\notifications\migrations;

use yii\db\Migration;

class m171212_141423_notifications extends Migration
{
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    public function safeUp()
    {
        $this->createTable('ntf_trigers', [
            'id'            => $this->primaryKey(11),
            'triger'        => $this->string(50),
            'corp_id'       => $this->integer(11),
            'key_id'        => $this->integer(11),
        ]);

        $this->createTable('ntf_keys', [
            'id'            => $this->primaryKey(11),
            'triger'        => $this->string(255),
            'type'          => $this->string(255),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('ntf_trigers');
        $this->dropTable('ntf_keys');
        echo "m171212_141423_notifications cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171212_141423_notifications cannot be reverted.\n";

        return false;
    }
    */
}
