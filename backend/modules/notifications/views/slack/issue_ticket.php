<?php
/**
 * User: Leonic
 * Date: 13.12.2017
 * Time: 13:38
 * @var $trigger \backend\modules\notifications\models\Send
 * @var $trigger->corp \backend\modules\notifications\models\base\TCorp
 * @var $model \backend\modules\integra\models\base\AmadeusIntegra
 */

$return = [
    'fallback' => 'Required plain-text summary of the attachment.',
    'color' => '#36a64f',
    'text' => 'Выписка электронного билета',
    'fields' => [
        [
            'title' => 'Номер билета',
            'value' => $model->DOCUMENT_NUMBER,
            'short' => true,
        ],
        [
            'title' => 'Номер брони',
            'value' => $model->RECORD_LOCATOR,
            'short' => true,
        ],
        [
            'title' => 'Компания',
            'value' => $trigger->corp->company_ru,
            'short' => true,
        ],
        [
            'title' => 'Маршрут',
            'value' => $model->ROUTE,
            'short' => true,
        ],
        [
            'title' => 'Пассажир',
            'value' => $model->LAST_NAME . ' ' . $model->FIRST_NAME,
            'short' => true,
        ],
    ],
    'footer' => 'Qway Mobile',
    'footer_icon' => 'https://platform.slack-edge.com/img/default_application_icon.png',
    'ts' => time(),
];
echo json_encode($return, JSON_UNESCAPED_UNICODE);