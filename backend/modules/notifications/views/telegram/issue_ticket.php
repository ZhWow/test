<?php
/**
 * User: Leonic
 * Date: 13.12.2017
 * Time: 13:38
 * @var $trigger \backend\modules\notifications\models\Send
 * @var $trigger->corp \backend\modules\notifications\models\base\TCorp
 */

use backend\modules\integra\models\base\AmadeusIntegra;
$model = AmadeusIntegra::findOne($trigger->condition['ticket']);
?>
Выписка электронного билета !!!
Код брони: <?=$model->RECORD_LOCATOR . PHP_EOL?>
Компания: <?=$trigger->corp->company_ru . PHP_EOL?>
Маршрут: <?=$model->ROUTE . PHP_EOL?>
Пассажир: <?=$model->LAST_NAME?> <?=$model->FIRST_NAME . PHP_EOL?>
Билеты: <?=$model->DOCUMENT_NUMBER . PHP_EOL?>
Количество дней до вылета: 0
Стоимость: <?=$model->TOTAL . PHP_EOL?>
Класс: <?=$model->BOOKING_CLASSES . PHP_EOL?>
Валидирующий перевозчик: <?=$model->VALIDATING_CARRIER . PHP_EOL?>
