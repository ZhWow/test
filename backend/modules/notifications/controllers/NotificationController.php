<?php

namespace backend\modules\notifications\controllers;

use backend\modules\notifications\models\Send;
use Yii;
use yii\web\Controller;

/**
 * Notification controller for the `notifications` module
 */
class NotificationController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {
//        $post = Yii::$app->request->post();
        $post = [
            'trigger'   => 'cron_checkout',
            'corp_id'   => '16',
            'condition' => [
                'hotel'    => ['1'],
                'provider'  => 'hotel'
            ]
        ];

        if ($post) {
            $send = new Send($post);
        }
    }
}
