<?php

namespace backend\modules\hotels\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use backend\modules\integra\models\TUsers;
use yii\helpers\ArrayHelper;


class HRooms extends \backend\modules\hotels\models\base\HRooms
{


    public static function getType()
    {
        return [
            'SGL' => 'SGL',
            'DBL' => 'DBL',
        ];
    }

    private static function getPath()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        // return Yii::getAlias("@backend/modules/hotels/images/" . $tUser->hotelID);
		return dirname(getcwd()) . '/../../manageit/hotels/' . $tUser->hotelID;
    }

    private static function getPathForAdmin($hotelID)
    {
//        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        // return Yii::getAlias("@backend/modules/hotels/images/" . $tUser->hotelID);
        return dirname(getcwd()) . '/../../manageit/hotels/' . $hotelID;
    }

    public function upload()
    {
        $success = false;
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        $this->images = UploadedFile::getInstances($this, 'images');
        if ($this->validate()) {
            if ($this->save()) {
                $success = true;
                foreach ($this->images as $image) {

                    $path = self::getPath();

                    if (!file_exists($path)) {
                        BaseFileHelper::createDirectory($path);
                    }
                    $save_img = $path . DIRECTORY_SEPARATOR . $image;


                    if ($image->saveAs($save_img)) {
                        $hImageHotel = new HImagehotel();
                        $hImageHotel->imageName = $image->name;
                        $hImageHotel->hotelID = $tUser->hotelID;
                        $hImageHotel->roomID = $this->id;
                        $hImageHotel->save();
                    } else {
                        Yii::error('не удалось загрузить изоброжение', 'admin');
                    }
                }
            }


        }
        return $success;
    }

    public function uploadByAdmin($hotelID)
    {
        $success = false;

        $this->images = UploadedFile::getInstances($this, 'images');
        if ($this->validate()) {
            if ($this->save()) {
                $success = true;
                foreach ($this->images as $image) {

                    $path = self::getPathForAdmin($hotelID);

                    if (!file_exists($path)) {
                        BaseFileHelper::createDirectory($path);
                    }
                    $save_img = $path . DIRECTORY_SEPARATOR . $image;


                    if ($image->saveAs($save_img)) {
                        $hImageHotel = new HImagehotel();
                        $hImageHotel->imageName = $image->name;
                        $hImageHotel->hotelID = $hotelID;
                        $hImageHotel->roomID = $this->id;
                        $hImageHotel->save();
                    } else {
                        Yii::error('не удалось загрузить изоброжение', 'admin');
                    }
                }
            }


        }
        return $success;
    }

    public function getHotel()
    {
        return $this->hasOne(HHotels::className(),['id' => 'hotelID']);
    }

    public function getAvailability()
    {
        return $this->hasMany(HAvailability::className(),['roomID' => 'id', 'hotelID' => 'hotelID']);
    }

}
