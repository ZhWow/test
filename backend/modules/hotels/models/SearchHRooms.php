<?php

namespace backend\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\hotels\models\HRooms;
use backend\modules\integra\models\TUsers;

/**
 * SearchHRooms represents the model behind the search form about `backend\modules\hotels\models\HRooms`.
 */
class SearchHRooms extends HRooms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Bedsnum', 'RoomTypeId', 'BoardBasisID', 'CancellationPolicyID', 'hotelID', 'accomodationTypeID'], 'integer'],
            [['Type', 'TypeDescription', 'RoomDescription', 'IsSpecialOffer', 'NeedNationality', 'Name', 'CheckInTime', 'CheckOutTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $query = HRooms::find()->where(['hotelID' => $tUser->hotelID]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'Bedsnum' => $this->Bedsnum,
            'RoomTypeId' => $this->RoomTypeId,
            'BoardBasisID' => $this->BoardBasisID,
            'CancellationPolicyID' => $this->CancellationPolicyID,
            'hotelID' => $this->hotelID,
            'accomodationTypeID' => $this->accomodationTypeID,
        ]);

        $query->andFilterWhere(['like', 'Type', $this->Type])
            ->andFilterWhere(['like', 'TypeDescription', $this->TypeDescription])
            ->andFilterWhere(['like', 'RoomDescription', $this->RoomDescription])
            ->andFilterWhere(['like', 'IsSpecialOffer', $this->IsSpecialOffer])
            ->andFilterWhere(['like', 'NeedNationality', $this->NeedNationality])
            ->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'CheckInTime', $this->CheckInTime])
            ->andFilterWhere(['like', 'CheckOutTime', $this->CheckOutTime]);

        return $dataProvider;
    }
}
