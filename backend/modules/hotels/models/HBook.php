<?php

namespace backend\modules\hotels\models;

use backend\models\Corp;
use Yii;

class HBook
{
    public static function find($condition = false)
    {
        $corps = Corp::find()->asArray()->all();
        $allBooks = [];

        $where = '';
        if (is_array($condition)) {
            foreach ($condition as $key => $val) {
                $where .=  ' AND ' . $key . '=' . $val;
            }
        }
        foreach ($corps as $corp) {
            $query = 'SELECT * FROM h_book WHERE stateID = 1 AND to_buy = 2' . $where; // TODO where isContentIn
            $results = Yii::$app->{'db_' . $corp['base']}->createCommand($query)->queryAll();
            foreach ($results as $result) {
                $allBooks[] = $result;
            }
        }

        return $allBooks;
    }

    public static function findCondition($id, $corp)
    {

        $query = 'SELECT * FROM h_book WHERE id =' . $id; // TODO where isContentIn
        $result = Yii::$app->{'db_' . $corp}->createCommand($query)->queryOne();
        return $result;
    }

    public static function checkStateIdAndToBuy($id, $corp, $stateId, $toBuy)
    {
        Yii::$app->{'db_' . $corp}->createCommand('UPDATE h_book SET stateID = ' . $stateId . ', to_buy = '. $toBuy .' WHERE id="' . $id . '"')->execute();
    }
}