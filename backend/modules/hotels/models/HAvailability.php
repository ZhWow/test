<?php

namespace backend\modules\hotels\models;

use Yii;
use backend\modules\integra\models\TUsers;

class HAvailability extends \backend\modules\hotels\models\base\HAvailability
{
    public static function roomList()
    {
        $result = [];

        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        $rooms = HRooms::find()->where(['hotelID' => $tUser->hotelID])->all();
        foreach ($rooms as $room) {
            $result[$room->id] = $room->Name . ' - '. $room->Type . '/' . $room->TypeDescription;
        }

        return $result;
    }

    public function getCity()
    {
        return $this->hasOne(HCities::className(),['id' => 'cityID']);
    }

    public function getRoom()
    {
        return $this->hasOne(HRooms::className(),['id' => 'roomID']);
    }

    public function getSpecialFares()
    {
        return $this->hasMany(HSpecial_fare::className(),['fare_id' => 'special_fare_id']);
    }

    public static function getGuid($opt = false)
    {

        if (function_exists('com_create_guid')) {
            if ($opt) {
                return com_create_guid();
            } else {
                return base64_encode(sha1((trim(com_create_guid(), '{}'))));
            }
        } else {
            mt_srand((double)microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);    // "-"
            $left_curly = $opt ? chr(123) : "";
            $right_curly = $opt ? chr(125) : "";
            $uuid = $left_curly
                . substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12)
                . $right_curly;
            return $uuid;
        }
    }
}
