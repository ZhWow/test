<?php

namespace backend\modules\hotels\models\base;

use Yii;

/**
 * This is the model class for table "h_services".
 *
 * @property integer $id
 * @property integer $hotelID
 * @property string $general
 * @property string $facilities
 * @property string $mealDrinks
 * @property string $health
 * @property string $transfer
 * @property string $reception
 * @property string $cleaning
 * @property string $services
 * @property string $other
 */
class HServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_services';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotelID'], 'integer'],
            [['general', 'facilities', 'mealDrinks', 'health', 'transfer', 'reception', 'cleaning', 'services', 'other'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotelID' => 'Hotel ID',
            'general' => 'General',
            'facilities' => 'Facilities',
            'mealDrinks' => 'Meal Drinks',
            'health' => 'Health',
            'transfer' => 'Transfer',
            'reception' => 'Reception',
            'cleaning' => 'Cleaning',
            'services' => 'Services',
            'other' => 'Other',
        ];
    }
}
