<?php

namespace backend\modules\hotels\models\base;

use Yii;

/**
 * This is the model class for table "h_imagehotel".
 *
 * @property integer $id
 * @property string $imageName
 * @property integer $hotelID
 * @property integer $roomID
 */
class HImagehotel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_imagehotel';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotelID', 'roomID'], 'integer'],
            [['imageName'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imageName' => 'Image Name',
            'hotelID' => 'Hotel ID',
            'roomID' => 'Room ID',
        ];
    }
}
