<?php

namespace backend\modules\hotels\models\base;

use Yii;

/**
 * This is the model class for table "h_availability".
 *
 * @property integer $id
 * @property integer $cityID
 * @property string $cityCode
 * @property integer $hotelID
 * @property integer $roomID
 * @property integer $roomNum
 * @property integer $paxNum
 * @property string $roomType
 * @property string $startDate
 * @property string $endDate
 * @property string $Amount
 * @property string $NetAmount
 * @property integer $PenaltyRefundAmount
 * @property integer $DaysForAutoCancel
 * @property string $Currency
 * @property string $GUID
 * @property integer $ReservationIdContentinn
 * @property integer $special_fare_id
 */
class HAvailability extends \yii\db\ActiveRecord
{

    public $range_date;
    public $corp_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_availability';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityID', 'hotelID', 'roomID', 'roomNum', 'paxNum', 'PenaltyRefundAmount', 'DaysForAutoCancel', 'ReservationIdContentinn'], 'integer'],
            [['cityCode', 'startDate', 'endDate', 'Amount', 'NetAmount', 'Currency'], 'string', 'max' => 50],
            [['roomType'], 'string', 'max' => 3],
            [['GUID'], 'string', 'max' => 70],
            [['special_fare_id'], 'integer'],
            [['range_date'], 'string'],
            [['hotelID', 'roomID', 'roomNum', 'paxNum', 'range_date', 'Amount', 'PenaltyRefundAmount'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cityID' => 'City ID',
            'cityCode' => 'City Code',
            'hotelID' => 'Hotel ID',
            'roomID' => 'Room ID',
            'roomNum' => 'Room Num',
            'paxNum' => 'Pax Num',
            'roomType' => 'Room Type',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'Amount' => 'Amount',
            'NetAmount' => 'Net Amount',
            'PenaltyRefundAmount' => 'Penalty Refund Amount',
            'DaysForAutoCancel' => 'Days For Auto Cancel',
            'Currency' => 'Currency',
            'GUID' => 'Guid',
            'ReservationIdContentinn' => 'Reservation Id Contentinn',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $dateArray = explode('to', $this->range_date);
            $this->startDate = trim($dateArray[0]);
            $this->endDate = trim($dateArray[1]);
            $this->NetAmount = trim($this->Amount);
            if ($this->isNewRecord) {
                $this->GUID = \backend\modules\hotels\models\HAvailability::getGuid();
            }

            return true;
        } else {
            return false;
        }
    }
}
