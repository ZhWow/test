<?php

namespace backend\modules\hotels\models\base;

use Yii;

/**
 * This is the model class for table "h_rules".
 *
 * @property integer $id
 * @property integer $hotelID
 * @property string $cancelPenalty
 * @property string $extraBedAndChilds
 * @property string $internet
 * @property string $parking
 * @property string $animals
 * @property string $additionalServices
 */
class HRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_rules';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotelID'], 'integer'],
            [['cancelPenalty', 'extraBedAndChilds', 'internet', 'parking', 'animals', 'additionalServices'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotelID' => 'Hotel ID',
            'cancelPenalty' => 'Cancel Penalty',
            'extraBedAndChilds' => 'Extra Bed And Childs',
            'internet' => 'Internet',
            'parking' => 'Parking',
            'animals' => 'Animals',
            'additionalServices' => 'Additional Services',
        ];
    }
}
