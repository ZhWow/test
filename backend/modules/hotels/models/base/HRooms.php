<?php

namespace backend\modules\hotels\models\base;

use Yii;

/**
 * This is the model class for table "h_rooms".
 *
 * @property integer $id
 * @property string $Type
 * @property string $TypeDescription
 * @property integer $Bedsnum
 * @property string $RoomDescription
 * @property string $IsSpecialOffer
 * @property integer $RoomTypeId
 * @property string $NeedNationality
 * @property string $Name
 * @property integer $BoardBasisID
 * @property integer $CancellationPolicyID
 * @property integer $hotelID
 * @property integer $accomodationTypeID
 * @property string $CheckInTime
 * @property string $CheckOutTime
 */
class HRooms extends \yii\db\ActiveRecord
{
    public $images;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_rooms';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Bedsnum', 'RoomTypeId', 'BoardBasisID', 'CancellationPolicyID', 'hotelID', 'accomodationTypeID'], 'integer'],
            [['RoomDescription'], 'string'],
            [['Type', 'TypeDescription', 'IsSpecialOffer', 'NeedNationality', 'Name'], 'string', 'max' => 50],
            [['CheckInTime', 'CheckOutTime'], 'string', 'max' => 100],
            [['images'], 'each', 'rule' => ['file', 'extensions' => 'jpg, jpeg, gif, png']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Type' => 'Type',
            'TypeDescription' => 'Type Description',
            'Bedsnum' => 'Bedsnum',
            'RoomDescription' => 'Room Description',
            'IsSpecialOffer' => 'Is Special Offer',
            'RoomTypeId' => 'Room Type ID',
            'NeedNationality' => 'Need Nationality',
            'Name' => 'Name',
            'BoardBasisID' => 'Board Basis ID',
            'CancellationPolicyID' => 'Cancellation Policy ID',
            'hotelID' => 'Hotel ID',
            'accomodationTypeID' => 'Accomodation Type ID',
            'CheckInTime' => 'Check In Time',
            'CheckOutTime' => 'Check Out Time',
        ];
    }
}
