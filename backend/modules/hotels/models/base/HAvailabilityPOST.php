<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 12.10.2017
 * Time: 19:22
 */

namespace backend\modules\hotels\models\base;

use Yii;
use yii\db\ActiveRecord;

class HAvailabilityPOST extends ActiveRecord
{

    public static function tableName()
    {
        return 'h_availability';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'NetAmount'], 'string'],
            [['startDate', 'endDate', 'NetAmount'], 'required'],
        ];
    }
}