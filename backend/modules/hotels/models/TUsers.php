<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 12.10.2017
 * Time: 15:24
 */

namespace backend\modules\hotels\models;

class TUsers extends \backend\modules\integra\models\base\TUsers
{

    public function getHotel()
    {
        return $this->hasOne(HHotels::className(),['id' => 'hotelID']);
    }
}