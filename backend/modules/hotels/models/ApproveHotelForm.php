<?php


namespace backend\modules\hotels\models;

use yii\base\Model;

class ApproveHotelForm extends Model
{
    public $hotel_id;
    public $hotel_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['hotel_id', 'hotel_name'], 'required'],
        ];
    }
}