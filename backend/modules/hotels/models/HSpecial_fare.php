<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 28-Sep-17
 * Time: 14:27
 */

namespace backend\modules\hotels\models;

use backend\models\Corp;
use Yii;
use yii\db\ActiveRecord;

class HSpecial_fare extends ActiveRecord
{

    public function rules()
    {
        return [
            [['fare_id','corp_id'], 'integer'],
        ];
    }
    public static function tableName()
    {
        return 'h_special_fares';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    public function getCorpName()
    {
        return $this->hasOne(Corp::className(), ['id' => 'corp_id']);
    }

    public static function getMaxFareId()
    {
        $max_id = HSpecial_fare::find()->max('fare_id');
        if ($max_id == null) {
            $max_id = 0;
        }
        return $max_id;
    }


}