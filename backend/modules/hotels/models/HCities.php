<?php

namespace backend\modules\hotels\models;

use Yii;

/**
 * This is the model class for table "h_cities".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $CountryCode
 * @property string $SubdivisionId
 * @property string $Type
 * @property string $IataCode
 * @property string $NearestIataCode
 * @property string $NameRu
 */
class HCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_cities';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id'], 'integer'],
            [['Name', 'CountryCode', 'Type', 'IataCode', 'NameRu'], 'string', 'max' => 100],
            [['SubdivisionId'], 'string', 'max' => 50],
            [['NearestIataCode'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Name',
            'CountryCode' => 'Country Code',
            'SubdivisionId' => 'Subdivision ID',
            'Type' => 'Type',
            'IataCode' => 'Iata Code',
            'NearestIataCode' => 'Nearest Iata Code',
            'NameRu' => 'Name Ru',
        ];
    }
}
