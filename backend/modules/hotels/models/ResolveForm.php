<?php

namespace backend\modules\hotels\models;

use yii\base\Model;

class ResolveForm extends Model
{
    public $action;
    public $corp;
    public $penalty;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'corp', 'penalty'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'penalty' => \Yii::t('app', 'Penalty'),
        ];
    }
}