<?php

namespace backend\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\hotels\models\HAvailability;
use backend\modules\integra\models\TUsers;

/**
 * SearchHAvailability represents the model behind the search form about `backend\modules\hotels\models\HAvailability`.
 */
class SearchHAvailability extends HAvailability
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cityID', 'hotelID', 'roomID', 'roomNum', 'paxNum', 'PenaltyRefundAmount', 'DaysForAutoCancel', 'ReservationIdContentinn'], 'integer'],
            [['cityCode', 'roomType', 'startDate', 'endDate', 'Amount', 'NetAmount', 'Currency', 'GUID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $query = HAvailability::find()->where(['hotelID' => $tUser->hotelID]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cityID' => $this->cityID,
            'hotelID' => $this->hotelID,
            'roomID' => $this->roomID,
            'roomNum' => $this->roomNum,
            'paxNum' => $this->paxNum,
            'PenaltyRefundAmount' => $this->PenaltyRefundAmount,
            'DaysForAutoCancel' => $this->DaysForAutoCancel,
            'ReservationIdContentinn' => $this->ReservationIdContentinn,
        ]);

        $query->andFilterWhere(['like', 'cityCode', $this->cityCode])
            ->andFilterWhere(['like', 'roomType', $this->roomType])
            ->andFilterWhere(['like', 'startDate', $this->startDate])
            ->andFilterWhere(['like', 'endDate', $this->endDate])
            ->andFilterWhere(['like', 'Amount', $this->Amount])
            ->andFilterWhere(['like', 'NetAmount', $this->NetAmount])
            ->andFilterWhere(['like', 'Currency', $this->Currency])
            ->andFilterWhere(['like', 'GUID', $this->GUID]);

        return $dataProvider;
    }
}
