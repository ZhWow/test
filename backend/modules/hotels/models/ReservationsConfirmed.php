<?php

namespace backend\modules\hotels\models;

use backend\models\TMail;
use backend\modules\integra\services\TelegramBot;
use backend\services\MailBotBTMC;
use common\modules\helpers\TelegramNotification;
use Yii;
use yii\helpers\ArrayHelper;
use backend\models\Corp;
use backend\modules\hotels\services\Voucher;

class ReservationsConfirmed
{
    const TABLE = 'h_reservations_confirmed';

    public static function record($hBook, $corp, $penalty, $isToCancel = false)
    {
        $hotelInfo = HotelInformation::findOne($hBook['hotelID']);
        $room = RoomInfo::findOne($hBook['roomID'], $hBook['hotelID']);
        $rules = Rules::findOne($hBook['hotelID']);

        $paxNames = explode(';', $hBook['paxNames']);
        $paxSurnames = explode(';', $hBook['paxSurnames']);
        $paxEmails = explode(';', $hBook['paxEmails']);

        foreach ($paxNames as $key => $val) {
            $guest = $paxSurnames[$key] . ' ' . $paxNames[$key];
            $mainData = [
                'hotel_bookingReference' => $hBook['bookingReference'],
                'hotelID' => $hBook['hotelID'],
                'roomID' => $hBook['roomID'],
                'bookID' => $hBook['id'],
                'checkInDateTime' => $hBook['checkInDateTime'],
                'checkOutDateTime' => $hBook['checkOutDateTime'],
                'price_totalPrice' => $hBook['totalPrice'],
                'cityCode' => $hBook['cityCode'],
                'stars' => $hBook['stars'],
                'daysBeforeCheckin' => $hBook['daysBeforeCheckin'],
                'discountAmount' => $hBook['discountAmount'],
                'discountPercent' => $hBook['discountPercent'],
                'discountCurrencyCode' => $hBook['discountCurrencyCode'],
                'userID' => $hBook['userID'],
                'corpID' => $hBook['corpID'],
                'roomTypeSentence' => $hBook['roomTypeSentence'],
                'payment_type' => $hBook['payment_type'],

                'sendTo' => $paxEmails[$key],
                'guest' => $guest,

                'hotel_address' => $hotelInfo['address'],
                'hotel_Name' => $hotelInfo['hotelName'],
                'hotel_latitude' => $hotelInfo['latitude'],
                'hotel_longitude' => $hotelInfo['longitude'],
                'hotel_phone' => $hotelInfo['telephone'],
                'hotel_email' => $hotelInfo['email'],

                'hotel_roomType' => $room['TypeDescription'],

                'rules_cancelPenalty' => $rules['cancelPenalty'],
                'rules_extraBedAndChilds' => $rules['extraBedAndChilds'],
                'rules_internet' => $rules['internet'],
                'rules_parking' => $rules['parking'],
                'rules_animals' => $rules['animals'],
                'rules_additionalServices' => $rules['additionalServices'],

                'penalty' => $penalty,

            ];

            $additionalData = [];
            if ($isToCancel) {
                $additionalData = [
                    'toCancel' => 0,
                    'isCancelled' => 1,
                ];
            }

            $data = ArrayHelper::merge($additionalData, $mainData);
            Yii::$app->{'db_' . $corp}->createCommand()->insert(self::TABLE , $data)->execute();

            self::emailNotification($hBook, $corp, $paxEmails[$key], $guest, $isToCancel);
            self::telegramNotification($isToCancel === false ? 'onHotelConfirmByBTM' : 'onHotelCancelByBTM',$corp,$hotelInfo, $hBook);

        }
    }

    public static function telegramNotification($trigger, $corpBase, $hotelInfo, $hBook)
    {
        $telegram = new TelegramBot();
        $telegram->sendHotelNotification($trigger,$corpBase,$hotelInfo,$hBook);
    }

    public static function emailNotification($hBook, $corpBase, $reciepent, $guest, $isToCancel)
    {
        $mail = new TMail();
        //TODO to decide whether we should send email to coordinator, and should be kept in mind that booking can be done as personal by credit card that means that only guest should get notification to email
        if($isToCancel === false ) {
            $title = "Ваше бронирование Гостиницы подтверждено";
            $body = MailBotBTMC::getView('@backend/modules/hotels/views/hotel/mail-template/hotel-approve.php',$hBook);

            $voucher = new Voucher();

            $query = "SELECT * FROM h_reservations_confirmed WHERE hotel_bookingReference = '" . $hBook['bookingReference'] . "' AND sendTo = '" . $reciepent . "' AND guest = '" . $guest . "'";
            $hReservationConfirmed = $results = Yii::$app->{'db_' . $corpBase}->createCommand($query)->queryOne();

            if($hReservationConfirmed === false) return false;

            $file = $voucher->getVoucher($hBook, $hReservationConfirmed);

            $mail->isAttachment = 1;
            $mail->attachment = $file;

        } else {
            $title = "Ваше бронирование номера в Гостинице отклонено";
            $body = MailBotBTMC::getView('@backend/modules/hotels/views/hotel/mail-template/hotel-reject.php',$hBook);
            $file = NULL;
        }

        $mail->reciepent = $reciepent;
        $mail->title = $title;
        $mail->htmlText = $body;
        $mail->save();
    }

    public static function confirmAll($hotelId = false)
    {
        $corps = Corp::find()->asArray()->all();
        $all = [];

        $andWhere = $hotelId ? ' AND hotelID = '. $hotelId : '';
        foreach ($corps as $corp) {
            $query = 'SELECT * FROM ' . self::TABLE .' WHERE isCancelled =0' . $andWhere; // TODO where isContentIn
            $results = Yii::$app->{'db_' . $corp['base']}->createCommand($query)->queryAll();
            foreach ($results as $result) {
                $all[$result['id']] = $result;
            }
        }

        return $all;
    }

    public static function canceledAll($hotelId = false)
    {
        $corps = Corp::find()->asArray()->all();
        $all = [];

        $andWhere = $hotelId ? ' AND hotelID = '. $hotelId : '';
        foreach ($corps as $corp) {
            $query = 'SELECT * FROM ' . self::TABLE .' WHERE isCancelled =1' . $andWhere; // TODO where isContentIn
            $results = Yii::$app->{'db_' . $corp['base']}->createCommand($query)->queryAll();
            foreach ($results as $result) {
                $all[$result['id']] = $result;
            }
        }

        return $all;
    }

    public static function report($hotelId = false)
    {
        $corps = Corp::find()->asArray()->all();
        $all = [];

        $andWhere = $hotelId ? ' WHERE hotelID = '. $hotelId : '';
        foreach ($corps as $corp) {
            $query = 'SELECT * FROM ' . self::TABLE . $andWhere; // TODO where isContentIn
            $results = Yii::$app->{'db_' . $corp['base']}->createCommand($query)->queryAll();
            foreach ($results as $result) {
                $all[$result['id']] = $result;
            }
        }

        return $all;
    }
}