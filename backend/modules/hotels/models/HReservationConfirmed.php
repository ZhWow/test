<?php

namespace backend\modules\hotels\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "h_reservations_confirmed".
 *
 * @property integer $id
 * @property string $hotel_bookingReference
 * @property integer $isCancelled
 * @property integer $toCancel
 * @property string $sendTo
 * @property string $hotel_Name
 * @property string $hotel_roomType
 * @property string $hotel_address
 * @property string $hotel_latitude
 * @property string $hotel_longitude
 * @property string $hotel_phone
 * @property string $hotel_email
 * @property string $checkInDateTime
 * @property string $checkOutDateTime
 * @property string $guest
 * @property string $tripDepartments
 * @property string $price_fare
 * @property string $tripReasons
 * @property string $price_VAT
 * @property string $price_totalPrice
 * @property string $rules_cancelPenalty
 * @property string $rules_extraBedAndChilds
 * @property string $rules_internet
 * @property string $rules_parking
 * @property string $rules_animals
 * @property string $rules_additionalServices
 * @property integer $hotelID
 * @property integer $roomID
 * @property integer $bookID
 * @property string $cityCode
 * @property integer $stars
 * @property integer $daysBeforeCheckin
 * @property string $discountAmount
 * @property string $discountPercent
 * @property string $discountCurrencyCode
 * @property integer $userID
 * @property integer $corpID
 * @property string $roomTypeSentence
 * @property string $attachmentURL
 * @property string $confirmationDate
 * @property integer $checkIn
 * @property integer $checkOut
 * @property string $realCheckInDateTime
 * @property string $realCheckOutDateTime
 * @property integer $ReservationIdContentinn
 * @property string $cancelDeclineReason
 * @property integer $penalty
 * @property integer $netAmount
 * @property integer $payment_type
 */
class HReservationConfirmed extends ActiveRecord
{
    public static $db;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_reservations_confirmed';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get(self::$db);
    }

    /**
     * The setDb set connection database name
     * @param $db_name
     * @return bool
     */
    public static function setDb($db_name)
    {
        $status = true;
        if (empty($db_name)) {
            $status = false;
        } else {
            self::$db = 'db_' . trim($db_name);
        }
        return $status;
    }

    /**
     * This search return static method find
     * @param $db_name
     * @return \yii\db\ActiveQuery
     */
    public static function search($db_name)
    {
        self::setDb($db_name);

        return static::find();
    }


    public function getHotel()
    {
        return $this->hasOne(HHotels::className(), ['id' => 'hotelID']);
    }

    public function getRoom()
    {
        return $this->hasOne(HRooms::className(), ['id' => 'roomID']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isCancelled', 'toCancel', 'hotelID', 'roomID', 'bookID', 'stars', 'daysBeforeCheckin', 'userID', 'corpID', 'checkIn', 'checkOut', 'ReservationIdContentinn', 'penalty', 'netAmount', 'payment_type'], 'integer'],
            [['rules_cancelPenalty', 'rules_extraBedAndChilds', 'rules_internet', 'rules_parking', 'rules_animals', 'rules_additionalServices'], 'string'],
            [['confirmationDate', 'realCheckInDateTime', 'realCheckOutDateTime'], 'safe'],
            [['hotel_bookingReference', 'sendTo', 'hotel_Name', 'hotel_roomType', 'hotel_address', 'hotel_latitude', 'hotel_longitude', 'hotel_phone', 'hotel_email', 'checkInDateTime', 'checkOutDateTime', 'guest', 'tripDepartments', 'price_fare', 'tripReasons', 'price_VAT', 'price_totalPrice', 'cityCode', 'discountAmount', 'discountPercent', 'discountCurrencyCode', 'roomTypeSentence', 'attachmentURL'], 'string', 'max' => 200],
            [['cancelDeclineReason'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_bookingReference' => 'Hotel Booking Reference',
            'isCancelled' => 'Is Cancelled',
            'toCancel' => 'To Cancel',
            'sendTo' => 'Send To',
            'hotel_Name' => 'Hotel  Name',
            'hotel_roomType' => 'Hotel Room Type',
            'hotel_address' => 'Hotel Address',
            'hotel_latitude' => 'Hotel Latitude',
            'hotel_longitude' => 'Hotel Longitude',
            'hotel_phone' => 'Hotel Phone',
            'hotel_email' => 'Hotel Email',
            'checkInDateTime' => 'Check In Date Time',
            'checkOutDateTime' => 'Check Out Date Time',
            'guest' => 'Guest',
            'tripDepartments' => 'Trip Departments',
            'price_fare' => 'Price Fare',
            'tripReasons' => 'Trip Reasons',
            'price_VAT' => 'Price  Vat',
            'price_totalPrice' => 'Price Total Price',
            'rules_cancelPenalty' => 'Rules Cancel Penalty',
            'rules_extraBedAndChilds' => 'Rules Extra Bed And Childs',
            'rules_internet' => 'Rules Internet',
            'rules_parking' => 'Rules Parking',
            'rules_animals' => 'Rules Animals',
            'rules_additionalServices' => 'Rules Additional Services',
            'hotelID' => 'Hotel ID',
            'roomID' => 'Room ID',
            'bookID' => 'Book ID',
            'cityCode' => 'City Code',
            'stars' => 'Stars',
            'daysBeforeCheckin' => 'Days Before Checkin',
            'discountAmount' => 'Discount Amount',
            'discountPercent' => 'Discount Percent',
            'discountCurrencyCode' => 'Discount Currency Code',
            'userID' => 'User ID',
            'corpID' => 'Corp ID',
            'roomTypeSentence' => 'Room Type Sentence',
            'attachmentURL' => 'Attachment Url',
            'confirmationDate' => 'Confirmation Date',
            'checkIn' => 'Check In',
            'checkOut' => 'Check Out',
            'realCheckInDateTime' => 'Real Check In Date Time',
            'realCheckOutDateTime' => 'Real Check Out Date Time',
            'ReservationIdContentinn' => 'Reservation Id Contentinn',
            'cancelDeclineReason' => 'Cancel Decline Reason',
            'penalty' => 'Penalty',
            'netAmount' => 'Net Amount',
            'payment_type' => 'Payment Type',
        ];
    }
}
