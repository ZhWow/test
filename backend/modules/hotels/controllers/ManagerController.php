<?php

namespace backend\modules\hotels\controllers;

use backend\modules\integra\models\TUsers;
use yii\web\Controller;
use backend\modules\hotels\models\ReservationsConfirmed;
use yii\data\ArrayDataProvider;
use backend\modules\hotels\models\HHotels;
use Yii;
use backend\modules\hotels\models\HRules;
use backend\modules\hotels\models\HServices;
use backend\modules\hotels\models\HBook;
use backend\models\Corp;
use backend\modules\hotels\models\ResolveForm;

class ManagerController extends Controller
{
    public function actionIndex()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        return $this->render('index', [
            'hotelID' => $tUser->hotelID
        ]);
    }

    public function actionConfirm()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        $allModel = ReservationsConfirmed::confirmAll($tUser->hotelID);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('confirm', ['dataProvider' => $dataProvider]);
    }

    public function actionToCanceled()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        $allModel = ReservationsConfirmed::canceledAll($tUser->hotelID);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('to-canceled', ['dataProvider' => $dataProvider]);
    }

    public function actionReport()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        $allModel = ReservationsConfirmed::report($tUser->hotelID);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('report', ['dataProvider' => $dataProvider]);
    }

    public function actionEdit()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);

        return $this->render('edit', ['hotelID' => $tUser->hotelID]);
    }

    public function actionEditMain()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $model = HHotels::findOne(['id' => $tUser->hotelID]);
        $model->load(Yii::$app->request->post());
        $model->save();
        return $this->redirect('/hotels/manager/edit');
    }

    public function actionResolve($id)
    {
        $resolveForm = new ResolveForm();

        if ($resolveForm->load(Yii::$app->request->post())) {

            if ($resolveForm->action === 'confirm') {
                $penalty = $resolveForm->penalty ? $resolveForm->penalty : 0;
                $corp = Corp::findOne($resolveForm->corp);
                $hBook = HBook::findCondition($id, $corp['base']);
                ReservationsConfirmed::record($hBook, $corp['base'], $penalty);
                HBook::checkStateIdAndToBuy($id, $corp['base'], 2, 6);
            }
            if ($resolveForm->action === 'cancel') {
                $penalty = $resolveForm->penalty ? $resolveForm->penalty : 0;
                $corp = Corp::findOne($resolveForm->corp);
                $hBook = HBook::findCondition($id, $corp['base']);
                ReservationsConfirmed::record($hBook, $corp['base'], $penalty, true);
                HBook::checkStateIdAndToBuy($id, $corp['base'], 2, 4);
            }

        }
        return $this->redirect('/hotels/manager');
    }

    public function actionEditRulesCreate()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $model = new HRules();
        $model->load(Yii::$app->request->post());
        $model->hotelID = $tUser->hotelID;
        $model->save();
        return $this->redirect('/hotels/manager/edit');
    }

    public function actionEditRulesUpdate()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $model = HRules::findOne(['hotelId' => $tUser->hotelID]);
        $model->load(Yii::$app->request->post());
        $model->save();
        return $this->redirect('/hotels/manager/edit');
    }

    public function actionEditServiceCreate()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $model = new HServices();
        $model->load(Yii::$app->request->post());
        $model->hotelID = $tUser->hotelID;
        $model->save();
        return $this->redirect('/hotels/manager/edit');
    }

    public function actionEditServiceUpdate()
    {
        $tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
        $model = HServices::findOne(['hotelId' => $tUser->hotelID]);
        $model->load(Yii::$app->request->post());
        $model->save();
        return $this->redirect('/hotels/manager/edit');
    }
	
	public function actionCheckIn($id, $corp)
	{
		$corp = Corp::findOne($corp);
		$sqlQuery = 'UPDATE h_reservations_confirmed SET realCheckInDateTime = NOW(), checkIn = 1 WHERE id = '.$id;
		Yii::$app->{'db_' . $corp->base}
            ->createCommand($sqlQuery)
            ->execute();
			
		return $this->redirect('/hotels/manager/confirm/');
	}
	
	public function actionCheckOut($id, $corp)
	{
		$corp = Corp::findOne($corp);
		$sqlQuery = 'UPDATE h_reservations_confirmed SET realCheckOutDateTime = NOW(), checkOut = 1 WHERE id = '.$id;
		Yii::$app->{'db_' . $corp->base}
            ->createCommand($sqlQuery)
            ->execute();
			
		return $this->redirect('/hotels/manager/confirm/');
	}
}