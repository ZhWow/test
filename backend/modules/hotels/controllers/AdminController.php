<?php


namespace backend\modules\hotels\controllers;

use backend\models\Corp;
use backend\modules\hotels\models\base\HAvailabilityPOST;
use backend\modules\hotels\models\HAvailability;
use backend\modules\hotels\models\HHotels;
use backend\modules\hotels\models\HImagehotel;
use backend\modules\hotels\models\HRooms;
use backend\modules\hotels\models\HRoomsSearch;
use backend\modules\hotels\models\HRules;
use backend\modules\hotels\models\HServices;
use backend\modules\hotels\models\HSpecial_fare;
use backend\modules\hotels\models\TUsers;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;


class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'edit-fare' => ['POST','GET'],
                    'create-fare' => ['POST','GET'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('rooms');
    }

    public function actionDeleteRoom()
    {
        if( Yii::$app->request->get() ) {
            $roomId = Yii::$app->request->get()["roomId"];
            $hotelID = Yii::$app->request->get()["hotelID"];

            if( HRooms::findOne($roomId)->delete() !== false ) {
                $images = HImagehotel::find()->where(["roomId" => $roomId])->all();
                foreach ($images as $image) {
                    if ( unlink(dirname(getcwd()) . '/../../manageit/hotels/' . $hotelID . '/' . $image->imageName) ) {
                        $image->delete();
                    }
                }
            }
        }

        return $this->redirect('/hotels/admin/rooms');
    }

    public function actionCreateRoom()
    {
        if ( Yii::$app->request->post() ) {
            $post = Yii::$app->request->post();
            $hotelID = $post["Hotel"];

            $model = new HRooms();

            if ($model->load(Yii::$app->request->post()) && $model->uploadByAdmin($hotelID)) {
                return $this->redirect('/hotels/admin/rooms');
            } else {
                return $this->render('create-room');
            }
        }else{
            return $this->render('create-room');
        }
    }

    public function actionGetHotelByCountry()
    {
        $response = NULL;

        if ( Yii::$app->request->get() ) {
            $countryCode = Yii::$app->request->get()["country"];
            $hotels = HHotels::find()->where(['countryIso' => $countryCode])->select(['id','hotelName'])->orderBy('hotelName')->asArray()->all();
            $response = json_encode($hotels);
        }


        return $response;
    }

    public function actionRooms()
    {
//        $allModel = TUsers::find()->where(['role' => 4])->all();
        if ( Yii::$app->request->get() && !empty(Yii::$app->request->get()["hotelID"]) ) {
            $rooms = HRooms::find()->where(["hotelID" => Yii::$app->request->get()["hotelID"]])->all();
        } else {
            $rooms = HRooms::find()->all();
        }

        $activeHotels = HRooms::find()->select(['hotelID'])->distinct()->all();

        $searchModel = new HRoomsSearch();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $rooms,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('rooms',['dataProvider' => $dataProvider,'activeHotels' => $activeHotels]);
    }

    public function actionFares()
    {
        return $this->render('fares');
    }

    public function actionFaresForRoom()
    {
        $roomId = Yii::$app->request->get()["roomId"];
        $hotelID = Yii::$app->request->get()["hotelID"];
        $availability = HAvailability::find()->where(["roomID" => $roomId, 'hotelID' => $hotelID])->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $availability,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('fares-for-room', ['dataProvider' => $dataProvider,'roomId' => $roomId, 'hotelID' => $hotelID]);
    }

    public function actionCreateFare()
    {
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post();
            $hotel = HHotels::findOne($post["hotelID"]);
            $room = HRooms::findOne($post["roomId"]);
            $availability = new HAvailability();
            $availability->cityID = $hotel->cityID;
            $availability->cityCode = $hotel->city->IataCode;
            $availability->hotelID = $post["hotelID"];
            $availability->roomID = $post["roomId"];
            $availability->roomNum = $post["roomNum"];
            $availability->paxNum = $post["paxNum"];
            $availability->roomType = $room->Type;
            $availability->startDate = $post["startDate"];
            $availability->endDate = $post["endDate"];
            $availability->Amount = $post["netAmount"];
            $availability->NetAmount = $post["netAmount"];
            $availability->PenaltyRefundAmount = 0;
            $availability->DaysForAutoCancel = 0;
            $availability->Currency = $post["currency"];
            $availability->range_date = $post["startDate"]."to".$post["endDate"];
            $availability->GUID = $availability->getGuid();
            if($availability->validate()){
                $availability->save();
                return $this->redirect(['/hotels/admin/fares-for-room?roomId='.Yii::$app->request->get()["roomId"].'&hotelID='.Yii::$app->request->get()["hotelID"]]);
            }
        }

        $roomId = Yii::$app->request->get()["roomId"];
        $hotelID = Yii::$app->request->get()["hotelID"];

        return $this->render('create-fare',['roomId' => $roomId,'hotelID'=>$hotelID]);
    }

    public function actionEditFare()
    {
        $fareId = Yii::$app->request->get()["fareId"];
        $special_fare_id = NULL;
        $availability = HAvailability::findOne($fareId);

        if(Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $availabilityPost = HAvailabilityPOST::findOne($fareId);
            $availabilityPost->startDate = $post["startDate"];
            $availabilityPost->endDate = $post["endDate"];
            $availabilityPost->NetAmount = $post["netAmount"];
            $availabilityPost->save();

            if(!empty($post["special_fare_id"])) {
                $special_fare_id = $post["special_fare_id"];
                HSpecial_fare::deleteAll(["fare_id" => $special_fare_id]);
            }

            if( !empty($post["specialFare"]) && count($post["specialFare"]) > 0 ) {
                foreach ($post["specialFare"] as $specialFareCorp) {

                    $specialFare = new HSpecial_fare();
                    $specialFare->fare_id = $special_fare_id;
                    $specialFare->corp_id = $specialFareCorp;
                    $specialFare->save();

                }
            }

            $availability = HAvailability::findOne($fareId);
        }


        $corps = Corp::find()->all();
        return $this->render('edit-fare',['availability' => $availability,'corps' => $corps]);
    }

    public function actionDeleteFare()
    {
        $fareId = Yii::$app->request->get()["fareId"];
        HAvailability::findOne($fareId)->delete();
        return $this->redirect(['/hotels/admin/fares-for-room?roomId='.Yii::$app->request->get()["roomId"].'&hotelID='.Yii::$app->request->get()["hotelID"]]);
    }

    public function actionShowSpecialFare()
    {
        $fareId = Yii::$app->request->get()["fareId"];
        $availability = HAvailability::find()->where(["id" => $fareId])->all();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $availability,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('show-special-fare',['dataProvider' => $dataProvider]);
    }

    public function actionHotelServices()
    {
        if(Yii::$app->request->post()) {
            $HServices = HServices::findOne(['hotelID' => YII::$app->request->get()["hotelID"]]);
            if($HServices->load(Yii::$app->request->post()) && $HServices->update()) {
//                $asd = '123';
            }
        }

        $HServices = HServices::findOne(['hotelID' => YII::$app->request->get()["hotelID"]]);
        return $this->render('hotel-services',['model' => $HServices]);
    }

    public function actionHotelRules()
    {
        $HRules = HRules::findOne(['hotelID' => YII::$app->request->get()["hotelID"]]);

        if(Yii::$app->request->post()) {
            if($HRules->load(Yii::$app->request->post()) && $HRules->update()) {
//                $asd = '123';
            }
        }

        return $this->render('hotel-rules',['model' => $HRules]);
    }

    public function actionHotelInfo()
    {

        if(YII::$app->request->post()) {
            $hotel = new HHotels();
            $post = YII::$app->request->post();
            if ( $hotel->load(YII::$app->request->post()) && $hotel->save() ) {
                return $this->render('hotel-info',['model' => $hotel]);
            }
        } else {
            $rooms = HRooms::find()->all();
            $searchModel = new HRoomsSearch();

            $dataProvider = new ArrayDataProvider([
                'allModels' => $rooms,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            if($hotelID = YII::$app->request->get()) {
                $model = HHotels::findOne(YII::$app->request->get()["hotelID"]);
                return $this->render('hotel-info',['model' => $model]);
            } else {
                return $this->render('rooms',['dataProvider' => $dataProvider,'searchModel' => $searchModel]);
            }
        }


    }

    public function beforeAction($action)
    {
        if ($action->id === 'edit-fare') {
            $this->enableCsrfValidation = false;
        }

        if ($action->id === 'create-fare') {
            $this->enableCsrfValidation = false;
        }

        if ($action->id === 'hotel-info') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}