<?php

namespace backend\modules\hotels\controllers;

use backend\models\TCorpTUsers;
use Yii;
use backend\modules\hotels\models\HAvailability;
use backend\modules\hotels\models\SearchHAvailability;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Corp;
use backend\modules\hotels\models\HSpecial_fare;

/**
 * AvailabilityController implements the CRUD actions for HAvailability model.
 */
class AvailabilityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HAvailability models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchHAvailability();
        $a = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HAvailability model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'corp_list' => $this->getCorpList($id,'str'),
        ]);
    }

    /**
     * Creates a new HAvailability model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HAvailability();
        $post = [];
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post("HAvailability");
            if ($post['corp_ids'] != "") {
                $corp_ids = explode(";",$post['corp_ids']);
                $special_fare_model = new HSpecial_fare();
                $max_id = $special_fare_model->getMaxFareId()+1;
                foreach ($corp_ids as $corp_id) {
                    if ($corp_id != '') {
                        $sql = "INSERT INTO h_special_fares SET fare_id=$max_id, corp_id=$corp_id";
                        $command = Yii::$app->db_manager->createCommand($sql);
                        $command->execute();
                    }
                }
                $model->special_fare_id = $max_id;
            }
            if (!$model->save()) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            } else {
                return $this->redirect('/hotels/availability');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HAvailability model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post = Yii::$app->request->post();
            $a = 1;
            $this->updSpecFares($id,$post);
            $model->special_fare_id = HSpecial_fare::getMaxFareId();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $a = $this->getCorpList($id,'array');
            return $this->render('update', [
                'model' => $model,
                'corp_array' => $a,
            ]);
        }
    }

    /**
     * Deletes an existing HAvailability model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HAvailability model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HAvailability the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HAvailability::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getCorpList($id,$type)
    {
        /*
         * $id = h_availability.id
         */
        $fare_id_query = HAvailability::find()
                                    ->select('special_fare_id')
                                    ->where(['id' => $id])
                                    ->one();
        $fare_id = $fare_id_query->special_fare_id;
        $corps = HSpecial_fare::find()
                                ->select('h_special_fares.*')
                                ->where(['fare_id' => $fare_id])
                                ->with('corpName')
                                ->asArray()
                                ->all();
        if ($type == 'str') {
            $corp_list_str = '';
            if (!$corps) {
                $corp_list_str = 'Для всех!';
            }
            $count = count($corps);
            foreach ($corps as $key => $corp) {
                $corp_list_str .= $corp['corpName']['company_ru'];
                if ($key != $count - 1) {
                    $corp_list_str .= ', ';
                } else {
                    $corp_list_str .= '!';
                }
            }
            return $corp_list_str;
        }
        return $corps;
    }


    public function actionGetCorpName()
    {
        $res = Corp::find()
            ->select(['company_ru as label', 'id'])
            ->andFilterWhere(['like', 'company_ru', Yii::$app->request->get('term')])
            ->asArray()
            ->all();
        return json_encode($res, JSON_UNESCAPED_UNICODE);
    }

    private function updSpecFares($id,$post)
    {
        $corp_id_arr = explode(";",$post['HAvailability']['corp_ids']);

        if ($post['HAvailability']['special_fare_id'] == '') {
            $fare_id = HSpecial_fare::getMaxFareId()+1;

        } else {
            $fare_id = $post['HAvailability']['special_fare_id'];
        }
        HSpecial_fare::deleteAll(['fare_id' => $fare_id]);
        $max_id = HSpecial_fare::getMaxFareId()+1;
        foreach ($corp_id_arr as $corp_id) {
            if ($corp_id != '') {
                $sql = "INSERT INTO h_special_fares SET fare_id=$max_id, corp_id=$corp_id";
                $command = Yii::$app->db_manager->createCommand($sql);
                $command->execute();
            }
        }

    }
}
