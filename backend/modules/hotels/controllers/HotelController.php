<?php

namespace backend\modules\hotels\controllers;

use backend\modules\hotels\models\ApproveHotelForm;
use backend\modules\hotels\models\HBook;
use backend\modules\hotels\models\HHotels;
use backend\modules\hotels\models\ReservationsConfirmed;
use backend\modules\hotels\models\ResolveForm;
use backend\modules\integra\models\TUsers;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use Yii;
use backend\models\Corp;
use modules\users\common\models\User;

class HotelController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index'/*, ['dataProvider' => $dataProvider]*/);
    }

    public function actionResolve($id)
    {
        $resolveForm = new ResolveForm();

        if ($resolveForm->load(Yii::$app->request->post())) {

            if ($resolveForm->action === 'confirm') {
                $penalty = $resolveForm->penalty ? $resolveForm->penalty : 0;
                $corp = Corp::findOne($resolveForm->corp);
                $hBook = HBook::findCondition($id, $corp['base']);
                ReservationsConfirmed::record($hBook, $corp['base'], $penalty);
                HBook::checkStateIdAndToBuy($id, $corp['base'], 2, 6);
            }
            if ($resolveForm->action === 'cancel') {
                $penalty = $resolveForm->penalty ? $resolveForm->penalty : 0;
                $corp = Corp::findOne($resolveForm->corp);
                $hBook = HBook::findCondition($id, $corp['base']);
                ReservationsConfirmed::record($hBook, $corp['base'], $penalty, true);
                HBook::checkStateIdAndToBuy($id, $corp['base'], 2, 4);
            }

        }
        return $this->redirect('/hotels');
    }

    public function actionConfirm()
    {
        $allModel = ReservationsConfirmed::confirmAll();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('confirm', ['dataProvider' => $dataProvider]);
    }

    public function actionToCanceled()
    {
        $allModel = ReservationsConfirmed::canceledAll();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('to-canceled', ['dataProvider' => $dataProvider]);
    }

    public function actionHotelsList()
    {

        $allModel = TUsers::find()->where(['role' => 4, 'hotelId' => 0])->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModel,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('hotel-list', ['dataProvider' => $dataProvider]);
    }

    public function actionApproveHotel($id = false, $t_user =false)
    {
        $approveForm = new ApproveHotelForm();

        if ($approveForm->load(Yii::$app->request->post())) {

            $tUser = TUsers::findOne($t_user);
            $tUser->hotelID = $approveForm->hotel_id;
            $tUser->save();

            $user = \common\models\User::findOne($id);

            if ($user->status === User::STATUS_HOTEL_BEFORE_APPLY) {
                $user->status = User::STATUS_HOTEL;
                $user->save();
            }

            $manager = Yii::$app->getAuthManager();
            $their = $manager->getRole('their');
            $manager->assign($their, $id);
            $hotel = $manager->getRole('hotel');
            $manager->assign($hotel, $id);

            return $this->redirect('/hotels/hotel/hotels-list');
        } else {

            return $this->render('approve-hotel', [
                'model' => $approveForm,
                'userId' =>$id,
                'tUserId' => $t_user,
            ]);
        }


    }

    public function actionGetHotelName()
    {
        $res = HHotels::find()
            ->select(['hotelName as label', 'id as id', 'countryIso as country'])
            ->andFilterWhere(['like', 'hotelName', Yii::$app->request->get('term')])
            ->asArray()
            ->all();
        foreach ( $res as $key => $val ){
            $res[$key]['label'] = $res[$key]['label'] . ' ('.$res[$key]['country'] . ')';
        }
        return json_encode($res);
    }

}