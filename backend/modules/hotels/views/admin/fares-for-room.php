<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\hotels\models\SearchHRooms */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $roomId  */
/* @var $hotelID  */

$this->title = Yii::t('app', 'Fares For Room');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="hrooms-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Fare'), ['create-fare?roomId='.$roomId.'&hotelID='.$hotelID], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    //        'filterModel' => $searchModel,
        'options' => ['style' => 'font-size:12px;'],
        'rowOptions' => function($model) {
            if(strtotime($model->endDate) - time() < 0){
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'startDate',
            'endDate',
            [
                'attribute' => 'Net Amount',
                'value' => function($model) {
                    return $model->NetAmount  . " " . $model->Currency;
                }
            ],
            [
                'attribute' => 'PenaltyRefundAmount',
                'value' => function($model) {
                    return number_format($model->PenaltyRefundAmount,0,' ', ' ') . " " . $model->Currency;
                }
            ],
            'DaysForAutoCancel',
            [
                'attribute' => 'Action',
                'format' => 'html',
                'value' => function($model) use ($roomId, $hotelID) {
                    $buttons = NULL;
                    $buttons .= Html::a('<i class="fa fa-fw fa-edit"></i> Edit',['admin/edit-fare?fareId='.$model->id], ['class' => 'btn btn-sm btn-black', 'title' => 'Edit']) . "<br>";
                    $buttons .= Html::a('<i class="fa fa-fw fa-edit"></i> Delete',['admin/delete-fare?fareId='.$model->id.'&roomId='.$roomId.'&hotelID='.$hotelID], ['class' => 'btn btn-sm btn-black', 'title' => 'Delete']) . "<br>";

                    if($model->special_fare_id != null) {
                        $buttons .= '<span>Special Fare</span>';
                    }

                    return $buttons;
                }
            ]
        ],
    ]); ?>
</div>