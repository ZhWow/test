<?php
use \kartik\form\ActiveForm;
use yii\helpers\Html;
/* @var $model backend\modules\hotels\models\HHotels */

$this->title = Yii::t('app', 'Hotel Services');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="hrooms-index">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'general')->textarea()  ?>
    <?= $form->field($model, 'facilities')->textarea()  ?>
    <?= $form->field($model, 'mealDrinks')->textarea()  ?>
    <?= $form->field($model, 'health')->textarea()  ?>
    <?= $form->field($model, 'transfer')->textarea()  ?>
    <?= $form->field($model, 'reception')->textarea()  ?>
    <?= $form->field($model, 'cleaning')->textarea()  ?>
    <?= $form->field($model, 'services')->textarea()  ?>
    <?= $form->field($model, 'other')->textarea()  ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

