<?php
use \kartik\form\ActiveForm;
use yii\helpers\Html;
/* @var $model backend\modules\hotels\models\HHotels */

$this->title = Yii::t('app', 'Hotel Rules');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="hrooms-index">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cancelPenalty')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'extraBedAndChilds')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'internet')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'parking')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'animals')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'additionalServices')->textInput(['maxlength' => true])  ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


