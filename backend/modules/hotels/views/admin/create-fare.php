<?php

/* @var $roomId  */
/* @var $hotelID  */

$this->title = Yii::t('app', 'Create Fare');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="hrooms-index">

    <form action="" method="post">
        <div class="form-group">
            <label for="roomNum">Number of rooms:</label>
            <input type="text" name="roomNum" class="form-control" id="roomNum">
        </div>
        <div class="form-group">
            <label for="paxNum">Number of guests allowed:</label>
            <input type="text" name="paxNum" class="form-control" id="paxNum">
        </div>
        <div class="form-group">
            <label for="startDate">Start Date:</label>
            <input type="text" name="startDate" class="form-control" id="startDate">
        </div>
        <div class="form-group">
            <label for="endDate">End Date:</label>
            <input type="text" name="endDate" class="form-control" id="endDate">
        </div>
        <div class="form-group">
            <label for="netAmount">Amount:</label>
            <input type="text" name="netAmount" class="form-control" id="netAmount">
        </div>
        <div class="form-group">
            <label for="currency">Currency:</label>
            <input type="text" name="currency" class="form-control" id="currency" value="KZT">
        </div>
        <input type="hidden" name="roomId" value="<?=$roomId?>">
        <input type="hidden" name="hotelID" value="<?=$hotelID?>">
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>

