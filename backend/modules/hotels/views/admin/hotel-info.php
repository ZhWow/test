<?php
use \kartik\form\ActiveForm;
use yii\helpers\Html;
/* @var $model backend\modules\hotels\models\HHotels */

$this->title = Yii::t('app', 'Hotel Info');
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a(Yii::t('app', 'Services'), ['hotel-services?hotelID='.$model->id], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Rules'), ['hotel-rules?hotelID='.$model->id], ['class' => 'btn btn-success']) ?>
</p>

<div class="hrooms-index">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hotelName')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'fax')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true])  ?>
    <?= $form->field($model, 'description')->textarea()  ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>