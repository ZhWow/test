<?php
use \kartik\form\ActiveForm;
use \backend\modules\hotels\models\HRooms;
use \kartik\file\FileInput;
//use Yii;

use yii\helpers\Html;
$this->title = Yii::t('app', 'Create Fare');
$this->params['breadcrumbs'][] = $this->title;

$model = new HRooms();
$countries = \backend\modules\hotels\models\HHotels::find()->distinct(['countryIso'])->select(['countryIso'])->orderBy('countryIso')->asArray()->all();
$tempJS = <<<JS
    $("#country").on("change", function() {
        var selectedCountry = $(this).val();
        $.ajax({
            url: '/hotels/admin/get-hotel-by-country?country=' + selectedCountry,
            method: 'GET',
            beforeSend: function(){
                $("#hotelList").html("<option>Please wait...</option>");
            },
            success: function (response) {
                var responseObj = JSON.parse(response);
                $("#hotelList").html("");
                $.each(responseObj, function(i, item) {
                    $("#hotelList").append($("<option></option>")
                        .attr('value', item.id)
                        .text(item.hotelName)
                    )
                });
            }
        });
    })
    
    $("#hotelList").on("change", function() {
        var hotelID = $(this).val();
        $("#hrooms-hotelid").val(hotelID);
    });
JS;

$this->registerJs($tempJS);

?>

<div class="hrooms-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="form-group field-hrooms-typedescription has-success">
        <label class="control-label" for="country">Hotel Country:</label>

        <select id="country" class="form-control" name="country">

            <option value="">Select</option>

            <?php foreach($countries as $country) : ?>
                <option value="<?= $country["countryIso"] ?>"><?= $country["countryIso"] ?></option>
            <?php endforeach; ?>

        </select>

        <div class="help-block"></div>

    </div>

    <div class="form-group field-hrooms-typedescription has-success">
        <label class="control-label" for="Hotel">Hotel:</label>

        <select class="form-control" id="hotelList" name="Hotel">
            <option value="">Select Country...</option>
        </select>

        <div class="help-block"></div>

    </div>

    <?= $form->field($model, 'Type')->dropDownList(\backend\modules\hotels\models\HRooms::getType())  ?>

    <?= $form->field($model, 'TypeDescription')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Bedsnum')->textInput() ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CheckInTime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CheckOutTime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RoomDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hotelID')->hiddenInput(['value' => ''])->label(false) ?>

    <?= $form->field($model, 'images[]')->widget(FileInput::classname(), [
        //'name' => 'attachment_48[]',
        'options'=>[
            'multiple' => true,
            'accept' => 'image/jpg, image/jpeg, image/png, image/gif',

        ],
        'pluginOptions' => [
            'showUpload' => false,
            'maxFileCount' => 10
        ]
    ]) ?>






    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
