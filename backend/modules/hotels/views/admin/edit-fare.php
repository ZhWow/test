<?php

/* @var $this yii\web\View */
/* @var $availability backend\modules\hotels\models\HAvailability */
/* @var $corps backend\models\Corp */

$this->title = Yii::t('app', 'Edit Fare');
$this->params['breadcrumbs'][] = $this->title;
$specialFares = $availability->specialFares;
$selected = NULL;
$specialFareIdForHidden = NULL;
?>
<div class="hrooms-index">

    <form action="" method="post">
        <div class="form-group">
            <label for="startDate">Start Date:</label>
            <input type="text" name="startDate" class="form-control" id="startDate" value="<?=$availability->startDate?>">
        </div>
        <div class="form-group">
            <label for="endDate">End Date:</label>
            <input type="text" name="endDate" class="form-control" id="endDate" value="<?=$availability->endDate?>">
        </div>
        <div class="form-group">
            <label for="netAmount">Amount:</label>
            <input type="text" name="netAmount" class="form-control" id="netAmount" value="<?=$availability->NetAmount?>">
        </div>
        <div class="form-group">
            <label for="specialFare">SpecialFare:</label>
            <select class="form-control" multiple size="<?= count($corps) ?>" name="specialFare[]">
                <?php foreach ($corps as $corp) : ?>
                    <?php foreach($specialFares as $specialFare) : ?>
                        <?php if($corp->id == $specialFare->corp_id) {
                            $specialFareIdForHidden = $specialFare->fare_id;
                            $selected = "selected";
                            break;
                        } else {
                            $selected = "";
                        }
                        ?>
                    <?php endforeach; ?>
                    <option value="<?=$corp->id?>" <?= $selected ?>><?=$corp->company?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <input type="hidden" name="special_fare_id" value="<?=$specialFareIdForHidden?>">
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>

