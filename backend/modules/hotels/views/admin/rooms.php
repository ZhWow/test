<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $activeHotels backend\modules\hotels\models\HRooms; */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rooms');
$this->params['breadcrumbs'][] = $this->title;

$tempJS = <<<JS
    $("#hotelList").on("change", function() {
        var hotelID = $(this).val();
        if(hotelID == "all"){
            location.href = "/hotels/admin/rooms"
        } else {
            location.href = "/hotels/admin/rooms?hotelID=" + hotelID;
        }
        
    });
JS;

$this->registerJs($tempJS);
?>
<div class="hrooms-index">

    <div class="row">
        <div class="col-md-1">
            <?= Html::a(Yii::t('app', 'Create Room'), ['create-room'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-2">
            <select class="form-control" id="hotelList">
                <option value="all">Все гостиницы</option>
                <?php foreach ($activeHotels as $activeHotel) : ?>
                    <?php if ($activeHotel->hotel["id"]) : ?>
                        <option value="<?=$activeHotel->hotel["id"]?>"><?=$activeHotel->hotel["hotelName"]?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-9"></div>
    </div>
<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'options' => ['style' => 'font-size:12px;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Hotel',
                'value' => function($model){
                    return $model->hotel->countryIso . " " . $model->hotel->hotelName;
                }
            ],
            [
                'attribute' => 'City',
                'value' => function($model){
                    return $model->hotel->city->Name;
                }
            ],
            [
                'attribute' => 'Fares',
                'value' => function($model){
                    return count($model->availability);
                }
            ],
            [
                'attribute' => 'Type',
                'value' => function($model){
                    return $model->Type . " " . $model->TypeDescription;
                }
            ],
            [
                'attribute' => 'Beds',
                'value' => function($model){
                    return $model->Bedsnum;
                }
            ],
            'RoomDescription',
            'Name',
            [
                'attribute' => 'CIT',
                'value' => function($model){
                    return $model->CheckInTime;
                }
            ],
            [
                'attribute' => 'COT',
                'value' => function($model){
                    return $model->CheckOutTime;
                }
            ],
            [
                'attribute' => 'Actions',
                'format' => 'html',
                'value' => function($model){
                    $buttons = NULL;
                    $buttons .= Html::a('<i class="fa fa-fw fa-money"></i> Fares',['admin/fares-for-room?roomId='.$model->id.'&hotelID='.$model->hotelID], ['class' => 'btn btn-black', 'title' => 'Fares']) . "<br>";
                    $buttons .= Html::a('<i class="fa fa-fw fa-close"></i> Delete',['admin/delete-room?roomId='.$model->id.'&hotelID='.$model->hotelID], ['class' => 'btn btn-black', 'title' => 'Fares']) . "<br>";
                    $buttons .= Html::a('<i class="fa fa-fw fa-info"></i> Hotel Info',['admin/hotel-info?hotelID='.$model->hotelID], ['class' => 'btn btn-black', 'title' => 'Fares']);

                    return $buttons;
                }
            ]
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
