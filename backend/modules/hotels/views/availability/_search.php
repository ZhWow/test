<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\SearchHAvailability */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="havailability-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cityID') ?>

    <?= $form->field($model, 'cityCode') ?>

    <?= $form->field($model, 'hotelID') ?>

    <?= $form->field($model, 'roomID') ?>

    <?php // echo $form->field($model, 'roomNum') ?>

    <?php // echo $form->field($model, 'paxNum') ?>

    <?php // echo $form->field($model, 'roomType') ?>

    <?php // echo $form->field($model, 'startDate') ?>

    <?php // echo $form->field($model, 'endDate') ?>

    <?php // echo $form->field($model, 'Amount') ?>

    <?php // echo $form->field($model, 'NetAmount') ?>

    <?php // echo $form->field($model, 'PenaltyRefundAmount') ?>

    <?php // echo $form->field($model, 'DaysForAutoCancel') ?>

    <?php // echo $form->field($model, 'Currency') ?>

    <?php // echo $form->field($model, 'GUID') ?>

    <?php // echo $form->field($model, 'ReservationIdContentinn') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
