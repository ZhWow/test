<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HAvailability */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Havailability',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Havailabilities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="havailability-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?
        $a = '';
    ?>
    <?= $this->render('_form', [
        'model' => $model,
        'corp_arrays' => $corp_array,
    ]) ?>

</div>
