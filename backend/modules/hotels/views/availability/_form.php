<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use backend\modules\hotels\models\HAvailability;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
use backend\modules\integra\models\TUsers;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HAvailability */
/* @var $form yii\widgets\ActiveForm */

$tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
$hHotel = \backend\modules\hotels\models\HHotels::findOne($tUser->hotelID);
$hCity = \backend\modules\hotels\models\HCities::findOne(['Id' => $hHotel->cityID]);

$cityCode = $hCity->IataCode ? $hCity->IataCode : $hCity->NearestIataCode;

$JS = <<<JS
    
$('#havailability-roomid').on('change', function() {
    if ($(this).find(':selected').val()) {
        var selectText = $.trim($(this).find(':selected').text().split('-')[1]);        
        $('#havailability-roomtype').val(selectText.split('/')[0]);
    } else {
        $('#havailability-roomtype').val('');
    }
});
$('body').on('click','.del_corp_div',function(){
    var corp_id = $(this).data('corp-id');
    $(".corp_ids").val($(".corp_ids").val().replace(+corp_id+';',''));
    $(this).closest('div').remove();
})
function getCorpDidv(corp_id,corp_name){
    $("<div id='corp_"+corp_id+"' class='corp_div'>"+corp_name+"</div>").insertAfter( ".special_fare_id_inp" );
    $("#corp_"+corp_id).append("<a href='#' class='del_corp_div' data-corp-id='"+corp_id+"'>X</a>");
}
JS;
$CSS = <<<CSS
    .corp_div{
        display:inline-block;
        border:1px solid black;
        margin-top:10px;
        padding:5px 15px 5px 15px;
        border-radius:5px;
        margin-right:5px;
    }
    .del_corp_div{
        margin-left: 5px;
        float: right;
        text-decoration: none;
    }
    .del_corp_div:hover{
        text-decoration: none;
    }

CSS;
$this->registerJs($JS);
$this->registerCss($CSS);
$val = '';
?>

<div class="havailability-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- TODO -->
    <?= $form->field($model, 'special_fare_id')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control special_fare_id_inp',
            'placeholder' => 'Для конкретной корпорации'
        ],
        'clientOptions' => [
            'source' => Url::to(['/hotels/availability/get-corp-name']),
            'autoFill' => true,
            'minLength' => 2,
            '_renderItem' => new JsExpression('function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a><strong>" + item.label + "</strong> / </a>" + ui.item.id)
                    .appendTo( ul );
            }'),
            'select' =>new JsExpression('function( event, ui ) {
                $( event.target ).val( "" );
                $old_var = $(".corp_ids").val();
                $(".corp_ids").val($old_var+ui.item.id+";");
                getCorpDidv(ui.item.id,ui.item.label);
                return false;
            }'),
        ],
    ]) ?>
    <?php if(isset($corp_arrays)):?>
        <div class="form-group">
        <?php foreach ($corp_arrays as $corp):?>
            <div id='corp_"<?=$corp['corpName']['id']?>"' class='corp_div'>
                <?=$corp['corpName']['company_ru']?>
                <a href='#' class='del_corp_div' data-corp-id='<?=$corp['corpName']['id']?>'>X</a>
            </div>
            <?php $val .= $corp['corpName']['id'].";";?>
        <?php endforeach;?>
        </div>
    <?php endif;?>

    <?= $form->field($model, 'roomID')->dropDownList(HAvailability::roomList(), ['prompt' => Yii::t('app', 'Select')]) ?>

    <!-- TODO -->

    <?= $form->field($model, 'roomNum')->textInput() ?>

    <?= $form->field($model, 'paxNum')->textInput() ?>

    <?= $form->field($model, 'range_date', [
        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
        'options' => ['class' => 'drp-container form-group'],
    ])->widget(DateRangePicker::classname(), [
        'useWithAddon' => true,
        'convertFormat'=>true,
        'pluginOptions' => [
            'locale' => [
                'format' => 'd.m.Y',
                'separator' => ' to ',
            ],
        ]
    ]) ?>

    <?= $form->field($model, 'Amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PenaltyRefundAmount')->textInput() ?>

    <?= $form->field($model, 'hotelID')->hiddenInput(['value' => $tUser->hotelID])->label(false) ?>

    <?= $form->field($model, 'cityID')->hiddenInput(['value' => $hHotel->cityID])->label(false) ?>

    <?= $form->field($model, 'cityCode')->hiddenInput(['value' => $cityCode])->label(false) ?>

    <?= $form->field($model, 'Currency')->hiddenInput(['value' => 'KZT'])->label(false) ?>

    <?= $form->field($model, 'roomType')->hiddenInput()->label(false) ?>

    <?php if(!isset($corp_arrays)):?>
        <?= $form->field($model, 'corp_ids')->hiddenInput(['value' => '','class' => 'corp_ids'])->label(false) ?>
        <?php else:?>
        <?= $form->field($model, 'corp_ids')->hiddenInput(['value' => $val,'class' => 'corp_ids'])->label(false) ?>
    <?php endif;?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
