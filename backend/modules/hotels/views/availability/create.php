<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HAvailability */

$this->title = Yii::t('app', 'Create Havailability');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Havailabilities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="havailability-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
