<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HAvailability */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Havailabilities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="havailability-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cityID',
            'cityCode',
            'hotelID',
            'roomID',
            'roomNum',
            'paxNum',
            'roomType',
            'startDate',
            'endDate',
            'Amount',
            'NetAmount',
            'PenaltyRefundAmount',
            'DaysForAutoCancel',
            'Currency',
            'GUID',
            'ReservationIdContentinn',
            [
                'label' => 'Корпорации(для кого действует)',
                'value' => $corp_list,
            ],
        ],
    ]) ?>

</div>
