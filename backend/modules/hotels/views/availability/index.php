<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\hotels\models\SearchHAvailability */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fares');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="havailability-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Fare'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'City',
                'value' => function ( $model ) {
                    return $model->city->Name;
                }
            ],
//            'cityCode',
//            'hotelID',
            [
                'attribute' => 'Room',
                'value' => function ( $model ) {
                    return $model->room->RoomDescription . ' (' . $model->room->Type . ')';
                }
            ],
            [
                'attribute' => 'NetAmount',
                'value' => function ( $model ) {
                    return $model->NetAmount;
                }
            ],
             'roomNum',
             'paxNum',
            // 'roomType',
             'startDate',
             'endDate',
            [
                'attribute' => 'Compnaies',
                'format' => 'html',
                'value' => function($model) {
                    $special_fare_companies = '';
                    if ( $model->special_fare_id != null ) {
                        foreach ($model->specialFares as $key => $specialFare) {
                            $breakpoint = ($key == count($model->specialFares) - 1) ? "" : "<br>";
                            $special_fare_companies .= $specialFare->corpName->company . $breakpoint;
                        }
                    }
                    return $model->special_fare_id == null ? 'Public' : $special_fare_companies;
                }
            ],
            // 'Amount',
            // 'NetAmount',
            // 'PenaltyRefundAmount',
            // 'DaysForAutoCancel',
            // 'Currency',
            // 'GUID',
            // 'ReservationIdContentinn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
