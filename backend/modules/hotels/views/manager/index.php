<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use backend\modules\hotels\models\ResolveForm;
use yii\data\ArrayDataProvider;
use backend\modules\hotels\models\HBook;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$tempJS = <<<JS

$('.resolve-sign-button').click(function (e) {
    e.preventDefault();
    var that = $(this);
    var url = that.attr('href');
    var form = $('#modal-resolve-form');
    var corpId = that.closest('tr').attr('data-corp-id');
    
    form.attr('action', url);
    $('#corp_id').val(corpId);
    
    $('#modal-resolve-signed-hotel').modal('show');
});

$('#modal-resolve-form').submit(function () {
  // $('#resolveform-corp').prop('disabled', true);
  $('.resolve-submit-btn').prop('disabled', true);
});

$('#action_id').on('change', function() {
    if ($(this).val() === 'cancel') {
        $('.field-resolveform-penalty').show();
    } else {
        $('.field-resolveform-penalty').hide();
    }
})

JS;
$this->registerJs($tempJS);

$items = HBook::find(['hotelID' => $hotelID]);

$searchAttributes = [
    'id',
    'paxSurnames',
    'paxNames',
    'corpID',
    'hotelName',
    'stars',
    'checkInDateTime',
    'checkOutDateTime',
    'totalPrice',

];
$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;
    if ($searchAttribute === 'corpID') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $corp = \backend\models\Corp::findOne($model['corpID']);
                if ($corp) {
                    return $corp->company;
                }
                return '';
            },
        ];
    } else {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => $searchAttribute,
        ];

    }
    $items = array_filter($items, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}

?>

    <style>
        .field-resolveform-penalty {
            display: none;
        }
    </style>

    <div class="hotel-index">

        <?= Html::a(Yii::t('app', 'Reset Search'), '/hotels', ['class' => 'btn btn-default']) ?>

        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider([
                'key'=>'id',
                'allModels' => $items,
                'sort' => [
                    'attributes' => $searchAttributes,
                ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'filterModel' => $searchModel,
            'rowOptions'   => function ($model, $index) {
                return [
                    'id' => $model['id'],
                    'data-corp-id' => $model['corpID']
                ];
            },
            'options' => ['style' => 'font-size:12px;'],
            'columns' => array_merge(
                $searchColumns, [
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('hot', 'Resolve'),
                        'template' => '{resolve}',
                        'buttons' => [
                            'resolve' => function ($url, $model) {
                                $button = Html::a('Resolve', $url,['class' => 'resolve-sign-button']);
                                return $button;
                            },
                        ],
                    ],
                ]
            )
        ]); ?>
    </div>

<?php

$resolveForm = new ResolveForm();

Modal::begin([
    'options' => ['id' => 'modal-resolve-signed-hotel'],
]);
?>
    <div id="modal-resolve-content">

        <?php $form = ActiveForm::begin([
            'id' => 'modal-resolve-form',
        ]); ?>

        <?= $form->field($resolveForm, 'action', ['inputOptions' => [
            'class' => 'form-control','id' => 'action_id']
        ])->dropDownList([
            'confirm' => 'Подтвердить',
            'cancel' => 'Отменить',
        ],[
            'prompt' => 'Выберите...'
        ])->label(false) ?>

        <?= $form->field($resolveForm, 'corp')->hiddenInput(['id' => 'corp_id'])->label(false) ?>

        <?= $form->field($resolveForm, 'penalty')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('ait', 'Submit'), ['class' => 'btn btn-success resolve-submit-btn']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php
Modal::end();
