<?php

use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $hotelID int */
?>

<div class="hotel-edit">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Main'),
                'content' => $this->render('edit/main', ['hotelID' => $hotelID]),
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Rules'),
                'content' => $this->render('edit/rules', ['hotelID' => $hotelID]),
            ],
            [
                'label' => Yii::t('app', 'Service'),
                'content' => $this->render('edit/service', ['hotelID' => $hotelID]),
            ],
        ],
    ]) ?>
</div>
