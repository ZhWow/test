<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
?>
<div class="hotel-report">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'label' => Yii::t('app', 'Status'),
                'format' => 'html',
                'value' => function ($model) {
                    /* @var $model \backend\modules\hotels\models\ReservationsConfirmed */
                    if ($model['isCancelled']) {
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                    } else {
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    }
                }
            ],
            'guest',
            'checkInDateTime',
            'checkOutDateTime',
            'price_totalPrice'

        ],
    ]); ?>
</div>