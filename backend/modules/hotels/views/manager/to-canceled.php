<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
?>
<div class="hotel-to-canceled">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'guest',
            'hotel_Name',
            'hotel_bookingReference',
            'checkInDateTime',
            'checkOutDateTime',
            'penalty',

        ],
    ]); ?>
</div>
