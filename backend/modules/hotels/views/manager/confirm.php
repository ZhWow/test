<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
?>
<div class="hotel-confirm">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'guest',
            'hotel_Name',
            'hotel_bookingReference',
            'checkInDateTime',
            'checkOutDateTime',
			[
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Check In/Out'),
                'template' => '{check-in} {check-out}',
                'buttons' => [
                    'check-in' => function ($url, $model) {
						if ($model["checkIn"]) {
							$button = Html::tag('span','<i class="fa fa-sign-in" aria-hidden="true"></i>',['class' => 'resolve-sign-button btn btn-default','disabled' => true]);  
						} else {
							$url .= '&corp=' . $model["corpID"];
							$button = Html::a('<i class="fa fa-sign-in" style="color: green;" aria-hidden="true"></i>', $url,['class' => 'resolve-sign-button btn btn-default']);                      
						}
                        return $button;
                    },
                    'check-out' => function ($url, $model) {
						if ($model["checkOut"] || !$model["checkIn"]) {
							$button = Html::tag('span','<i class="fa fa-sign-out" aria-hidden="true"></i>',['class' => 'resolve-sign-button  btn btn-default', 'disabled' => true]);
						} else {
							$url .= '&corp=' . $model["corpID"];
							$button = Html::a('<i class="fa fa-sign-out" style="color: red;" aria-hidden="true"></i>', $url,['class' => 'resolve-sign-button  btn btn-default']);
						}
                        
                        return $button;
                    },
                ],
            ]
        ],
    ]); ?>
</div>
