<?php
use backend\modules\hotels\models\HRules;

/* @var $this yii\web\View */
/* @var $hotelID int */
/* @var $model \backend\modules\hotels\models\HRules */

$model = HRules::findOne(['hotelId' => $hotelID]);

if (!$model) {
    $model = new HRules();
}

echo $this->render('rules-form', ['model' => $model]);