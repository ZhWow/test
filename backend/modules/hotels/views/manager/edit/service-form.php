<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\hotels\models\HServices */

$action = $model->isNewRecord ? '/hotels/manager/edit-service-create' : '/hotels/manager/edit-service-update';
?>

<div class="hotel-edit-service">
    <?php $form = ActiveForm::begin([
        'method' => 'POST',
        'action' => $action,
    ]); ?>

    <?= $form->field($model, 'general')->textarea() ?>

    <?= $form->field($model, 'facilities')->textarea() ?>

    <?= $form->field($model, 'mealDrinks')->textarea() ?>

    <?= $form->field($model, 'health')->textarea() ?>

    <?= $form->field($model, 'transfer')->textarea() ?>

    <?= $form->field($model, 'reception')->textarea() ?>

    <?= $form->field($model, 'cleaning')->textarea() ?>

    <?= $form->field($model, 'services')->textarea() ?>

    <?= $form->field($model, 'other')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

