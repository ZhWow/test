<?php

use yii\widgets\ActiveForm;
use backend\modules\hotels\models\HHotels;
use yii\helpers\Html;

/* @var $hotelID int */
/* @var $model \backend\modules\hotels\models\HHotels */

$model = HHotels::findOne($hotelID);

?>

<div class="hotel-edit-main">
    <?php $form = ActiveForm::begin([
        'method' => 'POST',
        'action' => '/hotels/manager/edit-main',
    ]); ?>

    <?= $form->field($model, 'hotelName')->textInput() ?>

    <?= $form->field($model, 'address')->textInput() ?>

    <?= $form->field($model, 'telephone')->textInput() ?>

    <?= $form->field($model, 'fax')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
