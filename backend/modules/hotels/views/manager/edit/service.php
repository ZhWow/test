<?php

use backend\modules\hotels\models\HServices;

/* @var $this yii\web\View */
/* @var $hotelID int */
/* @var $model \backend\modules\hotels\models\HServices */

$model = HServices::findOne(['hotelId' => $hotelID]);

if (!$model) {
    $model = new HServices();
}

echo $this->render('service-form', ['model' => $model]);