<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\hotels\models\HHotels */

$action = $model->isNewRecord ? '/hotels/manager/edit-rules-create' : '/hotels/manager/edit-rules-update';
?>

<div class="hotel-edit-rules">
    <?php $form = ActiveForm::begin([
        'method' => 'POST',
        'action' => $action,
    ]); ?>

    <?= $form->field($model, 'cancelPenalty')->textarea() ?>

    <?= $form->field($model, 'extraBedAndChilds')->textarea() ?>

    <?= $form->field($model, 'internet')->textarea() ?>

    <?= $form->field($model, 'parking')->textarea() ?>

    <?= $form->field($model, 'animals')->textarea() ?>

    <?= $form->field($model, 'additionalServices')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

