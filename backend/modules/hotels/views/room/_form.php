<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\integra\models\TUsers;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HRooms */
/* @var $form yii\widgets\ActiveForm */
$tUser = TUsers::findOne(['user_login' => \Yii::$app->user->identity->email]);
$script = <<< JS
    $(document).ready(function () {
        goTime($('#hrooms-checkintime'));
        goTime($('#hrooms-checkouttime'));
    });
    function goTime(element) {
        element.timepicker({
            timeFormat: 'h:mm',
            interval: 60,
            minTime: '24',
            maxTime: '12:00pm',
            defaultTime: '12:00',
            startTime: '12:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    }
JS;
$this->registerJs($script);
?>

<div class="hrooms-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'Type')->dropDownList(\backend\modules\hotels\models\HRooms::getType())  ?>

    <?= $form->field($model, 'TypeDescription')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Bedsnum')->textInput() ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CheckInTime')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'CheckOutTime')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'RoomDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hotelID')->hiddenInput(['value' => $tUser->hotelID])->label(false) ?>

    <?= $form->field($model, 'images[]')->widget(FileInput::classname(), [
        //'name' => 'attachment_48[]',
        'options'=>[
            'multiple' => true,
            'accept' => 'image/jpg, image/jpeg, image/png, image/gif',

        ],
        'pluginOptions' => [
            'showUpload' => false,
            'maxFileCount' => 10
        ]
    ]) ?>






    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>