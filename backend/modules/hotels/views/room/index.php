<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\hotels\models\SearchHRooms */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rooms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrooms-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Room'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered',
            'style' => 'width: 100%',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'id',
            'Type',
            'TypeDescription',
            'Bedsnum',
            [
                'label' => Yii::t('app', 'Description'),
                'format' => 'ntext',
                'value' => 'RoomDescription',
                'contentOptions'=>['style' => 'white-space: normal;']
            ],
            // 'IsSpecialOffer',
            // 'RoomTypeId',
            // 'NeedNationality',
            // 'Name',
            // 'BoardBasisID',
            // 'CancellationPolicyID',
            // 'hotelID',
            // 'accomodationTypeID',
            // 'CheckInTime',
            // 'CheckOutTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
