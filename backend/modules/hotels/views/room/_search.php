<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\SearchHRooms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hrooms-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Type') ?>

    <?= $form->field($model, 'TypeDescription') ?>

    <?= $form->field($model, 'Bedsnum') ?>

    <?= $form->field($model, 'RoomDescription') ?>

    <?php // echo $form->field($model, 'IsSpecialOffer') ?>

    <?php // echo $form->field($model, 'RoomTypeId') ?>

    <?php // echo $form->field($model, 'NeedNationality') ?>

    <?php // echo $form->field($model, 'Name') ?>

    <?php // echo $form->field($model, 'BoardBasisID') ?>

    <?php // echo $form->field($model, 'CancellationPolicyID') ?>

    <?php // echo $form->field($model, 'hotelID') ?>

    <?php // echo $form->field($model, 'accomodationTypeID') ?>

    <?php // echo $form->field($model, 'CheckInTime') ?>

    <?php // echo $form->field($model, 'CheckOutTime') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
