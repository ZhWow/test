<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HRooms */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Hrooms',
]) . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hrooms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="hrooms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
