<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\hotels\models\HRooms */

$this->title = Yii::t('app', 'Create Hrooms');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hrooms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrooms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
