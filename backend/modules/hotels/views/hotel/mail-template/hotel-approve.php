<?php

/* @var $this yii\web\View */
/* @var $data */
?>

<?= $this->render('_before') ?>

    <table class="body-wrap" bgcolor="#f6f6f6">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <!-- content -->
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <p>Здравствуйте, </p>
                                <p>Ваше бронирование успешно было подтверждено Гостиницей</p>
                                <p> Номер брони: <?= $data["bookingReference"] ?> <br>
                                    Гостиница: <?= $data["hotelName"] ?> <br>
                                    Заезд: <?= date("H:i d.m.Y", strtotime($data["checkInDateTime"])) ?> <br>
                                    Выезд: <?= date("H:i d.m.Y", strtotime($data["checkOutDateTime"])) ?> <br>
                                    Стоимость: <?= $data["totalPrice"] ?> <br>
                                </p>

                                <!-- /button --><br /><br />
                                <p>
                                    Данное сообщение было сгенерировано автоматически и не требует ответа. <br />
                                    Спасибо,
                                </p>
                                <p>
                                    <a href="mailto:tickets@btmc.kz">Связаться с нами</a><br />
                                    <a href="http://www.btmc.kz" style="font-size:12px; color:#aaaaaa"> www.btmc.kz</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /content -->
            </td>
            <td></td>
        </tr>
    </table>

<?= $this->render('_after') ?>