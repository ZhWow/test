<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Confirmed');
?>
<div class="hotel-confirm">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'guest',
            'hotel_Name',
            'hotel_bookingReference',
            'checkInDateTime',
            'checkOutDateTime',

        ],
    ]); ?>
</div>
