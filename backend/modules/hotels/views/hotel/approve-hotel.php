<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \backend\modules\hotels\models\ApproveHotelForm */
/* @var $userId int \common\models\User */
/* @var $tUserId int \backend\modules\integra\models\TUsers */

$this->params['breadcrumbs'][] = Yii::t('app', 'Hotels');
?>

<div class="approve-hotel-form">

    <?php $form = ActiveForm::begin([
        'action' => '/hotels/hotel/approve-hotel?id=' . $userId . '&t_user=' . $tUserId,
        'method' => 'POST'
    ]); ?>

    <?= $form->field($model, 'hotel_name')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'placeholder' => 'Поиск по АП...'
        ],
        'clientOptions' => [
            'source' => Url::to(['/hotels/hotel/get-hotel-name']),
            'autoFill'=>true,
            'minLength'=>'0',
            '_renderItem'=>new JsExpression('function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a><strong>" + item.name + "</strong> / </a>" + ui.item.id)
                    .appendTo( ul );
            }'),
            'select' =>new JsExpression('function( event, ui ) {
                $( event.target ).val( ui.item.label );
                $( "#hotel-id" ).val( ui.item.id);
                return false;
            }'),
        ],
    ]) ?>

    <?= $form->field($model, 'hotel_id')->hiddenInput(['id' => 'hotel-id'])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Approve'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
