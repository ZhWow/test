<?php
/* @var $hBook */
/* @var $hreservation */
$imagesFolder = "@backend/modules/hotels/assets/images/voucher/";
?>


<div id="wrap">
    <div class="header">


        <table class="table" style="width: 100%">
            <tr>
                <td width="55%"><img src="<?= Yii::getAlias($imagesFolder) ?>logotip.png" border="0" height="140"/></td>
                <td width="5%"><img src="<?= Yii::getAlias($imagesFolder) ?>hdr_lines.png" border="0" height="150"/></td>
                <td width="40%" align="right"><img src="<?= Yii::getAlias($imagesFolder) ?>hdr_address.png" border="0" height="220"/></td>
            </tr>
        </table>

        <h3>HOTEL VOUCHER / ВАУЧЕР</h3>
        <table class="table" style="width: 100%">
            <tr>
                <td>Номер брони: </td>
                <td><?= $hreservation["hotel_bookingReference"] ?></td>
            </tr>
            <tr>
                <td>Гостиница: </td>
                <td><?= $hreservation["hotel_Name"] ?></td>
            </tr>
            <tr>
                <td>Дата и время заезда: </td>
                <td><?= date("H:i d.m.Y", strtotime($hreservation["checkInDateTime"])) ?></td>
            </tr>
            <tr>
                <td>Дата и время выезда: </td>
                <td><?= date("H:i d.m.Y", strtotime($hreservation["checkOutDateTime"])) ?></td>
            </tr>
            <tr>
                <td>Адрес: </td>
                <td><?= $hreservation["hotel_address"] ?></td>
            </tr>
            <tr>
                <td>Телефон: </td>
                <td><?= $hreservation["hotel_phone"] ?></td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><?= $hreservation["hotel_email"] ?></td>
            </tr>
        </table>

        <h3>BOOKING DETAIL / ДЕТАЛИ БРОНИ</h3>
        <table class="table">
            <tr>
                <td>Гость: </td>
                <td><?= $hreservation["guest"] ?></td>
            </tr>
            <tr>
                <td>Тип номера: </td>
                <td><?= $hreservation["hotel_roomType"] . " / " . $hreservation["roomTypeSentence"] ?></td>
            </tr>
            <tr>
                <td>Дата подтверждения: </td>
                <td><?= $hreservation["confirmationDate"] ?></td>
            </tr>
        </table>

        <h3>CONDITIONS / УСЛОВИЯ</h3>
        <table class="table">
            <tr>
                <td>Правила отмены: </td>
                <td><?= $hreservation["rules_cancelPenalty"] ?></td>
            </tr>
            <tr>
                <td>Интернет: </td>
                <td><?= $hreservation["rules_internet"] ?></td>
            </tr>
            <tr>
                <td>Паркинг: </td>
                <td><?= $hreservation["rules_parking"] ?></td>
            </tr>
            <tr>
                <td>Животные: </td>
                <td><?= $hreservation["rules_animals"] ?></td>
            </tr>
            <tr>
                <td>Дети: </td>
                <td><?= $hreservation["rules_extraBedAndChilds"] ?></td>
            </tr>
            <tr>
                <td>Дополнительно: </td>
                <td><?= $hreservation["rules_additionalServices"] ?></td>
            </tr>
        </table>

    </div>
    <div class="main"></div>
</div>