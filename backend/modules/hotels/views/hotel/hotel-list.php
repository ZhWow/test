<?php

use yii\grid\GridView;
use common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Hotels');
?>
<div class="hotel-confirm">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_login',
            [
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    $user = User::findOne(['email' => $model->user_login]);
                    if ($user) {
                        return Yii::t('adm', User::getStatus($user->status));
                    }
                }
            ],
            'position',
            'phone',
            /*[
                'label' => Yii::t('app', 'Action'),
                'value' => function ($model) {
                    $user = User::findOne(['email' => $model->user_login]);
                    if ($user) {
                        if ($user->status === User::STATUS_HOTEL_BEFORE_APPLY) {

                        } else {
                            return 'ok';
                        }
                    }
                }
            ],*/

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Action'),
                'template' => '{approve-hotel}',
                'buttons' => [
                    'approve-hotel' => function ($url, $model) {

                        $user = User::findOne(['email' => $model->user_login]);

                        if ($user) {
                            $button = Html::a('<span class="glyphicon glyphicon-user"></span>',
                                'approve-hotel?id=' . $user->id . '&t_user=' . $model->ID);
                            if ($user->status === User::STATUS_HOTEL_BEFORE_APPLY) {
                                return $button;
                            } else {
                                return 'ok';
                            }
                        }
                    },
                ],
            ],

        ],
    ]); ?>
</div>
