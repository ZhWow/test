<?php
namespace backend\modules\hotels\services;
use \yii\web\View;
class VoucherHelper extends View
{

    public static function getVoucherHTML($templateName, $data)
    {
        $self = new self();
        return $self->renderFile('@backend/modules/hotels/views/hotel/vouchers/' . $templateName . '.php', $data);
    }

}