<?php
/**
 * Created by PhpStorm.
 * User: karim
 * Date: 07.11.2017
 * Time: 12:05
 */

namespace backend\modules\hotels\services;

use Yii;
use \yii\base\Model;
use kartik\mpdf\Pdf;

class Voucher extends Model
{
    private $path;
    private $template;

    public function __construct(array $config = [])
    {
        $this->path = Yii::getAlias("@backend/modules/hotels/files/vouchers/");
        parent::__construct($config);
    }

    public function getVoucher($hbook,$hreservation)
    {
        $this->template = VoucherHelper::getVoucherHTML('_hotel-voucher',[
            'hbook' => $hbook,
            'hreservation' => $hreservation,
        ]);

        $fileName = $hreservation["hotel_bookingReference"] . "_" . mb_strtoupper($hreservation["hotel_Name"]) . "_" . str_replace(" ","_", $hreservation["guest"]) . ".pdf";
        $this->generate($fileName);

        return $this->path . $fileName;
    }

    public function generate($filename)
    {
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'cssFile' => '@backend/modules/hotels/assets/css/hotel-voucher.css',
            'options' => ['title' => 'BTMC'],
            'methods' => [
                'SetHeader'=>['BTMC'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        $this->savePDF($this->template, $filename, $pdf);
    }

    public function savePDF($content, $filename, Pdf &$pdf)
    {
        $pdf->output($content, $this->path . $filename, Pdf::DEST_FILE);
    }
}