<?php

namespace backend\modules\hotels;

use yii\helpers\ArrayHelper;

/**
 * Hotels module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\hotels\controllers';

    public $defaultRoute = 'hotel';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $localConfig = '/config-local.php';
        if (file_exists(__DIR__ . $localConfig)) {
            \Yii::configure($this, ArrayHelper::merge(
                require(__DIR__ . '/config.php'),
                require(__DIR__ . $localConfig)
            ));
        } else {
            \Yii::configure($this, require(__DIR__ . '/config.php'));
        }
    }
}
