<?php

namespace backend\modules\hotels\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `h_hotels_user`.
 * Has foreign keys to the tables:
 *
 * - `h_hotels`
 * - `user`
 */
class m170726_184216_create_junction_table_for_h_hotels_and_user_tables extends Migration
{
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('h_hotels_user', [
            'h_hotels_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY(h_hotels_id, user_id)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('h_hotels_user');
    }
}
