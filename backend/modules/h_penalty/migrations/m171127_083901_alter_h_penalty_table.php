<?php

namespace backend\modules\h_penalty\migrations;

use yii\db\Migration;

/**
 * Class m171127_083901_alter_h_penalty_table
 */
class m171127_083901_alter_h_penalty_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('h_penalty', 'penalty_time', $this->integer(11)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171127_083901_alter_h_penalty_table cannot be reverted.\n";

        return false;
    }

}
