<?php

namespace backend\modules\h_penalty\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `h_penalty`.
 */
class m170912_091514_create_h_penalty_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('h_penalty', [
            'id' => $this->primaryKey(),
            'hotel_id' => $this->integer()->notNull(),
            'penalty' => $this->double()->notNull(),
            'penalty_time' => $this->string(50)->comment('Time when the penalty begins')->notNull(),
            'status' => $this->smallInteger()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('h_penalty');
    }
}
