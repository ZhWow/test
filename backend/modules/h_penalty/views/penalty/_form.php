<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\AutoCompletePixaBay\RegisterAutoComplete;

/* @var $this yii\web\View */
/* @var $model backend\modules\h_penalty\models\HPenalty */
/* @var $form yii\widgets\ActiveForm */

$bundle = RegisterAutoComplete::register($this);
?>

<div class="hpenalty-form">

    <?php $form = ActiveForm::begin(
        [
            'id' => 'hotelPenaltyForm',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validationUrl' => '/hotel-penalty/penalty/validation',
        ]
    ); ?>

    <?= Html::activeHiddenInput($model, 'hotel_id') ?>

    <?= $form->field($model, 'hotel_name', ['enableAjaxValidation' => true])->textInput() ?>

    <?= $form->field($model, 'penalty', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->textInput() ?>

    <?= $form->field($model, 'penalty_time', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->textInput() ?>

    <?= $form->field($model, 'status', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->checkbox([], false) ?>

    <div class="form-group">
        <?= Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success', 'id' => 'sendBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

