<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\h_penalty\assets\HPenaltyAsset;
use backend\modules\h_penalty\models\HPenalty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\h_penalty\models\HPenaltySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$bundle = HPenaltyAsset::register($this);
$this->title = Yii::t('app', 'Hotel penalty');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hpenalty-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::button(Yii::t('app', 'Create'), ['class' => 'btn btn-success', 'id' => 'createPenalty', 'data-toggle' => 'modal', 'data-target' => '#penaltyModal']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'hotel_name',
                'value' => 'hotel.hotelName',
            ],
            'hotel_id',
            'penalty',
            [
                'attribute' => 'penalty_time',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ($model->status == '1') ? YII::t('app', 'Active') : Yii::t('app', 'Blocked');
                },
                'filter' => [0 => Yii::t('app', 'Blocked'), 1 => YII::t('app', 'Active')],
            ],
            [
                'format' => 'raw',
                'value' => function ($model) {
                    $button = Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil btn-s', 'id' => 'update', 'title' => 'Update', 'aria-label' => 'Update']);
                    $button .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']), ['delete', 'id' => $model->id], ['title' => 'Delete', 'aria-label' => 'Delete', 'style' => 'margin-left: 5px', 'data-confirm' => Yii::t('app', 'Do you want to delete' . '?')]);
                    return $button;
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>


<?php
Modal::begin([
    'options' => [
        'id' => 'penaltyModal',
    ],
    'header' => Yii::t('app', 'Penalty for hotel'),
]);
$model = new HPenalty();
echo $this->render('_form', ['model' => $model]);
Modal::end();

?>
