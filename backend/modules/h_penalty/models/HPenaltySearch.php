<?php

namespace backend\modules\h_penalty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HPenaltySearch represents the model behind the search form about `backend\modules\h_penalty\models\HPenalty`.
 */
class HPenaltySearch extends HPenalty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hotel_id', 'status'], 'integer'],
            [['penalty'], 'number'],
            [['penalty_time', 'hotel_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HPenalty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('manager.h_hotels as h', 'h.id = h_penalty.hotel_id');


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hotel_id' => $this->hotel_id,
            'penalty' => $this->penalty,
            'penalty_time' => $this->penalty_time,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'h.hotelName', $this->hotel_name]);

        return $dataProvider;
    }
}
