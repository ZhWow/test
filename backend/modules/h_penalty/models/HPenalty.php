<?php

namespace backend\modules\h_penalty\models;

use Yii;
use yii\db\ActiveRecord;
use backend\models\Hotel;

/**
 * This is the model class for table "manager.h_penalty".
 *
 * @property integer $id
 * @property integer $hotel_id
 * @property double $penalty
 * @property double $penalty_time
 * @property integer $status
 */
class HPenalty extends ActiveRecord
{
    public $hotel_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager.h_penalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'penalty', 'penalty_time', 'hotel_name'], 'required'],
            [['hotel_id', 'status'], 'integer'],
            [['hotel_id'], 'unique'],
            [['penalty'], 'number'],
            [['penalty_time'], 'integer'],
            [['hotel_name'], 'backend\modules\h_penalty\validators\IsHotelName'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Hotel ID',
            'penalty' => 'Penalty',
            'penalty_time' => 'Penalty Time',
            'status' => 'Status',
        ];
    }

    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }
}
