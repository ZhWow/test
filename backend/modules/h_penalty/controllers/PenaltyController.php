<?php

namespace backend\modules\h_penalty\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Hotel;
use backend\modules\h_penalty\models\HPenalty;
use backend\modules\h_penalty\models\HPenaltySearch;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PenaltyController implements the CRUD actions for HPenalty model.
 */
class PenaltyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all HPenalty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $source_message = new HPenalty();
        if (Yii::$app->request->isAjax && $source_message->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($source_message);
        }


        $searchModel = new HPenaltySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new HPenalty model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        parse_str(Yii::$app->request->post()['data'], $data);

        $model = new HPenalty();
        if ($model->load($data) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been created'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to create'));
            Yii::error($model->getErrors(), 'hotel_penalty_error');
        }

        return $this->redirect(['index']);
    }

    /**
     * Return HPenalty with join manager.h_hotels
     * @return string
     */
    public function actionUpdateInfo()
    {
        $id = Yii::$app->request->post()['id'];

        $result = HPenalty::find()
            ->from('manager.h_penalty as p')
            ->select('p.*, h.hotelName')
            ->innerJoin('manager.h_hotels as h', 'h.id = p.hotel_id')
            ->where(['p.id' => $id])
            ->asArray()
            ->one();

        return json_encode($result);
    }

    /**
     * Updates an existing HPenalty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        $model = $this->findModel($post['id']);

        if ($model->load($data) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been updated'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to update'));
            Yii::error($model->getErrors(), 'hotel_penalty_error');
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing HPenalty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been deleted'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the HPenalty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HPenalty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HPenalty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Return Hotel model
     * @return string
     */
    public function actionHotelAutoComplete()
    {
        $term = Yii::$app->request->get()['term'];
        $hotel = '';
        if ($term) {
            $hotel = Hotel::find()->select(['hotelName as label', 'id'])->where(['like', 'hotelName', $term])->limit(15)->asArray()->all();
        }

        return json_encode($hotel);
    }

    /**
     * Client validate form
     * @return array
     */
    public function actionValidation()
    {
        $result = 'validate error';
        $model = new HPenalty();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
        }
        return $result;

    }

}
