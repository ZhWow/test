<?php

namespace backend\modules\h_penalty\assets;

use yii\web\AssetBundle;

class HPenaltyAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/h_penalty/assets';

    public $css = [
        'css/api.css',
    ];

    public $js = [
        'js/function.js',
        'js/script.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}