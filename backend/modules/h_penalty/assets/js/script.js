/**
 * @param {{status, penalty, hotel_id,  hotelName, penalty_time, status}} response
 */
$(document).ready(function () {
    var body = $('body'),
        model_url = '/hotel-penalty/penalty/create',
        model_id;

    body.on('click', '#createPenalty', function (e) {
        e.preventDefault();
        $('.field-hpenalty-hotel_name').show();
        $('#hotelPenaltyForm').trigger("reset");
        $('#hpenalty-status').prop('checked', false);
    });

    body.on('click', '#update', function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('data-key');

        $.ajax({
            url: '/hotel-penalty/penalty/update-info',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            beforeSend: function () {
                $(this).prop('disabled', true);
            },
            success: function (response) {
                model_url = '/hotel-penalty/penalty/update';
                model_id = id;
                $('.field-hpenalty-hotel_name').hide();
                $('#hpenalty-hotel_id').val(response.hotel_id);
                $('#hpenalty-hotel_name').val(response.hotelName);
                $('#hpenalty-penalty').val(response.penalty);
                $('#hpenalty-penalty_time').val(response.penalty_time);
                $('#hpenalty-status').prop('checked', false);
                if (response.status === '1') $('body #hpenalty-status').prop('checked', true);
                $('#penaltyModal').modal('show');
            }
        });
    });

    body.on('submit', '#hotelPenaltyForm', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $('#penaltyModal').modal('hide');
        formSend(model_url, data, model_id);
    });

    $('input[name="HPenalty[hotel_name]"]').autoComplete({
        minChars: 3,
        source: function (term, response) {
            try {
                xhr.abort();
            } catch (e) {
            }
            xhr = $.getJSON('/hotel-penalty/penalty/hotel-auto-complete', {
                term: term
            }, function (data) {
                response(data);
            });
        },
        renderItem: function (item) {
            return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-val="' + item.label + '">' + item.label + '</div>';
        },
        onSelect: function (e, term, item) {
            $('#hpenalty-hotel_id').val($(item).attr('data-id'));
        }
    });
});