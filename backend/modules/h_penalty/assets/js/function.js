function formSend(url, data, model_id) {
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
            data: data,
            id: model_id
        },
        beforeSend: function () {
            $('#sendBtn').prop('disabled', true);
        }
    });

    return true;
}
