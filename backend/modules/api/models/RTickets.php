<?php

namespace backend\modules\api\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "r_tickets".
 *
 * @property integer $id
 * @property integer $isCancelled
 * @property integer $userID
 * @property integer $corpID
 * @property integer $ticketId
 * @property integer $bookingId
 * @property integer $ticketSupplierId
 * @property integer $passengerId
 * @property integer $fareId
 * @property string $ticketType
 * @property string $coupons
 */
class RTickets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_tickets';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_ktgak');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isCancelled', 'userID', 'corpID', 'ticketId', 'bookingId', 'ticketSupplierId', 'passengerId', 'fareId'], 'integer'],
            [['ticketType', 'coupons'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isCancelled' => 'Is Cancelled',
            'userID' => 'User ID',
            'corpID' => 'Corp ID',
            'ticketId' => 'Ticket ID',
            'bookingId' => 'Booking ID',
            'ticketSupplierId' => 'Ticket Supplier ID',
            'passengerId' => 'Passenger ID',
            'fareId' => 'Fare ID',
            'ticketType' => 'Ticket Type',
            'coupons' => 'Coupons',
        ];
    }
}
