<?php

namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SysUserHotelSearch represents the model behind the search form about `backend\modules\api\models\SysUserHotel`.
 */
class SysUserHotelSearch extends SysUserHotel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'hotel_id', 'is_api'], 'integer'],
            [['email', 'hotel_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysUserHotel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'hotel_id' => $this->hotel_id,
            'is_api' => $this->is_api,
        ]);

        $query->joinWith('user');
        $query->leftJoin('manager.h_hotels as h', 'h.hotelID = sys_user_hotel.hotel_id');

        $query->andFilterWhere(['like', 'user.email', $this->email]);
        $query->andFilterWhere(['like', 'h.hotelName', $this->hotel_name]);

        return $dataProvider;
    }
}
