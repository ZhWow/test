<?php

namespace backend\modules\api\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use backend\models\Hotel;

/**
 * This is the model class for table "sys_user_hotel".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $hotel_id
 * @property integer $is_api
 */
class SysUserHotel extends ActiveRecord
{
    public $email;
    public $hotel_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_user_hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'hotel_id', 'email', 'hotel_name'], 'required'],
            [['user_id', 'hotel_id', 'is_api'], 'integer'],
            ['hotel_name', 'backend\modules\api\validators\IsHotelName'],
            ['email', 'backend\modules\api\validators\UserUnique'],
            ['email', 'backend\modules\api\validators\HotelUserUnique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => 'User ID',
            'hotel_id' => 'Hotel ID',
            'is_api' => Yii::t('app', 'Is Api'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['hotelID' => 'hotel_id']);
    }
}
