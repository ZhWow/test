<?php

namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SysUserAgencySearch represents the model behind the search form about `backend\modules\api\models\SysUserAgency`.
 */
class SysUserAgencySearch extends SysUserAgency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'agency_id', 'is_api'], 'integer'],
            [['email'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysUserAgency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'agency_id' => $this->agency_id,
            'is_api' => $this->is_api,
        ]);

        $query->andFilterWhere(['like', 'user.email', $this->email]);

        return $dataProvider;
    }
}
