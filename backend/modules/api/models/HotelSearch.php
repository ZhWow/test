<?php

namespace backend\modules\api\models;

use yii\base\Model;

class HotelSearch extends Model
{
    const GET_TICKET_STATUS = 'get_ticket_status';
    const GET_TICKETS = 'get_ticket';

    public $star;
    public $cityCode;
    public $hotelName;
    public $firstName;
    public $secondName;
    public $bookingReference;

    public function rules()
    {
        return [
            [['bookingReference'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['star', 'cityCode', 'hotelName', 'firstName', 'secondName'], 'string'],
            [['star', 'cityCode', 'hotelName', 'firstName', 'secondName'], 'trim'],
            [['bookingReference'], 'required', 'on' => self::GET_TICKET_STATUS],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::GET_TICKET_STATUS] = ['bookingReference'];
        $scenarios[self::GET_TICKETS] = ['star', 'cityCode', 'hotelName', 'firstName', 'secondName'];
        return $scenarios;
    }
}