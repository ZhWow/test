<?php

namespace backend\modules\api\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "api_response_history".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $corp_id
 * @property integer $user_id
 * @property integer $last_id
 */
class ApiResponseHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_response_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'corp_id', 'user_id'], 'required'],
            [['provider_id', 'corp_id', 'user_id', 'last_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider_id' => 'Provider ID',
            'corp_id' => 'Corp ID',
            'user_id' => 'User ID',
            'last_id' => 'Last ID',
        ];
    }

}
