<?php

namespace backend\modules\api\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use backend\models\AgencyOids;

/**
 * This is the model class for table "sys_user_agency".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $agency_id
 * @property integer $is_api
 */
class SysUserAgency extends ActiveRecord
{
    public $email;
    public $agency_oid;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_user_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'agency_id', 'email'], 'required'],
            [['user_id', 'agency_id', 'is_api'], 'integer'],
            ['agency_oid', 'backend\modules\api\validators\AgentOidUnique'],
            [['email'], 'backend\modules\api\validators\AgentUserUnique'],
            ['email', 'backend\modules\api\validators\UserUnique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => 'User ID',
            'agency_id' => 'Agency ID',
            'is_api' => Yii::t('app', 'Is Api'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getAgency()
    {
        return $this->hasOne(AgencyOids::className(), ['id' => 'agency_id']);
    }

}
