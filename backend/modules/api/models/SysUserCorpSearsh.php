<?php

namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SysUserCorpSearsh represents the model behind the search form about `backend\modules\api\models\SysUserCorp`.
 */
class SysUserCorpSearsh extends SysUserCorp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'corp_id', 'is_api',], 'integer'],
            [['email'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysUserCorp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        // grid filtering conditions
        $query->andFilterWhere([
            'sys_user_corp.id' => $this->id,
            'corp_id' => $this->corp_id,
            'is_api' => $this->is_api,
        ]);

        $query->andFilterWhere(['like', 'user.email', $this->email]);

        return $dataProvider;
    }
}
