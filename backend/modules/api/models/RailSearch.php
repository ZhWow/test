<?php

namespace backend\modules\api\models;


use yii\base\Model;

class RailSearch extends Model
{

    const GET_TICKET_STATUS = 'get_ticket_status';
    const GET_TICKET = 'get_ticket';

    public $ticketId;
    public $ticketSupplierId;
    public $status;
    public $firstName;
    public $secondName;

    public function rules()
    {
        return [
            [['ticketId', 'ticketSupplierId'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['status', 'firstName', 'secondName'], 'string'],
            [['status', 'firstName', 'secondName'], 'trim'],
            [['ticketId'], 'required', 'on' => self::GET_TICKET_STATUS],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::GET_TICKET_STATUS] = ['ticketId'];
        $scenarios[self::GET_TICKET] = ['firstName', 'secondName'];
        return $scenarios;
    }

}