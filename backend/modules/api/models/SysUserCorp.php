<?php

namespace backend\modules\api\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use backend\models\Corp;

/**
 * This is the model class for table "sys_user_corp".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $corp_id
 * @property integer $is_api
 */
class SysUserCorp extends ActiveRecord
{
    public $email;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_user_corp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'corp_id', 'email'], 'required'],
            [['user_id', 'corp_id', 'is_api'], 'integer'],
            [['email'], 'backend\modules\api\validators\CorpUserUnique'],
            ['email', 'backend\modules\api\validators\UserUnique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => 'User ID',
            'corp_id' => 'Corp ID',
            'is_api' => Yii::t('app', 'Is Api'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCorp()
    {
        return $this->hasOne(Corp::className(), ['id' => 'corp_id']);
    }
}
