<?php

namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use common\models\User;
use backend\modules\api\services\Methods;

class Api extends Model
{
    public $userId;
    public $login;
    public $password;
    public $token;
    public $request;
    public $responseType;
    public $searchData;
    public $category;

    public function rules()
    {
        return [
            [['login', 'password', 'token', 'request', 'responseType'], 'trim'],
            [['login', 'password', 'token', 'request', 'responseType', 'searchData'], 'string'],
            [['login', 'request'], 'required'],
            ['login', 'validateLoginStatus'],
            ['login', 'validateLogin'],
            ['login', 'validatePswCount'],
            ['login', 'validateToken'],
            ['login', 'validatePermission'],
            ['request', 'validateRequestPermission'],
            ['request', 'validateRequest'],
            ['password', 'validateApiPassword'],
        ];
    }

    public function validateLogin($attribute, /* @noinspection PhpUnusedParameterInspection */$params)
    {
        $model = User::findOne(['email' => $this->$attribute]);
        if (!$model) {
            $this->addError($attribute, 'Wrong login!');
        }
        /* @noinspection PhpUndefinedFieldInspection */
        $this->userId = $model->id;
    }


    public function validateLoginStatus($attribute, /* @noinspection PhpUnusedParameterInspection */ $params)
    {
        $model = User::findOne(['email' => $this->$attribute]);
        /* @noinspection PhpUndefinedFieldInspection */
        if ($model->status === 0) {
            $this->addError($attribute, 'This login ' . $this->$attribute . ' has blocked!');
        }
    }

    public function validateApiPassword($attribute, /* @noinspection PhpUnusedParameterInspection */ $params)
    {
        $model = User::findOne(['email' => $this->login]);
         /* @noinspection PhpUndefinedMethodInspection */
        if (!empty($model) && !$model->validatePassword($this->$attribute)) {
            if ($this->setPswCount()) $this->addError('password', 'Wrong password!');
        }
    }
    public function validatePermission($attribute, /* @noinspection PhpUnusedParameterInspection */ $params)
    {
        $model = User::findOne(['email' => $this->$attribute]);
        /* @noinspection PhpUndefinedFieldInspection */
        $category = Methods::getCategory($model->id);
        if (!$category) {
            $this->addError('permission', 'You do not have any permission!');
        } else {
            $this->category = $category;
        }
    }

    public function validateToken()
    {
        $sysToken = $this->getToken();
		if (empty($this->password) && $this->token != $sysToken && empty($this->token) !== false) {
            $this->addError('password', 'Password has not be empty');
        } else {
            $this->instanceToken();
        }
    }

    public function validateRequestPermission($attribute, /* @noinspection PhpUnusedParameterInspection */
                                              $params)
    {
        $errorMessage = "You do not have a permission for request {$this->$attribute}";
        if (($this->$attribute == 'getHotelTickets' || $this->$attribute == 'getHotelTicketsStatus') && ($this->category['table_name'] == 'sys_user_agency')) {
            $this->addError('request permission', $errorMessage);
        }
        if (($this->$attribute == 'getFlightTickets' || $this->$attribute == 'getFlightTicketsStatus') && ($this->category['table_name'] == 'sys_user_hotel')) {
            $this->addError('request permission', $errorMessage);
        }
        if (($this->$attribute == 'getRailTickets' || $this->$attribute == 'getRailTicketsStatus') && ($this->category['table_name'] == 'sys_user_hotel' || $this->category['table_name'] == 'sys_user_agency')) {
            $this->addError('request permission', $errorMessage);
        }
    }

    public function instanceToken()
    {
        $sysToken = $this->getToken();
        if ($this->token == $sysToken && $this->token != false && $sysToken != false) {
            $this->token = $this->getToken();
        } else {
            $this->setToken();
            $this->token = $this->getToken();
        }
        return true;
    }

    public function validatePswCount()
    {
        $pswCount = $this->getPswCount();
        if ($pswCount > 3) {
            $status = User::updateAll(['status' => 0], ['email' => $this->login]);
            if ($status) $this->addError('password_count', 'You exceeded the limit! ' . $this->login . ' has been blocked!');
        }
    }

    public function validateRequest($attribute, /* @noinspection PhpUnusedParameterInspection */
                                    $params)
    {
        $requestList = ['getFlightTickets', 'getFlightTicketsStatus', 'getHotelTickets', 'getHotelTicketsStatus', 'getRailTickets', 'getRailTicketsStatus'];
        if (!in_array($this->$attribute, $requestList)) {
            $this->addError('request', ' There is no such request => ' . $this->$attribute);
        }
    }

    public function getPswCount()
    {
        return Yii::$app->cache->get('psw_err_' . $this->login);
    }

    public function setPswCount()
    {
        $pswCount = $this->getPswCount() + 1;
        $duration = Yii::$app->params['apiDuration'];
        return Yii::$app->cache->set('psw_err_' . $this->login, $pswCount, $duration);
    }

    public function setToken()
    {
        $token = time() . Yii::$app->getSecurity()->generateRandomString(32);
        $duration = Yii::$app->params['apiDuration'];
        return Yii::$app->cache->set('token_' . $this->login, $token, $duration);
    }

    public function getToken()
    {
        return Yii::$app->cache->get('token_' . $this->login);
    }
}