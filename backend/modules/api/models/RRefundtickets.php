<?php

namespace backend\modules\api\models;

use modules\organisation\models\rails\RFares;
use Yii;

/**
 * This is the model class for table "r_refundtickets".
 *
 * Database from is required
 *
 * @property integer $id
 * @property integer $refundId
 * @property integer $refundTicketId
 * @property integer $ticketId
 * @property integer $passengerId
 * @property integer $fareId
 * @property integer $coupons
 */
class RRefundtickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_refundtickets';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
//    public static function getDb()
//    {
//        return Yii::$app->get('db_ktgak');
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['refundId', 'refundTicketId', 'ticketId', 'passengerId', 'fareId', 'coupons'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'refundId' => 'Refund ID',
            'refundTicketId' => 'Refund Ticket ID',
            'ticketId' => 'Ticket ID',
            'passengerId' => 'Passenger ID',
            'fareId' => 'Fare ID',
            'coupons' => 'Coupons',
        ];
    }

    public function getFareRefund()
    {
        return $this->hasOne(RFares::relation(static::$db), ['fareId' => 'fareId']);
    }
}
