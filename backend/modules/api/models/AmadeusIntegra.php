<?php

namespace backend\modules\api\models;

use Yii;
use backend\modules\integra\models\base\AmadeusIntegra as BaseAmadeusIntegra;

/**
 * Class AmadeusIntegra
 *
 * @property string $fullName
 * @property string $date
 * @property string $time
 *
 * @package backend\modules\integra\models
 */
class AmadeusIntegra extends BaseAmadeusIntegra
{
	
	const GET_TICKET_STATUS = 'get_ticket_status';
    const GET_TICKETS = 'get_tickets';

    public $TICKET_NUMBER;
	
	public function rules()
    {
        return [
            [['FIRST_NAME', 'LAST_NAME', 'DEPARTURE_DATE', 'ARRIVAL_DATES', 'DOCUMENT_ISSUED', 'RECORD_LOCATOR'], 'string', 'max' => 250],
            [['TICKET_NUMBER'], 'required', 'on' => self::GET_TICKET_STATUS],
            [['TICKET_NUMBER'], 'filter', 'filter' => 'trim', 'skipOnArray' => true, 'on' => self::GET_TICKET_STATUS],
        ];
    }
	
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::GET_TICKET_STATUS] = ['TICKET_NUMBER'];
        $scenarios[self::GET_TICKETS] = ['FIRST_NAME', 'LAST_NAME', 'DEPARTURE_DATE', 'ARRIVAL_DATES', 'DOCUMENT_ISSUED', 'RECORD_LOCATOR'];
        return $scenarios;
    }
}