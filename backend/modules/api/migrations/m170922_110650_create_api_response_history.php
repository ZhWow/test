<?php

namespace backend\modules\api\migrations;

use yii\db\Migration;

class m170922_110650_create_api_response_history extends Migration
{
    public function safeUp()
    {
        $this->createTable('api_response_history', [
            'id' => $this->primaryKey(),
            'provider_id' => $this->integer()->notNull(),
            'corp_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'last_id' => $this->integer()->comment('The last ticket id'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('sys_user_agency');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170922_110650_create_api_response_history cannot be reverted.\n";

        return false;
    }
    */
}
