<?php

namespace backend\modules\api\migrations;

use yii\db\Migration;

class m170922_111326_create_provider_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('provider_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(60),
        ]);
    }

    public function safeDown()
    {
        $this->delete('provider_type');
    }
}
