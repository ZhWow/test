<?php

namespace backend\modules\api\migrations;

use yii\db\Migration;

class m170926_112826_delete_table_params extends Migration
{
    public function safeUp()
    {
        $this->dropTable('params');
    }

    public function safeDown()
    {
        echo "m170926_112826_delete_table_params cannot be reverted.\n";

        return false;
    }
}
