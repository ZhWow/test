<?php

namespace backend\modules\api\migrations;

use yii\db\Migration;

class m170919_044159_create_params extends Migration
{
    public function safeUp()
    {
        $this->createTable('params', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'api_last_id' => $this->integer()->comment('The last ticket id'),
            'api_ticket_count' => $this->integer()->comment('The ticket return number'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('params');
    }
}
