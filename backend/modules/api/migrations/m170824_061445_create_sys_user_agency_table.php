<?php

namespace backend\modules\api\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `sys_user_agency`.
 */
class m170824_061445_create_sys_user_agency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sys_user_agency', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'agency_id' => $this->integer()->notNull(),
            'is_api' => $this->smallInteger()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sys_user_agency');
    }
}
