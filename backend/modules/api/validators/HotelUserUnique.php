<?php

namespace backend\modules\api\validators;

use Yii;
use yii\validators\Validator;
use backend\modules\api\models\SysUserAgency;
use backend\modules\api\models\SysUserCorp;

class HotelUserUnique extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Yii::t('app', 'The email exist in another table');
    }

    public static function getCheckList()
    {
        $check = array_unique(array_merge(
            SysUserAgency::find()
                ->select('u.email')
                ->from('sys_user_agency as a')
                ->leftJoin('user as u', 'u.id = a.user_id')
                ->asArray()
                ->column(),
            SysUserCorp::find()
                ->select('u.email')
                ->from('sys_user_corp as c')
                ->leftJoin('user as u', 'u.id = c.user_id')
                ->asArray()
                ->column()
        ));
        return array_values($check);
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $check = self::getCheckList();
        if (in_array($value, $check)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $result = json_encode(self::getCheckList());

        return <<<JS
        if ($.inArray(value, $result) !== -1) {
            messages.push($message);
        }
JS;
    }
}