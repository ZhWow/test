<?php

namespace backend\modules\api\validators;

use Yii;
use yii\validators\Validator;
use backend\modules\api\models\SysUserAgency;
use backend\modules\api\models\SysUserHotel;

class CorpUserUnique extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Yii::t('app', 'The email exist in another table');
    }

    public static function getCheckList()
    {
        $check = array_unique(array_merge(
            SysUserAgency::find()
                ->select('u.email')
                ->from('sys_user_agency as a')
                ->leftJoin('user as u', 'u.id = a.user_id')
                ->asArray()
                ->column(),
            SysUserHotel::find()
                ->select('u.email')
                ->from('sys_user_hotel as h')
                ->innerJoin('user as u', 'u.id = h.user_id')
                ->asArray()
                ->column()
        ));
        return array_values($check);
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $check = self::getCheckList();
        if (in_array($value, $check)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $result = json_encode(self::getCheckList());

        return <<<JS
        if ($.inArray(value, $result) !== -1) {
            messages.push($message);
        }
JS;
    }
}