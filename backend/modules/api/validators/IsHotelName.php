<?php

namespace backend\modules\api\validators;

use Yii;
use yii\validators\Validator;
use backend\models\Hotel;

class IsHotelName extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Yii::t('app', 'The hotel name not found in the table hotel');
    }


    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $check = Hotel::find()->select('hotelName')->asArray()->column();
        if (!in_array($value, $check)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $hotel = Hotel::find()->select('hotelName')->asArray()->column();
        $result = json_encode($hotel);

        return <<<JS
        if ($.inArray(value, $result) !== -1) {
            messages.push($message);
        }
JS;
    }
}