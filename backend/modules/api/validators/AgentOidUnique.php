<?php

namespace backend\modules\api\validators;

use Yii;
use yii\validators\Validator;
use backend\models\AgencyOids;

class AgentOidUnique extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Yii::t('app', 'The agency oid is not exist in the table agency');
    }

    public function getModel()
    {
        return AgencyOids::find()->select('oid')->asArray()->column();
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $check = $this->getModel();
        if (!in_array($value, $check)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $check = json_encode($this->getModel());

        return <<<JS
        if ($.inArray(value, $check) !== -1) {
            messages.push($message);
        }
JS;
    }
}