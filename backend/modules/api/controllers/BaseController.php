<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 31.08.2017
 * Time: 12:11
 */

namespace backend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\User;

class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Return User model
     * @return string
     */
    public function actionUserAutoComplete()
    {
        $term = Yii::$app->request->get()['term'];
        $result = '';
        if ($term) {
            $result = User::find()->select(['email as label', 'id as id'])->where(['like', 'email', $term])->limit(15)->asArray()->all();
        }

        return json_encode($result);
    }
}