<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use backend\models\Hotel;
use backend\modules\api\models\SysUserHotel;
use backend\modules\api\models\SysUserHotelSearch;
use backend\modules\api\services\Methods;

class SysUserHotelController extends BaseController
{
    public function actionIndex()
    {
        $searchModel = new SysUserHotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Create an existing SysUserHotel model.
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);

        try {
            $model = new SysUserHotel();
            if ($model->load($data) && $model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been created'));
            } else {
                throw new Exception(json_encode($model->getErrors()));
            }
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to create'));
            Yii::error($e, 'user_hotel_error');
        }

        return $this->redirect(Url::toRoute('sys-user-hotel/index'));
    }

    /**
     * Return SysUserHotel join model
     * @return string
     */
    public function actionUpdateInfo()
    {
        $id = Yii::$app->request->post()['id'];
        $result = SysUserHotel::find()
            ->select('sys_user_hotel.user_id, user.email, hotel_id, is_api, h.hotelName')
            ->innerJoin('user', 'user.id = sys_user_hotel.user_id')
            ->innerJoin('manager.h_hotels as h', 'h.hotelID = sys_user_hotel.hotel_id')
            ->where(['sys_user_hotel.id' => $id])
            ->asArray()
            ->one();

        return json_encode($result);
    }

    /**
     * Updates an existing SysUserHotel model.
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        $model = $this->findModel($post['id']);

        if ($model->load($data) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been updated'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to update'));
            \
            Yii::error($model->getErrors(), 'user_hotel_error');
        }

        return $this->redirect(Url::toRoute('sys-user-hotel/index'));
    }

    /**
     * Deletes an existing SysUserHotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SysUserHotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SysUserHotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SysUserHotel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Return Hotel model
     * @return string
     */
    public function actionHotelAutoComplete()
    {
        $term = Yii::$app->request->get()['term'];
        $hotel = '';
        if ($term) {
            $hotel = Hotel::find()->select(['hotelName as label', 'hotelID as id'])->where(['like', 'hotelName', $term])->limit(15)->asArray()->all();
        }

        return json_encode($hotel);
    }

    /**
     * Unblock changes user status
     * @return \yii\web\Response
     */
    public function actionUnblock()
    {
        $userId = Yii::$app->request->post()['userId'];

        Methods::unblockUser($userId, 'user_hotel_error');

        return $this->redirect(Url::toRoute('sys-user-hotel/index'));
    }

    /**
     * Client validate form
     * @return array
     */
    public function actionValidation()
    {
        $result = 'validate error';
        $model = new SysUserHotel();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
        }
        return $result;
    }
}