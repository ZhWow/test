<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\helpers\Url;
use yii\base\Exception;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use backend\modules\api\models\SysUserCorp;
use backend\modules\api\models\SysUserCorpSearsh;
use backend\modules\api\services\Methods;

/**
 * SysUserCorpController implements the CRUD actions for SysUserCorp model.
 */
class SysUserCorpController extends BaseController
{
    /**
     * Lists all SysUserCorp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SysUserCorpSearsh();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Create an existing SysUserCorp model.
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        try {
            $model = new SysUserCorp();
            if ($model->load($data) && $model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been created'));
            } else {
                throw new Exception(json_encode($model->getErrors()));
            }
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to create'));
            Yii::error($e, 'user_corp_error');
        }

        return $this->redirect(Url::toRoute('sys-user-corp/index'));
    }

    /**
     * Return SysUserCorp join model
     * @return string
     */
    public function actionUpdateInfo()
    {
        $id = Yii::$app->request->post()['id'];
        $result = SysUserCorp::find()
            ->select('sys_user_corp.user_id, user.email, corp_id, is_api ')
            ->rightJoin('user', '`user`.`id` = `sys_user_corp`.`user_id`')
            ->where(['sys_user_corp.id' => $id])
            ->asArray()
            ->one();

        return json_encode($result);
    }

    /***
     * Updates an existing SysUserCorp model.
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        $model = $this->findModel($post['id']);

        if ($model->load($data) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been updated'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to update'));
            Yii::error($model->getErrors(), 'user_corp_error');
        }

        return $this->redirect(Url::toRoute('sys-user-corp/index'));
    }

    /**
     * Deletes an existing SysUserCorp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SysUserCorp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SysUserCorp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SysUserCorp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Unblock changes user status
     * @return \yii\web\Response
     */
    public function actionUnblock()
    {
        $userId = Yii::$app->request->post()['userId'];

        Methods::unblockUser($userId, 'user_corp_error');

        return $this->redirect(Url::toRoute('sys-user-corp/index'));
    }

    /**
     * Client validate form
     * @return array
     */
    public function actionValidation()
    {
        $result = 'validate error';
        $model = new SysUserCorp();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
        }
        return $result;
    }
}
