<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use backend\modules\api\services\TicketRequest;
use backend\modules\api\models\Api;


class TicketController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'list' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'list') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'status' => false,
            'message' => 'Error',
        ];
        try {
            $post = Yii::$app->request->post();
            $api = new Api();
            $api->attributes = $post;

            if ($api->validate()) {
                if ($api->responseType === 'xml') Yii::$app->response->format = Response::FORMAT_XML;

                if ($api->request === 'getFlightTickets') {
                    $ticket = TicketRequest::flight($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                } elseif ($api->request === 'getFlightTicketsStatus') {
                    $ticket = TicketRequest::getFlightTicketsStatus($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                } elseif ($api->request === 'getHotelTickets') {
                    $ticket = TicketRequest::hotel($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                } elseif ($api->request === 'getHotelTicketsStatus') {
                    $ticket = TicketRequest::getHotelStatus($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                } elseif ($api->request === 'getRailTickets') {
                    $ticket = TicketRequest::rail($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                } elseif ($api->request === 'getRailTicketsStatus') {
                    $ticket = TicketRequest::getRailTicketStatus($api);
                    if (array_key_exists('error', $ticket))
                        $result['message'] = $ticket;
                    else
                        $result['data'] = $ticket;
                }

                if (array_key_exists('data', $result)) {
                    $result['status'] = true;
                    $result['token'] = $api->token;
                    $result['message'] = 'Success';
                }
            } else {
                Yii::error($api->getErrors(), 'api_error');
                $result['message'] = $api->getErrors();
            }

        } catch (Exception $e) {
            Yii::error($e, 'api_error');
        } catch (\Exception $e) {
            Yii::error($e, 'api_error');
        }

        return $result;
    }

}