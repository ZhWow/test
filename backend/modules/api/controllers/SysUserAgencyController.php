<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 28.08.2017
 * Time: 16:10
 */

namespace backend\modules\api\controllers;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use backend\models\AgencyOids;
use backend\modules\api\models\SysUserAgencySearch;
use backend\modules\api\models\SysUserAgency;
use backend\modules\api\services\Methods;

class SysUserAgencyController extends BaseController
{
    public function actionIndex()
    {
        $searchModel = new SysUserAgencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,

            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Create an existing SysUserAgency model.
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);

        try {
            $model = new SysUserAgency();
            if ($model->load($data) && $model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been created'));
            } else {
                throw new Exception(json_encode($model->getErrors()));
            }
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to create'));
            Yii::error($e, 'user_agency_error');
        }

        return $this->redirect(Url::toRoute('sys-user-agency/index'));
    }

    /**
     * Return SysUserAgency join model
     * @return string
     */
    public function actionUpdateInfo()
    {
        $id = Yii::$app->request->post()['id'];
        $result = SysUserAgency::find()
            ->select('sys_user_agency.user_id, user.email, agency_id, a.oid, is_api')
            ->innerJoin('user', 'user.id = sys_user_agency.user_id')
            ->innerJoin('manager.t_agency_oids as a', 'a.id = sys_user_agency.agency_id')
            ->where(['sys_user_agency.id' => $id])
            ->asArray()
            ->one();

        return json_encode($result);
    }

    /**
     * Update an existing SysUserAgency model.
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        $model = $this->findModel($post['id']);

        if ($model->load($data) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been updated'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to update'));
            Yii::error($model->getErrors(), 'user_agency_error');
        }

        return $this->redirect(Url::toRoute('sys-user-agency/index'));
    }

    /**
     * Deletes an existing SysUserAgency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SysUserAgency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SysUserAgency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SysUserAgency::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Unblock changes user status
     * @return \yii\web\Response
     */
    public function actionUnblock()
    {
        $userId = Yii::$app->request->post()['userId'];

        Methods::unblockUser($userId, 'user_agency_error');

        return $this->redirect(Url::toRoute('sys-user-agency/index'));
    }

    /**
     * Return AgencyOids model
     * @return string
     */
    public function actionOidAutoComplete()
    {
        $term = Yii::$app->request->get()['term'];
        $result = '';
        if ($term) {
            $result = AgencyOids::find()->select(['oid as label', 'id as id'])->where(['like', 'oid', $term])->limit(15)->asArray()->all();
        }

        return json_encode($result);
    }

    /**
     * Client validate form
     * @return array
     */
    public function actionValidation()
    {
        $result = 'validate error';
        $model = new SysUserAgency();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
        }
        return $result;
    }
}