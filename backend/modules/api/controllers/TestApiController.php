<?php

namespace backend\modules\api\controllers;

use  yii;
use yii\web\Controller;

class TestApiController extends Controller
{

    public function actionCurl()
    {
        $dataT = [
//            'token' => "15051058457JQQe_3tKmmlc0P88i0MQ53YtOizDJv5",
            'login' => 'mr.dosik1994@gmail.com',
            'password' => 'coordinator',
            'request' => 'getHotelTicketsStatus',
            'responseType' => 'json',
            'searchData' => json_encode([
//                'arrival_date' => '07-07-2017',
//                'departure_date' => '07-07-2017',
//                'document_issued' => '10-05-2017',
//                'first_name' => 'MARLEN',
//                'last_name' => 'BELGIBAYEV',
                'bookingReference' => [
                    '3VK7I7I8RV6KPSVJ009',
                    '3VK7I7I8RV6KPSVJ010',
                ]
            ]),
        ];

        $url = "http://backend.qway.test/api/ticket/list";

        $result = $this->curl($url, $dataT);

        echo '<pre>';
        echo $result;
        echo '</pre>';

    }

    public function curl($url, $post = null)
    {
        $ch = curl_init($url);

        $header = [
            'Contect-Type:application/xml', 'Accept:application/xml'
        ];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


}
