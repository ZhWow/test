<?php

namespace backend\modules\api\assets;

use yii\web\AssetBundle;

class ApiAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/api/assets';

    public $css = [
        'css/api.css',
    ];

    public $js = [
        'js/function.js',
        'js/api.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}