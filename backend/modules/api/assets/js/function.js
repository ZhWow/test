function isError(form) {
    var status = true;
    $(form).find('.help-block').toArray().forEach(function (item) {
        if (!$(item).is(':empty')) {
            status = false;
        }
    });
    return status;
}

function formSend(url, data, model_id) {
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
            data: data,
            id: model_id
        },
        beforeSend: function () {
            $('#sendBtn').prop('disabled', true);
        }
    });
    return true;
}
