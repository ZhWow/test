/**
 * @param {{corp_id:string, user_id,  email, is_api, agency_id, oid, hotel_id, hotelName}} response
 */
$(document).ready(function () {
    var body = $('body'),
        model_url,
        model_id;

    // SysUserCorp
    body.on('click', '#createUserCorp', function (e) {
        e.preventDefault();
        $('#UserCorpForm').trigger("reset");
        $('#sysusercorp-is_api').prop('checked', false);
        model_url = '/api/sys-user-corp/create';
    });

    body.on('click', '#updateUseCorp', function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('data-key');

        $.ajax({
            url: '/api/sys-user-corp/update-info',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            beforeSend: function () {
                $(this).prop('disabled', true);
            },
            success: function (response) {
                model_url = '/api/sys-user-corp/update';
                model_id = id;
                $('#sysusercorp-email').val(response.email);
                $('#sysusercorp-user_id').val(response.user_id);
                $('#sysusercorp-corp_id').val(response.corp_id);
                $('#sysusercorp-is_api').prop('checked', false);
                if (response.is_api === '1') $('body #sysusercorp-is_api').prop('checked', true);
                $('.field-sysusercorp-email').hide();
                $('#createUserCorpModal').modal('show');
            }
        });
    });

    body.on('submit', '#UserCorpForm', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        formSend(model_url, data, model_id);
    });
    // End SysUserCorp

    // SysUserAgency
    body.on('click', '#createUserAgency', function (e) {
        e.preventDefault();
        $('#UserAgencyForm').trigger("reset");
        $('#sysuseragency-is_api').prop('checked', false);
        model_url = '/api/sys-user-agency/create';
    });

    body.on('click', '#updateUserAgency', function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('data-key');

        $.ajax({
            url: '/api/sys-user-agency/update-info',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            beforeSend: function () {
                $(this).prop('disabled', true);
            },
            success: function (response) {
                model_url = '/api/sys-user-agency/update';
                model_id = id;
                $('#sysuseragency-email').val(response.email);
                $('#sysuseragency-user_id').val(response.user_id);
                $('#sysuseragency-agency_id').val(response.agency_id);
                $('#sysuseragency-agency_oid').val(response.oid);
                $('#sysuseragency-is_api').prop('checked', false);
                if (response.is_api === '1') $('body #sysuseragency-is_api').prop('checked', true);
                $('.field-sysuseragency-email').hide();
                $('#createUserAgencyModal').modal('show');
            }
        });
    });

    body.on('submit', '#UserAgencyForm', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        formSend(model_url, data, model_id);
    });
    // End SysUserAgency

    // SysUserHotel
    body.on('click', '#createUserHotel', function (e) {
        e.preventDefault();
        $('#UserHotelForm').trigger("reset");
        $('#sysuserhotel-is_api').prop('checked', false);
        model_url = '/api/sys-user-hotel/create';
    });

    body.on('click', '#updateUserHotel', function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('data-key');

        $.ajax({
            url: '/api/sys-user-hotel/update-info',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            beforeSend: function () {
                $(this).prop('disabled', true);
            },
            success: function (response) {
                model_url = '/api/sys-user-hotel/update';
                model_id = id;
                $('#sysuserhotel-email').val(response.email);
                $('#sysuserhotel-user_id').val(response.user_id);
                $('#sysuserhotel-hotel_id').val(response.hotel_id);
                $('#sysuserhotel-hotel_name').val(response.hotelName);
                $('#sysuserhotel-is_api').prop('checked', false);
                if (response.is_api === '1') $('body #sysuserhotel-is_api').prop('checked', true);
                $('.field-sysuserhotel-email').hide();
                $('#createUserHotelModal').modal('show');
            }
        });
    });

    body.on('submit', '#UserHotelForm', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        formSend(model_url, data, model_id);
    });
    // End SysUserHotel

    body.on('click', '#unblock', function (e) {
        e.preventDefault();
        var userId = $(this).attr('data-user'),
            host = location.pathname,
            url = host.substring(0, host.length - 5) + 'unblock';

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                userId: userId
            },
            beforeSend: function () {
                $(this).prop('disabled', true);
            }
        });
    });
});