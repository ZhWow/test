<?php

namespace backend\modules\api\services;

 use Yii;
use yii\db\ActiveQuery;
use modules\organisation\models\flights\TTickets;
use backend\modules\hotels\models\HReservationConfirmed;
use backend\modules\api\models\RailSearch;
use backend\modules\api\models\Api;
use backend\modules\api\models\HotelSearch;
use backend\modules\api\models\AmadeusIntegra;
use modules\organisation\models\rails\RRefundtickets;
use modules\organisation\models\rails\RTickets;

class TicketRequest
{
    /**
     * The flight return AmadeusIntegra model
     * @param $param
     * @return array|null|\yii\db\ActiveRecord[]
     */
    public static function flight(API $param)
    {
        $category = $param->category;
        $result = null;
        $client = new AmadeusIntegra();
        $client->setScenario('get_tickets');
        $sendAttributes = array_change_key_case((array)json_decode($param['searchData']), CASE_UPPER);
        $client->attributes = $sendAttributes;

        if (!$client->validate() || (empty($sendAttributes) == false && Methods::issetArray($client->attributes) == true)) {
            $result['error'] = $client->getErrors();
            if (empty($result['error'])) $result['error'] = 'Wrong param name';
            return $result;
        }

        $lastId = Methods::getLastId($param->userId, 0, 1);
        $query = $client->find()
            ->from('manager.t_amadeus_integra as i')
            ->select(Methods::airSelectList($param->userId))
            ->leftJoin('t_corp c', 'c.id = i.corp_id');

        if (!empty($category['column_name']) && !empty($category['searchData'])) $query->andWhere([$category['column_name'] => $category['searchData']]);
        if (Methods::issetArray($client->attributes)) $query->andWhere('i.id > :id', [':id' => $lastId]);
        if (!empty($client->FIRST_NAME)) $query->andWhere('i.first_name like :f_name', [':f_name' => $client->FIRST_NAME . '%']);
        if (!empty($client->LAST_NAME)) $query->andWhere('i.last_name like :l_name', [':l_name' => $client->LAST_NAME . '%']);
        if (!empty($client->DEPARTURE_DATE)) $query->andWhere('i.departure_date=:d_date', [':d_date' => $client->DEPARTURE_DATE]);
        if (!empty($client->ARRIVAL_DATES)) $query->andWhere('i.arrival_dates LIKE :a_date', [':a_date' => '%' . str_replace('-', '', $client->ARRIVAL_DATES . '%')]);
        if (!empty($client->DOCUMENT_ISSUED)) $query->andWhere('i.document_issued = :d_issued', [':d_issued' => $client->DOCUMENT_ISSUED]);
        if (!empty($client->RECORD_LOCATOR)) $query->andWhere('i.record_locator = :record_locator', [':record_locator' => $client->RECORD_LOCATOR]);
        if (Methods::checkUserCan($param->userId, 'api_btm')) $query->andWhere(['not in', 'i.CORP_ID', Yii::$app->params['api']['btm_admin_excl_corps']]);

        $result = $query->asArray()->all();

        if ($result) {
            foreach ($result as $key => $value) {
                if (!empty($value['BASE'])) {
                    $fare = TTickets::search($value['BASE'])->select('ServiceFee as SERVICE_FEE, (ServiceFee * 12)/112 as SERVICE_FEE_EQ_VAT, formOfPayment as FORM_OF_PAYMENT_2 ')->WHERE(['id' => $value['TICKET_ID']])->asArray()->limit(1)->one();
                    if ($fare) {
                        if (strpbrk($value["ROUTE_COUNTRY"], 'KZ')) {
                            $fare["SERVICE_FEE_EQ_VAT"] = (string)round($fare["SERVICE_FEE_EQ_VAT"], 2);
                        } else {
                            $fare["SERVICE_FEE_EQ_VAT"] = 0;
                        }
                        $result[$key]['SERVICE_FEE_EQ'] = $fare['SERVICE_FEE'];
                        $result[$key]['SERVICE_FEE_EQ_VAT'] = $fare['SERVICE_FEE_EQ_VAT'];
                        $result[$key]['FORM_OF_PAYMENT_2'] = $fare['FORM_OF_PAYMENT_2'];
                    }
                }
            }
        }

        $corpsId[] = Methods::genCorpLastId(0, $result);
        if (Methods::issetArray($client->attributes)) Methods::updateLastId($param->userId, $corpsId, 1);

        return $result;
    }

    public static function getFlightTicketsStatus(API $param)
    {
        $result = [];
        $childrenResult = [];
        $parentResult = [];
        $client = new AmadeusIntegra();
        $client->setScenario('get_ticket_status');
        $client->attributes = array_change_key_case((array)json_decode($param['searchData']), CASE_UPPER);;

        if (!$client->validate()) {
            $result['error'] = $client->getErrors();
            return $result;
        }

        $query = $client->find()
            ->select(Methods::airSelectList($param->userId))
            ->from('manager.t_amadeus_integra as i')
            ->leftJoin('t_corp c', 'c.id = i.corp_id')
            ->andWhere(['in', 'document_number', $client->TICKET_NUMBER]);

        //if (Methods::checkUserCan($param->userId, 'api_btm')) $query->andWhere(['not in', 'i.CORP_ID', Yii::$app->params['api']['btm_admin_excl_corps']]);

        if (!empty($param->category['column_name']) && !empty($param->category['searchData'])) $query->andWhere([$param->category['column_name'] => $param->category['searchData']]);

        $tickets = $query->asArray()->all();
        if ($tickets) {
            foreach ($tickets as $item) {
                if (!empty($item['FO_NUMBER'])) {
                    Methods::getChildren($item['FO_NUMBER'], $childrenResult, $param->userId);
                }
            }
            $result = array_merge($tickets, $childrenResult);
        }

        $ticketFoNumber = AmadeusIntegra::find()
            ->select(Methods::airSelectList($param->userId))
            ->from('manager.t_amadeus_integra as i')
            ->leftJoin('t_corp c', 'c.id = i.corp_id')
            ->where(['fo_number' => $client->TICKET_NUMBER]);
        if (!empty($param->category['column_name']) && !empty($param->category['searchData'])) $ticketFoNumber->andWhere([$param->category['column_name'] => $param->category['searchData']]);

        $ticketFoNumber = $ticketFoNumber->asArray()->all();
        if ($ticketFoNumber) {
            foreach ($ticketFoNumber as $item) {
                Methods::getParent($item['DOCUMENT_NUMBER'], $parentResult, $param->userId);
            }
            $result = array_merge($result, $parentResult, $ticketFoNumber);
        }

        usort($result, function ($item1, $item2) {
            if ($item1['id'] == $item2['id']) return 0;
            return $item1['id'] < $item2['id'] ? -1 : 1;
        });

        if ($result) {
            foreach ($result as $key => $value) {
                $fare = TTickets::search($value['BASE'])->select('ServiceFee as SERVICE_FEE, (ServiceFee * 12)/112 as SERVICE_FEE_EQ_VAT, formOfPayment as FORM_OF_PAYMENT_2 ')->WHERE(['id' => $value['TICKET_ID']])->asArray()->limit(1)->one();
                if ($fare) {
                    if (strpbrk($value["ROUTE_COUNTRY"], 'KZ')) {
                        $fare["SERVICE_FEE_EQ_VAT"] = (string)round($fare["SERVICE_FEE_EQ_VAT"], 2);
                    } else {
                        $fare["SERVICE_FEE_EQ_VAT"] = 0;
                    }
                    $result[$key]['SERVICE_FEE_EQ'] = $fare['SERVICE_FEE'];
                    $result[$key]['SERVICE_FEE_EQ_VAT'] = $fare['SERVICE_FEE_EQ_VAT'];
                    $result[$key]['FORM_OF_PAYMENT_2'] = $fare['FORM_OF_PAYMENT_2'];
                }
            }
        }

        return $result;
    }

    /**
     * The hotel return HReservationConfirmed model
     * @param $param
     * @return array|null|\yii\db\ActiveRecord[]
     */
    public static function hotel(API $param)
    {
        $tickets = [];
        $corpsLastId = [];
        $corps = Methods::getCorp($param->userId);
        $search = new HotelSearch;
        $search->setScenario('get_ticket');
        $sendAttributes = (array)json_decode($param['searchData']);
        $search->attributes = $sendAttributes;

        if ((empty($sendAttributes) == false && Methods::issetArray($search->attributes) == true)) {
            $result['error'] = 'Wrong param name';
            return $result;
        }

        foreach ($corps as $k => $corp) {
            $lastId = Methods::getLastId($param->userId, $corp['id'], 2);
            $client = new HReservationConfirmed;
            $client->setDb($corp['base']);
            $query = $client->find()
                ->select(Methods::hotelSelectList('h'))
                ->from($corp['base'] . '.h_reservations_confirmed as h')
                ->leftJoin($corp['base'] . '.h_book as b', 'b.id = h.bookID')
                ->with([
                    'hotel' => function (ActiveQuery $query) {
                        $query->select('id, hotelName, address, telephone, email, latitude, longitude');
                    },
                    'room' => function (ActiveQuery $query) {
                        $query->select('id, Type, TypeDescription, RoomDescription');
                    },
                ]);

            if (Methods::issetArray($search->attributes)) $query->andFilterWhere(['>', 'h.id', $lastId]);
            if (!empty($corps[0]['searchHotelId'])) $query->andFilterWhere(['h.hotelID' => $corps[0]['searchHotelId']]);
            $query->andFilterWhere(['h.stars' => $search->star]);
            $query->andFilterWhere(['h.cityCode' => $search->cityCode]);
            $query->andFilterWhere(['like', 'b.paxNames', $search->firstName]);
            $query->andFilterWhere(['like', 'b.paxSurnames', $search->secondName]);

            $ticket = $query->asArray()->all();
            if (!empty($ticket)) {
                $tickets[] = $ticket;
                $corpsLastId[] = Methods::genCorpLastId($corp['id'], $ticket);
            }
        }

        Methods::updateLastId($param->userId, $corpsLastId, 2);
        return Methods::singleArray($tickets);
    }

    public static function getHotelStatus($param)
    {
        $tickets = [];
        $corps = Methods::getCorp($param->userId);;
        $search = new HotelSearch;
        $search->setScenario('get_ticket_status');
        $search->attributes = (array)json_decode($param['searchData']);

        if (!$search->validate()) {
            $result['error'] = $search->getErrors();
            return $result;
        }

        foreach ($corps as $k => $corp) {
            $client = new HReservationConfirmed;
            $client->setDb($corp['base']);
            $query = $client->find()
                ->select('hotel_bookingReference, isCancelled, penalty, hotelID, roomID, corpID, guest, checkInDateTime, checkOutDateTime')
                ->from($corp['base'] . '.h_reservations_confirmed')
                ->with([
                    'hotel' => function (ActiveQuery $query) {
                        $query->select('id, hotelName, address, telephone, email, latitude, longitude');
                    },
                    'room' => function (ActiveQuery $query) {
                        $query->select('id, Type, TypeDescription, RoomDescription');
                    },
                ])
                ->andWhere(['in', 'hotel_bookingReference', $search->bookingReference]);

            $ticket = $query->asArray()->all();

            if (!empty($ticket)) $tickets[] = $ticket;
        }

        return Methods::singleArray($tickets);
    }

    /**
     * The rail return RTickets model
     * @param Api $param
     * @return array|string
     */
    public static function rail(API $param)
    {
        $tickets = [];
        $corpsLastId = [];
        $refundTicketsAllCorps = [];
        $corps = Methods::getCorp($param->userId);;
        $sendAttributes = (array)json_decode($param['searchData']);
        $search = new RailSearch();
        $search->setScenario('get_ticket');
        $search->attributes = $sendAttributes;

        if (!$search->validate() || (empty($sendAttributes) == false && Methods::issetArray($search->attributes) == true)) {
            $result['error'] = $search->getErrors();
            if (empty($result['error'])) $result['error'] = 'Wrong param name';
            return $result;
        }

        foreach ($corps as $k => $corp) {

            $refundTickets = RRefundtickets::search($corp['base'])
                ->select('ticketId')
                ->asArray()
                ->all();

            if ($refundTickets) {
                $refundTicketsAllCorps = array_merge($refundTicketsAllCorps, $refundTickets);
            }

            $lastId = Methods::getLastId($param->userId, $corp['id'], 3);
            $client = new RTickets();
            $client->setDb($corp['base']);
            $query = $client->find()
                ->select(Methods::railSelectList($client, 'r'))
                ->from($corp['base'] . '.r_tickets as r')
                ->leftJoin($corp['base'] . '.r_passengers as p', 'p.passengerId = r.passengerId')
                ->with([
                    'owner' => function (ActiveQuery $query) {
                        $query->select('id, firstname, lastname, position');
                    },
                    'organisation' => function (ActiveQuery $query) {
                        $query->select('id, company, manager, support, contact');
                    },
                    'journey' => function (ActiveQuery $query) {
                        $query->select('bookingId, departureName, departureDate, departureTime, arrivalName, arrivalDate, arrivalTime, duration');
                    },
                    'passenger' => function (ActiveQuery $query) {
                        $query->select('passengerId, name, surname, documentType, documentNumber');
                    },
                    'seat' => function (ActiveQuery $query) {
                        $query->select('ticketId, carNumber, placeNumber, serviceClass, serviceType, placeType');
                    },
                    'station' => function (ActiveQuery $query) {
                        $query->select('bookingId, stationName, stationArrivalTime, stationDepartureTime, stationStayTime');
                    },
                    'fare' => function (ActiveQuery $query) {
                        $query->select('fareId, netFare, markup, resellerMarkup');
                    },
                    'booking' => function (ActiveQuery $query) {
                        $query->select('bookingId, status, operatorBookingId, createDate, createTime');
                    },
                    'segments' => function (ActiveQuery $query) {
                        $query->select('bookingId, operatorName, vendorName, carrierName');
                    },
                ]);

            if (Methods::issetArray($search->attributes)) $query->andWhere('r.id > :id', [':id' => $lastId]);
            if (!empty($search->firstName)) $query->andWhere('p.name like :firstName', [':firstName' => $search->firstName]);
            if (!empty($search->secondName)) $query->andWhere('p.surname=:secondName', [':secondName' => $search->secondName]);

            $ticket = $query->asArray()->all();

            if (!empty($ticket)) {
                $deleteRefund = array_column($refundTicketsAllCorps, 'ticketId');
//                foreach ($ticket as $key => $i) {
//                    if (in_array($i['ticketId'], $deleteRefund)) {
//                        $ticket[$key]['status'] = 'refunded';
//                    } else {
//                        $ticket[$key]['status'] = 'issued';
//                    }
//                }

                foreach ($ticket as $key => $i) {
                    if (in_array($i['ticketId'], $deleteRefund)) {
                        $ticket[$key]['status'] = 'refunded';
                    } else {
                        if ($ticket[$key]['booking']['status'] == 'OK') {
                            $ticket[$key]['status'] = 'issued';
                        } else {
                            unset($ticket[$key]);
                        }
                    }
                }
                $tickets[] = $ticket;
                $corpsLastId[] = Methods::genCorpLastId($corp['id'], $ticket);
            }
        }

        Methods::updateLastId($param->userId, $corpsLastId, 3);
        return Methods::singleArray($tickets);
    }

    public static function getRailTicketStatus(API $param)
    {
        $tickets = [];
        $corps = Methods::getCorp($param->userId);;
        $search = new RailSearch();
        $search->setScenario('get_ticket_status');
        $search->attributes = (array)json_decode($param['searchData']);

        if (!$search->validate()) {
            $result['error'] = $search->getErrors();
            return $result;
        }

        $refundTicketsAllCorps = [];

        foreach ($corps as $k => $corp) {

            if (!empty($search->ticketId)) {
                $refundTickets = RRefundtickets::search($corp['base'])
                    ->select('ticketId')
                    ->from($corp['base'] . '.r_refundtickets')
                    ->where(['in', 'ticketId', $search->ticketId])
                    ->asArray()->all();
                if ($refundTickets) {
                    $refundTicketsAllCorps = array_merge($refundTicketsAllCorps, $refundTickets);
                }
            }

            $client = new RTickets();
            $client->setDb($corp['base']);
            $query = $client->find()
                ->select(Methods::railSelectList($client, 'r'))
                ->from($corp['base'] . '.r_tickets as r')
                ->with([
                    'owner' => function (ActiveQuery $query) {
                        $query->select('id, firstname, lastname, position');
                    },
                    'organisation' => function (ActiveQuery $query) {
                        $query->select('id, company, manager, support, contact');
                    },
                    'journey' => function (ActiveQuery $query) {
                        $query->select('bookingId, departureName, departureDate, departureTime, arrivalName, arrivalDate, arrivalTime, duration');
                    },
                    'passenger' => function (ActiveQuery $query) {
                        $query->select('passengerId, name, surname, documentType, documentNumber');
                    },
                    'seat' => function (ActiveQuery $query) {
                        $query->select('ticketId, carNumber, placeNumber, serviceClass, serviceType, placeType');
                    },
                    'station' => function (ActiveQuery $query) {
                        $query->select('bookingId, stationName, stationArrivalTime, stationDepartureTime, stationStayTime');
                    },
                    'fare' => function (ActiveQuery $query) {
                        $query->select('fareId, netFare, markup, resellerMarkup');
                    },
                    'booking' => function (ActiveQuery $query) {
                        $query->select('bookingId, status, operatorBookingId, createDate, formOfPayment, createTime');
                    },
                    'segments' => function (ActiveQuery $query) {
                        $query->select('bookingId, operatorName, vendorName, carrierName');
                    },
                    'refund' => function (ActiveQuery $query) {
                        $query->select('fareId, netFare, markup, resellerMarkup');
                    },
                ])
                ->andWhere(['in', 'r.ticketId', $search->ticketId]);

            if (!empty($search->ticketSupplierId)) $query->orWhere(['in', 'r.ticketSupplierId', $search->ticketSupplierId]);
            $ticket = $query->asArray()->all();

            if (!empty($ticket)) $tickets[] = $ticket;
        }

        $result = Methods::singleArray($tickets);
        $deleteRefund = array_column($refundTicketsAllCorps, 'ticketId');

        foreach ($result as $k => $i) {
            if (in_array($i['ticketId'], $deleteRefund)) {
                $result[$k]['status'] = 'refunded';
            } else {
                if ($result[$k]['booking']['status'] == 'OK') {
                    $result[$k]['status'] = 'issued';
                } else {
                    unset($result[$k]);
                }
            }
        }

        return $result;
    }
}
