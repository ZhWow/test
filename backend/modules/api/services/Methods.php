<?php

namespace backend\modules\api\services;

use Yii;
use common\models\User;
use backend\models\Corp;
use backend\modules\hotels\models\HReservationConfirmed;
use backend\modules\api\models\AmadeusIntegra;
use backend\modules\api\models\SysUserAgency;
use backend\modules\api\models\SysUserCorp;
use backend\modules\api\models\SysUserHotel;
use backend\modules\api\models\ApiResponseHistory;

class Methods
{
    public static function isEmpty($param)
    {
        $result = true;
        if (!empty($param) && $param != '') $result = false;

        return $result;
    }

    /***
     * The getCategory returns user exists in tables (SysUserCorp, SysUserAgency, SysUserHotel)
     * @param $user_id
     * @return array|bool|null|\yii\db\ActiveRecord
     */
    public static function getCategory($user_id)
    {
        $result = false;

        $corp = SysUserCorp::find()
            ->select('corp_id, user_id')
            ->where('user_id=:user_id', [':user_id' => $user_id])
            ->andWhere(['is_api' => 1])
            ->asArray()
            ->all();

        $agent = SysUserAgency::find()
            ->select('oid as FIRST_OWNER_OFFICEID, user_id, agency_id')
            ->innerJoin('manager.t_agency_oids as a', 'a.id = sys_user_agency.agency_id')
            ->where('user_id=:user_id', [':user_id' => $user_id])
            ->andWhere(['is_api' => 1])
            ->asArray()
            ->all();

        $hotel = SysUserHotel::find()
            ->where('user_id=:user_id', [':user_id' => $user_id])
            ->andWhere(['is_api' => 1])
            ->asArray()
            ->all();

        if ($hotel) {
            $result['table_name'] = 'sys_user_hotel';
        } elseif ($agent) {
            $result['table_name'] = 'sys_user_agency';
            $result['column_name'] = 'FIRST_OWNER_OFFICEID';
            $result['searchData'] = array_column($agent, 'FIRST_OWNER_OFFICEID');
        } elseif ($corp) {
            $result['table_name'] = 'sys_user_corp';
            $result['column_name'] = 'CORP_ID';
            $result['searchData'] = array_column($corp, 'corp_id');
        } elseif (self::checkUserCan($user_id, 'api_btm')) {
            $result = true;
        }

        return $result;
    }

    /**
     * The getLastId returns last search id
     * @param $userId
     * @param $corp
     * @param $providerId
     * @return int|mixed
     */
    public static function getLastId($userId, $corp, $providerId)
    {
        $model = ApiResponseHistory::find()->where(['corp_id' => $corp, 'user_id' => $userId, 'provider_id' => $providerId])->asArray()->one();
        return (isset($model['last_id'])) ? $model['last_id'] : 0;
    }

    public static function airSelectList($userId)
    {
        $result = self::airList('i') . ', c.base as BASE';

        if (self::checkUserCan($userId, 'api_service_fee')) {
            $result .= ', i.SERVICE_FEE as SERVICE_FEE_AMA , i.SERVICE_FEE_VAT as SERVICE_FEE_AMA_VAT';
        }

        return $result;
    }

    public static function airList($selector = null)
    {
        $result = null;
        $Client = new AmadeusIntegra();
        $fields = $Client->getAttributes();
        unset(
            $fields['CONFLICT'], $fields['REMARK1'], $fields['REMARK2'], $fields['REMARK3'], $fields['REMARK4'], $fields['REMARK5'], $fields['REMARK6'],
            $fields['REMARK7'], $fields['REMARK8'], $fields['REMARK9'], $fields['REMARK10'], $fields['REMARK11'], $fields['REMARK12'], $fields['REMARK13'],
            $fields['REMARK14'], $fields['REMARK15'], $fields['FORMAT_VERSION'], $fields['PROCESSED'], $fields['UPDATE'], $fields['CLIENT_ACCOUNT_NUMBER'],
            $fields['ERR_MESSAGE'], $fields['CONFLICT'], $fields['created_by'], $fields['updated_by'], $fields['UPDATE'], $fields['CLIENT_ACCOUNT_NUMBER'],
            $fields['SERVICE_FEE'], $fields['SERVICE_FEE_VAT']
        );

        $list = array_keys($fields);
        foreach ($list as $i) {
            if (!empty($selector)) {
                $result .= $selector . '.' . $i . ', ';
            } else {
                $result .= $i . ', ';
            }

        }
        return $result;
    }

    /**
     * The updateLastId updated last search id
     * @param $userId
     * @param $corpsId
     * @param $providerId
     * @return bool
     */
    public static function updateLastId($userId, $corpsId, $providerId)
    {
        $result = false;
        if (!empty($userId) && !empty($corpsId)) {
            foreach ($corpsId as $item) {
                if (isset($item['last_id']) && $item['last_id'] == 0) return $result;
                $model = ApiResponseHistory::find()->where(['corp_id' => $item['corp_id'], 'user_id' => $userId, 'provider_id' => $providerId])->one();
                if (empty($model)) $model = new ApiResponseHistory();
                $model->provider_id = $providerId;
                $model->corp_id = $item['corp_id'];
                $model->user_id = (int)$userId;
                $model->last_id = $item['last_id'];
                if ($model->validate() && $model->save()) {
                    $result = true;
                } else {
                    $result = false;
                    Yii::error($model->getErrors(), 'api_error');
                }
            }
        }
        return $result;
    }

    /**
     * Unblock changes the status of the user to the active
     * @param integer $userId
     * @param string $error_case
     * @return bool
     */
    public static function unblockUser($userId, $error_case)
    {
        $result = false;
        $user = User::find()
            ->where('id = :userId', [':userId' => $userId])
            ->andWhere(['status' => '0'])
            ->one();

        if (empty($user)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User already has been unblocked'));
            return $result;
        }
        /* @noinspection PhpUndefinedFieldInspection */
        $user->status = 10;
        if ($user->save()) {
            $result = true;;
            /* @noinspection PhpUndefinedFieldInspection */
            Yii::$app->cache->delete('psw_err_' . $user->email);
            Yii::$app->session->setFlash('success', Yii::t('app', 'User has unblocked'));
        } else {
            Yii::error($user->getErrors(), $error_case);
            Yii::$app->session->setFlash('error', Yii::t('app', 'Error to unblock a user'));
        }

        return $result;
    }

    public static function hotelSelectList($selector)
    {
        $result = null;
        $client = new HReservationConfirmed();
        $fields = $client->getAttributes();

        unset(
            $fields['isCancelled'], $fields['sendTo'], $fields['attachmentURL'], $fields['confirmationDate']
        );

        $i = 0;
        $fields = array_keys($fields);
        foreach ($fields as $k => $item) {
            $result .= $selector . '.' . $item;
            ($i == $k && $k != count($fields) - 1) ? $result .= ', ' : $result .= ' ';
            $i++;
        }

        $result .= ', b.paxNames, b.paxSurnames, b.payType';
        return $result;
    }

    public static function genCorpLastId($corp, array $ticket)
    {
        $result = [];
        $result['corp_id'] = $corp;
        $result['last_id'] = (int)array_pop($ticket)['id'];
        return $result;
    }

    public static function singleArray(array $tickets)
    {
        $result = [];

        array_map(function ($a) use (&$result) {
            $result = array_merge($result, $a);
        }, $tickets);

        return $result;
    }

    public static function checkUserCan($userId, $permission)
    {
        $user = User::findOne($userId);
        Yii::$app->user->login($user);
        $result = Yii::$app->user->can($permission);
        Yii::$app->user->logout();

        return $result;
    }

    public static function getCorp($userId)
    {
        $corps = false;
        $btmStatus = (self::checkUserCan($userId, 'api_btm'));
        $corpStatus = SysUserCorp::find()->select('corp_id')->where(['user_id' => $userId, 'is_api' => 1])->column();
        $hotelStatus = SysUserHotel::find()->select('hotel_id')->where(['user_id' => $userId, 'is_api' => 1])->column();
        if ($btmStatus) {
            $corps = Corp::find()->select('id, base')->asArray()->all();
        } elseif (!empty($hotelStatus)) {
            $corps = Corp::find()->select('id, base')->asArray()->all();
            $corps[0]['searchHotelId'] = $hotelStatus;
        } elseif (!empty($corpStatus)) {
            $corps = Corp::find()->select('id, base')->where(['id' => $corpStatus])->asArray()->all();
        }

        return $corps;
    }

    public static function railSelectList($object, $selector)
    {
        $result = null;
        /* @noinspection PhpUndefinedMethodInspection */
        $fields = $object->getAttributes();

        unset(
            $fields['isCancelled'], $fields['coupons'], $fields['ticketType']
        );

        $i = 0;
        $fields = array_keys($fields);
        foreach ($fields as $k => $item) {
            $result .= $selector . '.' . $item;
            ($i == $k && $k != count($fields) - 1) ? $result .= ', ' : $result .= ' ';
            $i++;
        }

        return $result;
    }

    public static function getParent($docNumber, &$result, $userId)
    {
        $parent = AmadeusIntegra::find()
            ->select(Methods::airSelectList($userId))
            ->from('manager.t_amadeus_integra as i')
            ->leftJoin('t_corp c', 'c.id = i.corp_id')
            ->where(['FO_NUMBER' => $docNumber])
            ->asArray()
            ->all();

        if ($parent) {
            $result[] = $parent[0];
            if (!empty($parent[0]['FO_NUMBER'])) self::getParent($parent[0]['DOCUMENT_NUMBER'], $result, $userId);

        }
        return $result;
    }

    public static function getChildren($foNumber, &$result, $userId)
    {
        $children = AmadeusIntegra::find()
            ->select(Methods::airSelectList($userId))
            ->from('manager.t_amadeus_integra as i')
            ->leftJoin('t_corp c', 'c.id = i.corp_id')
            ->where(['document_number' => $foNumber])
            ->asArray()
            ->all();

        if ($children) {
            $result[] = $children[0];
            if (!empty($children[0]['FO_NUMBER'])) self::getChildren($children[0]['FO_NUMBER'], $result, $userId);
        }
        return $result;
    }

    public static function issetArray($array)
    {
        $status = array_filter($array, 'strlen');
        return empty($status);
    }
}