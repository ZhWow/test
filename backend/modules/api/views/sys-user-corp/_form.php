<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\models\Corp;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\SysUserCorp */
/* @var $form yii\widgets\ActiveForm */

$company = Corp::find()->select(['id', 'company'])->asArray()->all();
?>

<div class="sys-user-corp-form">

    <?php $form = ActiveForm::begin(
        [
            'id' => 'UserCorpForm',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validationUrl' => Url::toRoute('/api/sys-user-corp/validation'),
        ]
    ); ?>

    <?= Html::activeHiddenInput($model, 'user_id') ?>

    <?= $form->field($model, 'email')->widget(AutoComplete::classname(), [
        'clientOptions' => [
            'source' => Url::toRoute('/api/base/user-auto-complete'),
            'autoFill' => true,
            'minLength' => '3',
            'select' => new JsExpression("function( event, ui ) {
                $('#sysusercorp-user_id').val(ui.item.id);
            }"),
            'success' => new JsExpression("function( data ) {
                response($.map(data, function (item) {
                    return item.name;
                }))
            }"),
        ],
        'options' => [
            'class' => 'form-control'
        ],

    ]) ?>

    <?= $form->field($model, 'corp_id', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->dropDownList(ArrayHelper::map($company, 'id', 'company'), ['prompt' => Yii::t('app', 'Select a company:')])->label(Yii::t('app', 'Corp name')) ?>

    <?= $form->field($model, 'is_api', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->checkbox([], false)->label('Is api') ?>

    <div class="form-group">
        <?= Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success', 'id' => 'sendBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
