<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\SysUserAgency */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="sys-user-agency-form">

    <?php $form = ActiveForm::begin([
        'id' => 'UserAgencyForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validationUrl' => Url::toRoute('/api/sys-user-agency/validation'),
    ]); ?>

    <?= Html::activeHiddenInput($model, 'user_id') ?>

    <?= $form->field($model, 'email')->widget(AutoComplete::classname(), [
        'clientOptions' => [
            'source' => Url::toRoute('/api/base/user-auto-complete'),
            'autoFill' => true,
            'minLength' => '3',
            'select' => new JsExpression("function( event, ui ) {
                $('#sysuseragency-user_id').val(ui.item.id);
            }"),
            'success' => new JsExpression("function( data ) {
                response($.map(data, function (item) {
                    return item.name;
                }))
            }"),
        ],
        'options' => [
            'class' => 'form-control'
        ],

    ]) ?>

    <?= Html::activeHiddenInput($model, 'agency_id') ?>

    <?= $form->field($model, 'agency_oid')->widget(AutoComplete::classname(), [
        'clientOptions' => [
            'source' => Url::toRoute('/api/sys-user-agency/oid-auto-complete'),
            'autoFill' => true,
            'minLength' => '3',
            'select' => new JsExpression("function( event, ui ) {
                $('#sysuseragency-agency_id').val(ui.item.id);
            }"),
            'success' => new JsExpression("function( data ) {
                response($.map(data, function (item) {
                    return item.name;
                }))
            }"),
        ],
        'options' => [
            'class' => 'form-control'
        ],

    ]) ?>

    <?= $form->field($model, 'is_api', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->checkbox([], false)->label('Is api') ?>

    <div class="form-group">
        <?= Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success', 'id' => 'sendBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
