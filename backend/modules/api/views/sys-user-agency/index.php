<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;
use backend\models\AgencyOids;
use backend\modules\api\assets\ApiAsset;
use backend\modules\api\models\SysUserAgency;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\api\models\SysUserAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$bundle = ApiAsset::register($this);
$this->title = Yii::t('app', 'Sys User Agencies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sys-user-agency-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::button(Yii::t('app', 'Create'), ['class' => 'btn btn-success', 'id' => 'createUserAgency', 'data-toggle' => 'modal', 'data-target' => '#createUserAgencyModal']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'email',
                'value' => 'user.email',
            ],
            [
                'attribute' => 'agency_id',
                'value' => 'agency.OID',
                'filter' => AgencyOids::find()->select(['oid', 'id'])->indexBy('id')->column(),
            ],
            [
                'attribute' => 'is_api',
                'value' => 'is_api',
                'filter' => [0, 1],
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return User::getStatus($model->user->status);
                }
            ],
            [
                'format' => 'raw',
                'value' => function ($model) {
                    $button = '<div class="btn-center">';
                    $button .= Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil btn-s', 'id' => 'updateUserAgency', 'title' => 'Update', 'aria-label' => 'Update']);
                    $button .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']), ['delete', 'id' => $model->id], ['title' => 'Delete', 'aria-label' => 'Delete', 'style' => 'margin-left: 5px', 'data-confirm' => Yii::t('app', 'Do you want to delete' . '?')]);
                    $button .= Html::tag('span', '', ['class' => 'glyphicon glyphicon-erase btn-s', 'id' => 'unblock', 'data-user' => $model->user->id, 'title' => 'Unblock', 'aria-label' => 'Unblock']);
                    $button .= '</div>';
                    return $button;
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>


<?php Modal::begin([
    'options' => [
        'id' => 'createUserAgencyModal',
    ],
    'header' => Yii::t('app', 'Set user into agency'),
]); ?>

<?php
$userCorp = new SysUserAgency();
echo $this->render('_form', ['model' => $userCorp]);
?>

<?php Modal::end(); ?>


