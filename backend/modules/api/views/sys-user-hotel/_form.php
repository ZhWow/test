<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\SysUserHotel */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="sys-user-hotel-form">

    <?php $form = ActiveForm::begin(
        [
            'id' => 'UserHotelForm',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validationUrl' => Url::toRoute('/api/sys-user-hotel/validation'),
        ]
    ); ?>

    <?= Html::activeHiddenInput($model, 'user_id') ?>

    <?= $form->field($model, 'email')->widget(AutoComplete::classname(), [
        'clientOptions' => [
            'source' => Url::toRoute('/api/base/user-auto-complete'),
            'autoFill' => true,
            'minLength' => '2',
            'select' => new JsExpression("function( event, ui ) {
                $('#sysuserhotel-user_id').val(ui.item.id);
            }")
        ],
        'options' => [
            'class' => 'form-control'
        ]
    ]) ?>

    <?= Html::activeHiddenInput($model, 'hotel_id') ?>

    <?= $form->field($model, 'hotel_name')->widget(AutoComplete::classname(), [
        'clientOptions' => [
            'source' => Url::toRoute('/api/sys-user-hotel/hotel-auto-complete'),
            'autoFill' => true,
            'minLength' => '3',

            'select' => new JsExpression("function( event, ui ) {
                $('#sysuserhotel-hotel_id').val(ui.item.id);
            }"),
            'success' => new JsExpression("function( data ) {
                response($.map(data, function (item) {
                    return item.name;
                }))
            }"),
        ],
        'options' => [
            'class' => 'form-control'
        ],

    ]) ?>

    <?= $form->field($model, 'is_api', ['enableClientValidation' => true, 'enableAjaxValidation' => false])->checkbox([], false) ?>

    <div class="form-group">
        <?= Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success', 'id' => 'sendBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
