<?php
/**
 * User: Qway
 * Date: 17.01.2018
 * Time: 15:30
 */
/* @var $results array */

use dosamigos\tableexport\ButtonTableExport;
use yii\helpers\Html;

\backend\modules\agent\assets\AgentAsset::register($this);
?>

<?php
$tempJS = <<<JS
$('body').on('click', '#download_xls', function (){
    $("#table_to_xls").table2excel({
        // exclude CSS class
        exclude: ".noExl",
        name: "Conversion Report",
        filename: "CoversionReport" // Тут надо указать имя файла... когда они будут известны!
    });
});

$('body').on('click', '.detail', function (){
    var corp = $(this).attr("data-corp"),
        dateStart = $("#corpconversionreportform-startdaterange").val(),
        dateEnd = $("#corpconversionreportform-enddaterange").val(),
        url = 'corp-conversion/'

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data: {
            request: "getDetail",
            corp: corp,
            dateStart: dateStart,
            dateEnd: dateEnd
        },
        beforeSend: function(xhr){
            $("#hiden_detail_report").empty();
        },
        success: function(data){
            $("#hiden_detail_report").html(data);

            $("#detailReport").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                name: "Conversion Report Detail",
                filename: "CoversionReport_Detail" // Тут надо указать имя файла... когда они будут известны!
            });
        }
    });

});




JS;
$this->registerJs($tempJS);
?>

<!--<pre>
    <?php /*print_r($results)*/?>
</pre>-->
<div class="row" style="margin-top: 15px">
    <div class="col-md-12">
        <table border="1"align="center" style="width: 100%; text-align: center;">
            <thead>
            <tr>
                <th></th>
                <th></th>
                <th style="text-align: center;">Total</th>

                <?php foreach ($results['list_month'] as $result):?>
                    <th style="text-align: center;"><?=$result?></th>
                <?php endforeach;?>
            </tr>

            </thead>

            <?php foreach ($results as $key => $corp) :?>
                <?php if ($key != 'list_month' && $key != 'months'): ?>
                    <tbody style="margin-bottom: 20px; text-align: center;">
                    <tr>
                        <td rowspan="4" style="text-align: center">
                            <?=$key?> <br/>
                            <?=Html::button('Подробнее',['class' => 'detail btn btn-link', 'data-corp' => $key])?>
                        </td><!-- corp name -->
                        <td>Offline</td>
                        <td><?=$corp['offline_total']?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=$value['offline']?></td>
                        <?php endforeach;?>
                    </tr>
                    <tr>
                        <td>Online</td>
                        <td><?=$corp['online_total']?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=$value['online']?></td>
                        <?php endforeach;?>
                    </tr>
                    <tr>
                        <td>Conversion</td>
                        <td><?= round($corp['conversion'])?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=round($value['total'])?></td>
                        <?php endforeach;?>
                    </tr>
                    </tbody>
                <?php endif;?>
            <?php endforeach;?>
        </table>
    </div>
</div>

<!-- HIDDEN TABLE TO XLS !-->

<div class="col-md-12" style="display: none;">
    <table border="1"align="center" id="table_to_xls" style="width: 100%;">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th>Total</th>

            <?php foreach ($results['list_month'] as $result):?>
                <th><?=$result?></th>
            <?php endforeach;?>
        </tr>

        </thead>

        <?php foreach ($results as $key => $corp) :?>
            <?php if ($key != 'list_month' && $key != 'months'): ?>
                <tbody>
                    <tr>
                        <td rowspan="4"><?=$key?></td><!-- corp name -->
                        <td>Offline</td>
                        <td><?=$corp['offline_total']?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=$value['offline']?></td>
                        <?php endforeach;?>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Online</td>
                        <td><?=$corp['online_total']?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=$value['online']?></td>
                        <?php endforeach;?>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Conversion</td>
                        <td><?= round($corp['conversion'])?></td>
                        <?php foreach ($corp['months'] as $month_name => $value) :?>
                            <td><?=round($value['total'])?></td>
                        <?php endforeach;?>
                    </tr>
                 </tbody>
            <?php endif;?>
        <?php endforeach;?>
    </table>
</div>

<!-- Hidden table detail report !-->

<div id="hiden_detail_report" style="display: none;"></div>
