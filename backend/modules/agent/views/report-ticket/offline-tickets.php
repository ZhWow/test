<?php
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        'DOCUMENT_NUMBER',
        'RECORD_LOCATOR',
        'DOCUMENT_ISSUED',
        [
            'attribute' => 'CORP_BASE',
            'value' => function ($model) {
                return ($model->corp != null) ? $model->corp->base : $model->CORP_ID;
            }
        ],
        [
            'attribute' => 'AGENCY_NAME',
            'value' => function ($model) {
                return $model->agency->AgencyName;
            }
        ],
        [
            'attribute' => 'UPDATED_BY',
            'value' => function ($model) {
                $signed = \backend\modules\integra\models\Signed::find()->where(['userSign' => substr($model->BOOKING_SINE, 0, -2)])->one();
                $result = '';
                if ( $signed ) $result .= $signed->userName;// . " / ";
//                $result .= $model->updatedByUser->username;
                return $result;
            }
        ],
    ],
    'hover' => true,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Offline Report</h3>',
        'type'=>'success',
        'before' => $this->render('_offline_stats',['counter' => $counter]), //'<div> Today Bought ' . $counter["todayBought"] . '; Bought Online '. $counter["todayOnline"] .'; Bought Offline '. $counter["todayOffline"] .'</div>',
//        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
//        'beforeGrid'=>'My fancy content before.',
//        'afterGrid'=>'My fancy content after.',
    ]
]);
Pjax::end();

?>