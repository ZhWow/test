<?php

use backend\modules\agent\services\TicketWeek;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use backend\modules\agent\models\CorpReportFilterForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $corps array */
/* @var $totalTicket */
/* @var $ticketCountWeek */
/* @var $ticketWeekResultPhone */
/* @var $timeFilter */

$this->params['breadcrumbs'][] = Yii::t('app', 'Flight Report');
$filterForm = new CorpReportFilterForm();

$keys = $dataProvider->allModels[0]['week'];

$header0 = TicketWeek::getStartEndDate($keys[0], $timeFilter['year']) . '  ' . $ticketCountWeek[$keys[0]] . ' / ' . $ticketWeekResultPhone['phone_' . $keys[0]];
$header1 = TicketWeek::getStartEndDate($keys[1], $timeFilter['year']) . '  ' . $ticketCountWeek[$keys[1]] . ' / ' . $ticketWeekResultPhone['phone_' . $keys[1]];
$header2 = TicketWeek::getStartEndDate($keys[2], $timeFilter['year']) . '  ' . $ticketCountWeek[$keys[2]] . ' / ' . $ticketWeekResultPhone['phone_' . $keys[2]];
$header3 = TicketWeek::getStartEndDate($keys[3], $timeFilter['year']) . '  ' . $ticketCountWeek[$keys[3]] . ' / ' . $ticketWeekResultPhone['phone_' . $keys[3]];
$header4 = (isset($keys[4])) ? TicketWeek::getStartEndDate($keys[4], $timeFilter['year']) . '  ' . $ticketCountWeek[$keys[4]] . ' / ' . $ticketWeekResultPhone['phone_' . $keys[4]] : '';
?>

<?php $form = ActiveForm::begin([
    'class' => 'row',
    'method' => 'GET',
    'action' => '/agent/report-ticket/air'
]); ?>

    <div class="col-md-4">
        <?= $form->field($filterForm, 'month')->dropDownList(CorpReportFilterForm::getMonthList(), [
            'prompt' => Yii::t('app', 'Month'),
            'options' => [$timeFilter['month'] => ['Selected' => true]]
        ])->label(false) ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($filterForm, 'year')->dropDownList(CorpReportFilterForm::getYearList(), [
            'prompt' => Yii::t('app', 'Year'),
            'options' => [$timeFilter['year'] => ['Selected' => true]]
        ])->label(false) ?>
    </div>

    <div class="form-group">,
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

    <div class="total-ticket">
    <span style="font-size: 12px;">
        <?= Yii::t('app', 'Total ticket') ?> : <b><?= $totalTicket ?></b>
    </span>
    </div>

<?php
$gridColumns = [
    'company',
    [
        'format' => 'raw',
        'value' => function ($model) {
            return isset($model['ticketWeek'][$model['week'][0]]) ? $model['ticketWeek'][$model['week'][0]] . ' / ' . $model['ticketCountCorpPhone']['phone_' . $model['week'][0]] : 0;
        },
        'label' => $header0,
    ],
    [
        'label' => $header1,
        'value' => function ($model) {
            return isset($model['ticketWeek'][$model['week'][1]]) ? $model['ticketWeek'][$model['week'][1]] . ' / ' . $model['ticketCountCorpPhone']['phone_' . $model['week'][1]] : 0;
        }
    ],
    [
        'label' => $header2,
        'value' => function ($model) {
            return isset($model['ticketWeek'][$model['week'][2]]) ? $model['ticketWeek'][$model['week'][2]] . ' / ' . $model['ticketCountCorpPhone']['phone_' . $model['week'][2]] : 0;
        }
    ],
    [
        'label' => $header3,
        'value' => function ($model) {
            return isset($model['ticketWeek'][$model['week'][3]]) ? $model['ticketWeek'][$model['week'][3]] . ' / ' . $model['ticketCountCorpPhone']['phone_' . $model['week'][3]] : 0;
        }
    ],
    [
        'label' => $header4,
        'value' => function ($model) {
            return isset($model['ticketWeek'][$model['week'][4]]) ? $model['ticketWeek'][$model['week'][4]] . ' / ' . $model['ticketCountCorpPhone']['phone_' . $model['week'][4]] : 0;
        }
    ],
    [
        'label' => Yii::t('app', 'count'),
        'value' => function ($model) {
            return $model['count'] . ' / ' . $model['countPhone'];
        }
    ]
];
if (!isset($keys[4])) unset($gridColumns[5]);

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => $gridColumns
]);
Pjax::end();
?>