<?php
/**
 * User: Qway
 * Date: 15.01.2018
 * Time: 15:04
 */

/* @var $results array */
/* @var $flag boolean */
use kartik\date\DatePicker;
use backend\modules\agent\models\CorpConversionReportForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$CorpConversionReportFormModel = new CorpConversionReportForm();

?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($CorpConversionReportFormModel, 'startDateRange')->widget(
                DatePicker::classname(),
                [
                    'value' => Yii::$app->request->post('startDateRange'),
                    'options' => ['placeholder' => 'Select issue date ...', 'value' => $_POST['CorpConversionReportForm']['startDateRange']],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true
                    ]
                ]
            );
            ?>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($CorpConversionReportFormModel, 'endDateRange')->widget(
                DatePicker::classname(),
                [
                    'value' => '',
                    'type' => DatePicker::TYPE_INPUT,
                    'options' => ['placeholder' => 'Select issue date ...', 'value' => $_POST['CorpConversionReportForm']['endDateRange']],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                    ]
                ]
            );
            ?>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($CorpConversionReportFormModel, 'corp')->dropDownList($CorpConversionReportFormModel::corpList()) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <?= Html::submitButton('Create Report', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php if ($flag) :?>
            <div class="form-inline">
                <?= Html::button('Download XLS',['id' => 'download_xls', 'class' => 'btn btn-primary']) ?>
            </div>
        <?php endif;?>
    </div>


</div>



<?php ActiveForm::end(); ?>

<?php
    if ($flag) {
        echo $this->render('conversion_report_content',['results' => $results]);
    } else {
        echo "<h3>НЕ НАЙДЕНО!</h3>";
    }
?>




