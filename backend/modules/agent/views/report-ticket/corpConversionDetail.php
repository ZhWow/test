<?php
/**
 * User: Qway
 * Date: 19.01.2018
 * Time: 14:08
 */
/* @var $results array */
?>
<table border="1" align="center" id="detailReport">
    <thead>
        <tr>
            <?php foreach ($results[0] as $key => $v) :?>
                <td><?=$key?></td>
            <?php endforeach;?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $val) :?>
            <tr>
                <?php foreach ($keys as $needlKeys) :?>
                    <td><?=$val[$needlKeys]?></td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
