<?php

use \yii\data\ArrayDataProvider;
use \kartik\grid\GridView;

/**
 * @var $model object is the response from GetPNR of Sirena Travel API
 * @var $result object is the response from GetPNR of Sirena Travel API
 */

if ($result["status"] == "error") {
    echo $result["message"];
} else {
    $dataProvider = new ArrayDataProvider([
        'allModels' => $result["message"],
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    echo '<p> Tickets successfylly created and placed into db </p>';

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'eTicketNumber',
            'TicketingDate',
            'PNRNo',
            'GivenName',
            'Surname',
            'DepartureAirport',
            'ArrivalAirport',
            'FareCalculation',
        ],
    ]);
}

$asd = '123';