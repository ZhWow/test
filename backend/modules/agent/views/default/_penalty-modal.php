<?php

use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use backend\modules\agent\models\PenaltyForm;

/* @var $this yii\web\View */

$model = new PenaltyForm();

Modal::begin([
    'options' => ['id' => 'modal-refund-exchange'],
]);
?>
    <div id="modal-refund-content">

        <?php $form = ActiveForm::begin([
            'id' => 'modal-refund-exchange-form',
        ]); ?>

        <?= $form->field($model, 'penalty')->textInput() ?>

        <?= $form->field($model, 'corp')->hiddenInput([
            'id' => 'corp_id'
        ])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success resolve-submit-btn']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php
Modal::end();