<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 22.11.2017
 * Time: 15:46
 * @var $registrationApp \backend\modules\agent\models\FlightRegistration
 */
use backend\modules\agent\assets\AgentAsset;
use backend\modules\agent\models\corp_base\TBook;

$this->title = 'Online registrations';

AgentAsset::register($this);
?>
<ul class="breadcrumb">
    <li>
        <a href="/">Home</a>
    </li>
    <li class="active">Online registrations</li>
</ul>
<table class="table table-striped table-bordered">
    <tr>
        <th>Status</th>
        <th>Corp</th>
        <th>Pnr</th>
        <th>Passenger</th>
        <th>Seat info</th>
        <th>Registration start date</th>
        <th>Action</th>
    </tr>
    <?php if (!$registrationApp) :?>
        <tr>
            <td colspan="5">Not found</td>
        </tr>
    <?php else :?>
        <?php foreach ($registrationApp as $registrationData) :?>
            <tr>
                <td><?=$registrationData->status?></td>
                <td><?=$registrationData->corp->company_ru?></td>
                <?php TBook::setDb('db_' . $registrationData->corp->base);?>
                <td><?=TBook::findOne($registrationData->book_id)->PNR?></td>
                <td>
                    <?=$registrationData->getFlightPassenger($registrationData->book_id, $registrationData->corp->base)['lastname']?>
                    <br>
                    <?=$registrationData->getFlightPassenger($registrationData->book_id, $registrationData->corp->base)['firstname']?>
                </td>
                <td><?=$registrationData->seats->name?></td>
                <td><?=$registrationData->registration_start_date?></td>
                <td>
                    <form enctype="multipart/form-data" class="send-file-pdf">
                        <label class="btn btn-default" for="seat_ticket_file_<?=$registrationData->book_id.$registrationData->passenger_id?>">Load pdf file</label>
                        <input type="file" class="reg-file" name="seat_ticket_file" id="seat_ticket_file_<?=$registrationData->book_id.$registrationData->passenger_id?>">
                        <button type="submit" class="btn btn-primary" disabled>Send</button>
                        <input type="hidden" name="registration_id" value="<?=$registrationData->id?>">
                    </form>
                </td>
            </tr>
        <?php endforeach;?>
    <?php endif;?>
</table>