<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;


/* @var $this yii\web\View */
/* @var $between array */
/* @var $dataProvider yii\data\ArrayDataProvider */


$this->params['breadcrumbs'][] = Yii::t('app', 'Qway Active Tickets');
$this->title = 'Qway active tickets';

//$modal = $this->renderAjax('_set_penalties.php', ['ticket' => null]);

$tempJS = <<<JS
    var body = $('body'),
        url = '/ru/agent/default/',
        loader = $('#overlay-loader');

    body.on('click', '.set-penalties', function () {
        var ticketId = $(this).attr('data-id');
        $('#overlay-loader').show();
        
        $.ajax({
            url: url + 'qway-get-penalty-modal',
            type: 'POST',
            data: {
                request: 'get_penalty_modal',
                ticketId: ticketId
            },
            success: function(response) {
                
                $('#overlay-loader').hide();
                
                swal.queue([{
                    title: 'Please fill form',
                    html: response,
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Save',
                    cancelButtonText: 'Cancel',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            var formData = $('#set-penalties').serialize();
                          $.ajax({
                            url: url + 'qway-set-penalty',
                            type: 'POST',
                            dataType: 'json',
                            data: formData,
                            success: function(response) {
                                swal({
                                    title: 'Attention!',
                                    text: response.message,
                                    type: response.status
                                }).then(function() {
                                    resolve();
                                    location.reload();
                                });
                            }
                          });
                        });
                    }
                }]);
            }
        });
    });
    
    body.on('click', '.refund', function() {
        var ticketId = $(this).attr('data-id');
        
        loader.fadeIn();
        
        $.ajax({
            url: url + 'qway-get-refund-modal',
            type: 'POST',
            data: {
                ticket_id: ticketId
            },
            success: function(response) {
                loader.hide();
                $('#result_modal').html(response);
                $('#tickets_modal').modal('show');
            }
        });
    });
    body.on('click', '.close-ticket-modal', function() {
        $('#tickets_modal').modal('hide');
        location.reload();
    });
    
    body.on('click', '.exchange-info', function() {
        var ticketId = $(this).attr('data-id');
        
        loader.fadeIn();
        
        $.ajax({
            url: url + 'qway-get-exchange-info',
            type: 'POST',
            data: {
                ticket_id: ticketId
            },
            success: function(response) {
                loader.hide();
                $('#result_modal').html(response);
                $('#tickets_modal').modal('show');
            }
        });
    });
    
    body.on('click', '.exchange-ticket', function() {
        var ticketId = $(this).attr('data-id');
        
        loader.fadeIn();
        
        $.ajax({
            url: url + 'qway-get-exchange',
            type: 'POST',
            data: {
                ticket_id: ticketId
            },
            success: function(response) {
                loader.hide();
                $('#result_modal').html(response);
                $('#tickets_modal').modal('show');
            }
        });
    });
    
    body.on('click', '.void', function() {
        var ticketId = $(this).attr('data-id');
        
        swal({
            title: 'Attention!',
            text: 'Are you sure?',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет',
            preConfirm: function () {
                return new Promise(function (resolve) {
                  $.ajax({
                    url: url + 'qway-void',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        ticket_id: ticketId
                    },
                    success: function(response) {
                        swal({
                            title: 'Attention!',
                            text: response.message,
                            type: response.status
                        }).then(function() {
                            resolve();
                            location.reload();
                        });
                    }
                  });
                });
            }
        });
    });
    // $('#tickets_modal').modal('show');
JS;
$this->registerJs($tempJS);
?>
<?php if($dataProvider->allModels && $dataProvider->allModels[0] !== null) :?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        [
            'attribute' => 'ticket_number',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->ticket_number) ? $model->ticket_number : false;
            }
        ],
        [
            'attribute' => 'pnr',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->pnr) ? $model->pnr : false;
            }
        ],
        [
            'attribute' => 'Issued',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->ticketing_date) ? date('H:i d.m.Y', strtotime($model->ticketing_date)) : false;
            }
        ],
        [
            'attribute' => 'Passenger',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return $model->passenger->surname . ' ' . $model->passenger->name;
            }
        ],
        [
            'attribute' => 'Route',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'format' => 'html',
            'value' => function ($model) {
                $str = '';
                if ($model->itineraries) {
                    foreach ($model->itineraries as $itinerary) {
                        $str .= $itinerary->from . '-' . $itinerary->to . '<br>';
                    }
                }

                return $str;
            }
        ],
        [
            'attribute' => 'Ailines',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'format' => 'html',
            'value' => function ($model) {
                $str = '';
                if ($model->itineraries) {
                    foreach ($model->itineraries as $itinerary) {
                        $str .= $itinerary->operating_airline_code . '-' . $itinerary->flight_no . '(' . $itinerary->class . ')<br>';
                    }
                }
                return $str;
            }
        ],
        [
            'attribute' => 'Departure date',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'format' => 'html',
            'value' => function ($model) {
                $str = '';
                if ($model->itineraries) {
                    foreach ($model->itineraries as $itinerary) {
                        $str .= date('H:i d.m.Y', strtotime($itinerary->departure_date)) . '<br>';
                    }
                }
                return $str;
            }
        ],
        [
            'attribute' => 'Arrival date',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'format' => 'html',
            'value' => function ($model) {
                $str = '';
                if ($model->itineraries) {
                    foreach ($model->itineraries as $itinerary) {
                        $str .= date('H:i d.m.Y', strtotime($itinerary->arrival_date)) . '<br>';
                    }
                }
                return $str;
            }
        ],
        [
            'attribute' => 'Total fare',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->total_fare) ? $model->total_fare . ' KZT' : false;
            }
        ],
        [
            'attribute' => 'Taxes',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->total_tax_amount) ? $model->total_tax_amount . ' KZT' : false;
            }
        ],
        [
            'attribute' => 'status',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            'value' => function ($model) {
                return ($model->flightStatus->name) ? $model->flightStatus->name : false;
            }
        ],
        [
            'header' => 'Action',
            'content' => function($model) {
                $buttons = '';

                if ($model->status != 7) {
                    $buttons .= '<button class="btn btn-danger void" data-id="' . $model->id . '">Void</button>&nbsp';
                }

                if ($model->status == 1) {
                    $buttons .= '<button class="btn btn-primary set-penalties" data-id="' . $model->id . '">Set penalties</button>';
                } elseif ($model->status == 3) {
                    $buttons .= '<button class="btn btn-primary refund" data-id="' . $model->id . '">Refund</button>';
                } elseif ($model->status == 5) {
                    $buttons .= '<button class="btn btn-primary exchange-info" data-id="' . $model->id . '">Exchange flight info</button>&nbsp';
                    if ($model->exchange->is_buy == 1) {
                        $buttons .= '<button class="btn btn-primary exchange-ticket" data-id="' . $model->id . '">Exchange</button>';
                    }
                }
                return $buttons;
            }
        ],
    ],
]); ?>
<div class="modal fade" id="tickets_modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-head">
                <h1 class="modal-title">Information</h1>
            </div>
            <div id="result_modal"></div>
        </div>
    </div>
</div>
<?php else :?>
    <h2>Active tickets not found</h2>
<?php endif;?>
