<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\agent\models\RegisterBook;

$this->title = 'Register book';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
    $(document).on('change', '#registerbook-corpid', function() {
        const  corp = $(this).val();

        $.ajax({
            url: '/agent/default/corp-user',
            type: 'post',
            dataType: 'json',
            data: {
                corp: corp
            },
            beforeSend: function () {
                $('#sendBtn').prop('disabled', true);
                $('#overlay-loader').show();
            },
            success: function (response) {
               $('#sendBtn').removeAttr('disabled');
               $('#overlay-loader').hide();
               if(response.status) {
                    $('#registerbook-userid').html(response.option);
               }
            }
        });
        
        $('body').on('submit', '#RegisterBookForm', function (e) {
            e.preventDefault();
            let data = $(this).serialize();
            console.log(data);
            $.ajax({
                url: '/agent/default/register-book',
                type: 'post',
                dataType: 'json',
                data: {
                    data: data
                },
                beforeSend: function () {
                    $('#overlay-loader').show();
                },
                success: function () {
                   $('#overlay-loader').hide();
                }
            });
        });
        
    });

JS;
$this->registerJs($script);

?>

<div class="register-book">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'RegisterBookForm']); ?>

    <?= $form->field($model, 'pnr')->textInput() ?>

    <?= $form->field($model, 'surname')->textInput() ?>

    <?= $form->field($model, 'agencyId')->dropDownList(ArrayHelper::map(RegisterBook::getAgency(), 'ID', 'position'), [
        'prompt' => Yii::t('app', 'Select'),
        'options' => [
            '205' => ['Selected' => true]
        ]
    ]) ?>

    <?= $form->field($model, 'corpId')->dropDownList(ArrayHelper::map(RegisterBook::getCorp(), 'id', 'company'), [
        'prompt' => Yii::t('app', 'Select'),
        'options' => ['29' => ['Selected' => true]]
    ]) ?>

    <?= $form->field($model, 'servicetype')->dropDownList([
        'Economy' => Yii::t('app', 'Economy'), 'Business' => Yii::t('Business', 'Business'), 'First' => Yii::t('First', 'First')], [
        'prompt' => Yii::t('app', 'Select')
    ]) ?>

    <?= $form->field($model, 'serviceFee')->textInput() ?>

    <?= $form->field($model, 'userId')->dropDownList(ArrayHelper::map(RegisterBook::getUser(), 'ID', 'formName'), [
        'prompt' => Yii::t('app', 'Select'),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success', 'id' => 'sendBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
