<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 02.10.2017
 * Time: 18:00
 * @var $ticketId
 */
?>
<div class="row">
    <form id="set-penalties" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-5" for="refund_bd">refund_bd</label>
            <div class="col-sm-5">
                <input type="number" name="refund_bd" class="form-control" id="refund_bd">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-5" for="refund_ad">refund_ad</label>
            <div class="col-sm-5">
                <input type="number" name="refund_ad" class="form-control" id="refund_ad">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-5" for="refund_ns">refund_ns</label>
            <div class="col-sm-5">
                <input type="number" name="refund_ns" class="form-control" id="refund_ns">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-5" for="exchange_bd">exchange_bd</label>
            <div class="col-sm-5">
                <input type="number" name="exchange_bd" class="form-control" id="exchange_bd">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-5" for="exchange_ad">exchange_ad</label>
            <div class="col-sm-5">
                <input type="number" name="exchange_ad" class="form-control" id="exchange_ad">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-5" for="exchange_ns">exchange_ns</label>
            <div class="col-sm-5">
                <input type="number" name="exchange_ns" class="form-control" id="exchange_ns">
            </div>
        </div>
        <input type="hidden" name="ticket_id" value="<?=$ticketId?>">
    </form>
</div>