<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;


/* @var $this yii\web\View */
/* @var $between array */
/* @var $dataProvider yii\data\ArrayDataProvider */


$this->params['breadcrumbs'][] = Yii::t('app', 'Qway Active Bookings');

$tempJS = <<<JS
    var controllerUrl = '/agent/default/';

    $(".cancel-pnr-button").click(function(){
        $(this).prop('disabled', true);
        var pnr = $(this).data('pnrno');
        var surname = $(this).data('surname');

        $.ajax({
            url: controllerUrl + "cancel-pnr",
            method: "POST",
            data: { pnr : pnr, surname:surname },
            dataType: "json",
            success: function(response){
                $(this).prop('disabled', false);
                alert(response.text);
                location.reload();
            }
        });
    });

    $(".issue-pnr-button").click(function(){
        $(this).prop('disabled', true);
        var pnr = $(this).data('pnrno');
        var surname = $(this).data('surname');

        $.ajax({
            url: controllerUrl + "create-ticket",
            method: "POST",
            data: { pnr : pnr, surname:surname },
            dataType: "json",
            success: function(response){
                $(this).prop('disabled', false);
                alert(response.message);
                location.reload();
            }
        });
    });
JS;
$this->registerJs($tempJS);
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'pnr_no',
        'ticket_time_limit',
        'departure_airport:html',
        'arrival_airport:html',
        'departure_date_time:html',
        'arrival_date_time:html',
        'flight_number:html',
        'flight_duration:html',
        'code:html',
        'prefix:html',
        'name:html',
        'surname:html',
        'email:html',
//        'firstPaxSurname'
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('ait', 'Actions'),
            'template' => '{cancel-pnr}',
            'buttons' => [
                'cancel-pnr' => function ($url, $model, $key) {
                    $html = '<div class="btn-group-vertical" role="group" aria-label="">
<button type="button" class="btn btn-default btn-sm cancel-pnr-button" data-pnrno="'.$model["pnr_no"].'"  data-surname="'.$model["firstPaxSurname"].'">
  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
</button>';
                    if(Yii::$app->user->can('create_ticket')){
                    $html .= '<button type="button" class="btn btn-default btn-sm issue-pnr-button" data-pnrno="'.$model["pnr_no"].'"  data-surname="'.$model["firstPaxSurname"].'">
                                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                </button>
                                </div>';
                    }
                    return $html;
                },
            ],
        ],
    ],
]); ?>