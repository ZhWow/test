<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $between array */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Report');

$tempJS = <<<JS

JS;
$this->registerJs($tempJS);

$items = \backend\modules\integra\models\TElectronicTickets::find(false, $between);
$searchAttributes = [
    'eTicketNumber',
    'corpId',
    'fullName',
    'route',
    'DepartureDateTime',
    'status',
    'PNRNo',
    'Email',
    'withoutCharge',
    'ServiceFee',
    'TotalFare',
];

$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;

    if ($searchAttribute === 'corpId') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'label' => Yii::t('app', 'Corp'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $corp = \backend\models\Corp::findOne($model['corpId']);
                if ($corp) {
                    return $corp->base;
                }
                return '';
            },
        ];
    }

    if ($searchAttribute === 'fullName') {
        $searchColumns[] = [
            'label' => Yii::t('app', 'Full Name'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                return $model['Surname'] . ' ' . $model['GivenName'];
            },
        ];
    }

    if ($searchAttribute === 'route') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => Yii::t('flt', 'Route'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'format' => 'html',
            'value' => function ($model) {
                $route = '';
                $departureArr = explode(';', $model['DepartureAirport']);
                $arrivalArr = explode(';', $model['ArrivalAirport']);
                $i = 0;
                foreach ($departureArr as $key =>$item) {
                    $br = $i === 0 ? '' : '<br>';
                    $route .= $br . $item . '-' . $arrivalArr[$key];
                    $i++;
                }
                return $route;
            },
        ];
    }

    if ($searchAttribute === 'DepartureDateTime') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => Yii::t('flt', 'DepartureDateTime'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $departure = explode(';', $model['DepartureDateTime'])[0];
                $departureTime = explode('T', $departure);
                return $departureTime[0] . ' ' . $departureTime[1];
            },
        ];
    }

    if ($searchAttribute === 'status') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => 'Status',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {

                if ($model['state'] == 2 && $model['to_buy'] == 2 && $model['to_cancel'] == 1) {
                    return Yii::t('rpt', 'Purchased');
                }

                if ($model['state'] == 2 && $model['to_buy'] == 2 && $model['to_cancel'] == 7) {
                    return Yii::t('rpt', 'Returned');
                }

                if ($model['state'] == 2 && $model['to_buy'] == 2 && $model['to_cancel'] == 13) {
                    return Yii::t('rpt', 'Exchange');
                }

                return Yii::t('app', 'Other');
            },
        ];
    }

    if ($searchAttribute === 'withoutCharge') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => Yii::t('rpt', 'Without charge'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                return number_format($model['BaseFare'], false, false, ' ') . ' KZT';
            },
        ];
    }

    if ($searchAttribute === 'ServiceFee') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'label' => Yii::t('rpt', 'Service Fee'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                return number_format(explode(';', $model['ServiceFee'])[0], false, false, ' ') . ' KZT';
            },
        ];
    }

    if ($searchAttribute === 'TotalFare') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'label' => Yii::t('rpt', 'Total'),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                return number_format($model['TotalFare'], false, false, ' ') . ' KZT';
            },
        ];
    }

    if ($searchAttribute !== 'corpId'
        && $searchAttribute !== 'fullName'
        && $searchAttribute !== 'route'
        && $searchAttribute !== 'DepartureDateTime'
        && $searchAttribute !== 'status'
        && $searchAttribute !== 'ServiceFee'
        && $searchAttribute !== 'TotalFare'
        && $searchAttribute !== 'withoutCharge') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => $searchAttribute,
        ];
    }


    $items = array_filter($items, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}

$dataProvider = new ArrayDataProvider([
    'key' => 'id',
    'allModels' => $items,
    'sort' => [
        'attributes' => $searchAttributes,
    ],
    'pagination' => [
        'pageSize' => 20,
    ],
]);

echo $this->render('_filter-date', ['dataProvider' => $dataProvider]);

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'rowOptions'   => function ($model) {
        return [
            'data-corp-id' => $model['corpId']
        ];
    },
    'filterModel' => $searchModel,
    'columns' => $searchColumns
]); ?>

<?= $this->render('_penalty-modal') ?>
