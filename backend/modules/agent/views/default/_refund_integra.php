<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 05.10.2017
 * Time: 13:24
 * @var $message
 */
?>
<div class="modal-body">
    <p><?=$message?></p>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary close-ticket-modal">Close</button>
</div>
