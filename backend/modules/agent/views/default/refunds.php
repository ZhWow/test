<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Refund');

$tempJS = <<<JS

$('.refund-sign-button').click(function (e) {
    e.preventDefault();
    var that = $(this);
    var url = that.attr('href');
    var form = $('#modal-refund-exchange-form');
    var corpId = that.closest('tr').attr('data-corp-id');
    
    form.attr('action', url);
    $('#corp_id').val(corpId);
    
    $('#modal-refund-exchange').modal('show');
});

JS;
$this->registerJs($tempJS);

$items = \backend\modules\integra\models\TElectronicTickets::find(['to_cancel' => [2, 3]]);
$searchAttributes = [
    'eTicketNumber',
    'GivenName',
    'Surname',
    'corpId',
    'PNRNo',
    'Email',
    'BaseFare',
    'penalty',
];

$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;

    if ($searchAttribute === 'corpId') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            //'filter' => Html::activeDropDownList($searchModel, 'corpId', ArrayHelper::map(\backend\models\Corp::find()->select(['id', 'company_ru'])->all(), 'id', 'company_ru'), ['class'=>'form-control','prompt' => 'All']),
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $corp = \backend\models\Corp::findOne($model['corpId']);
                if ($corp) {
                    return $corp->base;
                }
                return '';
            },
        ];
    } else {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => $searchAttribute,
        ];
    }






    $items = array_filter($items, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}
?>
<?= GridView::widget([
    'dataProvider' => new ArrayDataProvider([
        'key' => 'id',
        'allModels' => $items,
        'sort' => [
            'attributes' => $searchAttributes,
        ],
        'pagination' => [
            'pageSize' => 20,
        ],
    ]),
    'rowOptions'   => function ($model) {
        return [
            'data-corp-id' => $model['corpId']
        ];
    },
    'filterModel' => $searchModel,
    'columns' => array_merge(
        $searchColumns, [
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('flt', 'Refund'),
                'template' => '{refunds}',
                'buttons' => [
                    'refunds' => function ($url, $model) {
                        $button = Yii::t('flt', 'Must refund');
                        if ($model['to_cancel'] == 2) {
                            $button = Html::a(Yii::t('flt', 'Refund'), $url,['class' => 'refund-sign-button']);
                        }
                        return $button;
                    },
                ],
            ],
        ]
    )
]); ?>

<?= $this->render('_penalty-modal') ?>
