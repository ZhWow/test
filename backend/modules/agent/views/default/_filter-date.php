<?php

use yii\widgets\ActiveForm;
use backend\modules\agent\models\DateFilterForm;
use yii\helpers\Html;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$model = new DateFilterForm();

?>

<div class="row">

    <div class="col-md-5">
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'filename' => 'tickets_' . Yii::$app->formatter->asDatetime(time(), 'yyyyMMdd_HHmm'),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'eTicketNumber',
                'GivenName',
                'Surname',
                'PNRNo',
                'Email',
                'BaseFare',
                'penalty',

//        ['class' => 'yii\grid\ActionColumn'],
            ],
            'target' => '_self',
            'showConfirmAlert' => false,
            'fontAwesome' => true,
        ]);
        ?>
    </div>

    <div class="col-md-7 date-filter">
        <?php $form = ActiveForm::begin([
            'id' => 'modal-refund-exchange-form',
            'class' => 'row',
            'method' => 'GET',
            'action' => '/agent/default/report'
        ]); ?>

        <div class="col-md-4">
            <?= $form->field($model, 'from')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'From'),
                ],
            ])->label(false) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'to')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'To'),
                ],
            ])->label(false) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


</div>

