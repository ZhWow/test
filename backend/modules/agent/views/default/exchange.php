<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use modules\organisation\models\flights\TRefundsExchanges;
use backend\models\Corp;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Exchange');

$tempJS = <<<JS

$('.exchange-sign-button').click(function (e) {
    e.preventDefault();
    var that = $(this);
    var url = that.attr('href');
    var form = $('#modal-refund-exchange-form');
    var corpId = that.closest('tr').attr('data-corp-id');
    
    form.attr('action', url);
    $('#corp_id').val(corpId);
    
    $('#modal-refund-exchange').modal('show');
});

JS;
$this->registerJs($tempJS);

$items = \backend\modules\integra\models\TElectronicTickets::find(['to_cancel' => [10, 12]]);

$searchAttributes = [
    'eTicketNumber',
    'GivenName',
    'Surname',
    'PNRNo',
    'Email',
    'BaseFare',
    'penalty',
];


$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;

    $searchColumns[] = [
        'attribute' => $searchAttribute,
        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
        'value' => $searchAttribute,
    ];

    foreach ($items as $key => $item) {
        $base = Corp::find()->select('base')->where(['id' => $item['corpId']])->one();
        TRefundsExchanges::setDb($base['base']);
        $kek = TRefundsExchanges::find()->where(['eTicketId' => $item['id']])->one();
//        $items[$key]['old_dep'] = $kek['oldDepAirport'];
//        $items[$key]['old_arr'] = $kek['oldArAirport'];

        $items[$key]['new_segment'] = getSegmentString($kek);
        #$items[$key]['new_segment'] = $kek['newDepAirport'] . '-' . $kek['newArAirport'] .' '. $kek['newArDate']. ' '. $kek['newArTime'];
    }
    $items = array_filter($items, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}
function getSegmentString($data){

    $exploded_dep = explode(';',$data['newDepAirport']);
    $exploded_depDate = explode(';',$data['newDepDate']);
    $exploded_depTime = explode(';',$data['newDepTime']);
    $exploded_arr = explode(';',$data['newArAirport']);
    $exploded_arrDate = explode(';',$data['newArDate']);
    $exploded_arrTime = explode(';',$data['newArTime']);
    $exploded_newAirline = explode(';',$data['newAirline']);
    $exploded_class = explode(';',$data['newClass']);
    $exploded_flt_number = explode(';',$data['newFlightNumber']);
    $string = '';
    foreach ($exploded_dep as $key => $item) {
        $string .= $item .' : '. $exploded_depDate[$key] . ' ' . $exploded_depTime[$key].'<br>';
        $string .= $exploded_arr[$key] .' : '. $exploded_arrDate[$key] . ' ' . $exploded_arrTime[$key].'<br>';
        $string .= $exploded_newAirline[$key]. ' - ' . $exploded_class[$key] . ' - '. $exploded_flt_number[$key] . '<br>';
        $string .= '-------------------------------<br>';
    }
    return $string;
}

?>
<?= GridView::widget([
    'dataProvider' => new ArrayDataProvider([
        'key' => 'id',
        'allModels' => $items,
        'sort' => [
            'attributes' => $searchAttributes,
        ],
        'pagination' => [
            'pageSize' => 20,
        ],
    ]),
    'rowOptions'   => function ($model) {
        return [
            'data-corp-id' => $model['corpId']
        ];
    },
    'filterModel' => $searchModel,
    'columns' => array_merge(
        $searchColumns,
        [
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('flt', 'Segment'),
                'template' => '{Segment}',
                'buttons' => [
                    'Segment' => function ($url, $model) {
                        return $model['new_segment'];
                    },
                ],
            ],
        ],
        [
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('flt', 'Exchange'),
                'template' => '{exchange}',
                'buttons' => [
                    'exchange' => function ($url, $model) {
                        $button = Yii::t('flt', 'Must exchange');
                        if ($model['to_cancel'] == 10) {
                            $button = Html::a(Yii::t('flt', 'Exchange'), $url,['class' => 'exchange-sign-button']);
                        }
                        return $button;
                    },
                ],
            ],
        ]
    )
]); ?>

<?= $this->render('_penalty-modal') ?>
