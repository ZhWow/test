<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;


/* @var $this yii\web\View */
/* @var $between array */
/* @var $dataProvider yii\data\ArrayDataProvider */


$this->params['breadcrumbs'][] = Yii::t('app', 'Active ticket');

$templateJs = <<<JS
$(document).ready(function() {
       var modelId,
           modelCorp;

    $(document).on('click', '#revalidation', function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('data-key'),
            corpId = $(this).parents('tr').attr('data-corp-id');
        modelId = id;
        modelCorp = corpId;

        $.ajax({
            url: '/agent/revalidation/date-time-info',
            method: 'post',
            dataTupe: 'json',
            data: {
                id: id,
                corpId: corpId
            },
            success: function(response) {
                var obj = JSON.parse(response);

                if(obj) {
                    $.each(obj, function(key, value) {
                        $('#revalidation-' + key.toLowerCase()).val(value);
                    });

                    for(i = 1; i <= obj.lastId; i++) {
                        $('.flight_' + i).removeClass('hidden ');
                    }
                    for(i = obj.lastId + 1; i < 7; i++) {
                        $('.flight_' + i).addClass('hidden ');
                        $('#revalidation-departuredatetime' + i).val('');
                        $('#revalidation-arrivaldatetime' + i).val('');
                    }

                    $('#modal-revalidation').modal('show');
                }
            }
        });
    });


     $('body').on('submit', '#modal-revalidation-form', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: '/agent/revalidation/revalidate',
            method: 'post',
            dataTupe: 'json',
            data: {
                id: modelId,
                corpId: modelCorp,
                data: data
            },
            beforeSend: function() {
                $('#sendBtn').prop('disabled', true);
            },
            success: function(response) {
                $('#modal-resolve-signed-integra').modal('hide');
            }
        });
    });

    $(document).on('click', '.flight-list', function (e) {
          e.preventDefault();
          $(this).find('.flight_content').toggle("slow");
    });
});
JS;
$this->registerJs($templateJs);


$items = \backend\modules\integra\models\TElectronicTickets::find([
    'state' => 2,
    'to_buy' => 2,
    'to_cancel' => 1,
], $between);

$searchAttributes = [
    'eTicketNumber',
    'GivenName',
    'Surname',
    'corpId',
    'agencyID',
    'PNRNo',
    'issuedDate',
    'route',
    'DepartureDateTime',
//    'Email',
    'BaseFare',
//    'penalty',
];

$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;

    if ($searchAttribute === 'corpId') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'label' => 'Corp',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $corp = \backend\models\Corp::findOne($model['corpId']);
                if ($corp) {
                    return $corp->base;
                }
                return '';
            },
        ];
    }

    if ($searchAttribute === 'agencyID') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'label' => 'Agency',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $agency = \backend\modules\integra\models\TUsers::findOne($model['agencyID']);
                if ($agency) {
                    return $agency->position;
                }
                return '';
            },
        ];
    }

    if ($searchAttribute === 'issuedDate') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => 'Issued Date',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $amadeusIntegra = \backend\modules\integra\models\AmadeusIntegra::findOne(['DOCUMENT_NUMBER' => $model['eTicketNumber'], 'DOCUMENT_ACTION' => 'ISSUE']);
                if ($amadeusIntegra) {
                    return $amadeusIntegra->DOCUMENT_ISSUED;
                }
                return '';
            },
        ];
    }

    if ($searchAttribute === 'route') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => 'Route',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'format' => 'html',
            'value' => function ($model) {
                $route = '';
                $departureArr = explode(';', $model['DepartureAirport']);
                $arrivalArr = explode(';', $model['ArrivalAirport']);
                $i = 0;
                foreach ($departureArr as $key => $item) {
                    $br = $i === 0 ? '' : '<br>';
                    $route .= $br . $item . '-' . $arrivalArr[$key];
                    $i++;
                }
                return $route;
            },
        ];
    }

    if ($searchAttribute === 'DepartureDateTime') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => 'DepartureDateTime',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                $departure = explode(';', $model['DepartureDateTime'])[0];
                $departureTime = explode('T', $departure);
                return $departureTime[0] . ' ' . $departureTime[1];
            },
        ];
    }

    if ($searchAttribute === 'BaseFare') {
        $searchColumns[] = [
            //'attribute' => $searchAttribute,
            'label' => 'BaseFare',
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => function ($model) {
                return number_format($model['BaseFare'], false, false, ' ') . ' KZT';
            },
        ];
    }


    if ($searchAttribute !== 'corpId' && $searchAttribute !== 'agencyID' && $searchAttribute !== 'issuedDate' && $searchAttribute !== 'route' && $searchAttribute !== 'DepartureDateTime' && $searchAttribute !== 'BaseFare') {
        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => $searchAttribute,
        ];
    }


    $items = array_filter($items, function ($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}

$dataProvider = new ArrayDataProvider([
    'key' => 'id',
    'allModels' => $items,
    'sort' => [
        'defaultOrder' => ['DepartureDateTime' => SORT_ASC],
        'attributes' => $searchAttributes,
    ],
    'pagination' => [
        'pageSize' => 20,
    ],
]) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'rowOptions' => function ($model) {
        return [
            'data-corp-id' => $model['corpId']
        ];
    },
    'filterModel' => $searchModel,
    'columns' => array_merge(
        $searchColumns, [
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => ' ',
                'template' => '{mail} {revalidation}',
                'buttons' => [
                    'mail' => function ($url, $model) {
                        $button = Html::a('<i class="fa fa-envelope" aria-hidden="true"></i>', $url . '&corpId=' . $model['corpId'] . '&ticket=' . $model['eTicketNumber'], ['class' => 'exchange-sign-button']);
                        return $button;
                    },
                    'revalidation' => function () {
                        return Html::tag('span', '<i class = "glyphicon glyphicon-retweet"></i>', ['id' => 'revalidation', 'title' => 'Revolidation', 'style' => 'cursor:pointer; color: #337ab7;']);
                    },
                ],
            ],
        ]
    )

]); ?>

<?= $this->render('_penalty-modal') ?>

<?php
Modal::begin([
    'options' => [
        'id' => 'modal-revalidation',
    ],
    'header' => Yii::t('app', 'Revalidation'),
]);

echo $this->render('/revalidation/_form');

Modal::end()
?>

