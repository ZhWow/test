<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 05.10.2017
 * Time: 16:26
 * @var $result
 * @var $model
 */
?>
<?php if (!$result) :?>
    <div class="modal-body">
        <p>Exchange info not found!</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary close-ticket-modal">Close</button>
    </div>
<?php else :?>
    <div class="modal-body">
        <table class="table">
            <tr>
                <th>Departure airport</th>
                <th>Arrival airport</th>
                <th>Departure date time</th>
                <th>Arrival date time</th>
                <th>Flight number</th>
                <th>Operating airline</th>
            </tr>
            <?php foreach ($model->flightDetails as $flightDetail) :?>
                <?php foreach ($flightDetail->flightSegments as $flightSegment) :?>
                    <tr>
                        <td><?=$flightSegment->departure_airport?></td>
                        <td><?=$flightSegment->arrival_airport?></td>
                        <td><?=date('H:i Y.m.d', strtotime($flightSegment->departure_date_time))?></td>
                        <td><?=date('H:i Y.m.d', strtotime($flightSegment->arrival_date_time))?></td>
                        <td><?=$flightSegment->flight_number?></td>
                        <td><?=$flightSegment->operating_airline?></td>
                    </tr>
                <?php endforeach;?>
            <?php endforeach;?>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
<?php endif;?>