<?php

/* @var $this yii\web\View */
/* @var $model \modules\flight\frontend\models\ElectronicTickets */
/* @var $id int */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin();

echo $form->field($model, 'exchange_before_departure')->textInput();
echo $form->field($model, 'exchange_after_departure')->textInput();
echo $form->field($model, 'refunds_before_departure')->textInput();
echo $form->field($model, 'refunds_after_departure')->textInput();
echo $form->field($model, 'id')->hiddenInput(['value' => $id])->label(false);

?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id' => 'penalty_submit', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?
ActiveForm::end();

