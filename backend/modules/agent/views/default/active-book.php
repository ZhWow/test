<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use backend\modules\agent\assets\AgentAsset;

/* @var $this yii\web\View */
/* @var $between array */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->params['breadcrumbs'][] = Yii::t('app', 'Active book');
$this->title = 'Active books';

$tempJS = <<<JS

$(document).ready(function(){
    $('[data-toggle="ticket-tooltip"]').tooltip();
});

JS;
$this->registerJs($tempJS);
AgentAsset::register($this);

//$items = \backend\models\TBook::find(['state' => 1, 'to_buy' => 2, 'to_cancel' => 1], ' OR (to_check = 1 AND state = 1 AND to_buy = 1 AND to_cancel = 1)');
$items = \backend\models\TBook::find(['state' => 1, 'to_buy' => 2, 'to_cancel' => 1]);
$searchAttributes = [
    'PNR',
    'price',
    'email',
    'bookdate',
];

$searchModel = [];
//$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;

  /*  $searchColumns[] = [
        'attribute' => $searchAttribute,
        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
        'value' => $searchAttribute,
    ];*/


    $items = array_filter($items, function ($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });

}

$dataProvider = new ArrayDataProvider([
    'key' => 'id',
    'allModels' => $items,
    'sort' => [
        'attributes' => $searchAttributes,
    ],
    'pagination' => [
        'pageSize' => 20,
    ],
]);

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        [
            'attribute' => 'Corp',
            'value' => function ($model)
            {
                $corp = \backend\models\Corp::findOne($model["corp"]);
                return $corp->company;
            }
        ],
        [
            'attribute' => 'agency',
            'value' => function ($model)
            {
                $agency = \backend\modules\integra\models\TUsers::findOne($model["agency_id"]);
                return $agency->position;
            }
        ],
        [
            'attribute' => 'PNR',
//            'filter' => '<input class="form-control" name="filterPNR" value="" type="text">',
            'value' => 'PNR',
        ],
        [
            'attribute' => 'Passengers',
            'format' => 'html',
            'value' => function ($model)
            {
                $passengerLine = '';
                $Surnames = explode(';', $model["Surnames"]);
                $Names = explode(';', $model["Names"]);
                foreach ($Surnames as $surnameKey => $surname) {
                    $breakPoint = ($surnameKey == count($Surnames) - 1) ? "<br>" : "";
                    $passengerLine .= $Surnames[$surnameKey] . " " . $Names[$surnameKey] . $breakPoint;
                }

                return $passengerLine;
            }
        ],
        [
            'attribute' => 'Route',
            'format' => 'html',
            'value' => function ($model)
            {
                $routeLine = '';
                $depair = explode(";", $model['depair']);
                $arrair = explode(";", $model['arrair']);
                $depdate = explode(";", $model['depdate']);
                $deptime = explode(";", $model['deptime']);
                $arrdate = explode(";", $model['arrdate']);
                $arrtime = explode(";", $model['arrtime']);

                foreach ($depair as $depairKey => $depairVal) {
                    $breakPoint = ($depairKey == count($depair) - 1) ? "" : "<br>";
                    $routeLine .= $depair[$depairKey] . ' (' . $depdate[$depairKey] . ' ' . $deptime[$depairKey] . ') - ' . $arrair[$depairKey] . ' (' . $arrdate[$depairKey] . ' ' . $arrtime[$depairKey] . ')' . $breakPoint;
                }
                return $routeLine;
            }
        ],
        [
            'attribute' => 'price',
//            'filter' => '<input class="form-control" name="filterprice" value="" type="text">',
            'value' => function ($model)
            {
                return number_format($model['price'],0,' ', ' ');
            }
        ],
        [
            'attribute' => 'Contact',
            'format' => 'html',
//            'filter' => '<input class="form-control" name="filteremail" value="" type="text">',
            'value' => function ($model)
            {
                return $model['email'] . '<br>' . $model['phones'];
            }
        ],

        [
            'attribute' => 'Info',
            'format' => 'html',
//            'filter' => '<input class="form-control" name="filterbookdate" value="" type="text">',
            'value' => function ($model)
            {
                return 'Book: ' . $model['bookdate'] . '<br>' . 'Limit: ' . $model['timelimit'];
            }
        ],
        [
            'attribute' => '',
            'value' =>function ($model) {
                return ($model['fixed'] == 1) ? Yii::t('app', 'Raise to without penalty ') : '';
            },
            'contentOptions' => function () {
                return ['style' => 'color:red'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('ait', 'Resolve'),
            'template' => '{check-sirena}',
            'buttons' => [
                'check-sirena' => function ($url, $model) {
                    $button = NULL;

                    if ( $model['pultversion'] === 'wssirena' ) {
                        $button = Html::a('Check', '/agent/default/check-sirena?id=' . $model['id'] . '&corp=' . $model['corp'],['class' => 'resolve-sign-button']);
                    } elseif (isset($model['to_check']) && $model['to_check'] == 1) {
                        $button = Html::button('detail', ['class' => 'detail-check btn btn-default', 'onClick' => 'getModalBookingInfo(' . $model['id'] . ', ' . $model['corp'] . ');']);
                    }

                    return $button;
                },
            ],
        ],
    ],
    'rowOptions' => function ($model) {
        $result = null;
        if ($model['fixed'] == 1) {
            $result = [
                'data-toggle' => 'ticket-tooltip',
                'title' => Yii::t('app', 'Raise to without penalty '),
                'data-placement' => 'top',
            ];
        }
        return $result;
    },
]); ?>

<div id="book_info_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content form"></div>
    </div>
</div>

<?= $this->render('_penalty-modal') ?>
