<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 29.11.2017
 * Time: 13:45
 * @var $bookData \modules\organisation\models\flights\TBook
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$names = explode(';', $bookData->Names);
$surnames = explode(';', $bookData->Surnames);

$depair = explode(';', $bookData->depair);
$arrair = explode(';', $bookData->arrair);
$depdate = explode(';', $bookData->depdate);
$deptime = explode(';', $bookData->deptime);
$arrdate = explode(';', $bookData->arrdate);
$arrtime = explode(';', $bookData->arrtime);
$duration = explode(';', $bookData->duration);
$flights = explode(';', $bookData->flights);
$sub_classes = explode(';', $bookData->sub_classes);
$servicetype = explode(';', $bookData->servicetype);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Book info</h4>
</div>
<div class="modal-body">
    <?php
    $form = ActiveForm::begin([
        'action' => '/agent/default/book-modal-save',
        'id' => 'book_info_form',
        'options' => [
            'class' => 'form-horizontal col-md-12'
        ],
    ]);
    ?>
    <?php foreach ($depair as $key => $book) :?>
        <h4 class="modal-title">Passenger <?=$surnames[$key];?> <?=$names[$key];?></h4>
        <?= $form->field($bookData, 'depair[]')->textInput(['value' => $depair[$key]])->label('Departure airport') ?>
        <?= $form->field($bookData, 'arrair[]')->textInput(['value' => $arrair[$key]])->label('Arrival airport') ?>
        <?= $form->field($bookData, 'depdate[]')->textInput(['value' => $depdate[$key]])->label('Departure date') ?>
        <?= $form->field($bookData, 'deptime[]')->textInput(['value' => $deptime[$key]])->label('Departure time') ?>
        <?= $form->field($bookData, 'arrdate[]')->textInput(['value' => $arrdate[$key]])->label('Arrival date') ?>
        <?= $form->field($bookData, 'arrtime[]')->textInput(['value' => $arrtime[$key]])->label('Arrival time') ?>
        <?= $form->field($bookData, 'duration[]')->textInput(['value' => $duration[$key]])->label('Flight duration') ?>
        <?= $form->field($bookData, 'flights[]')->textInput(['value' => $flights[$key]])->label('Flight number') ?>
        <?= $form->field($bookData, 'sub_classes[]')->textInput(['value' => $sub_classes[$key]])->label('Sub class') ?>
        <?= $form->field($bookData, 'servicetype[]')->textInput(['value' => $servicetype[$key]])->label('Service type') ?>
    <?php endforeach;?>
    <?= $form->field($bookData, 'corp')->hiddenInput(['value' => $bookData->corp])->label(false) ?>
    <?= $form->field($bookData, 'id')->hiddenInput(['value' => $bookData->id])->label(false) ?>
    <div>
        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">close</button>
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-lg']) ?>
    </div>
    <?php ActiveForm::end() ?>
    <div class="clearfix"></div>
</div>