<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 05.10.2017
 * Time: 18:38
 * @var $result
 */
?>
<?php if (!$result) :?>
    <div class="modal-body">
        <p>Файл обмена не найден, проведите возврат вручную затем повторите операцию</p>
    </div>
<?php else :?>
    <div class="modal-body">
        <p>Обмен совершен успешно</p>
    </div>
<?php endif;?>
<div class="modal-footer">
    <button class="btn btn-secondary close-ticket-modal">Close</button>
</div>
