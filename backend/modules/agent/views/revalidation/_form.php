<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\modules\agent\models\Revalidation;

/* @var $this yii\web\View */
/* @var $model backend\modules\integra\models\Revalidation */
/* @var $form yii\widgets\ActiveForm */

$model = new Revalidation();
?>
<style>
    .flight_content {
        display: none;
    }
</style>
<div class="modal-revalidation-content">

    <?php $form = ActiveForm::begin([
        'id' => 'modal-revalidation-form',
    ]); ?>

    <?php for ($i = 1; $i < 7; $i++) : ?>
        <div class="flight_wrapper">
            <div class="flight-list flight_<?= $i ?>">
                <fieldset>
                    <legend><?= Yii::t('app', 'Flight') . ' № ' . $i ?></legend>
                    <div class="flight_content">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <?= $form->field($model, 'departureDateTime' . $i)->widget(DateTimePicker::classname(), [
                                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                'removeButton' => false,
                                'pickerButton' => ['icon' => 'calendar'],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy hh:ii',
                                    'showMeridian' => true,
                                    'todayBtn' => true
                                ],
                            ])->label(Yii::t('app', 'Departure date time')); ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <?= $form->field($model, 'arrivalDateTime' . $i)->widget(DateTimePicker::classname(), [
                                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                'removeButton' => false,
                                'pickerButton' => ['icon' => 'calendar'],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy hh:ii',
                                    'showMeridian' => true,
                                    'todayBtn' => true
                                ],
                            ])->label(Yii::t('app', 'Arrival date time')); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    <?php endfor ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('ait', 'Save'), ['class' => 'btn btn-success resolve-submit-btn', 'id' => 'sendBtn', 'style' => 'margin-left:13px']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
