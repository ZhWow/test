<?php

/* @var $this yii\web\View */
/* @var $ticket \backend\modules\integra\models\AmadeusIntegra */
?>

<?= $this->render('_before') ?>

    <table class="body-wrap" bgcolor="#f6f6f6">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <!-- content -->
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <p>Доброе время суток,</p>
                                <p>Для Вас был осуществлён возврат билета с номером - <?= $ticket->DOCUMENT_NUMBER ?>.</p>

                                <!-- /button --><br /><br />
                                <p>
                                    Данное сообщение было сгенерированно автоматически и не требует ответа. <br />
                                    Спасибо,
                                </p>
                                <p>
                                    <a href="mailto:tickets@btmc.kz">Связаться с нами</a><br />
                                    <a href="http://www.btmc.kz" style="font-size:12px; color:#aaaaaa"> www.btmc.kz</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /content -->
            </td>
            <td></td>
        </tr>
    </table>

<?= $this->render('_after') ?>