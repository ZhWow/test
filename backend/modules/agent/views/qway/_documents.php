<?php
/** @var $parameters */
use yii\bootstrap\Modal;

Modal::begin([
    'options' => ['id' => 'modal-documents'],
]);

?>

<div id="webapp-documents-container">
    <div class="doc-list" v-if="docItems">
        <div class="docs-item" v-for="docItem in docItems">
            <a data-fancybox="gallery" class="fancybox" :href="'<?= $parameters['qwayUrl'] ?>'+docItem.path">
                <img :src="'<?= $parameters['qwayUrl'] ?>'+docItem.path" alt="">
            </a>
        </div>
    </div>
    <span v-if="docItems.length == 0">There is no documents attached! Please contact passenger</span>
</div>

<?php Modal::end(); ?>