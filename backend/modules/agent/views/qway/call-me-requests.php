<?php
use kartik\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\agent\assets\AgentAsset;

//$this->registerJs($tempJS);
AgentAsset::register($this);

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        'phone',
        'email',
        [
            'class' => '\kartik\grid\ActionColumn',
            'template' => '{documents} &nbsp; {get-pnr} &nbsp; {set-order-processed}',
            'buttons' => [
                'get-pnr' => function ($url, $model)
                {
                    return '<a href="#" data-modelid="'.$model->id.'" data-userid="'.$model->user_id.'" id="get-pnr-modal-btn"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>';
                },
                'set-order-processed' => function ($url, $model)
                {
                    return '<a href="/agent/qway/processed-request?model='.$model->id.'" id="set-order-processed"><i class="fa fa-times" aria-hidden="true"></i></a>';
                },
                'documents' => function ($utl, $model) {
                    return '<a href="#" data-userid="'.$model->user_id.'" id="get-documents-modal"><i class="fa fa-address-card-o" aria-hidden="true"></i></a>';
                }
            ],
        ]
    ],
    'hover' => true,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Qway Mobile App Requests</h3>',
        'type'=>'success',
//        'before' => $this->render('_offline_stats',['counter' => $counter]), //'<div> Today Bought ' . $counter["todayBought"] . '; Bought Online '. $counter["todayOnline"] .'; Bought Offline '. $counter["todayOffline"] .'</div>',
//        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
//        'beforeGrid'=>'My fancy content before.',
//        'afterGrid'=>'My fancy content after.',
    ]
]);
Pjax::end();
?>

<?= $this->render('_get_pnr') ?>
<?= $this->render('_documents', ['parameters' => Yii::$app->params['params']]) ?>
