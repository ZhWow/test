<?php
use yii\bootstrap\Modal;
use \backend\modules\agent\models\WAPNR;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$PNR = new WAPNR();

Modal::begin([
    'options' => ['id' => 'modal-webapp-getpnr'],
]);

$form = ActiveForm::begin([
    'id' => 'modal-webapp-getpnr-form',
    'action' => ['qway/get-pnr'],
]);
?>

<div id="webapp-getpnr-container">

    <?= $form->field($PNR, 'pnr')->textInput() ?>
    <?= $form->field($PNR, 'surname')->textInput() ?>
    <?= $form->field($PNR, 'serviceFee')->textInput() ?>
    <?= $form->field($PNR, 'user_id')->hiddenInput(['value'=> ''])->label(false) ?>
    <?= $form->field($PNR, 'model_id')->hiddenInput(['value'=> ''])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('ait', 'Submit'), ['class' => 'btn btn-success getpnr-submit']) ?>
    </div>
</div>

<?php
ActiveForm::end();
Modal::end();
?>
