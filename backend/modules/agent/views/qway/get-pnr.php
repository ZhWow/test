<?php
use kartik\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        'PNR',
        [
            'attribute' => 'Price',
            'value' => function ($model) {
                return number_format($model['price_all'],0,' ',' ') . " " . $model['currency'];
            }
        ],
        [
            'attribute' => 'TimeLimit',
            'value' => function ($model) {
                return date('H:i d.m.Y', strtotime($model['timelimit']));
            }
        ],
        [
            'attribute' => 'Passengers',
            'format' => 'html',
            'value' => function ($model) {
                $result = '';
                $Names = explode(';',$model["Names"]);
                $Surnames = explode(';',$model["Surnames"]);
                foreach ($Names as $key => $nameItem) {
                    $breakPoint = (count($Names) - 1 == $key) ? "" : "<br>";
                    $result .= $Names[$key] . " " . $Surnames[$key] . $breakPoint;
                }
                return $result;
            }
        ],
        [
            'attribute' => 'Flights',
            'format' => 'html',
            'value' => function ($model) {
                $result = '';
                $depair = explode(';', $model['depair']);
                $depdate = explode(';', $model['depdate']);
                $deptime = explode(';', $model['deptime']);
                $arrair = explode(';', $model['arrair']);
                $arrdate = explode(';', $model['arrdate']);
                $arrtime = explode(';', $model['arrtime']);
                $duration = explode(';', $model['duration']);
                foreach ($depair as $depairKey => $depairItem) {
                    $breakPoint = (count($depair) - 1 == $depairKey) ? "" : "<br>";
                    $result .= $depair[$depairKey] . " - " . $arrair[$depairKey] . " ";
                    $result .= $deptime[$depairKey] . " " . $depdate[$depairKey] . " - " .  $arrtime[$depairKey] . " " . $arrdate[$depairKey] . $breakPoint;
                }
                return $result;
            }
        ]
    ],
    'hover' => true,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Qway Mobile App Booking Inserted</h3>',
        'type'=>'success',
//        'before' => $this->render('_offline_stats',['counter' => $counter]), //'<div> Today Bought ' . $counter["todayBought"] . '; Bought Online '. $counter["todayOnline"] .'; Bought Offline '. $counter["todayOffline"] .'</div>',
//        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
]);