<?php
use kartik\grid\GridView;
use yii\widgets\Pjax;

echo \kartik\daterange\DateRangePicker::widget([
    'name'=>'date_range_3',
    'value'=>'2017-12-12 - 2017-12-12',
    'convertFormat'=>true,
//    'hideInput' => true,
    'containerTemplate' => '<div class="input-group" style="margin: 10px 0;">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </span>
                                <span class="form-control">
                                    <span class="pull-left">
                                        <span class="range-value">{value}</span>
                                    </span>
                                    <b class="caret"></b>
                                    {input}
                                </span>
                                <div style="clear: both;"></div>
                            </div>',
    'pluginOptions'=>[
        'locale'=>['format'=>'Y-m-d']
    ],
    'pluginEvents' => [
//                'apply.daterangepicker' => 'function() { console.log(data:this.value); }',
        'apply.daterangepicker' => 'function() { $.pjax({
                                                        type: "POST",
                                                        url: "/agent/corp",
                                                        container: "#w1-pjax",
                                                        data: {data:this.value},
                                                      })}',
    ]
]);

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'font-size:12px;'],
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        'eTicketNumber',
        [
            'attribute' => 'TicketingDate',
            'value' => function ($model) {
                return $model->TicketingDate;
            },
        ],
        'PNRNo',
        [
            'attribute' => 'Passenger',
            'value' => function ($model) {
                return $model->NamePrefix . " " . $model->GivenName . " " . $model->Surname;
            }
        ],
        [
            'attribute' => 'Flight',
            'format' => 'html',
            'value' => function ($model) {
                    $result = '';
                    foreach (explode(";", $model->DepartureAirport) as $key => $DepartureAirport) {
                        $breakPoint = $key == count(explode(";", $model->DepartureAirport)) - 1 ? "" : "<br>";
                        $result .= explode(";", $model->DepartureAirport)[$key];
                        $result .= '-';
                        $result .= explode(";", $model->ArrivalAirport)[$key];
                        $result .= ' / ';
                        $result .= date("H:i d.m.Y", strtotime(explode(";", $model->DepartureDateTime)[$key]));
                        $result .= $breakPoint;
                    }
                return $result;
            }
        ],
        'ValidatingAirline',
        [
            'attribute' => 'GrandTotalFare',
            'value' => function ($model) {
                return number_format($model->GrandTotalFare, 0, " ", " ");
            }
        ],
    ],
    'hover' => true,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Corporation Report</h3>',
        'type'=>'success',
//        'before' => '',
//        'before' => $this->render('_offline_stats',['counter' => $counter]), //'<div> Today Bought ' . $counter["todayBought"] . '; Bought Online '. $counter["todayOnline"] .'; Bought Offline '. $counter["todayOffline"] .'</div>',
//        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
//        'beforeGrid'=>'My fancy content before.',
//        'afterGrid'=>'My fancy content after.',
    ]
]);
Pjax::end();

?>