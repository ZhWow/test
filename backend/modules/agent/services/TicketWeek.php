<?php
namespace backend\modules\agent\services;

use DateTime;

class TicketWeek
{
    public static function getWeekInterval($filterDate)
    {
        $arrWeeks = [];
        $countDay = date('t', strtotime($filterDate . '-01'));

        for ($i = 1; $i <= $countDay; $i++) {
            $week = date('W', strtotime($filterDate . '-' . $i));
            $arrWeeks[] = $week;
        }
        return array_values(array_unique($arrWeeks));
    }

    public static function checkWeekKey($weeks)
    {
        $keys = $weeks;
        if ($keys[0] + 1 != $keys[1]) {
            unset($keys[0]);
            $keys = array_values($keys);
        }
        return $keys;
    }

    public static function getStartEndDate($week, $year)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['weekStart'] = $dto->format('d-m-y');
        $dto->modify('+6 days');
        $ret['weekEnd'] = $dto->format('d-m-y');

        return $ret['weekStart'] . ' : ' . $ret['weekEnd'];
    }
}