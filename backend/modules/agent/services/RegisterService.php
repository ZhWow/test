<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 16.01.2018
 * Time: 15:39
 */

namespace backend\modules\agent\services;


use modules\robot\frontend\providers\epower\methods\SignOut;
use Yii;
use modules\organisation\models\manager\TApiCredentials;
use modules\robot\frontend\providers\epower\models\BTMRequest;
use modules\robot\frontend\providers\epower\models\EPSession;
use modules\robot\frontend\providers\epower\services\EPCredentials;
use modules\robot\frontend\providers\epower\methods\GetPNR;

class RegisterService
{
    public static function register($post)
    {
        $result = false;
        try {
            $credential = TApiCredentials::find()->select(['pult_name', 'pult_type'])->where(['agency_id' => $post['agencyId']])->asArray()->one();
//            $pultsName = PultsName::getName($credential['pult_name']);
            $pultsName = $credential['pult_name'];
            $pultsType = $credential['pult_type'];
            $requestData = [
                'request' => 'GetPNR',
                'json' => urlencode(json_encode(['PNR' => $post['pnr'], 'lastname' => $post['surname']])),
                'user_id' => md5('registerBook'),
                'pult_name' => $pultsName
            ];

            $BTMRequest = new BTMRequest();
            $BTMRequest->attributes = $requestData;
            $EPSession = EPSession::checkSessionByUID($BTMRequest, $pultsName);

            if ($BTMRequest->validate()) {
                $tBook = SetTBook::setData($post);
                if ($tBook) {
                    $tBook->pultname = $pultsName;
                    $tBook->pultversion = $pultsType;
                    $EPCredentials = new EPCredentials($BTMRequest->pult_name, $BTMRequest->user_id, $EPSession, $pultsType);
                    $GetPNR = new GetPNR($EPCredentials);
                    $iBook = $GetPNR->pnr($BTMRequest, $tBook);
                    if ($iBook && $iBook->validate() && $iBook->save()) {
                        $result = true;
					} else {
                        $mess = (!empty($iBook)) ? $iBook->getErrors() : 'Model Book is not valid';
                        Yii::error($mess, 'register_book');
                    }
                    $SignOut = new SignOut($EPCredentials, $GetPNR->SessionID);
                }
            } else {
                Yii::error($BTMRequest->getErrors(), 'register_book');
            }
        } catch (\Exception $e) {
            Yii::error($e, 'register_book');
        }

        return $result;
    }
}