<?php
namespace backend\modules\agent\services;

use yii\data\ArrayDataProvider;
use backend\models\Corp;
use modules\organisation\models\flights\TTickets;
use modules\organisation\models\hotel\HReservationsConfirmed;

class CorpReport
{
    public static function air($post)
    {
        $year = ($post['year']) ? $post['year'] : date("Y");
        $month = ($post['month']) ? $post['month'] : date("m");
        $corpReportArray = [];
        $ticketWeekResult = [];
        $ticketWeekResultPhone = [];
        $ticketWeekResultCorpPhone = [];
        $timeFilter = ['year' => $year, 'month' => $month];
        $corps = Corp::find()->all();
        $weeks = TicketWeek::checkWeekKey(TicketWeek::getWeekInterval("{$year}-{$month}"));
        $ticketCountWeek = array_fill_keys($weeks, 0);

        foreach ($corps as $key => $corp) {
            $ticketCount = TTickets::search($corp['base'])
                ->where(['YEAR(TicketingDate)' => $year])
                ->andWhere(['WEEK(TicketingDate)' => $weeks])
                ->count();

            foreach ($weeks as $week) {
                $weekCount = TTickets::search($corp['base'])
                    ->where(['YEAR(TicketingDate)' => $year])
                    ->andWhere(['WEEK(TicketingDate)' => $week])
                    ->count();

                $weekCountPhone = TTickets::search($corp['base'])
                    ->where(['YEAR(TicketingDate)' => $year])
                    ->andWhere(['WEEK(TicketingDate)' => $week])
                    ->andWhere(['t_bookId' => 0])
                    ->count();

                $ticketCountCorpPhone = TTickets::search($corp['base'])
                    ->where(['YEAR(TicketingDate)' => $year])
                    ->andWhere(['WEEK(TicketingDate)' => $week])
                    ->andWhere(['t_bookId' => 0])
                    ->count();

                $ticketWeekResult[$week] = $weekCount;
                $ticketWeekResultCorpPhone['phone_' . $week] = $ticketCountCorpPhone;

                $phoneCount = !empty($ticketWeekResultPhone['phone_' . $week]) ? (int)$ticketWeekResultPhone['phone_' . $week] : 0;
                $ticketWeekResultPhone['phone_' . $week] = $phoneCount + (int)$weekCountPhone;
            }

            $totalTicketPhone = array_sum(array_map(function ($item) {
                return $item;
            }, $ticketWeekResultCorpPhone));

            $corpReportArray[$key]["company"] = $corp["company"];
            $corpReportArray[$key]["count"] = $ticketCount;
            $corpReportArray[$key]["countPhone"] = $totalTicketPhone;
            $corpReportArray[$key]["ticketWeek"] = $ticketWeekResult;
            $corpReportArray[$key]["ticketCountCorpPhone"] = $ticketWeekResultCorpPhone;

            if ($key === 0) $corpReportArray[0]['week'] = $weeks;
            if (!empty($corpReportArray[0]['week'])) {
                $corpReportArray[$key]['week'] = $corpReportArray[0]['week'];
            }
        }

        $totalTicket = array_sum(array_map(function ($item) {
            return $item['count'];
        }, $corpReportArray));

        array_map(function ($item) use (&$ticketCountWeek) {
            foreach ($item['ticketWeek'] as $k => $v) {
                $ticketCountWeek[$k] = $ticketCountWeek[$k] + $v;
            }
        }, $corpReportArray);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $corpReportArray,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'attributes' => [
                    'count',
                    'company'
                ]
            ]
        ]);

        return ['dataProvider' => $dataProvider, 'totalTicket' => $totalTicket, 'timeFilter' => $timeFilter, 'ticketCountWeek' => $ticketCountWeek, 'ticketWeekResultPhone' => $ticketWeekResultPhone];
    }

    public static function hotel($post)
    {
        $year = ($post['year']) ? $post['year'] : date("Y");
        $month = ($post['month']) ? $post['month'] : date("m");
        $corpReportArray = [];
        $ticketWeekResult = [];
        $timeFilter = ['year' => $year, 'month' => $month];
        $corps = Corp::find()->all();
        $weeks = TicketWeek::checkWeekKey(TicketWeek::getWeekInterval("{$year}-{$month}"));
        $ticketCountWeek = array_fill_keys($weeks, 0);

        foreach ($corps as $key => $corp) {
            $ticketCount = HReservationsConfirmed::search($corp['base'])
                ->where(['YEAR(confirmationDate)' => $year])
                ->andWhere(['WEEK(confirmationDate)' => $weeks])
                ->count();

            foreach ($weeks as $week) {
                $weekCount = HReservationsConfirmed::search($corp['base'])
                    ->where(['YEAR(confirmationDate)' => $year])
                    ->andWhere(['WEEK(confirmationDate)' => $week])
                    ->count();
                $ticketWeekResult[$week] = $weekCount;
            }

            $corpReportArray[$key]['company'] = $corp['company'];
            $corpReportArray[$key]['count'] = $ticketCount;
            $corpReportArray[$key]['ticketWeek'] = $ticketWeekResult;

            if ($key === 0) $corpReportArray[0]['week'] = $weeks;
            if (!empty($corpReportArray[0]['week'])) {
                $corpReportArray[$key]['week'] = $corpReportArray[0]['week'];
            }
        }

        $totalTicket = array_sum(array_map(function ($item) {
            return $item['count'];
        }, $corpReportArray));

        array_map(function ($item) use (&$ticketCountWeek) {
            foreach ($item['ticketWeek'] as $k => $v) {
                $ticketCountWeek[$k] = $ticketCountWeek[$k] + $v;
            }
        }, $corpReportArray);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $corpReportArray,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'attributes' => [
                    'count',
                    'company'
                ]
            ],
        ]);

        return ['dataProvider' => $dataProvider, 'totalTicket' => $totalTicket, 'timeFilter' => $timeFilter, 'ticketCountWeek' => $ticketCountWeek];
    }
}