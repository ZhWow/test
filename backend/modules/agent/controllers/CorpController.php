<?php

namespace backend\modules\agent\controllers;

use Yii;
use backend\modules\api\models\SysUserCorp;
use backend\modules\notifications\models\base\TCorp;
use backend\modules\notifications\models\base\TElectronictickets;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

class CorpController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if( !Yii::$app->user->can('corp_accountant')) exit();
        $user = SysUserCorp::findOne(['user_id' => Yii::$app->user->identity->id]);
        $corp = TCorp::findOne(['id' => $user->corp_id]);
        TElectronictickets::setDb($corp->base);

        if(Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $dateRange = explode(" - ", $post["data"]);

            $tickets = TElectronictickets::find()
                ->where(['between', 'str_to_date(TicketingDate, "%Y-%m-%d")', new Expression('str_to_date("'.$dateRange[0].'","%Y-%m-%d")') ,  new Expression('str_to_date("'.$dateRange[1].'","%Y-%m-%d")')])
                ->all();

        } else {
            $tickets = TElectronictickets::find()
                ->where(['=', 'month(str_to_date(TicketingDate, "%Y-%m-%d"))', new Expression('month(current_date())')])
                ->all();
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $tickets,
            'pagination' => false,
        ]);

        return $this->render('corp-report', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
