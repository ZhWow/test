<?php

namespace backend\modules\agent\controllers;


use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use backend\modules\agent\models\Revalidation;

class RevalidationController extends Controller
{
    public function actionDateTimeInfo()
    {
        $result = null;
        $id = Yii::$app->request->post()['id'];
        $corpId = Yii::$app->request->post()['corpId'];

        $result = Revalidation::createDateTime($id, $corpId);
        return json_encode($result);
    }

    public function actionRevalidate()
    {
        $post = Yii::$app->request->post();
        parse_str($post['data'], $data);
        $model = new  Revalidation();
        $result = $model->revalidate($post['id'], $post['corpId'], $data['Revalidation']);

        if ($result['status']) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been done'));
        } else {
            $errMessge = empty($result['message']) ? 'Error to revalidation' : $result['message'];
            Yii::$app->session->setFlash('error', Yii::t('app', $errMessge));
        }

        return $this->redirect(Url::toRoute('/agent/default/active-tickets'));
    }
}