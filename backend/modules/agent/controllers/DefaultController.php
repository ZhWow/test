<?php

namespace backend\modules\agent\controllers;

use backend\modules\agent\models\CheckSirenaGETRQ;
use backend\modules\agent\models\FlightRegistration;
use backend\modules\agent\models\PdfUpload;
use backend\modules\agent\models\PNR;
use backend\modules\agent\models\QwayActiveTicket;
use backend\modules\agent\models\SendMail;
use backend\modules\agent\services\RegisterService;
use backend\modules\integra\services\TelegramBot;
use backend\modules\agent\models\RegisterBook;
use backend\modules\agent\services\SetTBook;
use common\helpers\PultsName;
use modules\organisation\models\flights\TBook;
use modules\organisation\models\manager\TApiCredentials;
use modules\robot\frontend\providers\epower\methods\GetPNR;
use modules\organisation\models\manager\TPortals;
use modules\robot\frontend\providers\epower\methods\SignOut;
use modules\robot\frontend\providers\epower\models\BTMRequest;
use modules\robot\frontend\providers\epower\models\EPSession;
use modules\robot\frontend\providers\epower\services\EPCredentials;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use modules\flight\frontend\models\base\FlightElectronicTickets;
use modules\flight\frontend\models\base\FlightItineraries;
use modules\flight\frontend\models\base\FlightPenalties;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\providers\amadeus\Amadeus;
use modules\flight\frontend\models\ElectronicTickets;
use backend\models\Corp;
use backend\modules\agent\models\PenaltyForm;
use backend\modules\integra\models\AmadeusIntegra;
use backend\modules\integra\services\MailBot;

class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['report', 'corp-report'],
                'rules' => [
                    [
                        'actions' => ['report'],
                        'allow' => true,
                        'roles' => ['accountant'],
                    ],
                    [
                        'actions' => ['corp-report'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
                'denyCallback' => function () {
                    throw new \yii\web\NotFoundHttpException();
                }
            ],
        ];
    }

    public function actionCancelPnr()
    {
        $provider = new Amadeus();
        $response = $provider->cancelBook(Yii::$app->request->post());

        echo $response;
    }


    public function actionRegisterBook()
    {

        $model = new RegisterBook();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post()['data'];
            parse_str($post, $post);
            $register = RegisterService::register($post['RegisterBook']);

            if ($register) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully has been added'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error to add book'));
            }
            return $this->redirect(Url::toRoute('default/register-book'));
        }

        return $this->render('set-book', ['model' => $model]);
    }

    public function actionCorpUser()
    {
        $post = Yii::$app->request->post();
        $result = RegisterBook::getCorpUser($post['corp']);

        return json_encode($result);
    }

    public function beforeAction($action)
    {
        if ($action->id === 'qway-get-penalty-modal') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}