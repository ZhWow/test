<?php


namespace backend\modules\agent\models;


use yii\base\Model;

class PNR extends Model
{
    public $pnr;
    public $surname;

    public function rules()
    {
        return [
          [['pnr','surname'], 'string'],
          [['pnr','surname'], 'required'],
          ['pnr', 'string', 'max' => 6, 'min' => 6],
        ];
    }

}