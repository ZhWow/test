<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 22.11.2017
 * Time: 17:56
 */

namespace backend\modules\agent\models;

use Yii;

class PdfUpload
{
    public static function upload($file, $base)
    {
        $path = __DIR__ . '../../../../../../'.$base.'/pdf_files/';

        if ($file['pdf_file']['error'] == UPLOAD_ERR_OK) {
            $tmp_name = $file["pdf_file"]["tmp_name"];
            $name = basename($file["pdf_file"]["name"]);
            move_uploaded_file($tmp_name, $path . $name);
            $result = $path . $name;
        } else {
            $result = false;
        }

        return $result;
    }
}