<?php

namespace backend\modules\agent\models;

use yii\base\Model;
use Yii;

class CorpReportFilterForm extends Model
{
    public $month;
    public $year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'year'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'month' => \Yii::t('app', 'Month'),
            'year' => \Yii::t('app', 'Year'),
        ];
    }

    public static function getMonthList()
    {
        return [
            1 => Yii::t('app', 'January'),
            2 => Yii::t('app', 'February'),
            3 => Yii::t('app', 'Mart'),
            4 => Yii::t('app', 'April'),
            5 => Yii::t('app', 'May'),
            6 => Yii::t('app', 'June'),
            7 => Yii::t('app', 'July'),
            8 => Yii::t('app', 'August'),
            9 => Yii::t('app', 'September'),
            10 => Yii::t('app', 'October'),
            11 => Yii::t('app', 'November'),
            12 => Yii::t('app', 'December'),
        ];
    }

    public static function getYearList()
    {
        return [
            2016 => 2016,
            2017 => 2017,
            2018 => 2018,
        ];
    }
}