<?php

namespace backend\modules\agent\models;

use yii\base\Model;

class DateFilterForm extends Model
{
    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from' => \Yii::t('app', 'From'),
            'to' => \Yii::t('app', 'To'),
        ];
    }
}