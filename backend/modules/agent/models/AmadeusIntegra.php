<?php


namespace backend\modules\agent\models;

use backend\modules\agent\models\TAgencyOids;
use backend\models\Corp;
use backend\modules\integra\models\Signed;
use common\models\User;
use modules\flight\frontend\models\base\FlightElectronicTickets;

class AmadeusIntegra extends \backend\modules\integra\models\base\AmadeusIntegra
{
    public function getFligthTicket()
    {
        return $this->hasOne(FlightElectronicTickets::className(), ['ticket_number', 'DOCUMENT_NUMBER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorp()
    {
        return $this->hasOne(Corp::className(),['id' => 'CORP_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(TAgencyOids::className(),['OID' => 'FIRST_OWNER_OFFICEID']);
    }

    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(),['id' => 'updated_by']);
    }
}