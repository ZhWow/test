<?php

namespace backend\modules\agent\models;


use Yii;
use yii\base\Model;
use backend\models\TCorpTUsers;
use backend\models\Corp;
use modules\organisation\models\common\TUsers;
use modules\organisation\models\manager\TUsers as MUsers;

class RegisterBook extends Model
{

    public $pnr;
    public $surname;
    public $agencyId;
    public $corpId;
    public $userId;
    public $servicetype;
    public $serviceFee;

    public function rules()
    {
        return [
            [['pnr', 'surname', 'agencyId', 'corpId', 'userId', 'servicetype', 'serviceFee'], 'required'],
            [['pnr'], 'string', 'min' => 1, 'max' => 6],
            [['servicetype'], 'string'],
            [['pnr', 'surname', 'agencyId'], 'trim'],
            [['surname'], 'string', 'max' => 60],
            [['agencyId', 'corpId', 'userId', 'serviceFee'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'pnr' => \Yii::t('app', 'pnr'),
            'surname' => \Yii::t('app', 'surname'),
            'agencyId' => \Yii::t('app', 'agencyId'),
            'corpId' => \Yii::t('app', 'corpId'),
            'userId' => \Yii::t('app', 'userId'),
            'servicetype' => \Yii::t('app', 'Service type'),
            'serviceFee' => \Yii::t('app', 'Service fee'),
        ];
    }

    public static function getAgency()
    {
        $result = MUsers::find()->where(['role' => 3])->asArray()->all();
        if (Yii::$app->user->can('saryarka')) {
            $result = MUsers::find()->where(['id' => [175, 205]])->asArray()->all();
        }
        return $result;
    }

    public static function getCorp()
    {
        $result = Corp::find()->asArray()->all();
        if (Yii::$app->user->can('saryarka')) {
            $corpUsers = TCorpTUsers::find()->select('t_corp_id')->where(['t_users_id' => Yii::$app->user->id])->column();
            $result = Corp::find()->where(['id' => $corpUsers])->asArray()->all();
        }

        return $result;
    }

    public static function getUser()
    {
        $result = [];
        $TUsers = TUsers::search('samrukenergy')->where(['differents' => 0])->asArray()->all();
        foreach ($TUsers as $TUser) {
            $TUser["formName"] = strtoupper($TUser["lastname"] . " " . $TUser["firstname"]);
            $result[] = $TUser;
        }
        return $result;
    }

    public static function getCorpUser($corpId)
    {
        $result = [
            'status' => false,
            'option' => '<option value="">Select</option>'
        ];
        $corp = Corp::find()->select('base')->where(['id' => $corpId])->scalar();

        if ($corp) {
            $users = TUsers::search($corp)->where(['differents' => 0])->asArray()->all();
            if ($users) {
                foreach ($users as $user) {
                    $result['status'] = true;
                    $result['option'] .= '<option value="' . $user['ID'] . '">' . strtoupper($user["lastname"] . " " . $user["firstname"]) . '</option>';
                }
            }
        }

        return $result;
    }
}