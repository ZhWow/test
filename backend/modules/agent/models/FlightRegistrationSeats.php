<?php

namespace backend\modules\agent\models;

use Yii;
use yii\db\ActiveRecord;

class FlightRegistrationSeats extends ActiveRecord
{
    public static function tableName()
    {
        return 'flt_registration_seats';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }
}