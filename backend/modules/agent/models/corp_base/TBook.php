<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 26.11.2017
 * Time: 23:24
 */

namespace backend\modules\agent\models\corp_base;


use Yii;
use yii\db\ActiveRecord;

class TBook extends ActiveRecord
{
    private static $_db;

    public static function tableName()
    {
        return 't_book';
    }

    public static function getDb()
    {
        return Yii::$app->get(self::$_db);
    }

    public static function setDb($db)
    {
        self::$_db = $db;
    }
}