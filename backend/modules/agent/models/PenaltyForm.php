<?php

namespace backend\modules\agent\models;

use yii\base\Model;

class PenaltyForm extends Model
{
    public $penalty;
    public $corp;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['penalty', 'corp'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'penalty' => \Yii::t('app', 'Penalty'),
        ];
    }
}