<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 22.11.2017
 * Time: 18:30
 */

namespace backend\modules\agent\models;

use Yii;
use yii\base\Model;

class SendMail extends Model
{
    public static function send($email, $attachment, $data, $template)
    {
        $mail =  Yii::$app
            ->mailer
            ->compose(
                ['html' => $template],
                ['data' => $data]
            )
            ->setFrom(['digital@qway.kz' => 'Qway no-reply'])
            ->setTo($email)
            ->setSubject('Онлайн регистрация');
        $mail->attach($attachment);
        return $mail->send();
    }
}