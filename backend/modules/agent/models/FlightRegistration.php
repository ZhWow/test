<?php


namespace backend\modules\agent\models;

use backend\models\Corp;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class FlightRegistration extends ActiveRecord
{
    public static function tableName()
    {
        return 'flt_registration';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    public function getSeats()
    {
        return $this->hasOne(FlightRegistrationSeats::className(), ['id' => 'seat_id']);
    }

    public function getCorp()
    {
        return $this->hasOne(Corp::className(), ['id' => 'corp_id']);
    }

    public function getBook()
    {
        $query = new Query();

        return $query->select('*')->from($this->corp->base.'.t_book')->where(['id' => $this->book_id])->one();
    }
    public function getFlightBook()
    {
        $query = new Query();

        return $query->select('*')->from($this->corp->base.'.t_book')->where(['id' => $this->book_id])->one();
    }

    public function getFlightPassenger()
    {
        $query = new Query();
        $return = [];

        if ($this->passenger_id > 0) {
            $return = $query->select('*')->from($this->corp->base.'.t_passengers')->where(['ID' => $this->passenger_id])->one();
        } else {
            $return = $query->select('*')->from($this->corp->base.'.t_users')->where(['ID' => $this->user_id])->one();
        }
        return $return;
    }

    public static function setTicketStatus($base, $bookId, $passenger)
    {
        $query = new Query();

        $return = false;

        $ticket_id = $query->select('*')->from($base.'.t_electronictickets')->where(['t_bookID' => $bookId, 'GivenName' => $passenger['firstname'], 'Surname' => $passenger['lastname']])->one();
        if ($ticket_id['id']) {
            Yii::$app->db->createCommand('UPDATE '.$base.'.t_electronictickets SET registration_status = 1 WHERE id = ' . $ticket_id['id'])
            ->execute();
            $return = $ticket_id['id'];
        }

        return $return;
    }

    public function updateStatus($passenger)
    {
        $return = false;
        $book = $this->getFlightBook();
        $ticketId = self::setTicketStatus($this->corp->base, $book['id'], $passenger);

        if ($ticketId) {
            $this->status = 1;
            $this->flight_ticket_id = $ticketId;
            $return = $this->save();
        }

        return $return;
    }
}