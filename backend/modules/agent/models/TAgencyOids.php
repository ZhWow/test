<?php


namespace backend\modules\agent\models;

use Yii;
/**
 * This is the model class for table "t_agency_oids".
 *
 * @property integer $id
 * @property string $OID
 * @property string $AgencyName
 * @property string $AgencyType
 * @property integer $AgencyID
 **/

use yii\db\ActiveRecord;

class TAgencyOids extends ActiveRecord
{

    /**
     * @return object
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_agency_oids';
    }

    public function rules()
    {
        return [
            [['OID','AgencyName','AgencyType'], 'string'],
            [['AgencyID'], 'integer'],
        ];
    }
}