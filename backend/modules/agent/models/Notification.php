<?php

namespace backend\modules\agent\models;

use Yii;
use backend\modules\integra\services\TelegramBot;

class Notification
{
    public static function send($view, $param, $title, $email)
    {
        return Yii::$app->mailer->compose($view, [
            'param' => $param
        ])
            ->setFrom([
                'info@btmc.kz' => 'BTM No-Reply'
            ])
            ->setTo($email)
            ->setSubject('BTM: ' . $title)
            ->send();
    }
    
    public static function bookParse($corp, $book)
    {
        $message = 'Бронирование изменено' . PHP_EOL;
        $message .= "Код брони: " . $book->PNR . PHP_EOL;
        $message .= "Компания: " . $corp->company_ru . PHP_EOL;

        return $message;
    }

    public static function telegramSend($message)
    {
        $key = Yii::$app->params['telegram']['flights']['Key'];
        $telegramBot = new TelegramBot();

        $telegramBot->pushingBot($key, $message);
    }
}