<?php

namespace backend\modules\agent\models;

use Yii;

/**
 * This is the model class for table "t_book".
 *
 * @property integer $id
 * @property integer $status
 * @property string $PNR
 * @property string $passenger_emails
 * @property string $timelimit
 * @property string $price_all
 * @property string $detail_price
 * @property string $fares
 * @property string $fees
 * @property string $serviceFee
 * @property string $baggages
 * @property string $pultname
 * @property string $pultversion
 * @property string $Surnames
 * @property string $Names
 * @property string $birthdates
 * @property string $sexs
 * @property string $states
 * @property string $doc_types
 * @property string $doc_nums
 * @property string $doc_exp_dates
 * @property string $PCCs
 * @property string $phones
 * @property string $email
 * @property string $depair
 * @property string $arrair
 * @property string $depdate
 * @property string $deptime
 * @property string $arrdate
 * @property string $arrtime
 * @property string $duration
 * @property string $companies
 * @property string $flights
 * @property string $sub_classes
 * @property string $servicetype
 * @property string $currency
 * @property string $currentDate
 * @property integer $id_user
 * @property string $buying_date
 * @property string $agency
 * @property integer $agency_id
 * @property string $bookdate
 * @property string $reference_number
 * @property string $val_company
 * @property integer $days_before_dep
 * @property integer $flight_type
 */
class WATBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_book';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'status', 'agency_id', 'days_before_dep', 'flight_type'], 'integer'],
            [['val_company'], 'required'],
            [['PNR', 'passenger_emails', 'timelimit', 'price_all', 'detail_price', 'fares', 'fees', 'baggages', 'pultname', 'pultversion', 'Surnames', 'Names', 'birthdates', 'doc_nums', 'doc_exp_dates', 'phones', 'email', 'currency'], 'string', 'max' => 300],
            [['serviceFee', 'sexs', 'states', 'doc_types', 'PCCs'], 'string', 'max' => 200],
            [['depair', 'arrair', 'companies', 'flights', 'sub_classes', 'servicetype', 'buying_date', 'agency', 'bookdate', 'reference_number'], 'string', 'max' => 100],
            [['depdate', 'deptime', 'arrdate', 'arrtime', 'duration'], 'string', 'max' => 150],
            [['currentDate'], 'string', 'max' => 50],
            [['val_company'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'PNR' => 'Pnr',
            'passenger_emails' => 'Passenger Emails',
            'timelimit' => 'Timelimit',
            'price_all' => 'Price All',
            'detail_price' => 'Detail Price',
            'fares' => 'Fares',
            'fees' => 'Fees',
            'serviceFee' => 'Service Fee',
            'baggages' => 'Baggages',
            'pultname' => 'Pultname',
            'pultversion' => 'Pultversion',
            'Surnames' => 'Surnames',
            'Names' => 'Names',
            'birthdates' => 'Birthdates',
            'sexs' => 'Sexs',
            'states' => 'States',
            'doc_types' => 'Doc Types',
            'doc_nums' => 'Doc Nums',
            'doc_exp_dates' => 'Doc Exp Dates',
            'PCCs' => 'Pccs',
            'phones' => 'Phones',
            'email' => 'Email',
            'depair' => 'Depair',
            'arrair' => 'Arrair',
            'depdate' => 'Depdate',
            'deptime' => 'Deptime',
            'arrdate' => 'Arrdate',
            'arrtime' => 'Arrtime',
            'duration' => 'Duration',
            'companies' => 'Companies',
            'flights' => 'Flights',
            'sub_classes' => 'Sub Classes',
            'servicetype' => 'Servicetype',
            'currency' => 'Currency',
            'currentDate' => 'Current Date',
            'id_user' => 'Id User',
            'buying_date' => 'Buying Date',
            'agency' => 'Agency',
            'agency_id' => 'Agency ID',
            'bookdate' => 'Bookdate',
            'reference_number' => 'Reference Number',
            'val_company' => 'Val Company',
            'days_before_dep' => 'Days Before Dep',
            'flight_type' => 'Flight Type',
        ];
    }

    public function setServiceFee($serviceFee)
    {
        $result = '';
        $surnames = explode(";", $this->Surnames);

        for ($i=0; $i < count($surnames); $i++){
            $breakPoint = count($surnames)-1 == $i ? "" : ";";
            $result .= $serviceFee . $breakPoint;
        }

        $this->serviceFee = $result;
        $this->updatePrices();
    }

    public function updatePrices()
    {
        $fares = explode(';', $this->fares);
        $fees = explode(';', $this->fees);
        $serviceFees = explode(';', $this->serviceFee);
        $this->detail_price = '';
        $elementCount = count(explode(";", $this->Surnames));

        $iter = 1;
        for ($i = 0; $i < $elementCount; $i++) {
            $breakPoint = ($iter == $elementCount) ? '' : ';';
            $this->detail_price .= ((int)$fares[$i] + (int)$fees[$i] + (int)$serviceFees[$i]) . $breakPoint;
            $iter++;
        }
        $dPrice = explode(';', $this->detail_price);
        $this->price_all = (string)array_sum($dPrice);
    }
}
