<?php

namespace backend\modules\agent\assets;

use yii\web\AssetBundle;

class AgentAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/agent/assets';
    public $css = [
        'css/agency.css',
        'css/jquery.fancybox.min.css'
    ];
    public $js = [
        'js/vue.js',
        'js/jquery.fancybox.min.js',
        'js/functions.js',
        'js/agent.js',
		'js/table2excel.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}