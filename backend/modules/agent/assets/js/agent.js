var modalWebApp = new Vue({
    el:"#webapp-documents-container",
    data: {
        docItems:[]
    }
});


(function ($) {
    var $body = $('body'),
        $loader = $('#overlay-loader');

    $body.delegate('.penalty-btn', 'click', function (e) {
        e.preventDefault();

        var href = $(this).closest('a').attr('href');

        $.ajax({
            url: href,
            method: 'get',
            success: function (response) {
                $('#penalty-content').empty().append(response);
                $('#penalty-modal').modal('show');
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    $body.delegate('#penalty_submit', 'click', function (e) {
        e.preventDefault();

        var data = $('#penalty-content').find('input');

        $.ajax({
            url: '/agent/default/penalty?id=' + $('#electronictickets-id').val(),
            method: 'post',
            data: $(data).serialize(),
            success: function (response) {
                if (JSON.parse(response) === 'ok') {
                    $('#penalty-modal').modal('hide');
                } else {

                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    $body.on('change', '.reg-file', function () {
        var File = $(this)[0].files[0],
            $submitButton = $(this).next(),
            formData = new FormData,
            registration_id = $(this).parent().find('[name="registration_id"]').val();

        if (File !== undefined) {
            formData.append('pdf_file', File);
            formData.append('id', registration_id);
            $loader.show();
            $.ajax({
                url: location.href,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    $loader.hide();
                    var response = JSON.parse(data);

                    swal({
                        title: 'Attention!',
                        text: response.mess,
                        type: response.status
                    }).then(function () {
                        if (response.status === 'success') {
                            $submitButton.prop('disabled', false);
                        }
                    });
                },
                error: function () {
                    $loader.hide();
                    swal({
                        title: 'Attention!',
                        text: 'Error upload file!',
                        type: 'error'
                    });
                }
            });
        } else {
            $submitButton.prop('disabled', true);
        }
    });

    $body.on('submit', '.send-file-pdf', function () {
        let requestData = $(this).serialize();

        $loader.show();

        $.ajax({
            url: '/agent/default/registration-send',
            type: 'POST',
            dataType: 'json',
            data: requestData,
            success: function (response) {
                $loader.hide();

                swal({
                    title: 'Attention!',
                    text: response.mess,
                    type: response.status
                }).then(function () {
                    location.reload();
                });
            },
            error: function () {
                $loader.hide();

                swal({
                    title: 'Attention!',
                    text: 'Error send',
                    type: 'error'
                }).then(function () {
                    location.reload();
                });
            }
        });

        return false;
    });

    $body.on('submit', '#book_info_form', function () {
        $('#overlay-loader').show();
        $('#book_info_modal').modal('hide');

        let requestData = $(this).serialize();

        $.ajax({
            url: '/agent/default/book-modal-save',
            type: 'POST',
            dataType: 'json',
            data: requestData,
            success: function (response) {
                $('#overlay-loader').hide();

                swal({
                    title: 'attention!',
                    text: response.mess,
                    type: response.status
                }).then(function () {
                    location.reload();
                });
            },
            error: function () {
                $('#overlay-loader').hide();

                swal({
                    title: 'attention!',
                    text: 'Book save error!',
                    type: 'error'
                });
            }
        });

        return false;
    });
    $(".fancybox").fancybox();



    $body.on('click', '#get-pnr-modal-btn', function()
        {
            var user_id = $(this).attr('data-userid');
            var model_id = $(this).attr('data-modelid');
            $('#wapnr-user_id').val(user_id);
            $('#wapnr-model_id').val(model_id);
            $("#modal-webapp-getpnr").modal('show');
        }
    );

    $body.on('click', '#get-documents-modal', function()
        {
            var user_id = $(this).attr('data-userid');
            $.ajax({
                url: '/agent/qway/get-documents?id=' + user_id,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    modalWebApp.docItems = response;
                    $("#modal-documents").modal('show');
                }
            });
        }
    );

}(jQuery));