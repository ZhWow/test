function getModalBookingInfo(bookId, corpId) {
    $('#overlay-loader').show();

    $.ajax({
        url: '/agent/default/book-modal-info',
        type: 'GET',
        dataType: 'json',
        data: {
            bookId: bookId,
            corpId: corpId
        },
        success: function (response) {
            $('#overlay-loader').hide();

            let $modal = $('#book_info_modal');

            $modal.find('.form').html(response.mess);
            $modal.modal('show');
        },
        error: function () {
            $('#overlay-loader').hide();

            swal({
                title: 'attention!',
                text: 'Book info not foumd!',
                type: 'error'
            });
        }
    });
}