<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row fields-auth-search">

        <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-6']]) ?>

        <?= $form->field($model, 'type', ['options' => ['class' => 'col-md-6']])->dropDownList($model->typeNameDrop, [
            'prompt' => Yii::t('adm', 'Select type for filter')
        ]) ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('adm', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('adm', 'Reset'), 'index', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>