<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \backend\modules\admin\models\AuthItem */

\backend\modules\admin\assets\AdministratorAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('adm', 'Auth'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'type',
                'value' => $model->typeName
            ],
        ],
    ]) ?>

    <?= $this->render('@backend/modules/admin/views/common/_select_item', [
            'model' => $model,
            'queryString' => $model->name
        ]) ?>

</div>