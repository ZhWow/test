<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\admin\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\backend\modules\admin\assets\AdministratorAsset::register($this);

$this->title = Yii::t('adm', 'Auth');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a(Yii::t('adm', 'Reset Search'), 'index', ['class' => 'btn btn-default']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function ($model) {
                    /* @var $model \backend\modules\admin\models\AuthItem*/
                    return Html::a($model->name, ['/admin-panel/auth/item', 'name' => $model->name,]);
                },
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    /* @var $model \backend\modules\admin\models\AuthItem */
                    return $model->typeName;
                },
                'filter' => Html::activeDropDownList($searchModel, 'type' ,\backend\modules\admin\models\AuthItem::getTypeNameArray(),
                    ['class' => 'form-control', 'prompt' => Yii::t('adm', 'Select type for filter')])
            ],
            [
                'format' => 'raw',
                'attribute' => 'users',
                'value' => function ($model) {
                    /* @var $model \backend\modules\admin\models\AuthItem */
                    $countUsers = count($model->authAssignments);
                    if ($countUsers === 0) {
                        return $countUsers;
                    }
                    return Html::a(count($model->authAssignments), ['/admin-panel/auth/item-users', 'name' => strtolower($model->name)]);
                },
            ],
            'description'
        ],
    ]); ?>
</div>
