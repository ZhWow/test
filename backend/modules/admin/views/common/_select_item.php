<?php

use yii\helpers\Html;
use yii\helpers\Json;

/* @var $model modules\user\common\models\User */
/* @var $queryString $model->id or ... */

$opts = Json::htmlEncode(['items' => $model->getItems()]);
$this->registerJs("var __options = {$opts};", \yii\web\View::POS_HEAD);
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
$queryStringName = is_int($queryString) ? 'id' : 'name';
?>
<div class="row">
    <div class="col-sm-5">
        <input class="form-control search" data-target="available" placeholder="<?= Yii::t('adm', 'Search for available') ?>">
        <select multiple size="20" class="form-control list" data-target="available"></select>
    </div>
    <div class="col-sm-1">
        <br><br>
        <?= Html::a('&gt;&gt;' . $animateIcon, ['assign', $queryStringName => $queryString], [
            'class' => 'btn btn-success btn-assign',
            'data-target' => 'available',
            'title' => Yii::t('adm', 'Assign')
        ]) ?>
        <br><br>
        <?= Html::a('&lt;&lt;' . $animateIcon, ['revoke', $queryStringName => $queryString], [
            'class' => 'btn btn-danger btn-assign',
            'data-target' => 'assigned',
            'title' => Yii::t('adm', 'Remove')
        ]) ?>
    </div>
    <div class="col-sm-5">
        <input class="form-control search" data-target="assigned" placeholder="<?= Yii::t('adm', 'Search for assigned') ?>">
        <select multiple size="20" class="form-control list" data-target="assigned"></select>
    </div>
</div>