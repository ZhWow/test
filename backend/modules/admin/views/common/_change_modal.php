<?php

use yii\bootstrap\Modal;
use modules\log\common\models\LogUser;

Modal::begin([
    'options' => ['id' => 'ap-user-change-modal'],
]);

$logUser = new LogUser();

echo '<div id="ap-user-change-modal-content">';
echo $this->render('@backend/modules/admin/views/log-user/_form', ['model' => $logUser]);
echo '</div>';

Modal::end();