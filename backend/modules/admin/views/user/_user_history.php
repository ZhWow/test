<?php

use yii\grid\GridView;
use modules\user\common\models\User;

/* @var $dataProvider \modules\log\common\models\LogUser */

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'change_by',
            'value' => function ($model) {
                /* @var $model \modules\log\common\models\LogUser */
                if ($model->change_status === User::STATUS_NEW_USER) {
                    return Yii::t('adm', 'Himself');
                } else {
                    return $model->changeByUsername;
                }
            }
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                /* @var $model \modules\log\common\models\LogUser */
                return User::getStatus($model->change_status);
            },
        ],
        [
            'attribute' => 'action',
            'value' => function ($model) {
                /* @var $model \modules\log\common\models\LogUser */
                return $model->action ? $model->action : '';
            }
        ],
        [
            'attribute' => 'assign',
            'value' => function ($model) {
                /* @var $model \modules\log\common\models\LogUser */
                return $model->change_assign ? $model->change_assign : '';
            }
        ],
        [
            'attribute' => 'reason',
            'value' => function ($model) {
                /* @var $model \modules\log\common\models\LogUser */
                return $model->reason ? $model->reason : '';
            }
        ],
        'date',
    ],
]);