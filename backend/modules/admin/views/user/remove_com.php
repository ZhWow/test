<?php
/**
 * Created by BADI.
 * DateTime: 01.03.2017 16:43
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $logModel \modules\log\common\models\LogComUsers */
/* @var $this \yii\web\View */
/* @var $form \yii\bootstrap\ActiveForm */
?>
<div class="admin-user-remove_com">
    <div class="panel">
        <div class="panel-body">

            <?php $form = ActiveForm::begin() ?>

            <?= $form->field($logModel, 'reason')->textarea(['rows' => 3]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger']) ?>
            </div>

            <?php ActiveForm::end() ?>

        </div>
    </div>
</div>
