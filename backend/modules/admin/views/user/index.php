<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
//use modules\user\common\models\User;
//use backend\modules\admin\assets\AdministratorAsset;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $comId int */

//AdministratorAsset::register($this);
\backend\modules\admin\assets\AdministratorAsset::register($this);

$this->title = Yii::t('adm', 'Users');

/* @var bool|string $whoController */
if ($whoController) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('adm', $whoController), 'url' => ['index']];
}

$this->params['breadcrumbs'][] = $this->title;
$url = '/admin-panel/user/';


/* @var object fpan */
$tempJS = <<<JS
var fpan = $('.user-search');

$('#shfb').on('click', function() {
    fpan.is(':visible') ? fpan.hide() : fpan.show();
});
JS;
$this->registerJs($tempJS);
?>

    <div class="user-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <button id="shfb" type="button" class="btn btn-primary">
            <span class="glyphicon glyphicon-cog"></span> Фильтры
        </button>
        <?= Html::a(Yii::t('adm', 'Reset Search'), 'index', ['class' => 'btn btn-default']) ?>

        <?= $this->render('_search', ['model' => $searchModel]); ?>

        <?= /** @noinspection PhpUnusedParameterInspection */
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
//            'rowOptions' => function ($model) {
//                $rowMyOptions['class'] = 'ap-item-user';
//                if ($model->status === User::STATUS_BLOCKED) {
//                    $rowMyOptions['class'] .= ' ap-blocked-user';
//                }
//                return $rowMyOptions;
//            },
            'columns' => [

                'id',
                'username',
                'email',
                [
                    /* @var $model common\models\User */
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return Yii::t('adm', User::getStatus($model->status));
                    },
                    'contentOptions' => function ($model) {
                        return ['class' => 'ap-user-status-' . $model->id];
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'status', \backend\modules\admin\models\UserSearch::status(),
                        ['class' => 'form-control', 'prompt' => Yii::t('adm', 'Select status for filter')])
                ],
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'header' => Yii::t('adm', 'History'),
//                    'template' => '{user-history}',
//                    'buttons' => [
//                        'user-history' => function ($u, $model) use ($url) {
//                            /* @var $model modules\user\common\models\User */
//                            return Html::a('<span class="glyphicon glyphicon-user"></span>',
//                                $url . 'user-history?id=' . $model->id);
//                        },
//                    ],
//                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('adm', 'Assignment'),
                    'template' => '{assignment} {change-status}',
                    'buttons' => [
                        'assignment' => function ($u, $model) use ($url) {

                            return Html::a('<span class="glyphicon glyphicon-cog"></span>',
                                $url . 'assignment?id=' . $model->id);
                        },
//                        'change-status' => function ($u, $model) use ($url) {
//                            /* @var $model User */
//                            if ($model->canIBlock()) {
//                                $urlChange = $url . 'change-status?id=' . $model->id;
//                                if ($model->status === User::STATUS_BLOCKED) {
//                                    return Html::a(
//                                        '<span class="glyphicon glyphicon-off"></span>', $urlChange,
//                                        [
//                                            'id' => 'ap-lock-' . $model->id,
//                                            'class' => 'AdminPanel-modal-show ap-lock',
//                                            'data-message' => 'Unblock user'
//                                        ]
//                                    );
//                                } else {
//                                    return Html::a(
//                                        '<span class="glyphicon glyphicon-off"></span>', $urlChange,
//                                        [
//                                            'id' => 'ap-lock-' . $model->id,
//                                            'class' => 'AdminPanel-modal-show ap-unlock',
//                                            'data-message' => 'Block user'
//                                        ]
//                                    );
//                                }
//                            }
//                            return false;
//                        },
//                        'add-company' => function ($url, $model) use ($comId) {
//                            /* @var $model User */
//                            $button = '';
//                            if (!$model->hasCompany) {
//                                $button = Html::a(Yii::t('adm', 'add com'),
//                                    ['/admin-panel/user/add-company', 'id' => $model->id]);
//                            } else {
//                                if ($comId !== null) {
//                                    $button = Html::a(Yii::t('adm', 'remove from company'),
//                                        ['/admin-panel/user/remove-company', 'id' => $model->id, 'comId' => $comId],
//                                        ['style' => 'color:red',
//                                            'data' => [
//                                                'confirm' => Yii::t('adm', 'Are you sure you want to delete this user from company'),
//                                                'method' => 'post',
//
//                                            ]
//                                        ]
//                                    );
//                                }
//                            }
//                            return $button;
//                        }
                    ],
                ],
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'header' => Yii::t('adm', 'Re login'),
//                    'template' => '{user-re-login}',
//                    'buttons' => [
//                        'user-re-login' => function ($url, $model) {
//                            /* @var $model modules\user\common\models\User */
//                            if (!$model->re_login) {
//                                return Html::a(
//                                    '<span class="glyphicon glyphicon-log-out"></span>',
//                                    ['/admin-panel/user/user-re-login', 'id' => $model->id],
//                                    [
//                                        'id' => 'ap-relogin-' . $model->id,
//                                        'class' => 'AdminPanel-modal-show'
//                                    ]
//                                );
//                            }
//                            return '';
//                        },
//                    ],
//                ],

            ],
        ]); ?>
    </div>
