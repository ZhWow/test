<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use modules\user\common\models\User;

/* @var $model User */

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'username',
        'email:email',
        [
            'attribute' => 'status',
            'value' => $model->statusName
        ],
        [
            'attribute' => 'created_at',
            'value' => $model->createDate
        ]
    ],
]);

echo Html::a(Yii::t('adm', 'Assign'), '/admin-panel/user/assignment?id=' . $model->id, ['class' => 'btn btn-default']);

if ($model->canIBlock()) {
    if ($model->status === User::STATUS_DELETED) {
        echo Html::a(
            Yii::t('adm', 'Activate'),
            '/admin-panel/user/change-status?id=' . $model->id,
            [
                'class' => 'btn btn-default ap-modal-show ap-lock',
                'data-views' => 'view'
            ]
        );
    } else {
        echo Html::a(
            Yii::t('adm', 'Block'),
            '/admin-panel/user/change-status?id=' . $model->id,
            [
                'class' => 'btn btn-default ap-modal-show ap-lock',
                'data-views' => 'view'
            ]
        );
    }
    echo $this->render('@backend/modules/admin/views/common/_change_modal');
}

