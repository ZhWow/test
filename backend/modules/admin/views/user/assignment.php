<?php

use yii\helpers\Html;
use backend\modules\admin\assets\AdministratorAsset;

/* @var $this yii\web\View */
/* @var $model \modules\users\common\models\User*/

$userName = Html::encode($model->username);

$this->title = Yii::t('adm', 'User') . ': ' . $userName;

$this->params['breadcrumbs'][] = ['label' => Yii::t('adm', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $userName;

AdministratorAsset::register($this);
?>
<div class="assignment-index">
    <h1><?= $this->title ?></h1>

    <?= $this->render('@backend/modules/admin/views/common/_select_item', [
        'model' => $model,
        'queryString' => $model->id
    ]) ?>

</div>