<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row fields-user-search">

        <?= $form->field($model, 'role', ['options' => ['class' => 'col-md-6']])->dropDownList($model->rolesSearch(), [
            'prompt' => Yii::t('adm', 'Select role for filter')
        ]) ?>

        <?= $form->field($model, 'permission', ['options' => ['class' => 'col-md-6']])->dropDownList($model->permissionsSearch(), [
            'prompt' => Yii::t('adm', 'Select permission for filter')
        ]) ?>

    </div>

    <?= $form->field($model, 'withoutCom')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('adm', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('adm', 'Reset'), 'index', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>