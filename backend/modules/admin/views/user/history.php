<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\user\common\models\User */
/* @var $dataProvider \modules\log\common\models\LogUser */

\backend\modules\admin\assets\AdministratorAsset::register($this);

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('adm', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-history">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Data',
                'content' => $this->render('_view', ['model' => $model]),
            ],
            [
                'label' => 'History',
                'content' => $this->render('_user_history', ['dataProvider' => $dataProvider]),
            ]
        ]
    ]) ?>

</div>
