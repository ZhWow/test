<?php
/**
 * Created by BADI.
 * DateTime: 01.03.2017 11:19
 */
use modules\company\common\services\ComUserService;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \modules\company\common\models\ComUsers */
/* @var $form ActiveForm */
/* @var $logModel \modules\log\common\models\LogComUsers */
?>
<div class="admin-user-add_com">
    <div class="panel">
        <div class="panel-body">

            <?php $form = ActiveForm::begin() ?>

            <?= $form->field($model, 'com_id')->dropDownList(ComUserService::getCompnyList(), [
                'placeholder' => Yii::t('adm', 'Select company')
            ]) ?>

            <?= $form->field($model, 'role_id') ?>

            <?= $form->field($logModel, 'reason')->textarea(['rows' => 3]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end() ?>

        </div>
    </div>
</div>
