;var adminPanel = function () {
    var url;

    /***************************************/
    /*                 EVENT               */
    /***************************************/

    /* change status */
    $('.AdminPanel-modal-show').on('click', showModal);

    $('#ap-submit').on('click', submitModalData);

    $('.btn-assign').on('click', assign);

    $('.move-user-show-modal').on('click', moveUser);

    $('.search[data-target]').keyup(function () {
        search($(this).data('target'));
    });

    /***************************************/
    /*               FUNCTION              */
    /***************************************/

    /**
     * Show modal window by change status reason
     * @param event
     */
    function showModal(event) {
        event.preventDefault();
        var $this = $(this);
        $('#ap-message').text('');
        $('.modal-header').text('');
        $('#ap-user-change-modal-content').find('form')[0].reset();
        $('#ap-user-change-modal').modal('show');
        // my_todo $('.modal-header').text(_t('adm', 'Block or Unblock user'));
        $('.modal-header').text($this.attr('data-message'));
        url = $(this).closest('a').attr('href');
    }

    /**
     *
     * @param event
     */
    function submitModalData(event) {
        event.preventDefault();
        var data;
        data = $('#ap-user-change-modal-content').find('form').serialize();
        $.ajax({
            context: this,
            url: url,
            type: 'post',
            data: data,
            success: modalDataSuccess
        });
    }

    function modalDataSuccess(response) {
        /* @var $this Form element button submit */

        var $modalBody = $('.modal-body'),
            $this = $(this),
            result = JSON.parse(response);

        $('#ap-message').remove(); // ap-message it is render modalMessage()
        $modalBody.before(modalMessage(result.message));
        $this.prop('disabled', true);

        if (result.status_name) {
            changeStatusUserOK(result);
        }

        if (result.delete_tmp_user) {
            var $showButtonModal = $('#ap-lock-' + result.id);
            $showButtonModal.closest('tr').remove();
        }

        /* If the required fields are empty not to close a modal window */
        if (result.empty_reason) {
            $this.prop('disabled', false);
        } else {
            setTimeout(function () {
                $('#ap-user-change-modal').modal('hide');
                $('#ap-user-change-modal-content').find('form')[0].reset();
                $this.prop('disabled', false);

                // backend/admin/views/user/_view
                if ($('.ap-modal-show').attr('data-views') == 'view') {
                    location.reload();
                }
            }, 2000);
        }
        if (result.re_login) {
            var $showReloginModal = $('#ap-relogin-' + result.id);
            $showReloginModal.is(':visible') ? $showReloginModal.hide() : $showReloginModal.show();
        }
    }

    function changeStatusUserOK(result) {
        var $showButtonModal = $('#ap-lock-' + result.id);
        $('.ap-user-status-' + result.id).text(result.status_name);
        if ($showButtonModal.hasClass('ap-lock')) {
            $showButtonModal.removeClass('ap-lock').addClass('ap-unlock');
            $showButtonModal.closest('tr').removeClass('ap-blocked-user');
            $showButtonModal.attr('data-message', '').attr('data-message', 'Block user');
        } else {
            $showButtonModal.removeClass('ap-unlock').addClass('ap-lock');
            $showButtonModal.attr('data-message', '').attr('data-message', 'Unblock user');
            $showButtonModal.closest('tr').addClass('ap-blocked-user');
        }

        if ($showButtonModal.hasClass('ap-tmp-user')) {
            $showButtonModal.is(':visible') ? $showButtonModal.hide() : $showButtonModal.show();
        }
    }

    function modalMessage(message) {
        return '<div id="ap-message"><h2>' + message + '</h2></div>';
    }

    function updateItems(r) {
        __options.items.available = r.available;
        __options.items.assigned = r.assigned;
        search('available');
        search('assigned');
    }

    function assign() {
        var $this, target, items;
        $this = $(this);
        target = $this.data('target');
        items = $('select.list[data-target="' + target + '"]').val();

        if (items && items.length) {
            $this.children('i.glyphicon-refresh-animate').show();
            $.post($this.attr('href'), {items: items}, function (r) {
                updateItems(r);
            }).always(function () {
                $this.children('i.glyphicon-refresh-animate').hide();
            });
        }
        return false;
    }

    /**
     * @param target string - available or assigned
     */
    function search(target) {
        var $list, searchString, groups;
        $list = $('select.list[data-target="' + target + '"]');
        $list.html('');
        searchString = $('.search[data-target="' + target + '"]').val();

        groups = {
            role: [$('<optgroup label="Roles">'), false],
            permission: [$('<optgroup label="Permission">'), false],
            route: [$('<optgroup label="Route">'), false],
            // child : [$('<optgroup label="Child" id="ap-child-role">'), false]
            // route: [$('<optgroup label="Route">'), false],
        };

        /**
         * @var _opts Global variable object
         * @prop items object
         * */
        /**
         * items object
         * @prop assigned object Roles and permissions which already have user. ({admin: role})
         * @prop available object Roles and permissions are not present at the user. ({moderator: permissions})
         */
        $.each(__options.items[target], function (name, group) {
            group = group.split(',');
            if (name.indexOf(searchString) >= 0) {
                if (group[0] == 'child') {
                    $('<option class="ap-child">')
                        .text('- ' + name + ' (' + group[2] + ')')
                        .val(name).prop('disabled', true)
                        .appendTo(groups[group[1]][0]);
                    groups[group[1]][1] = true;
                } else {
                    $('<option>').text(name).val(name).appendTo(groups[group[0]][0]);
                    groups[group[0]][1] = true;
                }
            }
        });
        $.each(groups, function () {
            if (this[1]) {
                $list.append(this[0]);
            }
        });
    }

    function moveUser(event) {
        event.preventDefault();
    }

    return {
        search: search
    };

}();

if (typeof __options !== 'undefined') {
    $('i.glyphicon-refresh-animate').hide();
    adminPanel.search('available');
    adminPanel.search('assigned');
}