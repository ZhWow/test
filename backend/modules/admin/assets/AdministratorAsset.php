<?php

namespace backend\modules\admin\assets;

use yii\web\AssetBundle;

class AdministratorAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/admin/assets';
    public $css = [
        'css/admin.css',
    ];
    public $js = [
        'js/admin-panel.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}