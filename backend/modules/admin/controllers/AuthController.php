<?php
//declare(strict_types=1);

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\admin\models\AuthItem;
use backend\modules\admin\models\AuthItemSearch;
use backend\modules\admin\models\UserSearch;

/**
 * Class AuthController
 * @package backend\modules\admin\controllers
 * @author BADA
 */
class AuthController extends Controller
{
    /**
     * Lists all AuthItem models.
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the AuthItem model based on its name key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $name
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name)
    {
        if (($model = AuthItem::find()->where(['name' => $name])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single AuthItem model
     * view added and remove Role, Permission
     * @param string $name AuthItem name
     * @return mixed
     */
    public function actionItem($name)
    {
        return $this->render('item', [
            'model' => $this->findModel($name)
        ]);
    }

    /**
     * Lists all users defining AuthItem
     * @param string $name AuthItem name
     * @return mixed
     */
    public function actionItemUsers($name)
    {
        $ids = [];
        $model = $this->findModel($name);
        /* @var $model AuthItem */
        foreach ($model->authAssignments as $item) {
            array_push($ids, $item->user_id);
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ids);
        return $this->render('../user/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'whoController' => 'Auth',
            'comId' => null,
        ]);
    }

    /**
     * Add child roles and permissions
     * @param $name string AuthItem name
     * @return array
     */
    public function actionAssign($name)
    {
        /* @var $model AuthItem */
        $model = $this->findModel($name);
        $items = Yii::$app->getRequest()->post('items', []);
        $success = $model->assign($items, $model);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getItems(), ['success' => $success]);
    }

    /**
     * Remove child roles and permissions
     * @param string $name AuthItem name
     * @return array
     */
    public function actionRevoke($name)
    {
        /* @var $model AuthItem */
        $model = $this->findModel($name);
        $items = Yii::$app->getRequest()->post('items', []);
        $success = $model->revoke($items, $model);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getItems(), ['success' => $success]);
    }
}
