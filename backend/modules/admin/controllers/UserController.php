<?php
//declare(strict_types = 1);

namespace backend\modules\admin\controllers;

/*use modules\company\common\models\ComUsers;
use modules\log\common\models\LogComUsers;*/

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
//use modules\user\common\models\User;
//use backend\modules\admin\models\UserSearch;
//use modules\log\common\models\LogUser;
use yii\web\Response;
use backend\modules\admin\models\UserSearch;
use modules\users\common\models\User;

/**
 * Class UserController
 * @package backend\modules\admin\controllers
 * @author BADA
 */
class UserController extends Controller
{
    /**
     * Lists all User models.
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'whoController' => false,
            'comId' => null
        ]);
    }

    /**
     * Displays a single User model
     * And all the records from one user logs
     * @param int $id User id
     * @return string
     */
    public function actionUserHistory($id)
    {
        /* @var $model User */
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => LogUser::find()->where(['user_id' => $id]),
        ]);

        return $this->render('history', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Display the page to add and delete roles, permissions at the user
     * @param int $id User id
     * @return string
     */
    public function actionAssignment($id)
    {
        /* @var $model User */
        $model = $this->findModel($id);

        return $this->render('assignment', [
            'model' => $model,
        ]);
    }

    /**
     * It changes the value of the user status and writes logs DB (log_user)
     * @param int $id User id
     * @return string
     */
    public function actionChangeStatus($id)
    {
        /* @var $model User */
        $model = $this->findModel($id);
        $result = $model->changeStatus();
        return json_encode($result->content());
    }

    /**
     * Add user roles and permissions
     * @param int $id User id
     * @return array
     */
    public function actionAssign($id)
    {
        /* @var $model User */
        $model = $this->findModel($id);
        $items = Yii::$app->getRequest()->post('items', []);
        $success = $model->assign($items, $model->id);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getItems(), ['success' => $success]);
    }

    /**
     * Remove user roles and permissions
     * @param int $id User id
     * @return array
     */
    public function actionRevoke($id)
    {
        $items = Yii::$app->getRequest()->post('items', []);
        $model = $this->findModel($id);
        $success = $model->revoke($items, $model->id);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getItems(), ['success' => $success]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Making the user re login
     * @param int $id User id
     * @return string Response Json
     */
    public function actionUserReLogin($id)
    {
        /* @var $model User */
        $model = $this->findModel($id);
        $result = $model->reLogin();
        return json_encode($result->content());
    }

    /**
     * @param $id int
     * @return string|Response
     */
    public function actionAddCompany($id)
    {
        $model = new ComUsers();
        $model->user_id = $id;
        $logModel = new LogComUsers();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $logModel->load(Yii::$app->request->post());
            if ($logModel->writeLog($model, 'add company')) {
                return $this->redirect(['index']);
            }
        }
        return $this->render('add_com', [
            'model' => $model,
            'logModel' => $logModel
        ]);
    }

    /**
     * @param $id int
     * @param $comId int
     * @return Response
     */
    public function actionRemoveCompany($id, $comId)
    {
        /* @var $comUser ComUsers */
        $comUser = ComUsers::find()->where(['com_id' => $comId, 'user_id' => $id])->one();
        $logModel = new LogComUsers();
        if ($logModel->load(Yii::$app->request->post()) && $logModel->validate()) {
            if ($comUser !== null && $comUser->delete()) {
                if ($logModel->writeLog($comUser, 'remove from company')) {
                    Yii::$app->session->setFlash('success', Yii::t('adm', 'Com user deleted'));
                    return $this->redirect(['index']);
                }
                //write log
            } else {
                Yii::$app->session->setFlash('error', Yii::t('adm', 'com users not founded or not deleted'));
            }
        }
        return $this->render('remove_com', ['logModel' => $logModel]);
    }
}
