<?php
//declare(strict_types=1);

namespace backend\modules\admin\controllers;

use Yii;
use modules\user\common\models\UserTemp;
use backend\modules\admin\models\UserTempSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class UserTempController
 * @package backend\modules\admin\controllers
 * @author BADA
 */
class UserTempController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserTemp models.
     * @return string
     */
    public function actionIndex() : string
    {
        $searchModel = new UserTempSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserTemp model.
     * @param int $id
     * @return string
     */
    public function actionView(int $id) : string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the UserTemp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return UserTemp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) : UserTemp
    {
        if (($model = UserTemp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Move UserTemp to User
     * @param int $id
     * @return string
     */
    public function actionMoveToUser($id)
    {
        /* @var $model UserTemp */
        $model = $this->findModel($id);
        if (Yii::$app->request->post() && $model->moveUser()) {
            return $this->redirect('index');
        } else {
            return $this->render('move-to-user', ['model' => $model]);
        }
    }

    /**
     * UserTemp blocked
     * @param int $id
     * @return string
     */
    public function actionBlock(int $id) : string
    {
        $model = $this->findModel($id);
        return $model->blockUser();
    }
}
