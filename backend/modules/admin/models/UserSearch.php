<?php

namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use modules\user\common\models\User;
use yii\db\Query;
use modules\users\common\models\User;

/**
 * UserSearch represents the model behind the search form about `modules\user\common\models\User`.
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property array $company
 * @property object $manager
 * @property object $role
 * @property object $permission
 */
class UserSearch extends User
{

    public $role;
    public $permission;
    public $withoutCom;




    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'withoutCom'], 'integer'],
            [['role', 'permission'], 'string'],
            [['username', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $ids
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ids = null)
    {
        $query = $ids !== null ? User::find()->where(['id' => $ids]) : User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['id' => $this->getSearchAssign($this->role)])
            ->andFilterWhere(['id' => $this->getSearchAssign($this->permission)])
            ->andFilterWhere(['id' => $this->getWithoutCompanyUsr()]);

        return $dataProvider;
    }

    /**
     * Returns an array of roles ​​for dropdown list
     *
     * @return array
     */
    public function rolesSearch()
    {
        $manager = $this->manager;
        $roles = [];
        foreach (array_keys($manager->getRoles()) as $name) {
            $roles[$name] = $name;
        }
        return $roles;
    }

    /**
     * Returns an array of permissions ​​for dropdown list
     *
     * @return array
     */
    public function permissionsSearch()
    {
        $manager = $this->manager;
        $permissions = [];
        foreach (array_keys($manager->getPermissions()) as $name) {
            $permissions[$name] = $name;
        }
        return $permissions;
    }

    /**
     * Returns an array of users
     *
     * @param $roleName string Role or permission name
     * @return array Users
     */
    private function getSearchAssign($roleName)
    {
        $manager = $this->manager;
        return $manager->getUserIdsByRole($roleName);
    }

    /**
     * @return array
     */
    public static function status()
    {
        return [
            self::STATUS_DELETED => 'Disabled',
            self::STATUS_ACTIVE => 'All',
            self::STATUS_AGENCY => 'Agency',
            self::STATUS_AGENCY_BEFORE_APPLY => 'Apply agency',
            self::STATUS_HOTEL => 'Hotel',
            self::STATUS_HOTEL_BEFORE_APPLY => 'Apply hotel',
            self::STATUS_ADMIN => 'Admin',
        ];
    }

    /**
     * @return int[]
     */
    public function getWithoutCompanyUsr()
    {
        $query = new Query();
        if ($this->withoutCom == 1) {
            $id = $query->select('user.id')
                ->from('user')
                ->leftJoin('com_users', 'com_users.user_id = user.id')
                ->where('com_users.user_id is null')
                ->column();
        } else {
            $id = $query->select('id')->from('user')->column();
        }
        return $id;
    }

}
