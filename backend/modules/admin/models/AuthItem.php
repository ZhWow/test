<?php
//declare(strict_types=1);

namespace backend\modules\admin\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property integer $created_at
 * @property integer $updated_at
 * @property mixed $manager Auth Manager
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $children
 * @property array $items
 * @property string $typeName
 * @property AuthItem[] $parents
 *
 * @package backend\modules\admin\models
 * @author BADA
 */
class AuthItem extends ActiveRecord
{
    const TYPE_ROLE = 1;
    const TYPE_PERMISSION = 2;
    const NAME_ROLE = 'role';
    const NAME_PERMISSION = 'permission';
    const NAME_ROUTE = 'route';

    public $manager = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->manager = Yii::$app->getAuthManager();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('adm', 'Name'),
            'type' => Yii::t('adm', 'Type'),
            'description' => Yii::t('adm', 'Description'),
            'rule_name' => Yii::t('adm', 'Rule Name'),
            'data' => Yii::t('adm', 'Data'),
            'created_at' => Yii::t('adm', 'Created At'),
            'updated_at' => Yii::t('adm', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
    }

    /**
     * Return AuthItem type name
     * @return string
     */
    public function getTypeName()
    {
        $typeNames = self::getTypeNameArray();
        return isset($typeNames[$this->type]) ? $typeNames[$this->type] : '';
    }

    public static function getTypeNameArray()
    {
        return [
            1 => 'Role',
            2 => 'Permission',
        ];
    }

    /**
     * Return all AuthItem
     * @return array
     */
    public function getItems()
    {
        $manager = $this->manager;

        $available = [];

        foreach (array_keys($manager->getRoles()) as $name) {
            $available[$name] = self::NAME_ROLE;
            foreach ($this->parents as $parent) {
                if ($parent->name == $name) {
                    unset($available[$name]);
                }
            }
        }

        foreach (array_keys($manager->getPermissions()) as $name) {
            if ($name[0] != '/') {
                $available[$name] = self::NAME_PERMISSION;
                foreach ($this->parents as $parent) {
                    if ($parent->name == $name) {
                        unset($available[$name]);
                    }
                }
            } else {
                $available[$name] = self::NAME_ROUTE;
                foreach ($this->parents as $parent) {
                    if ($parent->name == $name) {
                        unset($available[$name]);
                    }
                }
            }
        }

        $assigned = [];
        if ($manager->getChildren($this->name)) {
            foreach ($manager->getChildren($this->name) as $item) {
                if (isset($available[$item->name])) {
                    $assigned[$item->name] = $available[$item->name];
                    unset($available[$item->name]);
                    if ($manager->getChildren($item->name)) {
                        foreach ($manager->getPermissionsByRole($item->name) as $childItem) {
                            if (! $this->childRole($item->name, $childItem->name, self::NAME_PERMISSION, $assigned, $available)) {
                                continue;
                            }
                        }
                        if ($item->type !== self::TYPE_PERMISSION) {
                            foreach ($manager->getChildRoles($item->name) as $childItem) {
                                if (! $this->childRole($item->name, $childItem->name, self::NAME_ROLE, $assigned, $available)) {
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($this->type === self::TYPE_PERMISSION) {
            foreach ($available as $key => $item) {
                if ($item == self::NAME_ROLE) {
                    unset($available[$key]);
                }
            }
        }

        if ($this->name[0] == '/') {
            foreach ($available as $key => $item) {
                unset($available[$key]);
            }
        }

        unset($available[$this->name]);
        unset($assigned[$this->name]);

        if (strtolower($this->name) !== 'guest') {
            $paramExclude = array_flip(Yii::$app->controller->module->params['paramExclude']);
            $availableExclude = array_diff_key($available, $paramExclude);
            $assignedExclude = array_diff_key($assigned, $paramExclude);
            return [
                'available' => $availableExclude,
                'assigned' => $assignedExclude
            ];
        }
        return [];
    }

    /**
     * @param string $parentName
     * @param string $childName
     * @param string $itemType
     * @param array $assigned
     * @param array $available
     *
     * @return bool
     */
    private function childRole($parentName, $childName, $itemType, array &$assigned, array &$available)
    {
        if ($parentName == $childName) {
            if (isset($available[$childName])) {
                $assigned[$childName] = $available[$childName];
                unset($available[$childName]);
            }
            return false;
        }
        if (isset($available[$childName])) {
            $available[$childName] = 'child' . ',' . $itemType . ',' . $parentName;
            $assigned[$childName] = $available[$childName];
            unset($available[$childName]);
        }
        return true;
    }

    /**
     * Saves child of the AuthItem
     * @param array $items
     * @param AuthItem $parentItem
     * @return int
     */
    public function assign(array $items, AuthItem $parentItem)
    {
        $manager = $this->manager;

        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ? : $manager->getPermission($name);
                $manager->addChild($parentItem, $item);
                $success++;
            } catch (Exception $e) {
                Yii::error($e->getMessage(), 'admin-panel');
            }
        }
        return $success;
    }

    /**
     * Remove child of the AuthItem
     * @param array $items
     * @param AuthItem $parentItem
     * @return int
     */
    public function revoke(array $items, AuthItem $parentItem)
    {
        $manager = $this->manager;

        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ?: $manager->getPermission($name);
                $manager->removeChild($parentItem, $item);
                $success++;
            } catch (Exception $e) {
                Yii::error($e->getMessage(), 'admin-panel');
            }
        }
        return $success;
    }

}
