<?php

namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\company\common\models\ComDataTemp;

/**
 * ComDataTempSearch represents the model behind the search form about `modules\company\common\models\ComDataTemp`.
 */
class ComDataTempSearch extends ComDataTemp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tmp_usr_id'], 'integer'],
            [['uniq', 'name_ru', 'name_kz', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ComDataTemp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tmp_usr_id' => $this->tmp_usr_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'uniq', $this->uniq])
            ->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_kz', $this->name_kz]);

        return $dataProvider;
    }
}
