<?php

namespace backend\modules\h_payment\models\base;

use Yii;

/**
 * This is the model class for table "h_payments_type".
 *
 * @property integer $id
 * @property string $hotel_id
 * @property integer $type_btm
 * @property integer $type_uc
 */
class HPaymentsType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_payments_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_btm', 'type_uc'], 'integer'],
            [['hotel_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Название отеля',
            'type_btm' => 'За счёт BTM',
            'type_uc' => 'Оплата на месте',
        ];
    }
}
