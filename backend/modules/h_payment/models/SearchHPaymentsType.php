<?php

namespace backend\modules\h_payment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\h_payment\models\HPaymentsType;

/**
 * SearchHPaymentsType represents the model behind the search form about `backend\modules\h_payment\models\HPaymentsType`.
 */
class SearchHPaymentsType extends HPaymentsType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type_btm', 'type_uc'], 'integer'],
            [['hotel_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HPaymentsType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_btm' => $this->type_btm,
            'type_uc' => $this->type_uc,
        ]);

        $query->andFilterWhere(['like', 'hotel_id', $this->hotel_id]);

        return $dataProvider;
    }
}
