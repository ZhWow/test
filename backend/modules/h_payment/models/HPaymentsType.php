<?php

namespace backend\modules\h_payment\models;

use backend\modules\integra\models\TUsers;
use Yii;

class HPaymentsType extends \backend\modules\h_payment\models\base\HPaymentsType
{

    public static function getHotelsList()
    {
        $result = [];
        $hotels = TUsers::find()->select(['hotelID', 'position'])->where(['role' => 4])->all();
        foreach ($hotels as $hotel) {
            /* @var $hotel TUsers */
            $result[$hotel->hotelID] = $hotel->position;
        }

        return $result;
    }
}
