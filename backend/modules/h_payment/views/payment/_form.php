<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\h_payment\models\HPaymentsType;

/* @var $this yii\web\View */
/* @var $model backend\modules\h_payment\models\HPaymentsType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hpayments-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hotel_id')->dropDownList(HPaymentsType::getHotelsList()) ?>

    <?= $form->field($model, 'type_btm')->checkbox() ?>

    <?= $form->field($model, 'type_uc')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
