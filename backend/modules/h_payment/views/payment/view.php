<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\h_payment\models\HPaymentsType */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hpayments Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hpayments-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'hotel_id',
            'type_btm',
            'type_uc',
        ],
    ]) ?>

</div>
