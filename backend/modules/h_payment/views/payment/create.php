<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\h_payment\models\HPaymentsType */

$this->title = 'Create Hpayments Type';
$this->params['breadcrumbs'][] = ['label' => 'Hpayments Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hpayments-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
