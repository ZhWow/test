<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\h_payment\models\SearchHPaymentsType */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hpayments Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hpayments-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить отель', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'hotel_id',
//                'label' => 'Компания',
                'format' => 'text',
                'value' => function($model) {
                    return \backend\modules\integra\models\TUsers::findOne(['hotelID'=> $model->hotel_id])->position;
                },
            ],
            [
                'attribute' => 'type_btm',
                'value'=> function($model){
                    return ($model->type_btm) ? 'Есть' : 'Нету';
                }
            ],
            [
                'attribute' => 'type_uc',
                'value'=> function($model){
                    return ($model->type_uc) ? 'Есть' : 'Нету';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
