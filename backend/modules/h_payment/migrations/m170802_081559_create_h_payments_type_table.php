<?php

namespace backend\modules\h_payment\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `h_payments_type`.
 */
class m170802_081559_create_h_payments_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('h_payments_type', [
            'id' => $this->primaryKey(),
            'hotel_id' => $this->string(),
            'type_btm' => $this->integer(1)->defaultValue(0),
            'type_uc' => $this->integer(1)->defaultValue(0),
        ]);


        /*$this->batchInsert('h_payments_type', ['type', 'display_name'], [
            ['card', 'Payment by card'],
            ['company', 'At the expense of the company'],
            ['in_place', 'Cash payment'],
        ]);*/

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('h_payments_type');
    }
}
