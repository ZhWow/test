<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 01.08.2017
 * Time: 12:52
 */

namespace backend\modules\h_payment;


class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\h_payment\controllers';

    public $defaultRoute = 'payment';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
//        $localConfig = '/config-local.php';
//        if (file_exists(__DIR__ . $localConfig)) {
//            \Yii::configure($this, ArrayHelper::merge(
//                require(__DIR__ . '/config.php'),
//                require(__DIR__ . $localConfig)
//            ));
//        } else {
//            \Yii::configure($this, require(__DIR__ . '/config.php'));
//        }
    }
}
