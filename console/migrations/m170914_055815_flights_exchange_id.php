<?php

use yii\db\Migration;

class m170914_055815_flights_exchange_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('flights', 'exchange_id', $this->integer());
    }

    public function safeDown()
    {
        echo "m170914_055815_flights_exchange_id cannot be reverted.\n";
        $this->dropColumn('flights', 'exchange_id');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_055815_flights_exchange_id cannot be reverted.\n";

        return false;
    }
    */
}
