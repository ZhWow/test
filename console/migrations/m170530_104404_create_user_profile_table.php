<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 */
class m170530_104404_create_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_profile', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'phone' => $this->string(),
            'birthday' => $this->string(),
            'gen' => $this->string(),
            'doc_type' => $this->string(),
            'doc_issue_country' => $this->string(),
            'doc_expire_date' => $this->string(),
            'doc_id' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_profile');
    }
}
