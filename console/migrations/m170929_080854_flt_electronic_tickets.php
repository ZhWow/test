<?php

use yii\db\Migration;

class m170929_080854_flt_electronic_tickets extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_electronic_tickets', [
            'id'                        => $this->primaryKey(11),
            'passenger_id'              => $this->integer(11),
            'user_id'                   => $this->integer(11),
            'ticket_number'             => 'CHAR(50)',
            'status'                    => 'CHAR(50)',
            'pnr'                       => 'CHAR(50)',
            'iata_id'                   => $this->integer(11),
            'ticketing_date'            => 'VARCHAR(255)',
            'issuing_airline'           => 'CHAR(50)',
            'booking_ref'               => 'CHAR(50)',
            'fare_calculation'          => 'VARCHAR(255)',
            'air_fare_currency'         => 'CHAR(50)',
            'equivalent_fare'           => $this->integer(11),
            'total_fare'                => $this->integer(11),
            'total_fare_currency'       => 'CHAR(50)',
            'total_tax_amount'          => $this->integer(11),
            'control_numbers'           => 'CHAR(50)',
        ]);
    }

    public function safeDown()
    {
        echo "m170929_080854_flt_electronic_tickets cannot be reverted.\n";
        $this->dropTable('flt_electronic_tickets');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_080854_flt_electronic_tickets cannot be reverted.\n";

        return false;
    }
    */
}
