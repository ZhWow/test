<?php

use yii\db\Migration;

class m170929_080915_flt_itineraries extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_itineraries', [
            'id'                            => $this->primaryKey(11),
            'ticket_id'                     => $this->integer(11),
            'from'                          => 'CHAR(50)',
            'from_terminal'                 => 'CHAR(50)',
            'to'                            => 'CHAR(50)',
            'to_terminal'                   => 'CHAR(50)',
            'carrier'                       => 'CHAR(50)',
            'flight_no'                     => 'CHAR(50)',
            'operating_airline_code'        => 'CHAR(50)',
            'marketing_airline_code'        => 'CHAR(50)',
            'class'                         => 'CHAR(50)',
            'departure_date'                => 'VARCHAR(50)',
            'arrival_date'                  => 'VARCHAR(50)',
            'fare_basis'                    => 'CHAR(50)',
            'baggage_weight'                => 'CHAR(50)',
            'baggage_weight_measure_unit'   => 'CHAR(50)',
            'status'                        => 'CHAR(50)',
        ]);
    }

    public function safeDown()
    {
        echo "m170929_080915_flt_itineraries cannot be reverted.\n";

        $this->dropTable('flt_itineraries');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_080915_flt_itineraries cannot be reverted.\n";

        return false;
    }
    */
}
