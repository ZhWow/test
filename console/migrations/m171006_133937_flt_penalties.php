<?php

use yii\db\Migration;

class m171006_133937_flt_penalties extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_penalties', [
            'id'            => $this->primaryKey(11),
            'ticket_id'     => $this->integer(11),
            'refund_bd'     => $this->integer(11),
            'refund_ad'     => $this->integer(11),
            'refund_ns'     => $this->integer(11),
            'exchange_bd'     => $this->integer(11),
            'exchange_ad'     => $this->integer(11),
            'exchange_ns'     => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('flt_penalties');

        echo "m171006_133937_flt_penalties cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_133937_flt_penalties cannot be reverted.\n";

        return false;
    }
    */
}
