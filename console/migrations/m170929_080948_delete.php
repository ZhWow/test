<?php

use yii\db\Migration;

class m170929_080948_delete extends Migration
{
    public function safeUp()
    {
        $this->dropTable('electronic_tickets');
    }

    public function safeDown()
    {
        $this->createTable('electronic_tickets', [
            'id'                            => $this->primaryKey(11),
            'ticket_number'                 => 'VARCHAR(255)',
            'user_id'                       => $this->integer(11),
            'created_at'                    => 'VARCHAR(255)',
            'flight_number'                 => 'VARCHAR(255)',
            'res_book_desig_code'           => 'VARCHAR(255)',
            'status'                        => 'TINYINT(4)',
            'equipment'                     => 'VARCHAR(255)',
            'iataid'                        => $this->integer(20),
            'operating_airline'             => 'VARCHAR(255)',
            'validating_airline'            => 'VARCHAR(255)',
            'marketing_airline'             => 'VARCHAR(255)',
            'departure_airport'             => 'VARCHAR(255)',
            'departure_terminal'            => 'VARCHAR(255)',
            'departure_datetime'            => 'VARCHAR(255)',
            'arrival_airport'               => 'VARCHAR(255)',
            'arrival_terminal'              => 'VARCHAR(255)',
            'arrival_datetime'              => 'VARCHAR(255)',
            'elapsed_time'                  => 'VARCHAR(255)',
            'baggage_weight'                => 'VARCHAR(255)',
            'baggage_quantity'              => 'VARCHAR(255)',
            'baggage_weight_measure_unit'   => 'VARCHAR(255)',
            'fare_basis'                    => 'VARCHAR(255)',
            'base_fare'                     => 'VARCHAR(255)',
            'fare_calculation'              => 'VARCHAR(255)',
            'tax'                           => 'VARCHAR(255)',
            'service_fee'                   => 'VARCHAR(255)',
            'total_fare'                    => 'VARCHAR(255)',
            'exchange'                      => 'SMALLINT(1)',
            'refunds'                       => 'SMALLINT(1)',
            'exchange_before_departure'     => $this->integer(11),
            'exchange_after_departure'      => $this->integer(11),
            'refunds_before_departure'      => $this->integer(11),
            'refunds_after_departure'       => $this->integer(11),
            'pnr_no'                        => 'VARCHAR(255)',
            'cabin_class'                   => 'VARCHAR(255)',
        ]);

        echo "m170929_080948_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_080948_delete cannot be reverted.\n";

        return false;
    }
    */
}
