<?php

use yii\db\Migration;

class m170626_054353_init_extensions_1 extends Migration
{
    public function up()
    {
        if (!file_exists('@Zelenin/yii/modules/I18n/migrations')) {
            echo 'php composer.phar require zelenin/yii2-i18n-module "dev-master"';
        }
        Yii::$app->runAction('migrate',['migrationPath' => '@Zelenin/yii/modules/I18n/migrations', 'interactive' => false]);
        Yii::$app->runAction('migrate/up',['migrationPath' => '@yii/rbac/migrations', 'interactive' => false]);
    }
}
