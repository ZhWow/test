<?php

use yii\db\Migration;

class m170922_160359_statuses extends Migration
{
    public function safeUp()
    {
        $this->createTable('statuses', [
            'id'                        => $this->primaryKey(11),
            'name'                      => $this->char(50),
            'description'               => 'VARCHAR(255)',
        ]);
    }

    public function safeDown()
    {
        echo "m170922_160359_statuses cannot be reverted.\n";

        $this->dropTable('statuses');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170922_160359_statuses cannot be reverted.\n";

        return false;
    }
    */
}
