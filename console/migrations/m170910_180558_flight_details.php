<?php

use yii\db\Migration;

class m170910_180558_flight_details extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_flight_details', [
            'id'            => $this->primaryKey(11),
            'flight_id'     => $this->integer(11),
            'direction_id'  => $this->integer(11),
            'elapsed_time'  => $this->char(50)
        ]);
    }

    public function safeDown()
    {
        echo "m170910_180558_flight_details cannot be reverted.\n";
        $this->dropTable('flt_flight_details');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170910_180558_flight_details cannot be reverted.\n";

        return false;
    }
    */
}
