<?php

use yii\db\Migration;

class m170910_180643_flight_price_info extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_price_info', [
            'id'                => $this->primaryKey(11),
            'flight_id'         => $this->integer(11),
            'base_fare_amount'  => $this->integer(11),
            'taxes_amount'      => $this->integer(11),
            'total_fare_amount' => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        echo "m170910_180643_flight_price_info cannot be reverted.\n";
        $this->dropTable('flt_price_info');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170910_180643_flight_price_info cannot be reverted.\n";

        return false;
    }
    */
}
