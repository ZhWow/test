<?php

use yii\db\Migration;

class m170921_042408_corp_module extends Migration
{
    public function safeUp()
    {
        $this->createTable('corp_modules', [
            'id'                        => $this->primaryKey(11),
            'name'                      => $this->char(50),
            'description'               => 'VARCHAR(255)',
        ]);
    }

    public function safeDown()
    {
        echo "m170921_042408_corp_module cannot be reverted.\n";
        $this->dropTable('corp_modules');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170921_042408_corp_module cannot be reverted.\n";

        return false;
    }
    */
}
