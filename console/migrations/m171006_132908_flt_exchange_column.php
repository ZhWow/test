<?php

use yii\db\Migration;

class m171006_132908_flt_exchange_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('flt_exchange', 'new_ticket_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('flt_exchange', 'new_ticket_id');
        echo "m171006_132908_flt_exchange_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_132908_flt_exchange_column cannot be reverted.\n";

        return false;
    }
    */
}
