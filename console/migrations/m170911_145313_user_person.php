<?php

use yii\db\Migration;

class m170911_145313_user_person extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_person', [
            'id'                        => $this->primaryKey(11),
            'user_id'                   => $this->integer(11),
            'person_id'                 => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        echo "m170911_145313_user_person cannot be reverted.\n";
        $this->dropTable('user_person');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170911_145313_user_person cannot be reverted.\n";

        return false;
    }
    */
}
