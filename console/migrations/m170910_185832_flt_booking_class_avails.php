<?php

use yii\db\Migration;

class m170910_185832_flt_booking_class_avails extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_booking_class_avails', [
            'id'                        => $this->primaryKey(11),
            'segment_id'                => $this->integer(11),
            'res_book_desig_code'       => $this->char(50),
            'res_book_desig_quantity'   => $this->integer(4),
            'rph'                       => $this->char(50),
            'available_ptc'             => $this->char(50),
            'res_book_desig_cabin_code' => $this->char(50),
            'fare_basis'                => $this->char(50),
        ]);
    }

    public function safeDown()
    {
        echo "m170910_185832_flt_booking_class_avails cannot be reverted.\n";
        $this->dropTable('flt_booking_class_avails');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170910_185832_flt_booking_class_avails cannot be reverted.\n";

        return false;
    }
    */
}
