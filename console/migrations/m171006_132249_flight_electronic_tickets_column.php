<?php

use yii\db\Migration;

class m171006_132249_flight_electronic_tickets_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('flt_electronic_tickets', 'penalty', $this->integer());
        $this->addColumn('flt_electronic_tickets', 'refund_amount', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('flt_electronic_tickets', 'penalty');
        $this->dropColumn('flt_electronic_tickets', 'refund_amount');

        echo "m171006_132249_flight_electronic_tickets_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_132249_flight_electronic_tickets_column cannot be reverted.\n";

        return false;
    }
    */
}
