<?php

use yii\db\Migration;

/**
 * Handles the creation of table `t_notification_company`.
 */
class m170801_112355_create_t_notification_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->db = 'db_manager';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('t_notification_company', [
            'id' => $this->primaryKey(),
            'corp_id' => $this->integer(),
            'email' => $this->string(),
            'onBook' => $this->integer(),
            'onApprove' => $this->integer(),
            'onApproveErr' => $this->integer(),
            'onAgentRefund' => $this->integer(),
            'onIssueTicket' => $this->integer(),
            'onRefund' => $this->integer(),
            'onCancel' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('t_notification_company');
    }
}
