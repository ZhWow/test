<?php

use yii\db\Migration;

class m170914_055839_flt_exchange extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_exchange', [
            'id'                        => $this->primaryKey(11),
            'flight_id'                   => $this->integer(11),
            'ticket_id'                 => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        echo "m170914_055839_flt_exchange cannot be reverted.\n";
        $this->dropTable('flt_exchange');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_055839_flt_exchange cannot be reverted.\n";

        return false;
    }
    */
}
