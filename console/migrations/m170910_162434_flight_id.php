<?php

use yii\db\Migration;

class m170910_162434_flight_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('flights_person','ticket_id',$this->integer(11 | null));
    }

    public function safeDown()
    {
        echo "m170910_162434_flight_id cannot be reverted.\n";
        $this->dropColumn('flights_person','ticket_id');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170910_162434_flight_id cannot be reverted.\n";

        return false;
    }
    */
}
