<?php

use yii\db\Migration;

class m170921_042427_corp_module_assignment extends Migration
{
    public function safeUp()
    {
        $this->createTable('corp_module_assignment', [
            'id'                        => $this->primaryKey(11),
            'module_id'                 => $this->integer(11),
            'corp_id'                   => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        echo "m170921_042427_corp_module_assignment cannot be reverted.\n";
        $this->dropTable('corp_module_assignment');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170921_042427_corp_module_assignment cannot be reverted.\n";

        return false;
    }
    */
}
