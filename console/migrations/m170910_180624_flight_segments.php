<?php

use yii\db\Migration;

class m170910_180624_flight_segments extends Migration
{
    public function safeUp()
    {
        $this->createTable('flt_segments', [
            'id'                    => $this->primaryKey(11),
            'flight_details_id'     => $this->integer(11),
            'departure_airport'     => $this->char(50),
            'arrival_airport'       => $this->char(50),
            'departure_date_time'   => $this->char(50),
            'arrival_date_time'     => $this->char(50),
            'flight_number'         => $this->integer(11),
            'operating_airline'     => $this->char(50),
            'marketing_airline'     => $this->char(50),
            'equipment'             => $this->char(50),
            'flight_duration'       => $this->char(50),
            'status'                => $this->char(50),
        ]);
    }

    public function safeDown()
    {
        echo "m170910_180624_flight_segments cannot be reverted.\n";
        $this->dropTable('flt_segments');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170910_180624_flight_segments cannot be reverted.\n";

        return false;
    }
    */
}
