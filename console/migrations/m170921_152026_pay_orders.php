<?php

use yii\db\Migration;

class m170921_152026_pay_orders extends Migration
{
    public function safeUp()
    {
        $this->createTable('pay_orders', [
            'id'                        => $this->primaryKey(11),
            'errorCode'                 => $this->integer(11),
            'params'                    => $this->string(255),
            'orderStatus'               => $this->integer(11),
            'orderNumber'               => 'VARCHAR (255)',
            'pan'                       => 'VARCHAR (255)',
            'expiration'                => 'VARCHAR (255)',
            'cardholderName'            => 'VARCHAR (255)',
            'amount'                    => $this->integer(11),
            'currency'                  => 'VARCHAR (255)',
            'approvalCode'              => 'VARCHAR (255)',
            'authCode'                  => $this->integer(11),
            'ip'                        => 'VARCHAR (255)',
            'date'                      => 'VARCHAR (255)',
            'orderDescription'          => 'VARCHAR (255)',
            'actionCodeDescription'     => 'VARCHAR (255)',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('pay_orders');
        echo "m170921_152026_pay_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170921_152026_pay_orders cannot be reverted.\n";

        return false;
    }
    */
}
