<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `index_username_to_user`.
 */
class m170526_192617_drop_index_username_to_user_table extends Migration
{
    public function up()
    {
        // remove the unique index
        $this->dropIndex('username', 'user');
    }

    public function down()
    {
        // add the unique index again
        $this->createIndex('username', 'user', 'username', $unique = true );
    }
}
