<?php
/**
 * User: Leonic
 * Date: 28.11.2017
 * Time: 17:26
 */

namespace console\models;

use backend\models\Corp;
use console\models\base\TBook;
use modules\robot\frontend\providers\epower\methods\GetPNR;
use modules\robot\frontend\providers\epower\methods\SignOut;
use modules\robot\frontend\providers\epower\models\BTMRequest;
use modules\robot\frontend\providers\epower\models\EPSession;
use modules\robot\frontend\providers\epower\services\EPCredentials;

class FlightBookStatus
{
    public static function runRobot()
    {
        $corps = Corp::find()->all();

        if ($corps) {
            foreach ($corps as $corp) {
                TBook::setDb($corp->base);
                $books = TBook::find()->where(['to_check' => 0])->all();
                foreach ($books as $book) {
                    $date = strtotime(explode(';', $book->depdate)[0]);
                    if ($date >= strtotime(date('d.m.Y')) && $book->AirItineratyHash) {
                        $post = [
                            'request'   => 'GetPNR',
                            'json'      => urlencode(json_encode(['PNR' => $book->PNR, 'lastname' => explode(';', $book->Surnames)[0]])),
                            'user_id'   => md5('flightStatusRobot228'),
                            'pult_name' => $book->pultname
                        ];

                        $BTMRequest = new BTMRequest();
                        $BTMRequest->setAttributes($post);

                        $EPSession = EPSession::checkSessionByUID($BTMRequest, $book->pultversion);

                        if ( $BTMRequest->validate() ) {
                            $EPCredentials = new EPCredentials($BTMRequest->pult_name, $BTMRequest->user_id, $EPSession, $book->pultversion);

                            $GetPNR = new GetPNR($EPCredentials);
                            $GetPNRResponse = $GetPNR->pnrCron($BTMRequest);
                            $SignOut = new SignOut($EPCredentials, $GetPNR->SessionID);
                            if ($GetPNRResponse) {
                                if (sha1($GetPNRResponse) != $book->AirItineratyHash) {
                                    $book->AirItineratyHash = sha1($GetPNRResponse);
                                    $book->to_check = 1;
                                    $book->save(false);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}