<?php
/**
 * User: Leonic
 * Date: 28.11.2017
 * Time: 17:27
 */

namespace console\models\base;

use modules\organisation\models\flights\TBook as BaseTBook;
use modules\organisation\models\TUsers;

class TBook extends BaseTBook
{
    public function getUsers()
    {
        return $this->hasOne(TUsers::relation(static::$db), ['ID' => 'id_user']);
    }
}