<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'modules' => [
        'flight' => [
            'class' => 'modules\flight\frontend\Module',
        ],
        'robot' => [
            'class' => 'modules\robot\frontend\Module',
        ],
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'modules\flight\common\migrations',
                'modules\countries\common\migrations',
                'backend\modules\integra\migrations',
                'backend\modules\hotels\migrations',
                'backend\modules\service\migrations',
                'backend\modules\h_payment\migrations',
				'backend\modules\api\migrations',
                'backend\modules\h_penalty\migrations',
                'backend\modules\notifications\migrations',
            ],
        ],
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['cron'],
                    'logFile' => '@runtime/logs/cron.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['cron_error'],
                    'logFile' => '@runtime/logs/cron_error.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['test'],
                    'logFile' => '@runtime/logs/test.log',
                    'logVars' => [],
                ],
            ],
        ],
    ],
    'params' => $params,
];
