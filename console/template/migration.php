<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use yii\db\Migration;

class <?= $className ?> extends Migration
{
    public function up()
    {
        $this->createTable('table_name_1', [
            'id' => $this->primaryKey(),
            // 'string' => $this->string(),
            // 'string' => $this->string()->notNull(),
            // 'string' => $this->string(12)->notNull()->unique(),
            // 'integer' => $this->integer()->after('id'),
            // 'default' => $this->smallInteger()->notNull()->defaultValue(0),
            // 'small' => $this->smallInteger(1),
            // 'big' => $this->bigInteger(),
            // 'float' => $this->float(),
            // 'double' => $this->double(),
            // 'decimal' => $this->decimal(),
            // 'timestamp' => $this->timestamp(),
            // 'dateTime' => $this->dateTime(),
            // 'time' => $this->time(),
            // 'date' => $this->date(),
            // 'text' => $this->text(),
            // 'boolean' => $this->boolean(),
            // 'char' => $this->char(),
            // 'binary' => $this->binary(),
            // 'test' => $this->integer(),
            // 'author' => $this->integer()
        ]);

        // creates index for column `test`
        // $this->createIndex('idx-test', 'table_name_1', 'test');

        // add foreign key for table `user`
        // $this->addForeignKey('fk-table_name_1-author_id', 'table_name_1', 'author_id', 'user', 'id', 'CASCADE');

        // $this->renameTable('table', 'new_name_table');
        // $this->addColumn('table_name_2', 'column', $this->integer());
        // $this->alterColumn('table_name_3', 'column', $this->string(10000));
        // $this->renameColumn('name_table', 'column', 'new_name_column');

        // очистка таблицы
        // $this->truncateTable('table_name');


        /*$this->createTable('news', [
            'id' => Schema::TYPE_PK,
            // 'string' => Schema::TYPE_STRING . ' NOT NULL',
            // 'text' => Schema::TYPE_TEXT,
            // 'int' => Schema::TYPE_INTEGER,
            // 'small' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            // 'big' => Schema::TYPE_BIGINT,
            // 'float' => Schema::TYPE_FLOAT,
            // 'double' => Schema::TYPE_DOUBLE,
            // 'decimal' => Schema::TYPE_DECIMAL,
            // 'datetime' => Schema::TYPE_DATETIME,
            // 'timestamp' => Schema::TYPE_TIMESTAMP,
            // 'time' => Schema::TYPE_TIME,
            // 'date' => Schema::TYPE_DATE,
            // 'binary' => Schema::TYPE_BINARY,
            // 'bool' => Schema::TYPE_BOOLEAN,
        ]);*/


/*
 * INSERT TABLE
 *
$sql = <<<SQL
INSERT INTO `table_name_4` (`id`, `name_ru`, `name_kz`, `disabled`) VALUES (2,'EEP', NULL, NULL);
INSERT INTO `table_name_5` (`id`, `table_name`, `model_name`, `disabled`, `created_at`, `updated_at`, `updated_by`) VALUES (1, 'ref_receivers_sources', 'backend\\modules\\refs\\models\\RefReceiversSources', 0, NULL, NULL, NULL);
SQL;
$this->execute($sql);
return true;
*/

/*
 * INSERT TABLE
 *
$this->insert('test_user', [
    'email' => 'test1@notanaddress.com',
    'username' => 'User One',
    'password' => md5('test1'),
]);
*/

    }

    public function down()
    {
        // $this->dropForeignKey('fk-post-author_id', 'post');

        // $this->dropIndex('idx-test', 'table_name_1');

        $this->dropTable('table_name_1');

        // $this->dropColumn('table_name_2', 'type_id');

        // $this->alterColumn('table_name_3', 'body', $this->string(5000));

        // $this->dropTable('news');

        // $this->dropPrimaryKey('name', 'table');

    }


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        /*$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sys_lang}}', [
            'id' => $this->primaryKey(),
        ], $tableOptions);

        $this->batchInsert('sys_lang', ['url', 'local', 'name', 'default', 'date_update', 'date_create'], [
            ['kz', 'kz-KZ', 'Қазақша', 0, time(), time()],
            ['ru', 'ru-RU', 'Русский', 1, time(), time()],
            ['en', 'en-EN', 'English', 0, time(), time()],
            ['tt', 'tt-RU', 'Test', 0, time(), time()],
        ]);*/
    }

    public function safeDown()
    {
        // $this->dropTable('{{%sys_lang}}');
    }
}
