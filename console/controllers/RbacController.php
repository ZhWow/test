<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $returnAndExchange = $auth->createPermission('ReturnAndExchange');
        $returnAndExchange->description = 'Return and exchange ticket';
        $auth->add($returnAndExchange);

        $agent = $auth->createRole('agent');
        $auth->add($agent);
        $auth->addChild($agent, $returnAndExchange);

        $their = $auth->createRole('their');
        $auth->add($their);
        $auth->addChild($agent, $their);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $agent);

        $auth->assign($agent, 1);
        $auth->assign($admin, 1);
        $auth->assign($their, 1);
    }

    public function actionAdd()
    {

    }
}