<?php

namespace console\controllers;

use backend\modules\notifications\models\base\TCorp;
use backend\modules\notifications\models\Send;
use Yii;
use yii\console\Controller;
use console\models\FlightBookStatus;
use modules\flight\frontend\models\Tokens;
use modules\flight\frontend\providers\amadeus\Amadeus;
use backend\modules\integra\services\IntegraService;
use modules\robot\frontend\providers\epower\methods\SignOut;
use modules\robot\frontend\providers\epower\models\EPSession;
use modules\robot\frontend\providers\epower\services\EPCredentials;

/**
 * Class CronController
 *
 * Controller for cron tab
 *
 * @package console\controllers
 * @author Bablanov Dauren <bablanov.dauren@yandex.kz>
 */
class CronController extends Controller
{
    /**
     * Sign Out Sessions That has been timed out 15 minutes
     */
    public function actionEpowerSessions()
    {
        $EPSessions = EPSession::find()->all();
        foreach ($EPSessions as $EPSession) {
            $session_time = strtotime($EPSession->session_time);
            $current_time = time();
            $time_difference = $current_time - $session_time;

            if( $time_difference >= 900 ) {
                $EPCredentials = new EPCredentials($EPSession->pult_name, null,null,$EPSession->pult_type);
                $SignOut = new SignOut($EPCredentials, $EPSession->session_id);
                echo 'Signed out ' . $EPSession->pult_name . " / " . $EPSession->session_id . PHP_EOL;
            }
        }
    }

    /**
     * Deleted old token Amadeus
     */
    public function actionClearOldAmadeusToken()
    {
        $tokens = Tokens::find()->all();
        $i = 0;
        foreach ($tokens as $token) {
            /* @var $token Tokens */
            if (Amadeus::isActualToken($token->created_at)) {
                continue;
            } else {
                $token->delete();
                $i++;
            }
        }

        if ($i > 0) {
            Yii::info('Cron-ом было удалено ' . $i . ' token-ов', 'cron');
        }
    }

    /**
     * Loads xml tickets files, ftp client. And writes to the database.
     * If the record was successful, the file is deleted, if not, the file is not deleted
     */
    public function actionDownloadIntegra()
    {
        IntegraService::downloadXmlFiles();
		IntegraService::checkTickets();
    }

    /**
     *
     */
    public function actionCheckTickets()
    {
        IntegraService::checkTickets();
    }

    public function actionQwaySession()
    {
        $tokens = Tokens::findAll(['pult_name' => 'qway']);
        $count = 0;

        if ($tokens) {
            foreach ($tokens as $token) {
                Tokens::isSessionAlive($token);
                $count++;
            }
        }

        return $count;
    }

    public function actionBookUpdate()
    {
        FlightBookStatus::runRobot();
    }

    public function actionHotelCheckout()
    {
        $corps = TCorp::find()->all();
        foreach ($corps as $corp) {
            $post = [
                'trigger'   => 'cron_checkout',
                'corp_id'   => $corp->id,
                'condition' => [
                    'hotel'    => ['1'],
                    'provider'  => 'hotel'
                ]
            ];

            if ($post) {
                $send = new Send($post);
            }
        }
    }
}
