<?php
return [
    ''=>'site/index',
    [
        'pattern' => 'lk',
        'route' => 'user/index',
        'suffix'=>'.html'
    ],

    [
        'pattern' => 'lk/<action:(?(?=index)|.*)>',
        'route' => 'user/<action>',
        'suffix'=>'.html',
    ],
    [
        'pattern' => '<action:(?(?=(index|site\/index))|(?(?!.*[\/].*).*))>',
//        'pattern' => '<action:(?(?=index)|(.*))>',
//        'pattern' => '^(?P<action>(?(?=index)|.*\/(.*)))$',
        'route' => 'site/<action>',
        'suffix'=>'.html',
    ],
    [
        'pattern' => 'api/<action>',
        'route' => 'api/<action>',
        'suffix'=>'.html',
    ],

//    [
//        'pattern' => 'lk',
//        'route' => 'user/index',
//        'suffix'=>'.html'
//    ],

//    '<controller>/<action>.html'=>'<controller>/<action>'
];