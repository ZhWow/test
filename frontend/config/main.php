<?php

Yii::$container->set('modules\flight\frontend\providers\base\FlightsProvider', function () {
    return new \modules\flight\frontend\providers\amadeus\Amadeus();
});

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'flight' => [
            'class' => 'modules\flight\frontend\Module',
        ],
        'profile' => [
            'class' => 'modules\users\frontend\Module',
        ],
    ],
    'components' => [
        /*'amadeus' => [
            'class' => 'modules\flight\services\Amadeus',
        ],*/
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['countries'],
                    'logFile' => '@runtime/logs/countries.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['flight'],
                    'logFile' => '@runtime/logs/flight.log',
                    'logVars' => [],
                ],
            ],

        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                /*'<action:\w+>' => '/site/<action>',
                '/' => '/site/index',*/
                /*'flight/get-city' => 'flight/flight/get-city',
                'flight/search-tickets' => 'flight/flight/search-tickets',
                'flight/book' => 'flight/flight/book',
                'flight/payment' => 'flight/flight/payment',*/
            ],
        ],

    ],
    'params' => $params,
];
