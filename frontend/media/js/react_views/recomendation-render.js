$(document).ready(function () {
    let $body = $('body'),
        $loaderSearch = $('#loader-flight-search'),
        $searchResultEl = $('#search-result'),
        $flightSubmit = $('button#flight-submit'),
        $bookForm = $('#book-form'),
        $filter = $('.recomendation_filter');

    $body.on('beforeSubmit', '#search-flight-form', function () {
        let $form = $(this).serialize();

        localStorage.setItem('searchFlighForm', $form);

        $.ajax({
            url: controllerUrl + 'search-tickets',
            method: 'post',
            dataType: 'json',
            data: $form,
            beforeSend: function () {
                $flightSubmit.prop('disabled', true);
                $loaderSearch.show();
                $searchResultEl.html('');
                $bookForm.html('');
                $('#selected-item').hide();
                $('#back-result').hide();
                $filter.hide();
            },
            success: function (response) {
                $flightSubmit.prop('disabled', false);
                $loaderSearch.hide();
                $body.addClass('mini');
                $searchResultEl.show();
                // renderRecomendation('search-result', response);
                renderItineraries('search-result', response);
                $bookForm.html(response.bookFormData);
                $filter.html(response.filter);
                $filter.show();
            },
            error: function() {
                $flightSubmit.prop('disabled', false);
                $loaderSearch.hide();
                $body.removeClass('mini');
            }
        }).then(function () {
            toolTipInit('.tooltipt');
        });

        return false;
    });

    $body.on('click', '.back-close', function () {
        $('#book-form').hide();
        $('.passengers-block-form').hide();
        $('#search-result').show();
        $('#selected-item').empty();
        $('#back-result').empty();
        $('.approve-recomendation').show();
        $('.selec-button').show();
        $('.recomendation-items').show();
        $('.recomendation-sorting').show();
        $('.recomendation_filter').show();
    });

    $body.on('click', '.by-time', function () {
        $('.butt-sort').removeClass('active');
        $(this).addClass('active');
        $(this).toggleClass('rotate');
        $(this).toggleClass('asc');
        $('.recomendation-items').hide();

        let type = $(this).hasClass('asc'),
            container = (!$(this).hasClass('back')) ? '.recomendation-sorting' : '.recomendation-sorting-back',
            parent = (!$(this).hasClass('back')) ? '#search-result' : '#back-result';

        if (type) {
            $(parent).find('.recomendation-item').sort(function(a, b) {
                return $(a).attr('data-timestamp') - $(b).attr('data-timestamp');
            }).appendTo(container);
        } else {
            $(parent).find('.recomendation-item').sort(function(a, b) {
                return $(b).attr('data-timestamp') - $(a).attr('data-timestamp');
            }).appendTo(container);
        }
    });

    $body.on('click', '.by-price', function () {
        $('.butt-sort').removeClass('active');
        $(this).addClass('active');
        $(this).toggleClass('rotate');
        $(this).toggleClass('asc');
        $('.recomendation-items').hide();

        let type = $(this).hasClass('asc'),
            container = (!$(this).hasClass('back')) ? '.recomendation-sorting' : '.recomendation-sorting-back',
            parent = (!$(this).hasClass('back')) ? '#search-result' : '#back-result';

        if (type) {
            $(parent).find('.recomendation-item').sort(function(a, b) {
                return $(a).attr('data-total') - $(b).attr('data-total');
            }).appendTo(container);
        } else {
            $(parent).find('.recomendation-item').sort(function(a, b) {
                return $(b).attr('data-total') - $(a).attr('data-total');
            }).appendTo(container);
        }
    });

    $body.on('change', '.filter-select', function() {
        let airlineValue = $('#airline').val(),
            rateValue = $('#rate').val(),
            routeValue = $('#route').val(),
            type = $(this).attr('id');

        $('#search-result').find('.recomendation-item').each(function () {
            if (type === 'airline') {
                if (airlineValue === 'all') {
                    $(this).css('display', 'flex');
                } else {
                    if ($(this).attr('data-airline') !== airlineValue) {
                        $(this).hide();
                    } else {
                        $(this).css('display', 'flex');
                    }
                }
            }

            if (type === 'rate') {
                if (rateValue === 'all') {
                    $(this).css('display', 'flex');
                } else {
                    if ($(this).attr('data-refund') !== rateValue) {
                        $(this).hide();
                    } else {
                        $(this).css('display', 'flex');
                    }
                }
            }

            if (type === 'route') {
                if (routeValue === 'all') {
                    $(this).css('display', 'flex');
                } else {
                    if ($(this).attr('data-route') !== routeValue) {
                        $(this).hide();
                    } else {
                        $(this).css('display', 'flex');
                    }
                }
            }
        });
    });
});

function helpBookForm(properties) {
    let $getFlightRules = $('#get-flight-rules'),
        $searchResult = $('#search-result'),
        $backResult = $('#back-result'),
        $bookForm = $('#book-form'),
        $selectedItem = $('#selected-item');

    $searchResult.hide();
    $backResult.hide();
    $bookForm.show();
    $selectedItem.empty();

    $getFlightRules.attr('data-combinationid', properties.combinationId);
    $getFlightRules.attr('data-sequenceid', properties.sequenceNumber);
    $('.combination-id').val(properties.combinationId);
    $('.sequence-id').val(properties.sequenceNumber);

    $('#book_total').text(number_format(properties.priceInfo.totalFare, 0, ' ', ' '));

    $('html,body').animate({
        scrollTop: $bookForm.offset().top - 20
    }, 700);
}

function bookForm(properties, searchData, container) {
    let $getFlightRules = $('#get-flight-rules'),
        $searchResult = $('#search-result'),
        $bookForm = $('#book-form'),
        $booSegments = $('#book_segments');

    if (searchData.back === false) {
        if (searchData.exchange === false) {
            $searchResult.hide();

            $getFlightRules.attr('data-combinationid', properties.combinationId.com);
            $getFlightRules.attr('data-sequenceid', properties.combinationId.seq);
            $('#book_total').text(number_format(properties.priceInfo.totalFare, 0, ' ', ' '));

            $('.combination-id').val(properties.combinationId.com);
            $('.sequence-id').val(properties.combinationId.seq);

            $booSegments.empty();

            renderBookForm(properties.dataItem, searchData, properties.priceInfo, container);
            $bookForm.show();
            toolTipInit('.tooltipp');

            $('.selec-button').show();
            $('.passengers-block-form').hide();
        } else {
            renderBookForm(properties, searchData, properties.priceInfo, container);
        }
    } else {
        renderBookForm(properties.dataItem, searchData, properties.priceInfo, container);
    }

    $('html,body').animate({
        scrollTop: $bookForm.offset().top - 20
    }, 700);
}

function ticketToExchange(data, priceInfo) {
    let $loader = $('#overlay-loader');

    $loader.show();

    $.ajax({
        url: controllerUrl + 'exchange',
        type: 'POST',
        dataType: 'json',
        data: {
            data: data,
            price: priceInfo
        },
        success: function (response) {
            $loader.hide();
            swal({
                title: 'Внимание!',
                text: response.message,
                type: response.status
            }).then(function () {
                if (response.status === 'success') {
                    location.reload();
                }
            });
        }
    });
}

function renderBookForm (data, searchData, priceInfo, container) {
    let searchdata = searchData;

    let RecomendationItemSelected = React.createClass({
            render: function () {
                if (searchdata.back === undefined || searchdata.back === false) {
                    $('.recomendation_filter').hide();
                    return (
                        <div className="item not">
                            <TopBlock searchdata={searchdata} />
                            <div className="bottom-block departure">
                                <RecomendationLeft dataItem={data} searchData={searchdata}/>
                                <div className="center">
                                    <div className="l-border" />
                                </div>
                                <RecomendationRight priceInfo={priceInfo} />
                            </div>
                        </div>
                    );
                } else {
                    return (
                        <div>
                            {
                                Object.keys(data.dataItem).map((itemKey, i) => {
                                    let itemData = data.dataItem[itemKey];
                                    return <div className="item" key={i}>
                                                <TopBlock searchdata={searchdata} />
                                                <div className="bottom-block departure">
                                                    <RecomendationLeft dataItem={itemData} searchData={searchdata}/>
                                                    <div className="center">
                                                        <div className="l-border" />
                                                    </div>
                                                    <RecomendationRight priceInfo={itemData.priceInfo} />
                                                </div>
                                            </div>
                                })
                            }
                        </div>
                    );
                }
            }
        }),
        Transfer = React.createClass({
            render: function () {
                const lnClass = 'transfer-line count-' + this.props.transferCount;
                if (!this.props.transfer) {
                    return (
                        <div className="transfer-line count-0">
                            <div className="ln" />
                            <div className="ln" />
                        </div>
                    );
                } else {
                    return (
                        <div className={lnClass}>
                            <div className="ln"/>
                            {
                                Object.keys(this.props.transfer).map((key) => {
                                    return <Tooltip transferdata={this.props.transfer[key]} key={key}/>;
                                })
                            }
                            <div className="ln"/>
                        </div>
                    );
                }
            }
        }),
        Tooltip = React.createClass({
            render: function () {
                return (
                    <div className="ln tooltipp"
                         data-transfer={this.props.transferdata.key}
                         data-city={this.props.transferdata.departureAirport}
                         data-deptime={this.props.transferdata.departureTime}
                         data-depdate={this.props.transferdata.departureDate}
                         data-arrtime={this.props.transferdata.arrivalTime}
                         data-arrdate={this.props.transferdata.arrivalDate}
                         key={this.props.transferdata.key}/>
                );
            }
        }),
        RecomendationLeft = React.createClass({
            render: function () {
                let dataItem = this.props.dataItem,
                    dateOptons = {
                        hour12: false,
                        weekday: 'short',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                    },
                    departureDate = new Date(dataItem.departureDateTime * 1000),
                    arrivalDate = new Date(dataItem.arrivalDateTime * 1000),
                    departureHours = (departureDate.getHours() > 9) ? departureDate.getHours() : '0'+departureDate.getHours(),
                    departureMinutes = (departureDate.getMinutes() > 9) ? departureDate.getMinutes() : '0'+departureDate.getMinutes(),
                    arrivalHours = (arrivalDate.getHours() > 9) ? arrivalDate.getHours() : '0'+arrivalDate.getHours(),
                    arrivalMinutes = (arrivalDate.getMinutes() > 9) ? arrivalDate.getMinutes() : '0'+arrivalDate.getMinutes();

                return (
                    <div className="left">
                        <div className="two-bl">
                            <div className="lt">
                                <div className="car-name">Перевозчик:</div>
                                <div className="logo">
                                    <object type="image/svg+xml" data={dataItem.img}></object>
                                </div>
                                <div className="air-company-name">
                                    {dataItem.marketingAirlineName}
                                </div>
                            </div>
                            <div className="rt">
                                <div className="top">
                                    <div className="bl">
                                        Рейс:
                                        <span>{dataItem.marketingAirlineCode}({dataItem.flightNumber}) — {searchData.cabin}</span>
                                    </div>
                                    <div className="bl">Общее время в пути – <span>{dataItem.elapsedTime.substr(0, 2)}
                                        ч {dataItem.elapsedTime.substr(2)} мин</span></div>
                                </div>
                                <div className="bottom">
                                    <div className="routes">
                                        <div className="left-route">
                                            <div className="bl">
                                                <span>Откуда:</span>
                                                <span>{dataItem.departureAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{dataItem.departureAirportName}</span>
                                            </div>
                                        </div>
                                        <div className="right-route">
                                            <div className="bl">
                                                <span>Куда:</span>
                                                <span>{dataItem.arrivalAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{dataItem.arrivalAirportName}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="time-and-transfer">
                                        <div className="bl">
                                            <div className="time">{departureHours}:{departureMinutes}</div>
                                            <div className="date">{departureDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                        <div className="bl">
                                            <Transfer transfer={dataItem.transferData.transfer}
                                                      transferCount={parseInt(dataItem.transferCount) + 2}/>
                                            <div className="transer">
                                                <span>{parseInt(dataItem.transferCount)}</span> {units(parseInt(dataItem.transferCount), {
                                                nom: 'пересадка',
                                                gen: 'пересадки',
                                                plu: 'пересадок'
                                            })}
                                            </div>
                                        </div>
                                        <div className="bl">
                                            <div className="time">{arrivalHours}:{arrivalMinutes}</div>
                                            <div className="date">{arrivalDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }),
        RecomendationRight = React.createClass({
            render: function () {
                return (
                    <div className="right">
                        <div className="l-content">
                            <div className="close">
                                <span className="back-close" />
                            </div>

                        </div>
                    </div>
                );
            }
        }),
        TopBlock = React.createClass({
            render: function () {
                let search = (searchdata.searchData === undefined) ? searchdata : searchdata.searchData;
                return (
                    <div className="top-block">
                        <div className="hd">
                            Выбран рейс → {search.departureDateSrt}
                        </div>
                        <div className="bt">
                            <div className="vertical-bl">{search.departureCityName}</div>
                            <div className="vertical-bl center">
                                <img src="/images/big-arrow.png" />
                            </div>
                            <div className="vertical-bl">{search.arrivalCityName}</div>
                        </div>
                    </div>
                );
            }
        });

    ReactDOM.render(
        <div className="selected-departure">
            <RecomendationItemSelected />
        </div>,
        document.getElementById(container)
    );

    if (searchdata.exchange === true) {
        if (searchdata.back !== false) {
            if (data.dataItem !== undefined) {
                ticketToExchange(data, priceInfo);
            }
        } else {
            ticketToExchange(data, priceInfo);
        }
    }
}

function renderBack(properties, searchDatas) {

    $('#selected-item').show();

    let countKey = -1,
        itemArray = {
            dataItem: [properties.dataItem],
            dataItems: getBackData(properties.combinationId, searchDatas.flightDataBack),
            back: true
        },
        searchData = searchDatas.searchData,
        Variant = React.createClass({
            render: function () {
                return (
                    <div className="arrival-head">
                        <div className="dep-icon" />
                        <div className="center">Выберите рейс обратно</div>
                        <div className="variants"><span>{itemArray.dataItems.length}</span> вариантов</div>
                    </div>
                );
            }
        }),
        Sort = React.createClass({
            render: function () {
                return (
                    <div className="rec-sort">
                        <div className="bl">Интересные варианты Обратно</div>
                        <div className="bl">
                            <div className="top">Летим Обратно → {this.props.arrivalDateSrt}</div>
                            <div className="bottom">
                                <div className="dep">{this.props.arrivalDateSrt}</div>
                                <div className="center">
                                    <img src="/images/head-arrow-icon.png"/>
                                </div>
                                <div className="arr">{this.props.arrivalCityName}</div>
                            </div>
                        </div>
                        <div className="bl">
                            <span className="by-price back butt-sort">Цена</span>
                            <span className="by-time back butt-sort">Время</span>
                        </div>
                    </div>
                );
            }
        }),
        RenderItem = React.createClass({
            render: function () {
                return (
                    <div className="recomendation-items">
                        <RecomendationPricedItinerary item={this.props.sequence}
                                                      searchData={this.props.searchData} />
                    </div>
                );
            }
        }),
        Transfer = React.createClass({
            render: function () {
                const lnClass = 'transfer-line count-' + this.props.transferCount;
                if (!this.props.transfer) {
                    return (
                        <div className="transfer-line count-0">
                            <div className="ln" />
                            <div className="ln" />
                        </div>
                    );
                } else {
                    return (
                        <div className={lnClass}>
                            <div className="ln"/>
                            {
                                Object.keys(this.props.transfer).map((key) => {
                                    return <Tooltip transferdata={this.props.transfer[key]} />;
                                })
                            }
                            <div className="ln"/>
                        </div>
                    );
                }
            }
        }),
        Tooltip = React.createClass({
            render: function () {
                return (
                    <div className="ln tooltipt"
                         data-transfer={this.props.transferdata.key}
                         data-city={this.props.transferdata.departureAirport}
                         data-deptime={this.props.transferdata.departureTime}
                         data-depdate={this.props.transferdata.departureDate}
                         data-arrtime={this.props.transferdata.arrivalTime}
                         data-arrdate={this.props.transferdata.arrivalDate}
                         key={this.props.transferdata.key}/>
                );
            }
        }),
        RecomendationLeft = React.createClass({
            render: function () {
                let dateOptons = {
                        hour12: false,
                        weekday: 'short',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                    },
                    departureDate = new Date(this.props.dataItem.departureDateTime * 1000),
                    arrivalDate = new Date(this.props.dataItem.arrivalDateTime * 1000),
                    departureHours = (departureDate.getHours() > 9) ? departureDate.getHours() : '0'+departureDate.getHours(),
                    departureMinutes = (departureDate.getMinutes() > 9) ? departureDate.getMinutes() : '0'+departureDate.getMinutes(),
                    arrivalHours = (arrivalDate.getHours() > 9) ? arrivalDate.getHours() : '0'+arrivalDate.getHours(),
                    arrivalMinutes = (arrivalDate.getMinutes() > 9) ? arrivalDate.getMinutes() : '0'+arrivalDate.getMinutes();
                return (
                    <div className="left">
                        <div className="two-bl red">
                            <div className="lt">
                                <div className="car-name">Перевозчик:</div>
                                <div className="logo">
                                    <object type="image/svg+xml" data={this.props.dataItem.img}></object>
                                </div>
                                <div className="air-company-name">
                                    {this.props.dataItem.marketingAirlineName}
                                </div>
                            </div>
                            <div className="rt">
                                <div className="top">
                                    <div className="bl">
                                        Рейс:
                                        <span>{this.props.dataItem.marketingAirlineCode}({this.props.dataItem.flightNumber}) — {searchData.cabin}</span>
                                    </div>
                                    <div className="bl">Общее время в пути – <span>{this.props.dataItem.elapsedTime.substr(0, 2)}
                                        ч {this.props.dataItem.elapsedTime.substr(2)} мин</span>
                                    </div>
                                </div>
                                <div className="bottom">
                                    <div className="routes">
                                        <div className="left-route">
                                            <div className="bl">
                                                <span>Откуда:</span>
                                                <span>{this.props.dataItem.departureAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{this.props.dataItem.departureAirportName}</span>
                                            </div>
                                        </div>
                                        <div className="right-route">
                                            <div className="bl">
                                                <span>Куда:</span>
                                                <span>{this.props.dataItem.arrivalAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{this.props.dataItem.arrivalAirportName}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="time-and-transfer">
                                        <div className="bl">
                                            <div className="time">{departureHours}:{departureMinutes}</div>
                                            <div className="date">{departureDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                        <div className="bl">
                                            <Transfer transfer={this.props.dataItem.transferData.transfer}
                                                      transferCount={parseInt(this.props.dataItem.transferCount) + 2}/>
                                            <div className="transer">
                                                <span>{parseInt(this.props.dataItem.transferCount)}</span> {units(parseInt(this.props.dataItem.transferCount), {
                                                nom: 'пересадка',
                                                gen: 'пересадки',
                                                plu: 'пересадок'
                                            })}
                                            </div>
                                        </div>
                                        <div className="bl">
                                            <div className="time">{arrivalHours}:{arrivalMinutes}</div>
                                            <div className="date">{arrivalDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }),
        RecomendationRight = React.createClass({
            renderBack: function (prop) {
                return (
                    <div className="recomendation-bl">
                        <VariantTo count={1} />
                        <Sort departureDateSrt={prop.departureDateSrt} departureCityName={prop.departureCityName}
                              arrivalCityName={prop.arrivalCityName}/>
                        <div className="recomendation-items">
                            <RecomendationPricedItinerary item={prop.searchData.pricedItineraries[prop.sequenceNumber]}
                                                          searchData={prop.searchData} />
                        </div>
                    </div>
                );
            },
            bookForms: function () {
                this.setState({
                    properties: this.props
                }, () => {
                    itemArray.dataItem.push(this.state.properties.dataItem);
                    renderBookForm(itemArray, searchData, properties.priceInfo, 'book_segments');
                    helpBookForm(this.state.properties);
                });
            },
            render: function () {
                return (
                    <div className="right">
                        <div className="l-content">
                            <div className="ticket-balance">
                                <span className="blue">Остался:</span>
                                <span>{'\u00A0'}</span>
                                <span className="red">1 БИЛЕТ</span>
                            </div>
                            <div className="summ">
                                <span
                                    className="price-r">{number_format(this.props.priceInfo.totalFare, 0, ' ', ' ')}</span>
                                <span className="currency">{' '}</span>
                                <span className="currency">KZT</span>
                                <span>Цена за всех пассажиров</span>
                            </div>
                            <div className="buy-button">
                                <a onClick={this.bookForms} className="button">Выбрать</a>
                            </div>
                        </div>
                    </div>
                );
            }
        }),
        RecomendationItem = React.createClass({
            getcombinationId: function (ids) {
                let aa = {};

                Object.keys(ids).map(function (key) {
                    aa = {
                        seq: key,
                        com: ids[key]
                    };
                });

                return aa;
            },
            render: function () {
                let combId = this.getcombinationId(this.props.pricedItinerary.combinationId);
                if (combId !== false) {
                    return (
                        <div className="recomendation-item"
                             data-timestamp={this.props.pricedItinerary.departureDateTime}
                             data-total={this.props.priceInfo.totalFare}
                             data-directtion={this.props.pricedItinerary.directionId}>
                            <RecomendationLeft dataItem={this.props.pricedItinerary}
                                               searchData={this.props.searchData}/>
                            <div className="center">
                                <div className="l-border"/>
                            </div>
                            <RecomendationRight priceInfo={this.props.priceInfo}
                                                dataItem={this.props.pricedItinerary}
                                                searchData={this.props.searchData}
                                                combinationId={combId.com}
                                                sequenceNumber={combId.seq} />
                        </div>
                    );
                } else {
                    return (
                        <div />
                    );
                }
            }
        }),
        RecomendationPricedItinerary = React.createClass({
            render: function () {
                return (
                    <div className="recomendation-items">
                        {
                            this.props.pricedItineraries.map((pricedItinerary, key) => {
                                return <RecomendationItem pricedItinerary={pricedItinerary}
                                                          priceInfo={pricedItinerary.priceInfo}
                                                          searchData={searchData}
                                                          key={key} />;
                            })
                        }
                    </div>
                );
            }
        }),
        Render = React.createClass({
            render: function () {
                return (
                    <div className="recomendation-bl">
                        <Variant />
                        <Sort arrivalDateSrt={searchData.arrivalDateSrt} arrivalCityName={searchData.arrivalCityName} />
                        <RecomendationPricedItinerary pricedItineraries={itemArray.dataItems} searchData={searchData} />
                        <div className="recomendation-sorting-back" />
                    </div>
                );
            }
        });

    $('#search-result').hide(200);

    ReactDOM.render(
        <Render />,
        document.getElementById('back-result')
    );

    setTimeout(function () {
        $('#back-result').show();
        $('html,body').animate({
            scrollTop: $('#selected-item').offset().top - 20
        }, 700);
    },250);
}

function renderItineraries(container, data) {
    //todo самый дешёвый
    let fastCheapest = {
        cheapest: null,
        fast: null
    };

    data.flightDataTo.sort(function (a, b) {
        return parseInt(a.priceInfo.totalFare) - parseInt(b.priceInfo.totalFare);
    });

    fastCheapest.cheapest = data.flightDataTo.splice(0, 1)[0];

    data.flightDataTo.sort(function (a, b) {
        return parseInt(a.elapsedTime) - parseInt(b.elapsedTime);
    });

    fastCheapest.fast = data.flightDataTo.splice(0, 1)[0];

    data.flightDataTo.sort(function (a, b) {
        return parseInt(a.priceInfo.totalFare) - parseInt(b.priceInfo.totalFare);
    });


    console.log(fastCheapest);
    console.log(data);

    let VariantFrom = React.createClass({
        render: function () {
                return (
                    <div className="departure-head">
                        <div className="dep-icon" />
                        <div className="center">Выберите рейс туда</div>
                        <div className="variants"><span>{this.props.count}</span> вариантов</div>
                    </div>
                );
            }
        }),
        Sort = React.createClass({
            render: function () {
                return (
                    <div className="rec-sort">
                        <div className="bl">Интересные варианты туда</div>
                        <div className="bl">
                            <div className="top">Летим туда → {this.props.departureDateSrt}</div>
                            <div className="bottom">
                                <div className="dep">{this.props.departureCityName}</div>
                                <div className="center">
                                    <img src="/images/head-arrow-icon.png"/>
                                </div>
                                <div className="arr">{this.props.arrivalCityName}</div>
                            </div>
                        </div>
                        <div className="bl">
                            <span className="by-price butt-sort">Цена</span>
                            <span className="by-time butt-sort">Время</span>
                        </div>
                    </div>
                );
            }
        }),
        Transfer = React.createClass({
            render: function () {
                const lnClass = 'transfer-line count-' + this.props.transferCount;
                if (!this.props.transfer) {
                    return (
                        <div className="transfer-line count-0">
                            <div className="ln" />
                            <div className="ln" />
                        </div>
                    );
                } else {
                    return (
                        <div className={lnClass}>
                            <div className="ln"/>
                            {
                                Object.keys(this.props.transfer).map((keyr) => {
                                    return <Tooltip transferdata={this.props.transfer[keyr]}/>;
                                })
                            }
                            <div className="ln"/>
                        </div>
                    );
                }
            }
        }),
        Tooltip = React.createClass({
            render: function () {
                return (
                    <div className="ln tooltipt"
                         data-transfer={this.props.transferdata.key}
                         data-city={this.props.transferdata.departureAirport}
                         data-deptime={this.props.transferdata.departureTime}
                         data-depdate={this.props.transferdata.departureDate}
                         data-arrtime={this.props.transferdata.arrivalTime}
                         data-arrdate={this.props.transferdata.arrivalDate} />
                );
            }
        }),
        RecomendationLeft = React.createClass({
            render: function () {
                let dateOptons = {
                        hour12: false,
                        weekday: 'short',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                    },
                    departureDate = new Date(this.props.dataItem.departureDateTime * 1000),
                    arrivalDate = new Date(this.props.dataItem.arrivalDateTime * 1000),
                    departureHours = (departureDate.getHours() > 9) ? departureDate.getHours() : '0'+departureDate.getHours(),
                    departureMinutes = (departureDate.getMinutes() > 9) ? departureDate.getMinutes() : '0'+departureDate.getMinutes(),
                    arrivalHours = (arrivalDate.getHours() > 9) ? arrivalDate.getHours() : '0'+arrivalDate.getHours(),
                    arrivalMinutes = (arrivalDate.getMinutes() > 9) ? arrivalDate.getMinutes() : '0'+arrivalDate.getMinutes();
                return (
                    <div className="left">
                        {(this.props.cheap !== undefined) ? <Head cheap={this.props.cheap} fast={false} searchData={this.props.searchData}/> : ''}
                        {(this.props.fast !== undefined) ? <Head cheap={false} fast={this.props.fast} searchData={this.props.searchData}/> : ''}
                        <div className="two-bl">
                            <div className="lt">
                                <div className="car-name">Перевозчик:</div>
                                <div className="logo">
                                    <object type="image/svg+xml" data={this.props.dataItem.img} />
                                </div>
                                <div className="air-company-name">
                                    {this.props.dataItem.marketingAirlineName}
                                </div>
                            </div>
                            <div className="rt">
                                <div className="top">
                                    <div className="bl">
                                        Рейс:
                                        <span>{this.props.dataItem.marketingAirlineCode}({this.props.dataItem.flightNumber}) — {this.props.searchData.cabin}</span>
                                    </div>
                                    <div className="bl">Общее время в пути – <span>{this.props.dataItem.elapsedTime.substr(0, 2)}
                                        ч {this.props.dataItem.elapsedTime.substr(2)} мин</span></div>
                                </div>
                                <div className="bottom">
                                    <div className="routes">
                                        <div className="left-route">
                                            <div className="bl">
                                                <span>Откуда:</span>
                                                <span>{this.props.dataItem.departureAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{this.props.dataItem.departureAirportName}</span>
                                            </div>
                                        </div>
                                        <div className="right-route">
                                            <div className="bl">
                                                <span>Куда:</span>
                                                <span>{this.props.dataItem.arrivalAirportName}</span>
                                            </div>
                                            <div className="bl">
                                                <span>Терминал:</span>
                                                <span>{this.props.dataItem.arrivalAirportName}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="time-and-transfer">
                                        <div className="bl">
                                            <div className="time">{departureHours}:{departureMinutes}</div>
                                            <div className="date">{departureDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                        <div className="bl">
                                            <Transfer transfer={this.props.dataItem.transferData.transfer}
                                                      transferCount={parseInt(this.props.dataItem.transferCount) + 2}/>
                                            <div className="transer">
                                                <span>{parseInt(this.props.dataItem.transferCount)}</span> {units(parseInt(this.props.dataItem.transferCount), {
                                                nom: 'пересадка',
                                                gen: 'пересадки',
                                                plu: 'пересадок'
                                            })}
                                            </div>
                                        </div>
                                        <div className="bl">
                                            <div className="time">{arrivalHours}:{arrivalMinutes}</div>
                                            <div className="date">{arrivalDate.toLocaleDateString('ru-RU', dateOptons)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }),
        RecomendationRight = React.createClass({
            bookForms: function () {
                if (data.searchData.back === false) {
                    bookForm(this.props, data.searchData, 'book_segments');
                } else {
                    renderBack(this.props, data);
                    bookForm(this.props, data, 'selected-item');
                }
            },
            render: function () {
                let priceString = (data.backTrip === true) ? 'от '+number_format(this.props.priceInfo.totalFare, 0, ' ', ' ') + ' KZT' : number_format(this.props.priceInfo.totalFare, 0, ' ', ' ') + ' KZT';
                return (
                    <div className="right">
                        <div className="l-content">
                            <div className="ticket-balance">
                                <span className="blue">Остался:</span>
                                <span>{'\u00A0'}</span>
                                <span className="red">1 БИЛЕТ</span>
                            </div>
                            <div className="summ">
                                <span
                                    className="price-r">{priceString}</span>
                                <span>Цена за всех пассажиров</span>
                            </div>
                            <div className="buy-button">
                                <a onClick={this.bookForms} className="button">Выбрать</a>
                            </div>
                        </div>
                    </div>
                );
            }
        }),
        Head = React.createClass({
            render: function () {
                let text = '',
                    classHead = '';
                if (this.props.fast === 1) {
                    text = 'Самый быстрый перелёт';
                    classHead = 'head hd-fast';
                } else {
                    text = 'Самый дешёвый перелёт';
                    classHead = 'head hd-cheap';
                }
                return (
                    <div className={classHead}>
                        <h1>{text}</h1>
                    </div>
                )
            }
        }),
        RenderItem = React.createClass({
            getVariantId: function(combinationId) {
                let returnVariant = {};

                Object.keys(combinationId).map((key) => {
                    returnVariant = {
                        seq: key,
                        com: combinationId[key]
                    }
                });
                return returnVariant;
            },
            itemBord: function () {
                let itemClas = '';
                if (this.props.fast === 1 || this.props.cheap === 1) {
                    itemClas = 'recomendation-item-bord';
                } else {
                    itemClas = 'recomendation-item';
                }

                return itemClas;
            },
            render: function () {
                let variant,
                    classItem = this.itemBord();
                if (!this.props.searchData.back) {
                    variant = this.getVariantId(this.props.item.combinationId);
                } else {
                    variant = this.props.item.combinationId
                }
                return (
                    <div className={classItem}
                         data-timestamp={this.props.item.departureDateTime}
                         data-total={this.props.item.priceInfo.totalFare}
                         data-refund={this.props.item.refund.toString()}
                         data-route={parseInt(this.props.item.transferCount)}
                         data-airline={this.props.item.marketingAirlineCode}
                         data-directtion={this.props.item.directionId} >
                        <RecomendationLeft dataItem={this.props.item}
                                           cheap={this.props.cheap}
                                           fast={this.props.fast}
                                           searchData={this.props.searchData} />
                        <div className="center">
                            <div className="l-border" />
                        </div>
                        <RecomendationRight priceInfo={this.props.item.priceInfo}
                                            dataItem={this.props.item}
                                            combinationId={variant} />
                    </div>
                );
            }
        }),
        RenderItems = React.createClass({
            render: function () {
                return (
                    <div className="recomendation-items">
                        {
                            Object.keys(this.props.pricedItineraries).map((itemKey) => {
                                let item = this.props.pricedItineraries[itemKey];
                                return <RenderItem key={itemKey}
                                                   item={item}
                                                   searchData={this.props.searchData} />;
                            })
                        }
                    </div>
                );
            }
        }),
        Rend = React.createClass({
            render: function () {
                return (
                    <div className="recomendation-bl">
                        <div className="cheap-reco bord">
                            <RenderItem item={fastCheapest.cheapest}
                                        cheap={1}
                                        searchData={data.searchData} />;
                        </div>
                        <div className="fast-reco bord">
                            <RenderItem item={fastCheapest.fast}
                                        fast={1}
                                        searchData={data.searchData} />;
                        </div>
                        <VariantFrom count={data.flightDataTo.length}/>
                        <Sort departureDateSrt={data.searchData.departureDateSrt} departureCityName={data.searchData.departureCityName}
                              arrivalCityName={data.arrivalCityName}/>
                        <RenderItems pricedItineraries={data.flightDataTo} searchData={data.searchData}/>
                        <div className="recomendation-sorting" />
                    </div>
                );
            }
        });

    ReactDOM.render(
        <Rend />,
        document.getElementById(container)
    );
}

function units(num, cases) {
    num = Math.abs(num);

    let word = '';

    if (num.toString().indexOf('.') > -1) {
        word = cases.gen;
    } else {
        word = (
            num % 10 === 1 && num % 100 !== 11
                ? cases.nom
                : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)
                ? cases.gen
                : cases.plu
        );
    }

    return word;
}

function number_format(number, decimals, dec_point, thousands_sep) {

    let i, j, kw, kd, km;

    // input sanitation & defaults
    if (isNaN(decimals = Math.abs(decimals))) {
        decimals = 2;
    }
    if (dec_point === undefined) {
        dec_point = ",";
    }
    if (thousands_sep === undefined) {
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

function toolTipInit(tooltip) {
    $(tooltip).tooltipster({
        theme: 'tooltipster-borderless',
        contentAsHTML: true,
        functionBefore: function (instance, helper) {
            let $content = $('#tooltip_template').clone(),
                transfer = helper.origin.attributes[1].textContent,
                city = helper.origin.attributes[2].textContent,
                depTime = helper.origin.attributes[3].textContent,
                depDate = helper.origin.attributes[4].textContent,
                arrTime = helper.origin.attributes[5].textContent,
                arrDate = helper.origin.attributes[6].textContent;
            $content.children('.transfer').text(transfer);
            $content.children('.city').text(city);
            $content.children('.dep-time').text(depTime);
            $content.children('.dep-date').text(depDate);
            $content.children('.arr-time').text(arrTime);
            $content.children('.arr-date').text(arrDate);
            instance.content($content.html());
        }
    });
}

function getBackData(combinationIds, flightDataBack) {

    return $.map(flightDataBack, function (flight) {
        let arr = [];
        Object.keys(combinationIds).map(function(comKey){
            Object.keys(flight.combinationId).map(function (flKey) {
                if (comKey === flKey) {
                    if (combinationIds[comKey].indexOf(flight.combinationId[flKey]) !== -1) {
                        arr.push(flight);
                    }
                }
            });
        });

        return arr;
    });
}