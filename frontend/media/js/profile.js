$(document).ready(function () {
    var $body = $('body');
    timeLimit($('.time-limit'));

    /*UPDATE Time limit*/
    $body.on('click', '.update-timelimit', updateTimeLimit);

    $body.on('click', '#send_pay_pdf', sendPdf);

    $body.on('click', '.penalty-info', penaltyInfo);
});