var controllerUrl = '/flight/flight/',
    $preloader = $('#overlay-loader');

function CancelBook(owner, pnr)
{
    swal({
        title: 'Внимание!',
        text: "Вы уверены что хотите отменить бронь?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        showLoaderOnConfirm: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Нет',
        confirmButtonText: 'Да'
    }).then(function () {
        $('#overlay-loader').fadeIn();
        $.ajax({
            url: controllerUrl + 'cancel-book',
            type: 'POST',
            dataType: 'json',
            data: {
                surname: owner,
                pnr: pnr
            },
            success: function (response) {
                $('#overlay-loader').fadeOut();
                returnSwall(response);
            }
        });
    });
}

function returnSwall(response) {
    swal({
        title: 'Внимание',
        text: response.text,
        type: response.status
    }).then(function () {
        location.reload();
    });
}

function downloadPdf(ticketId) {
    var pdfUrl = '/profile/user-profile/get-pdf?ticketId='+ticketId,
        pdfWindow = window.open('about:blank', 'pdf');
    $.ajax({
        url: '/',
        type: 'POST',
        data: {
            request: 'pdf'
        },
        success: function (response) {
            pdfWindow.location = pdfUrl;
        }
    });
}

function onExchange(ticketId, ticketNumber) {
    swal({
        title: 'Внимание!',
        text: 'Вы действительно хотите отправить на обмен билет '+ticketNumber+'?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        showLoaderOnConfirm: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Нет',
        confirmButtonText: 'Да'
    }).then(function () {
        $preloader.fadeIn();
        $.ajax({
            url: '/profile/user-profile/to-exchange-ticket',
            type: 'POST',
            dataType: 'json',
            data: {
                exchangeTicketId: ticketId,
                exchangeTicketNumber: ticketNumber
            },
            success: function (response) {
                if (response.status === 'success') {
                    location.href = '/';
                }
            }
        });
    });
}

function onRefund(ticketId, ticketNumber) {
    swal({
        title: 'Внимание!',
        text: 'Вы действительно хотите отправить на возврат билет '+ticketNumber+'?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        showLoaderOnConfirm: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Нет',
        confirmButtonText: 'Да'
    }).then(function () {
        $('#overlay-loader').fadeIn();
        $.ajax({
            url: '/profile/user-profile/to-refund-ticket',
            type: 'POST',
            dataType: 'json',
            data: {
                ticketId: ticketId,
                ticketNumber: ticketNumber
            },
            success: function (response) {
                if (response.status === 'success') {
                    location.reload();
                }
            }
        });
    });
}

function cancelToExchange() {
    $('#overlay-loader').fadeIn();
    $.ajax({
        url: '/profile/user-profile/cancel-to-exchange-ticket',
        type: 'POST',
        dataType: 'json',
        data: {
            cancel: 'ok'
        },
        success: function (response) {
            if (response.status === 'success') {
                location.reload();
            }
        }
    });
}

function timeLimit(element) {
    let $minute = element.find('.minute'),
        $second = element.find('.second');

    if (element.hasClass('active')) {
        let timer = setInterval(function () {
            let min = parseInt($minute.text()),
                sec = parseInt($second.text());
            if (min === 0 && sec === 0) {
                clearInterval(timer);
                sec = '0'+sec;
                swal({
                    title: 'Внимание!',
                    text: 'Тайм лимит на оплату закончился',
                    type: 'warning'
                }).then(function () {
                    location.reload();
                });
            } else {
                if (sec === 0) {
                    sec = 60;
                    --min;
                }
                --sec;
                if (sec < 10) {
                    sec = '0'+sec;
                }
            }
            $second.text(sec);
            $minute.text(min);
        }, 1000);
    }
}

function updateTimeLimit() {
    var exchangeId = $(this).attr('data-id');
    $('#overlay-loader').fadeIn();
    $.ajax({
        url: '/profile/user-profile/update-time-limit',
        type: 'POST',
        dataType: 'json',
        data: {
            exchangeId: exchangeId
        },
        success: function (response) {
            if (response.status === 'success') {
                location.reload();
            }
        }
    });
}

function sendPdf() {
    var payId = $(this).attr('data-id'),
        email = $('#payment_reciept_email').val();

    $('#overlay-loader').fadeIn();

    $.ajax({
        url: '/profile/user-profile/send-pay-pdf',
        type: 'POST',
        dataType: 'json',
        data: {
            payId: payId,
            email: email
        },
        success: function (response) {
            $('#overlay-loader').fadeOut();
            if (response.status === 'success') {
                $(this).disabled = true;
                swal({
                    title: 'Внимание!',
                    text: 'Платёжная квитанция отправлена.',
                    type: 'success'
                });
            }
            swal({
                title: 'Внимание!',
                text: response.message,
                type: response.status
            });
        }
    });
}

function penaltyInfo() {
    var ticketId = $(this).attr('data-id');

    $('#overlay-loader').fadeIn();

    $.ajax({
        url: '/profile/user-profile/penalty-info',
        type: 'POST',
        data: {
            ticket_id: ticketId
        },
        success: function (response) {
            $('#overlay-loader').hide();
            $('#result_modal').html(response);
            $('#tickets_modal').modal('show');
        }
    });
}

function openPassengerButtonContainer()
{
    $(this).next().fadeToggle(200);
}