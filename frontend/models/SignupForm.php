<?php
namespace frontend\models;

use backend\modules\integra\models\TUsers;
use common\models\UserProfile;
use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const SCENARIO_COMPANY = 'company';
    const SCENARIO_USER = 'user';

    public $username;
    public $email;
    public $password;

    public $repeat_password;
    public $company_type;
    public $company_name;
    public $phone;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_COMPANY => ['username', 'email', 'password', 'rememberMe', 'repeat_password', 'company_type', 'company_name', 'phone'],
            self::SCENARIO_USER => ['username', 'email', 'password', 'rememberMe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Поле не может быть пустым'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Пользователь с таким именем уже существует'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'message' => 'Поле не может быть пустым'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Пользователь с таким адресом уже существует'],

            ['password', 'required', 'message' => 'Поле не может быть пустым'],
            ['password', 'string', 'min' => 6],

            [['company_type', 'company_name', 'phone'], 'required', 'message' => 'Поле не может быть пустым'],

            ['repeat_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
            ['repeat_password', 'required', 'message' => 'Поле не может быть пустым'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me'),
            'repeat_password' => Yii::t('app', 'Repeat password'),
            'company_name' => Yii::t('app', 'Company name'),
            'phone' => Yii::t('app', 'Phone'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($this->company_name) {
            $companyTypeRole = NULL;
            $companyTypeStatus = NULL;
            switch ($this->company_type) {
                case "agency":
                    $companyTypeRole = 3;
                    $companyTypeStatus = User::STATUS_AGENCY_BEFORE_APPLY;
                    break;
                case "hotel":
                    $companyTypeRole = 4;
                    $companyTypeStatus = User::STATUS_HOTEL_BEFORE_APPLY;;
                    break;
                case "corp_accountant":
                    $companyTypeRole = 6;
                    $companyTypeStatus = User::STATUS_CORP_ACCOUNTANT_BEFORE_APPLY;
                    break;
            }

//            $companyTypeRole = $this->company_type === 'agency' ? 3 : 4;
//            $companyTypeStatus = $this->company_type === 'agency' ? User::STATUS_AGENCY_BEFORE_APPLY : User::STATUS_HOTEL_BEFORE_APPLY;
            $tUser = new TUsers();
            $tUser->user_login = $this->email;
            $tUser->role = $companyTypeRole;
            $tUser->phone = $this->phone;
            $tUser->position = $this->company_name;
            $tUser->save();

            $user->status = $companyTypeStatus;
        }
        
        return $user->save() ? $user : null;
    }

    public static function getCompanyTypeList()
    {
        return [
            'agency' => Yii::t('app', 'Agency'),
            'hotel' => Yii::t('app', 'Hotel'),
            'corp_accountant' => Yii::t('app', 'Corp Accountant'),
        ];
    }
}
