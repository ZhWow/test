<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PersonForm extends Model
{
    public $code;
    public $lastname;
    public $name;
    public $gender;
    public $docIssueCountry;
    public $innerDocType;
    public $docExpireDate;
    public $birthday;
    public $docnumber;
    public $email;
    public $phone;

    public $lastSegmentDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lastSegmentDate', 'code', 'lastname', 'name', 'gender', 'docIssueCountry', 'innerDocType', 'docExpireDate', 'birthday', 'docnumber', 'phone', 'email'], 'required', 'message'=>'Не заполнено поле {attribute}'],
            [['docExpireDate', 'birthday'],'match','pattern' => '/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/','message'=>'Некорректный формат поля {attribute}'],
            ['birthday', 'checkAge'],
            [['phone'],'match','pattern' => '/^(\+\d{11})$/','message'=>'Некорректный формат поля {attribute}'],
            [['email'], 'email'],
        ];
    }


    public function checkAge($attribute) {
        list($y, $m, $d) = explode('-', $this->birthday); //Дата рождения
        list($f_y, $f_m, $f_d) = explode('-', $this->lastSegmentDate); //Дата последнего вылета

        if($m > $f_m || $m == $f_m && $d > $f_d) {
            $age = ($f_y - $y - 1); // если ДР в этом году не было, то ещё -1
        } else {
            $age = ($f_y - $y); // если ДР в этом году был, то отнимаем от этого года год рождения
        }

        if ($this->code == 'INF') {
            if ($age >= 2) {
                $this->addError('birthday', 'Младенец до 2 лет');
            }
        }

        if ($this->code == 'CHD') {
            if ($age >= 12) {
                $this->addError('birthday', 'Ребенок от 2 до 12 лет');
            }
        }

    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code'          =>  'Тип по возрасту',
            'lastname'      =>  'Фамилия',
            'gender'        =>  'Пол',
            'innerDocType'  =>  'Паспорт/удостоверение',
            'docExpireDate' =>  'Годен до',
            'birthday'      =>  'Дата рождения',
            'docnumber'     =>  'Номер документа',
            'email'         =>  'Email',
            'phone'         =>  'Телефон',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }

    public static function transformValues($code, $value, $i) {


        if (!isset($value[$i]) || !$value[$i]) {
            return '';
        }
        $grandVal = $value[$i];

        if (in_array($code, array('docExpireDate', 'birthday'))) {
            $day = substr($grandVal, 0, 2);
            $month = substr($grandVal, 3, 2);
            $year = substr($grandVal, 6);
            $grandVal = "$year-$month-$day";

        }
        if ($code == 'phone') {
            $grandVal = str_replace("-","",$grandVal);
        }

        return $grandVal;
    }
}
