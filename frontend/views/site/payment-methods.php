<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use frontend\assets\PayAsset;

$this->title = 'Способы оплаты';
$this->params['breadcrumbs'][] = $this->title;

PayAsset::register($this);
?>
<div class="site-payment-methods qway-wrap">
    <?php

    echo Tabs::widget([
        'items' => [
            [
                'label' => 'Банковской картой',
                'content' => $this->render('payment/_bank'),
                'active' => true
            ],
            [
                'label' => 'Наличными в офисе',
                'content' => $this->render('payment/cash'),
//                'headerOptions' => [...],
//                'options' => ['id' => 'myveryownID'],
            ],
    ],
]);

    ?>
</div>