<?php

/* @var $this yii\web\View */

$this->title = 'Логин';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login qway-wrap">

    <?= $this->render('_login-form', ['model' => $model]) ?>

</div>
