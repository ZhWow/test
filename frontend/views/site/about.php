<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О компании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about qway-wrap">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Qway- онлайн-сервис для покупки авиабилетов и бронирования отелей по всему миру.</p>

    <h1>Контакты</h1>
    <p>
        ТОО «Qway»<br>
        г. Алматы, ул. Сатпаева, дом 30/А/3<br>
        тел: 8(727) 346 83 86<br>
        E-mail: info@qway.kz<br>
    </p>
</div>
