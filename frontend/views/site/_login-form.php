<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>

<div class="row login-form-wrap">

    <div class="col-lg-12 login-form-header">
        <h3>Авторизация</h3>
    </div>

    <div class="col-lg-12 login-form-body">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'action' => '/site/login',
        ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Email') ?>

        <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>

        <div class="form-group">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <div class="col-lg-12 login-form-additional">
            <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить меня') ?>

            <div style="color:#999;">
                <?= Html::a('Забыли пароль?',
                    ['site/request-password-reset'],
                    ['class' => 'reset-password-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>