<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use modules\flight\frontend\models\ElectronicTickets;
use yii\bootstrap\Modal;

$this->title = 'Следуюшая поездка';
$this->params['breadcrumbs'][] = $this->title;

\modules\flight\frontend\assets\ProfileAsset::register($this);
?>
<div class="site-next-trip qway-wrap">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php

    $allTicketsUser = ElectronicTickets::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
    /* @var $ticket ElectronicTickets */
    foreach ($allTicketsUser as $ticket) : ?>
        <?php
        $currentTime = time();
        $ticketDepartureDate = explode(';', $ticket->departure_datetime)[0];
        $ticketDepartureTimeArray = explode('T', $ticketDepartureDate);
        $ticketDepartureTime = strtotime($ticketDepartureTimeArray[0] . ' ' . $ticketDepartureTimeArray[1]);


        if ($currentTime < $ticketDepartureTime) : ?>
            <div>
                номер билета: <?= $ticket->ticket_number ?><br>

                TotalFare: <?= $ticket->total_fare ?><br>
                <?php
                if ($ticket->exchange === ElectronicTickets::PENALTY_READY) {
                    echo Html::button('Условия обмена', [
                        'class' => 'penalty-condition',
                        'data' => [
                            'action' => 'exchange',
                            'key' => $ticket->id,
                            'before' => $ticket->exchange_before_departure,
                            'after' => $ticket->exchange_after_departure,
                        ]
                    ]);
                }

                if ($ticket->refunds === ElectronicTickets::PENALTY_READY) {
                    echo Html::button('Условия возврата', [
                        'class' => 'penalty-condition',
                        'data' => [
                            'key' => $ticket->id,
                            'action' => 'refunds',
                            'before' => $ticket->refunds_before_departure,
                            'after' => $ticket->refunds_after_departure,
                        ]
                    ]);
                }
                ?>
            </div>
        <?php endif; ?>
        <hr>
    <?php endforeach; ?>

</div>

<?php
Modal::begin([
    'header' => '<h3>Штрафы</h3>',
    'id' => 'penalty-modal-client',
    /*'options' => [
        'class' => 'book-modal'
    ]*/
]);
?>
<div id="penalty-content">
    <div class="before">
        До первого вылета: <span></span>
    </div>
    <div class="after">
        После первого вылета: <span></span>
    </div>

    <button type="button" class="penalty-confirm"></button>
</div>
<?php
Modal::end();
