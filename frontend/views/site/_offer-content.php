<h1>Публичная оферта Qway.kz</h1>

<p>ТОО «Qway», расположенное по адресу: Республика Казахстан, г. Алматы, ул. Сатпаева, 30А/3 именуемое в дальнейшем «Агент», предлагает любому физическому лицу, индивидуальному предпринимателю или юридическому лицу, в дальнейшем именуемому «Клиент», платные услуги, службы и сервисы в сети Интернет, а также программное обеспечение и иные программные продукты (далее – Услуги и/или Продукты).</p>
<p>Настоящее предложение в соответствии со ст. 395 Гражданского Кодекса РК является публичной офертой (далее – Договор), полным и безоговорочным принятием (акцептом) условий которой в соответствии со ст. 396 Гражданского кодекса РК считается осуществление Клиентом конклюдентных действий – осуществление поиска, бронирования и покупки Билетов в системе Агента, расположенной на Сайте Агента.</p>

<h3>1. ТЕРМИНЫ И ОПРЕДЕЛЕНИЯ.</h3>
<ul>
    <li>1.1. Сайт Агента – WEB-сайт, расположенный по адресу http://www.Qway.kz,  в сети Интернет. На Cайте Агента Клиентам круглосуточно и бесплатно предоставляется в том числе информация, необходимая для заключения и исполнения настоящего Договора, публикуются все официальные документы Агента и т. д.;</li>
    <li>1.2. ГДС (GDS) — глобальные системы дистрибуции авиабилетов, используемые большинством авиакомпаний.</li>
    <li>1.3. Бронь — предварительный резерв авиабилетов конкретной авиакомпании по конкретному направлению на конкретные даты, закрепляемый за Клиентом по определенной стоимости.</li>
    <li>1.4. Билет — (пассажирский билет и багажная квитанция) — документ на перевозку, выдаваемый авиакомпанией либо его агентами, удостоверяющий заключение договора с условиями воздушной перевозки пассажира.</li>
    <li>1.5. Лоукостеры, авиадискаунтеры — низкобюджетные авиаперевозчики, стоимость авиабилетов на которые может быть в несколько раз ниже билетов традиционных авиалиний. Как правило, подобная стоимость достигается отсутствием бесплатного питания на борту, отсутствием бесплатного багажа, а также рядом других ограничений, которые публикуются на сайте авиаперевозчика. Подобные авиаперевозчики выделены на Сайте Агента в отдельный раздел, либо в случае размещения в общем разделе поиска авиабилетов имеют специальную пометку «лоукостер».</li>
</ul>

<h3>2. ПРАВА И ОБЯЗАННОСТИ КЛИЕНТА</h3>
<ul>
    <li>2.1. Клиент несет ответственность за правильность введенных данных, в том числе за соответствие введенных данных о пассажирах их паспортным данным. При любых изменениях личных данных пассажиров, по международным стандартам, необходимо переоформление билетов по тарифам, доступным на момент переоформления.</li>
    <li>2.2. Билеты считаются купленными Клиентом по факту отправки Агентом письма с Билетом (маршрутной квитанцией) на электронную почту Клиента, указанную при бронировании.</li>
    <li>2.3. Покупая (оплачивая) билеты на сайте Qway.kz, Клиент подтверждает, что самостоятельно изучил, понял и принимает Правила применения тарифа к билету, отправляемые на указанный Клиентом электронный адрес на английском языке и/или подтверждает, что Агент проинформировал его (Клиента) в Публичной оферте о праве получить разъяснение Правил применения тарифа на казахском и/или русском языке до покупки (оплаты) билета, обратившись в службу поддержки, отправив запрос на электронную почту, и/или по телефонам, указанным в разделе «Контакты». Клиент соглашается, что самостоятельно решает обращаться или не обращаться в службу поддержки за разъяснением Правил применения тарифа на казахском и/или русском языке и принимает, что после оплаты билета, все процедуры, связанные с обменом, возвратом билета производятся Агентом только в строгом соответствии с Правилами применения тарифа и Клиент не имеет претензий по содержанию данных Правил применения тарифа, так как подтвердил своё согласие с ними.</li>
    <li>2.4. Клиенту необходимо самостоятельно уточнять информацию о допустимом весе, габаритах и количестве единиц багажа, ручной клади, при этом вся устная информация, получаемая от Агента, носит лишь рекомендательный характер и актуальна на день информирования. </li>
    <li>2.5. Клиенту необходимо самостоятельно уточнять информацию, по какому документу необходимо лететь (внутренний документ или международный паспорт), о необходимых визах, разрешениях на въезд, прививках, страховых полисах и иных разрешительных документах и процедурах для страны пункта назначения или пересадочного пункта, при этом вся устная информация, получаемая от Агента, носит лишь рекомендательный характер и актуальна на день информирования.</li>
    <li>2.6. При оплате наличными в офисах партнёров Агента, сотрудники данных офисов не обязаны консультировать Клиента по каким-либо вопросам, так как осуществляют только приём оплаты за билеты Агента и не являются сотрудниками Агента, всю необходимую информацию Клиент вправе получить в службе поддержки Агента, связавшись любым удобным способом, указанным в разделе «Контакты».</li>
</ul>

<h3>3. ПРАВА И ОБЯЗАННОСТИ АГЕНТА</h3>
<ul>
    <li>3.1. При оплате Клиентом авиабилетов банковской картой, Агент имеет право запросить у Клиента скан или фотографию лицевой стороны банковской карты, где будут четко различимы первые 6 и последние 4 цифры номера карты, а также копию документа, удостоверяющего личность. В случае отказа Клиента, Агент имеет право отказать в предоставлении услуг, а также аннулировать ранее купленные Клиентом авиабилеты.</li>
    <li>3.2. Агент имеет право отправлять Клиенту информационные сообщения по электронной почте и/или посредством СМС, связанные с авиаперелетами, такие как информация о ценах, об изменениях в брони, изменениях и отменах рейсов и др.</li>
    <li>3.3. Предоставляемая Клиентом персональная информация (имя, адрес, телефон, e-mail, номер кредитной карты) является конфиденциальной и не подлежит разглашению третьим лицам, за исключением случаев, предусмотренных Законодательством РК.</li>
    <li>3.4. Информация о ценах, публикуемая на сайте и в мобильных приложениях до момента предварительного бронирования, а также в информационных письмах-рассылках, не является офертой. Ввиду ограниченного количества билетов по каждому тарифу, установленному авиакомпанией, актуальная цена появляется после ввода паспортных данных и появления информации о том, что билеты успешно забронированы.</li>
</ul>

<h3>4. УСЛОВИЯ ОБМЕНА И ВОЗВРАТА АВИАБИЛЕТОВ</h3>
<ul>
    <li>4.1. Обмен и возврат авиабилетов осуществляется ежедневно 24 часа в сутки <strong>в соответствии с правилами применения тарифов</strong>, устанавливаемыми авиакомпанией (Далее — «Правила применения тарифов»), которые прикрепляются к письму, отправляемому после создания Брони на электронную почту Клиента, указанную в момент бронирования. Для заявки на возврат или обмен необходимо написать запрос на help@qway.kz с того Email адреса, который указывался при бронировании или позвонить с того номера телефона, который указывался при бронировании.</li>
    <li>4.2. При отмене авиабилетов возврат денежных средств производится строго в соответствии с Правилами применения тарифов.</li>
    <li>4.2.1 Рассчитанная к возврату сумма с учётом возможных штрафов авиакомпании, согласно внутреннему регламенту платежных систем Visa/MasterCard, перечисляется на счёт карты, по которой ранее была совершена покупка, от двух до тридцати рабочих дней (зависит от банка, выпустившего карту) с момента запуска процесса отмены/возврата.</li>
    <li>4.2.2 Если вы оплачивали забронированные на Qway авиабилеты в офисе компании, то в первую очередь необходимо отправить нам заявку на возврат/обмен. Только после обработки вашей заявки мы отправляем уведомление , где была произведена оплата, после чего можно получить сумму, подлежащую к возврату.</li>
    <li>4.3. Услуга обмена билета является платной. При обмене билета на рейсы внутри Республики Казахстан стоимость обмена составляет 1500 (Одна тысяча пятьсот) тенге, при обмене билета на международный рейс стоимость обмена билета в эконом классе составляет 4000 (Четыре тысячи) тенге, в бизнес классе - 6000 (Шесть тысяч) тенге.</li>
    <li>4.4 Услуга возврата авиабилета является платной. При возврате билета на рейсы внутри Республики Казахстан стоимость возврата составляет 2500 (Две тысячи пятьсот) тенге, при возврате билета на международный рейс стоимость возврата билета в эконом классе составляет 4000 (Четыре тысячи) тенге, в бизнес классе - 6000 (Шесть тысяч) тенге.</li>
    <li>4.5 При вынужденном возврате услуга возврата осуществляется бесплатно.</li>
    <li>4.6 Для оформления возврата авиабилета пассажир предъявляет билет с полетным купоном. Билет с использованными полетными купонами недействителен для перевозки пассажира и возврата денег. Авиакомпания принимает полетные купоны для перевозки пассажира и его багажа только в строгой последовательности, начиная с пункта отправления, указанного в пассажирском купоне.</li>
    <li>4.7. Авиабилеты на перелеты лоукостерами обмену и возврату не подлежат.</li>
</ul>

<h3>5. ОТВЕТСТВЕННОСТЬ СТОРОН</h3>
<ul>
    <li>5.1. Результаты поиска цен на билеты по выбранному Клиентом направлению, отображаемые на Сайте Агента, не являются окончательными, а окончательные цены на билеты указываются только в момент появления страницы для заполнения данных по брони и действительны на момент появления, при этом в момент нажатия на кнопку «Забронировать» происходит фиксация стоимости Билета в ГДС при наличии достаточного количества мест в ГДС.</li>
    <li>5.2. Бронь, создаваемая более чем на 30 минут, до момента оплаты и выписки билета может быть изменена или отменена авиакомпанией в одностороннем порядке с уведомлением Агента посредством ГДС, вследствие чего Агент не несет ответственности за изменение стоимости забронированных авиабилетов, при этом Агент обязуется приложить все усилия для уведомления Клиента о соответствующих изменениях путем оповещения Клиента на электронную почту или мобильный телефон, указанные Клиентом в момент создания Брони.</li>
    <li>5.3. В случае создания Клиентом двух или более броней на одних и тех же пассажиров на один и тот же рейс, авиакомпании в одностороннем порядке могут аннулировать дублирующиеся брони с более дешевой стоимостью.</li>
</ul>

<h3>6. Прочие условия</h3>
<ul>
    <li>Пользователь согласен с тем, что совершая покупки на сайте Qway.kz, он передает свои личные данные и на его электронный адрес могут направляться письма и сообщения, в том числе рекламного характера.</li>
    <li>6.1. Пользователь согласен с тем, что Qway.kz использует и обрабатывает персональные данные Пользователя.</li>
    <li>6.2. Qway.kz использует информацию в том числе:
        <ul><li>- для выполнения своих обязательств перед Пользователем;</li></ul>
    </li>
    <li>6.3. Qway.kz обязуется не разглашать полученную от Пользователя информацию. Не считается нарушением предоставление Qway.kz информации агентам и третьим лицам, действующим на основании договора с Qway.kz, для исполнения обязательств перед Пользователем. Не считается нарушением обязательств разглашение информации в соответствии с обоснованными и применимыми требованиями закона. Qway.kz вправе использовать технологию «cookies». «Cookies» не содержат конфиденциальную информацию и не передаются третьим лицам. Qway.kz получает информацию об ip-адресе посетителя Сайта. Данная информация не используется для установления личности Пользователя, за исключением случаев мошеннических действий Пользователя.</li>
    <li>6.4. Пользователь несет ответственность за достоверность передаваемых Qway.kz персональных данных.</li>
</ul>

<h3>7. Информационные сообщения</h3>
<ul>
    <li>Пользователь дает свое согласие на получение обновленной информации о деятельности компании Qway.kz, информационных сообщений с рекламными и акционными предложениями и объявлениями о продаже, а также информации о новостях и предложениях партнеров Qway.kz посредством электронной почты.</li>
</ul>

<p>8. Пользователь дает согласие на запись телефонных разговоров для повышения качества обслуживания и подтверждения устных заявлений Пользователя, сохранение паспортных данных пассажиров исключительно в целях удобства последующих покупок.</p>
<p>9. Пользователь уведомлен и соглашается, что при использовании мобильного приложения Qway.kz передается следующая информация: тип операционной системы мобильного устройства Клиента, версия и идентификатор приложения, состояние сети, статистику использования приложения и другую техническую информацию.</p>
