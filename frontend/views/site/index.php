<?php
use \modules\flight\frontend\models\base\FlightElectronicTickets;
/* @var $this yii\web\View */

$this->title = 'Главная';
$session = $session = Yii::$app->session;
$ticketData = [];
$ticketRouteStr = '';
//$session->destroy();
//$session->remove('exchangeTicketId');
//$session->remove('exchangeTicketNumber');
if ($session->has('exchangeTicketId')) {
    $ticket = FlightElectronicTickets::find()->where(['id' => $session->get('exchangeTicketId')])->with('itineraries')->one();
    foreach ($ticket->itineraries as $key => $segment) {
        $ticketRouteStr .= $segment->from . '(' . $segment->cityFrom->name_ru . ')-' . $segment->to . '(' . $segment->cityTo->name_ru . ')';
        if ($key < count($ticket->itineraries)-1) {
            $ticketRouteStr .= ', ';
        }
    }
} else {
    $session->remove('exchangeTicketId');
    $session->remove('exchangeTicketNumber');
}

?>
<div class="site-index">
    <?php if ($session->has('exchangeTicketId')) :?>
        <div class="exchange-data">
            <h1 class="exchange-data-title">Билет на обмен: <?=$session->get('exchangeTicketNumber');?> <?=$ticketRouteStr;?> <button class="cancel-exchange btn btn-default" onclick="cancelToExchange();">Отменить</button></h1>
        </div>
        <div id="search-form-flight-wrap">
            <?= $this->render('@modules/flight/frontend/views/flight/_search-form-flight', ['ticketData' => $ticketData]); ?>
        </div>
    <?php else :?>
        <div id="search-form-flight-wrap">
            <?= $this->render('@modules/flight/frontend/views/flight/_search-form-flight') ?>
        </div>
    <?php endif;?>

</div>

<div id="loader-flight-search">
    <div class="loader-main">
        <div></div>
    </div>
    <!--<img src="/images/loaders/loader.gif" alt="load...">-->
</div>
<div id="selected-item"class="recomendation-bl center"></div>
<div class="recomendation_filter"></div>
<div id="search-result"></div>
<div id="back-result" style="display: none;"></div>
<div id="book-form"></div>
