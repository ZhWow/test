<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Зарегистрироватся';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup qway-wrap">

    <div class="page-description">
        <h2>Регистрация,</h2>
        <h3>Заполните следующие поля</h3>

    </div>

    <div class="row">

        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Имя пользователя',['style' => 'color: #fff;']) ?>

                <?= $form->field($model, 'email')->label('Email/Логин',['style' => 'color: #fff;']) ?>

                <?= $form->field($model, 'password')->passwordInput()->label('Пароль',['style' => 'color: #fff;']) ?>

                <br>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироватся', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>
