<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Договор публичной оферты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-offer qway-wrap">

    <?= $this->render('_offer-content') ?>

</div>