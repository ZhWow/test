<?php

use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use common\models\LoginForm;

?>
<header>
    <div class="content">
        <div class="logotip">
            <a href="/"><img src="/images/Qway_logo.png"></a>
        </div>
    <?php
    NavBar::begin([
        'options' => [
            'id' => 'qway-header-nav',
            'class' => 'navbar-inverse',
        ],
    ]);

//    $menuItems[] = ['label' => 'О компании', 'url' => ['/site/about']];

    $menuItemsLeft = [
        ['label' => 'О компании', 'url' => ['/site/about']],
        ['label' => 'Способы оплаты', 'url' => ['/site/payment-methods']],
    ];

    $menuItems = [
        ['label' => 'Авиа', 'url' => ['/site/index']],
//        ['label' => 'Отели', 'url' => ['/site/hotels']],
        ['label' => 'Отели'],
//        ['label' => 'ЖД', 'url' => ['/site/rail-ways']],
        ['label' => 'ЖД'],
    ];



//    if (!Yii::$app->user->isGuest && Yii::$app->user->can('ReturnAndExchange')) {
//        $menuItems[] = ['label' => 'Управление продажами', 'items' => [
//            ['label' => 'Активные билеты', 'url' => ['/site']],
//            ['label' => 'Заявки на обмен', 'url' => ['/site']],
//            ['label' => 'Заявки на возврат', 'url' => ['/site']],
//            ['label' => 'Отчеты', 'url' => ['/site']],
//        ]];
//        /* $menuItems[] = ['label' => 'История поездок', 'url' => ['/site/history-trip']];
//         $menuItems[] = ['label' => 'Следуюшая поездка', 'url' => ['/site/next-trip']];*/
//        /* $menuItems[] = ['label' => 'profile', 'items' => [
//             ['label' => 'История поездок', 'url' => ['/site/history-trip']],
//             ['label' => 'Следуюшая поездка', 'url' => ['/site/next-trip']],
//         ]];*/
//    }

    $menuItemsRight = [];
    if (Yii::$app->user->isGuest) {
        //$menuItemsRight[] = ['label' => 'Войти', 'url' => ['/site/login']];

        $loginForm = $model = new LoginForm();

        $menuItemsRight[] = ['label' => 'Войти', 'items' => [
            $this->render('@frontend/views/site/_login-form', ['model' => $model])
        ],'options'=> ['class'=>'login-button'],];

        $menuItemsRight[] = [
            'label' => 'Регистрация',
            'url' => ['/site/signup'],
            'options'=> ['class'=>'sign-up-button'],
//            'linkOptions'=>['class'=>'item-a-class'],
        ];



    } else {

//        if ($profile = \common\models\UserProfile::findOne(['user_id' => Yii::$app->user->identity->id])) {
//            $userProfileId = $profile->id;
//            $labelProfile = ['label' => 'Редактировать', 'url' => ['/profile/user-profile/update', 'id' => $userProfileId]];
//        } else {
//            $labelProfile = ['label' => 'Редактировать', 'url' => ['/profile/user-profile/create']];
//        }
//
//
//        $menuItemsRight[] = ['label' => 'Профиль', 'items' => [
//            ['label' => 'История поездок', 'url' => ['/site/history-trip']],
//            ['label' => 'Следуюшая поездка', 'url' => ['/site/next-trip']],
////            ['label' => 'Редактировать', 'url' => ['/profile']],
//            $labelProfile,
//            '<li>'
//            . Html::beginForm(['/site/logout'], 'post')
//            . Html::submitButton(
//                'Выйти',
//                ['class' => 'btn btn-link qway-logout']
//            )
//            . Html::endForm()
//            . '</li>'
//        ]];

        $menuItemsRight[] = ['label' => 'Меню пользователя', 'items' => [
            ['label' => 'Личный кабинет', 'url' => ['/profile']],
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выйти',
                ['class' => 'btn btn-link qway-logout']
            )
            . Html::endForm()
            . '</li>'
        ]];

    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left-custom'],
        'items' => $menuItemsLeft,
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-center-custom'],
        'items' => $menuItems,
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right navbar-right-custom'],
        'items' => $menuItemsRight,
    ]);


    NavBar::end();
    ?>
    </div>
</header>