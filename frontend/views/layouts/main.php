<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\JSXAsset;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

JSXAsset::register($this);
AppAsset::register($this);
$this->registerJs("var __CUR_LANG = " . json_encode(\common\services\QLanguage::getCurrentLang()) . ";", \yii\web\View::POS_HEAD);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Qway | <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="overlay"></div>


<div class="wrap">

    <?= $this->render('@frontend/views/layouts/_index-header') ?>

    <?= $content ?>

    <?= $this->render('@frontend/views/layouts/_pre_footer') ?>

</div>

<?= $this->render('@frontend/views/layouts/_footer') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
