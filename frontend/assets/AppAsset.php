<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/media';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/history.css',
        'css/index.css',
        'css/sweetalert2.min.css',
        'css/tooltipster.bundle.min.css',
        'css/tooltipster.main.min.css',
        'css/tooltipster-sideTip-borderless.min.css',
    ];
    public $js = [
        'js/jquery.cookie.js',
        'js/sweetalert2.min.js',
        'js/tooltipster.bundle.min.js',
        'js/functions.js',
        'js/profile.js',
        'js/react-full.js',
        'js/react-dom.js',
        'js/babel-core.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
