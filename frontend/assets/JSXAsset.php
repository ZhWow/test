<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 17.10.2017
 * Time: 21:36
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class JSXAsset extends AssetBundle
{
    public $sourcePath = '@app/media';
    public $baseUrl = '@web';
    public $js = [
        'js/react_views/recomendation-render.js',
    ];

    public $jsOptions = [
        'type'=>'text/babel',
        'position' => \yii\web\View::POS_HEAD
    ];

    public $depends = [
        'frontend\assets\AppAsset'
    ];
}