<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 07.11.2017
 * Time: 13:44
 */
namespace frontend\assets;

use yii\web\AssetBundle;

class PayAsset extends AssetBundle
{
    public $sourcePath = '@app/media';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $baseUrl = '@web';
    public $css = [
        'css/payment/pay.css',
    ];
}