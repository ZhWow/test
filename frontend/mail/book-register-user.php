<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $person_attribute SimpleXMLElement */
/* @var $flight modules\flight\frontend\models\Flights */
/* @var $NewUser boolean comes from controller and means that user registered automatically or not*/
?>
    Здравствуйте,<br /> <br />

    Для Вас было создано бронирование с номером <b><?= $flight->pnr_no ?></b>. Для оплаты за авиабилеты Вы можете воспользоваться личным кабинетом.<br />
    Для авторизации пользователя воспользуйтесь функцией "Войти" по ссылке <a href="<?= Url::home('http') ?>">QWAY</a> <br /> <br />

    <?php if($NewUser) : ?>

    Ваш логин: <?= $person_attribute->Email ?><br />
    Ваш пароль: 123456 <br />
    Для безопасности Ваших данных, рекомендуем сменить пароль в личном кабинете в разделе "Настройки"<br/>

    <?php endif; ?>
