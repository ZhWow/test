<?php

namespace common\services;

use common\models\SysLang;
use Yii;

class QLanguage
{
    private static $instance;

    protected static $excludeSysLang = ['tt']; // sysLang->url
    protected static $systemLang = ['en']; // sysLang->url

    private $_excludeSysLang = [];

    private function __construct($excludeSystemLang = false)
    {
        $allSysLang = SysLang::findBySql('
            SELECT `url`, `local`, `name` 
            FROM `sys_lang`')
            ->all();

        foreach ($allSysLang as $sysLang) {
            /* @var $sysLang SysLang */
            if (in_array($sysLang->url, self::$excludeSysLang)) { continue; }
            if ($excludeSystemLang && in_array($sysLang->url, self::$systemLang)) { continue; }
            array_push($this->_excludeSysLang, $sysLang);
        }
    }

    public static function getInstance($excludeSystemLang = false)
    {
        if (empty(self::$instance)) {
            self::$instance = new self($excludeSystemLang);
        }
        return self::$instance;
    }

    public function getLang()
    {
        return $this->_excludeSysLang;
    }

    public static function getCurrentLang()
    {
        $currentLang = Yii::$app->language;
        return trim(explode('-', $currentLang)[0]);
    }

    public static function getDefaultLang()
    {
        // TODO hard code
        return 'ru';
    }

    public static function getSourceLang()
    {
        $sourceLang = Yii::$app->sourceLanguage;
        return trim(explode('-', $sourceLang)[0]);
    }
}