<?php

namespace common\models;

use modules\flight\frontend\models\Documents;
use modules\flight\frontend\models\Persons;
use Yii;
use modules\flight\frontend\providers\amadeus\Amadeus;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $surname
 * @property string $phone
 * @property string $birthday
 * @property string $gen
 * @property string $doc_type
 * @property string $doc_issue_country
 * @property string $doc_expire_date
 * @property string $doc_id
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'surname', 'phone', 'birthday', 'gen', 'doc_type', 'doc_issue_country', 'doc_expire_date', 'doc_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => 'Phone',
            'birthday' => 'Birthday',
            'gen' => 'Gen',
            'doc_type' => 'Doc Type',
            'doc_issue_country' => 'Doc Issue Country',
            'doc_expire_date' => 'Doc Expire Date',
            'doc_id' => 'Doc ID',
        ];
    }

    /*public static function getProfile()
    {
        return static::findOne(['user_id' =>  Yii::$app->user->identity->id]);
    }*/

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->phone) {
                $ex = explode('+', $this->phone)[1];
                $this->phone = '+' . $ex;
            }
            return true;
        } else {
            return false;
        }
    }

    public static function autoCreateProfile(Persons $person, $userId, $phone)
    {
        $model = new self();
        $model->name = $person->name;
        $model->surname = $person->surname;
        $model->phone = $phone;
        $model->birthday = $person->birthday;
        $model->gen = Amadeus::getGen($person->prefix);
        $model->doc_type = $person->doc_type;
        $model->doc_id = $person->doc_id;
        $model->doc_expire_date = $person->expire_date;
        $model->doc_issue_country = $person->doc_issue_country;
        $model->user_id = $userId;
        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        }
    }

    public static function autoUpdateProfile(Persons $person, $userId, $phone)
    {
        $model = self::findOne(['user_id' => $userId]);
        $model->name = $model->name ? $model->name : $person->name;
        $model->surname = $model->surname ? $model->surname : $person->surname;
        $model->phone = $model->phone ? $model->phone : $phone;
        $model->birthday = $model->birthday ? $model->birthday : $person->birthday;
        $model->gen = $model->gen ? $model->gen : Amadeus::getGen($person->prefix);
        $model->doc_type = $model->doc_type ? $model->doc_type : $person->doc_type;
        $model->doc_id = $model->doc_id ? $model->doc_id : $person->doc_id;
        $model->doc_expire_date = $model->doc_expire_date ? $model->doc_expire_date : $person->expire_date;
        $model->doc_issue_country = $model->doc_issue_country ? $model->doc_issue_country : $person->doc_issue_country;
        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        }
    }

    public static function getProfile(&$profileData)
    {
        /* @noinspection PhpUndefinedFieldInspection */
        $model = self::findOne(['user_id' => Yii::$app->user->identity->id]);
        if ($model !== null) {
            $profileData['surname'] = $model->surname ? $model->surname : '';
            $profileData['name'] = $model->name ? $model->name : '';


            if ($model->birthday) {
                $profFormatBirthday = explode('-', $model->birthday);
                $profileData['birthday'] = $profFormatBirthday[2] . '-' . $profFormatBirthday[1] . '-' . $profFormatBirthday[0];
            }
//            $profileData['birthday'] = $model->birthday ? $model->birthday : '';
            $profileData['phone'] = $model->phone ? $model->phone : '';
            /* @noinspection PhpUndefinedFieldInspection */
            $profileData['email'] = Yii::$app->user->identity->email;
            $profileData['gen'] = $model->gen ? $model->gen : '';
            $profileData['docType'] = $model->doc_type ? $model->doc_type : '';
            $profileData['docId'] = $model->doc_id ? $model->doc_id : '';
            $profileData['docIssueCountry'] = $model->doc_issue_country ? $model->doc_issue_country : '';
//            $profileData['docExpireDate'] = $model->doc_expire_date ? $model->doc_expire_date : '';
            if ($model->doc_expire_date) {
                $profFormatDocExpire = explode('-', $model->doc_expire_date);
                $profileData['docExpireDate'] = $profFormatDocExpire[2] . '-' . $profFormatDocExpire[1] . '-' . $profFormatDocExpire[0];
            }
        }

    }
}
