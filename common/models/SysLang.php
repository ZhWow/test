<?php

namespace common\models;

use common\models\base\SysLang as BaseSysLang;
use yii\helpers\ArrayHelper;
use Yii;

class SysLang extends BaseSysLang
{
    static $current = null;

    public static function lang()
    {
        $lang = self::find()->all();
        return ArrayHelper::map($lang, 'id', 'name');
    }

    public static function langLocal()
    {
        $lang = self::find()->all();
        return ArrayHelper::map($lang, 'local', 'name');
    }

    public static function langByLocal($default = false)
    {
        $cookies = Yii::$app->request->cookies;
        $language = $cookies->getValue('language');
        if (($langModel = self::find()->where(['local' => $language])->one()) === null || $default) {
            $langModel = self::findOne(['default' => 1]);
        }
        return $langModel->id;
    }

    //Получение текущего объекта языка
    static function getCurrent()
    {
        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    //Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->local;
    }

    //Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return self::find()->where('`default` = :default', [':default' => 1])->one();
    }

    //Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = self::find()->where('url = :url', [':url' => $url])->one();
            if ( $language === null ) {
                return null;
            }else{
                return $language;
            }
        }
    }
}