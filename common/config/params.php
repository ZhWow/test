<?php
return [
    'adminEmail' => 'flight-form@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
