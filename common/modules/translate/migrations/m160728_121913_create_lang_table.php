<?php

namespace common\modules\translate\migrations;

use yii\db\Migration;

class m160728_121913_create_lang_table extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sys_lang}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
            'local' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'default' => $this->smallInteger()->notNull()->defaultValue(0),
            'date_update' => $this->integer()->notNull(),
            'date_create' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->batchInsert('sys_lang', ['url', 'local', 'name', 'default', 'date_update', 'date_create'], [
            ['kz', 'kz-KZ', 'Қазақша', 0, time(), time()],
            ['ru', 'ru-RU', 'Русский', 1, time(), time()],
            ['en', 'en-US', 'English', 0, time(), time()],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%sys_lang}}');
    }
    
}
