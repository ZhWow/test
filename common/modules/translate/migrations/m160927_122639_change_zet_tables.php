<?php

namespace common\modules\translate\migrations;

use yii\db\Migration;

class m160927_122639_change_zet_tables extends Migration
{
    public function init()
    {
        $this->db = 'dbLog';
        parent::init();
    }

    public function up()
    {
        /** @noinspection SqlDialectInspection */
        /** @noinspection SqlNoDataSourceInspection */
        $sql = <<< sql
ALTER TABLE `zet_source_message_arch`
	ALTER `version` DROP DEFAULT;
ALTER TABLE `zet_source_message_arch`
	CHANGE COLUMN `version` `version` INT(11) NOT NULL AFTER `message`,
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`id`, `version`);

ALTER TABLE `zet_message_arch`
	DROP FOREIGN KEY `fk_zet_source_message_message`,
	ADD INDEX `fk_zet_source_message_message` (`id`, `version`);
ALTER TABLE `zet_message_arch`
	ADD CONSTRAINT `fk_zet_source_message_message` FOREIGN KEY (`id`, `version`) REFERENCES `zet_source_message_arch` (`id`, `version`) ON DELETE CASCADE;

sql;

        $this->execute($sql);
        return true;
    }

    public function down()
    {
        echo "m160927_122639_change_zet_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
