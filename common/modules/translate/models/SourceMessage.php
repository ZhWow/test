<?php

namespace common\modules\translate\models;

use Yii;
use yii\db\ActiveQuery;
use backend\modules\translate\services\ExcelService;
use backend\modules\translate\services\RevertTranslateService;

/**
 * This is the model class for table "sys_source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 * @property SysLang[] $allSysLang
 */
class SourceMessage extends Translatable
{
    const FILL_EMPTY = 10;
    const REPLACE = 0;
    const SKIP = 5;
    const SCENARIO_REVERT = 'revert';

    public $excelFile;

    use ExcelService;
    use RevertTranslateService;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 3],
            [['category'], 'required', 'message' => Yii::t('app', 'Enter category or write "app"')],
            [['kz', 'ru', 'en'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => Yii::t('trn', 'Category'),
            'message' => Yii::t('trn', 'Message'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REVERT] = ['id', 'message', 'category'];
        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }

    /**
     * Checks to Cyrillic message field, if there is a Cyrillic returns 1 otherwise 0
     * @param SourceMessage $model
     * @return int
     */
    public static function checkToCyrillic(SourceMessage $model)
    {
        return preg_match('/[а-яА-ЯёЁ]/iu', $model->message);
    }

    /**
     * Gives language translation of which is specified in the parameters,
     * if there is translation. If not then an empty string
     * @param string $lang format [ kz-KZ ]
     * @return string
     */
    public function getTranslations($lang)
    {
        /* @var $messageModel Message */
        $messageModel = $this->findMessage($lang)->one();
        return $messageModel !== null ? $messageModel->translation : '';
    }

    /**
     * @param string $lang
     * @return ActiveQuery Message
     */
    public function findMessage($lang)
    {
        return Message::find()->where(['id' => $this->id, 'language' => $lang]);
    }

    /**
     * @deprecated new version this method - existingTranslate()
     */
    public function existing_translates()
    {
        $this->existingTranslate();
    }

    /**
     * Sets the properties of this class, the values ​​of a column from the Message translation table
     */
    public function existingTranslate()
    {
        $allSysLang = self::getAllSysLang();
        foreach ($allSysLang as $sysLang) {
            /* @var $sysLang SysLang */
            $this[$sysLang['url']] = $this->getMessage($sysLang->local)->translation;
        }
    }

    /**
     * Returns a single Message. Descendant of the Source Message.
     * If it exists, if not create it and returns it
     * @param string $language format [en-US]
     * @return array|Message|null|\yii\db\ActiveRecord
     */
    public function getMessage($language)
    {
        $message = $this->findMessage($language)->one();
        if ($message === null) {
            $message = new Message();
            $message->language = $language;
            $message->id = $this->id;
        }
        return $message;
    }

    /**
     * Saves the properties of this class of languages. Which can be translated,
     * even if there still retain a blank line in the table sys_message.
     * Properties translations are added in the form
     * @return bool
     */
    public function saveTranslates()
    {
        $success = true;
        $allSysLang = self::getAllSysLang();
        foreach ($allSysLang as $sysLang) {
            /* @var $sysLang SysLang */
            $success = $this->writeTranslates($sysLang->local, $this[$sysLang->url]) ? true : false;
        }
        return $success;
    }

    /**
     * @param string $language
     * @param string $translation
     * @return bool
     */
    public function writeTranslates($language, $translation)
    {
        /* @var $message Message */
        $message = $this->getMessage($language);
        $message->translation = $translation;
        return $message->save();
    }

    /**
     * The total number of languages ​​in the system
     * @return int
     */
    protected static function langCount()
    {
        return count(self::getAllSysLang());
    }

    // TODO
    public static function fixMessages()
    {
        $languages = ['kz-KZ', 'en-US', 'ru-RU'];
        foreach ($languages as $lang) {
            $source_messages = SourceMessage::find()->all();
            /* @var $src_message SourceMessage */
            foreach ($source_messages as $src_message) {
                $message = Message::find()->where(['id' => $src_message->id])->andWhere(['language' => $lang])->one();
                if ($message === null) {
                    $message = new Message();
                    $message->id = $src_message->id;
                    $message->language = $lang;
                    if (!$message->save()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
