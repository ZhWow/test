<?php

namespace common\modules\translate\models;

use common\modules\translate\services\AllSysLang;
use yii\db\ActiveRecord;

/**
 * This class will be parent of all classes which have translations
 *
 * @property string $kz
 * @property string $ru
 * @property string $en
 * @static array $excludeSysLang Languages ​​to be excluded.
 * @static array $systemLang This field of language message from SourceMessage.
 */
abstract class Translatable extends ActiveRecord
{
    const DEFAULT_CATEGORY = 'app';

    public $kz;
    public $en;
    public $ru;
    // public $tt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kz', 'en', 'ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * It returns an array SysLang, excluding the languages ​​prescribed in excludeSysLang.
     * if passed parameter is true then even eliminates system language
     * @param bool $excludeSystemLang It eliminates the default language. System lang - en
     * @return array
     */
    public static function getAllSysLang($excludeSystemLang = false)
    {
        $allSysLang = AllSysLang::getInstance($excludeSystemLang);
        return $allSysLang->getLangs();
    }

    public static function simpleShortLangName()
    {
        $allLang = self::getAllSysLang();
        $result = [];
        foreach ($allLang as $item) {
            $result[$item->url] = $item->url;
        }
        return $result;

    }

}