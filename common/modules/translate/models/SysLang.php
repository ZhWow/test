<?php

namespace common\modules\translate\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sys_lang".
 *
 * @property integer $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property integer $default
 * @property integer $date_update
 * @property integer $date_create
 */
class SysLang extends ActiveRecord
{

    const TEST_URL = 'tt';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'local', 'name', 'date_update', 'date_create'], 'required'],
            [['default', 'date_update', 'date_create'], 'integer'],
            [['url', 'local', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'local' => Yii::t('app', 'Local'),
            'name' => Yii::t('app', 'Name'),
            'default' => Yii::t('app', 'Default'),
            'date_update' => Yii::t('app', 'Date Update'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }

    public static function langs()
    {
        $langs = self::find()->all();
        return ArrayHelper::map($langs, 'id', 'name');
    }

    public static function langsLocal()
    {
        $langs = self::find()->all();
        return ArrayHelper::map($langs, 'local', 'name');
    }

    public static function langByLocal($default = false)
    {
        $cookies = Yii::$app->request->cookies;
        $language = $cookies->getValue('language');
        if (($langModel = self::find()->where(['local' => $language])->one()) === null || $default) {
            $langModel = self::findOne(['default' => 1]);
        }
        return $langModel->id;
    }
}
