<?php

namespace common\modules\translate\services;

use common\modules\translate\models\SysLang;

class AllSysLang
{
    private static $instance;

    protected static $excludeSysLang = ['tt']; // sysLang->url
    protected static $systemLang = ['en']; // sysLang->url

    private $_excludeSysLang = [];

    private function __construct($excludeSystemLang = false)
    {
        $allSysLang = SysLang::findBySql('
            SELECT `url`, `local`, `name` 
            FROM `sys_lang`')
            ->all();

        foreach ($allSysLang as $sysLang) {
            /* @var $sysLang SysLang */
            if (in_array($sysLang->url, self::$excludeSysLang)) { continue; }
            if ($excludeSystemLang && in_array($sysLang->url, self::$systemLang)) { continue; }
            array_push($this->_excludeSysLang, $sysLang);
        }
    }

    public static function getInstance($excludeSystemLang = false)
    {
        if (empty(self::$instance)) {
            self::$instance = new self($excludeSystemLang);
        }
        return self::$instance;
    }

    public function getLangs()
    {
        return $this->_excludeSysLang;
    }
}