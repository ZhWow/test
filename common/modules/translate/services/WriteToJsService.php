<?php

namespace common\modules\translate\services;

use Yii;
use common\modules\translate\models\SourceMessage;
use common\modules\translate\models\Message;

class WriteToJsService
{
    public function execute()
    {
        $fileName = Yii::getAlias('@frontend') . '/web/js/translate/message.js';
        $recordsArray = Yii::$app->db->createCommand('
                                                select * from ' . SourceMessage::tableName() . ' sm
                                                left join ' . Message::tableName() . ' m
                                                on m.id=sm.id
                                                where category=\'_js\'
                                                    ')->queryAll();

        $newArray = [];

        foreach ($recordsArray as $record) {
            $newArray[$record['language']][$record['message']] = $record['translation'];//change structure of array
        }

        if (file_exists($fileName)) {
            $date = date('ymHi');
            rename($fileName, $fileName . $date);
        }

        $file = fopen($fileName, "w+");
        foreach ($newArray as $key => $item) {
            $varName = substr($key, 0, 2);
            fwrite($file, "var {$varName} = " . json_encode($item) . ';');
        }
        fclose($file);
    }
}