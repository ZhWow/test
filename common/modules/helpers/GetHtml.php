<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 12.09.2017
 * Time: 18:48
 */

namespace common\modules\helpers;


class GetHtml
{
    public function getHtml($keys, $template, $isForeach = true)
    {
        if ($isForeach === true) {
            $template = str_replace('<foreach>', '', $template);
            $template = str_replace('</foreach>', '', $template);
        }

        $pattern = [];
        $replacement = [];

        foreach ($keys as $key => $item) {
            $pattern[] = '#{' . $key . '}#';
            $replacement[] = $item;
        }

        $htmlResponse = preg_replace($pattern, $replacement, $template);
        return $htmlResponse;
    }

    public function getForeach($keys, $template, $getIf = false)
    {
        $foreachPattern = '#<foreach>(.+?)</foreach>#s';
        preg_match($foreachPattern, $template, $match);
        $foreachParse = str_replace('<foreach>', '', $match[0]);
        $foreachParse = str_replace('</foreach>', '', $foreachParse);
        $htmlResponse = NULL;
        $foreachParse = htmlspecialchars($foreachParse);

        foreach ($keys as $keyValue) {
            if ($getIf) {
                $htmlResponseForGetIf = $this->getIf($keyValue, $foreachParse);
                $htmlResponse .= $this->getHtml($keyValue, $htmlResponseForGetIf);
            } else {
                $htmlResponse .= $this->getHtml($keyValue, $foreachParse);
            }
        }

        $finalTemplate = preg_replace($foreachPattern, $htmlResponse, $template);

        return htmlspecialchars_decode($finalTemplate);
    }

    public function getFor($keys = null, $count, $template, $getForeachKeys = null)
    {
        $foreachPattern = '#<for>(.+)</for>#s';
        preg_match($foreachPattern, $template, $match);
        $foreachParse = str_replace('<for>', '', $match[0]);
        $foreachParse = str_replace('</for>', '', $foreachParse);
        $htmlResponse = NULL;
        $foreachParse = htmlspecialchars($foreachParse);

        for ($i = 0; $i <= $count - 1; $i++) {
            if ($keys != null) {
                $htmlResponse .= $this->getHtml($keys, $foreachParse);
            } elseif ($getForeachKeys != null) {
                $htmlResponse .= $this->getHtml($getForeachKeys[$i], $foreachParse);
            } else {
                $htmlResponse .= $foreachParse;
            }
        }

        $finalTemplate = preg_replace($foreachPattern, $htmlResponse, $template);

        return htmlspecialchars_decode($finalTemplate);
    }

    public function getIf($keys, $template, $bool = true)
    {
        $finalTemplate = '';

        if (!$bool) {
            foreach ($keys as $key => $value) {
                $keys[$key] = ($value == "true") ? true : false;
            }
        }

        foreach ($keys as $key => $value) {

            $foreachPattern = '#<if{' . $key . '}>(.+?)</if>#s';

            if ($finalTemplate == '')
                preg_match_all($foreachPattern, $template, $match);
            else
                preg_match_all($foreachPattern, $finalTemplate, $match);


            if ($value == true) {
                if ($finalTemplate == '')
                    $finalTemplate = preg_replace($foreachPattern, $match[1][0], $template);
                else
                    $finalTemplate = preg_replace($foreachPattern, $match[1][0], $finalTemplate);
            } else {
                if ($finalTemplate == '')
                    $finalTemplate = preg_replace($foreachPattern, "", $template);
                else
                    $finalTemplate = preg_replace($foreachPattern, "", $finalTemplate);
            }
            unset($foreachPattern);
        }

        return htmlspecialchars_decode($finalTemplate);
    }
}