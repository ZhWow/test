<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 13.09.2017
 * Time: 1:00
 */

namespace common\modules\helpers;

use Yii;
use kartik\mpdf\Pdf;

class CreatePdf extends Pdf
{
    public function createPdf($fileName, $template)
    {
        $fileNamePdf = Yii::getAlias('@modules') . '/qway/files/' . $fileName . '.pdf';
        $api = $this->getApi();
        $api->WriteHTML($template);
        $api->output($fileNamePdf, 'F');
        return $fileNamePdf;
    }

    public static function existPayFilePdf($orderNumber)
    {
        $fileName = Yii::getAlias('@modules') . '/qway/files/pay_' . $orderNumber . '.pdf';
        if (file_exists($fileName)) {
            return $fileName;
        } else {
            return false;
        }
    }
}