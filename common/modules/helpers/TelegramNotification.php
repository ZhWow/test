<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 15.09.2017
 * Time: 23:33
 */

namespace common\modules\helpers;

use yii\base\Model;

class TelegramNotification extends Model
{
    public $passengers;
    public $eTicketNumber;
    public $PNR;
    public $departureDate;
    public $route;
    public $email;
    public $phone;
    public $totalPrice;
    public $freeText;

    const PRODUCTION_KEY = '-235944290';
    const ERROR_KEY = '-149490530';
    const KARIM_KEY = '885982';

    public function setBookingDetails($bookingDetails)
    {
        if($bookingDetails["passengers"]) $this->passengers = $bookingDetails["passengers"];
        if($bookingDetails["PNR"]) $this->PNR = $bookingDetails["PNR"];
        if($bookingDetails["departureDate"]) $this->departureDate = $bookingDetails["departureDate"];
        if($bookingDetails["route"]) $this->route = $bookingDetails["route"];
        if($bookingDetails["email"]) $this->email = $bookingDetails["email"];
        if($bookingDetails["totalPrice"]) $this->totalPrice = $bookingDetails["totalPrice"];

        return $this;
    }

    public static function prepareBookDetails($dataForTelegram)
    {
        $response = [];
        $response["passengers"] = NULL;
        $response["PNR"] = NULL;
        $response["departureDate"] = NULL;
        $response["email"] = NULL;
        $response["totalPrice"] = NULL;
        $response["route"] = NULL;

        $dataForTelegram = $dataForTelegram[0];

        foreach ($dataForTelegram["PAX"] as $key => $pax) {
            $breakpoint = ($key == count($dataForTelegram["PAX"]) - 1 ) ? "" : " ; ";

            $response["passengers"] .= $pax["prefix"] . " " . $pax["name"] . " " . $pax["surname"] . $breakpoint;
        }

        $response["PNR"] = $dataForTelegram["FLIGHTINFO"]["pnr_no"];

        foreach ($dataForTelegram["SEGMENTS"] as $key => $segment) {
            $breakpoint1 = ($key == count($dataForTelegram["SEGMENTS"]) - 1 ) ? "" : " ; ";
            foreach ($segment as $segmentKey => $segmentItem) {
                $breakpoint2 = ($segmentKey == count($segment) - 1 ) ? "" : " , ";
                $response["route"] .= $segmentItem["departure_airport"] . " - " . $segmentItem["arrival_airport"] . $breakpoint1 . $breakpoint2;
            }
        }

        $response["departureDate"] = date("H:i d.m.Y", strtotime($dataForTelegram["SEGMENTS"][0][0]["departure_date_time"]));
        $response["email"] = $dataForTelegram["PAX"][0]["email"];
        $response["totalPrice"] = $dataForTelegram["PRICEINFO"]["total_fare_amount"];

        return $response;
    }
}