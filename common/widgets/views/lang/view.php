<?php

use yii\helpers\Html;
?>



<ul class="dropdown-menu select-lang">
    <li>
        <ul class="menu">
            <?php foreach ($langs as $lang): ?>

                <?php $active = $current->url == $lang->url ? ' active ' : ''; ?>

                <li  class="<?= $active ?>"  data-lang="<?= $lang->url ?>">
                    <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
                </li>
            <?php endforeach;?>
        </ul>
    </li>
</ul>



