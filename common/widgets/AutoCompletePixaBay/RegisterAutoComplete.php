<?php

namespace common\widgets\AutoCompletePixaBay;

use yii\web\AssetBundle;

/**
 * jQuery AutoComplete PixaBay
 * Version 1.0.7 - 2015/08/15
 * url https://github.com/Pixabay/jQuery-autoComplete
 */
class RegisterAutoComplete extends AssetBundle
{
    public $sourcePath = '@common/widgets/AutoCompletePixaBay';

    public $css = [
        'css/jquery.auto-complete.css'
    ];

    public $js = [
        'js/jquery.auto-complete.min.js'
    ];
}