<?php

namespace common\helpers;

use Yii;

class PultsName
{
    public static function getName($SubSiteCode)
    {
        $pultsNames = Yii::$app->params['providers']['epower']['epowerPults'];
        $pultsNameKeys = array_keys($pultsNames);
        $searchCol = array_column($pultsNames, 'SubSiteCode');
        $key = array_search((string)$SubSiteCode, $searchCol);
        $result = $pultsNameKeys[$key];
        return $result;
    }

}