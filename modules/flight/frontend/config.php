<?php
return [
	'params' => [
        'error_codes' => [
            'am_e000' => \Yii::t('app', 'Server is not responding'),
            'am_e001' => \Yii::t('app', 'Timeout expired'),
        ],
        'soap_param' => [
            'PROVIDER_TYPE' => 'OnlyAmadeus',
            'REFUNDABLE_TYPE' => 'AllFlights',
            'WSDL_STG' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx?WSDL',
            'CONNECT_TIMEOUT' => 120,
            'SOAP_SUB_SITE_CODE' => 'Bestar',
            'WS_USER_NAME' => 'WstravelBusinessKZ1',
            'WS_PASSWORD' => 'G1Ladm1N*!!@Best2017',
            'SOAP_HEADER_NS' => 'http://epowerv5.amadeus.com.tr/WS',
        ],
        'passenger_code' => [
            'ADULTS' => 'ADT',
            'CHILDREN' => 'CHD',
            'INFANTS' => 'INF',
        ],
        'session_var_names' => [
            'tokenId' => '__token_id_',
            'flightId' => '__flight_id_',
            'cabinClass' => '__cabin_class_'
        ]
	]
];
