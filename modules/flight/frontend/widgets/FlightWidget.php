<?php

namespace modules\flight\frontend\widgets;

use modules\flight\frontend\widgets\assets\SearchResultAsset;
use modules\flight\frontend\providers\amadeus\components\Flight;
use modules\flight\frontend\providers\amadeus\components\Option;
use modules\flight\frontend\providers\amadeus\components\Segment;
use yii;
use yii\base\Widget;
use yii\helpers\Html;

class FlightWidget extends Widget
{
    public $items = [];
    public $airline = [];
    public $cabin = '';
    public $bestAmountKey = null;
    public $bestElapsedKey = null;
    private $isVisibleFilter = 0;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        SearchResultAsset::register($this->getView());
        echo $this->renderItems();
    }

    private function renderItems()
    {
        $items = $this->items;
        $content = $this->renderInformationBlock();
        $content .= $this->renderBestFlights();
        $this->renderItem($items, $content);
        return $content;
    }

    private function renderItem($items, &$content)
    {
        foreach ($items as $key => $item) {
            /* @var $item Flight */

            $this->renderOption($content, $key, $item);

        }
    }

    /**
     * @param string $content
     * @param int $key
     * @param Flight $item
     */
    private function renderOption(&$content, $key, $item)
    {
//        \Yii::error(count($item->options), 'flight');
        /* @var $option Option */
        foreach ($item->options as $option) {

            $segmentCount = count($option->segments);
            $isMulti = ($segmentCount > 1) ? true : false;

            if ($this->isVisibleFilter === 1) {
                $segment = $item->options[0]->getFirstSegment();
                $segmentLast = $item->options[0]->getLastSegment();


                $content .= $this->renderFilter();
                $content .= $this->renderCombinationBlock();


                $segment->getDepartureAirport()->city->getName();
                $content .= Html::beginTag('div', ['class' => 'backward-header']);
                    $content .= Html::tag('i', '', ['class' => 'fa fa-plane fa-departure', 'aria-hidden' => true]);
                    $content .= Html::tag('div', 'Выберите рейс вылета', ['class' => 'backward-header-txt']);
                    $content .= Html::beginTag('div', ['class' => 'backward-content']);
                        $content .= Html::beginTag('div', ['class' => 'backward-departure-cont']);
                            $content .= Html::tag('strong', $segment->getDepartureAirport()->city->getName());
                        $content .= Html::endTag('div');
                        $content .= Html::beginTag('div', ['class' => 'backward-img-cont']);
                            $content .= '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>';
                        $content .= Html::endTag('div');

                        $content .= Html::beginTag('div', ['class' => 'backward-arrival-cont']);
                            $content .= Html::tag('strong', $segmentLast->getArrivalAirport()->city->getName());
                        $content .= Html::endTag('div');

                    $content .= Html::endTag('div');
                $content .= Html::endTag('div');



            }

            $isReturnable = $this->isReturnable($item->ticketDesignatorCode ,$item->ticketDesignators);

            $session = \Yii::$app->session;

            $content .= Html::beginTag('div',[
                'class' => 'item-wrapper search-result-item item-search-' . $key,
                'data' => [
                    'amount' => $item->sequence->totalFare,
                    'elapsed' => $item->options[0]->elapsedTime,
                    'departure-time' => str_replace(':', '', $option->getFirstSegment()->getDepartureTime()),
                    'airline' => $option->getFirstSegment()->airlineCode,
                    'multi' => (int) ($segmentCount > 1),
                    'key' => $key,
                    'returnable' => $isReturnable ? 0 : 1,
                    'trip-id' => $item->options[0]->segments[0]->flightNumber,
                    'backward-trip' => $session->get('backward_trip'),
                    'book-ready' => $session->get('backward_trip') ? 0 : 1,
                ],
            ]);

            /*$content .= Html::beginTag('div', ['class' => 'wrap-info-content']);
            $content .= $this->renderInfoContent($isReturnable);
            $content .= Html::endTag('div');*/

//            $content .= Html::beginTag('div', ['class' => 'wrap-main-content']);

                $content .= $this->logoBlock($option);
                $content .= $this->contBlock($option);
                $content .= $this->amountBlock($item, $option, $key);


            //$content .= $this->_header();

//                if ($isMulti) {
//                    $content .= Html::beginTag('div', ['class' => 'item-content-multi',]);
//                    $i = 1;
//                    foreach ($option->segments as $segment) {
//                        /* @var $segment Segment */
//                        $isAmountVisible = $i === $segmentCount ? true : false;
//                        $content .= Html::beginTag('div', [
//                            'class' => 'multi-flight flight-number',
//                            'data' => ['trip' => $segment->flightNumber]
//                        ]);
//                        $content .= $this->renderSegment($segment, $option, $item, $isAmountVisible, $key);
//                        $content .= Html::endTag('div');
//                        $content .= '<hr class="flight-border">';
//                        $i++;
//                    }
//                    $content .= Html::endTag('div');
//                } else {
//                    $isAmountVisible = true;
//                    $content .= Html::beginTag('div', [
//                        'class' => 'item-content flight-number',
//                        'data' => ['trip' => $option->getFirstSegment()->flightNumber]
//                    ]);
//                    $content .= $this->renderSegment($option->getFirstSegment(), $option, $item, $isAmountVisible, $key);
//                    $content .= Html::endTag('div');
//                }
//                $content .= Html::endTag('div');
                $this->isVisibleFilter++;
            $content .= Html::endTag('div'); // #wrap-main-content
        }
    }


    private function logoBlock($option)
    {
        $segment = $option->getFirstSegment();
        return '<div class="item-logo">
                    <header><span>Перевозчик:</span></header>
                    <div style="width: 100%">

                        <img style="width: 80%; height: 80%" src="/images/airlineslogo/'.$segment->airlineCode.'.png" />

                    </div>
                    <h4>' . $segment->getAirline()->getName() . '</h4>

                </div>';

//        return '<div class="item-logo">
//                    <header><span>Перевозчик:</span></header>
//                    <section class="' . strtolower(str_replace(' ', '-', $segment->getAirline()->getName(true))) . '">
//
//
//                    </section>
//                    <h4>' . $segment->getAirline()->getName() . '</h4>
//
//                </div>';
    }

    private function contBlock($option)
    {
        $segmentCount = count($option->segments);
        /*if ($segmentCount > 2) {
            $ptText = '-я пересадка: ';
        } else {
            $ptText = ' пересадка: ';
        }*/

        $iElement = '';
        $ptText = '-я пересадка: ';
        switch ($segmentCount) {
            case 2:
                $ptText = ' пересадка: ';
                $iElement .= $this->ptElementRender($option->segments, $ptText, [1 => 3]);
            break;
            case 3:
                $iElement .= $this->ptElementRender($option->segments, $ptText, [1 => 2, 4]);
            break;
            case 4:
                $iElement .= $this->ptElementRender($option->segments, $ptText, [1 => 1, 3, 5]);
            break;
            case 5:
                $iElement .= $this->ptElementRender($option->segments, $ptText, [1 => 1, 2, 4, 5]);
            break;
            case 6:
                $iElement .= $this->ptElementRender($option->segments, $ptText, [1 => 1, 2, 3, 4, 5]);
            break;
        }


        /* @var $option Option */
        /* @var $segment Segment */

        $segment = $option->getFirstSegment();
        $segmentLast = $option->getLastSegment();

        return '<div class="item-cont">
                    <header>
                        <div class="left-block">
                            <span>рейс</span>&nbsp<h5>' . $segment->getBort()->getName() . '</h5>
                        </div>
                        <div class="right-block">
                            <span>общее время в пути</span>&nbsp<h5>' . $option->getElapsedTime() . '</h5>
                        </div>


                    </header>
                    <section>
                        <div class="departure-cont port-cont">
                            <header>
                                <span>Откуда</span>
                                <h4>' . $segment->getDepartureAirport()->city->getName() . '</h4>
                            </header>
                            <section>
                                <h2>' . $segment->getDepartureTime() . '</h2>
                                <p>' . $segment->getDepartureDate() . '</p>
                            </section>
                        </div>
                        <div class="transplantation-cont">
                            <i class="fa fa-plane fa-departure" aria-hidden="true"></i>
                            <i class="fa fa-circle-thin fa-departure" aria-hidden="true"></i>' . $iElement .'
                            ' . $this->renderSvg() . '
                            <i class="fa fa-circle-thin fa-arrival" aria-hidden="true"></i>
                            <i class="fa fa-plane fa-arrival" aria-hidden="true"></i>
                        </div>
                        <div class="arrival-cont port-cont">
                            <header>
                                <span>Куда</span>
                                <h4>' . $segmentLast->getArrivalAirport()->city->getName() . '</h4>
                            </header>
                            <section>
                                <h2>' . $segmentLast->getArrivalTime() . '</h2>
                                <p>' . $segmentLast->getArrivalDate() . '</p>
                            </section>
                        </div>
                    </section>
                </div>';
    }

    private function amountBlock($flight, $option, $key)
    {
        $amount = $flight->sequence->getTotalFare();
        $currency = $flight->sequence->currency;
        $segment = $option->getFirstSegment();
        $combinationID = $flight->combinationID;
        $sequenceID = $flight->sequence->sequenceNumber;
        $session = $session = Yii::$app->session;
        $button = (!$session->get('exchangeTicketId') != null) ? '<button id="add-ticket" class="btn btn-info" data-key="' . $key . '" data-combination-id="' . $combinationID . '" data-sequence-id="' . $sequenceID . '">Выбрать</button>' : '<button class="btn btn-info exchange-to" data-key="' . $key . '" data-combination-id="' . $combinationID . '" data-sequence-id="' . $sequenceID . '">Выбрать</button>';
        return '<div class="item-amount">
                    <header>
                        <span>Остался:&nbsp<span> ' . $segment->getFreeSeats() . ' билет</span></span>
                    </header>
                    <section>
                        <div class="amount"><h2>' . $amount . '&nbsp</h2><span>' . $currency . '</span></div>
                        <p>Цена за всех пассажиров</p>
                        ' . $button . '
                </div>';
    }

    private function ptElementRender($segments, $ptText, $count)
    {

        $i = 0;
        $iElement = '';
        /* @var $segment Segment */
        foreach ($segments as $segment) {
            if ($i !== 0) {
                $title = $i . $ptText . $segment->getDepartureAirport()->city->getName();
                $iElement .= '<i class="fa fa-circle protip fa-transplantation-num_' . $count[$i] . ' fa-transplantation-all" aria-hidden="true" data-pt-title="'. $title .'" data-pt-position="top" data-pt-skin="square"></i>';
            }
            $i++;
        }
        return $iElement;
    }

    public static function ptElementR($segments, $ptText, $count)
    {

        $i = 0;
        $iElement = '';
        /* @var $segment Segment */
        foreach ($segments as $segment) {
            if ($i !== 0) {
                $title = $i . $ptText . $segment->getDepartureAirport()->city->getName();
                $iElement .= '<i class="fa fa-circle protip fa-transplantation-num_' . $count[$i] . ' fa-transplantation-all" aria-hidden="true" data-pt-title="'. $title .'" data-pt-position="top" data-pt-skin="square"></i>';
            }
            $i++;
        }
        return $iElement;
    }


    private function renderInformationBlock()
    {
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'information-block']);
            $html .= Html::beginTag('div', ['class' => 'header-information-block']);
                $html .= '<span><i class="material-icons">airplanemode_active</i></span><p>Выберите билет</p>';
            $html .= Html::endTag('div');
            $html .= Html::beginTag('div', ['class' => 'flight-information-block']);
                $html .= 'fkk - aa';
            $html .= Html::endTag('div');
        $html .= Html::endTag('div');
        return $html;
    }



    private function renderSvg()
    {
        return '<svg
                    xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:cc="http://creativecommons.org/ns#"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:svg="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/2000/svg"
                    id="curve-line"
                    version="1.1"
                    viewBox="0 0 67.468968 11.641677"
                    height="44"
                    width="255">
                <defs
                        id="defs2" />
                <metadata
                        id="metadata5">
                    <rdf:RDF>
                        <cc:Work
                                rdf:about="">
                            <dc:format>image/svg+xml</dc:format>
                            <dc:type
                                    rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                            <dc:title></dc:title>
                        </cc:Work>
                    </rdf:RDF>
                </metadata>
                <g
                        transform="translate(0,-285.35828)"
                        id="layer1">
                    <path
                            id="path4485"
                            d="m 0.28568292,296.6392 c 0,0 13.91834808,-10.55666 33.85130408,-10.50599 19.932956,0.0507 32.85568,10.50599 32.85568,10.50599"
                            style="fill:none;stroke:#000000;stroke-width:0.53561896;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:2.14247584, 0.53561896;stroke-dashoffset:0;stroke-opacity:0.5" />
                </g>
            </svg>';
    }

    public static function renderSvgs()
    {
        return '<svg
                    xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:cc="http://creativecommons.org/ns#"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:svg="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/2000/svg"
                    id="curve-line"
                    version="1.1"
                    viewBox="0 0 67.468968 11.641677"
                    height="44"
                    width="255">
                <defs
                        id="defs2" />
                <metadata
                        id="metadata5">
                    <rdf:RDF>
                        <cc:Work
                                rdf:about="">
                            <dc:format>image/svg+xml</dc:format>
                            <dc:type
                                    rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                            <dc:title></dc:title>
                        </cc:Work>
                    </rdf:RDF>
                </metadata>
                <g
                        transform="translate(0,-285.35828)"
                        id="layer1">
                    <path
                            id="path4485"
                            d="m 0.28568292,296.6392 c 0,0 13.91834808,-10.55666 33.85130408,-10.50599 19.932956,0.0507 32.85568,10.50599 32.85568,10.50599"
                            style="fill:none;stroke:#000000;stroke-width:0.53561896;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:2.14247584, 0.53561896;stroke-dashoffset:0;stroke-opacity:0.5" />
                </g>
            </svg>';
    }







    /**
     * @param Segment $segment
     * @param Flight $flight
     * @param Option $option
     * @param bool $isAmountVisible
     * @param int $key
     * @return string
     */
    private function renderSegment($segment, $option, $flight, $isAmountVisible, $key)
    {
        $html = '';
        $html .= $this->renderDepartureBlock($segment);
        $html .= $this->renderArrivalBlock($segment);
        $html .= $this->renderFlightBlock($segment, $option, $isAmountVisible);
        $html .= $this->renderAirlineBlock($segment);
        if ($isAmountVisible) {
            $html .= $this->renderAmountBlock($flight, $key);
        } else {
            $html .= Html::beginTag('div', ['class' => 'amount-content block-item-content']) . Html::endTag('div');
        }
        return $html;
    }

    private function _header()
    {
        return '
        <div class="item-header">
            <span class="search-result-header">Вылет</span>
            <span class="search-result-header">Прибытие</span>
            <span class="search-result-header">Рейс</span>
            <span class="search-result-header search-result-header-airline">Перевозчик</span>
            <span class="search-result-header search-result-header-amount">Цена</span>
        </div>';
    }

    /**
     * @param Segment $segment
     * @return string
     */
    private function renderDepartureBlock($segment)
    {
        return '<div class="departure-content block-item-content">
            <span class="block-child-data">' . $segment->getDepartureDate() . '</span>
            <span class="block-child-data block-time-location">' . $segment->getDepartureTime() . '</span>
            <!--<span class="block-child-data"><?/*= $segment->getDepartureAirport()->getName() */?></span>-->
            <span class="block-child-data">' . $segment->getDepartureAirport()->city->getName() . '</span>
        </div>';
    }

    /**
     * @param Segment $segment
     * @return string
     */
    private function renderArrivalBlock($segment)
    {
        return '<div class="arrival-content block-item-content">
            <span class="block-child-data">' . $segment->getArrivalDate() . '</span>
            <span class="block-child-data block-time-location">' . $segment->getArrivalTime() . '</span>
            <span class="block-child-data">' . $segment->getArrivalAirport()->city->getName() . '</span>
        </div>';
    }

    /**
     * @param Segment $segment
     * @param Option $option
     * @param int $isAmountVisible
     * @return string
     */
    private function renderFlightBlock($segment, $option, $isAmountVisible)
    {

        $elapsedTime = '<span class="block-child-data"></span>';
        if ($isAmountVisible) {
            $elapsedTime = '<span class="block-child-data">' . $option->getElapsedTime() . '</span>';
        }

        $html = '<div class="flight-content block-item-content">
            <span class="block-child-data">' . $this->cabin . '(' . $segment->resBookDesigCode  .')</span>
            <span class="block-child-data free-seats">
                ' . $segment->getFreeSeats() . ' мест
                <br>
                ' . $segment->getBort()->getName() . '
            </span>'
            .
            $elapsedTime
            .
            '
        </div>';

        return $html;
    }

    /**
     * @param Segment $segment
     * @return string
     */
    private function renderAirlineBlock($segment)
    {
        return '<div class="airline-content block-item-content">
            <span class="block-child-data"><img src="' . $segment->getAirline()->getLogo() . '" alt="' . $segment->getAirline()->getName() . '"></span>
            <span class="block-child-data airline-name">' . $segment->getAirline()->getName() . '</span>
        </div>';
    }

    /**
     * @param Flight $flight
     * @param int $key
     * @return string
     */
    private function renderAmountBlock($flight, $key)
    {
        $amount = $flight->sequence->getTotalFare();
        $currency = $flight->sequence->currency;
        $combinationID = $flight->combinationID;
        $sequenceID = $flight->sequence->sequenceNumber;
        return '<div class="amount-content block-item-content">
            <span class="block-child-data block-amount-data">' . $amount . ' ' . $currency . '</span>
            <span>
                <button id="add-ticket" class="btn btn-info" data-key="' . $key . '" data-combination-id="' . $combinationID . '" data-sequence-id="' . $sequenceID . '">Выбрать</button>
            </span>
        </div>';
    }

    private function renderFilter()
    {
        $html = '';
        $html .= Html::beginTag('div', ['id' => 'search-flight-filter']);
        $html .= Html::tag('h5', Html::encode('Отсортировать варианты перелета по:'));
        $html .= Html::beginTag('div', ['class' => 'content']);

        $html .= Html::button('Цена <i class="fa" aria-hidden="true"></i>', [
            'id' => 'search-flight-filter-amount',
            'class' => 'btn btn-default item-filter',
            'data-next-sort' => 'min',
            'data-state' => 'none',
        ]);

        $html .= Html::dropDownList('returnable', null, [
            '1' => 'Возвратные',
            '0' => 'Невозвратные',
        ], [
            'prompt' => 'Возвратный/невозвратный',
            'class' => 'form-control select-filter item-filter',
            'id' => 'search-flight-filter-returnable',
        ]);

        $html .= Html::button('Время <i class="fa" aria-hidden="true"></i>', [
            'id' => 'search-flight-filter-elapsed',
            'class' => 'btn btn-default item-filter',
            'data-next-sort' => 'min',
            'data-state' => 'none',
        ]);
        /*$html .= Html::button('Времени <i class="fa" aria-hidden="true"></i>', [
            'id' => 'search-flight-filter-departure-time',
            'class' => 'btn btn-default item-filter',
            'data-next-sort' => 'min',
            'data-state' => 'none',
        ]);*/

        /*$html .= Html::dropDownList('airline', null, $this->airline, [
            'prompt' => 'Авиакомпании',
            'class' => 'form-control select-filter item-filter',
            'id' => 'search-flight-filter-airline-select',
        ]);*/

        $html .= Html::dropDownList('round', null, [
            '0' => 'Прямые',
            '1' => 'Транзитные',
        ], [
            'prompt' => 'Все рейсы',
            'class' => 'form-control select-filter item-filter',
            'id' => 'search-flight-filter-airline-round',
        ]);



        $html .= Html::endTag('div');
        $html .= Html::endTag('div');
        return $html;
    }

    private function renderBestFlights()
    {
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'best-flight']);
            $html .= $this->renderBestFlight('amount');
            $html .= $this->renderBestFlight('elapsed');
        $html .= Html::endTag('div');
        return $html;
    }

    private function renderBestFlight($type)
    {
        $txt = $type === 'amount' ? 'Самый дешевый' : 'Самый удобный';
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'best-' . $type]);
        $html .= Html::beginTag('header');
        $html .= Html::beginTag('div', ['class' => 'title']);
        $html .= Html::encode($txt);
        $html .= Html::endTag('div');
        /*$html .= Html::beginTag('div', ['class' => 'count-result']);
        $html .= Html::endTag('div');*/
        $html .= Html::endTag('header');


        $html .= Html::beginTag('div', ['class' => 'recommendation-count']);
        $html .= Html::tag('span', '43');
        $html .= '&nbsp;варианта';
        $html .= Html::endTag('div');


        $html .= Html::endTag('div');
        return $html;
    }

    private function renderInfoContent($output)
    {
        $html = '';
        if ($output) {
            $html .= Html::tag('i', '', ['class' => 'fa fa-exclamation-circle ticket-designator', 'aria-hidden' => 'true']);
            $html .= Html::beginTag('div', ['class' => 'ticket-designator-message']);
            $html .= Html::encode($output);
            $html .= Html::endTag('div');
        }
        return $html;
    }

    private function isReturnable($ticketDesignatorCode, $ticketDesignators)
    {
        $result = '';
        $codes = explode(';', $ticketDesignatorCode);
        $messages = explode(';', $ticketDesignators);
        foreach ($codes as $key => $code) {
            $cd = explode('|', $code);
            if (in_array('PEN', $cd)) {
                $result = $messages[$key];
            }
        }
        return $result;
    }

    private function renderCombinationBlock()
    {
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'combination-block']);
        $html .= Html::endTag('div');
        return $html;
    }

}