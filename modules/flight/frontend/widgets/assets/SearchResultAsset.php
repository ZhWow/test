<?php

namespace modules\flight\frontend\widgets\assets;

use yii\web\AssetBundle;

class SearchResultAsset extends AssetBundle
{
    public $sourcePath = '@modules/flight/frontend/widgets/assets';
    public $css = [
    ];
    public $js = [
        'js/render-search-result.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}