<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 13.09.2017
 * Time: 1:40
 */

namespace modules\flight\frontend\providers\mail;

use Yii;
use yii\base\Model;

class SendTicket extends Model
{
    public function sendTicket($email, $attachment, $ticket)
    {
        $mail =  Yii::$app
            ->mailer
            ->compose(
                ['html' => 'send-ticket'],
                ['ticket' => $ticket]
            )
            ->setFrom(['digital@qway.kz' => 'Qway no-reply'])
            ->setTo($email)
            ->setSubject('Выписка электронного билета');
        $mail->attach($attachment);
        return $mail->send();
    }

    public function sendPaymentReceipt($email, $attachment, $payOrder)
    {
        $mail =  Yii::$app
            ->mailer
            ->compose(
                ['html' => 'send-payment-receipment'],
                ['payOrder' => $payOrder]
            )
            ->setFrom(['digital@qway.kz' => 'Qway no-reply'])
            ->setTo($email)
            ->setSubject('Платёжная квитанция');
        $mail->attach($attachment);
        return $mail->send();
    }
}