<?php

namespace modules\flight\frontend\providers\amadeus\methods;

use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\providers\amadeus\components\TravelerInfo;

class CreateTicket extends Method
{
    public static function run($resultticketExistInTBooks, \SoapClient $soap)
    {

        //$soapClient = AmadeusSoapClient::getInstance(true);
        $param_data = self::getRequestOptimization($resultticketExistInTBooks);

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->CreateTicket($param_data);
        $firstResult = self::getResponseOptimization($soap->__getLastResponse());
        $finalParamData = self::getRequestOptimizationFinal($firstResult['ReferenceNumber']);

        if ( $firstResult->Errors && $firstResult->Errors->Error["ShortText"] != "EPowerException.Price charge required" ) {
            /* @noinspection PhpUndefinedMethodInspection */
            $soap->SignOut();
            return $firstResult;
        }

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->CreateTicket($finalParamData);
        $finalResult = self::getResponseOptimization($soap->__getLastResponse());

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->SignOut();

        return $finalResult;
    }

    private static function getRequestOptimization($resultticketExistInTBooks)
    {
        $param_data = [];
/*        $session = Yii::$app->session;
        $pnrNO = $session->get('pnrNO');*/
        /*$travelers = TravelerInfo::find()->where(['pnr_no' => $pnrNO])->all();*/

        $param_data['OTA_AirBookRQ']['BookingReferenceID']['ID_Context'] = $resultticketExistInTBooks['pnrNO'];
        $i = 0;
        foreach ($resultticketExistInTBooks['travelerInfo'] as $traveler) {
            /* @var $traveler TravelerInfo */
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['PassengerTypeCode'] = $traveler->passengerTypeCode;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['PersonName']['GivenName'] = $traveler->name;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['PersonName']['NamePrefix'] = $traveler->namePrefix;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['PersonName']['Surname'] = $traveler->surname;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['Document']['DocType'] = $traveler->docType;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['Document']['DocID'] = $traveler->docID;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['Document']['DocIssueCountry'] = $traveler->docIssueCountry;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['Document']['ExpireDate'] = $traveler->docExpireDate;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['NumberOfBaggages'] = 0;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['NumberOfBaggages1'] = 0;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['HandLuggages'] = 0;
            $param_data['OTA_AirBookRQ']['TravelerInfo']['AirTraveler'][$i]['HandLuggages1'] = 0;
            $i++;
        }
        return $param_data;

    }

    private static function getRequestOptimizationFinal($param)
    {
        $param_data = [];
        $controlNumber = base64_encode(sha1($param . '123787555',true));
        $param_data['OTA_AirBookRQ']['ReferenceNumber'] = $param;
        $param_data['OTA_AirBookRQ']['ControlNumber'] = $controlNumber;
        return $param_data;
    }

    private static function getResponseOptimization($response)
    {
        /*$response = str_replace('soap:', '', $lastResponse);
        $xml = new SimpleXMLElement($response);*/
        $result = parent::responseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
        return $result->Body->CreateTicketResponse->OTA_AirBookRS;
    }

}