<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 12.09.2017
 * Time: 14:10
 */

namespace modules\flight\frontend\providers\amadeus\methods;

namespace modules\flight\frontend\providers\amadeus\methods;


use modules\flight\frontend\models\base\BookingClassAvails;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\base\Persons;
use modules\flight\frontend\models\Flights;
use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\models\Tokens;
use modules\users\common\models\UserPerson;

class Cancel extends Method
{

    public static function run($param, \SoapClient $soap)
    {
        $param_data = self::getRequestOptimization($param);

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->Cancel($param_data);
        $CancelResponse = $soap->__getLastResponse();

        return self::getResponseOptimization($CancelResponse);
    }

    private static function getRequestOptimization($param)
    {
        $param_data = [];
        $param_data["OTA_CancelRQ"]["BookingReferenceID"]["Type"] = "F";
        $param_data["OTA_CancelRQ"]["BookingReferenceID"]["ID_Context"] = $param["pnr"];
        $param_data["OTA_CancelRQ"]["Verification"]["PersonName"]["Surname"] = $param["surname"];

        return $param_data;
    }

    private static function getResponseOptimization($response)
    {
        $result = parent::responseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
        return $result->Body->CancelResponse->OTA_CancelRS;
    }

    public static function removeDataAfterCancel($pnr)
    {
        $flight_segments = [];
        $flight_id = Flights::findOne(['pnr_no' => $pnr])->id;
        $flight_persons = FlightsPerson::find()->select(['person_id'])->where(['flight_id' => $flight_id])->asArray()->all();
        $flight_details = FlightDetails::find()->select(['id as flight_details_id'])->where(['flight_id' => $flight_id])->asArray()->all();
        foreach ($flight_details as $flight_detail) {
            $flight_segments[] = FlightSegments::find()->select(['id'])->where($flight_detail)->asArray()->all();
        }
        $flight_price = FlightPriceInfo::findOne(['flight_id' => $flight_id]);

        foreach ($flight_segments as $flight_segmentKey => $flight_segment) {
            foreach ($flight_segment as $flight_segmentItemKey => $flightSegmentItem) {
                if($flightSegmentItem) {
                    FlightSegments::findOne($flightSegmentItem)->delete();
                    BookingClassAvails::findOne(['segment_id' => $flightSegmentItem['id']])->delete();
                }
            }
        }

        foreach ($flight_persons as $flight_person) {
            if($flight_person) {

                //count Persons in Flights Person table to check is there any records for this person id
                $countPersons = FlightsPerson::find()->where(['person_id' => $flight_person["person_id"]])->count();
                if ( $countPersons == 1 ) {
                    //delete Person and Link with User registered if it is last person in base with this particular person id that persist in this booking
                    Persons::findOne($flight_person["person_id"])->delete();
                    UserPerson::findOne(['person_id' => $flight_person["person_id"]])->delete();
                }
                FlightsPerson::findOne(['person_id' => $flight_person["person_id"], 'flight_id' => $flight_id])->delete();
            }
        }

        foreach ($flight_details as $flight_detail) {
            if($flight_detail) FlightDetails::findOne($flight_detail["flight_details_id"])->delete();
        }

        if($flight_id) Flights::findOne($flight_id)->delete();
        if($flight_price) FlightPriceInfo::findOne(['flight_id' => $flight_id])->delete();
    }
}