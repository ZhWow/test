<?php

namespace modules\flight\frontend\providers\amadeus\methods;

use modules\flight\frontend\providers\amadeus\methods\base\Method;

class GetPNR extends Method
{
    const TYPE = 'Flight';

    /**
     * @param $pnrNO
     * @param $surname
     * @param  $soap
     * @return mixed
     */
    public static function run($pnrNO, $surname, \SoapClient $soap)
    {
        $param_data = self::getRequestOptimization($pnrNO, $surname);
        /* @noinspection PhpUndefinedMethodInspection */
        $soap->GetPNR($param_data);
        return self::getResponseOptimization($soap->__getLastResponse());

//
//        TravelerInfo::getTravelerInformation($travelerInfoParam->AirReservation->TravelerInfo->AirTraveler, $pnrNo);



    }

    /**
     * Парсит ответ
     *
     * @param string $response Сам ответ
     * @return mixed
     */
    private static function getResponseOptimization($response)
    {
       /* $response = str_replace('soap:', '', $lastResponse);
        $xml = new SimpleXMLElement($response);*/
        $result = parent::responseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
        return $result->Body->GetPNRResponse->OTA_AirBookRS;
    }

    private static function getRequestOptimization($pnrNO, $surname)
    {
        $param_data = [];
        $param_data['OTA_ReadRQ']['ReadRequests']['ReadRequest']['BookingReferenceID']['Type'] = self::TYPE;
        $param_data['OTA_ReadRQ']['ReadRequests']['ReadRequest']['BookingReferenceID']['ID_Context'] = $pnrNO;
        $param_data['OTA_ReadRQ']['ReadRequests']['ReadRequest']['Verification']['PersonName']['Surname'] = $surname;
        return $param_data;
    }

}