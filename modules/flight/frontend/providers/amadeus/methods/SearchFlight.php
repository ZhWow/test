<?php

namespace modules\flight\frontend\providers\amadeus\methods;

use Yii;
use modules\flight\frontend\models\Tokens;
use modules\flight\frontend\models\base\SearchForm;
use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\providers\amadeus\services\AmadeusFormatter;

/**
 * Class SearchFlight
 * @package modules\flight\frontend\providers\amadeus\methods
 */
class SearchFlight extends Method
{
    use AmadeusFormatter;

    public static function run(SearchForm $param, \SoapClient $soap, Tokens $token)
    {
        if($token->id != null) {
            $soap->__setCookie('ASP.NET_SessionId', $token->token);
        }

        $param_data = self::getRequestOptimization($param->attributes);
        Yii::error($param_data, 'flight');
        /* @noinspection PhpUndefinedMethodInspection */
        $soap->SearchFlight($param_data);
//        $res = file_get_contents(Yii::getAlias('@xmlFiles') . '/result.xml');
//        Yii::error($soap->__getLastResponse(), 'flight');
        return self::getResponseOptimization($soap->__getLastResponse());
//        return self::getResponseOptimization($res);
    }

    private static function getRequestOptimization($param)
    {
//        \Yii::error($param, 'flight');
        $soapParams = Yii::$app->controller->module->params['soap_param'];
        $passengerCode = Yii::$app->controller->module->params['passenger_code'];
        $param_data = [];
        $param_data['OTA_AirLowFareSearchRQ']['ProviderType'] = $soapParams['PROVIDER_TYPE'];
//        $param_data['OTA_AirLowFareSearchRQ']['RefundableType'] = $soapParams['REFUNDABLE_TYPE'];
        $param_data['OTA_AirLowFareSearchRQ']['RefundableType'] = 'OnlyRefundable';
//        $param_data['OTA_AirLowFareSearchRQ']['DirectFlightsOnly'] = 'false';

        /*$param_data['OTA_AirLowFareSearchRQ']['AdvanceSearchInfo']['MaxLayoverPerConnection']['Hour'] = '24';
        $param_data['OTA_AirLowFareSearchRQ']['AdvanceSearchInfo']['MaxLayoverPerConnection']['Minute'] = '0';*/

        for ($i = 0; $i < count($param['arrivalCity']); $i++) {
            if ($i === 0) {
                if ($param['tripDateBackward']) {
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DepartureDateTime'] = self::getDepartureDate($param['tripDate'][$i]);
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['LocationCode'] = $param['departureCode'];

                    if ($param['departureType'] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['MultiAirportCityInd'] = 'true';
                    }

                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['LocationCode'] = $param['arrivalCode'][$i];

                    if ($param['arrivalType'][$i] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['MultiAirportCityInd'] = 'true';
                    }

                    // ---------------------------------------------------------------------------------------------- //

                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][1]['DepartureDateTime'] = self::getDepartureDate($param['tripDateBackward']);
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][1]['OriginLocation']['LocationCode'] = $param['arrivalCode'][0];

                    if ($param['arrivalType'][0] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][1]['OriginLocation']['MultiAirportCityInd'] = 'true';
                    }

                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][1]['DestinationLocation']['LocationCode'] = $param['departureCode'];

                    if ($param['departureType'] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][1]['DestinationLocation']['MultiAirportCityInd'] = 'true';
                    }
                } else {
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DepartureDateTime'] = self::getDepartureDate($param['tripDate'][$i]);
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['LocationCode'] = $param['departureCode'];

                    if ($param['departureType'] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['MultiAirportCityInd'] = 'true';
                    }

                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['LocationCode'] = $param['arrivalCode'][$i];

                    if ($param['arrivalType'][$i] == 'c') {
                        $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['MultiAirportCityInd'] = 'true';
                    }
                }

            } else {
                $departureIndex = ($i - 1);
                $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DepartureDateTime'] = self::getDepartureDate($param['tripDate'][$i]);
                $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['LocationCode'] = $param['arrivalCode'][$departureIndex];

                if ($param['arrivalType'][$departureIndex] == 'c') {
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['OriginLocation']['MultiAirportCityInd'] = 'true';
                }
                $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['LocationCode'] = $param['arrivalCode'][$i];

                if ($param['arrivalType'][$i] == 'c') {
                    $param_data['OTA_AirLowFareSearchRQ']['OriginDestinationInformation'][$i]['DestinationLocation']['MultiAirportCityInd'] = 'true';
                }
            }
        }

        $j = 0;
        for ($q = 0; $q < $param['adults']; $q++) {
            $param_data['OTA_AirLowFareSearchRQ']['TravelerInfoSummary']['AirTravelerAvail']['PassengerTypeQuantity'][$j]['Code'] = $passengerCode['ADULTS'];
            $j++;
        }
        for ($q = 0; $q < $param['children']; $q++) {
            $param_data['OTA_AirLowFareSearchRQ']['TravelerInfoSummary']['AirTravelerAvail']['PassengerTypeQuantity'][$j]['Code'] = $passengerCode['CHILDREN'];
            $j++;
        }
        for ($q = 0; $q < $param['infants']; $q++) {
            $param_data['OTA_AirLowFareSearchRQ']['TravelerInfoSummary']['AirTravelerAvail']['PassengerTypeQuantity'][$j]['Code'] = $passengerCode['INFANTS'];
            $j++;
        }

        $param_data['OTA_AirLowFareSearchRQ']['TravelPreferences']['CabinPref']['Cabin'] = $param['cabin'];

//        $param_data['OTA_AirLowFareSearchRQ']['AdvanceSearchInfo']['NumberOfRecommendation'] = '110';

        return $param_data;
    }

    private static function getResponseOptimization($response)
    {
        return $result = self::searchFlightResponseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
//        return $result->SearchFlightResponse->OTA_AirLowFareSearchRS;
    }

    public static function searchFlightResponseOptimization($response)
    {
        $replace = preg_replace('#<PricedItineraryForOWC(.+?)</PricedItineraryForOWC>#', '', $response);
        preg_match('#<PricedItineraries>(.+?)</PricedItineraries>#s', $replace, $result);
        $xml =  new \SimpleXMLElement($result[0]);
//        $xml =  new \SimpleXMLElement($replace);
        /* @noinspection PhpUndefinedFieldInspection */
        return $xml;
    }
}