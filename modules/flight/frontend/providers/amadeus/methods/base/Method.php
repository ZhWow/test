<?php

namespace modules\flight\frontend\providers\amadeus\methods\base;

class Method
{

    public static function getParseErrorMessage($response)
    {
        return <<<MSG
            \n
            ShortText - {$response['ShortText']},
            Code - {$response['Code']},
            Type - {$response['Type']},
            ErrorCode - {$response['ErrorCode']}
MSG;
    }

    protected static function responseOptimization($response)
    {
        $result = str_replace('soap:', '', $response);
        $xml =  new \SimpleXMLElement($result);
        /* @noinspection PhpUndefinedFieldInspection */
        return $xml;
    }

    public static function isActualToken($tokenStartTime)
    {
        $tokenStartTimeSec = strtotime($tokenStartTime);
        $currentTime = time();
        $fifteenMin = 60 * 15; // 60 sec * 15 = 15 min
        $diff = $currentTime - $tokenStartTimeSec;
        return (($tokenStartTimeSec - $currentTime) > $fifteenMin) ? false : true;
    }
}