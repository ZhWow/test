<?php

namespace modules\flight\frontend\providers\amadeus\methods;

use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\models\base\BookingClassAvails;
use modules\flight\frontend\providers\amadeus\components\Person;
use modules\users\common\models\UserPerson;
use Yii;
use modules\flight\frontend\models\Persons;
use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\models\Tokens;
use modules\flight\frontend\models\Flights;
use modules\flight\frontend\models\Options;
use modules\flight\frontend\models\Documents;
use modules\users\common\models\User;
use common\models\UserProfile;
use modules\flight\frontend\providers\amadeus\services\AmadeusFormatter;

/**
 * Class BookFlight
 *
 * @property integer $recommendationID
 * @property integer $combinationID
 * @property array $persons
 *
 * @package modules\flight\services\methods
 * @author Bablanov Dauren <bablanov.dauren@yandex.kz>
 */
class BookFlight extends Method
{
    use AmadeusFormatter;

    public $recommendationID;
    public $combinationID;
    public $persons = [];

    /**
     * @param $param
     * @param $token
     * @param $soap
     * @return array
     */
    public static function run($param, Tokens $token, \SoapClient $soap)
    {
        $soap->__setCookie('ASP.NET_SessionId', $token->token);

        $param_data = self::getRequestOptimization($param);

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->BookFlight($param_data);
        $BookFlightResponse = $soap->__getLastResponse();
        /* @noinspection PhpUndefinedMethodInspection */
        $soap->SignOut();
        $response = self::getResponseOptimization($BookFlightResponse);

        return $response;
    }

    private static function getRequestOptimization($param)
    {

        $sequenceID = $param[0]['Persons']['sequenceID'];
        $combinationID = $param[0]['Persons']['combinationID'];
        /* @var $param Persons */
        $param_data = [];
        $param_data["OTA_AirBookRQ"]["RecommendationID"] = $sequenceID;
        $param_data["OTA_AirBookRQ"]["CombinationID"] = $combinationID;
        $param_data["OTA_AirBookRQ"]["POS"] = "";
        /*$param_data["OTA_AirBookRQ"]["PNRRemarks"]["PNRRemark"]["RemarkType"] = "RM";
        $param_data["OTA_AirBookRQ"]["PNRRemarks"]["PNRRemark"]["RemarkCategory"] = "*";
        $param_data["OTA_AirBookRQ"]["PNRRemarks"]["PNRRemark"]["Note"] = "RNN000002";*/
//        $param_data["OTA_AirBookRQ"]["PNRRemarks"]= "";

        $all_travelers = [];
        foreach ($param as $item) {
            $person = $item['Persons'];

            $traveler = [];
            $traveler["NumberOfBaggages"] = 0;
            $traveler["NumberOfBaggages1"] = 0;
            $traveler["HandLuggages"] = 0;
            $traveler["HandLuggages1"] = 0;

            $traveler["PassengerTypeCode"] = $person['code'];
            $traveler["PersonName"]["GivenName"] = $person['name'];
            $traveler["PersonName"]["NamePrefix"] = self::getPrefixName($person['code'], $person['gen']);
            $traveler["PersonName"]["Surname"] = $person['surname'];
            $traveler["BirthDate"] = self::getDate($person['birthday']);
            $traveler["Telephone"]["CountryCode"] = "";
            $traveler["Telephone"]["AreaCityCode"] = "";
            $traveler["Telephone"]["PhoneNumber"] = self::getPhone($person['phonenumber']);
            $traveler["Telephone"]["PhoneType"] = 'Gsm'; //todo hard code;
            $traveler["Email"]["EmailType"] = 1;
            $traveler["Email"]["_"] = $person['email'];

            $params_document = [];
            $params_document["DocType"] = 'DOCS'; // todo always DOCS
            $params_document["InnerDocType"] = $person['docType'];
            $params_document["DocID"] = $person['docID'];
            $params_document["DocIssueCountry"] = $person['docIssueCountry'];
            $params_document["ExpireDate"] = self::getDate($person['docExpireDate']);

            $traveler["Document"][] = $params_document;

            $all_travelers[] = $traveler;
        }

        $param_data["OTA_AirBookRQ"]["TravelerInfo"]['AirTraveler'] = $all_travelers;

        $param_data["OTA_AirBookRQ"]["Fulfillment"]["DeliveryAddress"]["AddressLine"] = "Kunayev st., 14/3";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["DeliveryAddress"]["CityName"] = "Astana";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["DeliveryAddress"]["CountryName"]["Code"] = "KZ";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["DeliveryAddress"]["PostalCode"] = '010000';
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["PaymentDetails"]["PaymentDetail"]["PaymentType"] = "None";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["PaymentDetails"]["PaymentDetail"]["BillingAddress"]["AddressLine"] = "Kunayev st., 14/3";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["PaymentDetails"]["PaymentDetail"]["BillingAddress"]["CityName"] = "Astana";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["PaymentDetails"]["PaymentDetail"]["BillingAddress"]["CountryName"]["Code"] = "KZ";
        $param_data["OTA_AirBookRQ"]["Fulfillment"]["PaymentDetails"]["PaymentDetail"]["BillingAddress"]["PostalCode"] = "010000";

        $param_data["OTA_AirBookRQ"]["Ticketing"]["TicketType"] = "BookingOnly";

        return $param_data;
    }

    private static function getResponseOptimization($response)
    {
        $result = parent::responseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
        return $result->Body->BookFlightResponse->OTA_AirBookRS;
    }

//    private static function getParseErrorMessage($response)
//    {
//        return <<<MSG
//            \n
//            ShortText - {$response['ShortText']},
//            Code - {$response['Code']},
//            Type - {$response['Type']},
//            ErrorCode - {$response['ErrorCode']}
//MSG;
//    }

    public static function recordInDB($response)
    {
        /* @var $flight Flights */
        $flight = Flights::saveSoapBookResult($response);
        $flightID = $flight->id;

        foreach ($response->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option_attribute) {
            $FlightDetails = new FlightDetails();
            $FlightDetails->saveFlightDetails($flightID, $option_attribute);

            foreach ($option_attribute->FlightSegment as $FlightSegment) {
                $FlightSegments = new FlightSegments();
                $FlightSegments->saveFlightSegment($FlightDetails->id, $FlightSegment);
                $BookingClassAvails = new BookingClassAvails();
                $BookingClassAvails->saveBookingClassAvails($FlightSegments->id, $FlightSegment);
            }
        }

        $FlightPriceInfo = new FlightPriceInfo();
        $FlightPriceInfo->savePriceInfo($flightID, $response->PriceInfo->ItinTotalFare);

        $responseJson = json_encode($response);
        foreach ($response->TravelerInfo->AirTraveler as $person_attribute) {


            $personId = null;

            $person = Person::getPerson($person_attribute);
            if ($person->issetPerson()) {
                $flightsPerson = new FlightsPerson();
                $flightsPerson->flight_id = $flightID;
                $flightsPerson->person_id = $person->issetPerson();
                $flightsPerson->save();
                $personId = $person->issetPerson();
            } else {
                $personId = Persons::saveSoapBookResult($person_attribute);
                $flightsPerson = new FlightsPerson();
                $flightsPerson->flight_id = $flightID;
                $flightsPerson->person_id = $personId;
                $flightsPerson->save();
                /*$

               Documents::saveSoapBookResult($person_attribute->Document, $personID);*/
            }

            if ($person_attribute->Email && $personId !== null) {
                $person = Persons::findOne($personId);
                /*
                $document = Documents::findOne(['personID' => $person->id]);*/

                if (Yii::$app->user->isGuest) {

                    $NewUser = false;

                    $user = User::findOne(['email' => $person_attribute->Email]);

                    $phone = str_replace('_', '', (string) $person_attribute->Telephone['LocationCode']);

                    if ( $user == null ) {
                        $NewUser = true;
                        $user = new User();
                        $user->saveNewUser($person, $person_attribute->Email, $phone);

                    } else {
                        $profile = UserProfile::findOne(['user_id' => $user->id]);
                        if ( $profile == null ) {
                            UserProfile::autoCreateProfile($person, $user->id, $phone);
                        } else {
                            UserProfile::autoUpdateProfile($person, $user->id, $phone);
                        }
                    }

//                    $messageSubject = 'Your book id ' . $flight->pnr_no . '. Your login and password ' . $person_attribute->Email . ' 123456';

                    // TODO hren poimi nujno li eto
                    /*$session = Yii::$app->session;
                    $session->set('user_ID', $user->id);*/

                } else {

                    $NewUser = false;

                    $userPerson = new UserPerson();
                    /* @noinspection PhpUndefinedFieldInspection */
                    $userPerson->saveUser(Yii::$app->user->identity->id, $personId);

                    /* @noinspection PhpUndefinedFieldInspection */
                    $profile = UserProfile::findOne(['user_id' => Yii::$app->user->identity->id]);
                    if ($profile == null) {
                        /* @noinspection PhpUndefinedFieldInspection */
                        UserProfile::autoCreateProfile($person, Yii::$app->user->identity->id, $person_attribute->Telephone['LocationCode']);
                    } else {
                        /* @noinspection PhpUndefinedFieldInspection */
                        UserProfile::autoUpdateProfile($person, Yii::$app->user->identity->id, $person_attribute->Telephone['LocationCode']);
                    }
//                    $messageSubject = 'Your book id ' . $flight->pnr_no;
                }

                Yii::$app->mailer->compose('book-register-user', [
                    'person_attribute' => $person_attribute,
                    'flight' => $flight,
                    'NewUser' => $NewUser
                ])
                    ->setFrom(['digital@qway.kz' => 'Qway No-Reply'])
                    ->setTo($person_attribute->Email)
                    ->setSubject('Qway: Бронирование авиабилета')
                    ->send();

            } elseif ($personId !== null) {
                $userPerson = new UserPerson();
                /* @noinspection PhpUndefinedFieldInspection */
                $userPerson->saveUser(Yii::$app->user->identity->id, $personId);
            }
        }

        return $flight;
    }
}

?>