<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 11.09.2017
 * Time: 20:16
 */

namespace modules\flight\frontend\providers\amadeus\methods;


use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\models\Tokens;

class GetFlightRules extends Method
{
    public $recommendationID;
    public $combinationID;

    public static function run($param, Tokens $token, \SoapClient $soap)
    {
        $soap->__setCookie('ASP.NET_SessionId', $token->token);

        $param_data = self::getRequestOptimization($param);

        /* @noinspection PhpUndefinedMethodInspection */
        $soap->GetFlightRules($param_data);
        $GetFlightRulesResponse = $soap->__getLastResponse();

        return self::getResponseOptimization($GetFlightRulesResponse);
    }

    private static function getRequestOptimization($param)
    {
        /* @var $param Persons */
        $param_data = [];
        $param_data["OTA_AirRulesRQ"]["RecommendationID"] = $param['recommendationid'];
        $param_data["OTA_AirRulesRQ"]["PassengerType"] = 'ADT';
        $param_data["OTA_AirRulesRQ"]["CombinationID"] = $param['combinationid'];

        return $param_data;
    }

    private static function getResponseOptimization($response)
    {
        $xml = null;
        preg_match('#<OTA_AirRulesRS(.+?)</OTA_AirRulesRS>#s', $response, $result);
        if ($result[0]) {
            $xml =  new \SimpleXMLElement($result[0]);
        }
//        $result = parent::responseOptimization($response);
        /* @noinspection PhpUndefinedFieldInspection */
//        return $result->GetFlightRulesResponse->OTA_AirRulesRS;
        return $xml;
    }
}