<?php

namespace modules\flight\frontend\providers\amadeus\components;

use modules\flight\frontend\models\Airports;
use modules\flight\frontend\models\Airlines;
use modules\flight\frontend\models\Plains;

/**
 * Class Segment
 *
 * @property string $departureDateTime
 * @property string $arrivalDateTime
 * @property int $flightNumber
 * @property string $departureAirportCode
 * @property string $arrivalAirportCode
 * @property string $departureAirportTerminal
 * @property string $arrivalAirportTerminal
 * @property string $bortCode
 * @property string $airlineCode
 * @property int $freeSeats Available seats FareBasis
 * @property int $resBookDesigCode
 * @property string $eTicketEligibility
 * @property string $operatingAirlineCode Provides airplane board. The logotype of the airplane is from him.
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Segment
{
    public $departureDateTime;
    public $arrivalDateTime;
    public $flightNumber;
    public $departureAirportCode;
    public $arrivalAirportCode;
    public $departureAirportTerminal;
    public $arrivalAirportTerminal;
    public $bortCode;
    public $airlineCode;
    public $freeSeats = 0;
    public $resBookDesigCode;
    public $eTicketEligibility;
    public $operatingAirlineCode;

    //public $bort;


    public function __construct($config)
    {
        $this->departureDateTime = (string) $config['DepartureDateTime'];
        $this->arrivalDateTime = (string) $config['ArrivalDateTime'];
        $this->flightNumber = (int) $config["FlightNumber"];
        $this->resBookDesigCode = (string) $config["ResBookDesigCode"];
        $this->operatingAirlineCode = (string) $config->OperatingAirline["Code"];
        $this->departureAirportCode = (string) $config->DepartureAirport['LocationCode'];
        $this->arrivalAirportCode= (string) $config->ArrivalAirport['LocationCode'];
        $this->bortCode = (string) $config->Equipment["AirEquipType"];
        //$this->bort = Plains::getBort((string) $config->Equipment["AirEquipType"])->name;
        $this->airlineCode = (string) $config->MarketingAirline['Code'];
        $this->eTicketEligibility = (string) $config->eTicketEligibility;
        $this->departureAirportTerminal = (string) $config->DepartureAirport['Terminal'];
        $this->arrivalAirportTerminal = (string) $config->ArrivalAirport['Terminal'];
        foreach ($config->BookingClassAvails->BookingClassAvail as $key => $avail) {
            $this->freeSeats += (int) $avail['ResBookDesigQuantity'];
        }
    }

    /**
     * Runs around all FlightSegment. And creates an object
     * Each created object puts into the segments array which is a property of the Option object
     *
     * @param array $flightSegment SoapClient XML FlightSegment
     * @param array $segments This is a property reference Option::segments
     */
    public static function getSegments($flightSegment, &$segments)
    {
        foreach ($flightSegment as $segments_param) {
            /* @var $segment Segment */
            $segment = new Segment($segments_param);
            $segments[] = $segment;
        }
    }

    public function getDepartureDate() {
        return $this->getDate($this->departureDateTime);
    }

    public function getDepartureDateNumber() {
        return substr($this->departureDateTime, 0, 10);
    }

    public function getDepartureTime() {
        return $this->getTime($this->departureDateTime);
    }

    public function getArrivalDate() {
        return $this->getDate($this->arrivalDateTime);
    }

    public function getArrivalTime() {
        return $this->getTime($this->arrivalDateTime);
    }

    protected function getDate($datetime)
    {
        $day = substr($datetime, 8, 2);
        $month_number = substr($datetime, 5, 2);
        $month = self::getMonth($month_number);
        return $day." ".$month;
    }

    public function getTime($datetime)
    {
        $time = substr($datetime, 11, 5);
        return $time;
    }

    public static function getMonth($number)
    {
        $month = [
            1 => 'января', 'февраля' , 'марта', 'апреля', 'мая', 'июня',
            'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
        ];

        return $month[(int)$number];
    }

    public function getFreeSeats()
    {
        return $this->freeSeats > 9 ? '9+' : $this->freeSeats;
    }

    public function getFlightNumber()
    {
        return $this->airlineCode . '-' . $this->flightNumber;
    }

    public function getDepartureAirport()
    {
        return Airports::getAirport($this->departureAirportCode);
    }

    public function getArrivalAirport()
    {
        return Airports::getAirport($this->arrivalAirportCode);
    }

    public function getBort() {
        //$this->bort = Plains::getBort($this->bortCode);
        return Plains::getBort($this->bortCode);
    }

    public function getAirline() {
        return Airlines::getAirline($this->airlineCode);
    }

    public function getAirlineCode() {
        return $this->airlineCode;
    }

}
