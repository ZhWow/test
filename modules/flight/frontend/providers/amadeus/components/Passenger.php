<?php

namespace modules\flight\frontend\providers\amadeus\components;

/**
 * Class Passenger
 *
 * @property string $code Passenger type (ADT etc.)
 * @property int $quantity Number of passengers of a particular type (ADT etc.)
 * @property int $baseFareAmount
 * @property int $totalFareAmount $baseFareAmount + $taxes
 * @property $negotiatedFare
 * @property array $taxes Airport pick-up
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Passenger {
    public $code;
    public $quantity;
    public $baseFareAmount;
    public $totalFareAmount;
    public $negotiatedFare;
    public $taxes = [];

    /**
     * Passenger constructor.
     *
     * @param array $soapResult PTC_FareBreakdown
     */
    public function __construct($soapResult)
    {
        /* @noinspection PhpUndefinedFieldInspection */
        $this->code = (string) $soapResult->PassengerTypeQuantity["Code"];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->quantity = (int) $soapResult->PassengerTypeQuantity["Quantity"];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->baseFareAmount = (int) $soapResult->PassengerFare->BaseFare["Amount"];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->totalFareAmount = (int) $soapResult->PassengerFare->TotalFare["Amount"];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->negotiatedFare = $soapResult->PassengerFare["NegotiatedFare"];

        /* @noinspection PhpUndefinedFieldInspection */
        $taxes =  $soapResult->PassengerFare->Taxes;
        foreach ($taxes->Tax as $tax) {
            $this->taxes[] = (int) $tax['Amount'];
        }
    }

    /**
     * Runs around all PTC_FareBreakdown. And creates an object
     * Each created object puts into the passengers array which is a property of the Sequence object
     *
     * @param array $PTC_FareBreakdown SoapClient XML. PTC_FareBreakdown – Calculating the ticket price for each passenger.
     * @param array $passengers This is a property reference Sequence::passengers
     */
    public static function getPassengers($PTC_FareBreakdown, &$passengers)
    {
        foreach ($PTC_FareBreakdown as $passengerAttr) {
            $passengers[] = new self($passengerAttr);
        }
    }

}