<?php

namespace modules\flight\frontend\providers\amadeus\components;

class TravelerInfo
{
    public $passengerTypeCode = '';
    public $namePrefix = '';
    public $name = '';
    public $surname = '';
    public $docID = '';
    public $docType = '';
    public $innerDocType = '';
    public $docIssueCountry = '';

    public static $travelerInfo = [];
    public static $pnrNO = '';

    /*public static function saveTravelerInfo($param)
    {
        $travelerInfo = new self();
        $travelerInfo->passengerTypeCode = $param['AirTraveler']['PassengerTypeCode'];
        $travelerInfo->name = $param['AirTraveler']['PersonName']['GivenName'];
        $travelerInfo->surname = $param['AirTraveler']['PersonName']['Surname'];
        $travelerInfo->docID = $param['AirTraveler']['Document']['DocID'];
        $travelerInfo->docType = $param['AirTraveler']['Document']['DocType'];
        $travelerInfo->innerDocType = $param['AirTraveler']['Document']['InnerDocType'];
        $travelerInfo->docIssueCountry = $param['AirTraveler']['Document']['DocIssueCountry'];
    }*/

    public function __construct($param)
    {
        $this->passengerTypeCode = $param['PassengerTypeCode'];
        $this->namePrefix = $param->PersonName->NamePrefix;
        $this->name = $param->PersonName->GivenName;
        $this->surname = $param->PersonName->Surname;
        $this->docID = $param->Document['DocID'];
        $this->docType = $param->Document['DocType'];
        $this->innerDocType = $param->Document['InnerDocType'];
        $this->docIssueCountry = $param->Document['DocIssueCountry'];
        $this->docExpireDate = $param->Document['ExpireDate'];
    }

    public static function getTravelerInformation($params)
    {
        $travelerInfo = [];
        foreach ($params as $airTraveler) {
            $travelerInfo[] = new self($airTraveler);
        }
        return $travelerInfo;
    }
}