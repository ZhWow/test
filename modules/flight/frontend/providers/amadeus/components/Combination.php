<?php

namespace modules\flight\frontend\providers\amadeus\components;

/**
 * Class Combination
 *
 * @property array $list
 * @property string $validatingAirlineCode Refers to the entire flight route to all cities.
 * It is necessary to make a mark-up on the ticket price depending on the validating carrier.
 * And on the ticket is written the ticket number from this carrier.
 * @property bool forceETicket not necessary. Means, if true, then you need to book and immediately buy.
 * @property int $combinationID
 * @property int $serviceFeeAmount not necessary.
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Combination
{
    public $list;
    public $validatingAirlineCode;
    public $combinationID;
    public $serviceFeeAmount;
    public $forceETicket;

    /**
     * Combination constructor.
     *
     * @param array $soapResult OriginDestinationCombination
     */
    public function __construct($soapResult)
    {
        $this->list = explode(";", $soapResult["IndexList"]);
        $this->validatingAirlineCode = (string) $soapResult["ValidatingAirlineCode"];
        $this->forceETicket = (boolean) $soapResult["ForceETicket"];
        $this->combinationID = (int) $soapResult["CombinationID"];
        $this->serviceFeeAmount = (int) $soapResult["ServiceFeeAmount"];
    }

    /**
     * Runs around all OriginDestinationCombination. And creates an object
     * Each created object puts into the combinations array which is a property of the Sequence object
     *
     * @param array $originDestinationCombinations SoapClient XML OriginDestinationCombination
     * @param array $combinations This is a property reference Sequence::combinations
     */
    public static function getCombinations($originDestinationCombinations, &$combinations)
    {
        foreach ($originDestinationCombinations as $combinationAttr) {
            $combinations[] = new Combination($combinationAttr);
        }
    }

}