<?php

namespace modules\flight\frontend\providers\amadeus\components;

/**
 * Class Flight
 *
 * @property int $combinationID
 * @property string $validatingAirlineCode
 * @property int $serviceFeeAmount
 * @property bool $forceETicket
 * @property Sequence $sequence
 * @property string $ticketDesignators
 * @property string $ticketDesignatorCode
 * @property array $options
 * @property array $tripId
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Flight
{
    public $combinationID;
    public $validatingAirlineCode;
    public $serviceFeeAmount;
    public $forceETicket;
    public $sequence;
    public $ticketDesignators;
    public $options = [];
    public $ticketDesignatorCode = '';
    public $tripId = '';

    /**
     * @param array $sequences Array of Sequence objects
     * @return array
     */
    public static function getFlights($sequences)
    {
        $flights = [];
        $saveFlight = false;
        foreach ($sequences as $sequence) {

            /* @var $sequence Sequence */
            foreach ($sequence->combinations as $combination) {
                /* @var $combination Combination */
                $flight = new self();
                $flight->combinationID = (int) $combination->combinationID;
                $flight->validatingAirlineCode = (string) $combination->validatingAirlineCode;
                $flight->serviceFeeAmount = (int) $combination->serviceFeeAmount;
                $flight->forceETicket = (int) $combination->forceETicket;

                $flight->sequence = $sequence;
                foreach ($combination->list as $key => $value) {
                    if (isset($sequence->options[$key][$value])) {
                        $saveFlight = true;
                        $flight->options[] = $sequence->options[$key][$value];

                    }
                }
                if ($saveFlight) {
                    $flight->ticketDesignators = $sequence->ticketDesignators;
                    $flight->ticketDesignatorCode = $sequence->ticketDesignatorCode;
                    $flights[] = $flight;
                }
            }
            $sequence->combinations = false;
            $sequence->options = false;
        }

        return $flights;
    }

}