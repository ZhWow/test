<?php

namespace modules\flight\frontend\providers\amadeus\components;

/**
 * Class Sequence
 *
 * @property int $sequenceNumber
 * @property string $currency
 * @property string $providerType
 * @property int $totalFare
 * @property array $passengers
 * @property array $combinations
 * @property array $options
 * @property string $ticketDesignators
 * @property string $ticketDesignatorCode
 * @property string $tripId
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Sequence
{
    public $sequenceNumber;
    public $currency;
    public $providerType;
    public $totalFare;
    public $baseFare;
    public $taxesAmount;
    public $BookingClassAvails = [];
    public $passengers = [];
    public $combinations = [];
    public $options = [];
    public $ticketDesignators = '';
    public $tripId = '';

    /**
     * Sequence constructor.
     *
     * @param array $soapResult PricedItinerary
     */
    public function __construct($soapResult)
    {
        $test = json_decode(json_encode($soapResult), true);
        $this->sequenceNumber = (int) $soapResult['SequenceNumber'];
        $this->currency = (string) $soapResult['Currency'];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->totalFare = (int) $soapResult->AirItineraryPricingInfo->ItinTotalFare->TotalFare['Amount'];
        /* @noinspection PhpUndefinedFieldInspection */
        $this->baseFare = (int) $soapResult->AirItineraryPricingInfo->ItinTotalFare->BaseFare['Amount'];
        $this->taxesAmount = (int) $this->totalFare - (int) $this->baseFare;
//        $this->BookingClassAvails =

        $this->providerType = (string) $soapResult['ProviderType'];
        $this->ticketDesignators = '';
        $this->ticketDesignatorCode = '';
        /* @noinspection PhpUndefinedFieldInspection */
        foreach ($soapResult->AirItineraryPricingInfo->PTC_FareBreakdowns->PTC_FareBreakdown->TicketDesignators->TicketDesignator as $TicketDesignator) {
            $this->ticketDesignators .=  $TicketDesignator['TicketDesignatorExtension'] . ';';
            $this->ticketDesignatorCode .= $TicketDesignator['TicketDesignatorCode'] . ';';
        }

        $c = 0;
        /* @noinspection PhpUndefinedFieldInspection */
        foreach ($soapResult->AirItinerary->OriginDestinationOptions->OriginDestinationOption->FlightSegment as $segment) {
            $s = 0;
            foreach ($segment->BookingClassAvails->BookingClassAvail as $key => $avail) {
                $this->BookingClassAvails[$s]['ResBookDesigCode'] = (string) $avail['ResBookDesigCode'];
                $this->BookingClassAvails[$s]['ResBookDesigQuantity'] = (string) $avail['ResBookDesigQuantity'];
                $this->BookingClassAvails[$s]['RPH'] = (string) $avail['RPH'];
                $this->BookingClassAvails[$s]['ResBookDesigCabinCode'] = (string) $avail['ResBookDesigCabinCode'];
                $this->BookingClassAvails[$s]['FareBasis'] = (string) $avail['FareBasis'][$s];
                $s++;
            }
            $c++;
        }
        /* @noinspection PhpUndefinedFieldInspection */
        Combination::getCombinations($soapResult->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination, $this->combinations);
        /* @noinspection PhpUndefinedFieldInspection */
        Passenger::getPassengers($soapResult->AirItineraryPricingInfo->PTC_FareBreakdowns->PTC_FareBreakdown, $this->passengers);
        /* @noinspection PhpUndefinedFieldInspection */
        Option::getOptions($soapResult->AirItinerary->OriginDestinationOptions->OriginDestinationOption, $this->options);
    }

    /**
     * @return int
     */
    public function getTotalFare() {
        return number_format($this->totalFare, 0, ',', ' ') . ' ';
    }

    /**
     * Runs around all PricedItinerary. And creates an object
     * Each created object puts into an array
     * @param array $sequenceAttributes SoapClient XML PricedItinerary
     * @return array Array of objects Sequence
     */
    public static function getSequences($sequenceAttributes)
    {
        $sequences = [];
        foreach ($sequenceAttributes as $sequence) {
            $sequences[] = new self($sequence);
        }
        return $sequences;
    }

}
