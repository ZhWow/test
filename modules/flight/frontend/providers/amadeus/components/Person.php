<?php

namespace modules\flight\frontend\providers\amadeus\components;

use modules\flight\frontend\models\Documents;
use modules\flight\frontend\models\Persons;

class Person {
    public $name;
    public $surname;
    public $prefix;
    public $birthday;
    public $code;
    public $docID;
    public $docType;
    public $innerDocType;
    public $expireDate;
    public $docIssueCountry;
    public $docIssueLocation;

    public static function getPerson($attributes)
    {
        $person = new self();
        $person->name = (string) $attributes->PersonName->GivenName;
        $person->surname = (string) $attributes->PersonName->Surname;
        $person->prefix = (string) $attributes->PersonName->NamePrefix;
        $person->birthday = (string) $attributes->BirthDate;
        $person->code = (string) $attributes['PassengerTypeCode'];
        $i = 0;
        foreach ($attributes->Document as $document) {
            if ($i === 0) {
                $person->docID = (string) $document['DocID'];
                $person->docType = (string) $document['DocType'];
                $person->innerDocType = (string) $document['InnerDocType'];
                $person->expireDate = (string) $document['ExpireDate'];
                $person->docIssueCountry = (string) $document['DocIssueCountry'];
            }
            if ($document['DocIssueLocation']) {
                $person->docIssueLocation = (string) $document['DocIssueLocation'];
            }

            $i++;
        }
        return $person;
    }

    public function issetPerson()
    {
        $isset = false;
        /* @var $person Persons */
        $person = Persons::find()->where([
            'code' => $this->code,
            'surname' => $this->surname,
            'name' => $this->name,
            'birthday' => $this->birthday,
            'doc_id' => $this->docID
        ])->one();

        if ( $person != null && $person->doc_issue_country == $this->docIssueCountry
            && $person->inner_doc_type == $this->innerDocType
            && $person->doc_id == $this->docID) {
                $isset = $person->id;

        }
        return $isset;
    }
}