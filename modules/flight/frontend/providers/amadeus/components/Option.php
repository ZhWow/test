<?php

namespace modules\flight\frontend\providers\amadeus\components;

use modules\flight\frontend\models\Airports;

/**
 * Class Option
 *
 * @property string $uniqID
 * @property string $elapsedTime
 * @property int $dir_id Represents the direction of a particular flight. For a one-way flight,
 * the DirectionId attribute is set to "0". For each stage, DirectionInd is incremented by one
 * @property int $ref_id
 * @property int $firstFlightID
 * @property int $lastFlightID
 * @property array $segments
 *
 * @package modules\flight\frontend\providers\amadeus\components
 */
class Option
{
    public $uniqID;
    public $dir_id;
    public $ref_id;
    public $elapsedTime;
    public $segments = [];
    public $optionID;

    protected $firstFlightID;
    protected $lastFlightID;

    /**
     * Option constructor.
     *
     * @param array $soapResult OriginDestinationOption
     */
    public function __construct($soapResult)
    {
        $this->dir_id = (int) $soapResult['DirectionId'];
        $this->ref_id = (int) $soapResult['RefNumber'];
        $this->elapsedTime = (string) $soapResult['ElapsedTime'];
        /* @noinspection PhpUndefinedFieldInspection */
        Segment::getSegments($soapResult->FlightSegment, $this->segments);
        $this->firstFlightID = $this->segments[0]->flightNumber;
        $this->lastFlightID = $this->segments[count($this->segments) - 1]->flightNumber;
        $this->uniqID = $this->firstFlightID . '-' . $this->lastFlightID;
    }

    /**
     * Runs around all OriginDestinationOption. And creates an object
     * Each created object puts into the options array which is a property of the Sequence object
     *
     * @param array $originDestinationOption SoapClient XML OriginDestinationOption
     * @param array $optionsReturn This is a property reference Sequence::options
     */
    public static function getOptions($originDestinationOption, &$optionsReturn)
    {
        $tempOptions = [];
        foreach ($originDestinationOption as $optionsAttribute) {
            $option = new self($optionsAttribute);
            $tempOptions[$option->dir_id][$option->uniqID] = $option;
        }
        foreach ($tempOptions as $key => $options) {
            foreach ($options as $key2 => $option) {
                $optionsReturn[$option->dir_id][$option->ref_id] = $option;
            }
        }
    }

    public function getFirstSegment() {
        return $this->segments[0];
    }

    public function getLastSegment() {
        return $this->segments[count($this->segments) - 1];
    }

    public function getElapsedTime()
    {
        $hour = substr($this->elapsedTime, 0, 2);
        $min = substr($this->elapsedTime, 2, 2);
        return $hour . ' ч ' . $min . ' мин';
    }

    public function getTransplantation() {
        $result = '';
        if (count($this->segments) > 1) {
            foreach ($this->segments as $key => $segment) {
                /* @var $segment Segment */
                if (!isset($this->segments[$key + 1])) {
                    continue;
                }
                /* @var $airport Airports */
                $airport = $segment->getArrivalAirport();
                $result .= 'Через ' . $airport->getName() . '<br>';
            }
        } else {
            $result = 'Прямой';
        }

        return $result;
    }

}
