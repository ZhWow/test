<?php

namespace modules\flight\frontend\providers\amadeus\components;

class ErrorResponse
{
    public static function getMessage($error)
    {
        $msg = ['error' => true, 'data' => 'Извините, '];
        switch ($error['ShortText']) {
            case 'EPowerException.No Available Flights': $msg['data'] .= 'Нет доступных рейсов' ; break;
            //default: $msg['data'] .= 'Неизвестная ошибка';
            default: $msg['data'] .= $error['ShortText'];
        }
        return $msg;
    }
}