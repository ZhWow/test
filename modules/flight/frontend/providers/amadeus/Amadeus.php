<?php

namespace modules\flight\frontend\providers\amadeus;

use backend\modules\integra\services\TelegramBot;
use common\models\User;
use common\modules\helpers\TelegramNotification;
use Faker\Provider\tr_TR\DateTime;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\Flights;
use modules\flight\frontend\models\Tokens;
use modules\flight\frontend\providers\amadeus\methods\Cancel;
use modules\flight\frontend\providers\amadeus\methods\CreateTicket;
use modules\flight\frontend\providers\amadeus\methods\GetFlightRules;
use modules\flight\frontend\providers\amadeus\methods\GetPNR;
use modules\flight\frontend\providers\amadeus\services\AmadeusFormatter;
use modules\flight\frontend\providers\amadeus\services\CustomResponse;
use modules\flight\frontend\providers\base\FlightsProvider;
use modules\flight\frontend\providers\amadeus\methods\base\Method;
use modules\flight\frontend\providers\amadeus\methods\SearchFlight;
use modules\flight\frontend\providers\amadeus\components\Flight;
use modules\services\Registry;
use modules\flight\frontend\providers\amadeus\components\Sequence;
use modules\flight\frontend\soap\AmadeusSoapClient;
use Yii;
use modules\flight\frontend\models\base\SearchForm;
use modules\flight\frontend\models\base\FlightElectronicTickets;
use modules\flight\frontend\models\Persons;
use modules\flight\frontend\providers\amadeus\methods\BookFlight;
use modules\flight\frontend\providers\amadeus\components\TravelerInfo;
use yii\base\Exception;

/**
 * Class Amadeus
 *
 * @package modules\flight\frontend\providers\amadeus
 */
class Amadeus implements FlightsProvider
{
    public $telegram;
    public $telegramBot;

    use AmadeusFormatter;

    public function __construct()
    {
        $this->telegram = new TelegramNotification();
        $this->telegramBot = new TelegramBot();
    }

    /**
     * @inheritdoc
     */
    public function search(SearchForm $param)
    {
        $return = null;
        $response = new CustomResponse();
        $uniqueUserKey = $_COOKIE['advanced-frontend'];

        try {
            if (!$this->validationCity($param)) {
                $response->setResponse('К сожалению таких рейсов нет!');
                $return = $response;
            } else {
                $soap = AmadeusSoapClient::getInstance();

//                $session = Yii::$app->session;
//                $sessionNames = Yii::$app->controller->module->params['session_var_names'];
                $token = Tokens::findOne(['user_id' => $uniqueUserKey]);

                if ($token) {
                    if (!$token::isSessionAlive($token)) {
                        $token = new Tokens();
                    }
                } else {
                    $token = new Tokens();
                }

//                $routeToken = base64_encode(md5($param['departureCode'] . $param['arrivalCode'][0] . $param['tripDate'][0] . $param['tripDateBackward']));
//                $session->set("routeKey", $routeToken);

//                if ($token != null) {
//                    if (self::isActualToken($token->created_at) == false) {
//                        /* @noinspection PhpUndefinedMethodInspection */
//                        $soap->SignOut();
////                        $session->remove($sessionNames['tokenId']);
//                        $token = new Tokens();
//                    }
//                } else {
//                    $token = new Tokens();
//                }

                $result = SearchFlight::run($param, $soap, $token);

                if ($token->id == null && $soap->getToken()) {
                    $tokenId = Tokens::saveToken($uniqueUserKey, $soap->getToken());
//                    $session->set($sessionNames['tokenId'], $tokenId);
//                    $session->set($sessionNames['cabinClass'], $param->attributes['cabin']);
                } else {
                    $tokenId = $token->id;
                }


                if ($param->attributes['tripDateBackward'] !== '') {
//                    $session->set('backward_trip', 1);
                } else {
//                    $session->set('backward_trip', 0);
                }

                if (isset($result->Errors->Error)) {

                    $token = null;

                    if ($soap->getToken() == null) {
//                        $tokens = Tokens::findOne($session->get($sessionNames['tokenId']));
//                        $token = $tokens->token;
                    } else {
                        $token = $soap->getToken();
                    }

                    $this->telegram->freeText = "SearchFlight : " . $token . " : " . json_encode($param, JSON_UNESCAPED_UNICODE) . json_encode($result->Errors);
                    $this->telegramBot->qwayTelegramNotification("onQwayError", $this->telegram, Yii::$app->params["telegram"]['flights']['ErrorKey']);

                    Yii::error(Method::getParseErrorMessage($result->Errors->Error), 'flight');
                    $response->setResponse($result->Errors->Error, 'error', true);
                    $return = $response;
                } else {
                    /* @noinspection PhpUndefinedFieldInspection */
//                    $sequence = Sequence::getSequences($result->PricedItineraries->PricedItinerary);
//                    $flights = Flight::getFlights($sequence);
//                    Registry::push($flights, '__search_result_' . $tokenId);
//                    $response->isOk = true;
//                    $response->setResponse([
//                        'flights' => $flights,
//                        'cabin' => $param->attributes['cabin']
//                    ]);
//                    $return = $response;
//                    Registry::push($result->PricedItineraries->PricedItinerary, '__search_result_' . $tokenId);
                    $flightResult = $result;
                    $flightResult->isOk = true;
                    $return = $flightResult;
                }
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'flight');
            $this->telegram->freeText = "SearchFlight : Exception : " . $e->getMessage();
            $this->telegramBot->qwayTelegramNotification("onQwayError", $this->telegram, Yii::$app->params["telegram"]['flights']['ErrorKey']);

            $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e000']);
            $return = $response;
        }

        return $return;
    }

    public function cancelBook($param)
    {
        $return = null;
        $response = new CustomResponse();

        try {
            $soap = AmadeusSoapClient::getInstance();
            $result = Cancel::run($param, $soap);
            if ($result->Success) {
                $return["status"] = "success";
                $return["text"] = "Бронирование успешно отменено!";

                $user = User::findOne(Yii::$app->user->identity->id);

                Yii::$app->mailer->compose('cancel_booking', [
                    'pnr' => $param['pnr']
                ])->setFrom(['digital@qway.kz' => 'Qway No-Reply'])
                ->setTo($user->email)
                ->setSubject('Qway: Отмена бронирования')
                ->send();

                $dataForTelegram = \modules\flight\frontend\models\base\Flights::getFlightsByFlightIds([Flights::findOne(['pnr_no' => $param["pnr"]])]);
                $dataForTelegramParsed = $this->telegram->prepareBookDetails($dataForTelegram);
                $this->telegram->setBookingDetails($dataForTelegramParsed);
                $this->telegramBot->qwayTelegramNotification("onQwayCancelPNR", $this->telegram, Yii::$app->params["telegram"]['flights']['Key']);

                Cancel::removeDataAfterCancel($param["pnr"]);

            } elseif ($result->Errors) {
                $return["status"] = "error";
                $return["text"] = (string)$result->Errors->Error["ShortText"];
                if ((string)$result->Errors->Error["ShortText"] == "EPower.PNR already cancelled") {
                    Cancel::removeDataAfterCancel($param["pnr"]);
                    $return["text"] = "Бронирование было уже отменено";
                }
            }

            /* @noinspection PhpUndefinedMethodInspection */
            $soap->signOut();

        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'flight');
            $this->telegram->freeText = "SearchFlight : Exception : " . $e->getMessage();
            $this->telegramBot->qwayTelegramNotification("onQwayError", $this->telegram, Yii::$app->params["telegram"]['flights']['ErrorKey']);
            $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e000']);
            $return = $response;
        }

        return json_encode($return);
    }

    public function getFlightRules($param)
    {
        $return = null;
        $response = new CustomResponse();
        $uniqueUserKey = $_COOKIE['advanced-frontend'];

        try {
//            $sessionNames = Yii::$app->controller->module->params['session_var_names'];
//            $session = Yii::$app->session;
            $token = Tokens::findOne(['user_id' => $uniqueUserKey]);

            if ($token == null || !$token::isSessionAlive($token)) {
                $result = new \stdClass();
                $result->Error = 'there is not any session opened';
                return $result;
            }

            $soap = AmadeusSoapClient::getInstance();

            if (self::isActualToken($token->created_at)) {
                $result = GetFlightRules::run($param, $token, $soap);
                if ($result->Success) {
                    $return = $result;
                }
            } else {
                $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e001']);
                $return = $response;
                /* @noinspection PhpUndefinedMethodInspection */
                $soap->signOut();
            }

        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'flight');
            $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e000']);
            $return = $response;
        }

        return $return;
    }

    /**
     * @inheritdoc
     */
    public function book($param)
    {
        $return = null;
        $response = new CustomResponse();
        $uniqueUserKey = $_COOKIE['advanced-frontend'];

        try {
//            $sessionNames = Yii::$app->controller->module->params['session_var_names'];
//            $session = Yii::$app->session;
            $token = Tokens::findOne(['user_id' => $uniqueUserKey]);
            $soap = AmadeusSoapClient::getInstance();

            if (self::isActualToken($token->created_at)) {
                $personModelArray = Persons::optimizationPersonsData($param);
                $result = BookFlight::run($personModelArray, $token, $soap);
                /* @noinspection PhpUndefinedFieldInspection */
                if (isset($result->Errors->Error)) {
                    /* @noinspection PhpUndefinedFieldInspection */
                    Yii::error(Method::getParseErrorMessage($result->Errors->Error), 'flight');

                    $this->telegram->freeText = "BookFlight : " . $token->token . " : " . json_encode($param, JSON_UNESCAPED_UNICODE) . json_encode($result->Errors);
                    $this->telegramBot->qwayTelegramNotification("onQwayError", $this->telegram, Yii::$app->params["telegram"]['flights']['ErrorKey']);

                    /* @noinspection PhpUndefinedFieldInspection */
                    $response->setResponse($result->Errors->Error, 'error', true);
                    $return = $response;
                } else {
                    /* @noinspection PhpUndefinedFieldInspection */
                    /* @var $flight Flights */
                    $flight = BookFlight::recordInDB($result->AirReservation);

                    $dataForTelegram = \modules\flight\frontend\models\base\Flights::getFlightsByFlightIds([$flight->id]);
                    $dataForTelegramParsed = $this->telegram->prepareBookDetails($dataForTelegram);
                    $this->telegram->setBookingDetails($dataForTelegramParsed);
                    $this->telegramBot->qwayTelegramNotification("onQwayBook", $this->telegram, Yii::$app->params["telegram"]['flights']['Key']);


                    $response->isOk = true;
                    $response->setResponse([
                        'pnrNO' => $flight->pnr_no
                    ]);
                    $token->delete();
                    $return = $response;
                }
            } else {
                $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e001']);
                $this->telegram->freeText = "BookFlight : Session Time Out : " . json_encode($param, JSON_UNESCAPED_UNICODE);
                $this->telegramBot->qwayTelegramNotification("onQwayError", $this->telegram, Yii::$app->params["telegram"]['flights']['ErrorKey']);

                $return = $response;
                /* @noinspection PhpUndefinedMethodInspection */
                $soap->signOut();
            }

        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'flight');
            $response->setResponse(Yii::$app->controller->module->params['error_codes']['am_e000']);
            $return = $response;
        }

        return $return;

    }


    public function bookId($pnr_no)
    {
        $uniqueUserKey = $_COOKIE['advanced-frontend'];
        $soap = AmadeusSoapClient::getInstance();
        $response = new CustomResponse();

        /* @noinspection PhpUndefinedMethodInspection */
        //$soap->signOut();

        $flight = \modules\flight\frontend\models\base\Flights::findOne(['pnr_no' => $pnr_no]);
        $firstPerson = self::getFirstPerson($flight->flightsPerson);

        //$travelerInfoParam = ticketExistInTBooks::run($flight->pnrNO, $firstPerson->surname, $soap);
        $result = GetPNR::run($flight->pnr_no, $firstPerson->surname, $soap);

        if (isset($result->Errors->Error)) {
            Yii::error(Method::getParseErrorMessage($result->Errors->Error), 'flight');

            /* @noinspection PhpUndefinedFieldInspection */
            $response->setResponse($result->Errors->Error, 'error', true);
            $return = $response;

            /* @noinspection PhpUndefinedMethodInspection */
            $soap->SignOut();

        } else {
            $pnrNo = $result->AirReservation->BookingReferenceID['ID_Context'];
            $travelerInfo = TravelerInfo::getTravelerInformation($result->AirReservation->TravelerInfo->AirTraveler);
            Tokens::removeToken();
            Tokens::saveToken($uniqueUserKey, $soap->getToken());
            $response->isOk = true;
            $response->setResponse([
                'travelerInfo' => $travelerInfo,
                'pnrNO' => $pnrNo
            ]);
            $return = $response;
        }

        return $return;
    }


    /**
     * @inheritdoc
     */
    public function createTicket($resultticketExistInTBooks)
    {
        $uniqueUserKey = $_COOKIE['advanced-frontend'];
        $session = Yii::$app->session;
        $sessionNames = Yii::$app->controller->module->params['session_var_names'];
        $cabinClass = $session->get($sessionNames['cabinClass']);

        $soap = AmadeusSoapClient::getInstance();
        $response = new CustomResponse();

        $result = CreateTicket::run($resultticketExistInTBooks, $soap);

        if (isset($result->Errors->Error)) {
            Yii::error(Method::getParseErrorMessage($result->Errors->Error), 'flight');
            /* @noinspection PhpUndefinedFieldInspection */
            $response->setResponse($result->Errors->Error, 'error', true);
            $return = $response;
        } else {
            $response->isOk = true;
//            $response->setResponse(ElectronicTickets::saveResult($result, $resultticketExistInTBooks['pnrNO'], $cabinClass));
            $response->setResponse(FlightElectronicTickets::addTikets($result, $resultticketExistInTBooks['pnrNO']));
            $return = $response;
//            ElectronicTickets::saveResult($result);
        }

        return $return;
    }

    /**
     * Return the truth if the token is still relevant
     * @param $tokenStartTime
     * @return bool
     */
    public static function isActualToken($tokenStartTime)
    {
        $tokenStartTimeSec = strtotime($tokenStartTime);
        $currentTime = time();
        $fifteenMin = 60 * 15; // 60 sec * 15 = 15 min

        return (($currentTime - $tokenStartTimeSec) > $fifteenMin) ? false : true;
    }

    private static function getFirstPerson($flightPersons)
    {
        foreach ($flightPersons as $flightPerson) {
            /* @var $flightPerson FlightsPerson */
            $test = $flightPerson->person_id;
            $persons = Persons::find()->where(['id' => $flightPerson->person_id])->all();
            /* @var $person Persons */
            foreach ($persons as $person) {
                if ($person->email) {
                    return $person;

                }
                continue;
            }
        }

        return null;
    }
}