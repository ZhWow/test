<?php

namespace modules\flight\frontend\providers\amadeus\services;

use Yii;

class CustomResponse
{
    public $isOk = false;
    public $response = '';

    public function setResponse($msg, $type = null, $provider = null)
    {
        if ($type) {
            $this->response = Yii::t('app', 'Sorry, ');
            if ($provider) {
                switch ($msg['ShortText']) {
                    case 'EPowerException.No Available Flights': $this->response .= Yii::t('app', 'No Available Flights'); break;
                    //default: $msg['data'] .= 'Неизвестная ошибка';
                    default: $this->response .= $msg['ShortText'];
                }
            }
        } else {
            $this->response = $msg;
        }

    }

    public function getResponse()
    {
        return $this->response;
    }
}