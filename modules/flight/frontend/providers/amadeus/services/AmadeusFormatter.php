<?php

namespace modules\flight\frontend\providers\amadeus\services;

use common\services\QLanguage;
use modules\flight\frontend\models\base\SearchForm;
use modules\countries\common\models\Cities;
use Yii;

trait AmadeusFormatter
{
    public static function getDepartureDate($date)
    {
        list($dd, $mm, $yy) = explode('-', $date);
        return $yy . '-' . $mm . '-' . $dd . 'T00:01:00'; //TODO время вылета 00:01:00
    }

    public static function getPrefixName($code, $gender) {
        $passengerCode = Yii::$app->controller->module->params['passenger_code'];
        if ($code != $passengerCode['ADULTS']) {
            return ($gender == 'male') ? 'MSTR' : 'MISS';
        } else {
            return ($gender == 'male') ? 'MR' : 'MRS';
        }
    }

    public static function getDate($date)
    {
        $day = substr($date, 0, 2);
        $month = substr($date, 3, 2);
        $year = substr($date, 6);

        return "$year-$month-$day";
    }

    public static function getPhone($number)
    {
        return str_replace('-','', $number);
    }

    public static function getGen($prefix)
    {
        if ($prefix === 'MRS') {
            return 'female';
        } else if ($prefix === 'MR') {
            return 'male';
        }
        return '';
    }

    public function validationCity(SearchForm $param)
    {
        $cityArray = $param['arrivalCity'];
        $cityArray[] = $param['departureCity'];

        $error = 0;
        foreach ($cityArray as $city) {

            $name = 'name_' . QLanguage::getCurrentLang();
            if (Cities::find()->where([$name => $city])->one() === null) {
                $error++;
            }
        }

        if ($error) {
            Yii::error('city name not valid', 'flight');
        }
        return $error ? false : true;

    }
}