<?php

namespace modules\flight\frontend\providers\alfa;

use modules\payments\AlfaBankSoapClient;

class AlfaBank
{
    private static function init()
    {
        return new AlfaBankSoapClient();
    }

    public static function getOrderStatus($orderId)
    {
        $soapClient = self::init();
        $data = ['orderParams' => ['orderId' => $orderId]];
        return $soapClient->__call('getOrderStatus', $data);
    }

    public static function registerOrder($paymentData, $returnUrl)
    {
        $soapClient = self::init();
        $amount = str_replace(' ', '', $paymentData['amount']);
        $amount .= '00';

        $data = [
            'orderParams' => [
                'returnUrl' => $returnUrl,
                'merchantOrderNumber' => urlencode($paymentData['orderNumber']),
                'amount' => urlencode($amount)
            ]
        ];

        return $soapClient->__call('registerOrder', $data);
    }

}