<?php

namespace modules\flight\frontend\providers\base;

use modules\flight\frontend\models\base\SearchForm;

interface FlightsProvider
{
    /**
     * Searches for tickets
     * @param SearchForm $param
     * @return mixed
     */
    public function search(SearchForm $param);

    /**
     * Book a ticket
     * @param $param
     * @return mixed
     */
    public function book($param);

    public function getFlightRules($param);

    public function cancelBook($param);

    public function bookId($pnr_no);

    /**
     * @return mixed
     */
    public function createTicket($resultticketExistInTBooks);
}