<?php

namespace modules\flight\frontend\models;

use Yii;
use modules\flight\frontend\models\base\Flights as BaseFlight;

/**
 * This is the model class for table "flights".
 *
 */
class Flights extends BaseFlight
{
    public static function saveSoapBookResult($result)
    {
        $model = new self();
        $model->ticket_type = (string) $result->Ticketing['TicketType'];
        $model->ticket_time_limit = (string) mb_strcut($result->Ticketing['TicketTimeLimit'], 0 ,19);
        $model->pnr_no = (string) $result->BookingReferenceID['ID_Context'];
        $model->booking_reference_id_type = (string) $result->BookingReferenceID['Type'];
        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        }
        return $model;

    }
}
