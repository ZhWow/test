<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "taxes".
 *
 * @property integer $id
 * @property integer $passengerID
 * @property integer $amount
 *
 * @property Passengers $passenger
 */
class Taxes extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passengerID', 'amount'], 'required'],
            [['passengerID', 'amount'], 'integer'],
            [['passengerID'], 'exist', 'skipOnError' => true, 'targetClass' => Passengers::className(), 'targetAttribute' => ['passengerID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passengerID' => 'Passenger ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassenger()
    {
        return $this->hasOne(Passengers::className(), ['id' => 'passengerID']);
    }
}
