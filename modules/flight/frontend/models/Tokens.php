<?php

namespace modules\flight\frontend\models;

use modules\flight\frontend\soap\AmadeusSoapClient;
use Yii;
use modules\flight\frontend\models\base\AmadeusTokens;

/**
 * This is the model class for table "amadeus_tokens".
 *
 * @property integer $id
 * @property string $pult_name
 * @property string $user_id
 * @property string $token
 * @property string $created_at
 */
class Tokens extends AmadeusTokens
{
    /**
     *  Save token
     *
     * @param string $token Id Amadeus
     * @return int|bool
     */
    public static function saveToken($userId, $token)
    {
        Yii::$app->db->open();
        $return = false;
        $model = new self();

        file_put_contents(Yii::getAlias('@xmlFiles'). '/sessions.txt', $token."\r\n");

        if (!$model->isSessionAlive($model)) {
            $model->pult_name = 'qway';
            $model->user_id = $userId;
            $model->token = $token;
            $model->created_at = date("Y-m-d H:i:s");
            if ($model->save(false)) {
                $return = $model->id;
            } else {
                Yii::error($model->getErrors(), 'flight');
            }
        }

        return $return;
    }

    public static function isSessionAlive($model)
    {
        $session_time = strtotime($model->created_at);
        $current_time = time();
        $time_difference = $current_time - $session_time;

        $response = true;

        if( $time_difference >= 855 ) {
            try {
                $soap = AmadeusSoapClient::getInstance();
                /* @noinspection PhpUndefinedMethodInspection */
                $soap->__setCookie('ASP.NET_SessionId', $model->token);
                /* @noinspection PhpUndefinedMethodInspection */
                $soap->SignOut();
                /* @noinspection PhpUndefinedMethodInspection */
                $model->delete();
                $response = false;
            } catch (\Exception $e) {
                $response = $e->getMessage();
            }
        }

        return $response;
    }

    public static function removeToken()
    {
        $sessionNames = Yii::$app->controller->module->params['session_var_names'];
        $session = Yii::$app->session;
        $session->remove($sessionNames['tokenId']);
    }
}
