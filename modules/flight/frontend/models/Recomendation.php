<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 17.10.2017
 * Time: 23:53
 */

namespace modules\flight\frontend\models;

use modules\flight\frontend\providers\amadeus\components\Passenger;
use Yii;
use modules\flight\frontend\models\base\Airlines;
use modules\flight\frontend\models\base\Airports;
use yii\web\View;

class Recomendation
{
    public static function getRecomendations($PricedItineraries) {
        $recomendation = [];
        $recomendation['countFrom'] = 0;
        $recomendation['countTo'] = 0;

//        $testt = self::getFlightNumbersPricedItineraries($PricedItineraries, 0);

        foreach ($PricedItineraries->PricedItinerary as $pricedItinerary) {
            $key = (string) $pricedItinerary['SequenceNumber'];
            $recomendation['pricedItineraries'][$key]['sequenceNumber'] = $key;
            $i = 0;
            $flightSegment = [];
            Yii::$app->formatter->locale = 'ru-RU';
            $session = $session = Yii::$app->session;

            $originDestinationoptions = self::getPricedItineraries($pricedItinerary->AirItinerary);

            foreach ($originDestinationoptions as $option) {
                $flightSegment['exchangeData'] = ($session->has('exchangeTicketId')) ? $option : false;
                $flightSegment['validatingAirlineCode'] = (string) $option['option']->FlightSegment->MarketingAirline['Code'];
                $flightSegment['validatingAirline'] = Airlines::findOne(['code_en' => (string) $option['option']->FlightSegment->MarketingAirline['Code']])->name_en;
                $flightSegment['elapsedTime'] = [
                    'h' => substr((string) $option['option']['ElapsedTime'], 0, 2),
                    'i' => substr((string) $option['option']['ElapsedTime'], 2)
                ];
                $flightSegment['refNumber'] = (string) $option['option']['RefNumber'];
                $flightSegment['directionId'] = (string) $option['option']['DirectionId'];
                $flightSegment['departureTime'] = date('H:i', strtotime((string) $option['option']->FlightSegment['DepartureDateTime']));
                $flightSegment['departureDate'] = Yii::$app->formatter->asDate(strtotime((string) $option['option']->FlightSegment['DepartureDateTime']), 'php:j M Y, D');
                $flightSegment['timestamp'] = strtotime((string) $option['option']->FlightSegment['DepartureDateTime']);
                $flightSegment['flightNumber'] = (string) $option['option']->FlightSegment['FlightNumber'];
                $flightSegment['combinationId'] = $option['combinationID'];
                $flightSegment['departureAirport'] = Airports::findOne(['code' => (string) $option['option']->FlightSegment->DepartureAirport['LocationCode']])->name_ru;
                $flightSegment['refund'] = self::getNonRefund($pricedItinerary->AirItineraryPricingInfo);

                $transferIterator = 0;
                foreach ($option['option']->FlightSegment as $segment) {
                    $flightSegment['arrivalTime'] = date('H:i', strtotime((string) $segment['ArrivalDateTime']));
                    $flightSegment['arrivalDate'] = Yii::$app->formatter->asDate(strtotime((string) $option['option']->FlightSegment['ArrivalDateTime']), 'php:j M Y, D');
                    $flightSegment['arrivalAirport'] = Airports::findOne(['code' => (string) $segment->ArrivalAirport['LocationCode']])->name_ru;
                    if ($transferIterator == 0) {
                        $transferIterator++;
                        continue;
                    }
                    $flightSegment['transfer'][$transferIterator]['key'] = $transferIterator;
                    $flightSegment['transfer'][$transferIterator]['departureAirport'] = Airports::findOne(['code' => (string) $segment->DepartureAirport['LocationCode']])->name_ru;
                    $flightSegment['transfer'][$transferIterator]['departureDate'] = Yii::$app->formatter->asDate(strtotime((string) $segment['DepartureDateTime']), 'php:j M Y, D');
                    $flightSegment['transfer'][$transferIterator]['departureTime'] = date('H:i', strtotime((string) $segment['DepartureDateTime']));
                    $flightSegment['transfer'][$transferIterator]['arrivalDate'] = Yii::$app->formatter->asDate(strtotime((string) $segment['ArrivalDateTime']), 'php:j M Y, D');
                    $flightSegment['transfer'][$transferIterator]['arrivalTime'] = date('H:i', strtotime((string) $segment['ArrivalDateTime']));
                    $transferIterator++;
                }

                $flightSegment['transferCount'] = (isset($flightSegment['transfer'])) ? count($flightSegment['transfer']) + 2 : 2;

                $i++;
                if ((string) $option['option']['DirectionId'] == 0) {
                    $recomendation['countFrom']++;
                } else {
                    $recomendation['countTo']++;
                }
                $recomendation['pricedItineraries'][$key]['flights'][] = $flightSegment;
            }

            $recomendation['pricedItineraries'][$key]['priceInfo'] = [
                'baseFare'      => (string) $pricedItinerary->AirItineraryPricingInfo->ItinTotalFare->BaseFare['Amount'],
                'markupFare'    => (string) $pricedItinerary->AirItineraryPricingInfo->ItinTotalFare->MarkupFare['Amount'],
                'totalFare'     => (string) $pricedItinerary->AirItineraryPricingInfo->ItinTotalFare->TotalFare['Amount']
            ];

        }

        return $recomendation;
    }

    public static function getPricedItineraries($airItinerary)
    {
        $originDestinationOptions = [];

        foreach ($airItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
            $indexList = explode(';', (string) $airItineraryItem['IndexList']);
            $iterator = 0;

            foreach ($indexList as $directionId => $refNumber) {

                foreach ($airItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {
                    if ($option['DirectionId'] == $directionId && $option['RefNumber'] == $refNumber) {
                        $originDestinationOptions[$iterator]['combinationID'][] = (string) $airItineraryItem['CombinationID'];
                        $originDestinationOptions[$iterator]['option'] = $option;
                    }
                    $iterator++;
                }
            }
        }

        return $originDestinationOptions;
    }

    public static function getPassengerForm($searchFormModel)
    {
        $priceInfo = $searchFormModel->PricedItinerary->AirItineraryPricingInfo->PTC_FareBreakdowns->PTC_FareBreakdown;
        $html = '';
        $contact_input = false;

        $allPassengersCount = 0;
        foreach ($priceInfo as $item) {
            $passenger = new Passenger($item);
            for ($i = 0; $i < $passenger->quantity; $i++) {

                $passengerCode = Yii::$app->controller->module->params['passenger_code'];
                if ($i === 0) {
                    if ($passenger->code == $passengerCode['ADULTS']) {
                        $contact_input = true;
                    }
                    $allPassengersCount += ($i + 1);
                } else {
                    $contact_input = false;
                    $allPassengersCount += $i;
                }

                $html[] = [
                    'model' => new Persons(),
                    'code' => $passenger->code,
                    'combinationID' => '',
                    'sequenceID' => '',
                    'contact_person' => $contact_input,
                    'num' => $allPassengersCount,
                ];
            }
        }

        return $html;
    }

    public static function getNonRefund(\SimpleXMLElement $AirItineraryPricingInfo)
    {
        $result = false;
        $pattern = '#(TICKETS.*?ARE.*?NON.*?REFUNDABLE.*?AFTER.*?DEPARTURE)|(TICKETS.*?ARE.*?NON.*?REFUNDABLE)|(PERCENT.*?PENALTY.*?APPLIES)|(CHANGE.*?PENALTY)#';
        $ticketDesignators = $AirItineraryPricingInfo->PTC_FareBreakdowns->PTC_FareBreakdown->TicketDesignators;

        foreach ($ticketDesignators->TicketDesignator as $ticketDesignator) {
            if (preg_match($pattern, (string) $ticketDesignator['TicketDesignatorExtension'])) {
                $result = true;
            }
        }

        return $result;
    }

    public static function getFlightNumbersPricedItineraries(\SimpleXMLElement $PricedItineraries, $directionId)
    {
        $searchPricedItineraries = [];
        $flightNumbers = [];
        $flightXpathNumbers = $PricedItineraries->xpath('//OriginDestinationOption[@DirectionId="' . $directionId . '"]/FlightSegment/@FlightNumber');

        foreach ($flightXpathNumbers as $flightNumber) {
            $flightNumbers[] = (string) $flightNumber['FlightNumber'];
        }

        $flightNumbers = array_unique($flightNumbers);

        foreach ($flightNumbers as $flightNumber) {
            $searchPricedItineraries[$flightNumber] = $PricedItineraries->xpath('//PricedItinerary[AirItinerary/OriginDestinationOptions/OriginDestinationOption/FlightSegment[@FlightNumber="' . $flightNumber . '"]]');
        }

        return $searchPricedItineraries;
    }
}