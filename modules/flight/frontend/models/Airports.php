<?php

namespace modules\flight\frontend\models;

use modules\flight\frontend\models\base\Airports as BaseAirports;
use common\services\QLanguage;

/**
 * This is the model class for table "airports".
 *
 * @property string $name
 */
class Airports extends BaseAirports
{
    public static $codes_array = [];
    /**
     * @param $code
     * @return array|mixed|Airports|null|ActiveRecord
     */
    public static function getAirport($code)
    {
        if (isset(self::$codes_array[$code])) {
            return self::$codes_array[$code];
        }

        $item = self::find()->andWhere(['code' => $code])->one();
        if (!$item) {
            $item = new self();
            $item->code = $code;
            $item->save();
        }
        self::$codes_array[$code] = $item;

        return $item;
    }

    public function getName()
    {
        if ($this['name_' . QLanguage::getCurrentLang()]) {
            return $this['name_' . QLanguage::getCurrentLang()];
        }
        return $this['name_' . QLanguage::getDefaultLang()];
    }

    public static function getAirportsKeyPair()
    {
        $airportsList = self::find()->select(['code','name_en'])->asArray()->all();
        $airports = [];
        foreach ($airportsList as $airport) {
            $airports[$airport['code']] = $airport['name_en'];
        }

        return $airports;
    }
}
