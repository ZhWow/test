<?php

namespace modules\flight\frontend\models;

use Yii;
use modules\flight\frontend\models\base\Persons as BasePersons;

/**
 * This is the model class for table "persons".
 */
class Persons extends BasePersons
{

    // TODO start

    public $docType = '';
    public $gen = '';
    public $citizenship = '';
    public $bestBefore = '';
    public $number = '';
    public $phone = '';
    public $combinationID = '';
    public $sequenceID = '';
    public $docID = '';
    public $docIssueCountry = '';
    public $docExpireDate = '';
    //public $email = '';
    public $phonenumber = '';

    // TODO end



    public static function getDocType()
    {
        return [
            'Identity' => 'Уд. личности',
            'Passport' => 'Пасспорт',
        ];
    }

    public static function getDocIssueCountry()
    {
        return [
            'KZ' => 'Казахстан',
            'RU' => 'Россия',
        ];
    }

    public static function getGender()
    {
        return [
            'male' => 'М', //Yii::t('app', 'Male'),
            'female' => 'Ж', //Yii::t('app', 'Female'),
        ];
    }

    public static function saveSoapBookResult($result)
    {
        $model = new self();
        $model->name = mb_strtoupper((string) $result->PersonName->GivenName);
        $model->surname = mb_strtoupper((string) $result->PersonName->Surname);
        $model->prefix = (string) $result->PersonName->NamePrefix;
        $model->birthday = (string) $result->BirthDate;
        $model->code = (string) $result['PassengerTypeCode'];
        $model->email = (string) $result->Email;
        $i = 0;
        foreach ($result->Document as $document) {
            if ($i === 0) {
                $model->doc_id = (string) $document['DocID'];
                $model->doc_type = (string) $document['DocType'];
                $model->inner_doc_type = (string) $document['InnerDocType'];
                $model->expire_date = (string) $document['ExpireDate'];
                $model->doc_issue_country = (string) $document['DocIssueCountry'];
            }
            if ($document['DocIssueLocation']) {
                $model->doc_issue_location = (string) $document['DocIssueLocation'];
            }

            $i++;
        }

        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        } else {
            return $model->id;
        }
    }

    public static function optimizationPersonsData($formData)
    {
        // This is done because the gen property is two
        // One empty, the other selected. It would be correct to have only the chosen one
        $formData['Persons']['gen'] = array_values(array_diff($formData['Persons']['gen'], ['null', '']));

        $personCount = count($formData['Persons']['code']);
        $personModelArray = [];

        for ($i = 0; $i < $personCount; $i++) {
            foreach ($formData['Persons'] as $key => $val) {
                $personModelArray[$i]['Persons'][$key] = $val[$i];
            }
        }

        return $personModelArray;
    }
}
