<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticketDesignators".
 *
 * @property integer $id
 * @property integer $passengerID
 * @property string $code
 * @property string $extension
 *
 * @property Passengers $passenger
 */
class TicketDesignators extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticketDesignators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passengerID', 'extension'], 'required'],
            [['passengerID'], 'integer'],
            [['code', 'extension'], 'string', 'max' => 255],
            [['passengerID'], 'exist', 'skipOnError' => true, 'targetClass' => Passengers::className(), 'targetAttribute' => ['passengerID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passengerID' => 'Passenger ID',
            'code' => 'Code',
            'extension' => 'Extension',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassenger()
    {
        return $this->hasOne(Passengers::className(), ['id' => 'passengerID']);
    }
}
