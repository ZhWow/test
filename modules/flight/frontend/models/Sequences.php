<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "sequences".
 *
 * @property integer $id
 * @property integer $flightID
 * @property integer $baseFareAmount
 * @property string $baseFareCurrency
 * @property integer $totalFareAmount
 * @property string $totalFareCurrency
 * @property integer $totalAmountInTicketingCurrencyAmount
 * @property string $totalAmountInTicketingCurrencyCurrency
 *
 * @property Passengers[] $passengers
 * @property Flights $flight
 */
class Sequences extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sequences';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flightID', 'baseFareAmount', 'totalFareAmount', 'totalAmountInTicketingCurrencyAmount'], 'integer'],
            [['baseFareAmount', 'totalFareAmount', 'totalAmountInTicketingCurrencyAmount'], 'required'],
            [['baseFareCurrency', 'totalFareCurrency', 'totalAmountInTicketingCurrencyCurrency'], 'string', 'max' => 10],
            [['flightID'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flightID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flightID' => 'Flight ID',
            'baseFareAmount' => 'Base Fare Amount',
            'baseFareCurrency' => 'Base Fare Currency',
            'totalFareAmount' => 'Total Fare Amount',
            'totalFareCurrency' => 'Total Fare Currency',
            'totalAmountInTicketingCurrencyAmount' => 'Total Amount In Ticketing Currency Amount',
            'totalAmountInTicketingCurrencyCurrency' => 'Total Amount In Ticketing Currency Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassengers()
    {
        return $this->hasMany(Passengers::className(), ['sequenceID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlight()
    {
        return $this->hasOne(Flights::className(), ['id' => 'flightID']);
    }
}
