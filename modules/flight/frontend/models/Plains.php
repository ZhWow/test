<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "plains".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property Segments[] $segments
 * @property Segments[] $segments0
 */
class Plains extends ActiveRecord
{
    public static $codes_array = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSegments()
    {
        return $this->hasMany(Segments::className(), ['plainID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSegments0()
    {
        return $this->hasMany(Segments::className(), ['planeID' => 'id']);
    }

    public static function getBort($code) {
        if ( isset(self::$codes_array[$code]) ) {
            return self::$codes_array[$code];
        }

        $item = self::find()->andWhere(['code' => $code])->one();
        if (!$item) {
            $item = new self();
            $item->code = $code;
            $item->save();
        }
        self::$codes_array[$code] = $item;

        return $item;
    }

    public function getName()
    {
        if ($this->name) {
            return $this->name;
        }

        return $this->code;
    }
}
