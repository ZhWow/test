<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "passengers".
 *
 * @property integer $id
 * @property integer $sequenceID
 * @property string $code
 * @property integer $quantity
 * @property integer $baseFareAmount
 * @property integer $markupFareAmount
 * @property integer $totalFareAmount
 *
 * @property Sequences $sequence
 * @property Taxes[] $taxes
 * @property TicketDesignators[] $ticketDesignators
 */
class Passengers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passengers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sequenceID', 'code', 'quantity', 'baseFareAmount', 'markupFareAmount', 'totalFareAmount'], 'required'],
            [['sequenceID', 'quantity', 'baseFareAmount', 'markupFareAmount', 'totalFareAmount'], 'integer'],
            [['code'], 'string', 'max' => 10],
            [['sequenceID'], 'exist', 'skipOnError' => true, 'targetClass' => Sequences::className(), 'targetAttribute' => ['sequenceID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sequenceID' => 'Sequence ID',
            'code' => 'Code',
            'quantity' => 'Quantity',
            'baseFareAmount' => 'Base Fare Amount',
            'markupFareAmount' => 'Markup Fare Amount',
            'totalFareAmount' => 'Total Fare Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSequence()
    {
        return $this->hasOne(Sequences::className(), ['id' => 'sequenceID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes()
    {
        return $this->hasMany(Taxes::className(), ['passengerID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketDesignators()
    {
        return $this->hasMany(TicketDesignators::className(), ['passengerID' => 'id']);
    }
}
