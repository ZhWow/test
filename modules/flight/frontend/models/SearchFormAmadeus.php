<?php

namespace modules\flight\frontend\models;

use modules\flight\frontend\models\base\SearchForm;

/**
 * Class SearchFormAmadeus
 *
 * @property int $adults
 * @property int $children
 * @property int $infants
 * @property int $isBackward If back there is a value of 1, otherwise always 0
 *
 * @package modules\flight\frontend\models
 */
class SearchFormAmadeus extends SearchForm
{
    public $adults = 1;
    public $children = 0;
    public $infants = 0;

    public $isBackward = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[
            ['departureCity', 'departureCityCode', 'departureCode', 'departureType', 'cabin'], 'string', 'max' => 256],

            [['arrivalCity', 'arrivalCityCode', 'arrivalCode', 'arrivalType', 'tripDate', ], 'each', 'rule' => ['string']],

            [['adults', 'children', 'infants', 'isBackward'], 'integer'],

            ['tripDateBackward', 'string'],

//            [['arrivalCity', 'departureCity', 'tripDate'], 'required', 'message' => \Yii::t('app', 'Required field')],
            [['arrivalCity', 'departureCity', 'tripDate'], 'required', 'message' => 'Обязательное поле'],

            /*['departureCity', 'required', 'when' => function ($model) {
                    return $model->departureCode == '';
                }, 'whenClient' => "function (attribute, value) {
                return $('#departure-code_1').val() == '';
            }", 'message' => \Yii::t('app', 'Required field correct')],*/

        ];
    }
}