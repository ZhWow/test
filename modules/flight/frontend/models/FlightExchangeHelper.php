<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 02.11.2017
 * Time: 17:07
 */

namespace modules\flight\frontend\models;

use modules\flight\frontend\models\base\BookingClassAvails;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightExchange;
use modules\flight\frontend\models\base\FlightSegments;

class FlightExchangeHelper extends FlightExchange
{
    public static function setExchangeData($data, $flightId)
    {
        $exchangeStatusSave = false;

        if ($data['dataItem'][0]) {
            foreach ($data['dataItem'] as $option) {
                $item = $option['dataItem']['exchangeData']['option'];
                $flightDetails = new FlightDetails();
                $flightDetails->flight_id = $flightId;
                $flightDetails->direction_id = (string) $item['@attributes']['DirectionId'];
                $flightDetails->elapsed_time = (string) $item['@attributes']['ElapsedTime'];
                $flightDetails->save();
                if ($flightDetails->save()) {
                    if ($item['FlightSegment'][0]) {
                        foreach ($item['FlightSegment'] as $optionItem) {
                            $flightSegment = new FlightSegments();
                            $flightSegment->flight_details_id = $flightDetails->id;
                            $flightSegment->departure_airport = $optionItem['DepartureAirport']['@attributes']['LocationCode'];
                            $flightSegment->arrival_airport = $optionItem['ArrivalAirport']['@attributes']['LocationCode'];
                            $flightSegment->departure_date_time = $optionItem['@attributes']['DepartureDateTime'];
                            $flightSegment->arrival_date_time = $optionItem['@attributes']['ArrivalDateTime'];
                            $flightSegment->flight_number = $optionItem['@attributes']['FlightNumber'];
                            $flightSegment->operating_airline = $optionItem['@attributes']['ResBookDesigCode'];
                            if ($flightSegment->save()) {
                                if ($optionItem['BookingClassAvails']['BookingClassAvail'][0]) {
                                    foreach ($optionItem['BookingClassAvails'] as $avail) {
                                        $bookingClassAvail = new BookingClassAvails();
                                        $bookingClassAvail->segment_id = $flightSegment->id;
                                        $bookingClassAvail->res_book_desig_code = $avail['@attributes']['ResBookDesigCode'];
                                        $bookingClassAvail->res_book_desig_quantity = $avail['@attributes']['ResBookDesigQuantity'];
                                        $bookingClassAvail->rph = $avail['@attributes']['RPH'];
                                        $bookingClassAvail->res_book_desig_cabin_code = $avail['@attributes']['ResBookDesigCabinCode'];
                                        $bookingClassAvail->fare_basis = $avail['@attributes']['FareBasis'];
                                        $bookingClassAvail->save();
                                        $exchangeStatusSave = true;
                                    }
                                } else {
                                    $bookingClassAvail = new BookingClassAvails();
                                    $bookingClassAvail->segment_id = $flightSegment->id;
                                    $bookingClassAvail->res_book_desig_code = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCode'];
                                    $bookingClassAvail->res_book_desig_quantity = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigQuantity'];
                                    $bookingClassAvail->rph = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['RPH'];
                                    $bookingClassAvail->res_book_desig_cabin_code = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCabinCode'];
                                    $bookingClassAvail->fare_basis = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['FareBasis'];
                                    $bookingClassAvail->save();
                                    $exchangeStatusSave = true;
                                }
                            }
                        }
                    } else {
                        $flightSegment = new FlightSegments();
                        $flightSegment->flight_details_id = $flightDetails->id;
                        $flightSegment->departure_airport = $item['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
                        $flightSegment->arrival_airport = $item['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
                        $flightSegment->departure_date_time = $item['FlightSegment']['@attributes']['DepartureDateTime'];
                        $flightSegment->arrival_date_time = $item['FlightSegment']['@attributes']['ArrivalDateTime'];
                        $flightSegment->flight_number = $item['FlightSegment']['@attributes']['FlightNumber'];
                        $flightSegment->operating_airline = $item['FlightSegment']['@attributes']['ResBookDesigCode'];
                        if ($flightSegment->save()) {
                            if ($item['FlightSegment']['BookingClassAvails']['BookingClassAvail'][0]) {
                                foreach ($item['FlightSegment']['BookingClassAvails'] as $avail) {
                                    $bookingClassAvail = new BookingClassAvails();
                                    $bookingClassAvail->segment_id = $flightSegment->id;
                                    $bookingClassAvail->res_book_desig_code = $avail['@attributes']['ResBookDesigCode'];
                                    $bookingClassAvail->res_book_desig_quantity = $avail['@attributes']['ResBookDesigQuantity'];
                                    $bookingClassAvail->rph = $avail['@attributes']['RPH'];
                                    $bookingClassAvail->res_book_desig_cabin_code = $avail['@attributes']['ResBookDesigCabinCode'];
                                    $bookingClassAvail->fare_basis = $avail['@attributes']['FareBasis'];
                                    $bookingClassAvail->save();
                                    $exchangeStatusSave = true;
                                }
                            } else {
                                $bookingClassAvail = new BookingClassAvails();
                                $bookingClassAvail->segment_id = $flightSegment->id;
                                $bookingClassAvail->res_book_desig_code = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCode'];
                                $bookingClassAvail->res_book_desig_quantity = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigQuantity'];
                                $bookingClassAvail->rph = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['RPH'];
                                $bookingClassAvail->res_book_desig_cabin_code = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCabinCode'];
                                $bookingClassAvail->fare_basis = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['FareBasis'];
                                $bookingClassAvail->save();
                                $exchangeStatusSave = true;
                            }
                        }
                    }
                }
            }
        } else {
            $item = $data['dataItem']['exchangeData']['option'];
            $flightDetails = new FlightDetails();
            $flightDetails->flight_id = $flightId;
            $flightDetails->direction_id = (string) $item['@attributes']['DirectionId'];
            $flightDetails->elapsed_time = (string) $item['@attributes']['ElapsedTime'];
            $flightDetails->save();
            if ($flightDetails->save()) {
                if ($item['FlightSegment'][0]) {
                    foreach ($item['FlightSegment'] as $optionItem) {
                        $flightSegment = new FlightSegments();
                        $flightSegment->flight_details_id = $flightDetails->id;
                        $flightSegment->departure_airport = $optionItem['DepartureAirport']['@attributes']['LocationCode'];
                        $flightSegment->arrival_airport = $optionItem['ArrivalAirport']['@attributes']['LocationCode'];
                        $flightSegment->departure_date_time = $optionItem['@attributes']['DepartureDateTime'];
                        $flightSegment->arrival_date_time = $optionItem['@attributes']['ArrivalDateTime'];
                        $flightSegment->flight_number = $optionItem['@attributes']['FlightNumber'];
                        $flightSegment->operating_airline = $optionItem['@attributes']['ResBookDesigCode'];
                        if ($flightSegment->save()) {
                            if ($optionItem['BookingClassAvails']['BookingClassAvail'][0]) {
                                foreach ($optionItem['BookingClassAvails'] as $avail) {
                                    $bookingClassAvail = new BookingClassAvails();
                                    $bookingClassAvail->segment_id = $flightSegment->id;
                                    $bookingClassAvail->res_book_desig_code = $avail['@attributes']['ResBookDesigCode'];
                                    $bookingClassAvail->res_book_desig_quantity = $avail['@attributes']['ResBookDesigQuantity'];
                                    $bookingClassAvail->rph = $avail['@attributes']['RPH'];
                                    $bookingClassAvail->res_book_desig_cabin_code = $avail['@attributes']['ResBookDesigCabinCode'];
                                    $bookingClassAvail->fare_basis = $avail['@attributes']['FareBasis'];
                                    $bookingClassAvail->save();
                                    $exchangeStatusSave = true;
                                }
                            } else {
                                $bookingClassAvail = new BookingClassAvails();
                                $bookingClassAvail->segment_id = $flightSegment->id;
                                $bookingClassAvail->res_book_desig_code = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCode'];
                                $bookingClassAvail->res_book_desig_quantity = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigQuantity'];
                                $bookingClassAvail->rph = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['RPH'];
                                $bookingClassAvail->res_book_desig_cabin_code = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCabinCode'];
                                $bookingClassAvail->fare_basis = $optionItem['BookingClassAvails']['BookingClassAvail']['@attributes']['FareBasis'];
                                $bookingClassAvail->save();
                                $exchangeStatusSave = true;
                            }
                        }
                    }
                } else {
                    $flightSegment = new FlightSegments();
                    $flightSegment->flight_details_id = $flightDetails->id;
                    $flightSegment->departure_airport = $item['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
                    $flightSegment->arrival_airport = $item['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
                    $flightSegment->departure_date_time = $item['FlightSegment']['@attributes']['DepartureDateTime'];
                    $flightSegment->arrival_date_time = $item['FlightSegment']['@attributes']['ArrivalDateTime'];
                    $flightSegment->flight_number = $item['FlightSegment']['@attributes']['FlightNumber'];
                    $flightSegment->operating_airline = $item['FlightSegment']['@attributes']['ResBookDesigCode'];
                    if ($flightSegment->save()) {
                        if ($item['FlightSegment']['BookingClassAvails']['BookingClassAvail'][0]) {
                            foreach ($item['FlightSegment']['BookingClassAvails'] as $avail) {
                                $bookingClassAvail = new BookingClassAvails();
                                $bookingClassAvail->segment_id = $flightSegment->id;
                                $bookingClassAvail->res_book_desig_code = $avail['@attributes']['ResBookDesigCode'];
                                $bookingClassAvail->res_book_desig_quantity = $avail['@attributes']['ResBookDesigQuantity'];
                                $bookingClassAvail->rph = $avail['@attributes']['RPH'];
                                $bookingClassAvail->res_book_desig_cabin_code = $avail['@attributes']['ResBookDesigCabinCode'];
                                $bookingClassAvail->fare_basis = $avail['@attributes']['FareBasis'];
                                $bookingClassAvail->save();
                                $exchangeStatusSave = true;
                            }
                        } else {
                            $bookingClassAvail = new BookingClassAvails();
                            $bookingClassAvail->segment_id = $flightSegment->id;
                            $bookingClassAvail->res_book_desig_code = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCode'];
                            $bookingClassAvail->res_book_desig_quantity = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigQuantity'];
                            $bookingClassAvail->rph = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['RPH'];
                            $bookingClassAvail->res_book_desig_cabin_code = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['ResBookDesigCabinCode'];
                            $bookingClassAvail->fare_basis = $item['FlightSegment']['BookingClassAvails']['BookingClassAvail']['@attributes']['FareBasis'];
                            $bookingClassAvail->save();
                            $exchangeStatusSave = true;
                        }
                    }
                }
            }
        }

        return $exchangeStatusSave;
    }
}