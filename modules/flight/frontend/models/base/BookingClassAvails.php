<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.09.2017
 * Time: 20:24
 */

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

class BookingClassAvails extends ActiveRecord
{
    /**
     * This is the model class for table "flt_booking_class_avails".
     *
     * @property integer $id
     * @property integer $segment_id
     * @property string $res_book_desig_code
     * @property integer $res_book_desig_quantity
     * @property string $rph
     * @property string $available_ptc
     * @property string $res_book_desig_cabin_code
     * @property string $fare_basis
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flt_booking_class_avails';
    }

    public function saveBookingClassAvails($segmentId, $FlightSegment){
        foreach ($FlightSegment->BookingClassAvails->BookingClassAvail as $BookingClassAvail) {
            $this->segment_id                   = $segmentId;
            $this->res_book_desig_code          = (string) $BookingClassAvail['ResBookDesigCode'];
            $this->res_book_desig_quantity      = (int) $BookingClassAvail['ResBookDesigQuantity'];
            $this->rph                          = (string) $BookingClassAvail['RPH'];
            $this->available_ptc                = (string) $BookingClassAvail['AvailablePTC'];
            $this->res_book_desig_cabin_code    = (string) $BookingClassAvail['ResBookDesigCabinCode'];
            $this->fare_basis                   = (string) $BookingClassAvail['FareBasis'];
            $this->save();
        }
    }
}