<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 02.10.2017
 * Time: 16:49
 */

namespace modules\flight\frontend\models\base;


use yii\db\ActiveRecord;

class Statuses extends ActiveRecord
{
    /**
     * table statuses
     *
     * @property integer $id
     * @property string $name
     * @property string $description
     */

    public static function tableName()
    {
        return 'statuses';
    }
}