<?php

namespace modules\flight\frontend\models\base;

//use modules\flight\frontend\models\ElectronicTickets;
use Yii;

/**
 * This is the model class for table "flights_person".
 *
 * @property integer $id
 * @property integer $flight_id
 * @property integer $person_id
 * @property integer $ticket_id
 *
 * @property Flights $flight
 * @property Persons $person
 */
class FlightsPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flights_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flight_id', 'person_id', 'ticket_id'], 'integer'],
//            [['flight_id'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flight_id' => 'id']],
//            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['person_id' => 'id']],
//            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElectronicTickets::className(), 'targetAttribute' => ['ticket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('flt', 'ID'),
            'flight_id' => Yii::t('flt', 'Flight ID'),
            'person_id' => Yii::t('flt', 'Person ID'),
            'ticket_id' => Yii::t('flt', 'Ticket ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlight()
    {
        return $this->hasOne(Flights::className(), ['id' => 'flight_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Persons::className(), ['id' => 'person_id']);
    }

    public static function getNotTicketedBookingIds()
    {
        return self::find()->select('DISTINCT(flight_id) as id')->where(['ticket_id' => null])->asArray()->all();
    }
}
