<?php

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "airlines".
 *
 * @property integer $id
 * @property string $code_en
 * @property string $code_ru
 * @property string $code_kz
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 * @property string $css_class_name
 */
class Airlines extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airlines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_en', 'code_ru', 'code_kz'], 'string', 'max' => 5],
            [['name_en', 'name_ru', 'name_kz', 'css_class_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_en' => Yii::t('flt', 'Code En'),
            'code_ru' => Yii::t('flt', 'Code Ru'),
            'code_kz' => Yii::t('flt', 'Code Kz'),
            'name_en' => Yii::t('flt', 'Name En'),
            'name_ru' => Yii::t('flt', 'Name Ru'),
            'name_kz' => Yii::t('flt', 'Name Kz'),
            //'css_class_name' => Yii::t('flt', 'Css Class Name'),
        ];
    }
}
