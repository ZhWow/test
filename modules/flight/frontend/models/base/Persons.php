<?php

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "persons".
 *
 * @property integer $id
 * @property string $code
 * @property string $prefix
 * @property string $name
 * @property string $surname
 * @property string $birthday
 * @property string $doc_id
 * @property string $doc_type
 * @property string $inner_doc_type
 * @property string $expire_date
 * @property string $doc_issue_country
 * @property string $doc_issue_location
 * @property string $email
 *
 * @property FlightsPerson[] $flightsPeople
 */
class Persons extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 3],
            [['prefix'], 'string', 'max' => 5],
            [['name', 'surname'], 'string', 'max' => 255],
            [['birthday', 'expire_date'], 'string', 'max' => 15],
            [['doc_id', 'doc_type', 'inner_doc_type', 'doc_issue_country', 'doc_issue_location'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('flt', 'ID'),
            'code' => Yii::t('flt', 'Code'),
            'prefix' => Yii::t('flt', 'Prefix'),
            'name' => Yii::t('flt', 'Name'),
            'surname' => Yii::t('flt', 'Surname'),
            'birthday' => Yii::t('flt', 'Birthday'),
            'doc_id' => Yii::t('flt', 'Doc ID'),
            'doc_type' => Yii::t('flt', 'Doc Type'),
            'inner_doc_type' => Yii::t('flt', 'Inner Doc Type'),
            'expire_date' => Yii::t('flt', 'Expire Date'),
            'doc_issue_country' => Yii::t('flt', 'Doc Issue Country'),
            'doc_issue_location' => Yii::t('flt', 'Doc Issue Location'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlightsPeople()
    {
        return $this->hasMany(FlightsPerson::className(), ['person_id' => 'id']);
    }
}
