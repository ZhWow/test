<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.09.2017
 * Time: 20:24
 */

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

class FlightDetails extends ActiveRecord
{
    /**
     * This is the model class for table "flt_segments".
     *
     * @property integer $id
     * @property integer $flight_id
     * @property integer $direction_id
     * @property integer $elapsed_time
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flt_flight_details';
    }

    public function getFlightSegments()
    {
        return $this->hasMany(FlightSegments::className(), ['flight_details_id' => 'id']);
    }

    public function saveFlightDetails($flightID, $option_attribute)
    {
        $this->flight_id = $flightID;
        $this->direction_id = (string) $option_attribute['DirectionId'];
        $this->elapsed_time = (string) $option_attribute['ElapsedTime'];
        $this->save();
    }

}