<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 27.09.2017
 * Time: 17:49
 */

namespace modules\flight\frontend\models\base;

use backend\modules\integra\models\base\AmadeusIntegra;
use modules\services\Notification;
use modules\users\common\models\User;
use Yii;
use yii\db\ActiveRecord;

class FlightElectronicTickets extends ActiveRecord
{
    /**
     * table flt_electronic_tickets
     *
     * @property integer $id
     * @property integer $passenger_id
     * @property string $ticket_number
     * @property string $status
     * @property string $pnr
     * @property string $iata_id
     * @property string $ticketing_date
     * @property string $issuing_airline
     * @property string $booking_ref
     * @property string $fare_calculation
     * @property string $air_fare_currency
     * @property integer $equivalent_fare
     * @property integer $total_fare
     * @property string $total_fare_currency
     * @property integer $total_tax_amount
     * @property string $control_numbers
    */

    public static function tableName()
    {
        return 'flt_electronic_tickets';
    }

    public function getPassenger()
    {
        return $this->hasOne(Persons::className(), ['id' => 'passenger_id']);
    }

    public function getItineraries()
    {
        return $this->hasMany(FlightItineraries::className(), ['ticket_id' => 'id']);
    }

    public function getExchange()
    {
        return $this->hasOne(FlightExchange::className(), ['ticket_id' => 'id']);
    }

    public function getFlightStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status']);
    }

    public function getFlightPenalty()
    {
        return $this->hasOne(FlightPenalties::className(), ['ticket_id' => 'id']);
    }

    public function getIntegraTicket()
    {
        return $this->hasOne(AmadeusIntegra::className(), ['DOCUMENT_NUMBER' => 'ticket_number']);
    }

    public function getIntegraExchangeTicket()
    {
        return $this->hasOne(AmadeusIntegra::className(), ['FO_NUMBER' => 'ticket_number']);
    }

    public static function refundTicket($ticket)
    {
        $refundAmount = 0;
        $result = false;

        if (strtotime($ticket->itineraries[0]->departure_date) > time()) {
            $penalty = (integer) $ticket->flightPenalty->refund_bd;
            $refundAmount = (integer) $ticket->total_fare - (integer) $ticket->total_tax_amount - $penalty;
        } else {
            $penalty = (integer) $ticket->flightPenalty->refund_ad;
            $refundAmount = (integer) $ticket->total_fare - ((integer) $ticket->integraTicket->FARE_USED + (integer) $ticket->integraTicket->TAXES_USED + $penalty);
        }

        $ticket->status = 4;
        $ticket->penalty = $penalty;
        $ticket->refund_amount = $refundAmount;

        if ($ticket->update() !== false) {
            $result = true;

            $user = User::findOne($ticket->user_id);

            Notification::telegramSend($ticket, 'onRefound');
            Notification::send('refund_ticket', $ticket, 'Возврат билета', $user->email);
        }


        return $result;
    }

    public static function exchangeTicket($ticket)
    {
        $exchangeAmount = 0;
        $result = false;

        if (strtotime($ticket->itineraries[0]->departure_date) > time()) {
            $penalty = (integer) $ticket->flightPenalty->exchange_bd;
        } else {
            $penalty = (integer) $ticket->flightPenalty->exchange_ad;
        }

        $integraTicket = $ticket->integraExchangeTicket;

        $newTicket = new self();
        $newTicket->passenger_id = $ticket->passenger_id;
        $newTicket->user_id = $ticket->user_id;
        $newTicket->ticket_number = $integraTicket->DOCUMENT_NUMBER;
        $newTicket->status = 1;
        $newTicket->pnr = $ticket->pnr;
        $newTicket->iata_id = $ticket->iata_id;
        $newTicket->ticketing_date = $integraTicket->DOCUMENT_ISSUED;
        $newTicket->issuing_airline = $integraTicket->VALIDATING_CARRIER;
        $newTicket->booking_ref = '1A/' . $integraTicket->RECORD_LOCATOR;
        $newTicket->fare_calculation = $integraTicket->FARE_CALCULATION;
        $newTicket->air_fare_currency = 'KZT';
        $newTicket->equivalent_fare = (integer) $integraTicket->EQUIVALENT_FARE;
        $newTicket->total_fare = (integer) $integraTicket->EQUIVALENT_FARE + (integer) $integraTicket->TAXES_TOTAL;
        $newTicket->total_fare_currency = 'KZT';
        $newTicket->total_tax_amount = (integer) $integraTicket->TAXES_TOTAL;
        $newTicket->control_numbers = $integraTicket->AIRLINE_RECORD_LOCATOR;
        $newTicket->penalty = $penalty;

        if ($newTicket->save() !== false) {
            $result = true;
            $routes = explode(';', $integraTicket->ROUTE);
            $exchange = $ticket->exchange;
            $exchange->new_ticket_id = $newTicket->id;
            if ($exchange->update() !== false) {
                Flights::deleteItems($exchange->flight_id);
            }

            foreach ($routes as $key => $route) {
                $departureDate = substr(explode(';', $integraTicket->DEPARTURE_DATES)[$key], 0, 2) . '-' . substr(explode(';', $integraTicket->DEPARTURE_DATES)[$key], 2, 2) . '-' . substr(explode(';', $integraTicket->DEPARTURE_DATES)[$key], 4) . 'T';
                $departureTime = substr(explode(';', $integraTicket->DEPARTURE_TIMES)[$key], 0, 2) . ':' . substr(explode(';', $integraTicket->DEPARTURE_TIMES)[$key], 2);
                $arrivalDate = substr(explode(';', $integraTicket->ARRIVAL_DATES)[$key], 0, 2) . '-' . substr(explode(';', $integraTicket->ARRIVAL_DATES)[$key], 2, 2) . '-' . substr(explode(';', $integraTicket->ARRIVAL_DATES)[$key], 4) . 'T';
                $arrivalTime = substr(explode(';', $integraTicket->ARRIVAL_TIMES)[$key], 0, 2) . ':' . substr(explode(';', $integraTicket->ARRIVAL_TIMES)[$key], 2);

                $itinaries = new FlightItineraries();
                $itinaries->ticket_id = $newTicket->id;
                $itinaries->from = explode('-', $route)[0];
                $itinaries->from_terminal = NULL;
                $itinaries->to = explode('-', $route)[1];
                $itinaries->to_terminal = NULL;
                $itinaries->carrier = $integraTicket->VALIDATING_CARRIER;
                $itinaries->flight_no = explode(';', $integraTicket->FLIGHT_NUMBERS)[$key];
                $itinaries->operating_airline_code = explode(';', $integraTicket->OPERATING_CARRIER)[$key];
                $itinaries->marketing_airline_code = explode(';', $integraTicket->MARKETING_CARRIER)[$key];
                $itinaries->class = explode(';', $integraTicket->BOOKING_CLASSES)[$key];
                $itinaries->departure_date = $departureDate . $departureTime;
                $itinaries->arrival_date = $arrivalDate . $arrivalTime;
                $itinaries->fare_basis = explode(';', $integraTicket->FARE_BASISES)[$key];
                $itinaries->baggage_weight = preg_replace('#\D#', '', explode(';', $integraTicket->BAGGAGE_ALLOWANCES)[$key]);
                $itinaries->baggage_weight_measure_unit = preg_replace('#\d#', '', explode(';', $integraTicket->BAGGAGE_ALLOWANCES)[$key]);
                $itinaries->status = 'OK';
                if (!$itinaries->save(false)) {
                    $test = $itinaries->getErrors();
                }
            }

            $ticket->status = 6;
            $ticket->update();

            $user = User::findOne($ticket->user_id);

            Notification::telegramSend($ticket, 'onExchangeProcessed');
            Notification::send('exchange_ticket', $ticket, 'Обмен билета', $user->email);
        }

        return $result;
    }

    public static function addTikets($response, $pnr)
    {
        $ticketIds = [];
        $session = Yii::$app->session;
        $userID = $session->get('userID');
        if ($userID == null) {
            $userID = Yii::$app->user->identity->id;
        }

        foreach ($response->AirReservation->TravelerInfo->AirTraveler as $airTraveler) {
            $person_id = null;
            foreach ($airTraveler->Document as $Document) {
                if($Document["DocType"] == "DOCS") {
                    $person_id = (int) Persons::findOne(["doc_id" => (string) $Document["DocID"]])->id;
                    if ($person_id != null) {
                        break;
                    }
                }
            }

            foreach ($airTraveler->eTicketDocuments->ETicketInfo as $document) {
                $tax = 0;
                foreach ($document->Taxes->TaxInfo as $taxInfo) {
                    $tax = $tax + (integer) $taxInfo->Amount;
                }

                $model = new self();
                $model->passenger_id       = $person_id;
                $model->user_id             = $userID;
                $model->ticket_number       = $document['TicketNumber'];
                $model->status              = 1;
                $model->pnr                 = (string) $pnr;
                $model->iata_id             = $document->IATAID;
                $model->ticketing_date      = $document->TicketingDate;
                $model->issuing_airline     = $document->IssuingAirline;
                $model->booking_ref         = $document->BookingRef;
                $model->fare_calculation    = $document->FareCalculation;
                $model->air_fare_currency   = $document->AirFareCurrency;
                $model->equivalent_fare     = $document->EquivalentFare;
                $model->total_fare          = $document->TotalFare;
                $model->total_fare_currency = $document->TotalFareCurrency;
                $model->total_tax_amount    = $tax;
                $model->control_numbers     = preg_replace('#[, ]#', '', $document->ControlNumbers);

                if ($model->save()) {
                    $ticketIds[] = $model->id;
                    foreach ($document->Itineraries->ETicketItineraryInfo as $itinerary) {
                        $flightItinerariesModel = new FlightItineraries();
                        $flightItinerariesModel->ticket_id                      = $model->id;
                        $flightItinerariesModel->from                           = $itinerary->From;
                        $flightItinerariesModel->from_terminal                  = ($itinerary->FromTerminal) ? $itinerary->FromTerminal : null;
                        $flightItinerariesModel->to                             = $itinerary->To;
                        $flightItinerariesModel->to_terminal                    = ($itinerary->ToTerminal) ? $itinerary->ToTerminal : null;
                        $flightItinerariesModel->carrier                        = $itinerary->Carrier;
                        $flightItinerariesModel->flight_no                      = $itinerary->FlightNo;
                        $flightItinerariesModel->operating_airline_code         = $itinerary->OperatingAirlineCode;
                        $flightItinerariesModel->marketing_airline_code         = $itinerary->OperatingAirlineCode;
                        $flightItinerariesModel->class                          = $itinerary->Class;
                        $flightItinerariesModel->departure_date                 = $itinerary->DepartureDate;
                        $flightItinerariesModel->arrival_date                   = $itinerary->ArrivalDate;
                        $flightItinerariesModel->fare_basis                     = $itinerary->FareBasis;
                        $flightItinerariesModel->baggage_weight                 = $itinerary->BaggageWeight;
                        $flightItinerariesModel->baggage_weight_measure_unit    = $itinerary->BaggageWeightMeasureUnit;
                        $flightItinerariesModel->status                         = $itinerary->Status;
                        $flightItinerariesModel->save();
                    }
                }
            }
        }

        if (count($ticketIds) > 0) {
            $flight = Flights::findOne(['pnr_no' => (string) $pnr]);
            foreach ($flight->flightsPerson as $flightsPerson) {
                $flightsPerson->delete();
            }
            foreach ($flight->flightDetails as $flightDetail) {
                foreach ($flightDetail->flightSegments as $flightSegment) {
                    $flightSegment->bookingClassAvails->delete();
                    $flightSegment->delete();
                }
                $flightDetail->delete();
            }
            $flight->priceInfo->delete();
            $flight->delete();
        }

        return $ticketIds;
    }

    public static function void($ticket)
    {
        $result = false;

        $ticket->status = 7;
        if ($ticket->update() !== false) {
            $result = true;

            $user = User::findOne($ticket->user_id);

            Notification::telegramSend($ticket, 'onVoid');
            Notification::send('void_ticket', $ticket, 'Анулирование билета', $user->email);
        }

        return $result;
    }

    public static function setTicketData($ticket)
    {
        $ticketData = [];
        $person = $ticket->passenger;
        $ticketData['passengers'] = (string) $person->name . ' ' . (string) $person->surname;
        $ticketData['eTicketNumber'] = (string) $ticket->ticket_number;
        $ticketData['PNR'] = (string) $ticket->pnr;
        $ticketData['email'] = (string) $person->email;

        $segmentKey = 0;
        $segmentStr = '';
        $flightNumberStr = '';
        $routeNumberStr = '';

        foreach ($ticket->itineraries as $key => $segment) {
            $ticketData['departureDate'] .= date('H:i d.m.Y', strtotime($segment->departure_date));
            $ticketData['route'] .= (string) $segment->from . '-' . (string) $segment->to;
            $segmentStr .= date('H:i d.m.Y', strtotime($segment->departure_date));
            $flightNumberStr .= $segment->operating_airline_code . '(' . $segment->flight_no . ')';
            $routeNumberStr .= $segment->from . '-' . $segment->to;

            if ($segmentKey == count($ticket->itineraries) -1) {
                $ticketData['departureDate'] .= '; ';
                $ticketData['route'] .= '; ';
            }
            $segmentKey++;
        }

        $ticketData['freeText'] = "Дата вылета: " . $segmentStr . "\r\n";
        $ticketData['freeText'] .= "Номер рейса: " . $flightNumberStr . "\r\n";
        $ticketData['freeText'] .= "Маршрут: " . $routeNumberStr . "\r\n";
        $ticketData['freeText'] .= "Стоймость: " . (string) $ticket->total_fare;

        return $ticketData;
    }

    public static function dateDifference($date_1 , $date_2)
    {
        $datetime1 = new \DateTime(date('Y-m-d', $date_1));
        $datetime2 = new \DateTime(date('Y-m-d', $date_2));

        $interval = $datetime1->diff($datetime2);

        return $interval;
    }
}