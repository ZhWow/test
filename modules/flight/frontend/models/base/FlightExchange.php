<?php
/**
 * Created by btmc.kz
 * User: Karim
 * Date: 14.09.2017
 * Time: 12:02
 */

namespace modules\flight\frontend\models\base;


use common\modules\helpers\TelegramNotification;
use yii\db\ActiveRecord;

class FlightExchange extends ActiveRecord
{
    /**
     * This is the model class for table "flt_exchange".
     *
     * @property integer $id
     * @property integer $flight_id
     * @property integer $ticket_id
     * @property integer $exchange_time
     * @property integer $is_buy
     */

    public static function tableName()
    {
        return 'flt_exchange';
    }

    public function getFlight()
    {
        return $this->hasOne(Flights::className(), ['id', 'flight_id']);
    }

    public static function setToTelegramData($ticketData)
    {
        $telegramNotification = new TelegramNotification();
        $telegramNotification->passengers = $ticketData['passengers'];
        $telegramNotification->departureDate = $ticketData['departureDate'];
        $telegramNotification->eTicketNumber = $ticketData['eTicketNumber'];
        $telegramNotification->PNR = $ticketData['PNR'];
        $telegramNotification->route = $ticketData['route'];
        $telegramNotification->email = $ticketData['email'];
//        $telegramNotification->phone = $ticketData['phone'];
        $telegramNotification->freeText = $ticketData['freeText'];

        return $telegramNotification;
    }

    public static function setTicketData($ticket)
    {
        $ticketData = [];
        $person = $ticket->passenger;
        $ticketData['passengers'] = (string) $person->name . ' ' . (string) $person->surname;
        $ticketData['eTicketNumber'] = (string) $ticket->ticket_number;
        $ticketData['PNR'] = (string) $ticket->pnr;
        $ticketData['email'] = (string) $person->email;
//        $ticketData['phone'] = (string) $ticket['person']->phone;
        $segmentKey = 0;
        foreach ($ticket->itineraries as $key => $segment) {
            $ticketData['departureDate'] .= (string) $segment->departure_date;
            $ticketData['route'] .= (string) $segment->from . '-' . (string) $segment->to;
            if ($segmentKey == count($ticket->itineraries) -1) {
                $ticketData['departureDate'] .= '; ';
                $ticketData['route'] .= '; ';
            }
            $segmentKey++;
        }

        $model = new self();

        $exchangeData = $model::find()->where(['ticket_id' => (integer) $ticket->id])->one();
        $exchangeFlight = Flights::findOne(['exchange_id' => $exchangeData->id]);

        $exchangeSegmentStr = '';
        $flightNumberStr = '';
        $routeNumberStr = '';
        foreach ($exchangeFlight->flightDetails as $flightDetail) {
            $segmentKey = 0;
            foreach ($flightDetail->flightSegments as $exchangeSegment) {
                $exchangeSegmentStr .= (string) $exchangeSegment->departure_date_time;
                $flightNumberStr .= (string) $exchangeSegment->flight_number;
                $routeNumberStr .= (string) $exchangeSegment->departure_airport . '-' . $exchangeSegment->arrival_airport;
                if ($segmentKey == count($flightDetail->flightSegments) -1) {
                    $exchangeSegmentStr .=  '; ';
                    $flightNumberStr .=  '; ';
                    $routeNumberStr .=  '; ';
                }
                $segmentKey++;
            }
        }
        $ticketData['freeText'] = "Дата вылета: " . $exchangeSegmentStr . "\r\n";
        $ticketData['freeText'] .= "Номер рейса: " . $flightNumberStr . "\r\n";
        $ticketData['freeText'] .= "Маршрут: " . $routeNumberStr . "\r\n";
        $ticketData['freeText'] .= "Стоймость: " . (string) $exchangeFlight->priceInfo->total_fare_amount;
        $ticket->status = 5;
        $ticket->update();

        return $ticketData;
    }

    public static function getExchangeTicketData($flightId)
    {
        $flightDetails = FlightDetails::findAll(['flight_id' => $flightId]);
        foreach ($flightDetails as $keys => $flightDetail) {
            $dataProvider['elapsed'] = $flightDetail->elapsed_time;
            $dataProvider['segments'][] = FlightSegments::findOne(['flight_details_id' => $flightDetail->id]);
        }
        $dataProvider['price_info'] = FlightPriceInfo::findOne(['flight_id' => $flightId]);

        return $dataProvider;
    }

    public static function exchangeDateDiff($exchangeDate)
    {
        $result = [
            'min'           => 0,
            'sec'           => 0,
            'limit'    => false
        ];
        $datetime1 = new \DateTime($exchangeDate);
        $datetime2 = new \DateTime('NOW');
        $interval = $datetime1->diff($datetime2);

        if ($interval->h == 0 && $interval->i < 15) {
            $result = [
                'min'           => 15 - $interval->i,
                'sec'           => 60 - $interval->s,
                'limit'    => true
            ];
        }

        return $result;
    }
}