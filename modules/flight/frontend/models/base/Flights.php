<?php

namespace modules\flight\frontend\models\base;

use modules\flight\frontend\models\ElectronicTickets as ElectronicTicketsBase;
use modules\flight\frontend\models\Persons as PersonsModel;
use modules\flight\frontend\models\Segments;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "flights".
 *
 * @property integer $id
 * @property string $validating_airline_code
 * @property string $ticket_time_limit
 * @property string $ticket_type
 * @property string $booking_reference_id_type
 * @property string $pnr_no
 *
 * @property FlightsPerson[] $flightsPeople
 */
class Flights extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['validating_airline_code'], 'string', 'max' => 10],
            [['ticket_time_limit', 'ticket_type', 'booking_reference_id_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('flt', 'ID'),
            'validating_airline_code' => Yii::t('flt', 'Validating Airline Code'),
            'ticket_time_limit' => Yii::t('flt', 'Ticket Time Limit'),
            'ticket_type' => Yii::t('flt', 'Ticket Type'),
            'booking_reference_id_type' => Yii::t('flt', 'Booking Reference Id Type'),
            'pnr_no' => Yii::t('flt', 'Pnr No'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlightsPerson()
    {
        return $this->hasMany(FlightsPerson::className(), ['flight_id' => 'id']);
    }

    public function getElectronicTickets()
    {
        return $this->hasMany(ElectronicTicketsBase::className(),['pnr_no' => 'pnr_no']);
    }

    public function getFlightDetails()
    {
        return $this->hasMany(FlightDetails::className(), ['flight_id' => 'id']);
    }

    public function getPriceInfo()
    {
        return $this->hasOne(FlightPriceInfo::className(), ['flight_id' => 'id']);
    }

    public static function getFlightsByFlightIds($flight_ids)
    {
        $dataProvider = [];
        foreach ($flight_ids as $key => $flight_id) {
            $person_id = FlightsPerson::find()->select('person_id as id')->where(['flight_id' => $flight_id])->asArray()->all();
            foreach ($person_id as $person_idItem) {
                $dataProvider[$key]["PAX"][] = PersonsModel::find()->where(['id' => $person_idItem["id"]])->asArray()->one();
            }

            $flightDetailsIds = FlightDetails::find()->select('id')->where(['flight_id' => $flight_id])->asArray()->all();
            foreach ($flightDetailsIds as $flightDetailsId) {
                $dataProvider[$key]["SEGMENTS"][] = FlightSegments::find()->where(['flight_details_id' => $flightDetailsId['id']])->asArray()->all();
            }

            $dataProvider[$key]["PRICEINFO"] = FlightPriceInfo::find()->where(['flight_id' => $flight_id])->asArray()->one();
            $dataProvider[$key]["FLIGHTINFO"] = Flights::find()->where(['id' => $flight_id])->asArray()->one();
        }
        return $dataProvider;
    }

    public static function deleteItems($flightId)
    {
        $model = self::findOne($flightId);

        foreach ($model->flightsPerson as $flightsPerson) {
            $flightsPerson->delete();
        }

        foreach ($model->flightDetails as $flightDetail) {
            foreach ($flightDetail->flightSegments as $flightSegment) {
                $flightSegment->delete();
            }
            $flightDetail->delete();
        }

        $model->priceInfo->delete();
        $model->delete();
    }

    public static function getFlightsArrayDataProvider($flight_ids)
    {
        $response = [];
        $bookings = self::getFlightsByFlightIds($flight_ids);
        foreach ($bookings as $key => $booking) {
            $response[$key]["departure_airport"] = NULL;
            $response[$key]["arrival_airport"] = NULL;
            $response[$key]["departure_date_time"] = NULL;
            $response[$key]["arrival_date_time"] = NULL;
            $response[$key]["flight_number"] = NULL;
            $response[$key]["flight_duration"] = NULL;
            $response[$key]["code"] = NULL;
            $response[$key]["prefix"] = NULL;
            $response[$key]["name"] = NULL;
            $response[$key]["surname"] = NULL;
            $response[$key]["email"] = NULL;
            $response[$key]["firstPaxSurname"] = $booking["PAX"][0]["surname"];

            $response[$key]["pnr_no"] = $booking["FLIGHTINFO"]["pnr_no"];
            $response[$key]["ticket_time_limit"] = $booking["FLIGHTINFO"]["ticket_time_limit"];
            $response[$key]["ticket_time_limit"] = $booking["FLIGHTINFO"]["ticket_time_limit"];
            foreach ($booking["SEGMENTS"] as $segmentKey => $segment) {
                $breakpoint1 = ($segmentKey == count($booking["SEGMENTS"]) - 1) ? "" : "<br />";
                foreach ($segment as $segmentItemKey => $segmentItem) {
                    $breakpoint2 = ($segmentItemKey == count($segment) - 1) ? "" : "<br />";
                    $response[$key]["departure_airport"] .= $segmentItem['departure_airport'] . $breakpoint1 . $breakpoint2;
                    $response[$key]["arrival_airport"] .= $segmentItem["arrival_airport"] . $breakpoint1 . $breakpoint2;
                    $response[$key]["departure_date_time"] .= date("H:i d.m.Y",strtotime($segmentItem["departure_date_time"])) . $breakpoint1 . $breakpoint2;
                    $response[$key]["arrival_date_time"] .= date("H:i d.m.Y",strtotime($segmentItem["arrival_date_time"])) . $breakpoint1 . $breakpoint2;
                    $response[$key]["flight_number"] .= $segmentItem["operating_airline"] . "-" . $segmentItem["flight_number"] . $breakpoint1 . $breakpoint2;
                    $response[$key]["flight_duration"] .= date("H:i", strtotime($segmentItem["flight_duration"])) . $breakpoint1 . $breakpoint2;
                }
            }
            foreach ($booking["PAX"] as $paxKey => $pax) {
                $paxBreakPoint = ($paxKey == count($booking["PAX"]) - 1) ? "" : "<br />";
                $response[$key]["code"] .= $pax["code"] . $paxBreakPoint;
                $response[$key]["prefix"] .= $pax["prefix"] . $paxBreakPoint;
                $response[$key]["name"] .= $pax["name"] . $paxBreakPoint;
                $response[$key]["surname"] .= $pax["surname"] . $paxBreakPoint;
                $response[$key]["email"] .= $pax["email"] . $paxBreakPoint;
            }
        }
        return $response;
    }
}
