<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.09.2017
 * Time: 20:24
 */

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

class FlightPriceInfo extends ActiveRecord
{
    /**
     * This is the model class for table "flt_segments".
     *
     * @property integer $id
     * @property integer $flight_id
     * @property integer $base_fare_amount
     * @property integer $taxes_amount
     * @property integer $total_fare_amount
     *
     * @inheritdoc
     */
    public $vat;
    public static function tableName()
    {
        return 'flt_price_info';
    }

    public function savePriceInfo($flightID,$ItinTotalFare){
        $this->flight_id = $flightID;
        $this->base_fare_amount = (int) $ItinTotalFare->BaseFare['Amount'];
        $this->taxes_amount = (int) $ItinTotalFare->TotalFare['Amount'] - (int) $ItinTotalFare->BaseFare['Amount'];
        $this->total_fare_amount = (int) $ItinTotalFare->TotalFare['Amount'];
        $this->save();
    }
}