<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 27.09.2017
 * Time: 17:57
 */

namespace modules\flight\frontend\models\base;

use modules\countries\common\models\base\Cities;
use yii\db\ActiveRecord;

class FlightItineraries extends ActiveRecord
{
    /**
     * table flt_itineraries
     *
     * @property integer $id
     * @property integer $ticket_id
     * @property string $from
     * @property string $from_terminal
     * @property string $to
     * @property string $to_terminal
     * @property string $carrier
     * @property string $operating_airline_code
     * @property string $marketing_airline_code
     * @property string $class
     * @property string $departure_date
     * @property string $arrival_date
     * @property string $fare_basis
     * @property string $baggage_weight
     * @property string $baggage_weight_measure_unit
     * @property string $status
     */

    public static function tableName()
    {
        return 'flt_itineraries';
    }

    public function getCityFrom()
    {
        return $this->hasOne(Cities::className(), ['code' => 'from']);
    }

    public function getCityTo()
    {
        return $this->hasOne(Cities::className(), ['code' => 'to']);
    }

    public function getFlightTickets()
    {
        $this->hasOne(FlightElectronicTickets::className(), ['id' => 'ticket_id']);
    }

    public static function getActiveTickets()
    {
        $tickets = [];
        $model = new self();
        $itineraries =  $model->find()->select('ticket_id')->distinct()->where('STR_TO_DATE(departure_date, "%Y-%m-%d") >= NOW()')->all();
        foreach ($itineraries as $itinerary) {
            $tickets[] = FlightElectronicTickets::findOne($itinerary->ticket_id);
        }

        return $tickets;
    }
}