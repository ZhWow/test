<?php

namespace modules\flight\frontend\models\base;

use yii\base\Model;
use Yii;

/**
 * Class SearchForm
 *
 * @package modules\flight\frontend\models\base
 */
class SearchForm extends Model
{
    public $departureCity = '';
    public $departureCityCode = '';
    public $departureCode = '';
    public $departureType = '';

    public $arrivalCity = '';
    public $arrivalCityCode = '';
    public $arrivalCode = '';
    public $arrivalType = '';

    public $tripDate;
    public $tripDateBackward;

    public $cabin;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'departureCity' => 'Пункт вылета',
            'arrivalCity' => 'Пункт прилета',
            'tripDate' => 'Дата вылета',
        ];
    }

    public static function getCabin()
    {
        return [
            'Economy' => 'Эконом',//Yii::t('flt', 'Economy'),
            'Business' => 'Бизнес',//Yii::t('flt', 'Business'),
            'First' => 'Первый',//Yii::t('flt', 'First'),
        ];
    }
}