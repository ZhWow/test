<?php

namespace modules\flight\frontend\models\base;

use Yii;

/**
 * This is the model class for table "plains".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 */
class Plains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('flt', 'ID'),
            'code' => Yii::t('flt', 'Code'),
            'name' => Yii::t('flt', 'Name'),
        ];
    }
}
