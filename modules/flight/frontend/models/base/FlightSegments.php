<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.09.2017
 * Time: 20:24
 */

namespace modules\flight\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

class FlightSegments extends ActiveRecord
{
    /**
     * This is the model class for table "flt_segments".
     *
     * @property integer $id
     * @property integer $flight_details_id
     * @property string $departure_airport
     * @property string $arrival_airport
     * @property string $departure_date_time
     * @property string $arrival_date_time
     * @property integer $flight_number
     * @property string $operating_airline
     * @property string $marketing_airline
     * @property string $equipment
     * @property string $status
     *
     * @inheritdoc
     */
    public $operating_airline_name;
    public $departure_airport_name;
    public $arrival_airport_name;

    public static function tableName()
    {
        return 'flt_segments';
    }

    public function getBookingClassAvails()
    {
        return $this->hasOne(BookingClassAvails::className(), ['segment_id' => 'id']);
    }

    public function getFlightSegmentByDetailId($FlightDetailId)
    {
        return $this::findOne(['flight_details_id' => $FlightDetailId]);
    }

    public function saveFlightSegment($FlightDetailsId, $FlightSegment)
    {
        $this->flight_details_id    = $FlightDetailsId;
        $this->departure_airport    = (string) $FlightSegment->DepartureAirport['LocationCode'];
        $this->arrival_airport      = (string) $FlightSegment->ArrivalAirport['LocationCode'];
        $this->departure_date_time  = (string) $FlightSegment['DepartureDateTime'];
        $this->arrival_date_time    = (string) $FlightSegment['ArrivalDateTime'];
        $this->flight_number        = (int) $FlightSegment['FlightNumber'];
        $this->operating_airline    = (string) $FlightSegment->OperatingAirline['Code'];
        $this->marketing_airline    = (string) $FlightSegment->MarketingAirline['Code'];
        $this->equipment            = (string) $FlightSegment->Equipment['AirEquipType'];
        $this->flight_duration      = (string) $FlightSegment->FlightDuration;
        $this->status               = (string) $FlightSegment['Status'];
        $this->save();
    }

    public static function getActiveSegmentIds()
    {
        return self::find()->select('flight_details_id as id')->where("STR_TO_DATE(departure_date_time, '%Y-%m-%d') >= NOW()")->asArray()->all();
    }

    public static function getActiveSegments()
    {
        return self::find()->where("STR_TO_DATE(departure_date_time, '%Y-%m-%d') >= NOW()")->asArray()->all();
    }
}