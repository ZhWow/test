<?php

namespace modules\flight\frontend\models\base;

use Yii;
use modules\countries\common\models\Cities;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "airports".
 *
 * @property integer $id
 * @property string $code
 * @property integer $city_id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 * @property string $latitude
 * @property string $longitude
 * @property string $altitude
 *
 * @property Cities $city
 */
class Airports extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['code'], 'string', 'max' => 5],
            [['name_en', 'name_ru', 'name_kz', 'latitude', 'longitude', 'altitude'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('flt', 'Code'),
            'city_id' => Yii::t('flt', 'City ID'),
            'name_en' => Yii::t('flt', 'Name En'),
            'name_ru' => Yii::t('flt', 'Name Ru'),
            'name_kz' => Yii::t('flt', 'Name Kz'),
            'latitude' => Yii::t('flt', 'Latitude'),
            'longitude' => Yii::t('flt', 'Longitude'),
            'altitude' => Yii::t('flt', 'Altitude'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }
}
