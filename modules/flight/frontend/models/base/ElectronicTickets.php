<?php

namespace modules\flight\frontend\models\base;

use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\Segments;
use modules\flight\frontend\providers\amadeus\components\Flight;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\providers\amadeus\components\Person;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "electronic_tickets".
 *
 * @property integer $id
 * @property string $ticket_number
 * @property integer $user_id
 * @property string $created_at
 * @property string $flight_number
 * @property string $res_book_desig_code
 * @property string $status
 * @property string $equipment
 * @property integer $iataid
 * @property string $operating_airline
 * @property string $validating_airline
 * @property string $marketing_airline
 * @property string $departure_airport
 * @property string $departure_terminal
 * @property string $departure_datetime
 * @property string $arrival_airport
 * @property string $arrival_terminal
 * @property string $arrival_datetime
 * @property string $elapsed_time
 * @property string $baggage_weight
 * @property string $baggage_quantity
 * @property string $baggage_weight_measure_unit
 * @property string $fare_basis
 * @property string $base_fare
 * @property string $fare_calculation
 * @property string $tax
 * @property string $service_fee
 * @property string $total_fare
 * @property integer $exchange
 * @property integer $refunds
 * @property integer $exchange_before_departure
 * @property integer $exchange_after_departure
 * @property integer $refunds_before_departure
 * @property integer $refunds_after_departure
 * @property string $pnr_no
 * @property string $cabin_class
 */
class ElectronicTickets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'electronic_tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'iataid', 'exchange', 'refunds', 'exchange_before_departure', 'exchange_after_departure', 'refunds_before_departure', 'refunds_after_departure'], 'integer'],
            [[/*'cabin_class', 'pnr_no', */'ticket_number', 'created_at', 'flight_number', 'res_book_desig_code', 'equipment', 'operating_airline', 'validating_airline', 'marketing_airline', 'departure_airport', 'departure_terminal', 'departure_datetime', 'arrival_airport', 'arrival_terminal', 'arrival_datetime', 'elapsed_time', 'baggage_weight', 'baggage_quantity', 'baggage_weight_measure_unit', 'fare_basis', 'base_fare', 'fare_calculation', 'tax', 'service_fee', 'total_fare'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('flt', 'ID'),
            'ticket_number' => Yii::t('flt', 'Ticket Number'),
            'user_id' => Yii::t('flt', 'User ID'),
            'created_at' => Yii::t('flt', 'Created At'),
            'flight_number' => Yii::t('flt', 'Flight Number'),
            'res_book_desig_code' => Yii::t('flt', 'Res Book Desig Code'),
            'status' => Yii::t('flt', 'Status'),
            'equipment' => Yii::t('flt', 'Equipment'),
            'iataid' => Yii::t('flt', 'Iataid'),
            'operating_airline' => Yii::t('flt', 'Operating Airline'),
            'validating_airline' => Yii::t('flt', 'Validating Airline'),
            'marketing_airline' => Yii::t('flt', 'Marketing Airline'),
            'departure_airport' => Yii::t('flt', 'Departure Airport'),
            'departure_terminal' => Yii::t('flt', 'Departure Terminal'),
            'departure_datetime' => Yii::t('flt', 'Departure Datetime'),
            'arrival_airport' => Yii::t('flt', 'Arrival Airport'),
            'arrival_terminal' => Yii::t('flt', 'Arrival Terminal'),
            'arrival_datetime' => Yii::t('flt', 'Arrival Datetime'),
            'elapsed_time' => Yii::t('flt', 'Elapsed Time'),
            'baggage_weight' => Yii::t('flt', 'Baggage Weight'),
            'baggage_quantity' => Yii::t('flt', 'Baggage Quantity'),
            'baggage_weight_measure_unit' => Yii::t('flt', 'Baggage Weight Measure Unit'),
            'fare_basis' => Yii::t('flt', 'Fare Basis'),
            'base_fare' => Yii::t('flt', 'Base Fare'),
            'fare_calculation' => Yii::t('flt', 'Fare Calculation'),
            'tax' => Yii::t('flt', 'Tax'),
            'service_fee' => Yii::t('flt', 'Service Fee'),
            'total_fare' => Yii::t('flt', 'Total Fare'),
            'exchange' => Yii::t('flt', 'Exchange'),
            'refunds' => Yii::t('flt', 'Refunds'),
            'exchange_before_departure' => Yii::t('flt', 'Exchange Before Departure'),
            'exchange_after_departure' => Yii::t('flt', 'Exchange After Departure'),
            'refunds_before_departure' => Yii::t('flt', 'Refunds Before Departure'),
            'refunds_after_departure' => Yii::t('flt', 'Refunds After Departure'),
        ];
    }

    public function getFlight()
    {
        return $this->hasMany(Flights::className(),['pnr_no' => 'pnr_no']);
    }

    public function getTicketsHistory($userId)
    {
        $historyTickets = $this::findAll(['user_id' => $userId]);
        $returnData = [];
        if ($historyTickets) {
            foreach ($historyTickets as $key => $historyTicket) {
                $flightPerson = FlightsPerson::findOne(['ticket_id' => $historyTicket->id]);
                if ($flightPerson) {
                    $returnData[$key]['person'] = Persons::findOne(['id' => $flightPerson->person_id]);
                }
                $flights = Flights::findAll(['pnr_no' => (string) $historyTicket->pnr_no]);
                if ($flights) {
                    foreach ($flights as $flight) {
                        $flightDetails = FlightDetails::findAll(['flight_id' => $flight->id]);
                        if ($flightDetails) {
                            foreach ($flightDetails as $flightDetail) {
                                $flightSegments = FlightSegments::find()->where(['flight_details_id' => $flightDetail->id])->all();
                                if ($flightSegments) {
                                    $returnData[$key]['ticket'] = $historyTicket;
                                    $returnData[$key]['flightDetail'] = $flightDetail;
                                    $returnData[$key]['flightSegments'] = $flightSegments;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $returnData;
    }

    public function getActiveTicketById($ticket_id)
    {
        $historyTickets = $this::findAll($ticket_id);
        $returnData = [];
        if ($historyTickets) {
            foreach ($historyTickets as $key => $historyTicket) {
                $flightPerson = FlightsPerson::findOne(['ticket_id' => $historyTicket->id]);
                if ($flightPerson) {
                    $returnData[$key]['person'] = Persons::findOne(['id' => $flightPerson->person_id]);
                }

                $flights = Flights::findAll(['pnr_no' => (string) $historyTicket->pnr_no]);
                if ($flights) {
                    foreach ($flights as $flight) {
                        $flightDetails = FlightDetails::findAll(['flight_id' => $flight->id]);
                        $flightPriceInfo = FlightPriceInfo::findOne(['flight_id' => $flight->id]);
                        if ($flightDetails) {
                            foreach ($flightDetails as $flightDetail) {
                                $flightSegments = FlightSegments::find()->where(['flight_details_id' => $flightDetail->id])->all();
                                if ($flightSegments) {
                                    $returnData[$key]['ticket'] = $historyTicket;
                                    $returnData[$key]['flightDetail'] = $flightDetail;
                                    $returnData[$key]['flightSegments'] = $flightSegments;
                                    $returnData[$key]['flightPriceInfo'] = $flightPriceInfo;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $returnData;
    }

    public function getActiveTickets()
    {
        $tickets = [];
        $ticket_ids = $this->getActiveTicketIds();
        foreach ( $ticket_ids as $ticket_id ) {
            $tickets = $this->getActiveTicketById($ticket_id);
        }

        return $tickets;
    }

    public function getActiveTicketIds()
    {
        $activeFlightDetails = FlightSegments::find()->select('flight_details_id as id')->where("STR_TO_DATE(departure_date_time, '%Y-%m-%d') >= NOW()")->asArray()->all();
        $test = '123';
        $activeFlights = [];
        $activeTickets = [];

        foreach ($activeFlightDetails as $activeFlightDetail) {
            $activeFlights[] = FlightDetails::findOne($activeFlightDetail)->flight_id;
            foreach ($activeFlights as $activeFlight) {
                $flightsSearch = FlightsPerson::find()->select('ticket_id as id')->where(['flight_id' => $activeFlight])->asArray()->one();
                if ($flightsSearch['id'] != null) {
                    $activeTickets[] = $flightsSearch['id'];
                }
            }
        }

        return $activeTickets;
    }
}
