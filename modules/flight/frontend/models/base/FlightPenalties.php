<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 02.10.2017
 * Time: 17:43
 */

namespace modules\flight\frontend\models\base;

use yii\db\ActiveRecord;

class FlightPenalties extends ActiveRecord
{
    /**
     * table flt_penalties
     *
     * @property integer $id
     * @property integer $refund_bd
     * @property integer $refund_ad
     * @property integer $refund_ns
     * @property integer $exchange_bd
     * @property integer $exchange_ad
     * @property integer $exchange_ns
     */

    public static function tableName()
    {
        return 'flt_penalties';
    }

    public function rules()
    {
        return [
            [['ticket_id', 'refund_bd', 'refund_ad', 'refund_ns', 'exchange_bd', 'exchange_ad', 'exchange_ns'], 'integer']
        ];
    }
}