<?php

namespace modules\flight\frontend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "amadeus_tokens".
 *
 * @property integer $id
 * @property string $token
 * @property string $created_at
 */
class AmadeusTokens extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'amadeus_tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['created_at'], 'safe'],
            [['token'], 'string', 'max' => 255],
        ];
    }

//    public function behaviors()
//    {
//        return [
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_at',
//                'updatedAtAttribute' => null,
//                'value' => new Expression('NOW()'),
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => Yii::t('flt', 'ID'),
//            'token' => Yii::t('flt', 'Token'),
//            'created_at' => Yii::t('flt', 'Created At'),
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->token = $this->token . '; path=/; HttpOnly';
            return true;
        } else {
            return false;
        }
    }
}
