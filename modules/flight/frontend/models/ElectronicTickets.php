<?php

namespace modules\flight\frontend\models;

use modules\flight\frontend\models\Airlines;
use modules\countries\common\models\Cities;
use modules\flight\frontend\models\base\ElectronicTickets as BaseElectronicTickets;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\providers\amadeus\components\Person;
use modules\flight\frontend\models\Persons;

use Yii;

/**
 * This is the model class for table "t_electronictickets".
 *
 * @property string $purchaseDate
 * @property string $departureCity
 * @property string $departureDateTime
 * @property string $departureDate
 * @property string $arrivalCity
 * @property string $arrivalDateTime
 * @property string $arrivalDate
 * @property string $elapsed
 */
class ElectronicTickets extends BaseElectronicTickets
{
    const PREFIX = ';';
    const PENALTY_READY = 1;
    const PENALTY_REQUEST = 2;


    public static function saveResult($response, $pnrNo, $cabinClass)
    {
        $modelIds = [];
        foreach ($response->AirReservation->TravelerInfo->AirTraveler as $airTraveler) {
            $tickets = explode(';', $airTraveler['eTicketNumber']);
            $ticketsNotEmptyVal = array_values(array_diff($tickets, [null, '']));
            $ticketsCount = count($ticketsNotEmptyVal);
            for ($i = 0; $i < $ticketsCount; $i++) {
                $modelIds[] = self::itemSave($response, $airTraveler, $i, $pnrNo, $cabinClass);
            }

            // get person_id of traveller in persons table in database to link it to the ticket
            $person_id = null;
            foreach ($airTraveler->Document as $Document) {
                if($Document["DocType"] == "DOCS") {
                    $person_id = (int) Persons::findOne(["doc_id" => (string) $Document["DocID"]])->id;
                }
            }

            // get ticket_id of the traveller and save link to the traveller into the database table - flight_person
            $ticket_id = null;

            $travelerTickets = array_diff(explode(";",$airTraveler["eTicketNumber"]),[null,'']);
            foreach ($travelerTickets as $travelerTicket) {

                $ticket_id = ElectronicTickets::findOne(["ticket_number" => $travelerTicket])->id;
                $flight_id = Flights::findOne(['pnr_no' => (string) $pnrNo])->id;

                $updateFlightPersonData = FlightsPerson::findOne(['flight_id' => $flight_id, 'person_id' => $person_id]);
                $updateFlightPersonData->flight_id = $flight_id;
                $updateFlightPersonData->person_id = $person_id;
                $updateFlightPersonData->ticket_id = $ticket_id;
                $updateFlightPersonData->update();
            }

        }
        return $modelIds;
    }

    private static function itemSave($response, $airTraveler, $i, $pnrNo, $cabinClass)
    {
        $session = Yii::$app->session;
        $userID = $session->get('userID');
        if ($userID == null) {
            $userID = Yii::$app->user->identity->id;
        }
        $model = new self();
        $model->ticket_number = (string) explode(';', $airTraveler['eTicketNumber'])[$i];

        $model->status = 1;
        $model->iataid = (string) $airTraveler->eTicketDocuments->ETicketInfo->IATAID;
        $model->created_at = (string) $airTraveler->eTicketDocuments->ETicketInfo->TicketingDate;
        $model->status = (string) $airTraveler->eTicketDocuments->ETicketInfo->Itineraries['Status'];
        $model->baggage_weight = (string) $airTraveler->eTicketDocuments->ETicketInfo->Itineraries->ETicketItineraryInfo->BaggageWeight;
        $model->baggage_weight_measure_unit = (string) $airTraveler->eTicketDocuments->ETicketInfo->Itineraries->ETicketItineraryInfo->BaggageWeightMeasureUnit;
        $model->fare_calculation = (string) $airTraveler->eTicketDocuments->ETicketInfo->FareCalculation;

//        $model->PNRNo = (string) $response->AirReservation->BookingReferenceID['ID_Context'];
        $model->base_fare = (string) $response->AirReservation->PriceInfo->ItinTotalFare->BaseFare['Amount'];
        $model->service_fee = (string) $response->AirReservation->PriceInfo->ServiceFees->ServiceFee['Amount'];
        $model->total_fare = (string) $response->AirReservation->PriceInfo->ItinTotalFare->TotalFare['Amount'];
        $model->tax = (string) $response->AirReservation->PriceInfo->PTC_FareBreakdowns->PTC_FareBreakdown->PassengerFare->Taxes->Tax['Amount'];
        $model->user_id = (int) $userID;
        $model->cabin_class = (string) $cabinClass;

        $flightNumber = '';
        $resBookDesigCode = '';
        $equipment = '';
        $operatingAirline = '';
        $marketingAirline = '';
        $departureAirport = '';
        $arrivalAirport = '';
        $departureDateTime = '';
        $arrivalDateTime = '';
        $fareBasis = '';
        $q = 0;

        $testArray = json_encode($response);

        foreach ($response->AirReservation->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {


//            $prefix = self::PREFIX;
            foreach ($option->FlightSegment as $FlightSegment) {

                if ($q === 0) {
                    $model->elapsed_time = (string) $option['ElapsedTime'];
                }

                $prefix = $q === 0 ? '' : self::PREFIX;

                $flightNumber .= (string) $prefix . $FlightSegment['FlightNumber'];
                $resBookDesigCode .= (string) $prefix . $FlightSegment['ResBookDesigCode'];
                $equipment .= (string) $prefix . $FlightSegment->Equipment['AirEquipType'];
                $operatingAirline .= (string) $prefix . $FlightSegment->OperatingAirline['Code'];
                $marketingAirline .= (string) $prefix . $FlightSegment->MarketingAirline['Code'];
                $departureAirport .= (string) $prefix . $FlightSegment->DepartureAirport['LocationCode'];
                $arrivalAirport .= (string) $prefix . $FlightSegment->ArrivalAirport['LocationCode'];
                $departureDateTime .= (string) $prefix . $FlightSegment['DepartureDateTime'];
                $arrivalDateTime .= (string) $prefix . $option['ElapsedTime'];
                $fareBasis .= (string) $prefix . $FlightSegment->BookingClassAvails->BookingClassAvail->FareBasis;

                $q++;
            }


        }

        $model->flight_number = $flightNumber;
        $model->res_book_desig_code = $resBookDesigCode;
        $model->equipment = $equipment;
        $model->operating_airline = $operatingAirline;
        $model->marketing_airline = $marketingAirline;
        $model->departure_airport = $departureAirport;
        $model->arrival_airport = $arrivalAirport;
        $model->departure_datetime = $departureDateTime;
        $model->arrival_datetime = $arrivalDateTime;
        $model->fare_basis = $fareBasis;
        $model->pnr_no = $pnrNo;

        if ($model->save()) {
            return $model->id;
        } else {
            \Yii::error($model->getErrors(), 'flight');
        }
    }

    public function getDepartureCity()
    {
        if (strpos($this->departure_airport, ';')) {
            return explode(';', $this->departure_airport)[0];
        } else {
            return $this->departure_airport;
        }

    }

    public function getArrivalCity()
    {
        if (strpos($this->departure_airport, ';')) {
            $arr = explode(';', $this->arrival_airport);
            return $arr[count($arr) - 1];
        } else {
            return $this->arrival_airport;
        }

    }

    public function getArrivalDateTime()
    {
        $hour = substr($this->arrival_datetime, 0, 2);
        $min = substr($this->arrival_datetime, 2, 2);
        return $hour . ':' . $min;
    }

    public function getDepartureDateTime()
    {
        $time = explode('T', $this->departure_datetime)[1];
        $hour = substr($time, 0, 2);
        $min = substr($time, 2, 2);
        return $hour . ':' . $min;
    }

    public function getDepartureDate()
    {
        return explode('T', $this->departure_datetime)[0];
    }

    public static function departureDate($date)
    {
        return explode('T', $date)[0];
    }

    public static function departureDateTime($date)
    {
        return explode('T', $date)[1];
    }

    public function getArrivalDate()
    {
        return explode('T', $this->arrival_datetime)[0];
    }

    public function getElapsed()
    {
        $hour = substr($this->elapsed_time, 0, 2);
        $min = substr($this->elapsed_time, 2, 2);
        return $hour . ':' . $min;
    }

    public function getPurchaseDate()
    {
        return explode('T', $this->created_at)[0];
    }

    public static function getTiketData($ticket)
    {
        $dataProvider['ticket'] = $ticket;
        $flightsPerson = FlightsPerson::findOne(['ticket_id' => $ticket->id]);
        $dataProvider['person'] = Persons::findOne(['id' => $flightsPerson->person_id]);
        $flight = Flights::findOne(['pnr_no' => $ticket->pnr_no]);
        $flightDetails = FlightDetails::findAll(['flight_id' => (int) $flight->id]);

        foreach ($flightDetails as $keys => $flightDetail) {
            $dataProvider['elapsed'] = $flightDetail->elapsed_time;
            $segment = FlightSegments::findOne(['flight_details_id' => $flightDetail->id]);
            $dataProvider['segments'][$keys] = $segment;
            $dataProvider['segments'][$keys]['operating_airline_name'] = Airlines::findOne(['code_en' => $segment->operating_airline])->name_en;
            $dataProvider['segments'][$keys]['departure_airport_name'] = Cities::findOne(['code' => $segment->departure_airport])->name_en;
            $dataProvider['segments'][$keys]['arrival_airport_name'] = Cities::findOne(['code' => $segment->arrival_airport])->name_en;

        }
        $FlightPriceInfo = FlightPriceInfo::findOne(['flight_id' => $flight->id]);
        $dataProvider['price_info'] = $FlightPriceInfo;
        $dataProvider['price_info']['vat'] = round(($FlightPriceInfo->total_fare_amount) * 12 / 112, 2);

        return $dataProvider;
    }
}
