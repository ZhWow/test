<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.11.2017
 * Time: 15:59
 */

namespace modules\flight\frontend\models\recomendations;

use Yii;
use modules\flight\frontend\models\Recomendation;
use modules\flight\frontend\models\base\Airports;

class FlightPricedItineraries
{
    public $flightDataTo = [];
    public $flightDataBack = [];
    public $backTrip;
    public $exchange;
    public $searchData = [];
    public $bookFormData;
    public $filter;

    public function __construct(\SimpleXMLElement $searchResult, $searchData)
    {
        $this->backTrip = $searchData['back'];
        $this->exchange = $searchData['exchange'];
        $this->flightDataTo = ($this->backTrip) ? self::getFlightsTo($searchResult) : self::getFlights($searchResult);
        $this->flightDataBack = ($this->backTrip) ? self::getFlightsBack($searchResult) : false;
        $this->searchData = $searchData;
    }

    public static function getFlightsTo($searchResult)
    {
        $flightsAll = Recomendation::getFlightNumbersPricedItineraries($searchResult, 0);
        $flights = [];

        foreach ($flightsAll as $number => $priceItineraries) {
            $combinations = [];

            foreach ($priceItineraries as $item) {
                $combinations[(string) $item['SequenceNumber']] = self::getPricedItinerariesTo($item, 0);
            }

            foreach ($priceItineraries as $itemFlight) {
                foreach ($itemFlight->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $flightOption) {
                    if ((string) $flightOption->FlightSegment['FlightNumber'] == $number) {
                        $option = $flightOption;
                        $segment = $flightOption->FlightSegment;
                        $priceInfo = new FlightPriceInfo($itemFlight->AirItineraryPricingInfo);

                        $flightData = FlightData::setData($segment, $option, $combinations, $priceInfo);
                        $flightData->combinationId = $combinations;
                        $flightData->transferCount = count($flightOption->FlightSegment) - 1;
                        $flightData->refund = Recomendation::getNonRefund($itemFlight->AirItineraryPricingInfo);
                        $flightData->transferData = self::getTransferData($itemFlight->AirItinerary->OriginDestinationOptions->OriginDestinationOption);

                        $flights[] = $flightData;

                        break;
                    }
                }
                break;
            }
        }

        return $flights;
    }

    public static function getFlightsBack($searchResult)
    {
        $flights = [];

        foreach ($searchResult->PricedItinerary as $priceItineraries) {
            foreach ($priceItineraries->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
                $indexList = explode(';', (string) $airItineraryItem['IndexList']);

                foreach ($indexList as $directionId => $refNumber) {
                    foreach ($priceItineraries->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {
                        if ((string) $option['DirectionId'] == '1') {
                            if ((string) $directionId == (string) $option['DirectionId'] && (string) $refNumber == (string) $option['RefNumber']) {
                                $combinations = [];
                                $combinations[(string) $priceItineraries['SequenceNumber']] = (string) $airItineraryItem['CombinationID'];

                                $segment = $option->FlightSegment;
                                $priceInfo = new FlightPriceInfo($priceItineraries->AirItineraryPricingInfo);
//
                                $flightData = FlightData::setData($segment, $option, $combinations, $priceInfo);
                                $flightData->transferCount = count($option->FlightSegment) - 1;
                                $flightData->refund = Recomendation::getNonRefund($priceItineraries->AirItineraryPricingInfo);
                                $flightData->transferData = self::getTransferData($priceItineraries->AirItinerary->OriginDestinationOptions->OriginDestinationOption);

                                $flights[(string) $priceItineraries['SequenceNumber']] = $flightData;
                            }
                        }
                    }
                }
            }
        }

        return $flights;
    }

    public static function getFlights($searchResult)
    {
//        $flightsTo = Recomendation::getFlightNumbersPricedItineraries($searchResult, 0);
        $flights = [];
        $iterator = 0;

        foreach ($searchResult->PricedItinerary as $priceItineraries) {
            foreach ($priceItineraries->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
                $indexList = explode(';', (string) $airItineraryItem['IndexList']);

                foreach ($indexList as $directionId => $refNumber) {
                    foreach ($priceItineraries->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {
                        $combinations = [];
                        if ((string) $option['DirectionId'] == '0') {
                            if ((string) $directionId == (string) $option['DirectionId'] && (string) $refNumber == (string) $option['RefNumber']) {

                                $combinations[(string) $priceItineraries['SequenceNumber']] = (string) $airItineraryItem['CombinationID'];

                                $segment = $option->FlightSegment;
                                $priceInfo = new FlightPriceInfo($priceItineraries->AirItineraryPricingInfo);
//
                                $flightData = FlightData::setData($segment, $option, $combinations, $priceInfo);
                                $flightData->transferCount = count($option->FlightSegment) - 1;
                                $flightData->refund = Recomendation::getNonRefund($priceItineraries->AirItineraryPricingInfo);
                                $flightData->transferData = self::getTransferData($priceItineraries->AirItinerary->OriginDestinationOptions->OriginDestinationOption);

                                $flights[$iterator] = $flightData;
                                $iterator++;
                            }
                        }
                    }
                }
            }
        }
//        foreach ($flightsTo as $number => $flightNumber) {
//            foreach ($flightNumber as $flightsItinerary) {
//                foreach ($flightsItinerary->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $optionFlight) {
//                    $combinations = [];
//                    $combination = self::getPricedItineraries($flightsItinerary, $optionFlight);
//                    $combinations[(string) $flightsItinerary['SequenceNumber']] = $combination;
//
//                    $option = $optionFlight;
//                    $segment = $optionFlight->FlightSegment;
//                    $priceInfo = new FlightPriceInfo($flightsItinerary->AirItineraryPricingInfo);
//
//                    $flightData = FlightData::setData($segment, $option, $combinations, $priceInfo);
//                    $flightData->transferCount = count($optionFlight->FlightSegment) - 1;
//                    $flightData->refund = Recomendation::getNonRefund($flightsItinerary->AirItineraryPricingInfo);
//                    $flightData->transferData = self::getTransferData($optionFlight);
//
//                    $flights[] = $flightData;
//                }
//            }
//        }

        return $flights;
    }

    public static function getPricedItinerariesTo(\SimpleXMLElement $airItinerary, $direction)
    {
        $result = [];
//        $test = json_decode(json_encode($airItinerary), true);

        foreach ($airItinerary->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
            $indexList = explode(';', (string) $airItineraryItem['IndexList']);

            foreach ($indexList as $directionId => $refNumber) {
                foreach ($airItinerary->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {
                    if ((string) $direction == (string) $option['DirectionId']) {
                        if ((string) $directionId == (string) $option['DirectionId'] && (string) $refNumber == (string) $option['RefNumber']) {
                            $result[] = (string) $airItineraryItem['CombinationID'];
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function getPricedItinerariesBack(\SimpleXMLElement $airItinerary, $direction)
    {
        $result = [];
        $combinations = [];
//        $test = json_decode(json_encode($airItinerary), true);

        foreach ($airItinerary->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
            $indexList = explode(';', (string) $airItineraryItem['IndexList']);

            foreach ($indexList as $directionId => $refNumber) {
                foreach ($airItinerary->AirItinerary->OriginDestinationOptions->OriginDestinationOption as $option) {
                    if ((string) $direction == (string) $option['DirectionId']) {
                        if ((string) $directionId == (string) $option['DirectionId'] && (string) $refNumber == (string) $option['RefNumber']) {
//                            $result[] = (string) $airItineraryItem['CombinationID'];

                            $combinations[(string) $airItinerary['SequenceNumber']][] = (string) $airItineraryItem['CombinationID'];

                            $segment = $option->FlightSegment;
                            $priceInfo = new FlightPriceInfo($airItinerary->AirItineraryPricingInfo);
//
                            $flightData = FlightData::setData($segment, $option, $combinations, $priceInfo);
                            $flightData->transferCount = count($option->FlightSegment) - 1;
                            $flightData->refund = Recomendation::getNonRefund($airItinerary->AirItineraryPricingInfo);
                            $flightData->transferData = self::getTransferData($airItinerary->AirItinerary->OriginDestinationOptions->OriginDestinationOption);

                            $result[] = $flightData;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function getPricedItineraries(\SimpleXMLElement $airItinerary, $optionFlight)
    {
        $combinationId = '';
//        $test = json_decode(json_encode($airItinerary), true);

        foreach ($airItinerary->AirItinerary->OriginDestinationCombinations->OriginDestinationCombination as $airItineraryItem) {
            $indexList = explode(';', (string) $airItineraryItem['IndexList']);

            foreach ($indexList as $directionId => $refNumber) {
                if ((string) $optionFlight['RefNumber'] == $refNumber) {
                    $combinationId = (string) $airItineraryItem['CombinationID'];
                }
            }
        }

        return $combinationId;
    }

    public static function getTransferData(\SimpleXMLElement $OriginDestinationOption)
    {
        $flightIterator = 0;
        $transferData = [];

        foreach ($OriginDestinationOption->FlightSegment as $FlightSegment) {

            if ($flightIterator == 0) {
                $flightIterator++;
                continue;
            }

            $transferData['transfer'][$flightIterator]['key'] = $flightIterator;
            $transferData['transfer'][$flightIterator]['departureAirport'] = Airports::findOne(['code' => (string) $FlightSegment->DepartureAirport['LocationCode']])->name_ru;
            $transferData['transfer'][$flightIterator]['departureDate'] = Yii::$app->formatter->asDate(strtotime((string) $FlightSegment['DepartureDateTime']), 'php:j M Y, D');
            $transferData['transfer'][$flightIterator]['departureTime'] = date('H:i', strtotime((string) $FlightSegment['DepartureDateTime']));
            $transferData['transfer'][$flightIterator]['arrivalDate'] = Yii::$app->formatter->asDate(strtotime((string) $FlightSegment['ArrivalDateTime']), 'php:j M Y, D');
            $transferData['transfer'][$flightIterator]['arrivalTime'] = date('H:i', strtotime((string) $FlightSegment['ArrivalDateTime']));

            $flightIterator++;
        }

        return $transferData;
    }
}