<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.11.2017
 * Time: 16:03
 */

namespace modules\flight\frontend\models\recomendations;


use modules\flight\frontend\models\base\Airlines;
use modules\flight\frontend\models\base\Airports;

class FlightData
{
    public $combinationId = [];
    public $variantId = [];
    public $departureAirportCode;
    public $departureAirportName;
    public $departureDateTime;
    public $arrivalAirportCode;
    public $arrivalAirportName;
    public $arrivalDateTime;
    public $directionId;
    public $elapsedTime;
    public $refNumber;
    public $flightNumber;
    public $refund;
    public $transferCount;
    public $transferData = [];
    public $marketingAirlineCode;
    public $marketingAirlineName;
    public $img;
    public $priceInfo = [];

    public static function setData($flightSegment, $OriginDestinationOption, $combinations, $priceInfo)
    {
        $model = new self();

        $model->departureAirportCode = (string) $flightSegment->DepartureAirport['LocationCode'];
        $model->departureAirportName = self::getAirportName((string) $flightSegment->DepartureAirport['LocationCode']);
        $model->departureDateTime = strtotime((string) $flightSegment['DepartureDateTime']);

        foreach ($OriginDestinationOption->FlightSegment as $segment) {
            $model->arrivalAirportCode = (string) $segment->ArrivalAirport['LocationCode'];
            $model->arrivalAirportName = self::getAirportName((string) $segment->ArrivalAirport['LocationCode']);
            $model->arrivalDateTime = strtotime((string) $segment['ArrivalDateTime']);
        }

        $model->directionId = (string) $OriginDestinationOption['DirectionId'];
        $model->elapsedTime = (string) $OriginDestinationOption['ElapsedTime'];
        $model->refNumber = (string) $OriginDestinationOption['RefNumber'];
        $model->flightNumber = (string) $flightSegment['FlightNumber'];
        $model->marketingAirlineCode = (string) $flightSegment->MarketingAirline['Code'];
        $model->marketingAirlineName = self::getAirlineName((string) $flightSegment->MarketingAirline['Code']);
        $model->img = '/images/Logos/' . $model->marketingAirlineCode .'.svg';
        $model->combinationId = $combinations;
        $model->priceInfo = $priceInfo;

        return $model;
    }

    public static function getAirportName($code)
    {
        return Airports::findOne(['code' => $code])->name_ru;
    }

    public static function getAirlineName($code)
    {
        return Airlines::findOne(['code_en' => $code])->name_ru;
    }
}