<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.11.2017
 * Time: 17:40
 */

namespace modules\flight\frontend\models\recomendations;


class FlightPriceInfo
{
    public $baseFare;
    public $markupFare;
    public $totalFare;
    public $serviceFee = 0;

    public function __construct(\SimpleXMLElement $AirItineraryPricingInfo)
    {
        $this->baseFare = (string) $AirItineraryPricingInfo->ItinTotalFare->BaseFare['Amount'];
        $this->markupFare = (string) $AirItineraryPricingInfo->ItinTotalFare->MarkupFare['Amount'];
        $this->totalFare = (string) $AirItineraryPricingInfo->ItinTotalFare->TotalFare['Amount'];
    }
}