<?php

namespace modules\flight\frontend\models;

use Yii;

/**
 * This is the model class for table "traveler_info".
 *
 * @property integer $id
 * @property string $passenger_type_code
 * @property string $name_prefix
 * @property string $name
 * @property string $surname
 * @property string $doc_ID
 * @property string $doc_type
 * @property string $inner_doc_type
 * @property string $doc_issue_country
 * @property string $doc_expire_date
 * @property string $pnr_no
 */
class TravelerInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'traveler_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'doc_ID', 'doc_type', 'inner_doc_type', 'doc_issue_country', 'doc_expire_date', 'pnr_no','passenger_type_code', 'name_prefix'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passenger_type_code' => 'Passenger Type Code',
            'name_prefix' => 'Name Prefix',
            'name' => 'Name',
            'surname' => 'Surname',
            'doc_ID' => 'Doc  ID',
            'doc_type' => 'Doc Type',
            'inner_doc_type' => 'Inner Doc Type',
            'doc_issue_country' => 'Doc Issue Country',
            'doc_expire_date' => 'Doc Expire Date',
            'pnr_no' => 'Pnr No',
        ];
    }

    public static function getTravelerInformation($params, $pnrNO)
    {

        foreach ($params as $airTraveler) {
            $model = new self();
//            $model->passenger_type_code = $airTraveler['PassengerTypeCode'];
            $model->passenger_type_code = 'ds';
            /* $model->name_prefix = $airTraveler->PersonName->NamePrefix;
             $model->name = $airTraveler->PersonName->GivenName;
             $model->surname = $airTraveler->PersonName->Surname;
             $model->doc_ID = $airTraveler->Document['DocID'];
             $model->doc_type = $airTraveler->Document['DocType'];
             $model->inner_doc_type = $airTraveler->Document['InnerDocType'];
             $model->doc_issue_country = $airTraveler->Document['DocIssueCountry'];
             $model->doc_expire_date = $airTraveler->Document['ExpireDate'];
             $model->pnr_no = $pnrNO;*/
            if (!$model->save()) {
                \Yii::error($model->getErrors(), 'flight');
            }
        }
    }
}
