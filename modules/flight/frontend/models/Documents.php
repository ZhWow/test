<?php

namespace modules\flight\frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property integer $personID
 * @property string $docID
 * @property string $docType
 * @property string $innerDocType
 * @property string $expireDate
 * @property string $docIssueCountry
 * @property string $docIssueLocation
 *
 * @property Persons $person
 */
class Documents extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personID', 'docType'], 'required'],
            [['personID'], 'integer'],
            [['docID', 'docType', 'innerDocType', 'docIssueLocation'], 'string', 'max' => 255],
            [['expireDate', 'docIssueCountry'], 'string', 'max' => 10],
            [['personID'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['personID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personID' => 'Person ID',
            'docID' => 'Doc ID',
            'docType' => 'Doc Type',
            'innerDocType' => 'Inner Doc Type',
            'expireDate' => 'Expire Date',
            'docIssueCountry' => 'Doc Issue Country',
            'docIssueLocation' => 'Doc Issue Location',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Persons::className(), ['id' => 'personID']);
    }

    public static function saveSoapBookResult($document_attributes, $personID)
    {
        $model = new self();
        $model->personID = $personID;
        $i = 0;
        foreach ($document_attributes as $document_attribute) {
            if ($i === 0) {
                $model->docID = (string) $document_attribute['DocID'];
                $model->docType = (string) $document_attribute['DocType'];
                $model->innerDocType = (string) $document_attribute['InnerDocType'];
                $model->expireDate = (string) $document_attribute['ExpireDate'];
                $model->docIssueCountry = (string) $document_attribute['DocIssueCountry'];
            }
            if ($document_attribute['DocIssueLocation']) {
                $model->docIssueLocation = (string) $document_attribute['DocIssueLocation'];
            }

            $i++;
        }
        /*$model->docID = (string) $document_attribute->Document[0]['DocID'];
        $model->docType = (string) $document_attribute->Document[0]['DocType'];
        $model->innerDocType = (string) $document_attribute->Document[0]['InnerDocType'];
        $model->expireDate = (string) $document_attribute->Document[0]['ExpireDate'];
        $model->docIssueCountry = (string) $document_attribute->Document[0]['DocIssueCountry'];
        $model->docIssueLocation = (string) $document_attribute->Document[1]['DocIssueLocation'];*/

        $model->save();

        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        }
    }
}
