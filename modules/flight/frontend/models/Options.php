<?php

namespace modules\flight\frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "options".
 *
 * @property integer $id
 * @property integer $flightID
 * @property integer $dir_id
 * @property integer $ref_id
 * @property string $elapsedTime
 *
 * @property Flights $flight
 * @property Segments[] $segments
 */
class Options extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flightID', 'dir_id', 'ref_id', 'elapsedTime'], 'required'],
            [['flightID', 'dir_id', 'ref_id'], 'integer'],
            [['elapsedTime'], 'string', 'max' => 10],
            [['flightID'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flightID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flightID' => 'Flight ID',
            'dir_id' => 'Dir ID',
            'ref_id' => 'Ref ID',
            'elapsedTime' => 'Elapsed Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlight()
    {
        return $this->hasOne(Flights::className(), ['id' => 'flightID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSegments()
    {
        return $this->hasMany(Segments::className(), ['optionID' => 'id']);
    }

    public static function saveSoapBookResult($result, $flightID)
    {
        $model = new self();
        $model->flightID = $flightID;
        $model->dir_id = (int) $result['DirectionId'];
        $model->ref_id = (string) $result['RefNumber'];
        $model->elapsedTime = (string) $result['ElapsedTime'];
        if (!$model->save()) {
            Yii::error($model->getErrors(), 'flight');
        }
    }
}
