<?php

namespace modules\flight\frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "segments".
 *
 * @property integer $id
 * @property integer $optionID
 * @property string $departureDateTime
 * @property string $arrivalDateTime
 * @property integer $flightNumber
 * @property string $resBookDesigCode
 * @property integer $operatingAirlineID
 * @property integer $marketingAirlineID
 * @property integer $planeID
 * @property integer $freeSeats
 * @property integer $departureAirportID
 * @property integer $arrivalAirportID
 * @property integer $plainID
 *
 * @property Airports $departureAirport
 * @property Airports $arrivalAirport
 * @property Options $option
 * @property Plains $plain
 * @property Airlines $operatingAirline
 * @property Airlines $marketingAirline
 * @property Plains $plane
 */
class Segments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'segments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['optionID', 'flightNumber', 'operatingAirlineID', 'marketingAirlineID', 'planeID', 'freeSeats', 'departureAirportID', 'arrivalAirportID', 'plainID'], 'integer'],
            [['departureDateTime', 'arrivalDateTime', 'flightNumber', 'operatingAirlineID', 'planeID', 'departureAirportID', 'arrivalAirportID'], 'required'],
            [['departureDateTime', 'arrivalDateTime'], 'string', 'max' => 255],
            [['resBookDesigCode'], 'string', 'max' => 10],
            [['departureAirportID'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['departureAirportID' => 'id']],
            [['arrivalAirportID'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['arrivalAirportID' => 'id']],
            [['optionID'], 'exist', 'skipOnError' => true, 'targetClass' => Options::className(), 'targetAttribute' => ['optionID' => 'id']],
            [['plainID'], 'exist', 'skipOnError' => true, 'targetClass' => Plains::className(), 'targetAttribute' => ['plainID' => 'id']],
            [['operatingAirlineID'], 'exist', 'skipOnError' => true, 'targetClass' => Airlines::className(), 'targetAttribute' => ['operatingAirlineID' => 'id']],
            [['marketingAirlineID'], 'exist', 'skipOnError' => true, 'targetClass' => Airlines::className(), 'targetAttribute' => ['marketingAirlineID' => 'id']],
            [['planeID'], 'exist', 'skipOnError' => true, 'targetClass' => Plains::className(), 'targetAttribute' => ['planeID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'optionID' => 'Option ID',
            'departureDateTime' => 'Departure Date Time',
            'arrivalDateTime' => 'Arrival Date Time',
            'flightNumber' => 'Flight Number',
            'resBookDesigCode' => 'Res Book Desig Code',
            'operatingAirlineID' => 'Operating Airline ID',
            'marketingAirlineID' => 'Marketing Airline ID',
            'planeID' => 'Plane ID',
            'freeSeats' => 'Free Seats',
            'departureAirportID' => 'Departure Airport ID',
            'arrivalAirportID' => 'Arrival Airport ID',
            'plainID' => 'Plain ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartureAirport()
    {
        return $this->hasOne(Airports::className(), ['id' => 'departureAirportID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrivalAirport()
    {
        return $this->hasOne(Airports::className(), ['id' => 'arrivalAirportID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Options::className(), ['id' => 'optionID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlain()
    {
        return $this->hasOne(Plains::className(), ['id' => 'plainID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperatingAirline()
    {
        return $this->hasOne(Airlines::className(), ['id' => 'operatingAirlineID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingAirline()
    {
        return $this->hasOne(Airlines::className(), ['id' => 'marketingAirlineID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlane()
    {
        return $this->hasOne(Plains::className(), ['id' => 'planeID']);
    }
}
