<?php

namespace modules\flight\frontend\models;

use common\services\QLanguage;
use modules\flight\frontend\models\base\Airlines as BaseAirlines;

/**
 * This is the model class for table "airlines".
 *
 * @property string $name
 */
class Airlines extends BaseAirlines
{
    public static $codes_array = [];

    /**
     * @return string
     */
    public function getName($sourceLang = false)
    {
        if ($sourceLang) {
            return $this['name_' . QLanguage::getSourceLang()];
        } else {
            if ($this['name_' . QLanguage::getCurrentLang()]) {
                return $this['name_' . QLanguage::getCurrentLang()];
            }
            return $this['name_' . QLanguage::getDefaultLang()];
        }
    }

    public static function getAirline($code) {
        if ( isset(self::$codes_array[$code]) ) {
            return self::$codes_array[$code];
        }

        $item = self::find()->andWhere(['code_en' => $code])->one();
        if (!$item) {
            $item = new self();
            $item->code_en = $code;
            $item->save();
        }
        self::$codes_array[$code] = $item;

        return $item;
    }

    public function getLogo()
    {
        return "/img/logo-air.png";
    }

    public static function getAirlineArray(\SimpleXMLElement $searchData)
    {
        $returnArr = [];
        $airlineNumbers = [];
        $airlineXpathNumbers = $searchData->xpath('//MarketingAirline/@Code');

        foreach ($airlineXpathNumbers as $airline) {
            $airlineNumbers[] = (string) $airline['Code'];
        }

        $airlineNumbers = array_unique($airlineNumbers);

        foreach ($airlineNumbers as $key => $airlineNumber) {
            $returnArr[$key]['code'] = $airlineNumber;
            $returnArr[$key]['name'] = self::findOne(['code_en' => $airlineNumber])->name_ru;
        }

        return $returnArr;
    }
}
