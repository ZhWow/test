<?php

namespace modules\flight\frontend\soap;

use modules\flight\frontend\Module;
use Yii;

/**
 * Class AmadeusSoapClient
 *
 * @package modules\flight\frontend\soap
 */
class AmadeusSoapClient extends \SoapClient
{
    private static $instance;

    /**
     * AmadeusSoapClient constructor.
     */
    private function __construct()
    {
        $module = Yii::$app->getModule('flight');
        $soapParams = $module->params['soap_param'];

        parent::__construct($soapParams['WSDL_STG'], [
            'trace' => true,
            'exceptions' => true,
            'stream_context' => self::getContextConnect(),
            'location' => $soapParams['WSDL_STG'],
            'connection_timeout' => $soapParams['CONNECT_TIMEOUT']
        ]);

    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            $module = Yii::$app->getModule('flight');
            $soapParams = $module->params['soap_param'];
            $soapClient = new self();
            self::$instance = $soapClient;
            $soapVar = new \SoapVar(self::getHeaderBody(), SOAP_ENC_OBJECT);
            $soapHeaders[] = new \SOAPHeader($soapParams['SOAP_HEADER_NS'], 'AuthenticationSoapHeader', $soapVar);
            $soapClient->__setSoapHeaders($soapHeaders);
        }

        return self::$instance;
    }

    private static function getContextConnect()
    {
        return stream_context_create([
            'ssl' => [
                'allow_self_signed' => TRUE,
                'verify_peer' => FALSE,
                'ciphers' => 'SHA1'
            ],
            'https'	=> [
                'curl_verify_ssl_peer'	=> FALSE,
                'curl_verify_ssl_host'	=> FALSE
            ]
        ]);
    }

    private static function getHeaderBody()
    {
        $module = Yii::$app->getModule('flight');
        $soapParams = $module->params['soap_param'];
        return [
            'WSUserName' => $soapParams['WS_USER_NAME'],
            'WSPassword' => $soapParams['WS_PASSWORD'],
        ];
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $module = Yii::$app->getModule('flight');
        $soapParams = $module->params['soap_param'];
        $request = preg_replace('/<ns1:AuthenticationSoapHeader>/','<ns1:AuthenticationSoapHeader xmlns="' . $soapParams['SOAP_HEADER_NS'] . '">', $request);
        $GenericSoapHeader = 'AuthenticationSoapHeader><GenericSoapHeader xmlns="' . $soapParams['SOAP_HEADER_NS'] . '"><SubSiteCode>'. $soapParams['SOAP_SUB_SITE_CODE'] .'</SubSiteCode></GenericSoapHeader>';
        $request = preg_replace('/AuthenticationSoapHeader>/',$GenericSoapHeader, $request);
        /* @noinspection PhpUndefinedFieldInspection */
        $this->__last_request = $request;
//        Yii::error($request, 'flight');
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    public function getToken()
    {
        $header = $this->__getLastResponseHeaders();
        $headerArray = explode("\n", $header);
        $keyword = "HttpOnly";
        $match = preg_grep("/{$keyword}/i", $headerArray);
        $arrayCount = count($match);
        if ($arrayCount == 0) {
            return false;
        }
        $sessionArray = array_values($match);
        $session_id_amadeus = substr($sessionArray[0], 30, 24);

        if ($session_id_amadeus) {
            return $session_id_amadeus;
        }
        return false;
    }
}