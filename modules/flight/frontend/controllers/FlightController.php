<?php

namespace modules\flight\frontend\controllers;

use modules\flight\frontend\models\FlightExchangeHelper;
use modules\services\Notification;
use common\modules\helpers\CreatePdf;
use common\modules\helpers\TelegramNotification;
use backend\modules\integra\services\TelegramBot;
use modules\flight\frontend\models\base\BookingClassAvails;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightElectronicTickets;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\FlightExchange;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\Persons;
use modules\countries\common\models\Cities;
use modules\flight\frontend\models\Tokens;
use modules\flight\frontend\providers\amadeus\services\CustomResponse;
use modules\flight\frontend\providers\alfa\AlfaBank;
use modules\services\Registry;
use modules\flight\frontend\models\SearchFormAmadeus;
use modules\users\common\models\PayOrders;
use modules\users\common\models\User;
use yii\base\Exception;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;
use modules\flight\frontend\providers\base\FlightsProvider;
use modules\flight\frontend\models\ElectronicTickets;
use modules\flight\frontend\models\base\ElectronicTickets as ElectronicTicketsBase;
use modules\flight\frontend\providers\mail\SendTicket;

/**
 * Class FlightController
 *
 * @package modules\flight\frontend\controllers
 */
class FlightController extends Controller
{
    private $provider;

    public function __construct($id, $module, FlightsProvider $provider, array $config = [])
    {
        $this->provider = $provider;
        parent::__construct($id, $module, $config);
    }

    /**
     * Accepts the substring entered by the user, from the input with autocomplite. Performs a search in the database
     * and returns a string if it finds a match
     * @param string $city Substring for search
     * @return array|string An array of similar names unless a specific string is found, or a string
     */
    public function actionGetCity($city)
    {
        return json_encode(Cities::getCitySearch($city));
    }

    /**
     * Accepts the post request ajax, with the search data. If there is no error,
     * the rendered html is returned with the search result back to js.
     * Js just adds it to the page in the right place
     *
     * @return string|object
     */
    public function actionSearchTickets()
    {
        $model = new SearchFormAmadeus();
        $return = 'error';
//        $uniqueUserKey = (string) $bodyParams['_csrf-frontend'];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $searchResult = $this->provider->search($model);
            /* @var $searchResult CustomResponse */
            if ($searchResult->isOk) {
//                $result = $searchResult->getResponse();
                //$return = json_encode($result);
//                $return = $this->renderAjax('@modules/flight/frontend/views/flight/search-result', [
//                    'searchFlight' => $result['flights'],
//                    'cabin' => $result['cabin'],
//                ]);
                $return = $this->renderAjax('recomendations.php', [
                    'model'             => $model,
                    'searchFlightData'  => $searchResult
                ]);
            } else {
                $return = $searchResult->getResponse();
            }
        } else {
            Yii::error($model->getErrors(), 'flight');
        }

        return $return;
    }

    /**
     * Elsi came to the post request, sends a request for the armor
     * If get, draws a form to fill in the data for the reservation,
     * also if the come can accept the key parameter at which the results
     * of the last search are simply retrieved
     *
     * @param int $key
     * @return string
     */
    public function actionBook($key = null)
    {
        if ($key === null) {
            /* @noinspection PhpUndefinedFieldInspection */
//            return $this->provider->book(Yii::$app->request->post());
            $result = $this->provider->book(Yii::$app->request->post());
            return $this->renderAjax('_book_modal.php', ['response' => $result]);
        } else {
            $sessionNames = Yii::$app->controller->module->params['session_var_names'];
            $session = Yii::$app->session;
            $data = Registry::getData('__search_result_' . $session->get($sessionNames['tokenId']));
            $model = new Persons();
            return $this->renderAjax('book-form', [
                'model' => $model,
                'flight' => $data[$key],
            ]);
        }

    }

    public function actionExchange($key = null)
    {
        $result = [
            'status'    => 'error',
            'message'   => 'Exchange ticket not created!'
        ];

//        $sessionNames = Yii::$app->controller->module->params['session_var_names'];
        $session = Yii::$app->session;
        $post = Yii::$app->request->post();
//        $data = Registry::getData('__search_result_' . $session->get($sessionNames['tokenId']));

        $exchangeStatusSave = false;
        $exchangeStatusSaveEnd = false;
//        $selected = $data[$key];

        $flights = new Flights();
        $flights->ticket_type = 'BookingOnly';
        $flights->booking_reference_id_type = 'F';

        if ($flights->save()) {
            $exchange = new FlightExchange();
            $exchange->flight_id = $flights->id;
            $exchange->ticket_id =  $session->get('exchangeTicketId');
            $exchange->exchange_time = date("Y-m-d H:i:s" ,time());
            if ($exchange->save()) {
                $flightsUpdate = Flights::findOne($flights->id);
                $flightsUpdate->exchange_id = $exchange->id;
                $flightsUpdate->update();

                $FlightPerson = new FlightsPerson();
                $oldFlightPerson = FlightsPerson::findOne(['ticket_id' => $session->get('exchangeTicketId')]);
                $FlightPerson->flight_id = $flights->id;
                $FlightPerson->person_id = $oldFlightPerson->person_id;
                if ($FlightPerson->save()) {
                    $exchangeStatusSave = FlightExchangeHelper::setExchangeData($post['data'], $flights->id);
                }
            }

            if ($exchangeStatusSave == true) {
                $priceInfo = new FlightPriceInfo();
                $priceInfo->flight_id = $flights->id;
                $priceInfo->base_fare_amount = $post['price']['baseFare'];
                $priceInfo->taxes_amount = 0;
                $priceInfo->total_fare_amount = $post['price']['totalFare'];
                if ($priceInfo->save()) {
                    $exchangeStatusSaveEnd = true;
                }
            }
        }

        if ($exchangeStatusSaveEnd == true) {
            $ticket = FlightElectronicTickets::findOne($session->get('exchangeTicketId'));
            $ticket->status = 5;
            $ticket->update();
//            $ticketData = FlightExchange::setTicketData($ticket);
//            $telegramData = FlightExchange::setToTelegramData($ticketData);
//
//            $telegramBotClass = new TelegramBot();
//            $telegramBotClass->qwayTelegramNotification('onQwayExchangeRequest', $telegramData, Yii::$app->params['telegram']['flights']['Key']);

            $user = User::findOne($ticket->user_id);

            Notification::telegramSend($ticket, 'onQwayExchangeRequest');
            Notification::send('to_exchange_ticket', $ticket, 'Запрос на обмен', $user->email);

            $session->remove('exchangeTicketId');
            $session->remove('exchangeTicketNumber');
            $session->close();

            $result = [
                'status'    => 'success',
                'message'   => 'Билет отправлен на обмен'
            ];
        }

        return $this->renderAjax('_to-exchange-result', ['result' => $result]);
    }

    public function actionCancelBook()
    {
        return $this->provider->cancelBook(Yii::$app->request->post());
    }

    public function actionGetFlightRules()
    {
        $getFlightRulesResponse = $this->provider->getFlightRules(Yii::$app->request->post());
        return $this->renderAjax('get-flight-rules.php',['getFlightRulesResponse' => $getFlightRulesResponse]);
    }

    /**
     * Action for the payment gateway. When POST request registers an order.
     * Passing orderId (pnrNO) and returnUrl (which is the given 'action').
     * When a GET request is processed, the bank response
     *
     * @param null|string This is the number of the order (which was transferred to the payment gateway),
     * the payment gateway resets it with a request.
     * @return string|\yii\web\Response
     */
    public function actionPayment($orderId = null)
    {
        try {
            // When you clicked the revert to payment we get here
            if (Yii::$app->request->post()) {
                $paymentData = Yii::$app->request->post();
                $response = AlfaBank::registerOrder($paymentData,  Url::canonical());

                if ($response->errorCode != 0) {
                    return $this->render('payment', [
                        'response' => $response,
                        'eTicketIDs' => null,
                    ]);
                } else {
                    return $this->redirect($response->formUrl);
                }
            }


            // Bank Response
            if (Yii::$app->request->get() && isset($orderId)) {

                $response = AlfaBank::getOrderStatus($orderId);
                $eTicketIDs = null;

                if ($response->errorCode == 0) {
                    $payOrders = new PayOrders();
                    $payOrders->setOrder($response);
                    /* @var $resultticketExistInTBooks CustomResponse */
                    $resultticketExistInTBooks = $this->provider->bookId($response->orderNumber);
                    if ($resultticketExistInTBooks->isOk) {

                        /* @var $createTicketResult CustomResponse */
                        $createTicketResult = $this->provider->createTicket($resultticketExistInTBooks->getResponse());
                        if ($createTicketResult->isOk) {
                            $eTicketIDs = $createTicketResult->getResponse();
                            //todo send mail and create pdf
                            $tickets = FlightElectronicTickets::findAll($eTicketIDs);
                            foreach ($tickets as $ticket) {
                                $passenger = $ticket->passenger;
                                $pdfTemplate = $this->renderAjax('_pdf-ticket', ['models' => $ticket]);
                                $pdfClass = new CreatePdf();
                                $filePdf = $pdfClass->createPdf($ticket->ticket_number. '_' . $passenger->surname. '_' . $passenger->name, $pdfTemplate);
                                $sendTicketClass = new SendTicket();
                                $sendTicketClass->sendTicket($passenger->email, $filePdf, $ticket);
                            }
                        }
                    }

                    /*$resultticketExistInTBooks = ticketExistInTBooks::getResult($session->get($sessionNames['tokenId']));
                    $eTicketIDs = CreateTicket::getResult($resultticketExistInTBooks);*/

                }

                return $this->render('payment', [
                    'response' => $response,
                    'eTicketIDs' => $eTicketIDs,
                ]);

            }

            return '';

        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'flight');
            return 'error';
        }
    }

    public function actionPaytest()
    {
        return $this->render('payment', [
            'response' => PayOrders::findOne(1),
            'eTicketIDs' => [1],
        ]);
    }

    public function actionTest()
    {
//        $searchFlightData = [];
//        return $this->render('recomendations.php', [
//            'searchFlightData' => $searchFlightData
//        ]);
    }

    public function actionRequestRefunds($id)
    {
        $model = ElectronicTickets::findOne($id);
        $model->refunds = ElectronicTickets::PENALTY_REQUEST;
        if ($model->save()) {
            return json_encode('ok');
        } else {
            Yii::error($model->getErrors(), 'admin');
            return json_encode('bad');
        }
    }

    public function actionRequestExchange($id)
    {
        $model = ElectronicTickets::findOne($id);
        $model->exchange = ElectronicTickets::PENALTY_REQUEST;
        if ($model->save()) {
            return json_encode('ok');
        } else {
            Yii::error($model->getErrors(), 'admin');
            return json_encode('bad');
        }
    }

    public function actionExchangeSearch()
    {
        $model = new SearchFormAmadeus();
        $return = 'error';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $searchResult = $this->provider->search($model);
            /* @var $searchResult CustomResponse */
            if ($searchResult->isOk) {
                $result = $searchResult->getResponse();
                $return = $this->renderAjax('@modules/flight/frontend/views/flight/search-result', [
                    'searchFlight' => $result['flights'],
                    'cabin' => $result['cabin'],
                ]);
            } else {
                $return = $searchResult->getResponse();
            }
        } else {
            Yii::error($model->getErrors(), 'flight');
        }

        return $return;
    }

    /**
     * @ignore
     * @param $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id === 'payment') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}