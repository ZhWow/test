<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 17.10.2017
 * Time: 16:03
 * @var $searchFlightData SimpleXMLElement
 * @var $model
 */
use modules\flight\frontend\models\Recomendation;
use modules\flight\frontend\models\recomendations\FlightPricedItineraries;

Yii::$app->formatter->locale = 'ru-RU';

$session = $session = Yii::$app->session;

$exchange = ($session->has('exchangeTicketId')) ? true : false;
$backTrip = ($model->tripDateBackward != '') ? true : false;

$returnFlightData = [
    'departureCityName' => (string) $model['departureCity'],
    'arrivalCityName'   => (string) $model['arrivalCity'][0],
    'departureDateSrt'  => Yii::$app->formatter->asDate(strtotime((string) $model['tripDate'][0]), 'php:j M Y, D'),
    'arrivalDateSrt'    => Yii::$app->formatter->asDate(strtotime((string) $model['tripDateBackward']), 'php:j M Y, D'),
    'cabin'             => (string) $model['cabin'],
    'back'              => ($model->tripDateBackward != '') ? true : false,
    'exchange'          => ($session->has('exchangeTicketId')) ? true : false,
];

$flightData = new FlightPricedItineraries($searchFlightData, $returnFlightData);

//$recomendations = Recomendation::getRecomendations($searchFlightData);
//$returnFlightData['searchFlightData'] = $recomendations;

$bookFormArr = Recomendation::getPassengerForm($searchFlightData);
$bookFormDataPassengers = '';

foreach ($bookFormArr as $book) {
    $bookFormDataPassengers .= $this->renderAjax('_passenger-data-form', $book);
}
$flightData->bookFormData = $this->renderAjax('flight_book_form', ['bookFormData' => $bookFormDataPassengers]);
$flightData->filter = $this->renderAjax('_filter', ['filterData'=> $searchFlightData]);

echo json_encode($flightData, JSON_UNESCAPED_UNICODE);
?>
