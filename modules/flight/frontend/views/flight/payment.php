<?php

/* @var $this yii\web\View */
/* @var $response Object Ответ от платежного шлюза */
/* @var $eTicketIDs array */

use yii\helpers\Url;
use modules\users\common\models\PayOrders;
use modules\flight\frontend\models\base\FlightElectronicTickets;

$tickets = FlightElectronicTickets::findAll($eTicketIDs);
$currentUser = \Yii::$app->user->identity;

$this->title = 'Оплата';
?>

<!-- Нужно вообще проверить есь ли response -->
<div id="wrap-payment">
    <?php if ($response->errorCode == 0) :?>
        <?php $responseId = PayOrders::findOne(['orderNumber' => $response->orderNumber])->id;?>
        <div class="pay-status">
            <?php foreach ($tickets as $ticket) :?>
                <div class="head"><?=$ticket->ticket_number?></div>
                <table class="flights">
                    <tr class="dashed">
                        <th>Маршрут:</th>
                        <th>Рейс:</th>
                        <th>Класс:</th>
                        <th>Дата вылета:</th>
                        <th>Дата прилёта:</th>
                        <th>Пассажир:</th>
                        <th>Стоимость:</th>
                    </tr>
                    <?php $itineraryIterator = 0;?>
                    <?php foreach ($ticket->itineraries as $itinerary) :?>
                        <tr>
                            <td>
                                <?=$itinerary->from?>-<span class="orange"><?=$itinerary->to?></span>
                            </td>
                            <td>
                                <?=$itinerary->marketing_airline_code?>-<?=$itinerary->flight_no?>
                            </td>
                            <td>
                                <?=$itinerary->class?>
                            </td>
                            <td>
                                <?=date('H:i', strtotime($itinerary->departure_date))?>/<?=date('d.m.Y', strtotime($itinerary->departure_date))?>
                            </td>
                            <td>
                                <span class="orange"><?=date('H:i', strtotime($itinerary->arrival_date))?>/<?=date('d.m.Y', strtotime($itinerary->arrival_date))?></span>
                            </td>
                            <?php if ($itineraryIterator == 0) :?>
                                <td rowspan="<?=count($ticket->itineraries)?>">
                                    <?=$ticket->passenger->surname?><br>
                                    <?=$ticket->passenger->name?>
                                </td>
                                <td class="blue" rowspan="<?=count($ticket->itineraries)?>">
                                    <?=number_format($ticket->total_fare, 0, ' ', ' ');?><br>
                                    KZT
                                </td>
                            <?php else :?>
                                <td></td>
                                <td></td>
                            <?php endif;?>
                        </tr>
                        <?php $itineraryIterator++;?>
                    <?php endforeach;?>
                </table>
            <?php endforeach;?>
            <div class="head not">
                <span>Квитанция №</span><?=explode('_', $response->orderNumber)[0];?>
            </div>
            <div class="success-head">Платёж прошёл успешно</div>
            <div class="pay-body">
                <div class="line">
                    <div class="l">ФИО плательщика:</div>
                    <div class="r">
                        <span class="name"><?=$response->cardholderName?></span>
                    </div>
                </div>
                <div class="line">
                    <div class="l">Время/Дата платежа:</div>
                    <div class="r"><?=date('H:i', strtotime($response->date))?>/<?=date('d.m.Y', strtotime($response->date))?></div>
                </div>
                <div class="line">
                    <div class="l">Получатель:</div>
                    <div class="r">Qway.kz</div>
                </div>
                <div class="line">
                    <div class="l">Реквизиты платежа:</div>
                    <div class="r"><?=$response->pan?></div>
                </div>
                <div class="line">
                    <div class="l">Оплачено:</div>
                    <div class="r">Кредитная карта</div>
                </div>
            </div>
            <div class="pay-price">
                <div class="block">Оплаченная сумма:</div>
                <div class="block"><?=number_format((float) $response->amount / 100, 0, ' ', ' ');?> &#8376;</div>
            </div>
            <div class="pay-bottom">
                <div class="block">
                    <?php if ($currentUser) :?>
                        <label for="payment_reciept_email">Email:</label> <input type="text" name="paymentRecieptEmail" id="payment_reciept_email" value="<?=$currentUser->email?>">
                    <?php else :?>
                        <label for="payment_reciept_email">Email:</label> <input type="text" name="paymentRecieptEmail" id="payment_reciept_email" value="">
                    <?php endif;?>
                    <button type="button" class="btn btn-success" id="send_pay_pdf" data-id="<?=$responseId?>">Отправить на email</button>
                </div>
                <div class="block">
                    <?php if ($currentUser) :?>
                        <a href="<?=Url::to('/profile/user-profile/my-flight-tickets');?>" class="btn  btn-default" style="margin-right: 10px;">Ближайшие поездки</a>
                        <a target="_blank" href="<?=Url::to('/profile/user-profile/pay-pdf?pay_order_id=' . $responseId);?>" class="btn btn-info">Скачать</a>
                    <?php endif;?>
                </div>
            </div>
            <div class="border-pay"></div>
        </div>
    <?php else :?>
        <p><?= $response->errorMessage ?></p>
    <?php endif;?>
</div>
