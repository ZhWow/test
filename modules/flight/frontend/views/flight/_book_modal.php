<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 30.10.2017
 * Time: 16:28
 * @var $response
 */
use yii\helpers\Url;
use modules\flight\frontend\models\base\Airports;
use modules\flight\frontend\models\base\Airlines;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\Persons;

if ($response->isOk == true) {
    $flight = Flights::findOne(['pnr_no' => $response->response['pnrNO']]);
    $flightDetails = $flight->flightDetails;
    $flightsPersons = $flight->flightsPerson;
    $price = $flight->priceInfo;
}
?>
<?php if ($response->isOk == true): ?>
<table class="book">
    <tr class="dashed">
        <th>Вылет:</th>
        <th>Прилёт:</th>
        <th>Время в пути:</th>
        <th>Рейс:</th>
        <th>Перевозчик:</th>
        <th>Пассажир:</th>
    </tr>
    <?php foreach ($flight->flightDetails as $options) :?>
        <?php foreach ($options->flightSegments as $option) :?>
            <tr>
                <td>
                    <div class="date"><?=date('d.m.Y', strtotime((string) $option->departure_date_time))?></div>
                    <div class="time"><?=date('H:i', strtotime((string) $option->departure_date_time))?> <span class="plane-dep"></span></div>
                    <?php
                        $airDep = Airports::findOne(['code' => (string) $option->departure_airport])->name_ru;
                        $airArr = Airports::findOne(['code' => (string) $option->arrival_airport])->name_ru;
                        $company = Airlines::findOne(['code_en' => (string) $option->operating_airline])->name_ru;
                    ?>
                    <div class="city"><?=$airDep?></div>
                </td>
                <td>
                    <div class="date"><?=date('d.m.Y', strtotime((string) $option->arrival_date_time))?></div>
                    <div class="time"><?=date('H:i', strtotime((string) $option->arrival_date_time))?> <span class="plane-arr"></span></div>
                    <div class="city"><?=$airArr?></div>
                </td>
                <td>
                    <?php
                        $h = date('H', strtotime((string) $option->flight_duration));
                        $i = date('i', strtotime((string) $option->flight_duration));
                    ?>
                    <span class="clock"></span><?=$h;?> час <?=$i;?> мин
                </td>
                <td>
                    <?=(string) $option->operating_airline?>(<?=(string) $option->flight_number?>)
                </td>
                <td>
                    <div class="fly">
                        <?=$company?>
                    </div>
                </td>
                <td>
                    <?php /**@var $flightsPersons*/?>
                    <?php $personIterator = 0;?>
                    <?php foreach ($flightsPersons as $flightsPerson) :?>
                        <?php $person = Persons::findOne(['id' => $flightsPerson->person_id]);?>
                        <div class="pass"><?=$person->surname?> <?=$person->name?> <?=$person->prefix?></div>
                        <?php if ($personIterator == count($flightsPersons) - 1 && $personIterator > 0) :?>
                            <br>
                        <?php endif;?>
                        <?php $personIterator++;?>
                    <?php endforeach;?>
                </td>
            </tr>
        <?php endforeach;?>
    <?php endforeach;?>
</table>
<div class="btm">
    <div class="book-inf">
        Номер вашей брони: <span class="pnr"><?=(string) $flight->pnr_no?></span>
    </div>
    <div class="summ">
        Сумма к оплате: <span><?=number_format((string) $price->total_fare_amount, 0, ' ', ' ')?> kzt</span>
    </div>
    <div class="action-pay">
        <form action="<?= Url::toRoute('/flight/flight/payment') ?>" id="payment-form" method="post">
            <input type="hidden" name="orderNumber" id="payment-order-number" value="<?=(string) $flight->pnr_no?>">
            <input type="hidden" name="amount" id="payment-amount" value="<?= str_replace(' ', '', (string) $price->total_fare_amount) ?>">
            <input type="submit" class="btn btn-primary btn-lg" id="modal-payment-submit" value="Перейти к оплате">
        </form>
    </div>
</div>
<?php else: ?>
    <p>При бронировании произошла ошибка, повторите попытку позже.</p>
<?php endif; ?>
