<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 09.11.2017
 * Time: 14:04
 * @var $filterData SimpleXMLElement
 */
use modules\flight\frontend\models\Airlines;

$airlinesArr = Airlines::getAirlineArray($filterData);
?>
<div class="filter-block">
    <select class="filter-select" name="route" id="route">
        <option value="all" disabled selected hidden>Пересадки</option>
        <option value="all">Все</option>
        <option value="0">Без пересадок</option>
        <option value="1">1 пересадка</option>
        <option value="2">2 пересадки</option>
        <option value="3">3 пересадки</option>
    </select>
    <select class="filter-select" name="rate" id="rate">
        <option value="all" disabled selected hidden>Тариф</option>
        <option value="all">Все</option>
        <option value="false">Возвратные</option>
        <option value="true">Невозвратные</option>
    </select>
    <select class="filter-select" name="airline" id="airline">
        <option value="all" disabled selected hidden>Авиакомпании</option>
        <option value="all">Все</option>
        <?php foreach ($airlinesArr as $airline) :?>
            <option value="<?=$airline['code']?>"><?=$airline['name']?></option>
        <?php endforeach;?>
    </select>
</div>
