<?php

/* @var $this yii\web\View */
/* @var $model \modules\flight\frontend\models\Persons */
/* @var $flight \modules\flight\frontend\providers\amadeus\components\Flight */

use yii\helpers\Html;
use yii\widgets\MaskedInput;
use modules\flight\frontend\widgets\FlightWidget;

$js = <<< JS
$('.passenger-data-forms').find('select').each(function() {
        var that = $(this), numberOfOptions = $(this).children('option').length;

        that.addClass('select-hidden');
        that.wrap('<div class="select"></div>');
        that.after('<div class="select-styled"></div>');

        var styledSelect = that.next('div.select-styled');
        styledSelect.text(that.children('option').eq(0).text());

        var listLi = $('<ul />', {
            'class': 'select-options'
        }).insertAfter(styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: that.children('option').eq(i).text(),
                rel: that.children('option').eq(i).val()
            }).appendTo(listLi);
        }

        var listItems = listLi.children('li');

        styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        listItems.click(function(e) {
            e.stopPropagation();
            styledSelect.text($(this).text()).removeClass('active');
            that.val($(this).attr('rel'));
            listLi.hide();
            //console.log(that.val());
        });

        $(document).click(function() {
            styledSelect.removeClass('active');
            listLi.hide();
        });

    });
JS;
$this->registerJs($js);

$combinationID = $flight->combinationID;
$sequenceID = $flight->sequence->sequenceNumber;

$this->title = 'Бронь';

?>

    <div id="wrap-book-result">

        <div id="navigation-flight-search">

            <?= Html::button('К результатам поиска', [
                'id' => 'back-to-search-result',
                'class' => 'btn btn-default',
            ]) ?>
            <?= Html::button('Маршрут и пассажиры', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('Оплата', ['class' => 'btn btn-default', 'disabled' => true]) ?>

        </div>


        <?php foreach ($flight->options as $option) : ?>
            <div class="item-wrapper">
                <?php
                $segment = $option->getFirstSegment();
                $segmentLast = $option->getLastSegment();
                $segmentCount = count($option->segments);
                $iElement = '';
                $ptText = '-я пересадка: ';
                switch ($segmentCount) {
                    /*case 1:
                        $ptText = ' пересадка: ';
                        $iElement .= FlightWidget::ptElementR($option->segments, $ptText, [1 => 3]);
                        break;*/
                    case 3:
                        $iElement .= FlightWidget::ptElementR($option->segments, $ptText, [1 => 2, 4]);
                        break;
                    case 4:
                        $iElement .= FlightWidget::ptElementR($option->segments, $ptText, [1 => 1, 3, 5]);
                        break;
                    case 5:
                        $iElement .= FlightWidget::ptElementR($option->segments, $ptText, [1 => 1, 2, 4, 5]);
                        break;
                    case 6:
                        $iElement .= FlightWidget::ptElementR($option->segments, $ptText, [1 => 1, 2, 3, 4, 5]);
                        break;
                }
                ?>
                <div class="item-logo">
                    <header><span>Перевозчик:</span></header>
                    <section class="<?= strtolower(str_replace(' ', '-', $segment->getAirline()->getName(true))) ?>">


                    </section>
                    <h4><?= $segment->getAirline()->getName() ?></h4>

                </div>
                <div class="item-cont">
                    <header>
                        <div class="left-block">
                            <span>рейс</span>&nbsp<h5><?= $segment->getBort()->getName() ?></h5>
                        </div>
                        <div class="right-block">
                            <span>общее время в пути</span>&nbsp<h5><?= $option->getElapsedTime() ?></h5>
                        </div>


                    </header>
                    <section>
                        <div class="departure-cont port-cont">
                            <header>
                                <span>Откуда</span>
                                <h4><?= $segment->getDepartureAirport()->city->getName() ?></h4>
                            </header>
                            <section>
                                <h2><?= $segment->getDepartureTime() ?></h2>
                                <p><?= $segment->getDepartureDate() ?></p>
                            </section>
                        </div>
                        <div class="transplantation-cont">
                            <i class="fa fa-plane fa-departure" aria-hidden="true"></i>
                            <i class="fa fa-circle-thin fa-departure"
                               aria-hidden="true"></i><?= $iElement . FlightWidget::renderSvgs() ?>
                            <i class="fa fa-circle-thin fa-arrival" aria-hidden="true"></i>
                            <i class="fa fa-plane fa-arrival" aria-hidden="true"></i>
                        </div>
                        <div class="arrival-cont port-cont">
                            <header>
                                <span>Куда</span>
                                <h4><?= $segmentLast->getArrivalAirport()->city->getName() ?></h4>
                            </header>
                            <section>
                                <h2><?= $segmentLast->getArrivalTime() ?></h2>
                                <p><?= $segmentLast->getArrivalDate() ?></p>
                            </section>
                        </div>
                    </section>
                </div>
                <div class="item-amount">

                </div>
            </div>
            <?= $this->render('_book-result-modal', [
                'option' => $option,
                'flight' => $flight,
                'segment' => $segment
            ]) ?>
        <?php endforeach; ?>


        <div class="price">
            <a id="get-flight-rules" data-combinationid="<?= $combinationID ?>" data-sequenceid="<?= $sequenceID ?>"
               href="javascrip:void()">Условия возврата и обмена</a>
            <div class="amount">
                Итого к оплате: <br>
                <span class="total"><?= $flight->sequence->getTotalFare() ?></span>
                <span class="currency"><?= $flight->sequence->currency ?></span>
                <br>
                <span class="taxes-and-fees">Включая все налоги и сборы</span>
            </div>
        </div>


        <div class="passenger-data-forms">

            <div class="passenger-data-header">
                <h3>Данные пассажиров</h3>
            </div>

            <?php

            $allPassengersCount = 0;
            /* @var $flight \modules\flight\frontend\providers\amadeus\components\Flight */
            foreach ($flight->sequence->passengers as $passenger) {
                for ($i = 0; $i < $passenger->quantity; $i++) {

                    $passengerCode = Yii::$app->controller->module->params['passenger_code'];
                    if ($i === 0) {
                        if ($passenger->code == $passengerCode['ADULTS']) {
                            $contact_input = true;
                        }
                        $allPassengersCount += ($i + 1);
                    } else {
                        $contact_input = false;
                        $allPassengersCount += $i;
                    }

                    echo $this->render('_passenger-data-form', [
                        'model' => $model,
                        'code' => $passenger->code,
                        'combinationID' => $combinationID,
                        'sequenceID' => $sequenceID,
                        'contact_person' => $contact_input,
                        'num' => $allPassengersCount,
                    ]);
                }
            }
            ?>
        </div>


        <?php
        $faker = \Faker\Factory::create('ru_RU');
        $email = '';
        $phone = '';
        if (!Yii::$app->user->isGuest) {
            /* @noinspection PhpUndefinedFieldInspection */
            $email = Yii::$app->user->identity->email;
            /* @noinspection PhpUndefinedFieldInspection */
            $userProfile = \common\models\UserProfile::findOne(['user_id' => Yii::$app->user->identity->id]);
            if ($userProfile !== null) {
                if ($userProfile->phone) {
                    $ex = explode('+', $userProfile->phone)[1];
                    if ($ex[0]) {
                        $f = substr($ex, 0, 1);
                        $s = substr($ex, 1, 3);
                        $t = substr($ex, 4, 3);
                        $fo = substr($ex, 7, 2);
                        $fi = substr($ex, 9, 2);

                        $phone = '+' . $f . '-' . $s . '-' . $t . '-' . $fo . '-' . $fi;
                    } else {
                        $phone = $userProfile->phone;
                    }
                }

            }
        }
        ?>

        <div class="contact-data">

            <div class="contact-data-header">
                <h3>Контактная информация</h3>
            </div>

            <div id="form-contact-data">

                <div class="form-group">
                    <label class="control-label" for="contact-email">Email</label>
                    <!-- Todo val hard code -->
                    <input type="email" id="contact-email" class="form-control" name="contact-email"
                           value="<?= $email ?>" required>
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="form-group">
                    <label class="control-label" for="contact-phone">Номер телефона</label>

                    <?php
                    if ($phone !== '') {
                        echo '
                        <input type="text" id="contact-phone" class="form-control" value="' . $phone . '" name="contact-phone" required="" data-plugin-inputmask="inputmask_8540f01d">
                        <p class="help-block help-block-error"></p>';
                    } else {
                        echo MaskedInput::widget([
                            'id' => 'contact-phone',
                            'name' => 'contact-phone',
                            'mask' => '+9-999-999-99-99',
                            'options' => [
                                'class' => 'form-control',
                                'required' => true,
                                'value' => $phone
                            ]
                        ]);
                    }
                    ?>

                    <p class="help-block help-block-error"></p>
                </div>

            </div>
        </div>

        <div class="check">
            <input type="checkbox" name="pr" id="confirm-return-ticket"/> Я ознакомлен и принимаю условия <a
                    href="/site/offer" id="qway-offer-modal-show">публичной оферты</a>
        </div>

        <p class="custom-help-block"></p>

        <div class="form-group book-submit-button">
            <?= Html::submitButton('Забронировать', ['id' => 'book-submit', 'class' => 'btn btn-primary']) ?>
        </div>

    </div>

<?= $this->render('_book-error-modal') ?>
<?= $this->render('_offer-modal') ?>
<?= $this->render('_book-flight-rules-modal') ?>