<?php

/* @var $this yii\web\View */

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

Modal::begin([
    'header' => 'Правила применения тарифа',
    'id' => 'book-flight-rules-modal',
    'options' => [
        'class' => 'flight-rules-modal'
    ],
    'closeButton' => false,
    'footer' => '<button type="button" onclick="$(\'#book-flight-rules-modal\').hide();" class="btn btn-default" data-dismiss="book-flight-rules-modal">Закрыть</button>',
]);

?>
    <div id="flight-rules-result-content">

        <div>
            <div id="flight-rules-container" style="height:300px;overflow-y: scroll;"></div>
        </div>

    </div>

<?php
Modal::end();
?>