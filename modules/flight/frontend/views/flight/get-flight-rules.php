<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

//print_r($getFlightRulesResponse);
$FareRuleInfoKey = 0;
$FareRulesKey = 0;

if ($getFlightRulesResponse->Error || !$getFlightRulesResponse->FareRuleResponseInfo->FareRuleInfo) {
    if ($getFlightRulesResponse->Error) {
        die((string) $getFlightRulesResponse->Error);
    } else {
        die('Произошла ошибка');
    }
}

?>

<?php foreach($getFlightRulesResponse->FareRuleResponseInfo->FareRuleInfo as $FareRuleInfo) : ?>
<table class="table table-bordered">
    <tr>
        <th>Тариф</th>
        <th>Перевозчик</th>
        <th>Вылет</th>
        <th>Прибытие</th>
    </tr>
    <tr>
        <td><?= $FareRuleInfo->FareReference ?></td>
        <td><?= $FareRuleInfo->FilingAirline["Code"] ?></td>
        <td><?= $FareRuleInfo->DepartureAirport["LocationCode"] ?></td>
        <td><?= $FareRuleInfo->ArrivalAirport["LocationCode"] ?></td>
    </tr>
    <tr>
        <td colspan="4">
            <?php foreach($getFlightRulesResponse->FareRuleResponseInfo->FareRules as $FareRules) : ?>
                <?php if($FareRuleInfoKey == $FareRulesKey) : ?>
                    <?php foreach($FareRules->SubSection as $SubSection) : ?>
                        ================= <br />
                        <?= (string) $SubSection["SubTitle"]  ?> <br />
                        ================= <br />
                        <?= (string) $SubSection->Paragraph->Text  ?> <br />
                        <br />
                    <?php endforeach; ?>
                    <?php $FareRulesKey++; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
    </tr>
</table>
<?php $FareRuleInfoKey++; ?>
<?php endforeach; ?>