<?php

/* @var $this yii\web\View */
/* @var $searchFlight array in \modules\flight\services\methods\Flight */
/* @var $cabin string Cabin class */

use modules\flight\frontend\widgets\FlightWidget;

$allFlights = [];
$allResultArray = [];

// todo BAD
$airline = [];


//$bestFlight = [];
//$tempBestAmountFlight = [];
//$tempBestElapsedFlight = [];

foreach ($searchFlight as $key => $data) {
    /* @var $data \modules\flight\frontend\providers\amadeus\components\Flight */
    /* @var $option \modules\flight\frontend\providers\amadeus\components\Option */
//    $tempBestAmountFlight[$key] = $data->sequence->totalFare;
    foreach ($data->options as $option) {
        //$tempBestElapsedFlight[$key] = $option->elapsedTime;
        $airline[$option->getFirstSegment()->airlineCode] = $option->getFirstSegment()->getAirline()->getName();
    }
}

//$bestAmount = min($tempBestAmountFlight);
//$bestElapsed = min($tempBestElapsedFlight);
//
//$bestAmountKey = array_search($bestAmount, $tempBestAmountFlight);
//$bestElapsedKey = array_search($bestElapsed, $tempBestElapsedFlight);
?>

<div id="search-result-content">

    <?= FlightWidget::widget([
        'items' => $searchFlight,
        'airline' => $airline,
        'cabin' => $cabin,
    ]) ?>

</div>