<?php

use yii\bootstrap\ActiveForm;
use modules\flight\frontend\models\Persons;
use common\models\UserProfile;

/* @var $this yii\web\View */
/* @var $model Persons */
/* @var $code string passenger code */
/* @var $contact_person bool */
/* @var $combinationID int */
/* @var $sequenceID int */
/* @var $num int */

$img = '';
$passengerCode = Yii::$app->controller->module->params['passenger_code'];
switch ($code) {
    case $passengerCode['ADULTS']: $img = '/man_in.png'; break;
    case $passengerCode['CHILDREN']: $img = '/child_in.png'; break;
    case $passengerCode['INFANTS']: $img = '/infant_in.png'; break;
}


$form = ActiveForm::begin([
    'options' => [
        'class' => 'passenger-data-form'
    ]
]);


$profileData = [
    'surname' => '',
    'name' => '',
    'birthday' => '',
    'phone' => '',
    'email' => '',
    'gen' => '',
    'docType' => '',
    'docIssueCountry' => '',
    'docExpireDate' => '',
    'docId' => '',
];


if (!Yii::$app->user->isGuest && $contact_person && $passengerCode['ADULTS'] === $code) {
    UserProfile::getProfile($profileData);
}

?>

<div class="passenger-data-form-content-wrap">

    <div class="passenger-input-content">
        <div class="passenger-input-content-row-one">
            <?= $form->field($model, 'surname[]')->textInput(['class' => 'form-control latin-charset-data', 'value' => $profileData['surname']])->label('Фамилия') ?>
            <?= $form->field($model, 'name[]')->textInput(['class' => 'form-control latin-charset-data', 'value' => $profileData['name']])->label('Имя') ?>
            <?php if ($profileData['birthday'] !== '') : ?>
                <?= $form->field($model, 'birthday[]')->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => [
                        'id' => 'birthday-input_' . $num,
                        'class' => 'form-control qway-passenger-form-date-input qway-passenger-form-birthday-input',
                        'data' => [
                            'passenger-code' => $code,
                        ],
                        'value' => $profileData['birthday'],
                    ],
                    'mask' => '99-99-9999',

                ])->label('Дата рождения') ?>
            <?php else : ?>
                <?= $form->field($model, 'birthday[]')->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => [
                        'id' => 'birthday-input_' . $num,
                        'class' => 'form-control qway-passenger-form-date-input qway-passenger-form-birthday-input',
                        'data' => [
                            'passenger-code' => $code,
                        ],
                    ],
                    'mask' => '99-99-9999',
                ])->label('Дата рождения') ?>
            <?php endif; ?>
            <?php if ($profileData['gen'] !== '') {
                echo $form->field($model, 'gen[]')->hiddenInput(['value' => $profileData['gen']])->label(false);
                ?>
            <?php } else { ?>
                <?= $form->field($model, 'gen[]')->inline()->radioList(Persons::getGender())->label('Пол') ?>
            <?php } ?>
        </div>
        <div class="passenger-input-content-row-two">
            <?= $form->field($model, 'docType[]')->dropDownList(Persons::getDocType())->label('Тип документа') ?>

            <?= $form->field($model, 'docIssueCountry[]')->dropDownList(\modules\countries\common\models\Countries::getListItem())->label('Гражданство') ?>
            <?php if ($profileData['docExpireDate'] !== '') : ?>
                <?= $form->field($model, 'docExpireDate[]')->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => [
                        'id' => 'doc-expire-date-input_' . $num,
                        'class' => 'form-control qway-passenger-form-date-input qway-passenger-form-doc-expire-input',
                        'value' => $profileData['docExpireDate']
                    ],
                    'mask' => '99-99-9999',
                ])->label('Срок действия') ?>
            <?php else: ?>
                <?= $form->field($model, 'docExpireDate[]')->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => [
                        'id' => 'doc-expire-date-input_' . $num,
                        'class' => 'form-control qway-passenger-form-date-input qway-passenger-form-doc-expire-input'
                    ],
                    'mask' => '99-99-9999',
                ])->label('Срок действия') ?>
            <?php endif; ?>
            <?= $form->field($model, 'docID[]')->textInput(['value' => $profileData['docId'], 'class' => 'passenger-doc-id form-control'])->label('Номер документа') ?>
        </div>

        <?= $form->field($model, 'email[]')->hiddenInput(['class' => 'persons-email'])->label(false) ?>
        <?= $form->field($model, 'phonenumber[]')->hiddenInput(['class' => 'persons-phonenumber'])->label(false) ?>

    </div>

</div>

<?= $form->field($model, 'combinationID[]')->hiddenInput(['value' => $combinationID, 'class' => 'combination-id', 'id' => 'flight_combination_id'])->label(false) ?>
<?= $form->field($model, 'sequenceID[]')->hiddenInput(['value' => $sequenceID, 'class' => 'sequence-id', 'id' => 'flight_sequence_id'])->label(false) ?>
<?= $form->field($model, 'code[]')->hiddenInput(['value' => $code, 'class' => 'passenger-code'])->label(false) ?>

<?php
ActiveForm::end();