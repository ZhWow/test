<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 23.10.2017
 * Time: 16:52
 * @var $bookFormData
 */
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use modules\flight\frontend\widgets\FlightWidget;

$js = <<< JS
$('.passenger-data-forms').find('select').each(function() {
        var that = $(this), numberOfOptions = $(this).children('option').length;

        that.addClass('select-hidden');
        that.wrap('<div class="select"></div>');
        that.after('<div class="select-styled"></div>');

        var styledSelect = that.next('div.select-styled');
        styledSelect.text(that.children('option').eq(0).text());

        var listLi = $('<ul />', {
            'class': 'select-options'
        }).insertAfter(styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: that.children('option').eq(i).text(),
                rel: that.children('option').eq(i).val()
            }).appendTo(listLi);
        }

        var listItems = listLi.children('li');

        styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        listItems.click(function(e) {
            e.stopPropagation();
            styledSelect.text($(this).text()).removeClass('active');
            that.val($(this).attr('rel'));
            listLi.hide();
            //console.log(that.val());
        });

        $(document).click(function() {
            styledSelect.removeClass('active');
            listLi.hide();
        });

    });
JS;
$this->registerJs($js);
?>
<div id="wrap-book-result">

    <div id="navigation-flight-search" style="margin-bottom: 20px;">

        <?= Html::button('К результатам поиска', [
            'id' => 'back-to-search-result',
            'class' => 'btn btn-primary btn-lg',
        ]) ?>

    </div>

    <div class="recomendation-bl center" id="book_segments"></div>

<!--    <div class="price">-->
<!--        <a id="get-flight-rules" data-combinationid="" data-sequenceid=""-->
<!--           href="javascrip:void()">Условия возврата и обмена</a>-->
<!--        <div class="amount">-->
<!--            Итого к оплате: <br>-->
<!--            <span class="total" id="book_total">0</span>-->
<!--            <span class="currency">KZT</span>-->
<!--            <br>-->
<!--            <span class="taxes-and-fees">Включая все налоги и сборы</span>-->
<!--        </div>-->
<!--    </div>-->

    <div class="flex-row">
        <div class="total-amount">
            <div class="bl">
                ИТОГО к оплате:
            </div>
            <div class="bl">
                <span id="book_total">0</span> KZT
            </div>
            <div class="bl">
                Включая все налоги и сборы.
            </div>
        </div>
        <div class="conditions">
            <a href="javascript:void(0);" id="get-flight-rules">Условия обмена и возврата</a>
        </div>
    </div>
    <div class="count-people"></div>

    <div class="selec-button">
        <a href="javascript:void(0);" class="approve-recomendation">подтвердить</a>
    </div>

    <div class="passengers-block-form" style="display: none;">
        <div class="passenger-data-forms">

            <div class="passenger-data-header">
                <h3>Данные пассажиров</h3>
            </div>
            <?=$bookFormData;?>
        </div>

        <?php
            $faker = \Faker\Factory::create('ru_RU');
            $email = '';
            $phone = '';
            if (!Yii::$app->user->isGuest) {
                /* @noinspection PhpUndefinedFieldInspection */
                $email = Yii::$app->user->identity->email;
                /* @noinspection PhpUndefinedFieldInspection */
                $userProfile = \common\models\UserProfile::findOne(['user_id' => Yii::$app->user->identity->id]);
                if ($userProfile !== null) {
                    if ($userProfile->phone) {
                        $ex = explode('+', $userProfile->phone)[1];
                        if ($ex[0]) {
                            $f = substr($ex, 0, 1);
                            $s = substr($ex, 1, 3);
                            $t = substr($ex, 4, 3);
                            $fo = substr($ex, 7, 2);
                            $fi = substr($ex, 9, 2);

                            $phone = '+' . $f . '-' . $s . '-' . $t . '-' . $fo . '-' . $fi;
                        } else {
                            $phone = $userProfile->phone;
                        }
                    }

                }
            }
        ?>

        <div class="contact-data">

            <div class="contact-data-header">
                <h3>Контактная информация</h3>
            </div>

            <div id="form-contact-data">

                <div class="form-group">
                    <label class="control-label" for="contact-email">Email</label>
                    <!-- Todo val hard code -->
                    <input type="email" id="contact-email" class="form-control" name="contact-email"
                           value="<?= $email ?>" required>
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="form-group">
                    <label class="control-label" for="contact-phone">Номер телефона</label>

                    <?php
                    if ($phone !== '') {
                        echo '
                            <input type="text" id="contact-phone" class="form-control" value="' . $phone . '" name="contact-phone" required="" data-plugin-inputmask="inputmask_8540f01d">
                            <p class="help-block help-block-error"></p>';
                    } else {
                        echo MaskedInput::widget([
                            'id' => 'contact-phone',
                            'name' => 'contact-phone',
                            'mask' => '+9-999-999-99-99',
                            'options' => [
                                'class' => 'form-control',
                                'required' => true,
                                'value' => $phone
                            ]
                        ]);
                    }
                    ?>

                    <p class="help-block help-block-error"></p>
                </div>

            </div>
        </div>

        <!--<div class="check">
            <input type="checkbox" name="pr" id="confirm-return-ticket"/> Я ознакомлен и принимаю условия <a
                href="/site/offer" id="qway-offer-modal-show">публичной оферты</a>
        </div>-->

        <div class="policy">
            <div class="bl">
                <input type="checkbox" name="pr" id="confirm-return-ticket" />
                <label for="confirm-return-ticket"></label>
                Данные верны, я согласен <a href="/site/offer">с правилами оферты</a>
            </div>
        </div>

        <p class="error-hint custom-help-block"></p>

        <div class="form-group book-submit-button">
            <?= Html::submitButton('Забронировать', ['id' => 'book-submit', 'class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
<?= $this->render('_book-error-modal') ?>
<?= $this->render('_offer-modal') ?>
<?= $this->render('_book-flight-rules-modal') ?>