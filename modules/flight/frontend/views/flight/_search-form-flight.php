<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\touchspin\TouchSpin;
use modules\flight\frontend\models\base\SearchForm;
use modules\flight\frontend\models\base\FlightElectronicTickets;

/*$errorCodes = Yii::$app->getModule('flight')->params['error_codes'];
$this->registerJs("var __AMADEUS_EC = " . json_encode($errorCodes) . ";", \yii\web\View::POS_HEAD);*/
\modules\flight\frontend\assets\FlightAsset::register($this);
\yii\materialicons\AssetBundle::register($this);

$model = new \modules\flight\frontend\models\SearchFormAmadeus();

$form = ActiveForm::begin([
    'id' => 'search-flight-form',
]);

$session = $session = Yii::$app->session;
$itineraries = '';

if ($session->has('exchangeTicketId')) {
    $ticket = FlightElectronicTickets::find()->where(['id' => $session->get('exchangeTicketId')])->with('itineraries')->one();
    $itineraries = $ticket->itineraries[0];
}

$exampleCity = 'Например:
    <div class="example-input-city">
        <span class="example-city" data-example-city="moscow">Москва</span>,
        <!--<span class="example-city" data-example-city="astana">Астана</span>-->
        <span class="example-city" data-example-city="almaty">Алматы</span>
    </div>';
?>

    <div id="logo">
        <img src="/images/Qway_logo.png" alt="">
    </div>

    <div class="page-description">
        <h2>Служба покупки авиабилетов</h2>
    </div>

    <div id="search-form-header">
<!--        <span>Откуда</span>-->
<!--        <span>Куда</span>-->
    </div>

    <div class="wrap-form-group-city-data">


        <div class="search-flight-form-city-group">

            <!-- Departure input start -->

            <?php /* @noinspection PhpParamsInspection */ ?>
            <?= $form->field($model, 'departureCity',[
                    'options' => ['class' => 'qway-search-required-city'],
                    /*'template' => "
                        <div class='one-block-city-input'>
                            {label}
                            <div class='input-imitate'>
                                {input}
                                <i class='clear-input glyphicon glyphicon-remove'></i>\n
                                <span class='blocks-input'>{hint}</span>
                            </div>\n
                        </div>
                        <div class='two-block-city-input'>
                            <i class='reverse-city-flight fa fa-arrows-v' aria-hidden='true'></i>
                            {error}
                        </div>",*/
                ])->textInput([
                    'id' => 'flight-departure-city',
                    'class' => 'flight-city-input form-control',
                    'data-action' => 'departure',
                    'placeholder' => 'Откуда',
                    'value' => ($itineraries != '') ? $itineraries->cityFrom->name_ru : '',
                ])/*->hint($exampleCity)*/->label(false)
                /*->label('<img src="' . \yii\helpers\Url::toRoute('/images/from-ico.png">'))*/ ?>

            <!-- Departure input end -->


            <!-- Arrival input start -->

            <?= $form->field($model, 'arrivalCity[]', [
                    'options' => ['class' => 'qway-search-required-city qway-search-arrival-city'],
                    /*'template' => "
                        <div class='one-block-city-input'>
                            {label}
                            <div class='input-imitate'>
                                {input}
                                <i class='clear-input glyphicon glyphicon-remove remove-arrival-button'></i>\n
                                 <span class='blocks-input'>{hint}</span>
                            </div>
                        </div>
                        <div class='two-block-city-input'>
                            <span class='reverse-city-flight-empty'></span>
                            {error}
                        </div>",*/
                ])->textInput([
                    'id' => 'flight-arrival-city_1',
                    'class' => 'flight-city-input form-control',
                    'data-action' => 'arrival',
                    'placeholder' => 'Куда',
                    'value' => ($itineraries != '') ? $itineraries->cityTo->name_ru : '',
                ])/*->hint($exampleCity)*/->label(false)
                /*->label('<img src="/images/when.png">')*/ ?>



            <!-- Arrival input end -->


        </div>

        <div class="search-flight-form-date-group">

            <!-- Trip date input start -->

            <?= $form->field($model, 'tripDate[]', ['options' => [
                'id' => 'trip-date-required_1',
//
            ]])->textInput([
                'id' => 'searchform-tripdate_1',
//                'id' => 'text-calendar',
                'class' => 'trip-date-input form-control calendar',
                'placeholder' => 'Дата туда'
            ])->label(false) ?>


            <?= $form->field($model, 'tripDateBackward')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Дата обратно',
                'readonly' => true,
            ])->label(false) ?>

            <!-- Trip date input end -->

        </div>

<!--        <div class="search-flight-form-passenger-group">-->
<!---->
<!--            <input type="button" class="form-control show-hide-passenger-and-cabin-block" value="1 Пассажир" placeholder="4 adt">-->
<!---->
<!--        </div>-->
<!--        <div class="search-flight-form-passenger-group">-->
<!---->
<!--            <input type="button" class="form-control show-hide-cabin-block" value="Эконом" placeholder="Эконом">-->
<!---->
<!--        </div>-->
        <div class="search-flight-form-passenger-group required input-bl">
            <div class="searchform-span-passenger-group">
                <span class="ad-span">1</span> взрослых,
                <span class="ch-span">0</span> детей,
                <span class="inf-span">0</span> младенцев
            </div>
            <div class="passenger-group-button-container" style="display: none;">
                <div class="ad-bl bl">
                    <div class="count">1</div>
                    <div class="tbl" data-class="ad-span" data-id="searchformamadeus-adults">
                        <div class="minus">-</div>
                        <div class="center">Взрослых</div>
                        <div class="plus">+</div>
                    </div>
                </div>
                <div class="ch-bl bl">
                    <div class="count">0</div>
                    <div class="tbl" data-class="ch-span" data-id="searchformamadeus-children">
                        <div class="minus">-</div>
                        <div class="center">Детей</div>
                        <div class="plus">+</div>
                    </div>
                </div>
                <div class="anf-bl bl">
                    <div class="count">0</div>
                    <div class="tbl" data-class="inf-span" data-id="searchformamadeus-infants">
                        <div class="minus">-</div>
                        <div class="center">Младенцев</div>
                        <div class="plus">+</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="field-searchformamadeus-cabin input-bl">
            <div id="search-formamadeus-cabin">Эконом</div>
            <div class="field-searchformamadeus-cabin-select" style="display: none;">
                <div class="cabin" data-class="Economy">Эконом</div>
                <div class="cabin" data-class="Business">Бизнес</div>
                <div class="cabin" data-class="First">Первый</div>
            </div>
        </div>









    </div> <!-- #wrap-form-group-city-date -->

    <div class="wrap-passenger-and-cabin-group">

        <div id="wrap-form-group-people">

            <!-- Passenger adults count input start -->

            <?= $form->field($model, 'adults', [
                'options' => [
                    'class' => 'qway-search-required-people',
                    'data-code' => 'adt',
                ]])
                ->widget(TouchSpin::classname(), [
                /*'options' => [
                    'readonly' => true,
                ],*/
                'pluginOptions' => [
                    'min' => 1,
                    'max' => 9,
                    'buttonup_class' => 'btn btn-primary passenger-count plus',
                    'buttondown_class' => 'btn btn-info passenger-count minus',
                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                ]
            ])->label('Взрослый') ?>

            <!-- Passenger adults count input end -->

            <!-- Passenger children count input start -->

            <?= $form->field($model, 'children', [
                'options' => [
                    'class' => 'qway-search-required-people',
                    'data-code' => 'chd',
                ]])->widget(TouchSpin::classname(), [
                'id' => 'flight-children',
                'name' => 't4',
                /*'options' => [
                    'readonly' => true,
                ],*/
                'pluginOptions' => [
                    'max' => 8,
                    'buttonup_class' => 'btn btn-primary passenger-count plus',
                    'buttondown_class' => 'btn btn-info passenger-count minus',
                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                ]
            ])->label('Дети') ?>

            <!-- Passenger children count input end -->

            <!-- Passenger infants count input start -->

            <?= $form->field($model, 'infants', [
                'options' => [
                    'class' => 'qway-search-required-people',
                    'data-code' => 'inf',
                ]])->widget(TouchSpin::classname(), [
                'id' => 'flight-infants',
                'name' => 't4',
                /*'options' => [
                    'readonly' => true,
                ],*/
                'pluginOptions' => [
                    'max' => 9,
                    'buttonup_class' => 'btn btn-primary passenger-count plus',
                    'buttondown_class' => 'btn btn-info passenger-count minus',
                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                ]
            ])->label('Младенцы') ?>

            <!-- Passenger infants count input end -->

            <?= $form->field($model, 'isBackward')->hiddenInput(['value' => 0, 'id' => 'is-backward-search'])->label(false) ?>

        </div> <!-- #wrap-form-group-people -->

        <div id="wrap-form-group-cabin">
            <?//$form->field($model, 'cabin')->dropDownList(SearchForm::getCabin())->label(false) ?>
            <?= $form->field($model, 'cabin')->hiddenInput(['value' => 'Economy', 'id' => 'searchformamadeus-cabin']) ?>
        </div>

    </div>


    <div class="wrap-continue-route-group">
        <?= Html::tag('span', 'Продолжить маршрут', ['class' => 'btn btn-warning additional-arrival-button', 'data' => [
            'add-arrival-type' => 'next',
        ]]) ?>

        <?= Html::tag('span', 'Удалить маршрут', ['class' => 'btn btn-warning remove-itinerary' /*, 'data' => [
            'add-arrival-type' => 'next',
        ]*/]) ?>

        <div class="search-flight-form-passenger-group">

            <input type="button" class="form-control show-hide-passenger-and-cabin-block" value="1 Пассажир, Эконом" placeholder="4 adt">

        </div>
    </div>





    <!-- div id="wrap-form-group-additional-arrival">

        <img src="/images/add-route.png" alt="">
        <?php
//        Html::Button('Добавить новый маршрут', ['class' => 'btn btn-warning make-route'])
        ?>

    </div -->





    <div id="wrap-form-group-hidden-input">
        <!-- Departure hidden input start -->

        <?= $form->field($model, 'departureCityCode')->hiddenInput(['value' => ($itineraries != '') ? $itineraries->from : '', 'id' => 'departure-city-code_1'])->label(false) ?>
        <?= $form->field($model, 'departureCode')->hiddenInput(['value' => ($itineraries != '') ? $itineraries->from : '', 'id' => 'departure-code_1', 'class' => 'code-hidden departure'])->label(false) ?>
        <?= $form->field($model, 'departureType')->hiddenInput(['value' => ($itineraries != '') ? 'c' : '', 'id' => 'departure-type_1'])->label(false) ?>

        <!-- Departure hidden input end -->

        <!-- Arrival hidden input start -->

        <?= $form->field($model, 'arrivalCityCode[]', ['options' => [
            'class' => 'reference-city-code'
        ]])->hiddenInput(['value' => ($itineraries != '') ? $itineraries->to : '', 'id' => 'arrival-city-code_1'])->label(false) ?>

        <?= $form->field($model, 'arrivalCode[]', ['options' => [
            'class' => 'reference-code'
        ]])->hiddenInput(['value' => ($itineraries != '') ? $itineraries->to : '', 'id' => 'arrival-code_1', 'class' => 'code-hidden arrival'])->label(false) ?>

        <?= $form->field($model, 'arrivalType[]', ['options' => [
            'class' => 'reference-type'
        ]])->hiddenInput(['value' => ($itineraries != '') ? 'c' : '', 'id' => 'arrival-type_1'])->label(false) ?>

        <!-- Arrival hidden input end -->
    </div>





    <div class="form-group search-flight-submit-button">
        <?= Html::submitButton('<i class="fa fa-ticket" aria-hidden="true"></i><span>ПОИСК БИЛЕТОВ</span><i class="fa fa-angle-right" aria-hidden="true"></i>', ['id' => 'flight-submit', 'class' => 'btn btn-primary']) ?>
    </div>

<?php
ActiveForm::end();
echo $this->render('@frontend/views/site/_payment-logo-block');
echo $this->render('_overlay-loader');