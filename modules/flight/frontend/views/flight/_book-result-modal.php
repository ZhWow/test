<?php

/* @var $this yii\web\View */
/* @var $option \modules\flight\frontend\providers\amadeus\components\Option */
/* @var $flight \modules\flight\frontend\providers\amadeus\components\Flight */
/* @var $segment \modules\flight\frontend\providers\amadeus\components\Segment */

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

Modal::begin([
    'header' => '<h3>Детали брони</h3>',
    'id' => 'book-result-modal',
    'options' => [
        'class' => 'book-modal'
    ]
]);
?>
    <div id="book-result-content">

        <div class="result-item">

            <div class="item-content">
                <?php /* @var $segment \modules\flight\frontend\providers\amadeus\components\Segment */ ?>
                <div class="to-date-content block-item-content">
                    <span class="block-empty-corruptible"></span>

                    <!--                        <span class="block-child-header">Туда</span>-->
                    <span class="block-departure-date"><?= $segment->getDepartureDate() ?></span>
                    <span class="block-empty-corruptible"></span>
                </div>

                <div class="elapsed-time-content block-item-content">
                    <span class="block-empty-corruptible"></span>
                    <!--                        <span class="block-child-header">В пути</span>-->
                    <span class="block-elapsed-time"><?= $option->getElapsedTime() ?></span>
                    <span class="block-empty-corruptible"></span>
                </div>

                <div class="arrival-content block-item-content">
                    <span class="block-empty-corruptible"></span>
                    <!--                        <span class="block-child-header"> Вылет </span>-->
                    <!--<span class="block-departure-date"> </span>-->
                    <span class="block-departure-time"><?= $segment->getDepartureTime() ?></span>

                    <!--<span class="block-departure-airport"> </span>-->
                    <span class="block-departure-city"><?= $segment->getDepartureAirport()->city->getName() ?></span>
                </div>

                <div class="departure-content block-item-content">
                    <!--                        <span class="block-child-header"> Прибытие </span>-->
                    <span class="block-arrival-date"><?= $segment->getArrivalDate() ?></span>
                    <span class="block-arrival-time"><?= $segment->getArrivalTime() ?></span>
                    <!--<span class="block-arrival-airport"> </span>-->
                    <span class="block-arrival-city"><?= $segment->getArrivalAirport()->city->getName() ?></span>
                </div>

                <div class="flight-content block-item-content">
                    <!--                        <span class="block-child-header"> Рейс </span>-->
                    <span class="block-bort-name"><?= $segment->getBort()->getName() ?></span>
                </div>

                <div class="airline-content block-item-content">
                    <span class="block-empty-corruptible"></span>
                    <!--                        <span class="block-child-header"> Перевозчик </span>-->
                    <span class="block-airline-logo"><img src="<?= $segment->getAirline()->getLogo() ?>" alt="<?= $segment->getAirline()->getName() ?>"></span>
                    <span class="block-empty-corruptible"></span>
                </div>
            </div>

        </div>
        <hr>

        <div>
            <p>Номер вашей брони: <strong><span id="book-id-text">787878</span></strong></p>
        </div>

        <form action="<?= Url::toRoute('/flight/flight/payment') ?>" id="payment-form" method="post">
            <input type="hidden" name="orderNumber" id="payment-order-number">
            <input type="hidden" name="amount" id="payment-amount" value="<?= str_replace(' ', '', $flight->sequence->getTotalFare()) ?>">
            <?= Html::submitButton('Переити к оплате', ['id' => 'modal-payment-submit', 'class'=>'btn btn-primary']) ?>
        </form>

    </div>
<?php
Modal::end();
?>