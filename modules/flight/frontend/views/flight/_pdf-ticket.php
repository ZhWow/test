<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 12.09.2017
 * Time: 22:37
 * @var $models \modules\flight\frontend\models\base\FlightElectronicTickets
 */
use modules\flight\frontend\models\base\Airlines;
use modules\countries\common\models\Cities;
$person = $models->passenger;
?>
<style>
    body {
        font-family: 'Arial', sans-serif;
        color: #606060;
    }
    table {
        width: 100%;
        border-collapse: collapse;
        background: #fbfbfb;
    }
    table tr.b-bg {
        background: url(/images/b-bg.png) no-repeat center;
    }
    table tr.total-bg {
        background: #f1ab00;
        background-size: cover;
    }
    table tr.total-bg td {
        text-align: center;
    }
    table td.blue-bottom {
        border-bottom: 2px solid #0e8bdf;
    }
    table td {
        padding: 10px;
        vertical-align: top;
        font-size: 20px;
    }
    table td.silver-bg {
        width: 50%;
        vertical-align: middle;
        background: #efefef;
    }
    table td.large {
        width: 50%;
        vertical-align: middle;
    }
    table td.bord {
        border-bottom: 1px solid #4d4d4d;
    }
    table td.bott-bg {
        width: 100%;
        height: 0;
        background: url(/images/border-bottom-bg.jpg) repeat-x;
        background-size: contain;
    }
    table td.light-silver {
        vertical-align: middle;
        text-align: center;
        background: #fbfbfb;
    }
    table td.small {
        color: #606060;
    }
    table td.blue-left {
        border-right: 1px solid #0e8bdf;
    }
    table td.blue-bg {
        width: 30%;
        background: #074980;
        color: #ffffff;
    }
    table td.pink-bg {
        width: 30%;
        text-align: right;
        background: #8f2d67;
        color: #ffffff;
    }
    table td.middle {
        vertical-align: middle;
    }
    table td.center {
        text-align: center;
    }
    table td.b-bg {
        height: 100px;
        vertical-align: middle;
        font-size: 20px;
        color: #ffffff;
    }
    table td.right-white-border {
        border-right: 2px solid #ffffff;
    }
    table td.policy {
        width: 33%;
        vertical-align: top;
    }
    h1 {
        color: #000;
    }
    h1.title1 {
        font-size: 20px;
        font-weight: normal;
    }
    h1.white {
        color: #ffffff;
        font-weight: normal;
    }
    h1.big {
        font-size: 50px;
        margin: 0;
    }
    h1.total {
        font-size: 70px;
    }
    .black {
        color: #000;
    }
    img.logo {
        width: 100px;
    }
</style>
<table>
    <tr>
        <td class="blue-bottom middle" colspan="2">
            <h1 class="title1">ЭЛЕКТРОНДЫҚ БИЛЕТ / ELECTRONIC TICKET / ЭЛЕКТРОННЫЙ БИЛЕТ</h1>
            <h1 class="title2"><?=$person->surname?> <?=$person->name?></h1>
        </td>
        <td class="blue-bottom">
            <img src="/images/logo-tickets.png" class="logo" alt="logo">
        </td>
        <td class="blue-bottom">
            <h2>Qway</h2>
            <p class="coords">
                Astana, 11, Enbekshilder st.,<br>
                BC "Bolashak", 2 fl.<br>
                +7 7172 456530<br>
                tickets@btmc.kz
            </p>
        </td>
    </tr>
    <tr>
        <td class="silver-bg bord small blue-left" align="center" colspan="2">
            Төлқұжат түрі / Identification /<br>
            Документ удостоверяющий личность :
        </td>
        <td class="large bord" colspan="2" align="center">
            <h1><?=$person->doc_id?></h1>
        </td>
    </tr>
    <tr>
        <td class="bord large small blue-left" align="center"  colspan="2">
            Билеттің нөмірі / Ticket number /<br>
            Номер билета :
        </td>
        <td class="bord large" colspan="2" align="center">
            <h1><?=$models->ticket_number?></h1>
        </td>
    </tr>
    <!--<tr>
        <td class="bott-bg" colspan="4"></td>
    </tr>-->
    <tr>
        <td class="bord small light-silver" style="text-align: right;">
            Броньдау нөмірі /<br>
            Booking Reference /<br>
            Номер бронирования
        </td>
        <td class="bord light-silver blue-left">
            <h3><?=$models->pnr?></h3>
        </td>
        <td class="bord small light-silver" style="text-align: right;">
            Әуе компаниясы / Airline /<br>
            Авиакомпания
        </td>
        <td class="bord light-silver">
            <?php foreach ($models->itineraries as $itinerary) :?>
                <h3><?=$itinerary->operating_airline_code?></h3>
            <?php endforeach;?>
        </td>
    </tr>
    <tr>
        <td class="bord small light-silver" style="text-align: right;">
            Берілген күні /<br>
            Issuing date /<br>
            Дата выписки билета
        </td>
        <td class="bord light-silver blue-left">
            <h3><?=date('d.m.Y', strtotime($models->ticketing_date));?></h3>
        </td>
        <td class="bord small light-silver" style="text-align: right;">
            БТН / BIN / БИН
        </td>
        <td class="bord light-silver">
            <h3>010940000162</h3>
        </td>
    </tr>
    <tr>
        <td class="bord small light-silver" style="text-align: right;">
            СТН / Tax No / РНН
        </td>
        <td class="bord light-silver blue-left">
            <h3>620300022100</h3>
        </td>
        <td class="bord small light-silver" style="text-align: right;">
            ҚҚС бойынша есепке қою нөмірі /<br>
            VAT series / Номер постановки<br>
            на учет НДС
        </td>
        <td class="bord light-silver">
            <h4 align="left">
                60001 № 0076632, 19 қыркүйек 2012<br>
                60001 No. 0076632 dated September 19, 2012<br>
                60001 № 0076632 от 19 сентября 2012
            </h4>
        </td>
    </tr>
    <?php foreach ($models->itineraries as $segment):?>
        <?php
        $operating_airline_name = Airlines::findOne(['code_en' => $segment->operating_airline_code])->name_en;
        $departure_airport_name = Cities::findOne(['code' => $segment->from])->name_en;
        $arrival_airport_name = Cities::findOne(['code' => $segment->to])->name_en;
        ?>
        <tr>
            <td colspan="1" class="blue-bg">
                Откуда:
                <h1 class="white"><?=$departure_airport_name?>(<?=$segment->from?>)</h1>
            </td>
            <td colspan="2" class="middle center">
                Тариф пен билеттің қолданыс мерзімі / Ticket/Fare Validity period /<br>
                Срок действия тарифа и билета: Мерзімі бойынша шектеу қойылмаған
            </td>
            <td colspan="1" class="pink-bg">
                Куда:
                <h1 class="white"><?=$arrival_airport_name?>(<?=$segment->to?>)</h1>
            </td>
        </tr>
        <tr>
            <td colspan="1" class="blue-bg">
                Время вылета:
                <h1 class="white big"><?=date('H:i', strtotime($segment->departure_date))?></h1>
                <?=date('d M Y D', strtotime($segment->departure_date))?>
            </td>
            <td colspan="2" class="middle center">
                <!--                Тариф пен билеттің қолданыс мерзімі / Ticket/Fare Validity period /<br>-->
                <!--                Срок действия тарифа и билета: Мерзімі бойынша шектеу қойылмаған-->
            </td>
            <td colspan="1" class="pink-bg">
                Время прилета:
                <h1 class="white big"><?=date('H:i', strtotime($segment->arrival_date))?></h1>
                <?=date('d M Y D', strtotime($segment->arrival_date))?>
            </td>
        </tr>
        <tr class="b-bg">
            <td class="b-bg" style="text-align: right;">
                Тасымалдаушы /<br>
                Airline /<br>
                Перевозчик :
            </td>
            <td class="b-bg right-white-border">
                <h3 style="color: #ffffff;"><?=$operating_airline_name?></h3>
            </td>
            <td class="b-bg" style="text-align: right;">
                Рейс нөмірі /<br>
                Flight number /<br>
                Номер рейса :
            </td>
            <td class="b-bg">
                <h3 style="color: #ffffff;"><?=$segment->operating_airline_code?><?=$segment->flight_no?></h3>
            </td>
        </tr>
    <?php endforeach;?>
    <tr>
        <td>
            Базалық тариф /<br>
            Fare base /<br>
            Базовый тариф :
        </td>
        <td class="blue-left">
            <h3><?=number_format((int) $models->total_fare, 0, ' ', ' ')?> KZT</h3>
        </td>
        <td>
            Салықтар, алымдар /<br>
            Taxes and charges /<br>
            Налоги и сборы :
        </td>
        <td>
            <h3><?=number_format((int) $models->total_tax_amount, 0, ' ', ' ')?> KZT</h3>
        </td>
    </tr>
    <tr>
        <td>
            Қызмет көрсету алымы<br>
            (12% қосымша құн салығын қосқанда) /<br>
            Agency service fee (VAT 12% included) /<br>
            Сервисный сбор (включая 12% НДС) :
        </td>
        <td class="blue-left">
            <?php
            $tax = round(($models->total_fare) * 12 / 112, 2);
            ?>
            <h3><?=number_format($tax, 0, ' ', ' ')?> KZT</h3>
        </td>
        <td>
            Алым /<br>
            Fee /<br>
            Сбор:
        </td>
        <td>
            <h3>0 KZT</h3>
        </td>
    </tr>
    <tr class="total-bg">
        <td colspan="4">
            <h3 class="black">Барлығы / Total price / Итого : </h3>
            <h1 class="total"><?=number_format(((int) $models->total_fare + (int) $models->total_tax_amount + $tax), 0, ' ', ' ')?> KZT</h1>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="center">
            <h3 class="black">
                <?=number_format((int) $models->total_fare, 0, ' ', ' ')?> теңгеден 12% ҚҚС <?=number_format((int) $tax, 0, ' ', ' ')?> теңгені құрайды / VAT 12% from <?=number_format((int) $models->total_fare, 0, ' ', ' ')?> KZT is <?=number_format((int) $tax, 0, ' ', ' ')?> KZT / НДС 12% от <?=number_format((int) $models->total_fare, 0, ' ', ' ')?> KZT составляет <?=number_format((int) $tax, 0, ' ', ' ')?> KZT
            </h3>
        </td>
    </tr>
    <!--<tr>
        <td class="bott-bg" colspan="4"></td>
    </tr>-->
    <tr>
        <td class="light-silver policy">
            <h2>IATA ЕСКЕРТПЕСІ</h2>
            <p class="text">
                http://www.iatatravelcentre.com/e-ticket-notice/General/English/
                (алдын-ала ескертусіз өзгертуге болады)
            </p>
            <h2>ТАСЫМАЛДАУ ШАРТТАРЫ</h2>
            <p class="text">
                ЖОЛАУШЫ ТАСЫМАЛЫ ЖӘНЕ ТАСЫМАЛДАУШЫ КӨРСЕТЕТІН
                ӨЗГЕ ДЕ ҚЫЗМЕТ ТҮРЛЕРІ ТАСЫМАЛДАУШЫ КІМ ЕКЕНІНЕ ТӘУЕЛДІ БОЛЫП ТАБЫЛАТЫН ТАСЫМАЛДАУ ШАРТТАРЫМЕН АНЫҚТАЛАДЫ. ТОЛЫҚ АҚПАРАТТЫ ВАЛИДАЦИЯ ЖАСАЙТЫН
                ТАСЫМАЛДАУШЫДАН АЛУЫҢЫЗҒА БОЛАДЫ. ВАРШАВА КОНВЕНЦИЯСЫНЫҢ 3-ШІ БАБЫНА СӘЙКЕС БАҒЫТ ПАРАҚШАСЫ ЖОЛАУШЫ БИЛЕТІ РЕТІНДЕ РЕСМИ ТҮРДЕ ТАНЫЛАДЫ (ТАСЫМАЛДАУШЫ ЖОЛАУШЫҒА 3-ШІ БАПТЫҢ ТАЛАПТАРЫНА САЙ КЕЛЕТІН БАСҚА АЛЬТЕРНАТИВТІ ҚҰЖАТ БЕРГЕН ЖАҒДАЙЛАРДЫ ЕСЕПТЕМЕГЕНДЕ).
            </p>
            <h2>ЕСКЕРТПЕ</h2>
            <p class="text">
                ЕГЕР ЖОЛ ЖҮРУДІҢ ІШІНЕ ҰШЫП ШЫҒУ ЕЛІНЕН ТЫС ЖЕРДЕГІ ТҮПКІЛІКТІ ПУНКТ НЕМЕСЕ АЯЛДАУ КІРЕТІН БОЛСА ВАРШАВА КОНВЕНЦИЯСЫНЫҢ ЕРЕЖЕЛЕРІ ҚОЛДАНЫЛУЫ МҮМКІН. ВАРШАВА КОНВЕНЦИЯСЫ ТАСЫМАЛДАУШЫНЫҢ ДЕНСАУЛЫҚҚА ЗАЛАЛ КЕЛУ НЕМЕСЕ АЖАЛ ЖАҒДАЙЛАРЫ, СОНДАЙ-АҚ, ЖҮКТІҢ ЖОҒАЛУ НЕМЕСЕ ЗАҚЫМДАЛУ ЖАҒДАЙЛАРЫ ҮШІН ЖАУАПКЕРШІЛІГІН РЕТТЕЙДІ ЖӘНЕ КӨП ЖАҒДАЙДА ШЕКТЕЙДІ. СОНДАЙ-АҚ, "ЖАУАПКЕРШІЛІКТІ ШЕКТЕУ МӘСЕЛЕЛЕРІ БОЙЫНША ЖОЛАУШЫЛАРҒА КЕҢЕСТЕР" ЖӘНЕ "ЖҮКТІҢ ЖОҒАЛҒАНЫ/ЗАҚЫМДАНҒАНЫ ҮШІН ШЕКТЕУЛЕР ТУРАЛЫ" ДЕГЕН ХАБАРЛАМАЛАРҒА НАЗАР АУДАРЫҢЫЗ.
            </p>
        </td>
        <td class="light-silver policy" colspan="2">
            <h2>IATA NOTICE</h2>
            <p class="text">
                http://www.iatatravelcentre.com/e-ticket-notice/General/English/
                (алдын-ала ескертусіз өзгертуге болады)
            </p>
            <h2>CONTRACT OF CARRIAGE</h2>
            <p class="text">
                CARRIAGE AND OTHER SERVICES PROVIDED BY THE CARRIER ARE SUBJECT TO CONDITIONS OF CARRIAGE, WHICH ARE HEREBY INCORPORATED BY REFERENCE. THESE CONDITIONS MAY BE OBTAINED FROM THE ISSUING CARRIER. THE ITINERARY/RECEIPT CONSTITUTES THE 'PASSENGER TICKET' FOR THE PURPOSES OF ARTICLE 3 OF THE WARSAW CONVENTION, EXCEPT WHERE THE CARRIER DELIVERS TO THE PASSENGER ANOTHER DOCUMENT COMPLYING WITH THE REQUIREMENTS OF ARTICLE 3.
            </p>
            <h2>NOTICE</h2>
            <p class="text">
                IF THE PASSENGER'S JOURNEY INVOLVES AN ULTIMATE DESTINATION OR STOP IN A COUNTRY OTHER THAN THE COUNTRY OF DEPARTURE THE WARSAW CONVENTION MAY BE APPLICABLE AND THE CONVENTION GOVERNS AND IN MOST CASES LIMITS THE LIABILITY OF CARRIERS FOR DEATH OR PERSONAL INJURY AND IN RESPECT OF LOSS OF OR DAMAGE TO BAGGAGE. SEE ALSO NOTICES HEADED ADVICE TO INTERNATIONAL PASSENGERS ON LIMITATION OF LIABILITY' AND 'NOTICE OF BAGGAGE LIABILITY LIMITATIONS'.
            </p>
        </td>
        <td class="light-silver policy">
            <h2>УВЕДОМЛЕНИЕ IATA</h2>
            <p class="text">
                http://www.iatatravelcentre.com/e-ticket-notice/General/English/
                (алдын-ала ескертусіз өзгертуге болады)
            </p>
            <h2>УСЛОВИЯ ПЕРЕВОЗКИ</h2>
            <p class="text">
                ПЕРЕВОЗКА И ПРОЧИЕ УСЛУГИ, ПРЕДОСТАВЛЯЕМЫЕ ПЕРЕВОЗЧИКОМ, ОПРЕДЕЛЯЮТСЯ УСЛОВИЯМИ ПЕРЕВОЗКИ, КОТОРЫЕ ВАРЪИРУЮТСЯ В ЗАВИСИМОСТИ ОТ ПЕРЕВОЗЧИКА. БОЛЕЕ ПОЛНУЮ ИНФОРМАЦИЮ ВЫ МОЖЕТЕ ПОЛУЧИТЬ У ВАЛИДИРУЮЩЕГО ПЕРЕВОЗЧИКА. МАРШРУТНЫЙ ЛИСТ ОФИЦИАЛЬНО ПРИЗНАЕТСЯ КАК "ПАССАЖИРСКИЙ БИЛЕТ" В СООТВЕТСТВИИ СО СТАТЬЕЙ 3 ВАРШАВСКОЙ КОНВЕНЦИИ, ЗА ИСКЛЮЧЕНИЕМ СЛУЧАЕВ, КОГДА ПЕРЕВОЗЧИК ПРЕДОСТАВЛЯЕТ ПАССАЖИРУ АЛЬТЕРНАТИВНЫЙ ДОКУМЕНТ, ОТВЕЧАЮЩИЙ ТРЕБОВАНИЯМ СТАТЬИ 3.
            </p>
            <h2>УВЕДОМЛЕНИЕ</h2>
            <p class="text">
                ЕСЛИ ПОЕЗДКА ВКЛЮЧАЕТ КОНЕЧНЫЙ ПУНКТ ИЛИ ОСТАНОВКУ ЗА ПРЕДЕЛАМИ СТРАНЫ ВЫЛЕТА, ПОЛОЖЕНИЯ ВАРШАВСКОЙ КОНВЕНЦИИ МОГУТ БЫТЬ ПРИМЕНИМЫ. ВАРШАВСКАЯ КОНВЕНЦИЯ РЕГУЛИРУЕТ И ЧАСТО ОГРАНИЧИВАЕТ ОТВЕТСТВЕННОСТЬ ПЕРЕВОЗЧИКА ЗА ПРИЧИНЕНИЕ ВРЕДА ЗДОРОВЬЮ ИЛИ СМЕРТЬ, А ТАКЖЕ В СЛУЧАЯХ УТЕРИ ИЛИ ПОВРЕЖДЕНИЯ БАГАЖА. ТАКЖЕ ОБРАТИТЕ ВНИМАНИЕ НА УВЕДОМЛЕНИЯ "СОВЕТЫ ПАССАЖИРАМ ПО ВОПРОСАМ ОГРАНИЧЕНИЯ
                ОТВЕТСТВЕННОСТИ" И " ОБ ОГРАНИЧЕНИЯХ ЗА
                УТЕРЮ/ПОРЧУ БАГАЖА".
            </p>
        </td>
    </tr>
</table>
