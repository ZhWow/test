<?php

/* @var $this yii\web\View */
/* @var $segment modules\flight\frontend\providers\amadeus\components\Segment */
/* @var $option modules\flight\frontend\providers\amadeus\components\Option*/

use modules\flight\frontend\providers\amadeus\Amadeus;

?>

<div class="flight-content block-item-content">
    <!--<span class="block-child-data"><?/*= $option->getTransplantation() */?></span>-->
    <span class="block-child-data"><?= AmadeusSoapClient::CABIN ?></span> <!--TODO HARD CODE-->
    <span class="block-child-data free-seats">
        <?= $segment->getFreeSeats() ?> мест
        <br>
        <?= $segment->getBort()->getName() ?>
    </span>
    <span class="block-child-data"><?= $option->getElapsedTime() ?></span>

</div>