<?php

/* @var $this yii\web\View */
/* @var $amount int */
/* @var $currency string */
/* @var $key int */
/* @var $combinationID int */
/* @var $sequenceID int */
/* @var $last bool */

?>

<div class="amount-content block-item-content">
    <?php if ($last) : ?>
    <span class="block-child-data block-amount-data"><?= $amount . ' ' . $currency ?></span>
    <span>
        <button id="add-ticket" class="btn btn-info" data-key="<?= $key ?>" data-combination-id="<?= $combinationID ?>" data-sequence-id="<?= $sequenceID ?>">Выбрать</button>
    </span>
    <?php endif; ?>
</div>