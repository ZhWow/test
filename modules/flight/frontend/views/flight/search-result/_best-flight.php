<?php

/* @var $this yii\web\View */
/* @var $option \modules\flight\services\components\Option */
/* @var $amount int */
/* @var $amount int */
/* @var $currency string */
/* @var $key int */
/* @var $combinationID int */
/* @var $sequenceID int */
/* @var $ticketDesignators string */
/* @var $bestAmountKey int */
/* @var $bestElapsedKey int */
?>



<div class="best-flight">
    <header>
        <div class="title">Самый удобный и самый дешевый</div>
    </header>
    <div class="content-best-flight">

        <?= $this->render('_departure-block', ['segment' => $option->getFirstSegment()]) ?>
        <?= $this->render('_arrival-block', ['segment' => $option->getLastSegment()]) ?>
        <?= $this->render('_flight-block', [
            'segment' => $option->getFirstSegment(),
            'option' => $option,
        ]) ?>
        <?= $this->render('_airline-block', ['segment' => $option->getFirstSegment(),]) ?>
        <?= $this->render('_amount-block', [
            'amount' => $amount,
            'currency' => $currency,
            'key' => $key,
            'combinationID' => $combinationID,
            'sequenceID' => $sequenceID,
            'last' => true,
        ]) ?>
    </div>
</div>
