<?php

/* @var $this yii\web\View */
/* @var $segment \modules\flight\services\components\Segment */

?>

<div class="departure-content block-item-content">
    <span class="block-child-data"><?= $segment->getDepartureDate() ?></span>
    <span class="block-child-data block-time-location"><?= $segment->getDepartureTime() ?></span>
    <!--<span class="block-child-data"><?/*= $segment->getDepartureAirport()->getName() */?></span>-->
    <span class="block-child-data"><?= $segment->getDepartureAirport()->city->getName() ?></span>
</div>