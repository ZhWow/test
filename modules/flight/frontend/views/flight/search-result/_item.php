<?php

/* @var $this yii\web\View */
/* @var $type string */
/* @var $amount int */
/* @var $currency string */
/* @var $key int */
/* @var $combinationID int */
/* @var $sequenceID int */
/* @var $num int */
/* @var $option modules\flight\frontend\providers\amadeus\components\Option*/
/* @var $segment */

?>

<?php if ($type === 'one') : ?>
    <?= $this->render('_departure-block', ['segment' => $option->getFirstSegment()]) ?>
    <?= $this->render('_arrival-block', ['segment' => $option->getLastSegment()]) ?>
    <?= $this->render('_flight-block', [
        'segment' => $option->getFirstSegment(),
        'option' => $option,
    ]) ?>
    <?= $this->render('_airline-block', ['segment' => $option->getFirstSegment(),]) ?>
    <?= $this->render('_amount-block', [
        'amount' => $amount,
        'currency' => $currency,
        'key' => $key,
        'combinationID' => $combinationID,
        'sequenceID' => $sequenceID,
        'last' => true,
    ]) ?>
<?php else :  ?>
    <div class="multi-flight">
        <?= $this->render('_departure-block', ['segment' => $segment]) ?>
        <?= $this->render('_arrival-block', ['segment' => $segment]) ?>
        <?= $this->render('_flight-block', [
            'segment' => $segment,
            'option' => $option,
        ]) ?>
        <?= $this->render('_airline-block', ['segment' => $segment]) ?>
        <?php
        $isLast = $num === count($option->segments) ? true : false;
        echo $this->render('_amount-block', [
            'amount' => $amount,
            'currency' => $currency,
            'key' => $key,
            'combinationID' => $combinationID,
            'sequenceID' => $sequenceID,
            'last' => $isLast,
        ]);
        ?>
    </div>
    <hr class="flight-border">
<?php endif; ?>