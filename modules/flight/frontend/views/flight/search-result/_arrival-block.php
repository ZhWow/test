<?php

/* @var $this yii\web\View */
/* @var $segment \modules\flight\services\components\Segment */

?>

<div class="arrival-content block-item-content">
    <span class="block-child-data"><?= $segment->getArrivalDate() ?></span>
    <span class="block-child-data block-time-location"><?= $segment->getArrivalTime() ?></span>
    <!--<span class="block-child-data"><?/*= $option->getLastSegment()->getArrivalAirport()->getName() */?></span>-->
    <span class="block-child-data"><?= $segment->getArrivalAirport()->city->getName() ?></span>
</div>