<?php

/* @var $this yii\web\View */
/* @var $segment \modules\flight\services\components\Segment */

?>

<div class="airline-content block-item-content">
    <span class="block-child-data"><img src="<?= $segment->getAirline()->getLogo() ?>" alt="<?= $segment->getAirline()->getName() ?>"></span>
    <span class="block-child-data airline-name"><?= $segment->getAirline()->getName() ?></span>
    <!--<span class="block-child-data"><?/*= $option->getFirstSegment()->getBort()->getName() */?></span>-->
</div>