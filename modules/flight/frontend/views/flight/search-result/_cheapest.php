<?php

/* @var $this yii\web\View */
/* @var $option modules\flight\frontend\providers\amadeus\components\Option*/

?>

<div class="cheapest-content block-item-content">
    <span class="block-child-data"><?= $option->getFirstSegment()->getDepartureDate() ?></span>
    <span class="block-child-data block-time-location"><?= $option->getFirstSegment()->getDepartureTime() ?></span>
    <span class="block-child-data"><?= $option->getFirstSegment()->getDepartureAirport()->getName() ?></span>
    <span class="block-child-data"><?= $option->getFirstSegment()->getDepartureAirport()->city->getName() ?></span>
</div>