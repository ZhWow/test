<?php

/* @var $this yii\web\View */

use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h3>Оферта</h3>',
    'id' => 'offer-modal',
]);

?>
    <div id="offer-content">

        <?= $this->render('@frontend/views/site/_offer-content') ?>

    </div>
<?php
Modal::end();
?>