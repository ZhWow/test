<?php

/* @var $this yii\web\View */

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

Modal::begin([
    'header' => '<h3>Детали брони</h3>',
    'id' => 'book-error-modal',
    'options' => [
        'class' => 'book-modal'
    ],

]);

?>
    <div id="book-result-content">

        <div>
            <p id="book-id-error-text"></p>
            <?= Html::a('Вернутся к поиску' , Url::toRoute('/')) ?>
        </div>

    </div>
<?php
Modal::end();
?>