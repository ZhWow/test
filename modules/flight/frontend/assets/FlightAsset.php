<?php

namespace modules\flight\frontend\assets;

use yii\web\AssetBundle;

class FlightAsset extends AssetBundle
{
    public $sourcePath = '@modules/flight/frontend/assets';
    public $css = [
         'css/jquery-ui.min.css',
         'css/jquery-ui.theme.min.css',
//         'pg/css/semantic.ui.min.css',
//         'pg/css/prism.css',
         'pg/css/calendar-style.css',
//         'pg/css/style.css',
         'pg/css/pignose.calendar.css',
         'css/protip.min.css',
    ];
    public $js = [
        'js/jquery.autocomplete.min.js',
        'js/jquery.datepicker.min.js',

        'pg/js/moment.latest.min.js',
//        'pg/js/semantic.ui.min.js',
//        'pg/js/prism.min.js',
        'pg/js/pignose.calendar.js',
        'js/protip.min.js',
        'js/flight.js',
        'js/sweetalert2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}