(function ($) {

    /* ========================================================================
     * INIT
     * ======================================================================== */


    var controllerUrl = '/flight/flight/';
    var mainPageUrl = '/';
    var $body = $('body');
    window.__allResult = null;
    var bookFormErrorMessage = 'Заполните все обязательнные поля';
    var allFlight = $('.search-result-item');
    var content = $('#all-search-item-content');
    var emptyInputs = [];
    var notCorrectData = [];
    var notCorrectBirthday = [];
    var notCorrectDocExpire = [];
    var airlineSortFlight = false;
    var roundSortFlight = false;







    /* ========================================================================
     * Date picker
     * ======================================================================== */

    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
    }
    $( "#searchform-tripdate_1").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: new Date(),
        duration: 'slow',
        firstDay: 1,
        // pickerPosition: "bottom",
        //orientation: "right bottom",
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        beforeShow : function(input, inst) {
            /*var today = $('.ui-datepicker-today');
            if (today.length) {
                $('.message-before').append(today.find('a').html()+'<br>');
            } else {
                $('.message-before').append('Today DOM element is not rendered yet.<br>');
            }*/
        },
        afterShow : function(inst) {
            /*var today = $('.ui-datepicker-today');
            if (today.length) {
                $('.message-after').append(today.find('a').html()+'<br>');
            } else {
                $('.message-after').append('Today DOM element should be already rendered.<br>');
            }*/
            $(".ui-datepicker-prev").empty().append('<i class="fa fa-chevron-left"></i>');
             $(".ui-datepicker-next").empty().append('<i class="fa fa-chevron-right"></i>');
        },
        onSelect: function(dateText, inst) {
            if (dateText && $('#searchformamadeus-tripdatebackward').is(':visible')) {
                var minDateArray = dateText.split('-');
                $('#searchformamadeus-tripdatebackward').prop({'readonly': false});
                $("#searchformamadeus-tripdatebackward").datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: new Date(minDateArray[2] + '-' + minDateArray[1] + '-' + minDateArray[0]),
                    duration: 'slow',
                    firstDay: 1,
                    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    /*beforeShow : function(input, inst) {
                        var today = $('.ui-datepicker-today');
                        if (today.length) {
                            $('.message-before').append(today.find('a').html()+'<br>');
                        } else {
                            $('.message-before').append('Today DOM element is not rendered yet.<br>');
                        }
                    },*/
                    afterShow : function(inst) {
                        /*var today = $('.ui-datepicker-today');
                        if (today.length) {
                            $('.message-after').append(today.find('a').html()+'<br>');
                        } else {
                            $('.message-after').append('Today DOM element should be already rendered.<br>');
                        }*/
                        $(".ui-datepicker-prev").empty().append('<i class="fa fa-chevron-left"></i>');
                        $(".ui-datepicker-next").empty().append('<i class="fa fa-chevron-right"></i>');
                    },
                    /*onSelect: function(dateText, inst) {
                        if (dateText && $('#searchformamadeus-tripdatebackward').is(':visible')) {
                            $('#searchformamadeus-tripdatebackward').prop({'readonly': false});
                        }
                        // console.log($("input[name='something']").val(dateText));
                    }*/
                });
            } else {
                if ($('#searchform-tripdate_2').is('[readonly]')) {
                    $('#searchform-tripdate_2').prop({'readonly': false});
                    var minDateArray = dateText.split('-');

                    $('#searchform-tripdate_2').datepicker({
                        dateFormat: 'dd-mm-yy',
                        minDate: new Date(minDateArray[2] + '-' + minDateArray[1] + '-' + minDateArray[0]),
                        duration: 'slow',
                        firstDay: 1,
                        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        /*beforeShow : function(input, inst) {
                         var today = $('.ui-datepicker-today');
                         if (today.length) {
                         $('.message-before').append(today.find('a').html()+'<br>');
                         } else {
                         $('.message-before').append('Today DOM element is not rendered yet.<br>');
                         }
                         },*/
                        afterShow : function(inst) {
                            /*var today = $('.ui-datepicker-today');
                             if (today.length) {
                             $('.message-after').append(today.find('a').html()+'<br>');
                             } else {
                             $('.message-after').append('Today DOM element should be already rendered.<br>');
                             }*/
                            $(".ui-datepicker-prev").empty().append('<i class="fa fa-chevron-left"></i>');
                            $(".ui-datepicker-next").empty().append('<i class="fa fa-chevron-right"></i>');
                        },
                        /*onSelect: function(dateText, inst) {
                         if (dateText && $('#searchformamadeus-tripdatebackward').is(':visible')) {
                         $('#searchformamadeus-tripdatebackward').prop({'readonly': false});
                         }
                         // console.log($("input[name='something']").val(dateText));
                         }*/
                    });
                }

            }
            // console.log($("input[name='something']").val(dateText));
        }
    });




    /*$('#searchform-tripdate_1').datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: new Date(),
        duration: 'slow',
        firstDay: 1,
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        navigationAsDateFormat: true, nextText: 'MM', prevText: 'MM',
        afterShow: function () {
            console.log($(".ui-datepicker-prev"));
        }
    });*/


    /*$('#searchform-tripdate_1').pignoseCalendar({
        format: 'DD-MM-YYYY', // date format string. (2017-02-02)
        lang: 'ru',
        apply: onSelectHandler,
        theme: 'blue',
        week: 1,
        buttons: true,
        // toggle: true
        // reverse: true,
        minDate: new Date(),
    });*/

    /*$('#searchformamadeus-tripdatebackward').pignoseCalendar({
     format: 'DD-MM-YYYY', // date format string. (2017-02-02)
     lang: 'ru',
     // apply: onSelectHandler,
     theme: 'blue',
     week: 1,
     buttons: true,
     minDate: new Date(),
     });*/


    /* function reverseDate(date) {
     var dateArray = date.split('-');
     var year = date[2];
     var month = date[1];
     var day = date[0];
     var sr = '-';
     console.log(year + sr + month + sr + day);
     return year + sr + month + sr + day;
     }*/


    /* ========================================================================
     * Auto complete Search form
     * ======================================================================== */

    autoComplete($('#flight-departure-city'), 1);
    autoComplete($('#flight-arrival-city_1'), 1);

    function getCountryByCode(data, code) {
        let matcher = new RegExp('^' + code.toUpperCase(), "i");

        return $.map(data, function (item) {
            let $return;
            if (matcher.test(item.citycode.toUpperCase())) {
                if (item.citycode === item.code && item.type === 'c') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                } else if (item.citycode !== item.code && item.type === 'a') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                }
            } else if (matcher.test(item.name_ru.toUpperCase())) {
                if (item.citycode === item.code && item.type === 'c') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                } else if (item.citycode !== item.code && item.type === 'a') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                }
            } else if (matcher.test(item.code.toUpperCase())) {
                if (item.citycode === item.code && item.type === 'c') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                } else if (item.citycode !== item.code && item.type === 'a') {
                    $return = {
                        'value': (item.type === 'c') ? item.name_ru : item.country + ' ('+item.name_ru+')',
                        'cityCode': item.citycode,
                        'code': item.code,
                        'typeCode': item.type
                    };
                }
            }

            return $return;
        });
    }


    /**
     * Автокомплит поиска городов
     *
     * @param $selector object jQuery
     * @param num int Порядковый номер элемента
     */
    // function autoComplete($selector, num) {
    //     // $selector.autocomplete({
    //     //     minLength: 2,
    //     //     source: function( request, add ) {
    //     //         let term = request.term;
    //     //
    //     //         fetch('/cities.json').then(function (responses) {
    //     //             return responses.json();
    //     //         }).then(function (responseData) {
    //     //             let city = getCountryByCode(responseData.rows, term);
    //     //             add(city);
    //     //         });
    //     //     }
    //     // });
    //
    //     $selector.autocomplete({
    //         serviceUrl: controllerUrl+'get-city',
    //         // serviceUrl: '/cities2.json',
    //         minChars: 3,
    //         deferRequestBy: 350, // Задержка запроса (мсек), на случай, если мы не хотим слать миллион запросов, пока пользователь печатает.
    //         // lookupLimit: 10,
    //         type: 'GET',
    //         maxHeight: 'auto',
    //         paramName: 'city',
    //         dataType: 'json',
    //         transformResult: function (response, query) {
    //
    //             if ($('.wrap-form-group-city-data').length === 1 && $selector.attr('id') === 'flight-departure-city') {
    //                 $('.wrap-form-group-city-data').css({'border-radius': '2em 2em 2em 0'});
    //             }
    //             if ($('.wrap-form-group-city-data').length === 2 && $selector.attr('id') === 'additional-departure_2') {
    //                 $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 0'});
    //             }
    //             return {
    //                 lookupLimit: 15,
    //                 suggestions: getCountryByCode(response.rows, query)
    //             };
    //         },
    //         onSelect: function (suggestions) {
    //             // На input-е поиска есть data атрибут с значением (напр [departure и arrival])
    //             // У всех hidden input-ов которые к ним принадлежат, ID начинаются с префикса [departure и arrival]
    //             // напр (departure-city-code_1)
    //
    //             if ($selector.hasClass('has-foreign-arrival')) {
    //
    //                 if ($('.wrap-form-group-city-data').length === 2 && $selector.attr('id') === 'additional-departure_2') {
    //                     // $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 2em'});
    //                     $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 2em'});
    //                 }
    //                 var $foreignSelector = $('#flight-arrival-city_' + $selector.attr('data-foreign'));
    //                 var number = $selector.attr('data-foreign');
    //
    //                 $foreignSelector.val(suggestions.value);
    //
    //                 /*console.log($selector);
    //                  console.log($foreignSelector.attr('data-action'));
    //                  console.log($('#' + $foreignSelector.attr('data-action') + '-city-code_' + number));
    //                  console.log(number);*/
    //
    //                 $('#' + $foreignSelector.attr('data-action') + '-city-code_' + number).val(suggestions.cityCode);
    //                 $('#' + $foreignSelector.attr('data-action') + '-code_' + number).val(suggestions.code);
    //                 $('#' + $foreignSelector.attr('data-action') + '-type_' + number).val(suggestions.typeCode);
    //
    //
    //             } else {
    //                 if ($('.wrap-form-group-city-data').length === 1 && $selector.attr('id') === 'flight-departure-city') {
    //                     $('.wrap-form-group-city-data').css({'border-radius': '2em'});
    //                 }
    //
    //                 if ($('#' + $selector.attr('data-action') + '-city-code_' + num)) {
    //                     $('#' + $selector.attr('data-action') + '-city-code_' + num).val(suggestions.cityCode);
    //                     $('#' + $selector.attr('data-action') + '-code_' + num).val(suggestions.code);
    //                     $('#' + $selector.attr('data-action') + '-type_' + num).val(suggestions.typeCode);
    //
    //                     if ($selector.attr('data-action') === 'arrival' && $('.has-foreign-arrival[data-foreign="' + num + '"]')) {
    //                         $('.has-foreign-arrival[data-foreign="' + num + '"]').val(suggestions.value);
    //                     }
    //                 }
    //             }
    //
    //             /*if ($selector.attr('data-action') === 'departure') {
    //              activateBackwardCityButton();
    //              }*/
    //         }
    //     });
    // }
    function autoComplete($selector, num) {
        $selector.autocomplete({
            serviceUrl: controllerUrl + 'get-city',
            minChars: 2,
            deferRequestBy: 350, // Задержка запроса (мсек), на случай, если мы не хотим слать миллион запросов, пока пользователь печатает.
            // lookupLimit: 10,
            type: 'GET',
            maxHeight: 'auto',
            paramName: 'city',
            dataType: 'json',
            transformResult: function (response) {
                return {
                    suggestions: $.map(response, function (value) {
                        //console.log(value);
                        if ($('.wrap-form-group-city-data').length === 1 && $selector.attr('id') === 'flight-departure-city') {
                            $('.wrap-form-group-city-data').css({'border-radius': '2em 2em 2em 0'});
                        }
                        if ($('.wrap-form-group-city-data').length === 2 && $selector.attr('id') === 'additional-departure_2') {
                            $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 0'});
                        }
                        /** @namespace value.citycode */
                        return {
                            'value': value['name_' + __CUR_LANG],
                            'cityCode': value.citycode,
                            'code': value.code,
                            'typeCode': value.type
                        };
                    })
                };
            },
            onSelect: function (suggestions) {
                // На input-е поиска есть data атрибут с значением (напр [departure и arrival])
                // У всех hidden input-ов которые к ним принадлежат, ID начинаются с префикса [departure и arrival]
                // напр (departure-city-code_1)

                if ($selector.hasClass('has-foreign-arrival')) {

                    if ($('.wrap-form-group-city-data').length === 2 && $selector.attr('id') === 'additional-departure_2') {
                        // $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 2em'});
                        $('.wrap-form-group-city-data').last().css({'border-radius': '0 2em 2em 2em'});
                    }
                    var $foreignSelector = $('#flight-arrival-city_' + $selector.attr('data-foreign'));
                    var number = $selector.attr('data-foreign');

                    $foreignSelector.val(suggestions.value);

                    /*console.log($selector);
                     console.log($foreignSelector.attr('data-action'));
                     console.log($('#' + $foreignSelector.attr('data-action') + '-city-code_' + number));
                     console.log(number);*/

                    $('#' + $foreignSelector.attr('data-action') + '-city-code_' + number).val(suggestions.cityCode);
                    $('#' + $foreignSelector.attr('data-action') + '-code_' + number).val(suggestions.code);
                    $('#' + $foreignSelector.attr('data-action') + '-type_' + number).val(suggestions.typeCode);


                } else {
                    if ($('.wrap-form-group-city-data').length === 1 && $selector.attr('id') === 'flight-departure-city') {
                        $('.wrap-form-group-city-data').css({'border-radius': '2em'});
                    }

                    if ($('#' + $selector.attr('data-action') + '-city-code_' + num)) {
                        $('#' + $selector.attr('data-action') + '-city-code_' + num).val(suggestions.cityCode);
                        $('#' + $selector.attr('data-action') + '-code_' + num).val(suggestions.code);
                        $('#' + $selector.attr('data-action') + '-type_' + num).val(suggestions.typeCode);

                        if ($selector.attr('data-action') === 'arrival' && $('.has-foreign-arrival[data-foreign="' + num + '"]')) {
                            $('.has-foreign-arrival[data-foreign="' + num + '"]').val(suggestions.value);
                        }
                    }
                }

                /*if ($selector.attr('data-action') === 'departure') {
                 activateBackwardCityButton();
                 }*/
            }
        });
    }


    /* ========================================================================
     * Flight function
     * ======================================================================== */


    /**
     * Отрисовывает результат поиска на страницу
     *
     * @param res string Response
     */
    function renderSearchResult(res) {
        /*var $allResultWrap, $allResult;
        $allResultWrap = $('<div>').attr('id', 'search-all-result-wrap');
        $allResult = $('#search-result');

        $allResult.append($allResultWrap.append(res));

        var $items = $('.search-result-item');


        var preKey = 0;
        var roundTrip = 1;
        var $backwardHeader = $('.backward-header');
        $items.toArray().forEach(function (item, index) {
            if (parseInt($(item).attr('data-backward-trip'), 10) === 1) {
                if (!$backwardHeader.is(':visible')) {
                    $backwardHeader.css({'display': 'flex'});
                }

                if (index !== 0) {
                    if (preKey === parseInt($(item).attr('data-key'), 10)) {
                        $(item).hide().attr('data-round-trip', roundTrip).attr('data-order', 2);
                        roundTrip++;
                    } else {
                        $(item).attr('data-round-trip', 0).attr('data-order', 1).find('.block-amount-data').prepend('<span>от </span>');
                        roundTrip = 1;
                        preKey++;
                    }
                } else {
                    $(item).attr('data-round-trip', 0).attr('data-order', 1).find('.block-amount-data').prepend('<span>от </span>');
                }
            } else {
                var $preItem = $('.item-search-' + preKey);
                if (index !== 0) {
                    if (preKey === parseInt($(item).attr('data-key'), 10)) {
                        $preItem.toArray().forEach(function (item, index) {
                            if (index !== 0) {
                                $(item).find('.search-result-header-amount').empty();
                            }
                        });

                        $($preItem.toArray()[0]).find('.amount-content').empty();
                        $($preItem.toArray()[0]).append($(item).find('.wrap-main-content').css({
                            'border-top': '1px solid #ddd'
                        }));
                        $(item).hide();
                    } else {
                        preKey++;
                    }
                }
            }

        });

        var $bestFlightEl = $('.best-flight');
        var $bestAmountFlight = bestFlight('amount');
        var $bestElapsedFlight = bestFlight('elapsed');
        var amountKey = parseInt($bestAmountFlight.attr('data-key'), 10);
        var elapsedKey = parseInt($bestElapsedFlight.attr('data-key'), 10);
        var $bestAmountBlock = $('.best-amount');
        var $bestElapsedBlock = $('.best-elapsed');

        if (amountKey === elapsedKey) {
            $bestAmountBlock.find('.title').text('Самый удобный и самый дешевый').append($bestAmountFlight);

            $bestAmountBlock.append($bestAmountFlight);

            $items.toArray().forEach(function (item, index) {
                if (parseInt($(item).attr('data-backward-trip'), 10) === 1 && parseInt($(item).attr('data-key'), 10)  === parseInt($bestAmountFlight.attr('data-key'), 10)) {

                    if (index !== 0) {
                        var $best = $(item).clone(true);
                        $best.removeAttr('class').addClass('content-best-flight').addClass('item-wrapper').attr('data-book-ready', 1);

                        $('.content-best-flight').eq(index - 1).find('.amount-content').empty();
                        $best.find('.item-header').remove();

                        $bestAmountBlock.append($($best).show());
                    }

                }
            });


            $bestElapsedBlock.hide();
        } else {
            $bestElapsedBlock.append($bestElapsedFlight);
            $bestAmountBlock.append($bestAmountFlight);
        }

        $bestFlightEl.show();



        var $wrap = $('<div/>', {id: 'all-search-item-content'});
        var $content = $('#search-result-content');




        // $wrap.append($(tmp));
        $wrap.append($items);
        $content.append($wrap);


        $allResult.css({'display': 'flex'});
        allFlight = $items;
        // allFlight = $(tmp);
        content = $('#all-search-item-content');*/

        $('#search-result').html(res);

        $('.tooltipt').tooltipster({
            theme: 'tooltipster-borderless',
            contentAsHTML: true,
            functionBefore: function(instance, helper) {
                let $content = $('#tooltip_template').clone(),
                    transfer = helper.origin.attributes[1].textContent,
                    city = helper.origin.attributes[2].textContent,
                    depTime = helper.origin.attributes[3].textContent,
                    depDate = helper.origin.attributes[4].textContent,
                    arrTime = helper.origin.attributes[5].textContent,
                    arrDate = helper.origin.attributes[6].textContent;
                $content.children('.transfer').text(transfer);
                $content.children('.city').text(city);
                $content.children('.dep-time').text(depTime);
                $content.children('.dep-date').text(depDate);
                $content.children('.arr-time').text(arrTime);
                $content.children('.arr-date').text(arrDate);
                instance.content($content.html());
            }
        });
    }



    function bestFlight(name) {
        var $items = $('.search-result-item');
        var dataAttr = 'data-' + name;
        var minItem = $($items.toArray()[0]);
        var min = parseInt($($items.toArray()[0]).attr(dataAttr), 10);
        $items.toArray().forEach(function (item) {
            if (parseInt($(item).attr(dataAttr), 10) < min) {
                min = parseInt($(item).attr(dataAttr), 10);
                minItem = item;
            }
        });

        var $best = $(minItem).clone(true);
        $best.removeAttr('class').addClass('content-best-flight').addClass('item-wrapper').attr('data-book-ready', 1);
        return $best;

    }


    function renderBookForm (res) {
        var $wrap, $bookForm;
        $('#search-result').hide();
        $wrap = $('<div>').attr('id', 'book-form-wrap');
        $bookForm = $('#book-form');
        $bookForm.append($wrap.append(res));
        $bookForm.fadeIn('slow');
    }


    /**
     * На самом деле это функцияне клонирует. Так как все дополнительные элементы
     * уже отрисованы, они просто скрыты. Фунция просто выводит их на экран.
     * Сделано это что бы сохранить начальные оброботчики, напр поле не заполнено
     *
     * @param nextNumber int Порядковый номер элемента
     * @param addType string Тип продолжения маршрута (обратно)
     */
    function cloneArrivalCity (nextNumber, addType) {
        var className = 'field-flight-arrival-city_' + nextNumber;
        var $clone = $('.field-flight-arrival-city_1').clone(false);
        $clone.removeClass('field-flight-arrival-city_1').addClass(className);
        if ($clone.hasClass('has-error')) {
            $clone.removeClass('has-error');
            $clone.find('.help-block').empty();
        }
        $clone.find('label').removeAttr('for').attr('for', 'flight-arrival-city_' + nextNumber);
        $clone.find('input').removeAttr('id').attr('id', 'flight-arrival-city_' + nextNumber).val('');
        $('.wrap-form-group-city-data').last().find('.search-flight-form-city-group')
            .append('<div class="qway-search-required-city"><input type="text" id="additional-departure_' + nextNumber + '" placeholder="Откуда" class="form-control has-foreign-arrival" data-foreign="' + (nextNumber - 1) + '"></div>');

        autoComplete($('#additional-departure_' + nextNumber), nextNumber);

        $('.wrap-form-group-city-data').last().find('.search-flight-form-city-group').append($clone);
        autoComplete($('#flight-arrival-city_' + nextNumber), nextNumber);
        $('#search-flight-form').yiiActiveForm('add', {
            // id: 'flight-arrival-city_' + nextNumber,
            name: 'arrivalCity[]',
            container: '.field-flight-arrival-city_' + nextNumber,
            input: '#flight-arrival-city_' + nextNumber,
            error: '.help-block',
            validate: function(attribute, value, messages/*, deferred, $form*/) {
                yii.validation.required(value, messages, {
                    "message": "Обязательное поле"
                });
            }
        });

        //showPlaneImg(nextNumber, addType);
    }


    /**
     * Скрывает последнии из добаленных маршрутов
     *
     * @param number int Номер позиции элемента
     * @param type bool Тип удаляемого маршрута (обратно)
     */
    function removeArrivalCity (number, type) {
        var className = 'field-flight-arrival-city_' + number;

        var $el = $('.' + className);

        $el.remove();
        removeArrivalHiddenInput('city-code', number);
        removeArrivalHiddenInput('code', number);
        removeArrivalHiddenInput('type', number);
        removeDate(number);
        $('.choose-img').show();

        // if ($el.find('input').val()) {
        //     // $el.find('input').val('');
        // } else {
        //     $el.remove();
        //     removeArrivalHiddenInput('city-code', number);
        //     removeArrivalHiddenInput('code', number);
        //     removeArrivalHiddenInput('type', number);
        //     removeDate(number);
        //     $('.choose-img').show();
        // }

    }


    function showPlaneImg (num, type) {
        var $el = '';
        if (type === 'backward') {
            $el = $('.search-img-control[data-num="6"]');
        } else {
            $el = $('.search-img-control[data-num="' + (num + 1) + '"]');
        }
        $el.show();
    }


    function hidePlaneImg (num) {
        var $el = $('.search-img-control[data-num="' + (num + 1) + '"]');
        $el.hide();
    }


    /**
     * Клонирует скрытый элементы элементы [city-code, code, type]
     *
     * @param item string название input-a  [city-code, code, type]
     * @param nextNumber int Порядковый номер элемента
     */
    function cloneArrivalHiddenInput (item, nextNumber) {
        var className = 'arrival-' + item + '_';
        var $arrivalHiddenClone = $('.reference-' + item).clone();

        $arrivalHiddenClone
            .removeClass('reference-' + item)
            .removeClass('field-' + className + '1')
            .addClass('field-' + className + nextNumber)
        ;

        $arrivalHiddenClone
            .find('input').val('')
            .attr('id', '')
            .attr('id', className + nextNumber)
        ;

        $('#wrap-form-group-hidden-input').append($arrivalHiddenClone);
    }


    /**
     * Удаляет склонированый элемен [city-code, code, type]
     *
     * @param item string название input-a  [city-code, code, type]
     * @param number int Номер позиции элемента
     */
    function removeArrivalHiddenInput (item, number) {
        var className = 'arrival-' + item + '_';
        $('.field-' + className + number).remove();
    }


    /*function onSelectHandler(date, context) {
        /!**
         * @date is an array which be included dates(clicked date at first index)
         * @context is an object which stored calendar interal data.
         * @context.calendar is a root element reference.
         * @context.calendar is a calendar element reference.
         * @context.storage.activeDates is all toggled data, If you use toggle type calendar.
         * @context.storage.events is all events associated to this date
         *!/
        var $element = context.element;
        var $calendar = context.calendar;

        if ($($element[0]).closest('.required').hasClass('has-error')) {
            $($element[0]).closest('.required').removeClass('has-error');
            $($element[0]).siblings('.help-block').empty();
        }

        if ($element.attr('id') === 'searchform-tripdate_1' && $('#searchformamadeus-tripdatebackward').is(':visible')) {
            $('#searchformamadeus-tripdatebackward').prop({'readonly': false});
            var minDateArray = $element.val().split('-');
            $('#date-separator').show();
            $('#searchformamadeus-tripdatebackward').pignoseCalendar({
                format: 'DD-MM-YYYY', // date format string. (2017-02-02)
                lang: 'ru',
                // apply: onSelectHandler,
                theme: 'blue',
                week: 1,
                buttons: true,
                minDate: new Date(minDateArray[2] + '-' + minDateArray[1] + '-' + (minDateArray[0] - 1)),
            });

        }

        if ($element.hasClass('trip-date-input')) {

            var currentNum = parseInt($element.attr('id').split('_')[1], 10);
            var $preElement = $('#searchform-tripdate_' + (currentNum + 1));

            if ($preElement.length > 0) {
                $preElement.removeClass('calendar').prop({'readonly': false});

                $preElement.pignoseCalendar({
                    format: 'DD-MM-YYYY', // date format string. (2017-02-02)
                    lang: 'ru',
                    apply: onSelectHandler,
                    theme: 'blue',
                    week: 1,
                    buttons: true,
                    // toggle: true
                    // reverse: true,
                    minDate: new Date(date[0].format('YYYY-MM-DD')),
                });
            }


        }

    }*/


    /**
     * На самом деле это функцияне клонирует. Так как все дополнительные элементы
     * уже отрисованы, они просто скрыты. Фунция просто выводит их на экран
     * Сделано это что бы сохранить оброботчики, напр поле не заполнено
     * и также чтобы при клики календарь появился у того поля по которому кликнули
     *
     * @param nextNumber int
     */
    function cloneDate (nextNumber) {
        var $clone = $('#trip-date-required_' + (nextNumber - 1)).clone(false);

        $clone.removeAttr('id').removeClass('field-searchform-tripdate_' + (nextNumber - 1)).addClass('only-row-trip-date field-searchform-tripdate_' + nextNumber).attr('id', 'trip-date-required_' + nextNumber);
        $clone.find('input').removeClass('hasDatepicker').removeAttr('id').attr('id', 'searchform-tripdate_' + nextNumber).val('').prop({'readonly': true});

        /*$clone.pignoseCalendar({
         format: 'YYYY-MM-DD', // date format string. (2017-02-02)
         lang: 'ru',
         apply: onSelectHandler,
         theme: 'blue',
         week: 1,
         buttons: true,
         // toggle: true
         // reverse: true,
         minDate: new Date(),
         });*/


        if ($clone.hasClass('has-error')) {
            $clone.removeClass('has-error');
            $clone.find('.help-block').empty();
        }


        // $('.search-flight-form-date-group').append($clone);

        $('.wrap-form-group-city-data').last().find('.search-flight-form-date-group').empty().append($clone);


        /*$('#searchform-tripdate_' + nextNumber).pignoseCalendar({
         format: 'DD-MM-YYYY', // date format string. (2017-02-02)
         lang: 'ru',
         apply: onSelectHandler,
         theme: 'blue',
         week: 1,
         buttons: true,
         // toggle: true
         // reverse: true,
         minDate: $('#searchform-tripdate_' + (nextNumber - 1)).val(),
         });*/


        $('#search-flight-form').yiiActiveForm('add', {
            id: 'trip-date-required_' + nextNumber,
            name: 'tripDate[]',
            container: '.field-searchform-tripdate_' + nextNumber,
            input: '#searchform-tripdate_' + nextNumber,
            error: '.help-block',
            validate: function (attribute, value, messages, deferred, $form) {
                yii.validation.required(value, messages, {
                    "message": "Обязательное поле"
                });
            }
        });
    }


    /**
     * Скрывает последни input элемент даты
     *
     * @param number int
     */
    function removeDate (number) {
        var IDName = 'trip-date-required_' + number;

        $('#' + IDName).remove();
    }


    /**
     * При закрыти перенаправляет на url которую передали, если не передавать url
     * то перенаправит на главную страницу
     */
    function closeModalWindow (url) {
        var urlTo = url || mainPageUrl;
        $(location).attr('href', urlTo);
    }


    function hideAddArrivalButton () {
        $('#wrap-form-group-additional-arrival').css('visibility', 'hidden');
    }


    /*function showRemoveArrivalButton () {
     $('#remove-arrival-button').show();
     }*/


    function activeReverseCity (action) {
        var dependentFieldVal = '';
        if (action === 'departure') {
            dependentFieldVal = $('#flight-arrival-city_1').val();
        } else {
            dependentFieldVal = $('#flight-departure-city').val();
        }

        if (dependentFieldVal !== '') {
            $('.reverse-city-flight').addClass('active');
        }
    }


    function activateBackwardCityButton () {
        $('#btn-backward-city').prop('disabled', false);
    }


    function exampleCityInit (cityData, action, number) {
        if (action === 'departure') {
            $('#departure-city-code_1').val(cityData.code);
            $('#departure-code_1').val(cityData.code);
            $('#departure-type_1').val(cityData.type);
        } else {
            $('#arrival-city-code_' + number).val(cityData.code);
            $('#arrival-code_' + number).val(cityData.code);
            $('#arrival-type_' + number).val(cityData.type);
        }

    }


    function filterSearchResult (filter, buttonEl) {
        $('.item-filter').find('i').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
        var sortFlight = '';
        var $buttonEl = $(buttonEl);

        var $selectItem = $('#search-flight-filter-airline-select').find('option:selected');

        var isAirlineSelected = $selectItem.val();
        var isAirlineRound = $selectItem.val();

        if (isAirlineSelected || isAirlineRound) {
            sortFlight = $('.search-result-item');
        } else {
            sortFlight = allFlight;
        }

        if ($buttonEl.attr('data-next-sort') === 'min') {
            sortFlight.sort(function (a, b) {
                var first = parseInt($(a).attr('data-' + filter), 10);
                var second = parseInt($(b).attr('data-' + filter), 10);
                if (first > second) {
                    return 1;
                } else if (first < second) {
                    return -1;
                } else {
                    return 0;
                }
            });
            $buttonEl.attr('data-next-sort', 'max').find('i').removeClass('fa-arrow-up').addClass('fa-arrow-down');

        } else {
            sortFlight.sort(function (a, b) {
                var first = parseInt($(a).attr('data-' + filter), 10);
                var second = parseInt($(b).attr('data-' + filter), 10);
                if (first > second) {
                    return -1;
                } else if (first < second) {
                    return 1;
                } else {
                    return 0;
                }
            });
            $buttonEl.attr('data-next-sort', 'min').find('i').removeClass('fa-arrow-down').addClass('fa-arrow-up');
        }

        $buttonEl.attr('data-state', 'active');

        $.when(content.fadeOut('slow')).done(function() {
            content.empty();
            content.append($(sortFlight)).fadeIn('slow');
        });

    }


    function filterAirlineSelect (el) {
        var company = $(el).val();
        var sortFlight = '';
        var multiFilter = [];

        var $selectItem = $('#search-flight-filter-airline-round');
        var $selectedItem = $selectItem.find('option:selected');

        if ($selectedItem.val()) {
            multiFilter = allFlight.toArray().filter(function(item) {
                return ($(item).attr('data-multi') === $selectedItem.val());
            });
            sortFlight = $(multiFilter);
        } else {
            sortFlight = allFlight;
        }

        if (company) {
            var filterResult = sortFlight.toArray().filter(function(item) {
                return ($(item).attr('data-airline') === company);
            });

            var i_round_z = 0;
            var i_round_o = 0;
            var i_returnable_z = 0;
            var i_returnable_o = 0;
            filterResult.forEach(function (item) {
                if (parseInt($(item).attr('data-multi'), 10) === 0) {
                    i_round_z++;
                }
                if (parseInt($(item).attr('data-multi'), 10) === 1) {
                    i_round_o++;
                }
                if (parseInt($(item).attr('data-returnable'), 10) === 0) {
                    i_returnable_z++;
                }
                if (parseInt($(item).attr('data-returnable'), 10) === 1) {
                    i_returnable_o++;
                }

            });

            if (i_round_z > 0 || i_round_o > 0) {
                $selectItem.prop('disabled', false);
            }
            if (i_round_z === 0) {
                $selectItem.prop('disabled', true);
            }

            if (i_returnable_z > 0 || i_returnable_o > 0) {
                $('#search-flight-filter-returnable').prop('disabled', false);
            }
            if (i_returnable_o === 0) {
                $('#search-flight-filter-returnable').prop('disabled', true);
            }



            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(filterResult)).fadeIn('slow');
            });

            $('.item-filter').find('i').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
            airlineSortFlight = filterResult;
        } else {
            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(sortFlight)).fadeIn('slow');
                $('#search-flight-filter-returnable').prop('disabled', false);
                $('#search-flight-filter-airline-round').prop('disabled', false);
            });
        }


    }


    function filterAirlineRound (el) {
        var round = $(el).val();
        var sortFlight = '';
        var multiFilter = [];

        var $selectItem = $('#search-flight-filter-airline-select');
        var $selectedItem = $selectItem.find('option:selected');

        if ($selectedItem.val()) {
            // sortFlight = $(airlineSortFlight);

            multiFilter = allFlight.toArray().filter(function(item) {
                return ($(item).attr('data-airline') === $selectedItem.val());
            });
            sortFlight = $(multiFilter);
        } else {
            sortFlight = allFlight;
        }


        if ($.isNumeric(round)) {
            var changeAirline = [];
            var filterResult = sortFlight.toArray().filter(function (item) {

                if ($(item).attr('data-multi') === round) {
                    changeAirline.push($(item).attr('data-airline'));
                    return $(item).attr('data-multi') === round;
                } else {
                    return false;
                }
            });

            roundSortFlight = filterResult;
            changeAirline = $.unique(changeAirline);

            $selectItem.find('option').toArray().forEach(function (itemOption, index) {
                if (index !== 0) {
                    $(itemOption).hide();
                    if (changeAirline.indexOf($(itemOption).val()) !== -1) {
                        $(itemOption).show();
                    }
                }

            });

            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(filterResult)).fadeIn('slow');
            });
        } else {
            $selectItem.find('option').show();

            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(sortFlight)).fadeIn('slow');
            });
        }
        $('.item-filter').find('i').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
    }


    function filterAirlineReturnable (el) {
        var value = $(el).val();
        var sortFlight = '';

        if (airlineSortFlight) {
            sortFlight = $(airlineSortFlight)
        } else {
            sortFlight = allFlight;
        }


        if ($.isNumeric(value)) {
            var filterResult = sortFlight.toArray().filter(function(item) {
                return ($(item).attr('data-returnable') === value);
            });

            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(filterResult)).fadeIn('slow');
            });
        } else {
            $.when(content.fadeOut('slow')).done(function() {
                content.empty();
                content.append($(sortFlight)).fadeIn('slow');
            });
        }
        $('.item-filter').find('i').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
    }


    function backwardCityData (num) {
        var className = 'field-flight-arrival-city_' + num;
        var code = $('#departure-code_1').val();
        var $el = $('.' + className);

        $el.find('input').val($('#flight-departure-city').val());
        $el.find('.clear-input').attr('data-type', 'backward');
        $('#arrival-city-code_' + num).val(code);
        $('#arrival-code_' + num).val(code);
        $('#arrival-type_' + num).val($('#departure-type_1').val());
    }


    function errorBorder ($el) {
        if ($el.attr('type') === 'radio' || $el.attr('type') === 'checkbox') {
            $el.addClass('custom-has-error').closest('div').css({
                'border': '1px solid red',
                'border-radius': '4px'
            });
        } else {
            $el.css({'border': '1px solid red'}).addClass('custom-has-error');
        }
        $('.custom-help-block').text(bookFormErrorMessage).show();
    }


    function hideReverseCity() {
        $('.reverse-city-flight').css('visibility', 'hidden');
    }


    /* ========================================================================
     * Event handler
     * ======================================================================== */



    var $loaderSearch = $('#loader-flight-search');

    /**
     * Отправляет данные поиска билетов
     */
    $body.on('beforeSubmit', '#search-flight-form2', function () {
        var $form = $(this);
        var $searchResultEl = $('#search-result');

        // $searchResultEl.empty();
        $('#book-form').empty();




        // Если обязательные поля не заполнены, не отправяем форму
        /*var dateError = 0;
         $('.trip-date-input').toArray().forEach(function (item) {
         if ($(item).val() === '') {
         $(item).siblings('.custom-help-block-date').show();
         dateError++;
         }
         });*/




        if ($form.find('.has-error').length /*&& !dateError*/) {
            console.log('error');
            return false;
        } else {

            var errorCorrectData = 0;
            $('.code-hidden').toArray().forEach(function (item) {
                if ($(item).val() === '') {
                    var num = $(item).attr('id').split('_')[1];
                    var $el = '';
                    if ($(item).hasClass('departure')) {
                        $el = $('.field-flight-departure-city');
                    } else {
                        $el = $('.field-flight-arrival-city_' + num);
                    }
                    $el.find('.help-block').text('Не корректные данные').css({'color': '#a94442'});
                    errorCorrectData++;
                }
            });

            if (errorCorrectData) {
                console.log('errorCorrectData');
                return false;
            }

            var errorFieldsSame = 0;
            var allInput = $('.flight-city-input').toArray();

            // return false;

            allInput.forEach(function (item, index) {
                if (index !== 0) {
                    if ($(item).val() === $(allInput[index-1]).val()) {
                        $(item).closest('div').find('.help-block').text('Одинаковые данные').css({'color': '#a94442'});
                        $(allInput[index-1]).closest('div').find('.help-block').text('Одинаковые данные').css({'color': '#a94442'});
                        errorFieldsSame++;
                    }
                }
            });

            if (errorFieldsSame) {
                console.log('errorFieldsSame');
                return false;
            }

            $loaderSearch.fadeIn('slow');
            $body.animate({
                scrollTop: $('#loader-flight-search').offset().top
            }, 2000);

            $.ajax({
                url: controllerUrl + 'search-tickets',
                method: 'post',
                data: $form.serialize(),
                beforeSend: function () {
                    $('button#flight-submit').prop('disabled', true);
                    $('#search-result').html('');
                },
                success: function (response) {
                    $('button#flight-submit').prop('disabled', false);
                   /* var result = JSON.parse(response);
                    renderItems(result.flights, result.cabin);*/
                    // console.log(JSON.parse(response));
                    $('#search-all-result-wrap').remove(); // Удаляю данные предыдущего поиска
                    renderSearchResult(response);
                },
                error: function (err) {

                }

            }).always(function () {
                // Попадает сюда, по оканчании ассинхрого запроса
                $body.animate({
                    scrollTop: $searchResultEl.offset().top - 200
                }, 2000);
                $loaderSearch.fadeOut('slow');
                $body.addClass('mini');
            });
        }
        return false;
    });


    /**
     * Сработает при клике на кнопке купит, в результатах поиска
     */
    $body.delegate('#add-ticket', 'click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $loader = $('#overlay-loader');

        var parentWrap = $this.closest('.item-wrapper');


        if (parseInt($(parentWrap).attr('data-book-ready'), 10) === 1) {
            $loader.fadeIn('slow');
            $.ajax({
                url: controllerUrl + 'book' + '?key=' + $this.attr('data-key'),
                method: 'get',
                success: function (response) {
                    renderBookForm(response);
                },
                error: function (err) {
                    console.log(err);
                }
            }).always(function () {
                $loader.fadeOut('slow');
            });
        } else {
            if (parseInt($(parentWrap).attr('data-backward-trip'), 10) === 1) {
                var cloneThisWrap = parentWrap.clone();
                // cloneThisWrap.find('#add-ticket').fadeOut();
                var $backwardHeader = $('.backward-header');
                $backwardHeader.empty().text('Выберите обратный рейс');

                // console.log($allHideEl);

                if (parseInt(cloneThisWrap.attr('data-last'), 10) === 1) {
                    $('.combination-block').append(cloneThisWrap.removeClass('search-result-item').attr('data-book-ready', 1)).show();
                    $('.search-result-item').hide();
                    replaceButton(cloneThisWrap, true);
                    $backwardHeader.hide();
                } else {
                    $('.combination-block').append(cloneThisWrap.removeClass('search-result-item')).show();
                    replaceButton(cloneThisWrap, false);
                    $('.search-result-item').toArray().forEach(function (item) {
                        if ($(item).is(':hidden')) {
                            if ($(item).attr('data-trip-id') === parentWrap.attr('data-trip-id')) {
                                $(item).fadeIn().attr('data-last', 1);
                            }
                        } else {
                            $(item).fadeOut();
                        }
                    });
                }
            }
        }
    });


    function replaceButton (parentEl, isLast) {

        if (isLast) {
            $(parentEl)
                .find('.amount')
                .prepend('<div class="clear-item-content"><i class="clear-item-backward last glyphicon glyphicon-remove"></i></div>');
        } else {
            $(parentEl)
                .find('.amount')
                .append('<div class="clear-item-content"><i class="clear-item-backward glyphicon glyphicon-remove"></i></div>');
            $(parentEl)
                .find('.amount').parent().children('#add-ticket').remove();
            $('html,body').animate({scrollTop: $('#search-flight-filter').offset().top}, 700);
        }

    }


    $body.delegate('.clear-item-backward', 'click', function () {
        var $this = $(this);

        if ($this.hasClass('last')) {
            $('.search-result-item').toArray().forEach(function (item) {
                if ($(item).is(':hidden') && parseInt($(item).attr('data-order'), 10) === 2) {
                    $(item).show();
                }
            });
            $this.closest('.item-wrapper').remove();
            $('.backward-header').text('Выберите обратный рейс').show();
        } else {
            $('.search-result-item').toArray().forEach(function (item) {
                if ($(item).is(':hidden')) {
                    $(item).show();
                }
                if (parseInt($(item).attr('data-order'), 10) === 2) {
                    $(item).hide();
                }
            });
            $this.closest('.combination-block').empty().hide();
            $('.backward-header').text('Выберите рейс вылета').show();
        }
    });

    $body.delegate('#get-flight-rules','click',function(e){
        var $recommendationID = $(this).attr('data-sequenceid'),
            $combinationID = $(this).attr('data-combinationid');

        $flightRuleLoader = $('#overlay-loader');
        $flightRuleLoader.fadeIn('slow');

        $.ajax({
            url: controllerUrl + 'get-flight-rules',
            method: 'post',
            data: { recommendationid:$recommendationID, combinationid:$combinationID },
            success: function (response) {
                $("#flight-rules-container").html(response);
                $("#book-flight-rules-modal").show();
                $flightRuleLoader.fadeOut('slow');
            }
        });
    });

    $body.on('click', '.close-book-modal', function () {
        $('#mask').fadeOut();
        location.reload();
    });

    /**
     * Запрос на бронирование
     */
    $body.delegate('#book-submit', 'click', function (e) {
        var $contactEmail, $contactPhone, $bookLoader, $confirm;
        e.preventDefault();


        emptyInputs = [];
        notCorrectData = [];
        notCorrectBirthday = [];
        notCorrectDocExpire = [];
        $('.modal-flight-to-payment').hide();

        $contactEmail = $('#contact-email');
        $contactPhone = $('#contact-phone');
        $confirm = $('#confirm-return-ticket');

        var $passengerForms = $('.passenger-data-forms');
        var $passengerForm = $('.passenger-data-form');

        /* start */

        $passengerForms.find('input').not('[type="hidden"]').toArray().forEach(function (item) {
            // console.log(item);
            if ($(item).attr('type') === 'radio') {
                var error = 0;
                var $wrapRadioButtons = $(item).closest('div');
                $wrapRadioButtons.find('input').toArray().forEach(function (radio) {
                    if ($(radio).prop('checked') !== true) {
                        error++;
                    }
                });
                // console.log(error);
                if (error > 1) {
                    emptyInputs.push($(item));
                }
            } else {
                if ($(item).val() === '') {
                    emptyInputs.push($(item));
                }
            }
        });

        if ($contactEmail.val() === '') {
            emptyInputs.push($contactEmail);
        }
        if ($contactPhone.val() === '') {
            emptyInputs.push($contactPhone);
        }
        if ($confirm.prop('checked') !== true) {
            $('.policy').addClass('error');
            $(this).prop('disabled', true);
            emptyInputs.push($confirm);
        }

        var allDocIdInput = $('.passenger-doc-id').toArray();
        allDocIdInput.forEach(function (item, index) {
            if (index !== 0) {
                if ($(item).val()) {
                    if ($(item).val() === $(allDocIdInput[index-1]).val()) {
                        notCorrectData.push($(item));
                        notCorrectData.push($(allDocIdInput[index-1]));
                    }
                }
            }
        });

        /* var allFormDate = $('.qway-passenger-form-date-input').toArray();
         allFormDate.forEach(function (item) {
         if (parseDate($(item).val()) === null) {
         notCorrectData.push($(item));
         }
         });*/

        var allBirthdayInput = $('.qway-passenger-form-birthday-input').toArray();
        allBirthdayInput.forEach(function (item) {
            if ($(item).val()) {
                if (validateBirthday($(item).val(), $(item).attr('data-passenger-code')) === false) {
                    notCorrectBirthday.push($(item));
                }
            }

        });

        var allDocExpireInput = $('.qway-passenger-form-doc-expire-input').toArray();
        allDocExpireInput.forEach(function (item) {
            if ($(item).val()) {
                if (validateDocExpire($(item).val()) === false) {
                    notCorrectData.push($(item));
                }
            }

        });

        var $customHelpBlock = $('.custom-help-block').empty();

        if (emptyInputs.length > 0 || notCorrectData.length > 0 || notCorrectBirthday.length > 0 || notCorrectDocExpire.length > 0) {
            // есть не заполненые поля
            emptyInputs.forEach(function (item) {
                errorBorder(item);
            });


            notCorrectData.forEach(function (item) {

                item.css({'border': '1px solid red'}).addClass('custom-has-error');

                var msg = 'Некоректные данные';
                if ($customHelpBlock.text() === '') {
                    $customHelpBlock.append('<div class="not-correct-help-block">' + msg + '</div>');
                } else {
                    $customHelpBlock.find('.not-correct-help-block').empty();
                    $customHelpBlock.append('<div class="not-correct-help-block">' + msg + '</div>');
                }
            });

            notCorrectBirthday.forEach(function (item) {
                item.css({'border': '1px solid red'}).addClass('custom-has-error');
                var msg = 'Есть некоректно заполненные дни рождения';
                if ($customHelpBlock.text() === '') {
                    $customHelpBlock.append('<div class="not-correct-birthday-help-block">' + msg + '</div>');
                } else {
                    $customHelpBlock.find('.not-correct-birthday-help-block').empty();
                    $customHelpBlock.append('<div class="not-correct-birthday-help-block">' + msg + '</div>');
                }
            });

            $customHelpBlock.show();
            return false;
        } else {
            $bookLoader = $('#overlay-loader');
            $('.persons-email').val($contactEmail.val());
            $('.persons-phonenumber').val($contactPhone.val());
            $bookLoader.fadeIn('slow');
            $.ajax({
                url: controllerUrl + 'book',
                method: 'post',
                data: $passengerForm.serialize(),
                success: function (response) {
                    $('#book-result-modal-content').html(response);
                    $('#mask').show();
                    // var $modal, $bookIdContentModal, flight = null, res;
                    //
                    // res = JSON.parse(response);
                    //
                    // /** @namespace res.isOk */
                    // if (res.isOk) {
                    //     $bookIdContentModal = $('#book-id-text');
                    //     $modal = $('#book-result-modal');
                    //     flight = res.response;
                    //     /** @namespace flight.pnrNO */
                    //     $('#payment-order-number').val(flight.pnrNO); // Это hidden input, платежки. Фома тоже в модальном окне
                    //     $bookIdContentModal.text(flight.pnrNO);
                    //     $('.modal-flight-to-payment').show();
                    // } else {
                    //     $bookIdContentModal = $( '#book-id-error-text');
                    //     $modal = $('#book-error-modal');
                    //     // TODO нужно выводить нормальное сообщение,
                    //     // это просто тестовое сообщение
                    //     $('.result-item').hide();
                    //     // $bookIdContentModal.text('Возникли проблемы попробуйте еще раз произвести поиск');
                    //     $bookIdContentModal.text(res.response);
                    //     $('#payment-form').hide();
                    // }
                    //
                    // $modal.modal('show');
                },
                error: function (err) {
                    console.log(err);
                }
            }).always(function () {
                $bookLoader.fadeOut('slow');
            });
        }

        /* end */

    });


    function validateBirthday (date, code) {

        var codeMinAge = 0;
        var codeMaxAge = 150;
        switch (code) {
            case 'INF':
                codeMaxAge = 2;
                break;
            case 'CHD':
                codeMinAge = 2;
                codeMaxAge = 12;
                break;
            case 'ADT':
                codeMinAge = 12;
                break;
        }

        var currentDate = new Date();

        var dateArray = date.split('-');
        // var newDate = new Date(date[1] + ',' + date[0] + ',' + date[2]);
        var newMinDate = new Date(dateArray[1] + ',' + dateArray[0] + ',' + (parseInt(dateArray[2], 10) + codeMinAge)).getTime();
        var newMaxDate = new Date(dateArray[1] + ',' + dateArray[0] + ',' + (parseInt(dateArray[2], 10) + codeMaxAge)).getTime();

        var daysIn= daysInMonth(dateArray[2], dateArray[1]);

        if (parseInt(dateArray[1]) > 12 || parseInt(dateArray[0]) > daysIn || parseInt(dateArray[2], 10) > parseInt(currentDate.getFullYear(), 10)) {
            return false;
        } else {
            return !(newMinDate > currentDate.getTime() && newMaxDate > currentDate.getTime());
        }

    }


    function validateDocExpire (date) {
        var dateArray = date.split('-');
        var newDate = new Date(dateArray[1] + ',' + dateArray[0] + ',' + dateArray[2]);
        var currentDate = new Date();
        var daysIn= daysInMonth(dateArray[2], dateArray[1]);
        return newDate.getTime() > currentDate.getTime() && parseInt(dateArray[0]) <= daysIn;
    }


    function daysInMonth (y, m) {
        return 33 - new Date(y, (m - 1), 33).getDate();
    }


    /*function parseDate(str) {
     var m = str.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/);
     return (m) ? new Date(m[3], m[2]-1, m[1]) : null;
     }*/

    /**
     * Проверка не кирилицу при вводе имении и фамилии
     */
    $body.delegate('.latin-charset-data', 'keyup', function () {
        var latinPreg = /^[a-z]+$/i;
        if (!latinPreg.test($(this).val())) {
            $(this).val('');
            $(this).prop('placeholder', 'На латинице');
        }
    });


    /**
     * Галочка о согласии при брони
     */
    $body.delegate('#confirm-return-ticket', 'change', function () {
        if ($(this).is(':checked')) {
            $('#book-submit').prop('disabled', false);
            $('.policy').removeClass('error');
            $('.custom-help-block').empty();
        } else {
            $('#book-submit').prop('disabled', true);
        }
    });


    /**
     * Добавление, продолжения маршрута
     */
    $body.delegate('.make-route', 'click', function (e) {
        e.preventDefault();

        var buttonType = $(this).attr('data-add-arrival-type');
        var countArrival = $('.qway-search-arrival-city').not('.additional-arrival-city').length;
        var nextNumber = countArrival + 1;
        var $lastElement = $('.wrap-form-group-city-data').last();

        if (nextNumber == 2) {
            var maxCountInput = 3;



            if (maxCountInput !== nextNumber) {

                $('.wrap-form-group-city-data').first().find('.field-searchformamadeus-tripdatebackward').hide();
                $('#trip-date-required_1').width('100%');

                $lastElement.css({
                    'border-radius' : '2em 2em 2em 0'
                });

                $lastElement.find('.search-flight-form-date-group').addClass('date-list');

                var $clone = $lastElement.clone(false);

                $('#flight-departure-city').css({'border-radius': '1.5em 0 0 0'});
                $('#additional-departure_2').css({'border-radius': '0 0 0 1.5em'});

                $clone.hide().css({
                    'border-radius' : ' 0 2em 2em 2em'
                }).find('.search-flight-form-passenger-group').empty();
                $lastElement.after($clone.find('.search-flight-form-city-group').empty().end()/*.show('slow')*/);

                $(this).text('Вернуться к стандартному маршруту');

                // cloneImg(nextNumber, buttonType);
                cloneArrivalCity(nextNumber, buttonType);
                cloneArrivalHiddenInput('city-code', nextNumber);
                cloneArrivalHiddenInput('code', nextNumber);
                cloneArrivalHiddenInput('type', nextNumber);
                cloneDate(nextNumber);

                $clone.show('slow');
                
                if ($lastElement.find('.trip-date-input').val()) {
                    $clone.find('.trip-date-input').prop({'readonly': false});
                    var minDateArray = $lastElement.find('.trip-date-input').val().split('-');

                    $clone.find('.trip-date-input').datepicker({
                        dateFormat: 'dd-mm-yy',
                        minDate: new Date(minDateArray[2] + '-' + minDateArray[1] + '-' + minDateArray[0]),
                        duration: 'slow',
                        firstDay: 1,
                        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        /*beforeShow : function(input, inst) {
                         var today = $('.ui-datepicker-today');
                         if (today.length) {
                         $('.message-before').append(today.find('a').html()+'<br>');
                         } else {
                         $('.message-before').append('Today DOM element is not rendered yet.<br>');
                         }
                         },*/
                        afterShow : function(inst) {
                            /*var today = $('.ui-datepicker-today');
                             if (today.length) {
                             $('.message-after').append(today.find('a').html()+'<br>');
                             } else {
                             $('.message-after').append('Today DOM element should be already rendered.<br>');
                             }*/
                            $(".ui-datepicker-prev").empty().append('<i class="fa fa-chevron-left"></i>');
                            $(".ui-datepicker-next").empty().append('<i class="fa fa-chevron-right"></i>');
                        },
                        /*onSelect: function(dateText, inst) {
                         if (dateText && $('#searchformamadeus-tripdatebackward').is(':visible')) {
                         $('#searchformamadeus-tripdatebackward').prop({'readonly': false});
                         }
                         // console.log($("input[name='something']").val(dateText));
                         }*/
                    });

                }


            }

            if (5 === nextNumber) {
                hideAddArrivalButton();
                $('.choose-img').hide();
            }

            if (buttonType === 'backward') {
                $('.choose-img').hide();
                hideAddArrivalButton();
                backwardCityData(nextNumber);
                $('#is-backward-search').val(1);
            }
            hideReverseCity();
        } else {
            $lastElement.hide('slow');

            // setTimeout(function () {
            //     var number = $lastElement.index() - 1;
            //     console.log(number);
            //     removeArrivalHiddenInput('city-code', number);
            //     removeArrivalHiddenInput('code', number);
            //     removeArrivalHiddenInput('type', number);
            //
            //     $('.wrap-form-group-city-data').first().css({'border-radius': '2em'});
            //     $('#trip-date-required_1').width('50%');
            //     $('.wrap-form-group-city-data').first().find('.field-searchformamadeus-tripdatebackward').show();
            //
            //     $lastElement.remove();
            // }, 500);


            $lastElement.hide('slow').queue(function () {

                var number = $('.wrap-form-group-city-data').index() - 1;

                removeArrivalHiddenInput('city-code', number);
                removeArrivalHiddenInput('code', number);
                removeArrivalHiddenInput('type', number);

                $('.wrap-form-group-city-data').first().css({'border-radius': '2em'});
                $('#trip-date-required_1').width('50%');
                $('.wrap-form-group-city-data').first().find('.field-searchformamadeus-tripdatebackward').show();


                /*$('#trip-date-required_1').width('50%').queue(function () {
                    $('.wrap-form-group-city-data').first().find('.field-searchformamadeus-tripdatebackward').show();
                    //next();
                });*/

                $(this).remove();

            });
        }

    });


    /**
     * Удаление последнего маршрута
     */
    $body.delegate('.remove-arrival-button', 'click', function (e) {
        e.preventDefault();


        var lastInputNumber = ($('.qway-search-arrival-city').not('.additional-arrival-city').length);

        if (lastInputNumber > 1) {
            if ($(this).attr('data-type') === 'backward') {
                removeArrivalCity(lastInputNumber, true);
            } else {
                removeArrivalCity(lastInputNumber);
            }
        }

        if (lastInputNumber === 2) {
            $('.reverse-city-flight').fadeIn('slow');
        }



        $('#wrap-form-group-additional-arrival').css('visibility', 'visible');
    });


    /**
     * Очищает поля ввода поиска
     */
    $body.delegate('.clear-input', 'click', function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.required');
        if ($parent.hasClass('field-flight-departure-city')) {
            $('#btn-backward-city').prop('disabled', true);
            $('#wrap-form-group-additional-arrival').css('visibility', 'visible');
            removeArrivalCity(2, true);
        }
        $parent.find('input').val('');
    });


    $body.delegate('#backward-button', 'click', function () {
        /*e.preventDefault();

         var maxCountInput = 6;

         var countArrival = $('.qway-search-arrival-city').not('.additional-arrival-city').length;
         var nextNumber = countArrival + 1;*/
    });


    /**
     * При клике закрыть модальное окно перенаправляем пользователя
     */
    $body.delegate('.book-modal .close', 'click', function () {
        closeModalWindow();
    });


    /**
     * Контролирует кол-во пассажиров
     */
    $body.delegate('.passenger-count', 'click', function () {
        var ADT = 'adt', CHD = 'chd', INF = 'inf';
        var $this = $(this);
        var $parentEl = $this.closest('.qway-search-required-people');

        var $chdInput = $('#searchformamadeus-children');
        var $adtInput = $('#searchformamadeus-adults');
        var $infInput = $('#searchformamadeus-infants');

        var currentPassengerCode = $parentEl.attr('data-code');
        var maxQuantity = 9;
        var maxQuantityIn = 0;
        var currentVal = $parentEl.find('input').val();

        switch (currentPassengerCode) {
            case ADT: maxQuantityIn = (maxQuantity - $chdInput.val());
                break;
            case CHD: maxQuantityIn = (maxQuantity - $adtInput.val());
                break;
            case INF: maxQuantityIn = $adtInput.val();
                break;
        }

        if (currentPassengerCode === INF) {
            if ($this.hasClass('plus') && (parseInt(currentVal) === parseInt(maxQuantityIn))) {
                $infInput.closest('.qway-search-required-people').find('.plus').prop('disabled', true);
            }
        } else {
            if ($this.hasClass('plus') && (parseInt(currentVal) === parseInt(maxQuantityIn))) {
                $chdInput.closest('.qway-search-required-people').find('.plus').prop('disabled', true);
                $adtInput.closest('.qway-search-required-people').find('.plus').prop('disabled', true);
                if (currentPassengerCode === ADT) {
                    $infInput.closest('.qway-search-required-people').find('.plus').prop('disabled', false);
                }
            }

            if ($this.hasClass('minus')) {
                if (currentPassengerCode === ADT && parseInt(currentVal) < $infInput.val()) {
                    $infInput.val(currentVal);
                }
                $chdInput.closest('.qway-search-required-people').find('.plus').prop('disabled', false);
                $adtInput.closest('.qway-search-required-people').find('.plus').prop('disabled', false);
            }
        }

        var chdCount = parseInt($chdInput.val(), 10);
        var adtCount = parseInt($adtInput.val(), 10);

        var allCount = chdCount + adtCount;

        // var text = ' Пассажира, Эконом';
        // if (allCount === 1) {
        //     var text = ' Пассажир, Эконом'
        // }

        var text = ' Пассажира';
        if (allCount === 1) {
            var text = ' Пассажир'
        }

        $('.show-hide-passenger-and-cabin-block').val(allCount + text);

    });


    /**
     * Города для авто заполнения
     */
    $body.delegate('.example-city', 'click', function () {
        var $this = $(this);
        var $parentElement = $this.closest('.required');
        var currentCity = $this.attr('data-example-city');
        var number = parseInt($parentElement.find('input').attr('id').split('_')[1], 10);
        var action = $parentElement.find('input').attr('data-action');

        var cityData = {
            'astana': {'code': 'TSE', 'type': 'c', 'name': 'Астана'},
            'almaty': {'code': 'ALA', 'type': 'c', 'name': 'Алматы'},
            'moscow': {'code': 'MOW', 'type': 'c', 'name': 'Москва'}
        };

        $parentElement.find('input').val(cityData[currentCity].name);

        activateBackwardCityButton();
        activeReverseCity(action);
        exampleCityInit(cityData[currentCity], action, number);
    });

    $body.delegate('#search-flight-filter-amount', 'click', function (e) {
        e.preventDefault();
        filterSearchResult('amount', this);

    });

    $body.delegate('#search-flight-filter-elapsed', 'click', function (e) {
        e.preventDefault();
        filterSearchResult('elapsed', this);
    });

    $body.delegate('#search-flight-filter-departure-time', 'click', function (e) {
        e.preventDefault();
        filterSearchResult('departure-time', this);
    });

    $body.delegate('#search-flight-filter-airline-select','change', function () {
        filterAirlineSelect(this);
    });

    $body.delegate('#search-flight-filter-airline-round', 'change', function () {
        filterAirlineRound(this);
    });

    $body.delegate('#search-flight-filter-returnable', 'change', function () {
        filterAirlineReturnable(this);
    });

    $body.delegate('.reverse-city-flight', 'click', function () {

        var $departureCityName = $('#flight-departure-city');
        var $departureCode = $('#departure-code_1');
        var $departureType = $('#departure-type_1');

        var $arrivalCityName = $('#flight-arrival-city_1');
        var $arrivalCode = $('#arrival-code_1');
        var $arrivalType = $('#arrival-type_1');

        var departureCityNameVal = $departureCityName.val();
        var departureCodeVal = $departureCode.val();
        var departureTypeVal = $departureType.val();

        var arrivalCityNameVal = $arrivalCityName.val();
        var arrivalCodeVal = $arrivalCode.val();
        var arrivalTypeVal = $arrivalType.val();

        $departureCityName.val(arrivalCityNameVal);
        $departureCode.val(arrivalCodeVal);
        $departureType.val(arrivalTypeVal);

        $arrivalCityName.val(departureCityNameVal);
        $arrivalCode.val(departureCodeVal);
        $arrivalType.val(departureTypeVal);


    });


    $body.delegate('.custom-has-error', 'focus', function () {
        var $this = $(this);
        if ($this.attr('type') === 'radio' || $this.attr('type') === 'checkbox') {
            var $parent = $this.closest('div');
            $parent.css({
                'border': 'none'
            });
            $parent.find('input').removeClass('custom-has-error');
        } else {
            if ($this.hasClass('passenger-doc-id')) {
                $('.passenger-doc-id').css({'border': 'none'}).removeClass('custom-has-error');
            } else {
                $this.css({'border': 'none'}).removeClass('custom-has-error');
            }
        }

        $('.custom-help-block').text(bookFormErrorMessage).fadeOut('slow');
        /*if (emptyInputs.length === 0) {
         $('.custom-help-block').text(bookFormErrorMessage).fadeOut('slow');
         }*/
    });


    $body.delegate('#back-to-search-result', 'click', function () {
        $('#book-form').hide();
        $('#search-result').fadeIn('slow');
        $('.recomendation_filter').show();
    });

    $body.on('click', '.approve-recomendation', function () {
        let $passBlock = $('.passengers-block-form');

        $('.selec-button').hide();
        $passBlock.fadeIn('slow');

        $('html,body').animate({
            scrollTop: $passBlock.offset().top
        }, 700);
    });


    $body.delegate('.ticket-designator', 'click', function () {
        var $this = $(this);
        var msg = $this.siblings('.ticket-designator-message').text().split('|');
        var message = '';
        msg.forEach(function (item) {
            if (item) {
                message += item + "\n";
            }
        });
        alert(message);
    });


    /*$body.delegate('#searchformamadeus-tripdatebackward', 'click', function () {
     if ($('#searchform-tripdate_1').val()) {


     }

     });*/


    // $body.delegate('.trip-date-input', 'focus', function (e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     var preIDName = 'searchform-tripdate_';
    //     var currentNum = parseInt($this.attr('id').split('_')[1], 10);
    //
    //
    //     if ($this.attr('id') !== 'searchform-tripdate_1') {
    //
    //         if ($('#' + preIDName + (currentNum - 1)).val()) {
    //             $this.removeClass('calendar').prop({'readonly': false});
    //             /*// $('.pignose-calendar-wrapper').eq(/!*(currentNum + 1)*!/currentNum).remove();
    //
    //             console.log();
    //             console.log($body.find('.pignose-calendar-wrapper'));*/
    //
    //             /*if ($('.pignose-calendar-wrapper').eq((currentNum - 1))) {
    //                 $('.pignose-calendar-wrapper').eq((currentNum - 1)).remove();
    //             }*/
    //
    //             var minDateArray = $('#' + preIDName + (currentNum - 1)).val().split('-');
    //             /*var minDay = parseInt(minDateArray[0], 10);
    //              var minMonth = parseInt(minDateArray[1], 10);
    //              var minYear = minDateArray[2];*/
    //
    //
    //
    //             // console.log( $('#' + preIDName + (currentNum - 1)).val());
    //             // console.log(minDateArray[2] + '-' + minDateArray[1] + '-' + minDateArray[0]);
    //
    //             $this.pignoseCalendar({
    //                 format: 'DD-MM-YYYY', // date format string. (2017-02-02)
    //                 lang: 'ru',
    //                 apply: onSelectHandler,
    //                 theme: 'blue',
    //                 week: 1,
    //                 buttons: true,
    //                 // toggle: true
    //                 // reverse: true,
    //                 minDate: new Date(minDateArray[2] + '-' + minDateArray[1] + '-' + minDateArray[0]),
    //             });
    //         } else {
    //             /*$this.pignoseCalendar({
    //                 format: 'DD-MM-YYYY', // date format string. (2017-02-02)
    //                 lang: 'ru',
    //                 apply: onSelectHandler,
    //                 theme: 'blue',
    //                 week: 1,
    //                 buttons: true,
    //                 // toggle: true
    //                 // reverse: true,
    //                 minDate: new Date(),
    //             });*/
    //         }
    //
    //     }
    //
    //     $('.trip-date-input').toArray().forEach(function (item) {
    //         var itemNum = $(item).attr('id').split('_')[1];
    //         if (itemNum <= currentNum) {
    //             return false;
    //         } else {
    //             $(item).val('');
    //         }
    //
    //     });
    //
    // });


    /**
     * При клике за предлами модального окна перенаправляем пользователя
     */
    $body.delegate('.book-modal', 'mouseup' , function (e) {
        var div = $('.modal-content');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            closeModalWindow();
        }
    });


    $body.delegate('#qway-offer-modal-show', 'click', function (e) {
        e.preventDefault();
        $('#offer-modal').modal('show');
    });


    $body.delegate('.penalty-condition', 'click', function (e) {
        e.preventDefault();

        var $this = $(this);

        var str = 'Сделать возрат';
        if ($(this).attr('data-action') === 'exchange') {
            var str = 'Сделать обмен';
        }

        $('#penalty-content').find('.before > span').empty().append($this.attr('data-before'));
        $('#penalty-content').find('.after > span').empty().append($this.attr('data-after'));
        $('.penalty-confirm').empty().attr('data-key', $this.attr('data-key')).append(str);

        $('#penalty-modal-client').modal('show');
    });


    $body.delegate('.penalty-confirm', 'click', function (e) {
        e.preventDefault();

        var $this = $(this);

        $.ajax({
            url: controllerUrl + 'request-refunds?id=' + $this.attr('data-key'),
            method: 'post',
            success: function (response) {
                if (JSON.parse(response) === 'ok') {
                    $('#penalty-modal-client').modal('hide');
                } else {

                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });


    $body.delegate('.remove-itinerary', 'click', function (e) {
        e.preventDefault();
        // console.log($('.wrap-form-group-city-data').length);

        if ($('.wrap-form-group-city-data').length > 1) {
            $('.wrap-form-group-city-data').last().hide('slow').queue(function () {

                var number = $(this).index();

                removeArrivalHiddenInput('city-code', number);
                removeArrivalHiddenInput('code', number);
                removeArrivalHiddenInput('type', number);

                $(this).remove();
            });
        }
    });


    $body.delegate('.show-hide-passenger-and-cabin-block', 'click', function (e) {
        e.preventDefault();
        var $el = $('.wrap-passenger-and-cabin-group');

        // console.log($('.wrap-form-group-city-data').length);

        if ($('.wrap-form-group-city-data').length > 1) {
            $el.css({'margin-top': '-3.4em'});
            $('.wrap-form-group-city-data').first().css({'border-radius': '2em 2em 0 0'});
        } else {
            //$el.css({'margin-top': '-0.2em'});
            if ($el.is(':visible')) {
                $('.wrap-form-group-city-data').first().css({'border-radius': '2em'});
            } else {
                $('.wrap-form-group-city-data').first().css({'border-radius': '2em 2em 0 2em'});
            }

        }
        $el.fadeToggle();
    });

    $body.on('click', function (e) {
        if (e.target.className.match(/show-hide-passenger-and-cabin-block/) === null && e.target.closest('.wrap-passenger-and-cabin-group') === null) {
            $('.wrap-passenger-and-cabin-group').fadeOut();
            $('.wrap-form-group-city-data').first().css({'border-radius': '2em'});
        }
    });


   /* $body.delegate('#searchform-tripdate_1', 'blur', function () {
        console.log($(this));
    });*/

    $body.delegate('#searchformamadeus-cabin', 'click', function (e) {
        e.preventDefault();

        var passengerCount = $('.show-hide-passenger-and-cabin-block').val().split(',');

        var cabinClass = $('#searchformamadeus-cabin option:selected').val();

        $('.show-hide-cabin-block').val(cabinClass);
    });


    /*$body.delegate('.make-route', 'click', function (e) {
        e.preventDefault();

        if ($('#date-separator').is(':visible')) {
            $('#date-separator').hide();
        }

        $(this).closest('#wrap-form-group-additional-arrival').fadeOut();

        $('.wrap-form-group-city-data').first().find('.search-flight-form-city-group').fadeOut().queue(function (next) {
            $(this).css({'width': '80%'}).fadeIn();
            next();
        });




        $('.wrap-form-group-city-data').first().find('.search-flight-form-date-group').fadeOut().queue(function (next) {
            $(this).css({'width': '20%'}).fadeIn().find('.required').css({'width': '100%'});
            next();
        });




        $('.wrap-form-group-city-data').first().find('.search-flight-form-passenger-group').hide();
        // $('.wrap-form-group-city-data').last().find('.search-flight-form-passenger-group').hide();
        $('.wrap-form-group-city-data').first().find('.field-searchformamadeus-tripdatebackward').hide();

        $('.wrap-continue-route-group').find('.search-flight-form-passenger-group').hide();

        if ($('.wrap-form-group-city-data').length === 1) {
            $('#search-form-header')/!*.find('span').first()*!/.animate({width: '79%'}, 'slow');

        }

        $('.wrap-continue-route-group').show('slow').queue(function () {
            // $(this).remove();
            $(this).css({'display': 'flex'});
            $('.wrap-continue-route-group').find('.search-flight-form-passenger-group').show();
            // $('.wrap-form-group-city-data').last().find('.search-flight-form-passenger-group').show();



        });



    });*/

    /*LEONID CHANGE*/

    window.addEventListener('storage', function (event) {
        if (event.key === 'searchFlighForm') {
            if (event.newValue !== event.oldValue) {
                swal({
                    title: 'Внимание',
                    text: 'Даные поиска устарели!',
                    type: 'warning'
                }).then(function () {
                    location.reload();
                });
            }
        }
    });

    function peoplePlus()
    {
        var count = parseInt($(this).parent().prev().text()) + 1,
            spanClass = $(this).parent().attr('data-class'),
            id = $(this).parent().attr('data-id');
        $(this).parent().prev().text(count);
        $('#'+id).val(count);
        $('.'+spanClass).text(count);
    }

    function peopleMinus()
    {
        var count = parseInt($(this).parent().prev().text()) - 1,
            spanClass = $(this).parent().attr('data-class'),
            id = $(this).parent().attr('data-id');
        if (count >= 0) {
            $(this).parent().prev().text(count);
            $('#'+id).val(count);
            $('.'+spanClass).text(count);
        }
    }

    function selectClass()
    {
        var value = $(this).text(),
            cabinClass = $(this).attr('data-class');
        $('#search-formamadeus-cabin').text(value);
        $('#searchformamadeus-cabin').val(cabinClass);
        $(this).parent().parent().toggleClass('selected');
        $(this).parent().fadeOut(200);
    }

    $body.on('click', '.searchform-span-passenger-group', function () {
        $(this).next('.passenger-group-button-container').toggle();
    });

    $body.on('click', '#search-formamadeus-cabin', function () {
        $(this).next('.field-searchformamadeus-cabin-select').toggle();
        $(this).parent().toggleClass('selected');
    });

    $body.on('click', '.tbl>.plus', peoplePlus);
    $body.on('click', '.tbl>.minus', peopleMinus);

    $body.on('click', '.field-searchformamadeus-cabin-select>.cabin', selectClass);

    $body.click(function (e) {
        if (e.target.offsetParent) {
            if (
                e.target.className !== 'searchform-span-passenger-group'
                && e.target.offsetParent.className !== 'passenger-group-button-container'
                && e.target.offsetParent.className !== 'search-flight-form-passenger-group required input-bl'
                && e.target.offsetParent.className !== 'field-searchformamadeus-cabin-select'
                && e.target.offsetParent.className === ''
            ) {
                $('.passenger-group-button-container').fadeOut(200);
                $('.field-searchformamadeus-cabin-select').fadeOut(200);
                $('.field-searchformamadeus-cabin').removeClass('selected');
            }
        }
    });


    $.protip();

    var elee = $('.fa-transplantation-num_1');

    elee.protipShow();



    /****************************************************/
    // render
    /****************************************************/

    function renderItems (items, cabin) {
        renderItem(items);
    }

    function renderItem (items) {
        $allResult = $('#search-result');
        $allResultWrap = $('<div>').attr('id', 'search-all-result-wrap');
        var all = '';
        items.forEach(function (item, index) {
            all += renderOption(index, item.options);
        });
        $allResultWrap.append(all);
        $allResult.append($allResultWrap);
        $allResult.css({'display': 'flex'});
    }

    function renderOption (key, options) {
        var all = '';
        // console.log(options);
        options.forEach(function (option) {
            var opt = '<div class="item-wrapper search-result-item item-search-' + key + '">';
            opt += renderContentBlock(option);
            opt += '</div>';
            all += opt;
        });
        return all;
    }

    function renderContentBlock (option, key) {
        console.log(option);
        var segment = option.segments[0];
        var segmentLast = option.segments[option.segments.length - 1];

        return '<div class="item-cont">'
            + '<header>'
                 + '<div class="left-block">'
                    + '<span>рейс</span>&nbsp<h5>' + segment.bort + '</h5>'
                    + '</div>'
                    + '<div class="right-block">'
                    + '<div class="right-block">' + option.elapsedTime + '</h5>'
                + '</div>'
            + '</header>'
        + '</div>';
    }

    /*EXCHANGE TICKET*/
    $body.on('click', '.exchange-to', function () {
        var parentWrap = $(this).closest('.item-wrapper'),
            $loader = $('#overlay-loader'),
            order = $(this).closest('.search-result-item').attr('data-order');
        if (order === undefined) {
            $loader.fadeIn();
            $.ajax({
                url: controllerUrl + 'exchange' + '?key=' + $(this).attr('data-key'),
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $loader.hide();
                    swal({
                        title: 'Внимание!',
                        text: response.message,
                        type: response.status
                    }).then(function () {
                        if (response.status === 'success') {
                            location.reload();
                        }
                    });
                },
                error: function (err) {
                    console.log(err);
                }
            }).always(function () {
                $loader.fadeOut('slow');
            });
        } else {
            if (order === 2) {
                $loader.fadeIn();
                $.ajax({
                    url: controllerUrl + 'exchange' + '?key=' + $(this).attr('data-key'),
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        $loader.hide();
                        swal({
                            title: 'Внимание!',
                            text: response.message,
                            type: response.status
                        }).then(function () {
                            if (response.status === 'success') {
                                location.reload();
                            }
                        });
                    },
                    error: function (err) {
                        console.log(err);
                    }
                }).always(function () {
                    $loader.fadeOut('slow');
                });
            } else {
                if (parseInt($(parentWrap).attr('data-backward-trip'), 10) === 1) {
                    var cloneThisWrap = parentWrap.clone();
                    // cloneThisWrap.find('#add-ticket').fadeOut();
                    var $backwardHeader = $('.backward-header');
                    $backwardHeader.empty().text('Выберите обратный рейс');

                    // console.log($allHideEl);

                    if (parseInt(cloneThisWrap.attr('data-last'), 10) === 1) {
                        $('.combination-block').append(cloneThisWrap.removeClass('search-result-item').attr('data-book-ready', 1)).show();
                        $('.search-result-item').hide();
                        replaceButton(cloneThisWrap, true);
                        $backwardHeader.hide();
                    } else {
                        $('.combination-block').append(cloneThisWrap.removeClass('search-result-item')).show();
                        replaceButton(cloneThisWrap, false);
                        $('.search-result-item').toArray().forEach(function (item) {

                            if ($(item).is(':hidden')) {
                                if ($(item).attr('data-trip-id') === parentWrap.attr('data-trip-id')) {
                                    $(item).fadeIn().attr('data-last', 1);
                                }
                            } else {
                                $(item).fadeOut();
                            }
                        });
                    }
                }
            }
        }
    });

    /****************************************************/
    // hbd
    /****************************************************/
}(jQuery));


