<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

class m170701_124041_add_column_pnr_no_from_electronic_tickets_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('electronic_tickets', 'pnr_no', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('electronic_tickets', 'pnr_no');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170701_124041_add_column_pnr_no_from_electronic_tickets_table cannot be reverted.\n";

        return false;
    }
    */
}
