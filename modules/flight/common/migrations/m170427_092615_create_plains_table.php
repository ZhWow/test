<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `amadeus_tokens`.
 */
class m170427_092615_create_plains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('plains', [
            'id' => $this->primaryKey(),
            'code' => $this->string(10),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('plains');
    }
}
