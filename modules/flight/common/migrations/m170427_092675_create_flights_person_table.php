<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

class m170427_092675_create_flights_person_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('flights_person', [
            'id' => $this->primaryKey(),
            'flight_id' => $this->integer(),
            'person_id' => $this->integer(),
        ]);

        $this->addForeignKey('fk-table_flights-flight_id', 'flights_person', 'flight_id', 'flights', 'id', 'CASCADE');
        $this->addForeignKey('fk-table_persons-person_id', 'flights_person', 'person_id', 'persons', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-table_flights-flight_id', 'flights_person');
        $this->dropForeignKey('fk-table_persons-person_id', 'flights_person');

        $this->dropTable('flights_person');
    }
}
