<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

class m170630_125004_add_column_email_from_persons_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('persons', 'email', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('persons', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170630_125004_add_column_email_from_flights_table cannot be reverted.\n";

        return false;
    }
    */
}
