<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

class m170701_124061_add_column_cabin_class_from_electronic_tickets_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('electronic_tickets', 'cabin_class', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('electronic_tickets', 'cabin_class');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170701_124041_add_column_pnr_no_from_electronic_tickets_table cannot be reverted.\n";

        return false;
    }
    */
}
