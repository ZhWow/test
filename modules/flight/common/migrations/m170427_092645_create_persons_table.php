<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

class m170427_092645_create_persons_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('persons', [
            'id' => $this->primaryKey(),
            'code' => $this->char(3),
            'prefix' => $this->string(5),
            'name' => $this->string(),
            'surname' => $this->string(),
            'birthday' => $this->string(15),
            'doc_id' => $this->string(50),
            'doc_type' => $this->string(50),
            'inner_doc_type' => $this->string(50),
            'expire_date' => $this->string(15),
            'doc_issue_country' => $this->string(50),
            'doc_issue_location' => $this->string(50),
        ]);

        $this->createIndex('idx_doc_id_persons', 'persons', 'doc_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx_doc_id_persons', 'persons');

        $this->dropTable('persons');
    }
}
