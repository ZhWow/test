<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `amadeus_tokens`.
 */
class m170427_092604_create_airlines_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('airlines', [
            'id' => $this->primaryKey(),
            'code_en' => $this->string(5),
            'code_ru' => $this->string(5),
            'code_kz' => $this->string(5),
            'name_en' => $this->string(),
            'name_ru' => $this->string(),
            'name_kz' => $this->string(),
            'css_class_name' => $this->string(),
        ]);

        $this->createIndex('idx_code_en_airlines', 'airlines', 'code_en');
        $this->createIndex('idx_code_ru_airlines', 'airlines', 'code_ru');
        $this->createIndex('idx_code_kz_airlines', 'airlines', 'code_kz');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx_code_en_airlines', 'airlines');
        $this->dropIndex('idx_code_ru_airlines', 'airlines');
        $this->dropIndex('idx_code_kz_airlines', 'airlines');

        $this->dropTable('airlines');
    }
}
