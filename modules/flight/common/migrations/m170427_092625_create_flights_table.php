<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `amadeus_tokens`.
 */
class m170427_092625_create_flights_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('flights', [
            'id' => $this->primaryKey(),
            'validating_airline_code' => $this->string(10),
            'ticket_time_limit' => $this->string(),
            'ticket_type' => $this->string(),
            'booking_reference_id_type' => $this->string(),
            'pnr_no' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('flights');
    }
}
