<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `electronictickets`.
 */
class m170516_132745_create_electronic_tickets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('electronic_tickets', [
            'id' => $this->primaryKey(),
            'ticket_number' => $this->string(),
            'user_id' => $this->integer(),
            'created_at' => $this->string(),
            'flight_number' => $this->string(),
            'res_book_desig_code' => $this->string(),
            'status' => $this->string(),
            'equipment' => $this->string(),
            'iataid' => $this->integer(20),
            'operating_airline' => $this->string(),
            'validating_airline' => $this->string(),
            'marketing_airline' => $this->string(),
            'departure_airport' => $this->string(),
            'departure_terminal' => $this->string(),
            'departure_datetime' => $this->string(),
            'arrival_airport' => $this->string(),
            'arrival_terminal' => $this->string(),
            'arrival_datetime' => $this->string(),
            'elapsed_time' => $this->string(),
            'baggage_weight' => $this->string(),
            'baggage_quantity' => $this->string(),
            'baggage_weight_measure_unit' => $this->string(),
            'fare_basis' => $this->string(),
            'base_fare' => $this->string(),
            'fare_calculation' => $this->string(),
            'tax' => $this->string(),
            'service_fee' => $this->string(),
            'total_fare' => $this->string(),
            'exchange' => $this->smallInteger(1)->defaultValue(0),
            'refunds' => $this->smallInteger(1)->defaultValue(0),
            'exchange_before_departure' => $this->integer(),
            'exchange_after_departure' => $this->integer(),
            'refunds_before_departure' => $this->integer(),
            'refunds_after_departure' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('electronic_tickets');
    }
}
