<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `amadeus_tokens`.
 */
class m170427_082604_create_amadeus_tokens_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('amadeus_tokens', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'created_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('amadeus_tokens');
    }
}
