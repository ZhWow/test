<?php

namespace modules\flight\common\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `amadeus_tokens`.
 */
class m170427_092605_create_airports_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('airports', [
            'id' => $this->primaryKey(),
            'code' => $this->string(5),
            'city_id' => $this->integer(),
            'name_en' => $this->string(),
            'name_ru' => $this->string(),
            'name_kz' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'altitude' => $this->string(),
        ]);

        $this->addForeignKey('fk-table_city-city_id', 'airports', 'city_id', 'cities', 'id', 'CASCADE');

        $this->createIndex('idx_code_airports', 'airports', 'code');

        $this->createIndex('idx_name_en_airports', 'airports', 'name_en');
        $this->createIndex('idx_name_ru_airports', 'airports', 'name_ru');
        $this->createIndex('idx_name_kz_airports', 'airports', 'name_kz');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx_name_en_airports', 'airports');
        $this->dropIndex('idx_name_ru_airports', 'airports');
        $this->dropIndex('idx_name_kz_airports', 'airports');

        $this->dropForeignKey('fk-table_city-city_id', 'airports');

        $this->dropTable('airports');
    }
}
