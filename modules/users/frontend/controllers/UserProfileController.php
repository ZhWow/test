<?php

namespace modules\users\frontend\controllers;

use modules\services\Notification;
use common\modules\helpers\CreatePdf;
use modules\flight\frontend\models\base\FlightElectronicTickets;
use modules\flight\frontend\models\base\FlightExchange;
use modules\flight\frontend\providers\alfa\AlfaBank;
use modules\flight\frontend\providers\mail\SendTicket;
use modules\users\common\models\PayOrders;
use modules\users\common\models\ResetPassword;
use modules\users\common\models\User;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UserProfile;
use modules\countries\common\models\Cities;
use modules\flight\frontend\models\base\Airlines;
use modules\users\common\models\UserPerson;
use modules\flight\frontend\models\base\Persons;
use modules\flight\frontend\models\base\ElectronicTickets;
use modules\flight\frontend\models\ElectronicTickets as BaseElectronicTickets;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightPriceInfo;
/*mPdf*/
use kartik\mpdf\Pdf;
/**
 * UserProfileController implements the CRUD actions for UserProfile model.
 */
class UserProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $dataProvider = new ActiveDataProvider([
//            'query' => UserProfile::find(),
//        ]);
//
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//        ]);
        $flightTickets = FlightElectronicTickets::find()->where(['status' => 1])->all();
        return $this->render('profile', ['flightData' => $flightTickets]);
    }

    /**
     * Displays a single UserProfile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionResetPassword()
    {
        $userModel = User::findOne(Yii::$app->user->id);
        $model = new ResetPassword();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $userModel->setPassword($model->password);
            $userModel->save();
        }

        return $this->render('reset-password', ['model' => $model]);
    }

    /**
     * Creates a new UserProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = UserProfile::findOne(['user_id' => Yii::$app->user->id]);

        if ( !$model ) {
            $model = new UserProfile();
            $model->load(Yii::$app->request->post());
            $model->user_id = Yii::$app->user->id;
            $model->name = mb_strtoupper($model->name);
            $model->surname = mb_strtoupper($model->surname);
            if ($model->save()) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }



        if ($model->load(Yii::$app->request->post())) {

            $model->name = mb_strtoupper($model->name);
            $model->surname = mb_strtoupper($model->surname);

            if ( $model->save() ) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            //return $this->goBack();
//        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionTravelHistoryAir()
    {
//        $dataProvider = new ActiveDataProvider([
//            'query' => ElectronicTickets::find()->where(['user_id' => Yii::$app->user->identity->id])
//        ]);
//        $ElectronicTickets = new ElectronicTickets();
        $dataProvider = FlightElectronicTickets::findAll(['user_id' => (int) Yii::$app->user->identity->id]);

        return $this->render('travel-history-air', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMyFlightBookings()
    {
        $user_id = Yii::$app->user->identity->id;
        $userPerson = new UserPerson();
        $flightBookings = $userPerson->getUserBookings($user_id);
        return $this->render('my-flight-bookings', [
            'flightBookings' => $flightBookings
        ]);
    }

    public function actionMyFlightTickets()
    {
        $tickets = FlightElectronicTickets::find()->where(['user_id' => Yii::$app->user->identity->id])->with('itineraries', 'exchange', 'passenger')->all();

        return $this->render('my-flight-tickets', [
            'tickets' => $tickets
        ]);
    }

    public function actionUpdateTimeLimit()
    {
        $result = [
            'status'    => 'success'
        ];

        $post = Yii::$app->request->post();

        $exchange = FlightExchange::findOne(['id' => $post['exchangeId']]);
        if ($exchange->id > 0) {
            $exchange->exchange_time = date("Y-m-d H:i:s" ,time());
            if ($exchange->update() !== false) {
                $result = [
                    'status'    => 'success'
                ];
            }
        }

        return $this->renderAjax('_update_time_limit', ['exchange' => $result]);
    }

    public function actionPenaltyInfo()
    {
        $post = Yii::$app->request->post();

        $ticket = FlightElectronicTickets::find()->where(['id' => $post['ticket_id']])->with('flightPenalty')->one();
        $flightPenalty = $ticket->flightPenalty;

        return $this->renderAjax('_penalty_info', ['model' => $flightPenalty]);
    }

    public function actionShowPenalties()
    {
        $post = Yii::$app->request->post();

    }

    public function actionHistoryFlightTickets()
    {
        return $this->render('history-flight-tickets');
    }

    public function actionComingTripsAir()
    {
        $result = [];
        $models = ElectronicTickets::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        foreach ($models as $model) {
            /* @var $model ElectronicTickets */
            $currentTime = time();
            $ticketDepartureDate = explode(';', $model->departure_datetime)[0];
            $ticketDepartureTimeArray = explode('T', $ticketDepartureDate);
            $ticketDepartureTime = strtotime($ticketDepartureTimeArray[0] . ' ' . $ticketDepartureTimeArray[1]);
            if ($currentTime < $ticketDepartureTime) {
                $result[] = $model;
            }
        }
        return $this->render('coming-trips-air', ['models' => $result]);
    }


    /**
     * Deletes an existing UserProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetPdf()
    {
        $dataProvider = [];
        if (Yii::$app->request->get()) {
            $requestData = Yii::$app->request->get();
            $pdfClass = new Pdf();

            $ticket = FlightElectronicTickets::findOne(['id' => $requestData['ticketId']]);

            $template = $this->renderAjax('_pdf-ticket', ['models' => $ticket]);
            $api = $pdfClass->getApi();
            $api->WriteHTML($template);
            return $api->Output();
        }
    }

    public function actionPayPdf()
    {
        $post = Yii::$app->request->get();
        $payOrders = PayOrders::findOne($post['pay_order_id']);

        $html = $this->renderAjax('_pdf_payment', ['response' => $payOrders]);

        $pdfClass = new Pdf();
        $api = $pdfClass->getApi();
        $api->WriteHTML($html);
        return $api->Output();
    }

    public function actionSendPayPdf()
    {
        $post = Yii::$app->request->post();
        $payOrder = PayOrders::findOne($post['payId']);
        $filePdf = CreatePdf::existPayFilePdf($payOrder->orderNumber);
        $sendTicketClass = new SendTicket();
        $sendTicketClass->sendPaymentReceipt($post['email'], $filePdf, $payOrder);

        return $this->renderAjax('_send-pay-pdf');
    }

    public function actionToExchangeTicket()
    {
        $result = [
            'status'    => 'success'
        ];

        $post = Yii::$app->request->post();

        $session = Yii::$app->session;
        $session->set('exchangeTicketId', $post['exchangeTicketId']);
        $session->set('exchangeTicketNumber', $post['exchangeTicketNumber']);

        return $this->renderAjax('_on-exchange', ['result' => $result]);
    }

    public function actionToRefundTicket()
    {
        $result = [
            'status'    => 'error'
        ];

        $post = Yii::$app->request->post();

        $ticket = FlightElectronicTickets::findOne($post['ticketId']);
        if ($ticket != null) {
            $ticket->status = 3;
            if ($ticket->update() !== false) {
                $user = User::findOne($ticket->user_id);
                Notification::send('to_refund', $ticket, 'Запрос на возврат', $user->email);

                $result = [
                    'status'    => 'success'
                ];
            }
        }

        return $this->renderAjax('_on-exchange', ['result' => $result]);
    }

    public function actionCancelToExchangeTicket()
    {
        $result = [
            'status'    => 'success'
        ];

        $post = Yii::$app->request->post();
        $session = Yii::$app->session;
        $session->remove('exchangeTicketId');
        $session->remove('exchangeTicketNumber');
        $session->close();

        return $this->renderAjax('_on-exchange', ['result' => $result]);
    }

    public function actionPayment()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $response = AlfaBank::registerOrder($post, Url::canonical());

            if ($response->errorCode != 0) {
                return $this->render('payment', ['response' => $response, 'responseData' => []]);
            } else {
                return $this->redirect($response->formUrl);
            }
        } else {
            $get = Yii::$app->request->get();
            $response = AlfaBank::getOrderStatus($get['orderId']);
            $responseData = [];

            if ($response->errorCode == 0) {
                $payOrders = new PayOrders();
                if ($payOrders->setOrder($response) > 0) {
                    $parseOrderData = explode('_', $response->orderNumber);
                    $ticket = FlightElectronicTickets::findOne($parseOrderData[1]);
                    $responseData['ticket'] = $ticket;
                    $responseData['orderId'] = $payOrders->setOrder($response);
                    if ($parseOrderData[2] == 'exchange') {
                        $exchange = $ticket->exchange;
                        $exchange->is_buy = 1;
                        $exchange->update();
                        $responseData['exchange'] = FlightExchange::getExchangeTicketData($exchange->flight_id);
                    }
                }

                $pdfClass = new CreatePdf();
                $sendTicketClass = new SendTicket();
                $html = $this->renderAjax('_pdf_payment', ['response' => $response]);
                $filePayPdf = 'pay_' . $response->orderNumber;
                $filePdf = $pdfClass->createPdf($filePayPdf, $html);
                $sendTicketClass->sendPaymentReceipt($responseData['ticket']->passenger->email, $filePdf, $response);
            }

            return $this->render('payment', [
                'response'      => $response,
                'responseData'  => $responseData
            ]);
        }
    }

    public function beforeAction($action)
    {
        if ($action->id === 'payment') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}
