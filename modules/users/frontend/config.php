<?php
return [
	'params' => [
        'error_codes' => [
            'ok' => 'ok',
            'cn_e001' => 'Timeout expired',
        ],
        'soap_param' => [
            'PROVIDER_TYPE' => 'OnlyAmadeus',
            'REFUNDABLE_TYPE' => 'AllFlights',
            'WSDL_STG' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx?WSDL',
            'CONNECT_TIMEOUT' => 120,
            'SOAP_SUB_SITE_CODE' => 'Bestar',
            'WS_USER_NAME' => 'WstravelBusinessKZ1',
            'WS_PASSWORD' => 'Btmckz2017@',
            'SOAP_HEADER_NS' => 'http://epowerv5.amadeus.com.tr/WS',
        ],
        'passenger_code' => [
            'ADULTS' => 'ADT',
            'CHILDREN' => 'CHD',
            'INFANTS' => 'INF',
        ],
        'session_var_names' => [
            'tokenId' => '__token_id_',
            'flightId' => '__flight_id_',
        ]
	]
];
