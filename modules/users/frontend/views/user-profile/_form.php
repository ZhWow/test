<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\flight\frontend\models\Persons;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Имя') ?>

            <?= $form->field($model, 'surname')->textInput(['maxlength' => true])->label('Фамилия') ?>

            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+9-999-999-99-99',
            ])->label('Номер телефона') ?>

            <?= $form->field($model, 'birthday')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99',
            ])->label('Дата рождения') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'doc_type')->dropDownList(Persons::getDocType())->label('Тип документа') ?>

            <?= $form->field($model, 'doc_issue_country')->dropDownList(\modules\countries\common\models\Countries::getListItem())->label('Гражданство') ?>

            <?= $form->field($model, 'doc_expire_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99',
            ])->label('Срок действия документа') ?>

            <?= $form->field($model, 'doc_id')->textInput(['maxlength' => true])->label('Номер документа') ?>

            <?= $form->field($model, 'user_id')->hiddenInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>

    <div class="form-group" style="margin: 10px 0;text-align: right;">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
