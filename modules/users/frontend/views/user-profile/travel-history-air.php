<?php

//use yii\grid\GridView;
use common\models\UserProfile;
use modules\flight\frontend\models\Airports;

/* @var $this yii\web\View */
/* @var $model \modules\flight\frontend\models\ElectronicTickets */

$this->title = Yii::t('app', 'Air travel history');

//$lang = \common\services\QLanguage::getCurrentLang();
//
//echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'layout' => '{items}{pager}',
//    /*'filterModel' => $searchModel,*/
//    'options' => [
//        'class' => 'profile-travel-history-table',
//    ],
//    'rowOptions' => function () {
//        return ['class' => 'profile-travel-history-row'];
//        /*$rowMyOptions['class'] = 'ap-item-user';
//        if ($model->status === User::STATUS_BLOCKED) {
//            $rowMyOptions['class'] .= ' ap-blocked-user';
//        }
//        return $rowMyOptions;*/
//    },
//    'columns' => [
//
//        'ticket_number',
//        'pnr_no',
//        [
//            'attribute' => Yii::t('app', 'Full name'),
//            'value' => function ($model) {
//                /* @var $model \modules\flight\frontend\models\ElectronicTickets */
//                $userProfile = UserProfile::findOne(['user_id' => $model->ticket->user_id]);
//                return $userProfile->surname . ' ' . $userProfile->name;
//            }
//        ],
//        [
//            'attribute' => Yii::t('app', 'Date of purchase'),
//            'value' => function ($model) {
//                /* @var $model \modules\flight\frontend\models\ElectronicTickets */
//                return date('d.m.Y H:i', strtotime($model->ticket->created_at));
//            }
//        ],
//        [
//            'attribute' => Yii::t('app', 'Direction'),
//            'format' => 'html',
//            'value' => function ($model) use ($lang) {
//                $airportsString = '';
//                foreach ($model['flightSegments'] as $key => $airports) {
//                    /* @var $model \modules\flight\frontend\models\ElectronicTickets */
//                    $airportsString .= (string) $airports->departure_airport . "-" . (string) $airports->arrival_airport;
//                    if ($key < (count($airports) - 1)) {
//                        $airportsString .= '<br>';
//                    }
//                }
//                return $airportsString;
//            }
//        ],
//        [
//            'attribute' => Yii::t('app', 'Departure date'),
//            'value' => function ($model) {
//                $datesString = '';
//                foreach ($model['flightSegments'] as $key => $dates) {
//                    /* @var $model \modules\flight\frontend\models\ElectronicTickets */
//                    $datesString .= (string) date('d.m.Y H:i', strtotime($dates->departure_date_time));
//                    if ($key < (count($dates) - 1)) {
//                        $datesString .= '<br>';
//                    }
//                }
//                return $datesString;
//            }
//        ],
//        [
//            'attribute' => Yii::t('app', 'Arrival date'),
//            'value' => function ($model) {
//                $datesString = '';
//                foreach ($model['flightSegments'] as $key => $dates) {
//                    /* @var $model \modules\flight\frontend\models\ElectronicTickets */
//                    $datesString .= (string) date('d.m.Y H:i', strtotime($dates->arrival_date_time));
//                    if ($key < (count($dates) - 1)) {
//                        $datesString .= '<br>';
//                    }
//                }
//                return $datesString;
//            }
//        ],
//        'base_fare'
//
//    ],
//]);
?>
<div id="tickets_history_list">
    <div class="title">
        <div class="bl">История поездок</div>
        <div class="bl"></div>
        <div class="bl"></div>
    </div>
    <div class="history-head">
        <div class="th">№ Билет</div>
        <div class="th">Номер брони</div>
        <div class="th">ФИО</div>
        <div class="th">Дата покупки</div>
        <div class="th">Направление</div>
        <div class="th">Дата вылета</div>
        <div class="th">Дата прилета</div>
        <div class="th">Стоимость билета</div>
    </div>
    <div class="history-body">
        <?php foreach ($dataProvider as $item):?>
            <?php
                $img = '';
                if ($item->status == 4 || $item->status == 7) {
                    $img = 'refund';
                } elseif ($item->status == 6) {
                    $img = 'exchange';
                } else {
                    $img = 'pay';
                }
            ?>
            <div class="item">
                <div class="td <?=$img?>"></div>
                <div class="td"><?=$item->ticket_number?></div>
                <div class="td"><?=$item->pnr?></div>
                <div class="td">
                    <?=$item->passenger->surname?><br>
                    <?=$item->passenger->name?>
                </div>
                <div class="td"><?=date('d.m.Y', strtotime($item->ticketing_date))?></div>
                <div class="td">
                    <?php foreach ($item->itineraries as $key => $segment):?>
                        <?=$segment->from?>-<?=$segment->to;?>
                        <?php if ($key <= (count($item->itineraries) - 1)):?>
                            <br>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?php foreach ($item->itineraries as $key => $dates):?>
                        <?=date('d.m.Y', strtotime($dates->departure_date))?>
                        <?php if ($key <= (count($item->itineraries) - 1)):?>
                            <br>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?php foreach ($item->itineraries as $key => $dates):?>
                        <?=date('d.m.Y', strtotime($dates->arrival_date))?>
                        <?php if ($key <= (count($item->itineraries) - 1)):?>
                            <br>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?php
                        //TODO service fee
                        $summ = (int) $item->total_fare + 0;
                    ?>
                    <?=$summ?> KZT
                </div>
                <div class="td">
                    <div class="ticket-side"></div>
                    <div class="download" onclick="downloadPdf(<?=$item->id?>);">
                        <img src="/images/download-ticket.png" alt="download">
                        <div class="text">Скачать</div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>

