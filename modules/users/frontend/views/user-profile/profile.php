<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 06.11.2017
 * Time: 15:54
 * @var $flightData
 */

use modules\flight\frontend\models\base\Airlines;
$this->title = Yii::t('app', 'User Profiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">
    <?php foreach ($flightData as $flightTicket) :?>
    <table class="table table-bordered ticket-table ">
        <tr>
            <?php $airline = Airlines::findOne(['code_en' => $flightTicket->issuing_airline])->name_en;?>
            <th colspan="5" class="head">Билет <?=$flightTicket->pnr?>/<?=$flightTicket->ticket_number?> <?=$airline?></th>
            <th colspan="2">Статус: <span class="status">Куплен</span></th>
        </tr>
        <tr>
            <th>Маршрут</th>
            <th>Рейс</th>
            <th>Класс</th>
            <th>Дата вылета</th>
            <th>Дата прибытия</th>
            <th>Пассажир</th>
            <th>Стоймость</th>
        </tr>
        <?php $iterator = 0;?>
        <?php foreach ($flightTicket->itineraries as $itinerary) :?>
            <tr>
                <td style="vertical-align: middle;">
                    <span class="departure-date"><?=$itinerary->from?></span>-<span class="arrival-date"><?=$itinerary->to?></span>
                </td>
                <td style="vertical-align: middle;"><?=$itinerary->operating_airline_code?>-<?=$itinerary->flight_no?></td>
                <td style="vertical-align: middle;">M</td>
                <td style="vertical-align: middle;">
                    <span class="departure-date"><?=date('H:i d.m.Y', strtotime($itinerary->departure_date))?></span>
                </td>
                <td style="vertical-align: middle;">
                    <span class="arrival-date"><?=date('H:i d.m.Y', strtotime($itinerary->arrival_date))?></span>
                </td>
                <?php
                    $passenger = $flightTicket->passenger;
                ?>
                <?php if ($iterator == 0) :?>
                    <td rowspan="<?=count($flightTicket->itineraries)?>" style="vertical-align: middle;">
                        <?=$passenger->surname?> <?=$passenger->name?>
                    </td>
                    <td rowspan="<?=count($flightTicket->itineraries)?>" style="vertical-align: middle;">
                        <span class="price"><?=number_format($flightTicket->total_fare, 0, ' ', ' ');?> KZT</span><br>
                        <button class="pdf btn btn-default" onclick="downloadPdf(<?=$flightTicket->id?>);">Скачать билет</button>
                    </td>
                <?php endif;?>
            </tr>
            <?php $iterator++;?>
        <?php endforeach;?>
        <tr>
            <td colspan="7">
                <h4>Информация о штрафах в обработке</h4>
            </td>
        </tr>
    </table>
    <?php endforeach;?>
</div>