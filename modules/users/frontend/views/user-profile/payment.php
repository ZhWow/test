<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 21.09.2017
 * Time: 13:15
 * @var $response
 * @var $responseData
 */

use yii\helpers\Url;
?>
<div id="tickets_history_list">
    <div class="title">
        <div class="bl">Статус оплаты</div>
    </div>
    <?php if ($response->errorCode == 0) :?>
        <div class="row">
            <div>
                <div class="container2 zigzag">
                    <div class="row">
                        <div class="col-md-12 textCenter">Квитанция № <?=substr($response->orderNumber, 0, 6);?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 textCenter paymentSuccess">
                            Платёж прошёл успешно
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row" style="padding: 10px;">
                                <div class="col-md-12">
                                    <div class="col-md-12" style="margin-top: 2px; color: #717171;">
                                        ФИО Плательщика: <?=$response->cardholderName?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="margin-top: 2px; color: #717171; text-transform: capitalize;">
                                        Дата платежа: <?=date('Y.m.d H:i', strtotime($response->date))?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="margin-top: 2px; color: #717171;">Получатель: qway.kz</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="margin-top: 2px; color: #717171;">Реквизиты платежа: <?=$response->pan?></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="margin-top: 2px; color: #717171; border-bottom: 2px #e4e4e4 solid; padding-bottom: 10px;">
                                        Оплачено: Кредитной картой
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="margin-left: 10px;">
                            <div class="row">
                                <div class="col-md-12" style="color: #717171;">Оплаченная сумма:</div>
                                <div class="col-md-12" style="color: #717171; font-size: 28px;" ><?=$response->amount / 100?> &#8376;</div>
                            </div>
                        </div>
                        <div class="col-md-6">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">&nbsp;</div>
        </div>
        <div class="row" style="display: flex;flex-direction: column-reverse;align-items: stretch;color: #717171;">
            <div style="padding: 10px;text-align: right;background-color: #f7f7f7;border-radius: 6px;">
                Email: <input type="text" name="paymentRecieptEmail" id="payment_reciept_email" value="<?=$responseData['ticket']->passenger->email?>">
                       <button type="button" class="btn btn-success" id="send_pay_pdf" data-id="<?=$responseData['orderId']?>" style="padding: 1% 5%; font-size: 12px;">Отправить на email</button>
            </div>
            <div class="col-md-6">&nbsp;</div>
        </div>
        <div class="row" style="display: flex;flex-direction: column-reverse;align-items: stretch;color: #717171;">
            <div style="padding: 10px;text-align: center;background-color: #f7f7f7;border-radius: 6px;">
                <a href="<?=Url::to('/profile/user-profile/my-flight-tickets');?>" class="btn  btn-info" style="padding: 1% 2%; font-size: 12px;" href="">Ближайшие поездки</a>
                <a target="_blank" href="<?=Url::to('/profile/user-profile/pay-pdf?pay_order_id=' . $responseData['orderId']);?>" class="btn btn-info" style="padding: 1% 5%; font-size: 12px;">Скачать</a>
            </div>
            <div class="col-md-6">&nbsp;</div>
        </div>
    <?php else :?>
        <h2 style="text-align: center; color: red;">
            <?= $response->errorMessage ?>
        </h2>
    <?php endif;?>
</div>