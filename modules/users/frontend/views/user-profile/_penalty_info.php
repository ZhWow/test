<?php
/**
 * Created by btm.system
 * User: Leonic
 * Date: 06.10.2017
 * Time: 12:28
 * @var $model
 */
?>
<div class="modal-body">
    <table class="table">
        <tr>
            <th>Возврат до вылета</th>
            <th>Возврат после вылета</th>
            <th>Возврат не явка на рейс</th>
            <th>Обмен до вылета</th>
            <th>Обмен после вылета</th>
            <th>Обмен не явка на рейс</th>
        </tr>
        <tr>
            <td><?=$model->refund_bd?> KZT</td>
            <td><?=$model->refund_ad?> KZT</td>
            <td><?=$model->refund_ns?> KZT</td>
            <td><?=$model->exchange_bd?> KZT</td>
            <td><?=$model->exchange_bd?> KZT</td>
            <td><?=$model->exchange_ns?> KZT</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>
