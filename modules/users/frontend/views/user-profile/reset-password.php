<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \modules\users\common\models\ResetPassword */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\users\frontend\assets\ProfileAsset;

$this->title = 'Cброс пароля';
ProfileAsset::register($this);
?>
<div class="user-profile-create qway-wrap">
    <div id="setting">
        <div class="head-title"><?= Html::encode($this->title) ?></div>
        <div class="user-profile-form">
            <?php $form = ActiveForm::begin(['id' => 'user-change-password-form']); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'password')->passwordInput(['id' => 'old_pass', 'maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'password_repeat')->passwordInput(['id'=> 'new_pass', 'maxlength' => true]) ?>
                </div>
            </div>

            <div class="form-group" style="margin: 10px 0;text-align: right;">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
