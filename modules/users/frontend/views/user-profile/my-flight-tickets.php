<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\base\Airlines;
use modules\flight\frontend\models\base\FlightExchange;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tickets */

$this->title = 'Мои билеты';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
/*@var tickets $tickets*/
    foreach ($tickets as $item) :
    $q = 0;
    $class = '';
    $status = '';

    if ($item->status == 1) {
        $status = 'Куплен';
        $class = '';
    } elseif ($item->status == 2) {
        $status = 'Куплен';
        $class = '';
    } elseif ($item->status == 3) {
        $status = 'Отправлен на возврат';
        $class = 'to-refund';
    } elseif ($item->status == 4) {
        $status = 'Возвращен';
        $class = 'to-refund';
    } elseif ($item->status == 5) {
        $status = 'Отправлен на обмен';
        $class = 'old-ticket';
    } elseif ($item->status == 6) {
        $status = 'Обменен';
        $class = 'old-ticket';
    } elseif ($item->status == 7) {
        $status = 'Анулирован';
        $class = 'to-refund';
    }

    $exchange = ($item->exchange != null) ? true : false;
    $exchanged = (FlightExchange::findOne(['new_ticket_id' => $item->id]) != null) ? true : false;
?>
<table class="table table-bordered ticket-table <?=$class?>" <?php if ($exchanged) :?>style="margin-top: -20px; border-radius: 0 0 6px 6px;"<?php endif;?>>
    <tr>
        <?php $airLine = Airlines::findOne(['code_en' => $item->issuing_airline])->name_en?>
        <th colspan="5" class="head">Билет <?=$item->pnr?>/<?=$item->ticket_number?> <?=$airLine?></th>
        <th colspan="2">Статус: <span class="status"><?=$status?></span></th>
    </tr>
    <tr>
        <th>Маршрут</th>
        <th>Рейс</th>
        <th>Класс</th>
        <th>Дата вылета</th>
        <th>Дата прибытия</th>
        <th>Пассажир</th>
        <th>Стоймость</th>
    </tr>
    <?php foreach ($item->itineraries as $segment) :?>
        <?php $totalPrice = (int) $item->total_fare + (int) $item->total_tax_amount; ?>
        <tr>
            <td style="vertical-align: middle;"><span class="departure-date"><?=(string) $segment->from?></span>-<span class="arrival-date"><?=(string) $segment->to?></span></td>
            <td style="vertical-align: middle;"><?=(string) $segment->operating_airline_code?>-<?=(string) $segment->flight_no?></td>
            <td style="vertical-align: middle;"><?=(string) $segment->class?></td>
            <td style="vertical-align: middle;"><span class="departure-date"><?=date('H:i d.m.Y', strtotime($segment->departure_date))?></span></td>
            <td style="vertical-align: middle;"><span class="arrival-date"><?=date('H:i d.m.Y', strtotime($segment->arrival_date))?></span></td>
            <?php if ($q == 0) :?>
                <td rowspan="<?=count($item->itineraries)?>" style="vertical-align: middle;">
                    <?=(string) $item->passenger->surname?> <?=(string) $item->passenger->name?>
                </td>
                <td rowspan="<?=count($item->itineraries)?>" style="vertical-align: middle;">
                    <span class="price"><?=number_format($totalPrice,0,' ',' ')?> KZT</span><br>
                    <?php if (!$exchange && $item->status != 7) :?>
                        <button class="pdf btn btn-default" onclick="downloadPdf(<?=$item->id?>);">Скачать билет</button>
                    <?php endif;?>
                </td>
            <?php endif;?>
        </tr>
    <?php
        $q++;
        endforeach;
    ?>
    <?php if (!$exchange) :?>
        <?php if ($item->status == 1) :?>
            <tr>
                <td colspan="7">
                    <h4>Информация о штрафах в обработке</h4>
                </td>
            </tr>
        <?php elseif ($item->status == 2) :?>
            <tr>
                <td colspan="7">
                    <button class="pdf btn btn-default btn-lg" onclick="onRefund(<?=$item->id?>, '<?=$item->ticket_number?>');">Отправить на возврат</button>
                    <button class="pdf btn btn-default btn-lg" onclick="onExchange(<?=$item->id?>, '<?=$item->ticket_number?>');">Отправить на обмен</button>
                    <button class="btn btn-default penalty-info btn-lg" data-id="<?=$item->id?>">Информация о штрафах</button>
                </td>
            </tr>
        <?php elseif ($item->status == 4) :?>
            <tr>
                <td colspan="7">
                    <h4>Сумма к возврату: <b><?=$item->refund_amount?> KZT</b></h4>
                </td>
            </tr>
        <?php endif;?>
    </table>
    <?php else:?>
    </table>
        <?php if (!$item->exchange->new_ticket_id) :?>
        <table class="table table-bordered ticket-table" style="margin-top: -20px; border-radius: 0 0 10px 10px;">
            <tr>
                <th colspan="8" class="head">Данные нового перелёта</th>
            </tr>
            <tr>
                <th>Маршрут</th>
                <th>Рейс</th>
    <!--            <th>Класс</th>-->
                <th>Дата вылета</th>
                <th>Дата прибытия</th>
    <!--            <th>Время в пути</th>-->
                <th>Пассажир</th>
                <th>Стоймость</th>
            </tr>
            <?php
                $flightExchange = Flights::findOne(['exchange_id' => $item->exchange->id]);
            ?>
            <?php foreach ($flightExchange->flightDetails as $flightDetail) :?>
            <?php $ex = 0;?>
                <?php foreach ($flightDetail->flightSegments as $segmentExchange) :?>
                    <?php $totalPrice = (int) $flightExchange->priceInfo->total_fare_amount; ?>
                    <tr>
                        <td style="vertical-align: middle;"><span class="departure-date"><?=(string) $segmentExchange->departure_airport?></span>-<span class="arrival-date"><?=(string) $segmentExchange->arrival_airport?></span></td>
                        <td style="vertical-align: middle;"><?=(string) $segmentExchange->operating_airline?>-<?=(string) $segmentExchange->flight_number?></td>
                        <td style="vertical-align: middle;"><span class="departure-date"><?=date('H:i d.m.Y', strtotime($segmentExchange->departure_date_time))?></span></td>
                        <td style="vertical-align: middle;"><span class="arrival-date"><?=date('H:i d.m.Y', strtotime($segmentExchange->arrival_date_time))?></span></td>
                        <?php if ($ex == 0):?>
                            <td rowspan="<?=count($flightDetail->flightSegments)?>" style="vertical-align: middle;">
                                <?=(string) $item->passenger->surname?> <?=(string) $item->passenger->name?>
                            </td>
                            <td rowspan="<?=count($flightDetail->flightSegments)?>" style="vertical-align: middle;">
                                <span class="price"><?=number_format($totalPrice,0,' ',' ')?> KZT</span><br>
                            </td>
                        <?php endif;?>
                    </tr>
                <?php
                    $ex++;
                endforeach;
                ?>
            <?php endforeach;?>
            <tr>
                <?php
                $timeLimit = false;
                if ($item->exchange->is_buy == 0) {
                    $timeLimit = FlightExchange::exchangeDateDiff($item->exchange->exchange_time);
                }
                ?>
                <?php if ($timeLimit['limit']) :?>
                    <td colspan="5" style="border-bottom: none; text-align: left; padding-left: 25px;">
                        <form action="<?= Url::toRoute('/profile/user-profile/payment') ?>" method="post">
                            <input type="hidden" name="orderNumber" value="<?=$item->pnr?>_<?=$item->id?>_exchange_<?=rand(0,999)?>">
                            <input type="hidden" name="amount" id="payment-amount" value="<?= ($item->total_fare - $flightExchange->priceInfo->total_fare_amount) + 1000?>">
                            <?= Html::submitButton('Переити к оплате', ['id' => 'modal-payment-submit', 'class'=>'btn btn-success btn-lg']) ?>
                        </form>
                        <!--<button class="pdf btn btn-success">Оплатить</button>-->
                    </td>
                    <td colspan="1" style="vertical-align: middle; border-bottom: none;">
                        <div class="time-limit active">
                            <div class="time">
                                <div class="minute"><?=$timeLimit['min']?></div>
                                <span>минут</span>
                            </div>
                            <span class="delimiter">:</span>
                            <div class="time">
                                <div class="second"><?=$timeLimit['sec']?></div>
                                <span>секунд</span>
                            </div>
                        </div>
                    </td>
                <?php else :?>
                    <?php if ($item->exchange->is_buy == 1) :?>
                        <td colspan="7" style="border-bottom: none;">
                            <h3 style="margin: 0; text-align: center; color: #0ca539;">Оплачено</h3>
                        </td>
                    <?php else :?>
                        <td colspan="5" style="border-bottom: none; text-align: left; padding-left: 25px;">
                            <button class="pdf btn btn-success update-timelimit btn-lg" data-id="<?=$item->exchange->id?>">Увеличить тайм лимит</button>
                        </td>
                        <td colspan="1" style="vertical-align: middle; border-bottom: none;">
                            <div class="time-limit red">
                                <div class="time">
                                    <div class="minute">0</div>
                                    <span>минут</span>
                                </div>
                                <span class="delimiter">:</span>
                                <div class="time">
                                    <div class="second">00</div>
                                    <span>секунд</span>
                                </div>
                            </div>
                        </td>
                    <?php endif;?>
                <?php endif;?>
            </tr>
        </table>
        <?php endif;?>
    <?php endif;?>
<?php endforeach;?>
<div class="modal fade" id="tickets_modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-head">
                <h1 class="modal-title">Информация о штрафах</h1>
            </div>
            <div id="result_modal"></div>
        </div>
    </div>
</div>