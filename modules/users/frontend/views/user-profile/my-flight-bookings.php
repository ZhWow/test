<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои брони';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="tickets_history_list">
    <div class="title">
        <div class="bl">Мои брони</div>
    </div>
    <div class="history-head">
        <div class="th">Номер брони</div>
        <div class="th">ФИО</div>
        <div class="th">Дата покупки</div>
        <div class="th">Направление</div>
        <div class="th">Дата вылета</div>
        <div class="th">Дата прилета</div>
        <div class="th">Стоимость билета</div>
    </div>
    <div class="history-body">
        <?php foreach ($flightBookings as $item):?>
            <div class="item">
                <div class="td"></div>
                <div class="td"><?=$item['pnr_no']?></div>
                <div class="td">
                    <?php foreach ($item['PAX'] as $key => $pax):?>
                        <?=$pax->surname?>
                        <br>
                        <?=$pax->name?>
                        <?php if ($key <= (count($item['PAX']) - 1)):?>
                            <br>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
                <div class="td"><?=date('d.m.Y', strtotime($item['ticket_time_limit']))?></div>
                <div class="td">
                    <?php foreach ($item['Segments'] as $key => $segment):?>
                        <?php foreach ($segment as $keys => $segmentItem):?>
                            <?=$segmentItem->departure_airport?>-<?=$segmentItem->arrival_airport;?>
                            <?php if ($keys <= (count($segmentItem) - 1)):?>
                                <br>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?php foreach ($item['Segments'] as $key => $dates):?>
                        <?php foreach ($dates as $keys => $dateItem):?>
                            <?=date('d.m.Y', strtotime($dateItem->departure_date_time))?>
                            <?php if ($keys <= (count($dateItem) - 1)):?>
                                <br>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?php foreach ($item['Segments'] as $key => $dates):?>
                        <?php foreach ($dates as $keys => $dateItem):?>
                            <?=date('d.m.Y', strtotime($dateItem->arrival_date_time))?>
                            <?php if ($keys <= (count($dateItem) - 1)):?>
                                <br>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </div>
                <div class="td">
                    <?=$item['PriceInfo']['total_fare_amount']?> KZT
                    <br>
                    <?php foreach ($item['PAX'] as $key => $pax):?>
                        <?php
                            $ownerSurname = $pax->surname;
                            break;
                        ?>
                    <?php endforeach;?>
                    <button class="cancel btn btn-default" style="margin: 10px 0; min-width: 110px;" onclick="CancelBook('<?=$ownerSurname?>', '<?=$item['pnr_no']?>');">Отменить</button>
                    <br>
                    <form action="/flight/flight/payment" id="payment-form" method="post">
                        <input type="hidden" name="orderNumber" id="payment-order-number" value="<?=$item['pnr_no']?>">
                        <input type="hidden" name="amount" id="payment-amount" value="<?=$item['PriceInfo']['total_fare_amount']?>">
                        <button type="submit" id="modal-payment-submit" class="buy btn btn-success" style="min-width: 110px; padding: initial; font-size: inherit;">Купить</button>
                    </form>
                </div>
                <div class="td"></div>
            </div>
        <?php endforeach;?>
    </div>
</div>
