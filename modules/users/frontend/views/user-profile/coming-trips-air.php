<?php

use yii\grid\GridView;
use common\models\UserProfile;
use modules\flight\frontend\models\Airports;
use modules\flight\frontend\models\ElectronicTickets;
use yii\helpers\Html;
use modules\flight\frontend\models\Plains;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $models array \modules\flight\frontend\models\ElectronicTickets */
/* @var $model \modules\flight\frontend\models\ElectronicTickets */

\modules\users\frontend\assets\ProfileAsset::register($this);

$this->title = Yii::t('app', 'Coming trips');

$lang = \common\services\QLanguage::getCurrentLang();
?>

<div class="profile-coming-trips">
    <?php foreach ($models as $model) : ?>

        <div class="coming-trips-item">
            <div class="coming-trips-item-header">
                <?php // todo get airline нужно продумать для несклолько перелетов ?>
                <?= \modules\flight\frontend\models\Airlines::getAirline($model->operating_airline)->name ?>
                <?= $model->ticket_number ?>
                <?= $model->ticket_number ?>
            </div>
            <div class="coming-trips-item-body">
                <div class="coming-trips-item-body-header">
                    <strong class="departure"><?= Yii::t('flt', 'Departure') ?></strong>
                    <strong class="terminal"><?= Yii::t('flt', 'Terminal') ?></strong>
                    <strong class="equipment"><?= Yii::t('flt', 'Equipment') ?></strong>
                    <strong class="bort-class"><?= Yii::t('flt', 'Class') ?></strong>
                    <strong class="date"><?= Yii::t('flt', 'Date') ?></strong>
                    <strong class="fullname"><?= Yii::t('flt', 'Fullname') ?></strong>
                    <strong class="status"><?= Yii::t('flt', 'Status') ?></strong>
                </div>
                <div class="coming-trips-item-body-content">
                    <?php

                    $dateArray = explode(';', $model->departure_datetime);
                    $depAirportArray = explode(';', $model->departure_airport);
                    $arrAirportArray = explode(';', $model->arrival_airport);
                    for ($i = 0; $i < count($dateArray); $i++) :
                    ?>
                        <div class="coming-trips-item-body-departure departure">
                            <?= $depAirportArray[$i] ?> - <?= $arrAirportArray[$i] ?>
                        </div>
                        <div class="coming-trips-item-body-terminal terminal">

                        </div>
                        <div class="coming-trips-item-body-equipment equipment">
                            <?= Plains::getBort($model->equipment)->name ?>
                        </div>
                        <div class="coming-trips-item-body-class bort-class">
                            <?= Yii::t('flt', $model->cabin_class) ?>
                        </div>
                        <div class="coming-trips-item-body-date date">
                            <?= ElectronicTickets::departureDate($dateArray[$i]) ?>
                            /
                            <?= ElectronicTickets::departureDateTime($dateArray[$i]) ?>
                        </div>
                    <?php endfor; ?>
                    <div class="coming-trips-item-body-fullname fullname">
                        <?= UserProfile::findOne(['user_id' => Yii::$app->user->identity->id])->surname ?>
                        <?= UserProfile::findOne(['user_id' => Yii::$app->user->identity->id])->name ?>
                    </div>
                    <div class="coming-trips-item-body-status status"></div>
                </div>
            </div>
            <div class="coming-trips-item-footer">

                <?php
                if ($model->exchange === ElectronicTickets::PENALTY_READY) {
                    echo Html::button('Условия обмена', [
                        'class' => 'penalty-condition btn btn-default',
                        'data' => [
                            'action' => 'exchange',
                            'key' => $model->id,
                            'before' => $model->exchange_before_departure,
                            'after' => $model->exchange_after_departure,
                        ]
                    ]);
                }

                if ($model->refunds === ElectronicTickets::PENALTY_READY) {
                    echo Html::button('Условия возврата', [
                        'class' => 'penalty-condition btn btn-default',
                        'data' => [
                            'key' => $model->id,
                            'action' => 'refunds',
                            'before' => $model->refunds_before_departure,
                            'after' => $model->refunds_after_departure,
                        ]
                    ]);
                }
                ?>


            </div>
        </div>

    <?php endforeach; ?>
</div>

<?php
Modal::begin([
    'header' => '<h3>Штрафы</h3>',
    'id' => 'penalty-modal-client',
    /*'options' => [
        'class' => 'book-modal'
    ]*/
]);
?>
    <div id="penalty-content">
        <div class="before">
            До первого вылета: <span></span>
        </div>
        <div class="after">
            После первого вылета: <span></span>
        </div>

        <button type="button" class="penalty-confirm"></button>
    </div>
<?php
Modal::end();