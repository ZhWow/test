<?php
/**
 * Created by PhpStorm.
 * User: Leonic
 * Date: 21.09.2017
 * Time: 15:46
 * @var $response
 */
?>
<style>
    body,tr,td{
        font-family: dejavusans;
    }
</style>
<table style="width: 300px; border: 2px solid #ddd; color: #7d7d7d; font-size: 10px">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr align="center">
        <td>Квитанция № <?=mb_substr($response->orderNumber, 0, 6);?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr align="center">
        <td style="padding: 10px;border-left: 1px #ddd solid; border-right: 1px #ddd solid; background-color: #e6e7e7; height: 5px;line-height:3px;">Платёж прошёл успешно</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp; ФИО Плательщика: <?=$response->cardholderName?></td>
    </tr>
    <tr>
        <td>&nbsp; Дата платежа: <?=date('Y.m.d H:i', strtotime($response->date));?></td>
    </tr>
    <tr>
        <td>&nbsp; Получатель: <?=$response->recipient?></td>
    </tr>
    <tr>
        <td>&nbsp; Реквизиты платежа: <?=$response->pan?></td>
    </tr>
    <tr>
        <td>&nbsp; Оплачено: Кредитной картой</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px #ddd solid;">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp; Оплаченная сумма: </td>
    </tr>
    <tr>
        <td style="font-size: 20px">&nbsp;<?=$response->amount / 100?> &#8376;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
