<?php

use yii\helpers\Html;
use modules\users\frontend\assets\ProfileAsset;


/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */

$this->title = 'Редактировать';
/*$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
ProfileAsset::register($this);
?>
<div class="user-profile-create qway-wrap">
    <div id="setting">
        <div class="head-title">Настройки</div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->
</div>
