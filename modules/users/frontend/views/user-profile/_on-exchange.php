<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 14.09.2017
 * Time: 17:18
 * @var $result
 */
$session = Yii::$app->session;
if ($session->get('exchangeTicketId') != null || $result['status'] == 'success') {
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}
