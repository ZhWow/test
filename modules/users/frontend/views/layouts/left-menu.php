<?php

use yii\helpers\Html;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="qway-wrap qway-profile-main">

    <div class="qway-profile-main-left-menu">
        <?= Menu::widget([
            'items' => [
                ['options' => ['class'=> 'personal-account'], 'label' => 'Личные данные', 'url' => ['/profile'], 'active' => $this->context->route == 'profile/user-profile/index'],
                ['options' => ['class'=> 'travel-history'], 'label' => 'История поездок', 'url' => ['/profile/user-profile/travel-history-air']],
//                ['label' => 'Ближайшие поездки', 'url' => ['/profile/user-profile/coming-trips-air']],
                ['options' => ['class'=> 'nearest-trip'], 'label' => 'Ближайшие поездки', 'url' => ['/profile/user-profile/my-flight-tickets']],
                ['options' => ['class'=> 'settings'], 'label' => 'Настройки', 'url' => ['/profile/user-profile/create']],
//                ['label' => Yii::t('app', 'Personal data'), 'url' => ['/profile'], 'active' => $this->context->route == 'profile/user-profile/index'],
//                ['label' => Yii::t('app', 'Travel history'), 'url' => ['/profile/user-profile/travel-history-air']],
//                ['label' => Yii::t('app', 'Coming trips'), 'url' => ['/profile/user-profile/coming-trips-air']],
//                ['label' => Yii::t('app', 'Settings'), 'url' => ['site/contacts']],
            ],
            'activeCssClass'=>'active',
            'firstItemCssClass'=>'fist',
            'lastItemCssClass' =>'last',
        ]) ?>
    </div>

    <?php if ($this->title === Yii::t('app', 'User Profiles')) :?>

            <div class="user-profile-header">
                <!-- a href="">Sales management</a -->
                <a href="<?=\yii\helpers\Url::to(['user-profile/my-flight-bookings']) ?>">Мои брони</a>
                <a href="<?=\yii\helpers\Url::to(['user-profile/my-flight-tickets']) ?>">Мои билеты</a>
            </div>

    <?php elseif ($this->title === Yii::t('app', 'Air travel history') || $this->title === Yii::t('app', 'Coming trips')) : ?>

        <div class="user-profile-header">
            <!--a href="history-flight-tickets">Авиа</a>
            <a href="">Rails</a>
            <a href="">Hotels</a-->
        </div>

    <?php elseif ($this->title === "Редактировать" || $this->title == "Cброс пароля") : ?>

        <div class="user-profile-header">
            <a href="<?=\yii\helpers\Url::to(['user-profile/create']) ?>">Пасспортные данные</a>
            <a href="<?=\yii\helpers\Url::to(['user-profile/reset-password']) ?>">Пароль</a>
        </div>

    <?php else : ?>

    <?php endif;?>

    <div class="qway-profile-main-content">
        <?= $content; ?>
    </div>

</div>


<?php $this->endContent(); ?>
<div id="overlay-loader">
    <div class="loader-main">
        <div></div>
    </div>
</div>