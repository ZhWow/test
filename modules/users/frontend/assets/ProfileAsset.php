<?php

namespace modules\users\frontend\assets;

use yii\web\AssetBundle;

class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@modules/users/frontend/assets';
    public $css = [
        'css/profile.css'
    ];
    public $js = [
        'js/profile.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}