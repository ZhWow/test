<?php

namespace modules\users\frontend;

use yii\helpers\ArrayHelper;

/**
 * user-profile module definition class
 */
class Module extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'modules\users\frontend\controllers';

    /**
     * @inheritdoc
     */
    public $defaultRoute = 'user-profile';

    /**
     * @inheritdoc
     */
    public $layout = 'left-menu';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
        parent::init();
        $localConfig = '/config-local.php';
        if (file_exists(__DIR__ . $localConfig)) {
            \Yii::configure($this, ArrayHelper::merge(
                require(__DIR__ . '/config.php'),
                require(__DIR__ . $localConfig)
            ));
        } else {
            \Yii::configure($this, require(__DIR__ . '/config.php'));
        }
    }
}
