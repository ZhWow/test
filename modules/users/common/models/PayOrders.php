<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 21.09.2017
 * Time: 14:42
 */

namespace modules\users\common\models;

use yii\db\ActiveRecord;

class PayOrders extends ActiveRecord
{
    public $recipient = 'qway.kz';

    public static function tableName()
    {
        return 'pay_orders';
    }

    public function setOrder($response)
    {
        $status = 0;

        $order = $this::findOne(['orderNumber' => $response->orderNumber]);

        if ($order == null) {
            $this->errorCode = $response->errorCode;
            $this->params = json_encode($response->params);
            $this->orderStatus = json_encode($response->params);
            $this->orderNumber = $response->orderNumber;
            $this->pan = $response->pan;
            $this->expiration = $response->expiration;
            $this->cardholderName = $response->cardholderName;
            $this->amount = $response->amount;
            $this->currency = $response->currency;
            $this->authCode = $response->authCode;
            $this->ip = $response->ip;
            $this->date = $response->date;
            $this->actionCodeDescription = $response->actionCodeDescription;

            if ($this->save() !== false) {
                $status = $this->id;
            }
        } else {
            $status = $order->id;
        }

        return $status;
    }
}