<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 13.09.2017
 * Time: 1:00
 */

namespace modules\users\common\models;


use yii\base\Model;

/**
 * ResetPassword is the model used by the view allowing user to change password from profile.
 */
class ResetPassword extends Model
{
    public $password;
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','password_repeat'],'required','message' => 'Поле не может быть пустым'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Поля не совпадают']
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_repeat' => 'Пароль еще раз',
        ];
    }
}