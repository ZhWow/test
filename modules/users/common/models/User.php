<?php

namespace modules\users\common\models;

use modules\flight\frontend\models\Documents;
use Yii;
use common\models\UserProfile;
use modules\flight\frontend\models\Persons;
use backend\modules\admin\models\AuthItem;
use yii\base\Exception;


class User extends \common\models\User
{

    public $manager = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->manager = Yii::$app->getAuthManager();
    }


    public function saveNewUser(Persons $person, $email, $phone)
    {
        $this->username = $person->name;
        $this->email = $email;
        $this->setPassword('123456');
        $this->generateAuthKey();
        if ($this->save()) {
            UserProfile::autoCreateProfile($person, $this->id, $phone);

            $userPerson = new UserPerson();
            $userPerson->saveUser($this->id,$person->id);

            $session = Yii::$app->session;
            $session->set('userID', $this->id);
        } else {
            Yii::error($this->getErrors(), 'flight');
        }
    }


    /**
     * Returns roles and permissions
     * $availableExclude - all roles and permissions,
     * other than those excluded (module config paramExclude)
     * $availableExclude - roles and permissions that are at the user, other than those excluded,
     * other than those excluded (module config paramExclude)
     * @return array
     */
    public function getItems()
    {
        $manager = $this->manager;

        $available = [];
        foreach (array_keys($manager->getRoles()) as $name) {
            $available[$name] = 'role';
        }

        foreach (array_keys($manager->getPermissions()) as $name) {
            if ($name[0] != '/') {
                $available[$name] = 'permission';
            }
        }

        $assigned = [];

        foreach ($manager->getAssignments($this->id) as $userAssignmentItem) {
            if ($manager->getChildren($userAssignmentItem->roleName)) {
                if (isset($available[$userAssignmentItem->roleName])) {
                    $assigned[$userAssignmentItem->roleName] = $available[$userAssignmentItem->roleName];
                    unset($available[$userAssignmentItem->roleName]);
                }
                foreach ($manager->getPermissionsByRole($userAssignmentItem->roleName) as $childPermissionItem) {

                    if (!$this->childRole($userAssignmentItem->roleName, $childPermissionItem->name, 'permission', $assigned, $available)) {
                        continue;
                    }
                }
                if (AuthItem::find()->where(['name' => $userAssignmentItem->roleName])->one()->type !== AuthItem::TYPE_PERMISSION) {
                    foreach ($manager->getChildRoles($userAssignmentItem->roleName) as $childItem) {
                        if (!$this->childRole($userAssignmentItem->roleName, $childItem->name, 'role', $assigned, $available)) {
                            continue;
                        }
                    }
                }
            } else {
                if (isset($available[$userAssignmentItem->roleName])) {
                    $assigned[$userAssignmentItem->roleName] = $available[$userAssignmentItem->roleName];
                    unset($available[$userAssignmentItem->roleName]);
                }
            }
        }

        $paramExclude = array_flip(Yii::$app->controller->module->params['paramExclude']);
        $availableExclude = array_diff_key($available, $paramExclude);
        $assignedExclude = array_diff_key($assigned, $paramExclude);

        return [
            'available' => $availableExclude,
            'assigned' => $assignedExclude
        ];
    }


    /**
     * Saves permissions and the role of the current user
     * @param int $user_id User id
     * @param array $items An array of names of roles and permissions
     * @return int
     */
    public function assign(array $items, $user_id)
    {
        $manager = $this->manager;
        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ?: $manager->getPermission($name);
                $manager->assign($item, $this->id);
                /*$param = LogUserParameters::getInstance();
                $param->changeAssign = ;*/
               /* LogUser::writeLog($user_id, LogUser::CHANGE_PERMISSION, [
                    'name' => $item->name,
                    'action' => 'Set'
                ]);*/
                $success++;
            } catch (Exception $e) {
//                Yii::error($e->getMessage(), 'admin-panel');
            }
        }
        return $success;
    }

    /**
     * Remove permissions and the role of the current user
     * @param int $user_id User id
     * @param array $items An array of names of roles and permissions
     * @return int
     */
    public function revoke(array $items, $user_id)

    {
        $manager = $this->manager;
        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ?: $manager->getPermission($name);
                $manager->revoke($item, $this->id);
                /*LogUser::writeLog($user_id, LogUser::CHANGE_PERMISSION, [
                    'name' => $item->name,
                    'action' => 'Remove'
                ]);*/
                $success++;
            } catch (Exception $e) {
//                Yii::error($e->getMessage(), 'admin-panel');
            }
        }
        return $success;
    }



    private function childRole($parentName, $childName, $itemType, array &$assigned, array &$available)
    {
        if ($parentName == $childName) {
            if (isset($available[$childName])) {
                $assigned[$childName] = $available[$childName];
                unset($available[$childName]);
            }
            return false;
        }
        if (isset($available[$childName])) {
            $available[$childName] = 'child' . ',' . $itemType . ',' . $parentName;
            $assigned[$childName] = $available[$childName];
            unset($available[$childName]);
        }
        return true;
    }


}