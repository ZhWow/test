<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 11.09.2017
 * Time: 16:24
 */

namespace modules\users\common\models;

use common\models\User as User2;
use modules\flight\frontend\models\base\BookingClassAvails;
use modules\flight\frontend\models\base\FlightDetails;
use modules\flight\frontend\models\base\FlightPriceInfo;
use modules\flight\frontend\models\base\FlightSegments;
use modules\flight\frontend\models\base\FlightsPerson;
use modules\flight\frontend\models\base\Flights;
use modules\flight\frontend\models\Persons;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_person".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $person_id
 */

class UserPerson extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_person';
    }

    public function getUser()
    {
        return $this->hasOne(User2::className(),['user_id' => 'id']);
    }

    public function getPerson()
    {
        return $this->hasOne(Persons::className(),['person_id' => 'id']);
    }

    public function getPersons()
    {
        return $this->hasMany(FlightsPerson::className(),['person_id' => 'person_id']);
    }

    public function saveUser($user_id, $person_id)
    {
        $user = $this->findOne(['user_id' => $user_id, 'person_id' => $person_id]);
        if($user == null){
            $this->user_id = $user_id;
            $this->person_id = $person_id;
            $this->save();
            return true;
        }
        return false;
    }

    public function getUserBookings($user_id)
    {
        $dataProvider = [];
        $userPerson = new UserPerson();
        $flightsPerson = new FlightsPerson();
        $flightSegments = new FlightSegments();
        $userPersons = $userPerson->findAll(['user_id' => $user_id]);


        foreach ($userPersons as $userPersonItem) {
            $flightIDsTemp = $flightsPerson->findAll(['person_id' => (int) $userPersonItem->person_id, 'ticket_id' => null ]);
            if($flightIDsTemp == null)
                continue;

            foreach ($flightIDsTemp as $flightIDsTempItem) {

                $personsInFlight = FlightsPerson::findAll(['flight_id' => $flightIDsTempItem->flight_id]);
                $flightDetails = FlightDetails::findAll(['flight_id' => $flightIDsTempItem->flight_id]);
                $flightsPnr = Flights::findOne(['id' => $flightIDsTempItem->flight_id]);
                if (!$flightsPnr->pnr_no) {
                    continue;
                }
                $dataProvider[$flightIDsTempItem->flight_id]["pnr_no"] = $flightsPnr->pnr_no;
                $dataProvider[$flightIDsTempItem->flight_id]["ticket_time_limit"] = $flightsPnr->ticket_time_limit;

                foreach ($flightDetails as $flightDetailKey => $flightDetail) {
                    $dataProvider[$flightIDsTempItem->flight_id]["Segments"][$flightDetailKey] = $flightSegments->findAll(['flight_details_id' => (int) $flightDetail->id]);

                    foreach ($dataProvider[$flightIDsTempItem->flight_id]["Segments"][$flightDetailKey] as $segment) {
                        $dataProvider[$flightIDsTempItem->flight_id]["BookingClassAvails"][$segment->id] = BookingClassAvails::findOne(['segment_id' => $segment->id]);
                    }

                }



                foreach ($personsInFlight as $personInFlight) {
                    $dataProvider[$flightIDsTempItem->flight_id]["PAX"][$personInFlight->person_id] = Persons::findOne($personInFlight->person_id);
                }

                $dataProvider[$flightIDsTempItem->flight_id]["PriceInfo"] = FlightPriceInfo::findOne(['flight_id' => $flightIDsTempItem->flight_id]);

            }
        }

        return $dataProvider;
    }
}