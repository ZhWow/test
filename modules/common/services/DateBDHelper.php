<?php

namespace modules\common\services;

class DateBDHelper
{
    public static function delimiterT($data)
    {
        $dateArray = explode('T', $data);
        return [
            'date' => $dateArray[0],
            'time' => $dateArray[1],
        ];
    }

    public static function normalize()
    {

    }
}