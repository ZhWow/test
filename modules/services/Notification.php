<?php
/**
 * Created by btmc.kz
 * User: Leonic
 * Date: 10.10.2017
 * Time: 10:33
 */

namespace modules\services;

use modules\flight\frontend\models\base\FlightElectronicTickets;
use Yii;
use yii\BaseYii;
use backend\modules\integra\services\TelegramBot;
use modules\flight\frontend\models\base\FlightExchange;

class Notification extends BaseYii
{
    public static function send($view, $param, $title, $email)
    {
        return Yii::$app->mailer->compose($view, [
            'param' => $param
        ])
        ->setFrom([
            'digital@qway.kz' => 'Qway No-Reply'
        ])
        ->setTo($email)
        ->setSubject('Qway: ' . $title)
        ->send();
    }

    public static function telegramSend($ticket, $trigger) {
        $ticketData = FlightElectronicTickets::setTicketData($ticket);
        $telegramData = FlightExchange::setToTelegramData($ticketData);

        $telegramBotClass = new TelegramBot();
        $telegramBotClass->qwayTelegramNotification($trigger, $telegramData, Yii::$app->params['telegram']['flights']['Key']);
    }
}