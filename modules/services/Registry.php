<?php

namespace modules\services;

use Yii;

class Registry
{

    public static function push($data, $name)
    {
        $session = Yii::$app->session;
        $session->set($name, serialize($data));
    }

    public static function getData($name)
    {
        $session = Yii::$app->session;
        return unserialize($session->get($name));
    }

}