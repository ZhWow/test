<?php

namespace modules\countries\common\models;;

use modules\countries\common\models\base\Countries as BaseCountries;
use common\services\QLanguage;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "countries".
 *
 * @property string $name
 */
class Countries extends BaseCountries
{

    public function getName() {
        if ($this['name_' . QLanguage::getCurrentLang()]) {
            return $this['name_' . QLanguage::getCurrentLang()];
        }
        return $this['name_' . QLanguage::getDefaultLang()];
    }

    public static function getListItem()
    {
        $result = self::find()->orderBy(['name_' . QLanguage::getCurrentLang() => SORT_ASC])->asArray()->all();
        $kazakhstan = self::findOne(['code' => 'kz']);
        $russia = self::findOne(['code' => 'ru']);
        array_unshift($result, $kazakhstan, $russia);

        return ArrayHelper::map($result, 'code', 'name_' . QLanguage::getCurrentLang());
    }
}
