<?php

namespace modules\countries\common\models\base;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $code
 * @property string $expanded_code
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 *
 * @property Cities[] $cities
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'expanded_code'], 'string', 'max' => 5],
            [['name_en', 'name_ru', 'name_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'expanded_code' => Yii::t('app', 'Expanded Code'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_kz' => Yii::t('app', 'Name Kz'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['country_id' => 'id']);
    }
}
