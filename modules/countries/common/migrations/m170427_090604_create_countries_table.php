<?php

namespace modules\countries\common\migrations;

use yii\db\Migration;

class m170427_090604_create_countries_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'code' => $this->string(5),
            'expanded_code' => $this->string(5),
            'name_en' => $this->string(),
            'name_ru' => $this->string(),
            'name_kz' => $this->string(),
        ]);

        $this->createIndex('idx_code_countries', 'countries', 'code');
    }



    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx_code_countries', 'countries');

        $this->dropTable('countries');
    }
}
