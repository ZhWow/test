<?php

namespace modules\countries\common\migrations;

use yii\db\Migration;

class m170427_091604_create_cities_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'code' => $this->string(5),
            'name_en' => $this->string(),
            'name_ru' => $this->string(),
            'name_kz' => $this->string(),
        ]);

        $this->addForeignKey('fk-table_country-country_id', 'cities', 'country_id', 'countries', 'id', 'CASCADE');
        $this->createIndex('idx_code_city', 'cities', 'code');
        $this->createIndex('idx_name_en_cities', 'cities', 'name_en');
        $this->createIndex('idx_name_ru_cities', 'cities', 'name_ru');
        $this->createIndex('idx_name_kz_cities', 'cities', 'name_kz');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx_name_en_cities', 'cities');
        $this->dropIndex('idx_name_ru_cities', 'cities');
        $this->dropIndex('idx_name_kz_cities', 'cities');

        $this->dropForeignKey('fk-table_country-country_id', 'cities');

        $this->dropTable('cities');
    }
}
