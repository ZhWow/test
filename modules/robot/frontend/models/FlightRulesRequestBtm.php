<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 12.10.2017
 * Time: 11:37
 */

namespace modules\robot\frontend\models;
use modules\robot\frontend\providers\sirena\components\XmlCreater;


class FlightRulesRequestBtm
{
    public $request;
    public $session_id;
    public $pult_type;
    public $pult_name;
    public $variant_id;
    public $rule;
    public $code;
    public $company;
    public $lang;
    public $upt;




    public static function parseRequest($post)
    {
        $parsePost = [];

        $parsePost = json_decode(urldecode($post['json']), true);
        $parsePost['request'] = $post['request'];
        $parsePost['session_id'] = $post['session_id'];
        $parsePost['pult_type'] = $post['pult_type'];
        $parsePost['pult_name'] = $post['pult_name'];
        $parsePost['variant_id'] = $post['variant_id'];
        $xmlString = '';
        $model = new self();
        $model->upt = $parsePost['UPT'];
        $model->lang = $parsePost['lang'];
        $model->code = $parsePost['code'];
        $model->company = $parsePost['company'];
//        $model->attributes = $parsePost;

        $xmlString = XmlCreater::getXmlFlightRules($model);

//        $xmlString = XmlCreater::getXmlSearchFlight($model);
//        $xmlString = XmlCreater::getAirports();
//        $xmlString = XmlCreater::getXmlSearchFlightMinPrice($model);


        return $xmlString;
    }
}