<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 04.10.2017
 * Time: 18:09
 */

namespace modules\robot\frontend\models\base;

use Yii;
use yii\db\ActiveRecord;

class Cities extends ActiveRecord
{
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['code', 'code_ru', 'name_ru', 'name_en'], 'string', 'max' => 50],
        ];
    }

}