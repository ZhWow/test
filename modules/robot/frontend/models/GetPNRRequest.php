<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 01.11.2017
 * Time: 14:20
 */

namespace modules\robot\frontend\models;
use modules\robot\frontend\providers\sirena\components\XmlCreater;


class GetPNRRequest
{
    public $request;
    public $surname;
    public $PNR;

    public static function parseRequest($post)
    {
        $parsePost = [];
        $xmlString = '';
        $model = new self();
        $parsePost = json_decode(urldecode($post['json']), true);
        $parsePost['request'] = $post['request'];

        $model->request = $post['request'];
        $model->surname = $parsePost['lastname'];
        $model->PNR = $parsePost['PNR'];

        $xmlString = XmlCreater::getXmlGetPNR($model);

        return $xmlString;
    }

    public static function parseRequestObject($post)
    {
        $model = new self();
        $model->request = $post->request;
        $model->surname = $post->lastname;
        $model->PNR = $post->PNR;

        $xmlString = XmlCreater::getXmlGetPNR($model);

        return $xmlString;
    }
}