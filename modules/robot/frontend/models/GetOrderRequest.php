<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 02.11.2017
 * Time: 17:36
 */

namespace modules\robot\frontend\models;
use modules\robot\frontend\providers\sirena\components\XmlCreater;


class GetOrderRequest
{
    public $request;
    public $surname;
    public $PNR;
    public $action;
    public $form_pay = 'НА';
    public $cost = 0;

    public static function parseRequest($post)
    {
        $parsePost = [];
        $xmlString = '';
        $model = new self();
        $parsePost = json_decode(urldecode($post['json']), true);
        $model->surname = $parsePost['lastname'];
        $model->PNR = $parsePost['PNR'];

        $model->request = $post['request'];
        $model->action = ($post['action']) ? 'confirm' : 'query';
        $model->cost = ($post['cost']) ? $post['cost'] : 0;

        $xmlString = XmlCreater::getXmlGetOrder($model);

        return $xmlString;
    }
}