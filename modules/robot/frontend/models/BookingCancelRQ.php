<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 31.10.2017
 * Time: 16:39
 */

namespace modules\robot\frontend\models;
use modules\robot\frontend\providers\sirena\components\XmlCreater;


class BookingCancelRQ
{
    public $request;
    public $surname;
    public $PNR;

    public static function parseRequest($post)
    {
        $parsePost = [];
        $xmlString = '';
        $model = new self();
        $parsePost = json_decode(urldecode($post['json']), true);
        $parsePost['request'] = $post['request'];

        $model->request = $post['request'];
        $model->surname = $parsePost['lastname'];
        $model->PNR = $parsePost['PNR'];

        $xmlString = XmlCreater::getXmlBookingCancel($model);

        return $xmlString;
    }

}