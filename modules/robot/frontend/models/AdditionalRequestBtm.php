<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 20.10.2017
 * Time: 14:22
 */

namespace modules\robot\frontend\models;
use modules\robot\frontend\providers\sirena\components\XmlCreater;


class AdditionalRequestBtm
{
    public static function parseRequest($post)
    {

        $parsePost = json_decode(urldecode($post['json']), true);
        $xmlString = XmlCreater::getXmlMonoBrand($parsePost);
        return $xmlString;
    }
}