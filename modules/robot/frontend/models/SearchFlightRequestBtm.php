<?php
/**
 * Created by PhpStorm.
 * User: Qway
 * Date: 04.10.2017
 * Time: 11:05
 */

namespace modules\robot\frontend\models;

use modules\robot\frontend\providers\sirena\methods\DataForSirena;
use modules\robot\frontend\providers\sirena\components\XmlCreater;
use yii\base\Model;

class SearchFlightRequestBtm extends Model
{
    public $request;
    public $depair;
    public $arrair;
    public $depdate;
    public $servicetype;
    public $adults;
    public $children;
    public $infants;
    public $DirectFlightsOnly;

    public function rules()
    {
        return [
            [['request', 'servicetype'], 'string', 'max' => 128],
            [['depair', 'arrair', 'depdate'], 'each', 'rule' => ['string']],
            [['adults', 'children', 'infants'], 'integer'],
            [['DirectFlightsOnly'], 'boolean'],
        ];
    }

    public static function parseRequest($post)
    {
        $parsePost = [];

        $parsePost = json_decode(urldecode($post['json']), true);
        $parsePost['request'] = $post['request'];
        $parsePost['user_id'] = $post['user_id'];
        $xmlString = '';
        $model = new self();
        $model->attributes = $parsePost;

        #$model = DataForSirena::getCities($model);
        #$model = DataForSirena::getClass($model);


        $xmlString = XmlCreater::getXmlPricing($model);

//        $xmlString = XmlCreater::getXmlSearchFlight($model);
//        $xmlString = XmlCreater::getAirports();
//        $xmlString = XmlCreater::getXmlSearchFlightMinPrice($model);


        return $xmlString;
    }
}