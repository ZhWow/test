<?php
return [
    'params' => [
        'auth' => [
            'address' => '194.84.25.50',
            'client_id' => 1833,
            'port' => 34323,
        ],
        'epowerPults' => [
            'wstransavia' => [
                'EpowerServiceURL' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx',
                'WSUserName' => 'WstravelBusinessKZ1',
                'WSPassword' => 'G1Ladm1N*!!@Best2017',
                'SubSiteCode' => 'Btmckz',
                'ControlNumberValue' => '123787555',
            ],
            'wssaryarka2' => [
                'EpowerServiceURL' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx',
                'WSUserName' => 'WstravelBusinessKZ1',
                'WSPassword' => 'G1Ladm1N*!!@Best2017',
                'SubSiteCode' => 'airportkgf',
                'ControlNumberValue' => '123787555',
            ],
            'wssaryarka' => [
                'EpowerServiceURL' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx',
                'WSUserName' => 'WstravelBusinessKZ1',
                'WSPassword' => 'G1Ladm1N*!!@Best2017',
                'SubSiteCode' => 'Saryarka',
                'ControlNumberValue' => '123787555',
            ],
            'wstriumph' => [
                'EpowerServiceURL' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx',
                'WSUserName' => 'WstravelBusinessKZ1',
                'WSPassword' => 'G1Ladm1N*!!@Best2017',
                'SubSiteCode' => 'TSEKZ28AO',
                'ControlNumberValue' => '123787555',
            ],
            'wsitmos' => [
                'EpowerServiceURL' => 'https://staging-ws.epower.amadeus.com/wsbusinesstravelkz/EpowerService.asmx',
                'WSUserName' => 'WstravelBusinessKZ1',
                'WSPassword' => 'G1Ladm1N*!!@Best2017',
                'SubSiteCode' => 'MOWR228TB',
                'ControlNumberValue' => '123787555',
            ],
        ],
        'soapSettings' => [
            'epowerContext' => [
                'ssl' => [
                    'allow_self_signed' => TRUE,
                    'verify_peer' => FALSE,
                    'ciphers' => "SHA1",
                ],
                'https' => [
                    'curl_verify_ssl_peer' => FALSE,
                    'curl_verify_ssl_host' => FALSE,
                ]
            ],
            'epowerOptions' => [
                ''
            ]
        ]
    ]
];
