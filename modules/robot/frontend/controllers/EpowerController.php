<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 10.11.2017
 * Time: 21:32
 */

namespace modules\robot\frontend\controllers;

use modules\robot\frontend\providers\epower\methods\BookFlight;
use modules\robot\frontend\providers\epower\methods\BookFlightWithRecommendation;
use modules\robot\frontend\providers\epower\methods\Cancel;
use modules\robot\frontend\providers\epower\methods\GetFlightFareFamilies;
use modules\robot\frontend\providers\epower\methods\GetFlightRules;
use modules\robot\frontend\providers\epower\methods\GetPNR;
use modules\robot\frontend\providers\epower\methods\Ping;
use modules\robot\frontend\providers\epower\methods\SearchFlight;
use modules\robot\frontend\providers\epower\methods\SignOut;
use modules\robot\frontend\providers\epower\models\EPSession;
use modules\robot\frontend\providers\epower\services\EPConnector;
use modules\robot\frontend\providers\epower\services\EPCredentials;
use modules\robot\frontend\providers\epower\models\BTMRequest;
use modules\robot\frontend\providers\epower\services\EPHelper;
use Yii;
use yii\caching\MemCache;
use yii\helpers\Url;
use yii\web\Controller;

class EpowerController extends Controller
{

    public function actionIndex()
    {
        $post = Yii::$app->request->post();

//        $post["request"] = "FlightSearch";
//        $post["json"] = "%7B%22depair%22%3A%22TSE%22%2C%22arrair%22%3A%22ALA%22%2C%22depdate%22%3A%2230.11.2017%22%2C%22flights%22%3A%22%22%2C%22sub_classes%22%3A%22%22%2C%22servicetype%22%3A%22Economy%22%2C%22adults%22%3A%221%22%2C%22infants%22%3A%220%22%2C%22children%22%3A%220%22%2C%22pensioners%22%3A%220%22%2C%22isOWC%22%3A%221%22%2C%22price%22%3A%22%22%2C%22calendar%22%3A0%2C%22DirectFlightsOnly%22%3Atrue%7D";
//        $post["user_id"] = "ezg0QzE1OTA2LTUxRDEtNDk0MC05NURDLThCN0M2ODYwNEJBNn0";
//        $post["pult_name"] = "wstransavia";

        $BTMRequest = new BTMRequest();
        $BTMRequest->setAttributes($post);

//        $variant_id = EPHelper::getVariantID("123","546");
//        $variant_id_res = EPHelper::getRecommendation($variant_id);

//        $file = file_get_contents( "C:/wamp64/www/qway.kz/modules/robot/frontend/providers/epower/services/SearchFlightResponse.xml");
//        $file = file_get_contents( Yii::getAlias('@modules')."/robot/frontend/providers/epower/services/BookFlightResponse.xml");
//        $BookFlightResponse = simplexml_load_string($file);
//        EPHelper::parseBookFlightBTM($BookFlightResponse);
//
//        EPHelper::parseSearchFlightBTM($SearchFlightResponse);

//        $file = file_get_contents( Yii::getAlias('@modules')."/robot/frontend/providers/epower/services/GetFlightRulesResponse.xml");
//        $response = EPHelper::prepareFlightRulesBTM($file);
//        EPHelper::parseFlightRulesBTM($response);
        //        $file = file_get_contents( Yii::getAlias('@modules')."/robot/frontend/providers/epower/services/GetPNRResponse.xml");
//        $data = EPHelper::prepareObjectGetPNRRS($file);
//        EPHelper::parseGetPNRBTM($data);
        $PultType = Yii::$app->request->get('type');
        $EPSession = EPSession::checkSessionByUID($BTMRequest, $PultType);
        if ( $BTMRequest->validate() ) {
            $EPCredentials = new EPCredentials($BTMRequest->pult_name, $BTMRequest->user_id, $EPSession, $PultType);
            if ( $EPCredentials === false ) {
                return false;
            }
//            $Ping = new Ping($EPCredentials);
//            $PingResponse = $Ping->run();

            switch ($BTMRequest->request) {
                case "FlightSearch" :
                    $SearchFlight = new SearchFlight($EPCredentials);
                    $SearchFlightResponse = $SearchFlight->run($BTMRequest);
                    $SearchFlightRSParsed = EPHelper::SearchFlightFinish( EPHelper::parseSearchFlightBTM($SearchFlightResponse), [
                        "PultName" => $EPCredentials->PultName,
                        "PultType" => $PultType,
                        "SessionId" => $EPCredentials->SessionID,
                    ] );
                    echo $SearchFlightRSParsed;
                    break;
                case "FlightRules" :
                    $GetFlightRules = new GetFlightRules($EPCredentials);
                    $GetFlightRulesResponse = $GetFlightRules->run($BTMRequest);
                    echo $GetFlightRulesResponse;
                    break;
                case "AnciliaryServices" :
                    $asd = '123';
                    break;
                case "FlightBaggage" :
                    $GetFlightFareFamilies = new GetFlightFareFamilies($EPCredentials);
                    $GetFlightFareFamiliesResponse = $GetFlightFareFamilies->run($BTMRequest);
                    echo $GetFlightFareFamiliesResponse;
                    break;
                case "FlightBooking" :
                    $BookFlight = new BookFlight($EPCredentials);
                    $BookFlightResponse = $BookFlight->run($BTMRequest);
                    $SignOut = new SignOut($EPCredentials, $BookFlight->SessionID);
                    echo $BookFlightResponse;
                    break;
                case "GetPNR" :
                    $GetPNR = new GetPNR($EPCredentials);
                    $GetPNRResponse = $GetPNR->run($BTMRequest);
                    $SignOut = new SignOut($EPCredentials, $GetPNR->SessionID);
                    echo $GetPNRResponse;
                    break;
                case "FlightBookingCancel" :
                    $Cancel = new Cancel($EPCredentials);
                    $CancelResponse = $Cancel->run($BTMRequest);
                    $SignOut = new SignOut($EPCredentials, $Cancel->SessionID);
                    echo $CancelResponse;
                    break;
            }
        }
    }

    public function beforeAction($action)
    {
        if ($action->id === 'index') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}