<?php

namespace modules\robot\frontend;

use yii\helpers\ArrayHelper;

/**
 * help module definition class
 */
class Module extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'modules\robot\frontend\controllers';

    /**
     * @inheritdoc
     */
    public $defaultRoute = 'robot';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
        parent::init();
        $localConfig = '/config-local.php';
        if (file_exists(__DIR__ . $localConfig)) {
            \Yii::configure($this, ArrayHelper::merge(
                require(__DIR__ . '/config.php'),
                require(__DIR__ . $localConfig)
            ));
        } else {
            \Yii::configure($this, require(__DIR__ . '/config.php'));
        }
    }
}
