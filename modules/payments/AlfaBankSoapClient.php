<?php

namespace modules\payments;

use SoapClient;
use SOAPHeader;
use SoapVar;

/**
 * Class AlfaBankSoapClient
 * @package modules\payments
 */
class AlfaBankSoapClient extends SoapClient
{
    const WS_USER_NAME = 'qway_kz-api';
    const WS_PASSWORD = 'qway_kz';
    const WSDL = '';
    const SOAP_HEADER = '';
    const RETURN_URL = '';

   public static function getResult($data)
   {
       $soapClient = new self();
       return $soapClient->__call('registerOrder', $data);
   }

    public function __construct()
    {
        parent::__construct(self::WSDL);
    }

    private function generateWSSecurityHeader() {
        $xml = '
            <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <wsse:UsernameToken>
                    <wsse:Username>' . self::WS_USER_NAME . '</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' . self::WS_PASSWORD . '</wsse:Password>
                    <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">' . sha1(mt_rand()) . '</wsse:Nonce>
                </wsse:UsernameToken>
            </wsse:Security>';

        return new SoapHeader(self::SOAP_HEADER, 'Security', new SoapVar($xml, XSD_ANYXML), true);
    }

    /**
     * @param string $method
     * @param array $data
     * @return mixed
     */
    public function __call($method, $data) {

        $this->__setSoapHeaders($this->generateWSSecurityHeader()); // Устанавливаем заголовок для авторизации
        /* @noinspection PhpParamsInspection */
        return parent::__call($method, $data); // Возвращаем результат метода SoapClient::__call()

    }

}