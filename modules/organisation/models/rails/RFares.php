<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "r_fares".
 *
 * @property integer $id
 * @property integer $fareId
 * @property integer $netFare
 * @property integer $markup
 * @property integer $resellerMarkup
 * @property string $currency
 * @property string $prodMarketingName
 * @property string $tariffCode
 */
class RFares extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_fares';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fareId', 'netFare', 'markup', 'resellerMarkup'], 'integer'],
            [['currency', 'prodMarketingName', 'tariffCode'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fareId' => 'Fare ID',
            'netFare' => 'Net Fare',
            'markup' => 'Markup',
            'resellerMarkup' => 'Reseller Markup',
            'currency' => 'Currency',
            'prodMarketingName' => 'Prod Marketing Name',
            'tariffCode' => 'Tariff Code',
        ];
    }
}
