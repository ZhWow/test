<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 17.10.2017
 * Time: 12:53
 */

namespace modules\organisation\models\rails;


use modules\organisation\models\BaseModel;

class RSegments extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_segments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookingId', 'operatorName', 'vendorName'], 'string'],
        ];
    }
}