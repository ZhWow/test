<?php
/**
 * Created by PhpStorm.
 * User: Karim
 * Date: 17.10.2017
 * Time: 12:33
 */

namespace modules\organisation\models\rails;

use modules\organisation\models\BaseModel;

class RBooking extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookingId', 'operatorBookingId', 'createDate', 'createTime'], 'string'],
        ];
    }

}