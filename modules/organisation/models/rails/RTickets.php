<?php

namespace modules\organisation\models\rails;

use Yii;
use backend\models\Corp;
use modules\organisation\models\TUsers;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "r_tickets".
 *
 * @property integer $id
 * @property integer $isCancelled
 * @property integer $userID
 * @property integer $corpID
 * @property integer $ticketId
 * @property integer $bookingId
 * @property integer $ticketSupplierId
 * @property integer $passengerId
 * @property integer $fareId
 * @property string $ticketType
 * @property string $coupons
 */
class RTickets extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isCancelled', 'userID', 'corpID', 'ticketId', 'bookingId', 'ticketSupplierId', 'passengerId', 'fareId'], 'integer'],
            [['ticketType', 'coupons'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isCancelled' => 'Is Cancelled',
            'userID' => 'User ID',
            'corpID' => 'Corp ID',
            'ticketId' => 'Ticket ID',
            'bookingId' => 'Booking ID',
            'ticketSupplierId' => 'Ticket Supplier ID',
            'passengerId' => 'Passenger ID',
            'fareId' => 'Fare ID',
            'ticketType' => 'Ticket Type',
            'coupons' => 'Coupons',
        ];
    }

    public function getOwner()
    {
        return $this->hasOne(TUsers::relation(static::$db), ['id' => 'userID']);
    }

    public function getOrganisation()
    {
        return $this->hasOne(Corp::className(), ['id' => 'corpID']);
    }

    public function getJourney()
    {
        return $this->hasOne(RJourneys::relation(static::$db), ['bookingId' => 'bookingId']);
    }

    public function getPassenger()
    {
        return $this->hasOne(RPassengers::relation(static::$db), ['passengerId' => 'passengerId']);
    }

    public function getSeat()
    {
        return $this->hasOne(RSeats::relation(static::$db), ['ticketId' => 'ticketId']);
    }

    public function getStation()
    {
        return $this->hasOne(RStations::relation(static::$db), ['bookingId' => 'bookingId']);
    }

    public function getFare()
    {
        return $this->hasOne(RFares::relation(static::$db), ['fareId' => 'fareId']);
    }

    public function getBooking()
    {
        return $this->hasOne(RBooking::relation(static::$db),['bookingId' => 'bookingId']);
    }

    public function getSegments()
    {
        return $this->hasMany(RSegments::relation(static::$db),['bookingId' => 'bookingId']);
    }

    public function getRefund()
    {
        return $this->hasOne(RFares::relation(static::$db),['fareId' => 'fareId'])
            ->viaTable('r_refundtickets',['ticketId'=>'ticketId']);
    }
}
