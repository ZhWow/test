<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;


/**
 * This is the model class for table "r_refundtickets".
 *
 * Database from is required
 *
 * @property integer $id
 * @property integer $refundId
 * @property integer $refundTicketId
 * @property integer $ticketId
 * @property integer $passengerId
 * @property integer $fareId
 * @property integer $coupons
 */
class RRefundtickets extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_refundtickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['refundId', 'refundTicketId', 'ticketId', 'passengerId', 'fareId', 'coupons'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'refundId' => 'Refund ID',
            'refundTicketId' => 'Refund Ticket ID',
            'ticketId' => 'Ticket ID',
            'passengerId' => 'Passenger ID',
            'fareId' => 'Fare ID',
            'coupons' => 'Coupons',
        ];
    }
}
