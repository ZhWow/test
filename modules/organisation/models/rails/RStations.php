<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "{{%r_stations}}".
 *
 * @property integer $id
 * @property integer $bookingId
 * @property integer $journeySegmentId
 * @property string $stationCode
 * @property string $stationName
 * @property string $stationCountry
 * @property string $stationArrivalTime
 * @property string $stationDepartureTime
 * @property string $stationStayTime
 */
class RStations extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_stations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookingId', 'journeySegmentId'], 'integer'],
            [['stationCode', 'stationName', 'stationCountry', 'stationArrivalTime', 'stationDepartureTime', 'stationStayTime'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bookingId' => 'Booking ID',
            'journeySegmentId' => 'Journey Segment ID',
            'stationCode' => 'Station Code',
            'stationName' => 'Station Name',
            'stationCountry' => 'Station Country',
            'stationArrivalTime' => 'Station Arrival Time',
            'stationDepartureTime' => 'Station Departure Time',
            'stationStayTime' => 'Station Stay Time',
        ];
    }
}
