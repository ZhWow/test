<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "r_seats".
 *
 * @property integer $id
 * @property integer $bookingId
 * @property integer $ticketId
 * @property integer $journeySegmentId
 * @property string $placeNumber
 * @property string $carNumber
 * @property string $deck
 * @property string $serviceClass
 * @property string $serviceType
 * @property string $placeType
 * @property string $orientation
 * @property string $genderCoupe
 */
class RSeats extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_seats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookingId', 'ticketId', 'journeySegmentId'], 'integer'],
            [['placeNumber', 'carNumber', 'deck', 'serviceClass', 'serviceType', 'placeType', 'orientation', 'genderCoupe'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bookingId' => 'Booking ID',
            'ticketId' => 'Ticket ID',
            'journeySegmentId' => 'Journey Segment ID',
            'placeNumber' => 'Place Number',
            'carNumber' => 'Car Number',
            'deck' => 'Deck',
            'serviceClass' => 'Service Class',
            'serviceType' => 'Service Type',
            'placeType' => 'Place Type',
            'orientation' => 'Orientation',
            'genderCoupe' => 'Gender Coupe',
        ];
    }
}
