<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "r_journeys".
 *
 * @property integer $id
 * @property integer $bookingId
 * @property string $departureCode
 * @property string $departureName
 * @property string $departureCountry
 * @property string $departureDate
 * @property string $departureTime
 * @property string $arrivalCode
 * @property string $arrivalName
 * @property string $arrivalCountry
 * @property string $arrivalDate
 * @property string $arrivalTime
 * @property string $duration
 * @property string $faresAvailable
 * @property string $fareOfferType
 * @property string $netFare
 * @property string $markup
 * @property string $resellerMarkup
 * @property string $currency
 * @property string $ticketOptions
 * @property string $categories
 */
class RJourneys extends BaseModel
{
    public static $db;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_journeys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookingId'], 'integer'],
            [['departureCode', 'departureName', 'departureCountry', 'departureDate', 'departureTime', 'arrivalCode', 'arrivalName', 'arrivalCountry', 'arrivalDate', 'arrivalTime', 'duration', 'faresAvailable', 'fareOfferType', 'netFare', 'markup', 'resellerMarkup', 'currency', 'ticketOptions', 'categories'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bookingId' => 'Booking ID',
            'departureCode' => 'Departure Code',
            'departureName' => 'Departure Name',
            'departureCountry' => 'Departure Country',
            'departureDate' => 'Departure Date',
            'departureTime' => 'Departure Time',
            'arrivalCode' => 'Arrival Code',
            'arrivalName' => 'Arrival Name',
            'arrivalCountry' => 'Arrival Country',
            'arrivalDate' => 'Arrival Date',
            'arrivalTime' => 'Arrival Time',
            'duration' => 'Duration',
            'faresAvailable' => 'Fares Available',
            'fareOfferType' => 'Fare Offer Type',
            'netFare' => 'Net Fare',
            'markup' => 'Markup',
            'resellerMarkup' => 'Reseller Markup',
            'currency' => 'Currency',
            'ticketOptions' => 'Ticket Options',
            'categories' => 'Categories',
        ];
    }
}
