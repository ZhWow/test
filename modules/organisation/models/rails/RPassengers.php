<?php

namespace modules\organisation\models\rails;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "r_passengers".
 *
 * @property integer $id
 * @property integer $passengerId
 * @property string $salutation
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $documentType
 * @property string $documentNumber
 * @property string $documentExpireDate
 * @property string $birthDate
 * @property string $nationality
 * @property string $residence
 * @property string $email
 * @property string $card
 * @property string $additionalOptions
 */
class RPassengers extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_passengers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passengerId'], 'integer'],
            [['salutation'], 'string', 'max' => 50],
            [['name', 'surname', 'patronymic', 'documentType', 'documentNumber', 'documentExpireDate', 'birthDate', 'nationality', 'residence', 'email', 'card', 'additionalOptions'], 'string', 'max' => 100],
            [['passengerId'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passengerId' => 'Passenger ID',
            'salutation' => 'Salutation',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'documentType' => 'Document Type',
            'documentNumber' => 'Document Number',
            'documentExpireDate' => 'Document Expire Date',
            'birthDate' => 'Birth Date',
            'nationality' => 'Nationality',
            'residence' => 'Residence',
            'email' => 'Email',
            'card' => 'Card',
            'additionalOptions' => 'Additional Options',
        ];
    }
}
