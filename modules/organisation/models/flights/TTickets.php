<?php

namespace modules\organisation\models\flights;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "t_electronictickets".
 *
 * @property integer $id
 * @property string $eTicketNumber
 * @property integer $state
 * @property integer $to_buy
 * @property integer $to_cancel
 * @property integer $approver
 * @property string $approvedDate
 * @property string $PassengerTypeCode
 * @property string $NamePrefix
 * @property string $GivenName
 * @property string $Surname
 * @property string $BirthDate
 * @property string $PNRNo
 * @property string $IATAID
 * @property string $TicketingDate
 * @property string $FlightNumber
 * @property string $ResBookDesigCode
 * @property string $Status
 * @property string $Equipment
 * @property string $OperatingAirline
 * @property string $ValidatingAirline
 * @property string $DepartureAirport
 * @property string $DepartureTerminal
 * @property string $ArrivalAirport
 * @property string $ArrivalTerminal
 * @property string $DepartureDateTime
 * @property string $ArrivalDateTime
 * @property string $ElapsedTime
 * @property string $BaggageWeight
 * @property string $BaggageWeightMeasureUnit
 * @property string $FareBasis
 * @property string $FareCalculation
 * @property string $BaseFare
 * @property string $Tax
 * @property string $ServiceFee
 * @property string $TotalFare
 * @property string $GrandTotalFare
 * @property string $cheapest_price
 * @property string $VAT
 * @property string $Telephone
 * @property string $Email
 * @property string $DocumentID
 * @property string $DocumentType
 * @property string $DocumentExpDate
 * @property string $DocumentCitizenship
 * @property integer $t_bookID
 * @property integer $agencyID
 * @property integer $id_user
 * @property integer $corpId
 * @property integer $t_departments
 * @property integer $days_before_dep
 * @property integer $flight_type
 * @property string $penalty
 * @property string $reason
 * @property string $iin
 * @property string $approve_time
 * @property string $BaggageQuantity
 * @property string $MarketingAirline
 * @property string $rejection_reason
 * @property integer $segmentIndex
 * @property integer $ISSUE
 * @property integer $REFUND
 * @property integer $REISSUE
 * @property integer $VOID
 * @property integer $UNDEFINED
 * @property integer $XMLID
 * @property integer $EXCHANGE
 * @property integer $REVALIDATION
 * @property integer $isPersonal
 * @property integer $formOfPayment
 * @property string $FO_NUMBER
 */
class TTickets extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_electronictickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'to_buy', 'to_cancel', 'approver', 't_bookID', 'agencyID', 'id_user', 'corpId', 't_departments', 'days_before_dep', 'flight_type', 'segmentIndex', 'ISSUE', 'REFUND', 'REISSUE', 'VOID', 'UNDEFINED', 'XMLID', 'EXCHANGE', 'REVALIDATION', 'isPersonal', 'formOfPayment'], 'integer'],
            [['eTicketNumber', 'approvedDate', 'rejection_reason'], 'string', 'max' => 255],
            [['PassengerTypeCode', 'NamePrefix', 'GivenName', 'Surname', 'BirthDate', 'PNRNo', 'IATAID', 'TicketingDate', 'FlightNumber', 'ResBookDesigCode', 'Status', 'Equipment', 'OperatingAirline', 'ValidatingAirline', 'DepartureAirport', 'DepartureTerminal', 'ArrivalAirport', 'ArrivalTerminal', 'DepartureDateTime', 'ArrivalDateTime', 'ElapsedTime', 'BaggageWeight', 'BaggageWeightMeasureUnit', 'FareBasis', 'FareCalculation', 'BaseFare', 'Tax', 'ServiceFee', 'TotalFare', 'GrandTotalFare', 'cheapest_price', 'VAT', 'Telephone', 'Email', 'DocumentID', 'DocumentType', 'DocumentExpDate', 'DocumentCitizenship', 'penalty'], 'string', 'max' => 300],
            [['reason', 'BaggageQuantity', 'MarketingAirline'], 'string', 'max' => 50],
            [['iin'], 'string', 'max' => 200],
            [['approve_time'], 'string', 'max' => 150],
            [['FO_NUMBER'], 'string', 'max' => 20],
            [['eTicketNumber'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eTicketNumber' => 'E Ticket Number',
            'state' => 'State',
            'to_buy' => 'To Buy',
            'to_cancel' => 'To Cancel',
            'approver' => 'Approver',
            'approvedDate' => 'Approved Date',
            'PassengerTypeCode' => 'Passenger Type Code',
            'NamePrefix' => 'Name Prefix',
            'GivenName' => 'Given Name',
            'Surname' => 'Surname',
            'BirthDate' => 'Birth Date',
            'PNRNo' => 'Pnrno',
            'IATAID' => 'Iataid',
            'TicketingDate' => 'Ticketing Date',
            'FlightNumber' => 'Flight Number',
            'ResBookDesigCode' => 'Res Book Desig Code',
            'Status' => 'Status',
            'Equipment' => 'Equipment',
            'OperatingAirline' => 'Operating Airline',
            'ValidatingAirline' => 'Validating Airline',
            'DepartureAirport' => 'Departure Airport',
            'DepartureTerminal' => 'Departure Terminal',
            'ArrivalAirport' => 'Arrival Airport',
            'ArrivalTerminal' => 'Arrival Terminal',
            'DepartureDateTime' => 'Departure Date Time',
            'ArrivalDateTime' => 'Arrival Date Time',
            'ElapsedTime' => 'Elapsed Time',
            'BaggageWeight' => 'Baggage Weight',
            'BaggageWeightMeasureUnit' => 'Baggage Weight Measure Unit',
            'FareBasis' => 'Fare Basis',
            'FareCalculation' => 'Fare Calculation',
            'BaseFare' => 'Base Fare',
            'Tax' => 'Tax',
            'ServiceFee' => 'Service Fee',
            'TotalFare' => 'Total Fare',
            'GrandTotalFare' => 'Grand Total Fare',
            'cheapest_price' => 'Cheapest Price',
            'VAT' => 'Vat',
            'Telephone' => 'Telephone',
            'Email' => 'Email',
            'DocumentID' => 'Document ID',
            'DocumentType' => 'Document Type',
            'DocumentExpDate' => 'Document Exp Date',
            'DocumentCitizenship' => 'Document Citizenship',
            't_bookID' => 'T Book ID',
            'agencyID' => 'Agency ID',
            'id_user' => 'Id User',
            'corpId' => 'Corp ID',
            't_departments' => 'T Departments',
            'days_before_dep' => 'Days Before Dep',
            'flight_type' => 'Flight Type',
            'penalty' => 'Penalty',
            'reason' => 'Reason',
            'iin' => 'Iin',
            'approve_time' => 'Approve Time',
            'BaggageQuantity' => 'Baggage Quantity',
            'MarketingAirline' => 'Marketing Airline',
            'rejection_reason' => 'Rejection Reason',
            'segmentIndex' => 'Segment Index',
            'ISSUE' => 'Issue',
            'REFUND' => 'Refund',
            'REISSUE' => 'Reissue',
            'VOID' => 'Void',
            'UNDEFINED' => 'Undefined',
            'XMLID' => 'Xmlid',
            'EXCHANGE' => 'Exchange',
            'REVALIDATION' => 'Revalidation',
            'isPersonal' => 'Is Personal',
            'formOfPayment' => 'Form Of Payment',
            'FO_NUMBER' => 'Fo  Number',
        ];
    }
}
