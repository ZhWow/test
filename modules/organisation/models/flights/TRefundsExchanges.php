<?php

namespace modules\organisation\models\flights;

use Yii;

/**
 * This is the model class for table "t_refunds_exchanges".
 *
 * @property integer $id
 * @property integer $eTicketId
 * @property string $eTicketNumber
 * @property string $DepAirport
 * @property string $ArAirport
 * @property integer $isChecked
 * @property integer $isExchange
 * @property string $oldDepAirport
 * @property string $oldArAirport
 * @property integer $agencyID
 * @property integer $requestID
 * @property string $agency_variant_name
 * @property string $newDepAirport
 * @property string $newArAirport
 * @property string $newDepTime
 * @property string $newArTime
 * @property integer $priceOfNewticket
 * @property string $segmentIndex
 * @property string $segment
 * @property string $newDepDate
 * @property string $newArDate
 * @property string $newFlightNumber
 * @property string $newClass
 * @property string $newEquipment
 * @property string $newAirline
 * @property integer $formOfPayment
 */
class TRefundsExchanges extends \modules\organisation\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_refunds_exchanges';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eTicketId', 'isChecked', 'isExchange', 'agencyID', 'requestID', 'priceOfNewticket', 'formOfPayment'], 'integer'],
            [['eTicketNumber', 'DepAirport', 'ArAirport', 'agency_variant_name'], 'string', 'max' => 100],
            [['oldDepAirport', 'oldArAirport', 'newDepAirport', 'newArAirport', 'newDepTime', 'newArTime', 'segmentIndex', 'segment', 'newDepDate', 'newArDate', 'newFlightNumber', 'newClass', 'newEquipment', 'newAirline'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eTicketId' => 'E Ticket ID',
            'eTicketNumber' => 'E Ticket Number',
            'DepAirport' => 'Dep Airport',
            'ArAirport' => 'Ar Airport',
            'isChecked' => 'Is Checked',
            'isExchange' => 'Is Exchange',
            'oldDepAirport' => 'Old Dep Airport',
            'oldArAirport' => 'Old Ar Airport',
            'agencyID' => 'Agency ID',
            'requestID' => 'Request ID',
            'agency_variant_name' => 'Agency Variant Name',
            'newDepAirport' => 'New Dep Airport',
            'newArAirport' => 'New Ar Airport',
            'newDepTime' => 'New Dep Time',
            'newArTime' => 'New Ar Time',
            'priceOfNewticket' => 'Price Of Newticket',
            'segmentIndex' => 'Segment Index',
            'segment' => 'Segment',
            'newDepDate' => 'New Dep Date',
            'newArDate' => 'New Ar Date',
            'newFlightNumber' => 'New Flight Number',
            'newClass' => 'New Class',
            'newEquipment' => 'New Equipment',
            'newAirline' => 'New Airline',
            'formOfPayment' => 'Form Of Payment',
        ];
    }
}
