<?php

namespace modules\organisation\models\flights;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "t_book".
 *
 * @property integer $id
 * @property string $message
 * @property integer $state
 * @property integer $to_buy
 * @property integer $to_cancel
 * @property integer $approverNext
 * @property integer $approverLast
 * @property string $PNR
 * @property string $passenger_emails
 * @property integer $send_me
 * @property string $UPNR
 * @property string $tickets_no
 * @property string $timelimit
 * @property string $price_all
 * @property string $detail_price
 * @property string $fares
 * @property string $fees
 * @property string $serviceFee
 * @property string $fine
 * @property string $cheapest_price
 * @property string $baggages
 * @property string $pultname
 * @property string $pultversion
 * @property string $exchangeRates
 * @property string $Surnames
 * @property string $Names
 * @property string $birthdates
 * @property string $sexs
 * @property string $states
 * @property string $doc_types
 * @property string $doc_nums
 * @property string $doc_exp_dates
 * @property string $PCCs
 * @property string $phones
 * @property string $email
 * @property string $subagent
 * @property string $position
 * @property string $agencyemail
 * @property string $agencyphone
 * @property string $depair
 * @property string $arrair
 * @property string $depdate
 * @property string $deptime
 * @property string $arrdate
 * @property string $arrtime
 * @property string $duration
 * @property string $json_booking
 * @property string $json_booking_response
 * @property string $companies
 * @property string $flights
 * @property string $sub_classes
 * @property string $servicetype
 * @property integer $adults
 * @property integer $children
 * @property integer $infants
 * @property integer $pensioners
 * @property string $price
 * @property string $currency
 * @property string $json_ticketing
 * @property string $json_ticketing_response
 * @property string $fare_rules
 * @property string $pnr_remarks
 * @property string $currentDate
 * @property integer $id_user
 * @property string $buying_date
 * @property string $marshruts
 * @property integer $balance
 * @property integer $corp
 * @property string $agency
 * @property integer $agency_id
 * @property string $bookdate
 * @property string $json_selected_agency
 * @property string $reference_number
 * @property string $cancel_free_text
 * @property string $val_company
 * @property string $reason
 * @property string $justification
 * @property string $policyBreakText
 * @property integer $t_departments
 * @property string $days_before_dep
 * @property integer $flight_type
 * @property integer $selfApprove
 * @property string $iins
 * @property string $attachment_url
 * @property integer $fixed
 * @property string $request_time
 * @property string $approve_time
 * @property string $max_price
 * @property integer $formOfPayment
 * @property string $book_id
 */
class TBook extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'json_booking', 'json_booking_response', 'json_ticketing', 'json_ticketing_response', 'fare_rules', 'pnr_remarks', 'marshruts', 'json_selected_agency', 'policyBreakText'], 'string'],
            [['state', 'to_buy', 'to_cancel', 'approverNext', 'approverLast', 'send_me', 'adults', 'children', 'infants', 'pensioners', 'id_user', 'balance', 'corp', 'agency_id', 't_departments', 'days_before_dep', 'flight_type', 'selfApprove', 'fixed', 'formOfPayment'], 'integer'],
            [['json_booking', 'json_booking_response', 'val_company'], 'required'],
            [['request_time', 'approve_time'], 'safe'],
            [['PNR', 'passenger_emails', 'UPNR', 'tickets_no', 'timelimit', 'price_all', 'detail_price', 'fares', 'fees', 'fine', 'cheapest_price', 'baggages', 'pultname', 'pultversion', 'exchangeRates', 'Surnames', 'Names', 'birthdates', 'doc_nums', 'doc_exp_dates', 'phones', 'email', 'subagent', 'position', 'agencyemail', 'agencyphone', 'price', 'currency', 'justification'], 'string', 'max' => 300],
            [['serviceFee', 'sexs', 'states', 'doc_types', 'PCCs', 'iins'], 'string', 'max' => 200],
            [['depair', 'arrair', 'companies', 'flights', 'sub_classes', 'servicetype', 'buying_date', 'agency', 'bookdate', 'reference_number'], 'string', 'max' => 100],
            [['depdate', 'deptime', 'arrdate', 'arrtime', 'duration'], 'string', 'max' => 150],
            [['currentDate', 'reason', 'max_price', 'book_id'], 'string', 'max' => 50],
            [['cancel_free_text'], 'string', 'max' => 250],
            [['val_company'], 'string', 'max' => 10],
            [['attachment_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'state' => 'State',
            'to_buy' => 'To Buy',
            'to_cancel' => 'To Cancel',
            'approverNext' => 'Approver Next',
            'approverLast' => 'Approver Last',
            'PNR' => 'Pnr',
            'passenger_emails' => 'Passenger Emails',
            'send_me' => 'Send Me',
            'UPNR' => 'Upnr',
            'tickets_no' => 'Tickets No',
            'timelimit' => 'Timelimit',
            'price_all' => 'Price All',
            'detail_price' => 'Detail Price',
            'fares' => 'Fares',
            'fees' => 'Fees',
            'serviceFee' => 'Service Fee',
            'fine' => 'Fine',
            'cheapest_price' => 'Cheapest Price',
            'baggages' => 'Baggages',
            'pultname' => 'Pultname',
            'pultversion' => 'Pultversion',
            'exchangeRates' => 'Exchange Rates',
            'Surnames' => 'Surnames',
            'Names' => 'Names',
            'birthdates' => 'Birthdates',
            'sexs' => 'Sexs',
            'states' => 'States',
            'doc_types' => 'Doc Types',
            'doc_nums' => 'Doc Nums',
            'doc_exp_dates' => 'Doc Exp Dates',
            'PCCs' => 'Pccs',
            'phones' => 'Phones',
            'email' => 'Email',
            'subagent' => 'Subagent',
            'position' => 'Position',
            'agencyemail' => 'Agencyemail',
            'agencyphone' => 'Agencyphone',
            'depair' => 'Depair',
            'arrair' => 'Arrair',
            'depdate' => 'Depdate',
            'deptime' => 'Deptime',
            'arrdate' => 'Arrdate',
            'arrtime' => 'Arrtime',
            'duration' => 'Duration',
            'json_booking' => 'Json Booking',
            'json_booking_response' => 'Json Booking Response',
            'companies' => 'Companies',
            'flights' => 'Flights',
            'sub_classes' => 'Sub Classes',
            'servicetype' => 'Servicetype',
            'adults' => 'Adults',
            'children' => 'Children',
            'infants' => 'Infants',
            'pensioners' => 'Pensioners',
            'price' => 'Price',
            'currency' => 'Currency',
            'json_ticketing' => 'Json Ticketing',
            'json_ticketing_response' => 'Json Ticketing Response',
            'fare_rules' => 'Fare Rules',
            'pnr_remarks' => 'Pnr Remarks',
            'currentDate' => 'Current Date',
            'id_user' => 'Id User',
            'buying_date' => 'Buying Date',
            'marshruts' => 'Marshruts',
            'balance' => 'Balance',
            'corp' => 'Corp',
            'agency' => 'Agency',
            'agency_id' => 'Agency ID',
            'bookdate' => 'Bookdate',
            'json_selected_agency' => 'Json Selected Agency',
            'reference_number' => 'Reference Number',
            'cancel_free_text' => 'Cancel Free Text',
            'val_company' => 'Val Company',
            'reason' => 'Reason',
            'justification' => 'Justification',
            'policyBreakText' => 'Policy Break Text',
            't_departments' => 'T Departments',
            'days_before_dep' => 'Days Before Dep',
            'flight_type' => 'Flight Type',
            'selfApprove' => 'Self Approve',
            'iins' => 'Iins',
            'attachment_url' => 'Attachment Url',
            'fixed' => 'Fixed',
            'request_time' => 'Request Time',
            'approve_time' => 'Approve Time',
            'max_price' => 'Max Price',
            'formOfPayment' => 'Form Of Payment',
            'book_id' => 'Book ID',
        ];
    }
}
