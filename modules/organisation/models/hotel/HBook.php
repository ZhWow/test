<?php

namespace modules\organisation\models\hotel;

use Yii;
use modules\organisation\models\BaseModel;

/**
 * This is the model class for table "h_book".
 *
 * @property integer $id
 * @property integer $stateID
 * @property integer $to_buy
 * @property integer $selfApprove
 * @property integer $approverNext
 * @property string $bookingReference
 * @property integer $roomNum
 * @property string $hotelName
 * @property string $cityCode
 * @property integer $stars
 * @property string $reservationDateTime
 * @property string $autoCancelDateTime
 * @property string $hotelPrice
 * @property string $tripDepartments
 * @property string $serviceFee
 * @property string $tripReasons
 * @property string $totalPrice
 * @property string $refundAmount
 * @property string $penaltyPrice
 * @property string $cheapestPrice
 * @property string $daysBeforeCheckin
 * @property integer $hotelID
 * @property integer $roomID
 * @property string $checkInDateTime
 * @property string $checkOutDateTime
 * @property string $discountAmount
 * @property string $discountPercent
 * @property string $discountCurrencyCode
 * @property string $comment
 * @property integer $corpID
 * @property integer $userID
 * @property string $paxTypes
 * @property string $paxDesigCodes
 * @property string $paxNames
 * @property string $paxSurnames
 * @property string $paxEmails
 * @property string $paxPhones
 * @property string $recommendationGUID
 * @property string $reservationRQJson
 * @property string $bookConfirmationRQJson
 * @property string $cancelRQJson
 * @property string $cancelConfirmationRQJson
 * @property string $roomTypeSentence
 * @property integer $ReservationIdContentinn
 * @property string $cityId
 * @property integer $netAmount
 * @property string $attachment_url
 * @property string $bookingStatus
 * @property integer $payment_type
 * @property string $payType
 * @property string $reservationData
 * @property integer $isContentInn
 * @property integer $isTemp
 * @property string $roomName
 * @property string $justification
 * @property string $policyBreakText
 * @property string $rejection_reason
 * @property integer $earlyCheckIn
 * @property integer $lateCheckOut
 * @property integer $earlyOrLateCheckInAmount
 * @property double $cancelPenalty
 * @property string $cancelPenaltyTime
 */
class HBook extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stateID', 'to_buy', 'selfApprove', 'approverNext', 'roomNum', 'stars', 'hotelID', 'roomID', 'corpID', 'userID', 'ReservationIdContentinn', 'netAmount', 'payment_type', 'isContentInn', 'isTemp', 'earlyCheckIn', 'lateCheckOut', 'earlyOrLateCheckInAmount'], 'integer'],
            [['reservationDateTime'], 'safe'],
            [['reservationRQJson', 'bookConfirmationRQJson', 'cancelRQJson', 'cancelConfirmationRQJson', 'reservationData'], 'string'],
            [['cancelPenalty'], 'number'],
            [['bookingReference', 'cityCode', 'autoCancelDateTime', 'checkInDateTime', 'checkOutDateTime', 'discountAmount', 'discountPercent', 'cancelPenaltyTime'], 'string', 'max' => 50],
            [['hotelName', 'hotelPrice', 'tripDepartments', 'serviceFee', 'tripReasons', 'totalPrice', 'refundAmount', 'penaltyPrice', 'cheapestPrice', 'daysBeforeCheckin', 'comment', 'paxTypes', 'paxDesigCodes', 'paxNames', 'paxSurnames', 'paxEmails', 'paxPhones', 'recommendationGUID'], 'string', 'max' => 100],
            [['discountCurrencyCode'], 'string', 'max' => 3],
            [['roomTypeSentence', 'bookingStatus', 'policyBreakText'], 'string', 'max' => 250],
            [['cityId', 'attachment_url', 'payType'], 'string', 'max' => 255],
            [['roomName', 'justification', 'rejection_reason'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stateID' => 'State ID',
            'to_buy' => 'To Buy',
            'selfApprove' => 'Self Approve',
            'approverNext' => 'Approver Next',
            'bookingReference' => 'Booking Reference',
            'roomNum' => 'Room Num',
            'hotelName' => 'Hotel Name',
            'cityCode' => 'City Code',
            'stars' => 'Stars',
            'reservationDateTime' => 'Reservation Date Time',
            'autoCancelDateTime' => 'Auto Cancel Date Time',
            'hotelPrice' => 'Hotel Price',
            'tripDepartments' => 'Trip Departments',
            'serviceFee' => 'Service Fee',
            'tripReasons' => 'Trip Reasons',
            'totalPrice' => 'Total Price',
            'refundAmount' => 'Refund Amount',
            'penaltyPrice' => 'Penalty Price',
            'cheapestPrice' => 'Cheapest Price',
            'daysBeforeCheckin' => 'Days Before Checkin',
            'hotelID' => 'Hotel ID',
            'roomID' => 'Room ID',
            'checkInDateTime' => 'Check In Date Time',
            'checkOutDateTime' => 'Check Out Date Time',
            'discountAmount' => 'Discount Amount',
            'discountPercent' => 'Discount Percent',
            'discountCurrencyCode' => 'Discount Currency Code',
            'comment' => 'Comment',
            'corpID' => 'Corp ID',
            'userID' => 'User ID',
            'paxTypes' => 'Pax Types',
            'paxDesigCodes' => 'Pax Desig Codes',
            'paxNames' => 'Pax Names',
            'paxSurnames' => 'Pax Surnames',
            'paxEmails' => 'Pax Emails',
            'paxPhones' => 'Pax Phones',
            'recommendationGUID' => 'Recommendation Guid',
            'reservationRQJson' => 'Reservation Rqjson',
            'bookConfirmationRQJson' => 'Book Confirmation Rqjson',
            'cancelRQJson' => 'Cancel Rqjson',
            'cancelConfirmationRQJson' => 'Cancel Confirmation Rqjson',
            'roomTypeSentence' => 'Room Type Sentence',
            'ReservationIdContentinn' => 'Reservation Id Contentinn',
            'cityId' => 'City ID',
            'netAmount' => 'Net Amount',
            'attachment_url' => 'Attachment Url',
            'bookingStatus' => 'Booking Status',
            'payment_type' => 'Payment Type',
            'payType' => 'Pay Type',
            'reservationData' => 'Reservation Data',
            'isContentInn' => 'Is Content Inn',
            'isTemp' => 'Is Temp',
            'roomName' => 'Room Name',
            'justification' => 'Justification',
            'policyBreakText' => 'Policy Break Text',
            'rejection_reason' => 'Rejection Reason',
            'earlyCheckIn' => 'Early Check In',
            'lateCheckOut' => 'Late Check Out',
            'earlyOrLateCheckInAmount' => 'Early Or Late Check In Amount',
            'cancelPenalty' => 'Cancel Penalty',
            'cancelPenaltyTime' => 'Cancel Penalty Time',
        ];
    }
}
