<?php

namespace modules\organisation\models;


use Yii;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    public static $db;

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get(self::$db);
    }

    /**
     * Set database connection name
     * @param string $db_name
     */
    public static function setDb($db_name)
    {
        static::$db = 'db_' . trim($db_name);
    }

    /**
     * This search return static method find
     * @param string $db_name
     * @return \yii\db\ActiveQuery
     */
    public static function search($db_name)
    {
        static::setDb($db_name);
        return static::find();
    }

    /**
     * ForHasMany Or HasOne
     * @param string $db_name
     * @return string
     */
    public static function relation($db_name)
    {
        $db_name = str_replace('db_', '', $db_name);
        static::setDb($db_name);
        return get_called_class();
    }
}