<?php

namespace modules\organisation\models;

use Yii;

/**
 * This is the model class for table "t_users".
 *
 * @property string $ID
 * @property string $user_login
 * @property string $user_pass
 * @property string $user_nicename
 * @property string $user_email
 * @property string $user_url
 * @property string $user_registered
 * @property string $user_activation_key
 * @property integer $user_status
 * @property string $display_name
 * @property integer $id_parent
 * @property string $position
 * @property integer $approver
 * @property integer $delegated
 * @property string $lastname
 * @property string $firstname
 * @property string $secondname
 * @property string $passport
 * @property string $passport_type
 * @property string $iin
 * @property string $country
 * @property string $validity
 * @property string $birthdate
 * @property string $ffcard
 * @property string $ffcardair
 * @property string $r_color
 * @property integer $differents
 * @property double $balanse
 * @property double $credit
 * @property integer $role
 * @property integer $company
 * @property string $gender
 * @property integer $company_id
 * @property integer $other_id
 * @property string $api_session_id
 * @property string $second_email
 * @property string $check_date
 */
class TUsers extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_registered', 'check_date'], 'safe'],
            [['user_status', 'id_parent', 'approver', 'delegated', 'differents', 'role', 'company', 'company_id', 'other_id'], 'integer'],
            [['balanse', 'credit'], 'number'],
            [['user_login', 'user_activation_key'], 'string', 'max' => 60],
            [['user_pass'], 'string', 'max' => 64],
            [['user_nicename', 'gender', 'second_email'], 'string', 'max' => 50],
            [['user_email', 'user_url', 'r_color'], 'string', 'max' => 100],
            [['display_name'], 'string', 'max' => 250],
            [['position'], 'string', 'max' => 150],
            [['lastname', 'firstname', 'secondname', 'passport', 'passport_type', 'iin', 'country', 'validity', 'birthdate', 'api_session_id'], 'string', 'max' => 200],
            [['ffcard'], 'string', 'max' => 300],
            [['ffcardair'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'user_login' => 'User Login',
            'user_pass' => 'User Pass',
            'user_nicename' => 'User Nicename',
            'user_email' => 'User Email',
            'user_url' => 'User Url',
            'user_registered' => 'User Registered',
            'user_activation_key' => 'User Activation Key',
            'user_status' => 'User Status',
            'display_name' => 'Display Name',
            'id_parent' => 'Id Parent',
            'position' => 'Position',
            'approver' => 'Approver',
            'delegated' => 'Delegated',
            'lastname' => 'Lastname',
            'firstname' => 'Firstname',
            'secondname' => 'Secondname',
            'passport' => 'Passport',
            'passport_type' => 'Passport Type',
            'iin' => 'Iin',
            'country' => 'Country',
            'validity' => 'Validity',
            'birthdate' => 'Birthdate',
            'ffcard' => 'Ffcard',
            'ffcardair' => 'Ffcardair',
            'r_color' => 'R Color',
            'differents' => 'Differents',
            'balanse' => 'Balanse',
            'credit' => 'Credit',
            'role' => 'Role',
            'company' => 'Company',
            'gender' => 'Gender',
            'company_id' => 'Company ID',
            'other_id' => 'Other ID',
            'api_session_id' => 'Api Session ID',
            'second_email' => 'Second Email',
            'check_date' => 'Check Date',
        ];
    }
}
