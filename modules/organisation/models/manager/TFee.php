<?php

namespace modules\organisation\models\manager;

use Yii;

/**
 * This is the model class for table "t_fee".
 *
 * @property integer $id
 * @property integer $id_company
 * @property string $class
 * @property string $subclass
 * @property double $cash
 * @property double $proc
 * @property string $airline
 * @property string $flight
 * @property string $type
 * @property string $passenger
 * @property string $corp
 */
class TFee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_fee';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company'], 'integer'],
            [['cash', 'proc'], 'number'],
            [['airline'], 'string'],
            [['class', 'flight', 'type', 'passenger'], 'string', 'max' => 100],
            [['subclass'], 'string', 'max' => 200],
            [['corp'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_company' => 'Id Company',
            'class' => 'Class',
            'subclass' => 'Subclass',
            'cash' => 'Cash',
            'proc' => 'Proc',
            'airline' => 'Airline',
            'flight' => 'Flight',
            'type' => 'Type',
            'passenger' => 'Passenger',
            'corp' => 'Corp',
        ];
    }
}
