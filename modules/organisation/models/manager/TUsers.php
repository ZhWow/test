<?php

namespace modules\organisation\models\manager;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_users".
 *
 * @property string $ID
 * @property string $user_login
 * @property string $user_pass
 * @property string $user_nicename
 * @property string $user_email
 * @property string $user_url
 * @property string $user_registered
 * @property string $user_activation_key
 * @property integer $user_status
 * @property string $display_name
 * @property string $position
 * @property string $lastname
 * @property string $cPhone
 * @property string $admName
 * @property string $adm_phone
 * @property string $address
 * @property string $phone
 * @property integer $differents
 * @property double $balanse
 * @property integer $role
 * @property double $limit
 * @property integer $seeCorps
 * @property integer $hotelID
 * @property string $second_email
 * @property string $check_date
 * @property string $vip_cip_airport
 *
 */
class TUsers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_users';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_registered'], 'safe'],
            [['user_status', 'differents', 'role', 'seeCorps', 'hotelID'], 'integer'],
            [['balanse', 'limit'], 'number'],
            [['user_login', 'user_activation_key'], 'string', 'max' => 60],
            [['user_pass'], 'string', 'max' => 64],
            [['user_nicename', 'second_email', 'check_date', 'vip_cip_airport'], 'string', 'max' => 50],
            [['user_email', 'user_url', 'phone'], 'string', 'max' => 100],
            [['display_name'], 'string', 'max' => 250],
            [['position'], 'string', 'max' => 150],
            [['lastname', 'cPhone', 'admName', 'adm_phone', 'address'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'user_login' => 'User Login',
            'user_pass' => 'User Pass',
            'user_nicename' => 'User Nicename',
            'user_email' => 'User Email',
            'user_url' => 'User Url',
            'user_registered' => 'User Registered',
            'user_activation_key' => 'User Activation Key',
            'user_status' => 'User Status',
            'display_name' => 'Display Name',
            'position' => 'Position',
            'lastname' => 'Lastname',
            'cPhone' => 'C Phone',
            'admName' => 'Adm Name',
            'adm_phone' => 'Adm Phone',
            'address' => 'Address',
            'phone' => 'Phone',
            'differents' => 'Differents',
            'balanse' => 'Balanse',
            'role' => 'Role',
            'limit' => 'Limit',
            'seeCorps' => 'See Corps',
            'hotelID' => 'Hotel ID',
            'second_email' => 'Second Email',
            'check_date' => 'Check Date',
            'vip_cip_airport' => 'Vip Cip Airport',
        ];
    }
}
