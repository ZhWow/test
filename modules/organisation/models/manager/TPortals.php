<?php

namespace modules\organisation\models\manager;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_portals".
 *
 * @property integer $id
 * @property string $PortalCode
 * @property string $WSUserName
 * @property string $WSPassword
 * @property string $SubSiteCode
 * @property string $WSSecurityNumber
 * @property string $isActive
 * @property integer $isPRD
 * @property string $AgencyId
 *
 */
class TPortals extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_portals';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isPRD', 'AgencyId'], 'integer'],
            [['PortalCode', 'WSUserName', 'WSPassword', 'SubSiteCode', 'WSSecurityNumber', 'isActive'], 'string', 'max' => 250],
            [['AgencyId'], 'exist', 'skipOnError' => true, 'targetClass' => TUsers::className(), 'targetAttribute' => ['AgencyId' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PortalCode' => 'Portal Code',
            'WSUserName' => 'Wsuser Name',
            'WSPassword' => 'Wspassword',
            'SubSiteCode' => 'Sub Site Code',
            'WSSecurityNumber' => 'Wssecurity Number',
            'isActive' => 'Is Active',
            'isPRD' => 'Is Prd',
            'AgencyId' => 'Agency ID',
        ];
    }
}
