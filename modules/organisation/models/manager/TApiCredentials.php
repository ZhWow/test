<?php

namespace modules\organisation\models\manager;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_api_credentials".
 *
 * @property integer $id
 * @property string $agency_id
 * @property integer $active
 * @property string $pult_type
 * @property string $pult_name
 * @property string $url
 * @property string $timestamp
 * @property string $last_request_time
 *
 */
class TApiCredentials extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_api_credentials';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_manager');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agency_id', 'pult_type', 'pult_name', 'url', 'last_request_time'], 'required'],
            [['agency_id', 'active'], 'integer'],
            [['timestamp', 'last_request_time'], 'safe'],
            [['pult_type', 'pult_name'], 'string', 'max' => 50],
            [['url'], 'string', 'max' => 500],
            [['pult_name'], 'unique'],
            [['agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => TUsers::className(), 'targetAttribute' => ['agency_id' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agency_id' => 'Agency ID',
            'active' => 'Active',
            'pult_type' => 'Pult Type',
            'pult_name' => 'Pult Name',
            'url' => 'Url',
            'timestamp' => 'Timestamp',
            'last_request_time' => 'Last Request Time',
        ];
    }
}
